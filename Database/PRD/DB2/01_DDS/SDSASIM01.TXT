      *--------------------------------------------------------------*
      *  Autor   : Canvia.                                           *
      *--------------------------------------------------------------*
      *  Descripci�n: Guarda las asignaciones de los usuarios        *
      *  a los perfiles de sistemas                                  *
      *--------------------------------------------------------------*
      * NOMBRE DE ARCHIVO NATIVO :  SDSASIM01                        *
      * ALIAS  DE ARCHIVO        :  SEGASIGNACION                    *
      *--------------------------------------------------------------*
     A                                      UNIQUE
     A          R RASIM01
      *
     A            VCODASI       10A         COLHDG('C�DIGO DE ASIGNACION')
     A                                      TEXT('VCODASIGNACION')
     A            VCODPRF       10A         COLHDG('C�DIGO DE PERFIL')
     A                                      TEXT('VCODPERFIL')
     A            VCODSIS       10A         COLHDG('C�DIGO DE  SISTEMA')
     A                                      TEXT('VCODSISTEMA')
     A            WEBUSR        15A         COLHDG('C�DIGO DE  USUARIO')
     A                                      TEXT('WEBUSR')
     A            VESTMOD        1S 0       COLHDG('ESTADO')
     A                                      TEXT('SIESTADO')
      * DATOS DE CREACION
     A            VUCRMOD       50A         COLHDG('USUARIO' 'CREACION')
     A                                      TEXT('VUSUCREACION')
     A            VDCRMOD         Z         COLHDG('FECHA  ' 'CREACION')
     A                                      TEXT('DFECCREACION')
     A            VHCRMOD       50A         COLHDG('HOST   ' 'CREACION')
     A                                      TEXT('VHSTCREACION')
      * DATOS DE MODIFICACION
     A            VUMOMOD       50A         COLHDG('USUARIO' 'MODIFICA')
     A                                      TEXT('VUSUACTUALIZACION')
     A                                      ALWNULL
     A            VDMOMOD         Z         COLHDG('FECHA  ' 'MODIFICA')
     A                                      TEXT('DFECACTUALIZACION')
     A                                      ALWNULL
     A            VHMOMOD       50A         COLHDG('HOST   ' 'MODIFICA')
     A                                      TEXT('VHSTACTUALIZACION')
     A                                      ALWNULL
      *
     A          K VCODASI
     A          K VCODPRF
     A          K VCODSIS
     A          K WEBUSR
