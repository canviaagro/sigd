-- CREATE TABLE AGRCYFILES.CNTRLWEB
-- (
	-- WEBUSR                VARCHAR(20)  NOT NULL 
-- );



-- ALTER TABLE AGRCYFILES.CNTRLWEB
	-- ADD CONSTRAINT XPKUsuario  PRIMARY KEY (WEBUSR);


CREATE TABLE AGRCYFILES.SegAccion
(
	vCodAccion            VARCHAR(10)  NOT NULL ,
	vUsuCreacion          VARCHAR(50) ,
	vHstCreacion          VARCHAR(50) ,
	vUsuActualizacion     VARCHAR(50) ,
	vHstActualizacion     VARCHAR(50) ,
	vNombre               VARCHAR(100) ,
	vDescripcion          VARCHAR(200) ,
	siEstado              INTEGER ,
	dFecCreacion          DATE ,
	dFecActualizacion     DATE 
);



ALTER TABLE AGRCYFILES.SegAccion
	ADD CONSTRAINT AGRCYFILES.XPKAccion  PRIMARY KEY (vCodAccion);



CREATE TABLE AGRCYFILES.SegAsignacion
(
	vCodAsignacion        VARCHAR(10)  NOT NULL ,
	vCodPerfil            VARCHAR(10)  NOT NULL ,
	vCodSistema           VARCHAR(10)  NOT NULL ,
	WEBUSR                CHAR(15)  NOT NULL ,
	siEstado              INTEGER ,
	vUsuCreacion          VARCHAR(50) ,
	dFecCreacion          DATE ,
	vHstCreacion          VARCHAR(50) ,
	vUsuActualizacion     VARCHAR(50) ,
	dFecActualizacion     DATE ,
	vHstActualizacion     VARCHAR(50) 
);



ALTER TABLE AGRCYFILES.SegAsignacion
	ADD CONSTRAINT AGRCYFILES.XPKAsignacion  PRIMARY KEY (vCodAsignacion,vCodPerfil,vCodSistema,WEBUSR);



CREATE TABLE AGRCYFILES.SegLogAcceso
(
	vCodAcceso            VARCHAR(10)  NOT NULL ,
	WEBUSR                VARCHAR(10)  NOT NULL ,
	vCodPerfil            VARCHAR(10)  NOT NULL ,
	vCodSistema           VARCHAR(10)  NOT NULL ,
	siEstado              INTEGER ,
	vUsuCreacion          VARCHAR(50) ,
	dFecCreacion          DATE ,
	vHstCreacion          VARCHAR(50) ,
	dIniSesion            DATE ,
	dFinSesion            DATE 
);



ALTER TABLE AGRCYFILES.SegLogAcceso
	ADD CONSTRAINT AGRCYFILES.XPKLogAcceso  PRIMARY KEY (vCodAcceso,WEBUSR,vCodPerfil,vCodSistema);



CREATE TABLE AGRCYFILES.SegModulo
(
	vCodModulo            VARCHAR(10)  NOT NULL ,
	vCodSistema           VARCHAR(10)  NOT NULL ,
	siEstado              INTEGER ,
	vUsuCreacion          VARCHAR(50) ,
	dFecCreacion          DATE,
	vHstCreacion          VARCHAR(50) ,
	vUsuActulizacion      VARCHAR(50) ,
	dFecActualizacion     DATE ,
	vHstActualizacion     VARCHAR(50) ,
	vNombre               VARCHAR(100) ,
	vDescripcion          VARCHAR(200) ,
	vCodModPadre          VARCHAR(20) ,
	vURL                  VARCHAR(200) ,
	iNivel                INTEGER,
	vEstiloIcono		  VARCHAR(200)
);


ALTER TABLE AGRCYFILES.SegModulo
	ADD CONSTRAINT AGRCYFILES.XPKModulo  PRIMARY KEY (vCodModulo,vCodSistema);



CREATE TABLE AGRCYFILES.SegOpcion
(
	vCodOpcion            VARCHAR(10)  NOT NULL ,
	vCodModulo            VARCHAR(10)  NOT NULL ,
	vCodSistema           VARCHAR(10)  NOT NULL ,
	siEstado              INTEGER ,
	vUsuCreacion          VARCHAR(50) ,
	dFecCreacion          DATE ,
	vHstCreacion          VARCHAR(50) ,
	vUsuActualizacion     VARCHAR(50) ,
	dFecActualizacion     DATE ,
	vHstActualizacion     VARCHAR(50) ,
	vNombre               VARCHAR(100) ,
	vDescripcion          VARCHAR(200) ,
	vCodOpcPadre          VARCHAR(10) ,
	vURL                  VARCHAR(200) ,
	iNivel                INTEGER ,
	vEstiloIcono		  VARCHAR(200)
);



ALTER TABLE AGRCYFILES.SegOpcion
	ADD CONSTRAINT AGRCYFILES.XPKOpcion  PRIMARY KEY (vCodOpcion,vCodModulo,vCodSistema);



CREATE TABLE AGRCYFILES.SegPerfil
(
	vCodPerfil            VARCHAR(10)  NOT NULL ,
	vCodSistema           VARCHAR(10)  NOT NULL ,
	siEstado              INTEGER ,
	vUsuCreacion          VARCHAR(50) ,
	dFecCreacion          DATE ,
	vHstCreacion          VARCHAR(50) ,
	vUsuActualizacion     VARCHAR(50) ,
	dFecActualizacion     DATE ,
	vHstActualizacion     VARCHAR(50) ,
	vNombre               VARCHAR(100) ,
	vDescripcion          VARCHAR(200) 
);



ALTER TABLE AGRCYFILES.SegPerfil
	ADD CONSTRAINT AGRCYFILES.XPKPerfil  PRIMARY KEY (vCodPerfil,vCodSistema);



CREATE TABLE AGRCYFILES.SegPermiso
(
	vCodPermiso           CHAR(10)  NOT NULL ,
	vCodOpcion            VARCHAR(10)  NOT NULL ,
	vCodModulo            VARCHAR(10)  NOT NULL ,
	vCodSistema           VARCHAR(10)  NOT NULL ,
	vCodAccion            VARCHAR(10),
	vCodPerfil            VARCHAR(10)  NOT NULL ,
	siEstado              INTEGER ,
	vUsuCreacion          VARCHAR(50) ,
	dFecCreacion          DATE ,
	vHstCreacion          VARCHAR(50) ,
	vUsuActualizacion     VARCHAR(50) ,
	dFecActualizacion     DATE ,
	vHstActualizacion     VARCHAR(50) 
);



ALTER TABLE AGRCYFILES.SegPermiso
	ADD CONSTRAINT AGRCYFILES.XPKPermiso  PRIMARY KEY (vCodPermiso,vCodOpcion,vCodModulo,vCodSistema,vCodAccion,vCodPerfil);



CREATE TABLE AGRCYFILES.SegSistema
(
	vCodSistema           VARCHAR(10)  NOT NULL ,
	vUsuCreacion          VARCHAR(50) ,
	vHstCreacion          VARCHAR(50) ,
	vUsuActualizacion     VARCHAR(50) ,
	vHstActualizacion     VARCHAR(50) ,
	vNombre               VARCHAR(100) ,
	vDescripcion          VARCHAR(1000) ,
	vRuta                 VARCHAR(200) ,
	siEstado              INTEGER ,
	dFecCreacion          DATE ,
	dFecActualizacion     DATE 
);



ALTER TABLE AGRCYFILES.SegSistema
	ADD CONSTRAINT AGRCYFILES.XPKSistema  PRIMARY KEY (vCodSistema);