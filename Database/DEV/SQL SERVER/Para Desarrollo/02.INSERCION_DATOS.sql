DELETE FROM [dbo].[MaeSecuencial]

DELETE FROM [dbo].[MovDocumento]
DBCC CHECKIDENT (MovDocumento, RESEED, 1)

--DELETE FROM [dbo].[SegUsuario]
----DBCC CHECKIDENT (SegUsuario, RESEED, 0)

--DELETE FROM [dbo].[MaeTrabajador]
--DBCC CHECKIDENT (MaeTrabajador, RESEED, 0)

--DELETE FROM [dbo].[MaeArea]
--DBCC CHECKIDENT (MaeArea, RESEED, 0)

DELETE FROM MaeRepresentante
DBCC CHECKIDENT (MaeRepresentante, RESEED, 0)

DELETE FROM MaeTipDocumento
DBCC CHECKIDENT (MaeTipDocumento, RESEED, 0)
--TRUNCATE TABLE MaeTipDocumento;

DELETE FROM MaeEmpresa
DBCC CHECKIDENT (MaeEmpresa, RESEED, 0)


SET IDENTITY_INSERT [dbo].[MaeArea] ON 
GO

INSERT [dbo].[MaeArea] ([iCodArea], [vAbreviatura], [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion], [vUsuActualizacion], [dFecActualizacion], [vHstActualizacion]) 
VALUES 
(2, N'RRHH', N'Recursos Humanos', 1, N'ADMIN', GETDATE(), N'10.0.0.1', N'DBAGRSIGD', N'ADMIN', 'ADMIN',  NULL, NULL, NULL),
(3, N'ADM', N'Administraci�n', 1, N'ADMIN', GETDATE(), N'10.0.0.1', N'DBAGRSIGD', N'ADMIN', 'ADMIN',  NULL, NULL, NULL),
(4, N'SIS', N'Sistemas', 1, N'ADMIN', GETDATE(), N'10.0.0.1', N'DBAGRSIGD', N'ADMIN', 'ADMIN',  NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[MaeArea] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeTipDocIdentidad] ON 
GO
INSERT [dbo].[MaeTipDocIdentidad] ([iCodTipDocIdentidad], [vAbreviatura], [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion], [vUsuActualizacion], [dFecActualizacion], [vHstActualizacion]) 
VALUES (2, N'RUC', N'RUC', 1, N'MIGUEL G�ERE', GETDATE(), N'10.0.0.1', N'DBAGRSIGD', N'mguere', 'Administrador',  NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[MaeTipDocIdentidad] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeTrabajador] ON 
GO
INSERT [dbo].[MaeTrabajador] ([iCodTrabajador], [vNumDocumento], [vNombres], [siEstado], [vCargo], [vUbicacion], [iAnexo], [vEmail], [vApePaterno], [vApeMaterno], [siEsJefe],[siFirma], [iCodArea], [iCodTipDocIdentidad], [vCelular], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion]) 
VALUES (2, N'46954968', N'Miguel', 1, N'Analista T�cnico', N'Lima', 213, N'rguere@canvia.com', N'G�ere', N'D�az', 0, 1, 3, 1, N'961521485', N'MIGUEL G�ERE', GETDATE(), N'10.0.0.1', N'DBAGRSIGD', N'MGUERE', N'Administrador')
GO
SET IDENTITY_INSERT [dbo].[MaeTrabajador] OFF
GO

--SET IDENTITY_INSERT [dbo].[SegUsuario] ON 
--GO
INSERT [dbo].[SegUsuario] ([iCodUsuario], [iCodPerfil], [dFecAcceso], [siEstado], [iCodTrabajador], [WEBUSR], [vCodPerfil], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion]) 
VALUES (2, 1, GETDATE(), 1, 2, N'AGRMGD', N'ADMIN', N'MIGUEL G�ERE', GETDATE(), N'10.0.0.1', 'DBAGRSIGD', N'mguere', 'Administrador')
GO
--SET IDENTITY_INSERT [dbo].[SegUsuario] OFF 
--GO

SET IDENTITY_INSERT [dbo].[MaeEmpresa] ON 
GO

INSERT INTO [dbo].[MaeEmpresa]
           (iCodEmpresa, [vAbreviatura], [vDescripcion], vDireccion, [siEstado], [vUsuCreacion],[dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion])
     VALUES
           (1, 'EMP01', 'EMPRESA 01', 'Direcci�n 1', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (2, 'EMP02', 'EMPRESA 02', 'Direcci�n 2', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (3, 'EMP03', 'EMPRESA 03', 'Direcci�n 3', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN')
GO

SET IDENTITY_INSERT [dbo].[MaeEmpresa] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeRepresentante] ON 
GO

INSERT INTO [dbo].[MaeRepresentante]
           (iCodRepresentante, [vNumDocumento], [vNombres], [siEstado], [siTipRepresentante], [vDireccion], [vTelefono], [vCelular], [vEmail], [iCodEmpresa], 
		   [iCodTipDocIdentidad], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion])
     VALUES
           (1, '12548965', 'Ivan Carrasco', 1, 1, 'PJE ACU�A 666', '921545185', '921545185', 'jcarrasco@canvia.com', 1, 
		   1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (2, '45353543', 'Andrea Sanchez', 1, 1, 'casita 346', '57657', '32323245', 'asanchez@canvia.com', 1, 
		   1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (3, '56757575', 'Jesus Recinas', 1, 1, 'emp ddd 666', '567575', '567575', 'jrecinas@canvia.com', 2, 
		   1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (4, '4574647', 'Zorka Diaz', 1, 1, 'direccion test', '578768', '546464', 'zdiaz@canvia.com', 2, 
		   1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (5, '675675876', 'Edson Ramirez', 1, 1, 'petit mouth 433', '5676575', '345353', 'gsayayin@canvia.com', 3, 
		   1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN')
GO
SET IDENTITY_INSERT [dbo].[MaeRepresentante] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeTipDocumento] ON
GO
INSERT INTO [dbo].[MaeTipDocumento] ( iCodTipDocumento, [vAbreviatura], [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (1, 'CAR', 'CARTA', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (2, 'INF', 'INFORME', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (3, 'MEM', 'MEMORANDUM', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (4, 'OFI', 'OFICIO', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN'),
		   (5, 'RES', 'RESOLUCI�N', 1, 'ADMIN', GETDATE(), 'localhost', 'localhost', 'ADMIN', 'ADMIN')
GO
SET IDENTITY_INSERT [dbo].[MaeTipDocumento] OFF
GO

DELETE FROM [dbo].[MaeAsunto]
DBCC CHECKIDENT ([MaeAsunto], RESEED, 0)

SET IDENTITY_INSERT [dbo].[MaeAsunto] ON
GO
INSERT INTO [dbo].[MaeAsunto]
           (iCodAsunto, [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion])
     VALUES
           (1, 'ASUNTO INTERNO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
		   (2, 'ASUNTO EXTERNO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
		   (3, 'ASUNTO URGENTE', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
		   (4, 'ASUNTO INSURGENTE', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
		   (5, 'ASUNTO DIVERGENTE', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
		   (6, 'ASUNTO CONVERGENTE', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)
GO

SET IDENTITY_INSERT [dbo].[MaeAsunto] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeTrabajador] ON
GO
INSERT [dbo].[MaeTrabajador] (iCodTrabajador, [vNumDocumento], [vNombres], [siEstado], [vCargo], [vUbicacion], [iAnexo], [vEmail], [vApePaterno], [vApeMaterno], [siEsJefe], [iCodArea], [iCodTipDocIdentidad], [vCelular], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion],[siFirma]) 
VALUES 
(3, N'34535567', N'Ever Daniel', 1, N'Analista T�cnico', N'Lima', 213, N'EVARGASY@canvia.com', N'VARGAS', N'YALICO', 1, 3, 1, N'999234567', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,1),
(4, N'98765456', N'Carolina', 1, N'Analista', N'Lima', 213, N'cyataco@agrobanco.com', N'YATACO', N'YALICO', 0, 3, 1, N'999234567', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,1),
(5, N'98765456', N'Marco', 1, N'Analista Programador', N'Lima', 213, N'mbravov@canvia.com', N'BRAVO', N'VEGA', 1, 2, 1, N'999234567', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,1),
(6, N'98765456', N'Oscar', 1, N'JP', N'Lima', 213, N'ocastillo@canvia.com', N'Castillo', N'VEGA', 1, 4, 1, N'999234567', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,1),
(7, N'98765456', N'Carlos', 1, N'Analista Programador', N'Lima', 213, N'mbravov@canvia.com', N'Ramirez', N'VEGA', 0, 4, 1, N'999234567', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,1),
(8, N'98765456', N'Christian', 1, N'Analista Programador', N'Lima', 213, N'mbravov@canvia.com', N'Ramirez', N'VEGA', 0, 4, 1, N'999234567', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,1)
GO

SET IDENTITY_INSERT [dbo].[MaeTrabajador] OFF
GO


INSERT [dbo].[SegUsuario] (iCodUsuario, [iCodPerfil], [dFecAcceso], [siEstado], [iCodTrabajador], [WEBUSR], [vCodPerfil], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion], [vUsuActualizacion], [dFecActualizacion], [vHstActualizacion]) 
VALUES 
(3, 1, GETDATE(), 1, 3, N'AGREGV', N'ADMIN', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER, NULL, NULL, NULL),
(4, 1, GETDATE(), 1, 4, N'AGRABD', N'ADMIN', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER, NULL, NULL, NULL),
(5, 1, GETDATE(), 1, 5, N'AGRDOC', N'ADMIN', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER, NULL, NULL, NULL),
(6, 1, GETDATE(), 1, 6, N'AGRARR', N'ADMIN', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER, NULL, NULL, NULL)
GO


