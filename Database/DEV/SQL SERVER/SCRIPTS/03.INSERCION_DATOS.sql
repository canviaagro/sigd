USE [DBAgrSIGD]
GO

/*INSERCI�N DE DATOS - ESTADO DEL DOCUMENTO*/
DELETE FROM [dbo].[ParParametro]
GO

DELETE FROM [DBO].[SegUsuario]
GO

DELETE FROM [DBO].[MaeTrabajador]
GO

DELETE FROM [DBO].[MaeFeriado] 
GO

DELETE FROM [DBO].[MaeRepresentante]
GO

DELETE FROM [DBO].[MaeTipDocIdentidad]
GO

DELETE FROM [DBO].[MaeArea] 
GO

INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (1, 0, '', NULL, NULL, 'ESTADOS DEL DOCUMENTO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 1, 'ING', NULL, 0, 'INGRESADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 2, 'PEN', NULL, 1, 'PENDIENTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 3, 'ANU', NULL, 6, 'ANULADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 4, 'DER', NULL, 5, 'DERIVADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 5, 'ATE', NULL, 4, 'ATENDIDO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 6, 'PRO', NULL, 3, 'EN PROCESO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (1, 7, 'DEV', NULL, 2, 'DEVUELTO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
		  
GO
/*INSERCI�N DE DATOS - TIPO ORIGEN*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (2, 0, '', 'ORIGEN',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (2, 1, 'IN', 'INTERNO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (2, 2, 'EXT', 'EXTERNO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
		   
		  
GO
/*INSERCI�N DE DATOS - PRIORIDAD*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (3, 0, '', NULL,NULL,'PRIORIDAD',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 1, 'B', NULL,4, 'BAJA',0,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 2, 'M', NULL,3, 'NORMAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 3, 'A', NULL,2, 'URGENTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (3, 4, 'U', NULL,1, 'MUY URGENTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
	
/*INSERCI�N DE DATOS - CORRELATIVOS*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (4, 0, '', 'CORRELATIVOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (4, 1, '[CORRELATIVO]-[ANIO]', 'MESA DE PARTES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (4, 2, '[TIPODOC]-[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]-[ANIO]-AGROBANCO-[AREA]', 'BANDEJA DE ENTRADA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (4, 3, '[TIPODOC]-[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]-[ANIO]-AGROBANCO-[AREA]', 'BANDEJA DE SALIDA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)

/*INSERCI�N DE DATOS - ESTADO*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (5, 0, '', 'ESTADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (5, 1, '1', 'ACTIVO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (5, 2, '0', 'INACTIVO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
		   
/*INSERCI�N DE DATOS - TIPO REPRESENTANTE*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (6, 0, '', 'TIPO REPRESENTANTE',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (6, 1, '1', 'NATURAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (6, 2, '2', 'JUR�DICO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
/*INSERCI�N DE DATOS - TIPO FERIADO*/
INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (7, 0, '', 'TIPO FERIADO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (7, 1, '1', 'FIN DE SEMANA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (7, 2, '2', 'FERIADO GENERAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (7, 3, '3', 'FERIADO ESTATAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
	
INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(8,0,'','TIPO ACCIONES',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(8,1,1,'NECESARIA',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8, 2, 2, 'REVISAR/INFORMAR', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,3,3,'CONOCIMIENTOS Y FINES',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,4,4,'PREPARAR RESPUESTA',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,5,5,'PARA DIRECTORIO',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(8,6,6,'ARCHIVAR',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
( 8,7,7,'OTROS',1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)

/*INSERCI�N DE DATOS - IDENTIFICADOR JEFE*/
INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(9,0,'','ESTADO JEFE',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(9,1,1,'SI',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(9,2,2, 'NO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER)

INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(10,0,'','INDICACIONES',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(10,1,1,'COORDINAR',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10, 2, 2, 'TOMAR ACCI�N', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,3,3,'EMITIR RESPUESTA PERSONAL',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,4,4,'EMITIR PROYECTO DE RESPUESTA',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,5,5,'PARA CONOCIMIENTO',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,6,6,'PARA SEGUIMIENTO',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(10,7,7,'OTROS',1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)

INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(11,0,'','AFIRMACION',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(11,1,1,'SI',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(11,2,0, 'NO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER)



INSERT  INTO ParParametro(iCodGrupo, iCodParametro,	vValor,	vCampo,	siEstado	,vUsuCreacion	,dFecCreacion	,
vHstCreacion,	vInsCreacion,	vLgnCreacion, vRolCreacion)
VALUES
(12,0,'','MOTIVOS FIRMA',1, SYSTEM_USER,GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
--DETALLE
(12,1,1,'SOY EL AUTOR DEL DOCUMENTO',1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(12,2,2, 'EN SE�AL DE CONFORMIDAD', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER),
(12,3,3, 'DOY V�B�', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME,@@SERVICENAME, SYSTEM_USER, USER)

SET IDENTITY_INSERT [MaeFeriado] ON 
GO

INSERT [DBO].[MaeFeriado] ([iCodFeriado], [dFecha], [vDescripcion], [iTipoFeriado], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion]) 
VALUES 
(1, GETDATE(), N'A�O NUEVO', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(2, GETDATE(), N'JUEVES SANTO', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(3, GETDATE(), N'VIERNES SANTO', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(4, GETDATE(), N'D�A DEL TRABAJO', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(5, GETDATE(), N'SAN PEDRO-SAN PABLO', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(6, GETDATE(), N'NO LABORABLE X GOB', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(7, GETDATE(), N'FIESTAS PATRIAS', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(8, GETDATE(), N'FIESTAS PATRIAS', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(9, GETDATE(), N'SANTA ROSA DE LIMA', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(10, GETDATE(), N'COMBATE DE ANGAMOS', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(11, GETDATE(), N'NO LABORABLE X GOB', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(12, GETDATE(), N'TODOS LOS SANTOS', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(13, GETDATE(), N'INMACULADA CONCEPCI�', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(14, GETDATE(), N'NAVIDAD', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER),
(15, GETDATE(), N'NO LABORABLE X GOB', 2, 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)

SET IDENTITY_INSERT [MaeFeriado] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeTipDocIdentidad] ON 
GO

INSERT [dbo].[MaeTipDocIdentidad] ([iCodTipDocIdentidad], [vAbreviatura], [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion]) 
VALUES 
(1, N'DNI', N'DNI', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)

GO

SET IDENTITY_INSERT [dbo].[MaeTipDocIdentidad] OFF
GO

SET IDENTITY_INSERT [dbo].[MaeArea] ON 
GO

/*USUARIO PARA CREACION DE DATA ,SOLO MAESTROS*/
INSERT [dbo].[MaeArea] ([iCodArea], [vAbreviatura], [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion]) 
VALUES 
(1, N'AREADEF', N'AREA_DEFECTO', 1, SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER)
GO

SET IDENTITY_INSERT [dbo].[MaeArea] OFF 
GO

SET IDENTITY_INSERT [dbo].[MaeTrabajador] ON 
GO

INSERT [dbo].[MaeTrabajador] ([iCodTrabajador], [vNumDocumento], [vNombres], [siEstado], [vCargo], [vUbicacion], [iAnexo], [vEmail], [vApePaterno], [vApeMaterno], [siEsJefe], [iCodArea], [iCodTipDocIdentidad], [vCelular], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion],[siFirma]) 
VALUES 
(1, N'99999999', N'USUARIO MAESTRO', 1, N'AGRSIGD', N'Lima', 213, N'cyataco@agrobanco.com.pe', N'AGRSIGD', N'AGRSIGD', 0, 1, 1, N'999999999', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER,0);
GO

SET IDENTITY_INSERT [dbo].[MaeTrabajador] OFF 
GO

INSERT [dbo].[SegUsuario] (iCodUsuario, [iCodPerfil], [dFecAcceso], [siEstado], [iCodTrabajador], [WEBUSR], [vCodPerfil], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion]) 
VALUES 
(ISNULL((SELECT MAX(iCodUsuario)+1 FROM [SegUsuario]), 1),1, GETDATE(), 1, 1, N'AGRSIGD', N'ADMIN', SYSTEM_USER, GETDATE(), @@SERVERNAME, @@SERVICENAME, SYSTEM_USER, USER);
GO
