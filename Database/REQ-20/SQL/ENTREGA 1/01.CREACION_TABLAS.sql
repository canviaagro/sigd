USE [DBAgrSIGD]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SEGUSUARIO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[SegUsuario]'))
ALTER TABLE [dbo].[SegUsuario] DROP CONSTRAINT [FK_SEGUSUARIO_MAETRABAJADOR]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MovDocumentoDerivacion_MaeTrabajador]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]'))
ALTER TABLE [dbo].[MovDocumentoDerivacion] DROP CONSTRAINT [FK_MovDocumentoDerivacion_MaeTrabajador]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MovDocumentoDerivacion_MaeArea]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]'))
ALTER TABLE [dbo].[MovDocumentoDerivacion] DROP CONSTRAINT [FK_MovDocumentoDerivacion_MaeArea]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] DROP CONSTRAINT [FK_MOVDOCUMENTO_MAETRABAJADOR]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAETIPDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] DROP CONSTRAINT [FK_MOVDOCUMENTO_MAETIPDOCUMENTO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAEREPRESENTANTE]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] DROP CONSTRAINT [FK_MOVDOCUMENTO_MAEREPRESENTANTE]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] DROP CONSTRAINT [FK_MOVDOCUMENTO_MAEAREA]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MOVDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento] DROP CONSTRAINT [FK_MOVDOCMOVIMIENTO_MOVDOCUMENTO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento] DROP CONSTRAINT [FK_MOVDOCMOVIMIENTO_MAETRABAJADOR]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento] DROP CONSTRAINT [FK_MOVDOCMOVIMIENTO_MAEAREA]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MovDocCom__iCodD__01D345B0]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocComentario]'))
ALTER TABLE [dbo].[MovDocComentario] DROP CONSTRAINT [FK__MovDocCom__iCodD__01D345B0]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCANEXO_MOVDOCMOVIMIENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocAnexo]'))
ALTER TABLE [dbo].[MovDocAnexo] DROP CONSTRAINT [FK_MOVDOCANEXO_MOVDOCMOVIMIENTO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCADJUNTO_MOVDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocAdjunto]'))
ALTER TABLE [dbo].[MovDocAdjunto] DROP CONSTRAINT [FK_MOVDOCADJUNTO_MOVDOCUMENTO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAETRABAJADOR_MAETIPDOCIDENTIDAD]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]'))
ALTER TABLE [dbo].[MaeTrabajador] DROP CONSTRAINT [FK_MAETRABAJADOR_MAETIPDOCIDENTIDAD]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAETRABAJADOR_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]'))
ALTER TABLE [dbo].[MaeTrabajador] DROP CONSTRAINT [FK_MAETRABAJADOR_MAEAREA]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAESECUENCIAL_MAETIPDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeSecuencial]'))
ALTER TABLE [dbo].[MaeSecuencial] DROP CONSTRAINT [FK_MAESECUENCIAL_MAETIPDOCUMENTO]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAESECUENCIAL_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeSecuencial]'))
ALTER TABLE [dbo].[MaeSecuencial] DROP CONSTRAINT [FK_MAESECUENCIAL_MAEAREA]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAEREPRESENTANTE_MAETIPDOCIDENTIDAD]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]'))
ALTER TABLE [dbo].[MaeRepresentante] DROP CONSTRAINT [FK_MAEREPRESENTANTE_MAETIPDOCIDENTIDAD]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAEREPRESENTANTE_MAEEMPRESA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]'))
ALTER TABLE [dbo].[MaeRepresentante] DROP CONSTRAINT [FK_MAEREPRESENTANTE_MAEEMPRESA]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SegUsuario]') AND type in (N'U'))
DROP TABLE [dbo].[SegUsuario]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParParametro]') AND type in (N'U'))
DROP TABLE [dbo].[ParParametro]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocumentoDerivacion]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocumentoFirma]') AND type in (N'U'))
BEGIN
	DROP TABLE [dbo].[MovDocumentoFirma]
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocMovimientoTEST]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocMovimientoTEST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocMovimiento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocComentario]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocComentario]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocAnexo]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocAnexo]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocAdjunto]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocAdjunto]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]') AND type in (N'U'))
DROP TABLE [dbo].[MaeTrabajador]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeTipDocumento]') AND type in (N'U'))
DROP TABLE [dbo].[MaeTipDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeTipDocIdentidad]') AND type in (N'U'))
DROP TABLE [dbo].[MaeTipDocIdentidad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeSecuencial]') AND type in (N'U'))
DROP TABLE [dbo].[MaeSecuencial]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]') AND type in (N'U'))
DROP TABLE [dbo].[MaeRepresentante]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeEmpresa]') AND type in (N'U'))
DROP TABLE [dbo].[MaeEmpresa]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeAsunto]') AND type in (N'U'))
DROP TABLE [dbo].[MaeAsunto]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeArea]') AND type in (N'U'))
DROP TABLE [dbo].[MaeArea]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocumento]') AND type in (N'U'))
DROP TABLE [dbo].[MovDocumento]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeArea]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeArea](
	[iCodArea] [int] IDENTITY(1,1) NOT NULL,
	[vAbreviatura] [varchar](20) NULL,
	[vDescripcion] [varchar](100) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAEAREA] PRIMARY KEY CLUSTERED 
(
	[iCodArea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeAsunto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeAsunto](
	[iCodAsunto] [int] IDENTITY(1,1) NOT NULL,
	[vDescripcion] [varchar](1000) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAEASUNTO] PRIMARY KEY CLUSTERED 
(
	[iCodAsunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeEmpresa]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeEmpresa](
	[iCodEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[vAbreviatura] [varchar](20) NULL,
	[vDescripcion] [varchar](100) NOT NULL,
	[vDireccion] [varchar](100) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](50) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](50) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAEEMPRESA] PRIMARY KEY CLUSTERED 
(
	[iCodEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeRepresentante](
	[iCodRepresentante] [int] IDENTITY(1,1) NOT NULL,
	[vNumDocumento] [varchar](20) NULL,
	[vNombres] [varchar](200) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[siTipRepresentante] [smallint] NULL,
	[vDireccion] [varchar](500) NULL,
	[vTelefono] [varchar](20) NULL,
	[vCelular] [varchar](20) NULL,
	[vEmail] [varchar](100) NULL,
	[vAnexo] [varchar](100) NULL,
	[vFax] [varchar](100) NULL,
	[iCodEmpresa] [int] NOT NULL,
	[iCodTipDocIdentidad] [int] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAEREPRESENTANTE] PRIMARY KEY CLUSTERED 
(
	[iCodRepresentante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeSecuencial]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeSecuencial](
	[iAnio] [int] NULL,
	[siEstado] [int] NOT NULL,
	[iCorrelativo] [int] NULL,
	[iCodSecuencial] [int] NOT NULL,
	[iCodArea] [int]  NULL,
	[iCodTipDocumento] [int]  NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[vCorrelativo] [varchar](50) NULL,
	[siOrigen] [smallint] NULL,
	[fUsado] bit
 CONSTRAINT [PK_MAESECUENCIAL] PRIMARY KEY CLUSTERED 
(
	[iCodSecuencial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeTipDocIdentidad]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeTipDocIdentidad](
	[iCodTipDocIdentidad] [int] IDENTITY(1,1) NOT NULL,
	[vAbreviatura] [varchar](20) NULL,
	[vDescripcion] [varchar](100) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAETIPDOCIDENTIDAD] PRIMARY KEY CLUSTERED 
(
	[iCodTipDocIdentidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeTipDocumento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeTipDocumento](
	[iCodTipDocumento] [int] IDENTITY(1,1) NOT NULL,
	[vAbreviatura] [varchar](20) NULL,
	[vDescripcion] [varchar](1000) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAETIPDOCUMENTO] PRIMARY KEY CLUSTERED 
(
	[iCodTipDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeTrabajador](
	[iCodTrabajador] [int] IDENTITY(1,1) NOT NULL,
	[vNumDocumento] [varchar](20) NULL,
	[vNombres] [varchar](20) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vCargo] [varchar](20) NULL,
	[vUbicacion] [varchar](20) NULL,
	[iAnexo] [int] NULL,
	[vEmail] [varchar](50) NULL,
	[vApePaterno] [varchar](20) NULL,
	[vApeMaterno] [varchar](20) NULL,
	[siEsJefe] [smallint] NULL,
	[siFirma] [smallint] NOT NULL,
	[iCodArea] [int] NOT NULL,
	[iCodTipDocIdentidad] [int] NOT NULL,
	[vCelular] [varchar](20) NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MAETRABAJADOR] PRIMARY KEY CLUSTERED 
(
	[iCodTrabajador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocAdjunto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MovDocAdjunto](
	[iCodDocAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[iCodLaserfiche] [int] NULL,
	[siEstado] [smallint] NOT NULL,
	[iCodDocumento] [int] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MOVDOCADJUNTO] PRIMARY KEY CLUSTERED 
(
	[iCodDocAdjunto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocAnexo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MovDocAnexo](
	[iCodDocAnexo] [int] IDENTITY(1,1) NOT NULL,
	[iCodLaserfiche] [int] NULL,
	[siEstado] [smallint] NULL,
	[iCodDocMovimiento] [int] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](20) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[iCodDocumento] [int] NOT NULL,
	[vNombre] [varchar](50) NOT NULL DEFAULT ((0)),
	[iSecuencia] [int] NOT NULL DEFAULT ((0)),
	[iTipoArchivo] [int] NULL DEFAULT ((1)),
 CONSTRAINT [PK_MOVDOCANEXO] PRIMARY KEY CLUSTERED 
(
	[iCodDocAnexo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocComentario]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MovDocComentario](
	[iCodDocComentario] [int] IDENTITY(1,1) NOT NULL,
	[vComentario] [varchar](1000) NULL,
	[siEstado] [smallint] NOT NULL,
	[iCodDocMovimiento] [int] NULL,
	[iCodDocumento] [int] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[iCodDocumentoDerivacion] [int] NULL,
 CONSTRAINT [PK_MOVDOCCOMENTARIO] PRIMARY KEY CLUSTERED 
(
	[iCodDocComentario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MovDocMovimiento](
	[iCodDocMovimiento] [int] IDENTITY(1,1) NOT NULL,
	[siEstado] [int] NOT NULL,
	[dFecRecepcion] [datetime] NULL,
	[dFecLectura] [datetime] NULL,
	[iCodMovAnterior] [int] NULL,
	[siPlazo] [smallint] NULL,
	[dFecPlazo] [datetime] NULL,
	[siNivel] [smallint] NULL,
	[siEstFirma] [int] NULL,
	[dFecVisado] [datetime] NULL,
	[siEstVisado] [smallint] NULL,
	[dFecFirma] [datetime] NULL,
	[iCodDocumento] [int] NOT NULL,
	[iCodArea] [int] NOT NULL,
	[iCodTrabajador] [int] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[dFecDerivacion] [datetime] NULL,
	[iEstadoDocumento] [int] NULL,
	[iCodDocDerivacion] [int] NULL,
	[iCodAreaDerivacion] [int] NULL,
	[iCodResponsableDerivacion] [int] NULL,
	[siEstadoDerivacion] [int] NULL,
	[iTipoMovimiento] [int] NULL,
	[vCodigosInternos] [VARCHAR](100) NULL
 CONSTRAINT [MOV_DOC_MOVIMIENTO] PRIMARY KEY CLUSTERED 
(
	[iCodDocMovimiento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocumento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MovDocumento](
	[iCodDocumento] [int] IDENTITY(1,1) NOT NULL,
	[iCodSecuencial] [int] NULL,
	[iAnio] [int] NULL,
	[siEstado] [smallint] NOT NULL,
	[siOrigen] [smallint] NULL,
	[vNumDocumento] [varchar](100) NULL,
	[siEstDocumento] [smallint] NULL,
	[dFecDocumento] [datetime] NULL,
	[dFecRegistro] [datetime] NULL,
	[vAsunto] [varchar](1000) NULL,
	[siPrioridad] [smallint] NULL,
	[vReferencia] [varchar](1000) NULL,
	[dFecRecepcion] [datetime] NULL,
	[siPlazo] [smallint] NULL,
	[dFecPlazo] [datetime] NULL,
	[vObservaciones] [varchar](1000) NULL,
	[iCodTipDocumento] [int] NOT NULL,
	[iCodRepresentante] [int]  NULL,
	[iCodArea] [int] NOT NULL,
	[iCodTrabajador] [int] NOT NULL,
	[siFlagFirmando] SMALLINT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[vCorrelativo] [varchar](50) NULL,
	[dFecDerivacion] [datetime] NULL,
 CONSTRAINT [PK_MOVDOCUMENTO] PRIMARY KEY CLUSTERED 
(
	[iCodDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MovDocumentoDerivacion](
	[iCodDocumentoDerivacion] [int] IDENTITY(1,1) NOT NULL,
	[iCodDocumento] [int] NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[iCodArea] [int] NOT NULL,
	[iCodResponsable] [int] NOT NULL,
	[siEstadoDerivacion] [int] NULL,
	[siTipoAcceso] [smallint] NOT NULL,
	[dFecDerivacion] [datetime] NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[iTipoAccion] [smallint] NULL,
 CONSTRAINT [PK_MovDocumentoDerivacion_CodDocumentoDerivacion] PRIMARY KEY CLUSTERED 
(
	[iCodDocumentoDerivacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ParParametro]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ParParametro](
	[iCodGrupo] [int] NOT NULL,
	[iCodParametro] [int] NOT NULL,
	[vValor] [varchar](100) NULL,
	[vCampo] [varchar](100) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
	[vValor2] [varchar](500) NULL,
	[iOrden] [int] NULL,
 CONSTRAINT [PK_PARPARAMETRO] PRIMARY KEY CLUSTERED 
(
	[iCodGrupo] ASC,
	[iCodParametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SegUsuario]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SegUsuario](
	[iCodUsuario] [int] NOT NULL,
	[iCodPerfil] [int] NULL,
	[dFecAcceso] [datetime] NULL,
	[siEstado] [smallint] NULL,
	[iCodTrabajador] [int] NOT NULL,
	[WEBUSR] [varchar](20) NULL,
	[vCodPerfil] [varchar](20) NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](50) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_SEGUSUARIO] PRIMARY KEY CLUSTERED 
(
	[iCodUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAEREPRESENTANTE_MAEEMPRESA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]'))
ALTER TABLE [dbo].[MaeRepresentante]  WITH CHECK ADD  CONSTRAINT [FK_MAEREPRESENTANTE_MAEEMPRESA] FOREIGN KEY([iCodEmpresa])
REFERENCES [dbo].[MaeEmpresa] ([iCodEmpresa])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAEREPRESENTANTE_MAEEMPRESA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]'))
ALTER TABLE [dbo].[MaeRepresentante] CHECK CONSTRAINT [FK_MAEREPRESENTANTE_MAEEMPRESA]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAEREPRESENTANTE_MAETIPDOCIDENTIDAD]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]'))
ALTER TABLE [dbo].[MaeRepresentante]  WITH CHECK ADD  CONSTRAINT [FK_MAEREPRESENTANTE_MAETIPDOCIDENTIDAD] FOREIGN KEY([iCodTipDocIdentidad])
REFERENCES [dbo].[MaeTipDocIdentidad] ([iCodTipDocIdentidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAEREPRESENTANTE_MAETIPDOCIDENTIDAD]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeRepresentante]'))
ALTER TABLE [dbo].[MaeRepresentante] CHECK CONSTRAINT [FK_MAEREPRESENTANTE_MAETIPDOCIDENTIDAD]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAETRABAJADOR_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]'))
ALTER TABLE [dbo].[MaeTrabajador]  WITH CHECK ADD  CONSTRAINT [FK_MAETRABAJADOR_MAEAREA] FOREIGN KEY([iCodArea])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAETRABAJADOR_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]'))
ALTER TABLE [dbo].[MaeTrabajador] CHECK CONSTRAINT [FK_MAETRABAJADOR_MAEAREA]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAETRABAJADOR_MAETIPDOCIDENTIDAD]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]'))
ALTER TABLE [dbo].[MaeTrabajador]  WITH CHECK ADD  CONSTRAINT [FK_MAETRABAJADOR_MAETIPDOCIDENTIDAD] FOREIGN KEY([iCodTipDocIdentidad])
REFERENCES [dbo].[MaeTipDocIdentidad] ([iCodTipDocIdentidad])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MAETRABAJADOR_MAETIPDOCIDENTIDAD]') AND parent_object_id = OBJECT_ID(N'[dbo].[MaeTrabajador]'))
ALTER TABLE [dbo].[MaeTrabajador] CHECK CONSTRAINT [FK_MAETRABAJADOR_MAETIPDOCIDENTIDAD]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCADJUNTO_MOVDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocAdjunto]'))
ALTER TABLE [dbo].[MovDocAdjunto]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCADJUNTO_MOVDOCUMENTO] FOREIGN KEY([iCodDocumento])
REFERENCES [dbo].[MovDocumento] ([iCodDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCADJUNTO_MOVDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocAdjunto]'))
ALTER TABLE [dbo].[MovDocAdjunto] CHECK CONSTRAINT [FK_MOVDOCADJUNTO_MOVDOCUMENTO]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCANEXO_MOVDOCMOVIMIENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocAnexo]'))
ALTER TABLE [dbo].[MovDocAnexo]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCANEXO_MOVDOCMOVIMIENTO] FOREIGN KEY([iCodDocMovimiento])
REFERENCES [dbo].[MovDocMovimiento] ([iCodDocMovimiento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCANEXO_MOVDOCMOVIMIENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocAnexo]'))
ALTER TABLE [dbo].[MovDocAnexo] CHECK CONSTRAINT [FK_MOVDOCANEXO_MOVDOCMOVIMIENTO]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__MovDocCom__iCodD__01D345B0]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocComentario]'))
ALTER TABLE [dbo].[MovDocComentario]  WITH CHECK ADD FOREIGN KEY([iCodDocumento])
REFERENCES [dbo].[MovDocumento] ([iCodDocumento])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCMOVIMIENTO_MAEAREA] FOREIGN KEY([iCodArea])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento] CHECK CONSTRAINT [FK_MOVDOCMOVIMIENTO_MAEAREA]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCMOVIMIENTO_MAETRABAJADOR] FOREIGN KEY([iCodTrabajador])
REFERENCES [dbo].[MaeTrabajador] ([iCodTrabajador])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento] CHECK CONSTRAINT [FK_MOVDOCMOVIMIENTO_MAETRABAJADOR]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MOVDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCMOVIMIENTO_MOVDOCUMENTO] FOREIGN KEY([iCodDocumento])
REFERENCES [dbo].[MovDocumento] ([iCodDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCMOVIMIENTO_MOVDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocMovimiento]'))
ALTER TABLE [dbo].[MovDocMovimiento] CHECK CONSTRAINT [FK_MOVDOCMOVIMIENTO_MOVDOCUMENTO]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCUMENTO_MAEAREA] FOREIGN KEY([iCodArea])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAEAREA]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] CHECK CONSTRAINT [FK_MOVDOCUMENTO_MAEAREA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAEREPRESENTANTE]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] CHECK CONSTRAINT [FK_MOVDOCUMENTO_MAEREPRESENTANTE]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAETIPDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCUMENTO_MAETIPDOCUMENTO] FOREIGN KEY([iCodTipDocumento])
REFERENCES [dbo].[MaeTipDocumento] ([iCodTipDocumento])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAETIPDOCUMENTO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] CHECK CONSTRAINT [FK_MOVDOCUMENTO_MAETIPDOCUMENTO]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento]  WITH CHECK ADD  CONSTRAINT [FK_MOVDOCUMENTO_MAETRABAJADOR] FOREIGN KEY([iCodTrabajador])
REFERENCES [dbo].[MaeTrabajador] ([iCodTrabajador])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MOVDOCUMENTO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumento]'))
ALTER TABLE [dbo].[MovDocumento] CHECK CONSTRAINT [FK_MOVDOCUMENTO_MAETRABAJADOR]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MovDocumentoDerivacion_MaeArea]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]'))
ALTER TABLE [dbo].[MovDocumentoDerivacion]  WITH CHECK ADD  CONSTRAINT [FK_MovDocumentoDerivacion_MaeArea] FOREIGN KEY([iCodArea])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MovDocumentoDerivacion_MaeArea]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]'))
ALTER TABLE [dbo].[MovDocumentoDerivacion] CHECK CONSTRAINT [FK_MovDocumentoDerivacion_MaeArea]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MovDocumentoDerivacion_MaeTrabajador]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]'))
ALTER TABLE [dbo].[MovDocumentoDerivacion]  WITH CHECK ADD  CONSTRAINT [FK_MovDocumentoDerivacion_MaeTrabajador] FOREIGN KEY([iCodResponsable])
REFERENCES [dbo].[MaeTrabajador] ([iCodTrabajador])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MovDocumentoDerivacion_MaeTrabajador]') AND parent_object_id = OBJECT_ID(N'[dbo].[MovDocumentoDerivacion]'))
ALTER TABLE [dbo].[MovDocumentoDerivacion] CHECK CONSTRAINT [FK_MovDocumentoDerivacion_MaeTrabajador]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SEGUSUARIO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[SegUsuario]'))
ALTER TABLE [dbo].[SegUsuario]  WITH CHECK ADD  CONSTRAINT [FK_SEGUSUARIO_MAETRABAJADOR] FOREIGN KEY([iCodTrabajador])
REFERENCES [dbo].[MaeTrabajador] ([iCodTrabajador])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SEGUSUARIO_MAETRABAJADOR]') AND parent_object_id = OBJECT_ID(N'[dbo].[SegUsuario]'))
ALTER TABLE [dbo].[SegUsuario] CHECK CONSTRAINT [FK_SEGUSUARIO_MAETRABAJADOR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MovDocumentoFirma](
	[iCodDocumentoFirma] [int] IDENTITY(1,1) NOT NULL,
	[iCodDocumento] [int] NOT NULL,
	[iCodAdjunto] [int] NOT NULL,
	[iCodLaserFiche] [int] NOT NULL,
	[iCodDerivacion] [int] NULL,
	[iCodTrabajador] [int] NOT NULL,
	[iCodArea] [int] NOT NULL,
	[dFechaFirma] [datetime] NOT NULL,
	[vComentario] [varchar](1000) NULL,
	[siEstado] [smallint] NOT NULL,
	[iPosx] [int] NULL,
	[iPosy] [int] NULL,
	[vUsuCreacion] [varchar](100) NULL,
	[dFecCreacion] [datetime] NULL,
	[vHstCreacion] [varchar](50) NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](50) NULL,
	[vRolCreacion] [varchar](50) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](50) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](50) NULL,
	[vRolActualizacion] [varchar](50) NULL,
 CONSTRAINT [PK_MovDocumentoFirma] PRIMARY KEY CLUSTERED 
(
	[iCodDocumentoFirma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MovDocumentoFirma]  WITH CHECK ADD  CONSTRAINT [FK_MovDocumentoFirma_Adjunto] FOREIGN KEY([iCodAdjunto])
REFERENCES [dbo].[MovDocAdjunto] ([iCodDocAdjunto])
GO

ALTER TABLE [dbo].[MovDocumentoFirma] CHECK CONSTRAINT [FK_MovDocumentoFirma_Adjunto]
GO

ALTER TABLE [dbo].[MovDocumentoFirma]  WITH CHECK ADD  CONSTRAINT [FK_MovDocumentoFirma_Area] FOREIGN KEY([iCodArea])
REFERENCES [dbo].[MaeArea] ([iCodArea])
GO

ALTER TABLE [dbo].[MovDocumentoFirma] CHECK CONSTRAINT [FK_MovDocumentoFirma_Area]
GO

ALTER TABLE [dbo].[MovDocumentoFirma]  WITH CHECK ADD  CONSTRAINT [FK_MovDocumentoFirma_Documento] FOREIGN KEY([iCodDocumento])
REFERENCES [dbo].[MovDocumento] ([iCodDocumento])
GO

ALTER TABLE [dbo].[MovDocumentoFirma] CHECK CONSTRAINT [FK_MovDocumentoFirma_Documento]
GO

ALTER TABLE [dbo].[MovDocumentoFirma]  WITH CHECK ADD  CONSTRAINT [FK_MovDocumentoFirma_Trabajador] FOREIGN KEY([iCodTrabajador])
REFERENCES [dbo].[MaeTrabajador] ([iCodTrabajador])
GO

ALTER TABLE [dbo].[MovDocumentoFirma] CHECK CONSTRAINT [FK_MovDocumentoFirma_Trabajador]
GO

