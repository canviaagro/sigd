USE [DBAgrSIGD]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_SegUsuario_qry01]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_SegUsuario_qry01]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_PAR_Listar_Parametros_x_Grupo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_PAR_Listar_Parametros_x_Grupo]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_Obtener_Usuario_qry01]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_Obtener_Usuario_qry01]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_Obtener_Usuario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_Obtener_Usuario]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ValidarFinalizarDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ValidarFinalizarDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumentosDerivados]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocumentosDerivados]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumento_qry01]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocumento_qry01]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocMovimientoDerivacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimientoDerivacion]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocMovimiento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocComentario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocComentario]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocAnexo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocAnexo]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocAdjunto]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocAdjunto]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerUltimoMovimiento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerUltimoMovimiento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorTrabajador]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorTrabajador]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivados]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosAnexos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAnexos]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosAdjuntos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAdjuntos]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCorrelativoAnxInicial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerCorrelativoAnxInicial]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_SeguimientoDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_Listar_SeguimientoDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_Documento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_Listar_Documento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaEntrada]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_InactivarDocumentosDerivados]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_InactivarDocumentosDerivados]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_FinalizarDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_FinalizarDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EliminarDerivacionCC]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_EliminarDerivacionCC]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EliminarAnexo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_EliminarAnexo]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DevolverDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_DevolverDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DerivarDocumentoV2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_DerivarDocumentoV2]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DerivarDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_DerivarDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_CambiarEstadoDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_CambiarEstadoDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_AnularDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_AnularDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ActualizarDocumento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarDocAdjunto]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ActualizarDocAdjunto]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_MaeTrabajador_qry01]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[up_MaeTrabajador_qry01]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Trabajador]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Trabajador]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarEmpresa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_RegistrarEmpresa]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarTrabajadorPorArea]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_ListarTrabajadorPorArea]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Tipo_Documento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_Tipo_Documento]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Representante_x_Emp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_Representante_x_Emp]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Empresa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_Empresa]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Asunto]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_Asunto]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Area]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_Area]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Generar_Secuencial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Generar_Secuencial]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_CalcularFechaPlazoDocumento]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_CalcularFechaPlazoDocumento]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UF_MOV_OBTENERNOMJEFE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UF_MOV_OBTENERNOMJEFE]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UF_MOV_OBTENERCORRELATIVO]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[UF_MOV_OBTENERCORRELATIVO]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Split]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Split]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Split]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[Split](@String varchar(150), @Delimiter char(1))
RETURNS @Results table (word varchar(50))
AS
BEGIN
--select * from dbo.Split(''danielsa asds asdsd asdsa d|vargas|yalico|451420751|carlos|cabos'',''|'')a
DECLARE @INDEX INT
DECLARE @SLICE varchar(200)
-- Asignar 1 a la variable que utilizaremos en el loop para no iniciar en 0.
SELECT @INDEX = 1

WHILE @INDEX !=0
BEGIN
-- Obtenemos el índice de la primera ocurrencia del split de caracteres.
SELECT @INDEX = CHARINDEX(@Delimiter,@STRING)
-- Ahora ponemos todo a la izquierda de el slice de la variable.
IF @INDEX != 0
SELECT @SLICE = LEFT(@STRING,@INDEX - 1)
ELSE
SELECT @SLICE = @STRING

insert into @Results(word) values(@SLICE)
SELECT @STRING = RIGHT(@STRING,LEN(@STRING) - @INDEX)

-- Salimos del loop si terminamos la búsqueda
IF LEN(@STRING) = 0 BREAK
END

RETURN

END

 ' 
END

GO
CREATE FUNCTION [dbo].[UF_MOV_OBTENERCORRELATIVO]
(
	@origen int,
	@iCodArea int,
	@iCodTipDocumento int,
	@Anio int,
	@Tipo int
)
RETURNS varchar(100)
AS
BEGIN
	declare @salida varchar(100)='';
	
	declare @CORRELATIVO varchar(50), 
			@TIPODOC VARCHAR(5), 
			@AREA VARCHAR(10),
			@NRO_TIPODOC_AREA_ANIO_CORR VARCHAR(100),
			@CORRELATIVOFINAL varchar(50);

	select @TIPODOC=vAbreviatura from MaeTipDocumento where iCodTipDocumento=@iCodTipDocumento;
	select @AREA=vAbreviatura from MaeArea where iCodArea=@iCodArea;
	select @CORRELATIVO=max(iCorrelativo)
	from MaeSecuencial a
	where a.iAnio=@Anio and a.siOrigen=@origen and siestado=1

	set @CORRELATIVO=isnull(cast(@CORRELATIVO as int),0)+1;
	
	select @NRO_TIPODOC_AREA_ANIO_CORR=max(iCorrelativo) from MaeSecuencial  a
	where a.iAnio=@Anio and a.siOrigen=@origen
	AND a.iCodTipDocumento=@iCodTipDocumento
	AND a.iCodArea=@iCodArea 
	and siestado=1

	set @NRO_TIPODOC_AREA_ANIO_CORR=isnull(cast(@NRO_TIPODOC_AREA_ANIO_CORR as int),0)+1;
	
	if(@origen=1)--interno--
	begin
			select @salida= vValor from ParParametro where iCodParametro=2 and icodgrupo=4 and siestado=1
			set @CORRELATIVOFINAL=@NRO_TIPODOC_AREA_ANIO_CORR
	end
	else 
	begin
		if(@origen=2)--externo--mesa de partes
		begin
			select @salida= vValor from ParParametro where iCodParametro=1 and icodgrupo=4 and siestado=1
			set @CORRELATIVOFINAL=@CORRELATIVO
		end
	end
	if(@Tipo=1)
	begin
		return @CORRELATIVOFINAL;
	end
	
	set @salida= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@salida,'[CORRELATIVO]',right('000000000'+@CORRELATIVO,4))
										,'[ANIO]',cast(@Anio as varchar(5)))
										,'[AREA]',isnull(@AREA,''))
										,'[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]',right('000000000'+isnull(@NRO_TIPODOC_AREA_ANIO_CORR,''),4))
										,'[TIPODOC]',isnull(@TIPODOC,''))

	RETURN @salida;
END
GO

IF OBJECT_ID('[dbo].[UP_MOV_Listar_Comentarios]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_Comentarios]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_Comentarios]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_Comentarios]
-- Objetivo:	Lista los comentarios del documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

SELECT
	COM.[iCodDocComentario],
	COM.[vComentario],
	COM.[siEstado],
	COM.[iCodDocMovimiento],
	COM.[iCodDocumento],
	COM.[vUsuCreacion],
	COM.[dFecCreacion],
	COM.[vHstCreacion],
	COM.[vInsCreacion],
	COM.[vLgnCreacion],
	COM.[vRolCreacion],
	COM.[vUsuActualizacion],
	COM.[dFecActualizacion],
	COM.[vHstActualizacion],
	COM.[vInsActualizacion],
	COM.[vLgnActualizacion],
	COM.[vRolActualizacion],
	COM.[iCodDocumentoDerivacion],
	TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres AS COMENTADOPOR,
	DOC.iEstadoDocumento,
	DOC.siEstadoDerivacion
	FROM MovDocComentario COM
	INNER JOIN  MovDocMovimiento DOC ON DOC.iCodDocMovimiento = COM.iCodDocMovimiento 
	INNER JOIN MovDocumento DOCU ON COM.iCodDocumento = DOCU.iCodDocumento
	INNER  JOIN SegUsuario SEG ON SEG.WEBUSR = DOC.vUsuCreacion
	INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
	WHERE COM.iCodDocumento =@iCodDocumento


 END



GO
GO

IF OBJECT_ID('[dbo].[UP_MOV_ActualizarMovimientosInternos]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_ActualizarMovimientosInternos]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_ActualizarMovimientosInternos]
   @iCodDocMovimiento INT
  ,@vCodigosInternos VARCHAR(1000)
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_ActualizarTipoDocumento]
-- Objetivo:	Actualizar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@iCodTipoDocumento - Codigo Tipo Documento
--				@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuActualizacion - Campo Auditoria Usuario Creación
--				@vHstActualizacion - Campo Auditoría Host
--				@vInsActualizacion - Campo Auditoría Instancia
--				@vLgnActualizacion - Campo Auditoría Login
--				@vRolActualizacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN


    UPDATE MovDocMovimiento set vCodigosInternos = @vCodigosInternos
	WHERE iCodDocMovimiento = @iCodDocMovimiento

  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento 

 END

GO


IF OBJECT_ID('[dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_CabeceraSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

WITH CABECERA AS (
select 
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	MAEA.vDescripcion AS AREADESDE,
	MOV.vCodigosInternos AS AREAPARA,
	MOV.iEstadoDocumento,
	(select max(dfecCreacion) from  MovDocMovimiento where  iCodDocumento= @iCodDocumento  and iEstadoDocumento =  Mov.iEstadoDocumento )  AS FECHA,
	cast(MOV.iCodDocDerivacion as varchar)iCodDocDerivacion
	FROM MovDocMovimiento MOV 
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario seg ON DOC.vUsuCreacion = SEG.WEBUSR 
	INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
	INNER JOIN MaeArea MAEA ON DOC.iCodArea = MAEA.iCodArea
	INNER JOIN MaeTrabajador TRAPARA ON MOV.iCodTrabajador = TRAPARA.iCodTrabajador
	WHERE MOV.iCodDocumento = @iCodDocumento AND MOV.vCodigosInternos  IS NOT NULL
--	ORDER BY MOV.iCodDocDerivacion DESC OFFSET 0 ROWS
), 
DERIVACIONCODIGOS AS (
SELECT AREAPARA,iEstadoDocumento, icodDocderivacion = STUFF((
    SELECT N' |' + icodDocderivacion FROM CABECERA
    WHERE AREAPARA = x.AREAPARA AND
		   iEstadoDocumento = x.iEstadoDocumento-- AND iEstadoDocumento <>1
    FOR XML PATH(''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 2, N'')
FROM CABECERA AS x
GROUP BY AREAPARA,iEstadoDocumento
--ORDER BY icodDocderivacion DESC OFFSET 0 ROWS
),  CABFIN AS  (
SELECT  DISTINCT
CAB.NOMBREDESDE,
CAB.AREADESDE,
CAB.AREAPARA,
CAB.iEstadoDocumento,
MOV.dFecCreacion FECHA,
CAB.icodDocderivacion AS iCodDerivacionesInternas,
0 as iCodDocumento
FROM  CABECERA CAB 
INNER JOIN MovDocMovimiento MOV ON CAB.iCodDocDerivacion = MOV.iCodDocDerivacion
WHERE  cab.iEstadoDocumento = CASE WHEN  CHARINDEX('|',cab.AREAPARA) > 0 THEN  '' ELSE 1 END 
UNION  ALL
SELECT  DISTINCT
CAB.NOMBREDESDE,
CAB.AREADESDE,
CAB.AREAPARA,
CAB.iEstadoDocumento,
CAB.FECHA,
DER.icodDocderivacion AS iCodDerivacionesInternas,
0 as iCodDocumento
FROM  CABECERA CAB 
INNER  JOIN DERIVACIONCODIGOS DER
ON CAB.AREAPARA = DER.AREAPARA AND CAB.iEstadoDocumento =  DER.iEstadoDocumento
WHERE  cab.iEstadoDocumento <> CASE WHEN  CHARINDEX('|',cab.AREAPARA) > 0 THEN  '' ELSE 1 END 
AND CAB.iEstadoDocumento = DER.iEstadoDocumento
--ORDER BY DER.icodDocderivacion DESC

UNION ALL

SELECT DISTINCT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	MAEA.vDescripcion AS AREADESDE,
	(select mm.vCodigosInternos from MovDocMovimiento mm where iCodDocumento =  @iCodDocumento 
	and  iCodDocMovimiento = (select max(iCodDocMovimiento) from MovDocMovimiento where iCodDocumento =  @iCodDocumento
	and vCodigosInternos is not null))  AS AREAPARA,
	MOV.iEstadoDocumento,
	(select max(dfecCreacion) from  MovDocMovimiento where  iCodDocumento= @iCodDocumento  and iEstadoDocumento =  Mov.iEstadoDocumento )  AS FECHA,
	null iCodDerivacionesInternas,
	doc.iCodDocumento
	FROM MovDocMovimiento MOV 
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario seg ON DOC.vUsuCreacion = SEG.WEBUSR 
	INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
	INNER JOIN MaeArea MAEA ON DOC.iCodArea = MAEA.iCodArea
	INNER JOIN MaeTrabajador TRAPARA ON MOV.iCodTrabajador = TRAPARA.iCodTrabajador
	WHERE MOV.iCodDocumento = @iCodDocumento and mov.iEstadoDocumento IN(6,5,3,7) AND Mov.iTipoMovimiento IN (0,2,1,4,3,6,5,8)
	)SELECT * FROM CABFIN  ORDER  BY case when iCodDerivacionesInternas is null then 'zzzzzz' else iCodDerivacionesInternas end
END


GO


IF OBJECT_ID('[dbo].[UP_MOV_Listar_DetalleSeguimiento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_DetalleSeguimiento]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_DetalleSeguimiento]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_DetalleSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

SELECT 
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	MOV.siEstadoDerivacion iEstadoDocumento,
	MOV.iEstadoDocumento EstadoDocumento,
	DER.siEstado,
	MOV.iCodDocMovimiento,
	MOV.dFecCreacion AS FECHA,
	MOV.iCodDocDerivacion
	FROM MovDocumentoDerivacion DER
	INNER JOIN MovDocMovimiento MOV ON DER.iCodDocumentoDerivacion = MOV.iCodDocDerivacion
	INNER JOIN SegUsuario USU on DER.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON DER.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  DER.iCodResponsable = TRAPARA.iCodTrabajador 
	WHERE DER.iCodDocumento =@iCodDocumento

 END


GO

IF OBJECT_ID('[dbo].[UP_MOV_Listar_CabeceraSeguimiento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimiento]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimiento]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_CabeceraSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

WITH SEGUIMIENTO AS (
	
	SELECT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	mov.dFecCreacion AS FECHA,
	MOV.iEstadoDocumento,
	MOV.iCodDocMovimiento,
	CASE WHEN MOV.iEstadoDocumento  IN (5,7) THEN doc.iCodDocumento ELSE 0  END iCodDocAuxiliar
	FROM MovDocMovimiento MOV
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario USU on DOC.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON MOV.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  MOV.iCodTrabajador = TRAPARA.iCodTrabajador 
	WHERE MOV.iCodDocumento = @iCodDocumento AND iEstadoDocumento <> 6 AND Mov.iCodDocDerivacion is null 

	UNION ALL

	SELECT DISTINCT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	ISNULL(DOC.dFecActualizacion,DOC.dFecCreacion) AS FECHA,
	MOV.iEstadoDocumento,
	0 iCodDocMovimiento,
	MOV.iCodDocumento iCodDocAuxiliar
	FROM MovDocumento DOC 
	INNER JOIN MovDocMovimiento MOV ON MOV.iCodDocumento = DOC.iCodDocumento AND MOV.iEstadoDocumento = 6
	INNER JOIN SegUsuario USU on DOC.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON DOC.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  DOC.iCodTrabajador = TRAPARA.iCodTrabajador 
	WHERE MOV.iCodDocumento = @iCodDocumento AND MOV.iEstadoDocumento = 6 AND Mov.iTipoMovimiento IN (0,2,1,3))
	SELECT * FROM SEGUIMIENTO ORDER BY FECHA ASC
 END

GO
GO


IF OBJECT_ID('[dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]') IS NOT NULL
BEGIN
	DROP PROCEDURE [UP_MOV_ObtenerDetalleCorreoDerivacion]
END
GO
CREATE  PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT	  a.iCodDocumento,
			a.siPlazo,
			a.dFecPlazo,
			a.siPrioridad,
			a.siOrigen,
			CASE WHEN A.siOrigen = 2  THEN R.vNombres
				 WHEN A.siOrigen = 1 THEN (SELECT  TJEFE.vApePaterno + ' '+ TJEFE.vApeMaterno + ' '  + TJEFE.vNombres
											FROM MaeTrabajador TJEFE WHERE iCodArea = ARE.iCodArea AND siEsJefe = 1)   END AS REPRESENTANTE,
			CASE WHEN A.siOrigen = 2  THEN  E.vDescripcion
				 WHEN A.siOrigen = 1 THEN ARE.vDescripcion END AS EMPRESA,
			T.vApePaterno + ' '+ T.vApeMaterno + ' '  + t.vNombres AS DERIVADOPOR,
			par.vCampo TipoAccion
		FROM [dbo].[MovDocumento] a
		INNER JOIN MovDocumentoDerivacion DER ON A.iCodDocumento =  DER.iCodDocumento AND DER.siTipoAcceso = 1 --AND ISNULL(A.vUsuActualizacion,A.vUsuCreacion) = ISNULL(DER.vUsuActualizacion,DER.vUsuCreacion)
		LEFT JOIN MaeRepresentante r on a.iCodRepresentante = r.iCodRepresentante
		INNER JOIN SegUsuario S ON S.WEBUSR = DER.vUsuCreacion
		INNER JOIN MaeTrabajador T ON T.iCodTrabajador = S.iCodTrabajador
		LEFT JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		LEFT JOIN MaeArea ARE ON T.iCodArea = ARE.iCodArea
		LEFT JOIN (SELECT * FROM ParParametro where iCodGrupo=8) PAR ON PAR.iCodParametro = der.iTipoAccion
		where a.iCodDocumento=@iCodDocumento
		ORDER BY  DER.dFecCreacion DESC
END
GO


IF OBJECT_ID('[dbo].[UP_MOV_ObtenerDetalleCorreoFinalizar]') IS NOT NULL
BEGIN
	DROP PROCEDURE [UP_MOV_ObtenerDetalleCorreoFinalizar]
END
GO
CREATE  PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoFinalizar]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
			SELECT	  a.iCodDocumento,
			a.vCorrelativo,
			a.vNumDocumento,
			a.dFecPlazo,
			a.siPrioridad,
			a.siOrigen,
			CASE WHEN A.siOrigen = 2  THEN R.vNombres
				 WHEN A.siOrigen = 1 THEN (SELECT  TJEFE.vApePaterno + ' '+ TJEFE.vApeMaterno + ' '  + TJEFE.vNombres
											FROM MaeTrabajador TJEFE WHERE iCodArea = ARE.iCodArea AND siEsJefe = 1)   END AS REPRESENTANTE,
			CASE WHEN A.siOrigen = 2  THEN  E.vDescripcion
				 WHEN A.siOrigen = 1 THEN ARE.vDescripcion END AS EMPRESA,
			ARE.vDescripcion AS AREA
		FROM [dbo].[MovDocumento] a
		LEFT JOIN MaeRepresentante r on a.iCodRepresentante = r.iCodRepresentante
		INNER JOIN SegUsuario S ON S.WEBUSR = a.vUsuCreacion
		INNER JOIN MaeTrabajador T ON T.iCodTrabajador = S.iCodTrabajador
		LEFT JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		LEFT JOIN MaeArea ARE ON T.iCodArea = ARE.iCodArea
		where a.iCodDocumento=@iCodDocumento
END
GO


IF OBJECT_ID('[dbo].[UP_MAE_Listar_Email_PorTrabajador]') IS NOT NULL
BEGIN
	DROP PROCEDURE [UP_MAE_Listar_Email_PorTrabajador]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_Listar_Email_PorTrabajador]
@CodTrabajador AS INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Empresa]
-- Objetivo:	Listar Empresas
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Empresas	
-- Create date: 04/02/20
-- =============================================
BEGIN
	
	SELECT 
	vEmail FROM MaeTrabajador WHERE iCodTrabajador = @CodTrabajador
	and siEstado = 1
 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_ActualizarSecuencia]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_ActualizarSecuencia]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_ActualizarSecuencia]
 @iCodSecuencial INT,
 @iOrigen INT,
 @anio INT,
 @iCodArea INT = NULL,
 @iCodTipoDocumento INT = NULL,
 @correlativo INT,
 @ValorCorrelativo VARCHAR(20) 
 ,@Estado INT
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_ActualizarTipoDocumento]
-- Objetivo:	Actualizar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@iCodTipoDocumento - Codigo Tipo Documento
--				@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuActualizacion - Campo Auditoria Usuario Creación
--				@vHstActualizacion - Campo Auditoría Host
--				@vInsActualizacion - Campo Auditoría Instancia
--				@vLgnActualizacion - Campo Auditoría Login
--				@vRolActualizacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
	UPDATE MaeSecuencial set siOrigen = @iOrigen,
							 iCodArea = @iCodArea,
							 iCodTipDocumento = @iCodTipoDocumento,
							 iCorrelativo = @correlativo,
							 vCorrelativo = @ValorCorrelativo,
							 siEstado  =  @Estado,
							 vUsuActualizacion = @vUsuActualizacion,
							 dFecActualizacion = GETDATE(),
							 vHstActualizacion = @@SERVERNAME,
							 vInsCreacion = @@SERVICENAME,
							 vLgnActualizacion = @vLgnActualizacion,
							 vRolActualizacion = @vRolActualizacion
							 where iCodSecuencial = @iCodSecuencial;


     SELECT @onFlagOK=MAX(iCodSecuencial) FROM dbo.MaeSecuencial 

 END
GO

IF OBJECT_ID('[dbo].[UP_MOV_OBTENERCORRELATIVO]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_OBTENERCORRELATIVO]
END
GO
CREATE PROCEDURE [dbo].[UP_MOV_OBTENERCORRELATIVO]
(
	@origen int,
	@iCodArea int = NULL,
	@iCodTipDocumento int = NULL,
	@Anio int,
	@SecCorrelativo int,
	@Tipo int,
	@ValorCorrelativo VARCHAR(100) OUTPUT
)
--RETURNS varchar(100)
AS
BEGIN
	declare @salida varchar(100)='';
	
	declare @CORRELATIVO varchar(50), 
			@TIPODOC VARCHAR(5), 
			@AREA VARCHAR(10),
			@NRO_TIPODOC_AREA_ANIO_CORR VARCHAR(100),
			@CORRELATIVOFINAL varchar(50);

	select @TIPODOC=vAbreviatura from MaeTipDocumento where iCodTipDocumento=@iCodTipDocumento;
	select @AREA=vAbreviatura from MaeArea where iCodArea=@iCodArea;
	select @CORRELATIVO=@SecCorrelativo

	--set @CORRELATIVO=isnull(cast(@CORRELATIVO as int),0)+1;
	
	select @NRO_TIPODOC_AREA_ANIO_CORR=@SecCorrelativo

	--set @NRO_TIPODOC_AREA_ANIO_CORR=isnull(cast(@NRO_TIPODOC_AREA_ANIO_CORR as int),0)+1;
	
	if(@origen=1)--interno--
	begin
			select @salida= vValor from ParParametro where iCodParametro=2 and icodgrupo=4 and siestado=1
			set @CORRELATIVOFINAL=@NRO_TIPODOC_AREA_ANIO_CORR
	end
	else 
	begin
		if(@origen=2)--externo--mesa de partes
		begin
			select @salida= vValor from ParParametro where iCodParametro=1 and icodgrupo=4 and siestado=1
			set @CORRELATIVOFINAL=@CORRELATIVO
		end
	end
	if(@Tipo=1)
	begin
		return @CORRELATIVOFINAL;
	end
	
	set @salida= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@salida,'[CORRELATIVO]',right('000000000'+@CORRELATIVO,4))
										,'[ANIO]',cast(@Anio as varchar(5)))
										,'[AREA]',isnull(@AREA,''))
										,'[NRO_TIPODOC_AREA_ANIO_CORRELATIVO]',right('000000000'+isnull(@NRO_TIPODOC_AREA_ANIO_CORR,''),4))
										,'[TIPODOC]',isnull(@TIPODOC,''))

	SELECT @ValorCorrelativo= @salida;
END


GO

IF OBJECT_ID('[dbo].[UP_MAE_EliminarTipoDocumento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_EliminarTipoDocumento]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_EliminarTipoDocumento]
 @iCodTipoDocumento int
, @vUsuActualizacion varchar(50)=NULL 
,@vLgnActualizacion varchar(50)=NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_EliminarTipoDocumento]
-- Objetivo:	Actualizar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@iCodTipoDocumento - Codigo Tipo Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

        UPDATE MaeTipDocumento set  siEstado = 0,
	vUsuActualizacion = @vUsuActualizacion,
	vLgnActualizacion = @vLgnActualizacion,
		dFecActualizacion = GETDATE(),
	vInsActualizacion =  @@SERVICENAME,
	vHstActualizacion = @@SERVERNAME
    where iCodTipDocumento = @iCodTipoDocumento

  
		set @onFlagOK=@iCodTipoDocumento

 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_RegistrarSecuencia]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_RegistrarSecuencia]
END
GO
create PROCEDURE [dbo].[UP_MAE_RegistrarSecuencia]
 @Anio INT,
 @Correlativo INT,
 @vCorrelativo VARCHAR(200),
 @iCodArea INT =  null,
 @iCodTipoDocumento INT =  null,
 @iEstado INT,
 @iOrigen INT,
  @vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOk VARCHAR(100) OUTPUT

AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_RegistrarSecuencia]
-- Objetivo:	Listar Tipo Documento
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de de Tipos de Documento Activos
-- Create date: 03/02/20
-- =============================================
DECLARE @IdSecuencialMax AS INT
BEGIN

	SELECT @IdSecuencialMax= ISNULL((MAX(iCodSecuencial)+1),1) FROM dbo.MaeSecuencial

	INSERT INTO dbo.MaeSecuencial(
	iAnio,
	siEstado,
	iCodSecuencial,
	iCorrelativo,
	vCorrelativo,
	iCodArea,
	iCodTipDocumento,
	vUsuCreacion,
	dFecCreacion,
	vHstCreacion,
	vInsCreacion,
	vLgnCreacion,
	vRolCreacion,
	siOrigen,
	fUsado
	)
	VALUES(
	@Anio,
	@iEstado,
	@IdSecuencialMax,
	@Correlativo,
	@vCorrelativo,
	@iCodArea,
	@iCodTipoDocumento,
	@vUsuCreacion,
	GETDATE(),
	@@SERVERNAME,
	@@servicename,
	@vLgnCreacion,
	@vRolCreacion,
	@iOrigen,
	0
	)
 
	SELECT @onFlagOk =  @IdSecuencialMax;

 END

GO

IF OBJECT_ID('[dbo].[UP_MAE_EliminarSecuencia]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_EliminarSecuencia]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_EliminarSecuencia]
 @iCodSecuencial int,
 @vUsuActualizacion varchar(50)=NULL ,
 @vLgnActualizacion varchar(50)=NULL ,
 @onFlagOK int OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_ActualizarTipoDocumento]
-- Objetivo:	Actualizar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@iCodTipoDocumento - Codigo Tipo Documento
--				@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuActualizacion - Campo Auditoria Usuario Creación
--				@vHstActualizacion - Campo Auditoría Host
--				@vInsActualizacion - Campo Auditoría Instancia
--				@vLgnActualizacion - Campo Auditoría Login
--				@vRolActualizacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
		UPDATE MaeSecuencial set siEstado = 0,
						     vUsuActualizacion = @vUsuActualizacion,
							 vLgnActualizacion = @vLgnActualizacion,
							 dFecActualizacion = GETDATE(),
							 vInsActualizacion =  @@SERVICENAME,
							 vHstActualizacion = @@SERVERNAME
						 where iCodSecuencial = @iCodSecuencial;

     SELECT @onFlagOK=MAX(iCodSecuencial) FROM dbo.MaeSecuencial 

 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_ObtenerSecuencia]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_ObtenerSecuencia]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_ObtenerSecuencia] 
 @iCodSecuencia int
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_SecuencialPaginado]
-- Objetivo:	Listar Tipo Documento
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de de Tipos de Documento Activos
-- Create date: 03/02/20
-- =============================================
BEGIN
		SELECT 
		ms.iAnio,
		ms.siEstado,
		ms.iCorrelativo,
		ms.iCodSecuencial,
		ms.iCodArea,
		ms.iCodTipDocumento,
		ms.vUsuCreacion,
		ms.dFecCreacion,
		ms.vCorrelativo,
		ms.siOrigen,
		ms.fUsado,
		ma.vDescripcion vDescripcionArea,
		mt.vDescripcion vDescripcionTipoDocumento
		FROM  MaeSecuencial ms
		left join MaeArea ma on ms.iCodArea  = ma.iCodArea
		left join MaeTipDocumento mt on ms.iCodTipDocumento = mt.iCodTipDocumento
		where iCodSecuencial = @iCodSecuencia
 
 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_Listar_SecuencialPaginado]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_Listar_SecuencialPaginado]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_Listar_SecuencialPaginado] 
 @NumeroPagina int,
 @tamaniopagina int,
 @texto VARCHAR(20)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_SecuencialPaginado]
-- Objetivo:	Listar Tipo Documento
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de de Tipos de Documento Activos
-- Create date: 03/02/20
-- =============================================
BEGIN
	WITH SECUENCIAL AS (SELECT 
		ms.iAnio,ms.siEstado,ms.iCorrelativo,ms.iCodSecuencial,ma.vDescripcion as vDescripcionArea,
		mt.vDescripcion as vDescripcionTipoDocumento,ms.vUsuCreacion,ms.dFecCreacion,ms.vCorrelativo,ms.siOrigen,ms.fUsado,
		ma.iCodArea,mt.iCodTipDocumento
		FROM  MaeSecuencial ms
		left join MaeArea ma on ms.iCodArea = ma.iCodArea
		left join MaeTipDocumento mt on ms.iCodTipDocumento = mt.iCodTipDocumento
		INNER JOIN ParParametro P ON P.iCodGrupo = 2 AND  P.iCodParametro = MS.siOrigen
	WHERE concat(ma.vDescripcion,' ',mt.vDescripcion, ' ', ms.vCorrelativo,' ', P.vCampo) like '%'+ UPPER(@texto) +'%'
	)

	SELECT iAnio,siEstado,iCorrelativo,iCodSecuencial,vDescripcionArea,vDescripcionTipoDocumento,vUsuCreacion,
	dFecCreacion,vCorrelativo,siOrigen,fUsado,iCodArea,iCodTipDocumento,
		(SELECT COUNT(1) FROM SECUENCIAL) TotalRegistros FROM SECUENCIAL
	order by siOrigen desc,iAnio desc,vCorrelativo desc,vDescripcionTipoDocumento desc,vDescripcionArea desc
	offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
 
 END
GO


IF OBJECT_ID('[dbo].[UP_MAE_Obtener_Tipo_Documento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_Obtener_Tipo_Documento]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_Obtener_Tipo_Documento] 
@iCodTipoDocumento  int
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Tipo_Documento]
-- Objetivo:	Listar Tipo Documento
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de de Tipos de Documento Activos
-- Create date: 03/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodTipDocumento,
		a.vAbreviatura,
		a.vDescripcion,
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado
	FROM dbo.MaeTipDocumento a
	where iCodTipDocumento = @iCodTipoDocumento
 
 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_ObtenerTipoDocumentoPorDescripcion]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_ObtenerTipoDocumentoPorDescripcion]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_ObtenerTipoDocumentoPorDescripcion] 

	@vDescripcion VARCHAR(1000) 

AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT 
		a.iCodTipDocumento,
		a.vAbreviatura,
		a.vDescripcion,
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado
	FROM dbo.MaeTipDocumento a
	where vDescripcion = @vDescripcion
END
GO
--test
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[UP_MAE_ActualizarTipoDocumento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_ActualizarTipoDocumento]
END
GO

CREATE PROCEDURE [dbo].[UP_MAE_ActualizarTipoDocumento]
   @iCodTipoDocumento int
 ,@vAbreviatura VARCHAR(20) = NULL
 ,@vDescripcion VARCHAR(1000) = NULL
 ,@Estado int
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_ActualizarTipoDocumento]
-- Objetivo:	Actualizar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@iCodTipoDocumento - Codigo Tipo Documento
--				@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuActualizacion - Campo Auditoria Usuario Creación
--				@vHstActualizacion - Campo Auditoría Host
--				@vInsActualizacion - Campo Auditoría Instancia
--				@vLgnActualizacion - Campo Auditoría Login
--				@vRolActualizacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

    UPDATE MaeTipDocumento set vAbreviatura = @vAbreviatura,vDescripcion= @vDescripcion,
							   vUsuActualizacion = @vUsuActualizacion,
							   siEstado = @Estado,
								dFecActualizacion = GETDATE(),
								vHstActualizacion = @@SERVERNAME,
								vInsCreacion = @@SERVICENAME,
								vLgnActualizacion = @vUsuActualizacion,
								vRolActualizacion = @vRolActualizacion
								where iCodTipDocumento = @iCodTipoDocumento

  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodTipDocumento) FROM dbo.MaeTipDocumento 

 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_RegistrarTipoDocumento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_RegistrarTipoDocumento]
END
GO

CREATE PROCEDURE [dbo].[UP_MAE_RegistrarTipoDocumento]
   @vAbreviatura VARCHAR(20) = NULL
 ,@vDescripcion VARCHAR(1000) = NULL
 ,@Estado int
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_RegistrarTipoDocumento]
-- Objetivo:	Registrar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

    INSERT INTO dbo.MaeTipDocumento(     
     vAbreviatura
    ,vDescripcion
    ,siEstado
    ,vUsuCreacion
    ,dFecCreacion
    ,vHstCreacion
	,vInsCreacion
	,vLgnCreacion
	,vRolCreacion
 )
 VALUES(     
     @vAbreviatura
    ,@vDescripcion
    ,@Estado
    ,@vUsuCreacion
    ,GETDATE()
    ,@@SERVERNAME
    ,@@servicename
    ,@vLgnCreacion
    ,@vRolCreacion
 )
  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodTipDocumento) FROM dbo.MaeTipDocumento 

 END
GO

IF OBJECT_ID('[dbo].[UP_MAE_Listar_Tipo_DocumentoPaginado]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_Listar_Tipo_DocumentoPaginado]
END
GO
CREATE PROCEDURE [dbo].[UP_MAE_Listar_Tipo_DocumentoPaginado] 
 @NumeroPagina int,
 @tamaniopagina int,
 @texto VARCHAR(20)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Tipo_DocumentoPaginado]
-- Objetivo:	Listar Tipo Documento
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de de Tipos de Documento Activos
-- Create date: 03/02/20
-- =============================================
BEGIN
		WITH TIPODOCUMENTO AS (SELECT 
		a.iCodTipDocumento,
		a.vAbreviatura,
		a.vDescripcion,
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado
	FROM dbo.MaeTipDocumento a
	WHERE  UPPER(vDescripcion) like '%'+ UPPER(@texto) +'%'
	)

	SELECT iCodTipDocumento,vAbreviatura,vDescripcion,siEstado,dFecCreacion
	,vUsuCreacion,dFecActualizacion,vUsuActualizacion,
	(SELECT COUNT(1) FROM TIPODOCUMENTO) TotalRegistros FROM TIPODOCUMENTO
	order by iCodTipDocumento
	offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
 
 END
GO

CREATE FUNCTION [dbo].[UF_MOV_OBTENERNOMJEFE]
(
	@iCodArea int
)
RETURNS varchar(1000)
AS
BEGIN
	declare @salida varchar(100)=''''
	
	
	select @salida=@salida + case when LEN(isnull(@salida,''''))=0 THEN '''' ELSE N''';''' END +vNombres+ SPACE(1)+ vApePaterno+ SPACE(1) +vApeMaterno from MaeTrabajador a
	inner join MaeArea b on a.iCodArea=b.iCodArea
	where b.iCodArea=@iCodArea
	and b.siEstado=1 and a.siEstado=1
	
	RETURN @salida
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_CalcularFechaPlazoDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_CalcularFechaPlazoDocumento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MAE_CalcularFechaPlazoDocumento] 
(
	@CantDiasPlazo INT,
	@FechaInicio DATETIME NULL,
	@FechaPlazo DATETIME OUT
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_CalcularFechaPlazoDocumento]
-- Objetivo:	Calcular fecha de plazo de los Documentos
-- Módulo:		Maestros
-- Input:		@CantDiasPlazo : Cantidad de Dias de Plazo
--				@FechaInicio :  Fecha de Inicio

-- Output:  
--				@FechaPlazo :  Fecha de Plazo Calculada	
-- Create date: 03/02/20
-- =============================================
BEGIN
	
	DECLARE @FechaIni DATETIME = GETDATE()

	IF @FechaInicio IS NOT NULL
		SET @FechaIni = @FechaInicio

	DECLARE @YEAR AS INT = DATENAME(YYYY, @FechaIni)
	DECLARE @FromDate DATETIME=DATEADD(yyyy, @YEAR - 1900, 0)
	DECLARE @ToDate DATETIME=DATEADD(yyyy, @Year - 1900 + 2, -1)

	--SELECT @YEAR, @FromDate, @ToDate

	DECLARE @DIAS_HABILES AS TABLE
	(
		NROW INT,
		FECHA DATETIME,
		MES VARCHAR(50), 
		NOMBRE VARCHAR(50),
		DIA_SEMANA INT
	);

	WITH DATES (DateNo) AS (  
	SELECT DATEADD(DAY, DATEDIFF(DAY, 0, @ToDate) - DATEDIFF(DAY, @FromDate, @ToDate), 0)  
	UNION ALL SELECT DATEADD(DAY, 1, DateNo)  
	FROM DATES  
	WHERE DATEADD(DAY, 1, DateNo) <=@ToDate)

	INSERT INTO @DIAS_HABILES(NROW, FECHA, MES, NOMBRE, DIA_SEMANA)
	SELECT 
		ROW_NUMBER() OVER (ORDER BY DateNo ASC) AS NROW,
		DateNo AS FECHA,
		DATENAME(MONTH, DateNo) AS MONTH_DAY,
		DATENAME(DW, DateNo) AS NAME_DAY,
		DATEPART(DW, DateNo) AS WEEK_DAY
	FROM DATES D
		LEFT JOIN MaeFeriado F 
		ON D.DateNo = F.dFecha
	WHERE DateNo > (@FechaIni)--LISTA A PARTIR DEL DIA SIGUIENTE
		AND DATEPART(DW, DateNo) NOT IN (1, 7)
		AND F.iCodFeriado IS NULL

	OPTION (MAXRECURSION 0)
	
	IF(@CantDiasPlazo = 0)
	BEGIN

		SET @FechaPlazo = @FechaIni;		
	
	END
	ELSE
	BEGIN
		SELECT @FechaPlazo = FECHA
		FROM @DIAS_HABILES
		WHERE NROW = @CantDiasPlazo--PLAZO

	END	
	
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[UP_MAE_Generar_Secuencial]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MAE_Generar_Secuencial]
END
GO


create PROCEDURE [dbo].[UP_MAE_Generar_Secuencial]
  @iOrigen INT
 ,@iCodTipoDocumento INT
 ,@iCodArea INT
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onSecuencial INT OUTPUT
 ,@onCorrelativo VARCHAR(100) OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Generar_Secuencial]
-- Objetivo:	Generar Secuencial de Documentos
-- Módulo:		Maestros
-- Input:		@iCodTipoDocumento : Código Tipo Documento
--				@iCodArea	: Código de Área
-- Output:
--				@onCorrelativo - Correlativo Generado
-- Create date: 27/01/20
-- =============================================
DECLARE @AnioActual AS INT
DECLARE @IdSecuencialMax AS INT
DECLARE @CorrelativoExist AS VARCHAR(100)
DECLARE @SecuencialExist AS INT
declare @onCorrelativoNumero AS int;
BEGIN
  
  SET @AnioActual=YEAR(GETDATE()) 
	--VALIDAMOS SI HAY SECUENCIAS USADAS
	IF(@iOrigen = (SELECT iCodParametro FROM ParParametro  WHERE vCampo='EXTERNO'))
	BEGIN
		 SELECT @CorrelativoExist= vCorrelativo , @SecuencialExist=iCodSecuencial FROM  MaeSecuencial WHERE siOrigen = @iOrigen AND iAnio  = @AnioActual AND fusado = 0;
	END
	ELSE IF(@iOrigen = (SELECT iCodParametro FROM ParParametro  WHERE vCampo='INTERNO'))
	BEGIN
		 SELECT @CorrelativoExist= vCorrelativo , @SecuencialExist=iCodSecuencial FROM  MaeSecuencial WHERE siOrigen = @iOrigen AND iAnio  = @AnioActual AND iCodArea=@iCodArea  AND iCodTipDocumento=@iCodTipoDocumento AND fusado = 0;
	END

  --Máximo Identificador
  SELECT @IdSecuencialMax=MAX(iCodSecuencial) FROM dbo.MaeSecuencial

	IF @IdSecuencialMax IS NULL 
	    SET @IdSecuencialMax=1;
	ELSE
		SET @IdSecuencialMax=@IdSecuencialMax+1;


   

   if(@CorrelativoExist is not null)
   begin
   SET @onSecuencial  =  @SecuencialExist
   SET @onCorrelativo  =  @CorrelativoExist
    UPDATE MaeSecuencial set fUsado = 1,
							iCodArea = @iCodArea,
							iCodTipDocumento = @iCodTipoDocumento
							where vCorrelativo = @CorrelativoExist;
   end
   else
   begin

   declare @Anio int =YEAR(GETDATE())
   SET @onSecuencial=dbo.UF_MOV_OBTENERCORRELATIVO(@iOrigen,@iCodArea,@iCodTipoDocumento,@Anio, 1)
   SET @onCorrelativo=dbo.UF_MOV_OBTENERCORRELATIVO(@iOrigen,@iCodArea,@iCodTipoDocumento,@Anio, 2)
    --INSERTANDO NUEVO SECUENCIAL
	INSERT INTO dbo.MaeSecuencial(
	iAnio,
	siEstado,
	iCodSecuencial,
	iCorrelativo,
	vCorrelativo,
	iCodArea,
	iCodTipDocumento,
	vUsuCreacion,
	dFecCreacion,
	vHstCreacion,
	vInsCreacion,
	vLgnCreacion,
	vRolCreacion,
	siOrigen,
	fUsado
	)
	VALUES(
	@AnioActual,
	1,
	@IdSecuencialMax,
	@onSecuencial,
	@onCorrelativo,
	@iCodArea,
	@iCodTipoDocumento,
	@vUsuCreacion,
	GETDATE(),
	@@SERVERNAME,
	@@servicename,
	@vLgnCreacion,
	@vRolCreacion,
	@iOrigen,
	1
	)
end

  IF @@ROWCOUNT  > 0 
     SELECT @onCorrelativo
  ELSE
	 SELECT 0

 END




GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Area]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Listar_Area] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Listar_Area]
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Area]
-- Objetivo:	Listar Áreas
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Áreas	
-- Create date: 30/01/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodArea,
		a.vDescripcion,
		a.siEstado
	FROM dbo.MaeArea a
	WHERE a.siEstado = 1;
 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Asunto]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Listar_Asunto] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Listar_Asunto]
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Asunto]
-- Objetivo:	Listar Asuntos
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Asuntos	
-- Create date: 06/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodAsunto,
		a.vDescripcion,
		a.siEstado
	FROM dbo.MaeAsunto a
	WHERE a.siEstado = 1;
 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Empresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Listar_Empresa] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Listar_Empresa]
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Empresa]
-- Objetivo:	Listar Empresas
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Empresas	
-- Create date: 04/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodEmpresa,
		a.vDescripcion,
		a.vDireccion,
		a.siEstado
	FROM dbo.MaeEmpresa a
	WHERE a.siEstado = 1;
 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Representante_x_Emp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Listar_Representante_x_Emp] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Listar_Representante_x_Emp]
@CodEmpresa INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Representante_x_Emp]
-- Objetivo:	Listar Representantes por Empresa
-- Módulo:		Maestros
-- Input:		@CodEmpresa : Código de la Empresa
-- Output:  
--			Listado de Representates de la Empresa enviada como parámetro	
-- Create date: 04/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodRepresentante,
		a.vNombres,
		a.siEstado
	FROM dbo.MaeRepresentante a
	WHERE a.siEstado = 1 AND a.iCodEmpresa=@CodEmpresa;
 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Tipo_Documento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Listar_Tipo_Documento] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_Listar_Tipo_Documento] 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Tipo_Documento]
-- Objetivo:	Listar Tipo Documento
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de de Tipos de Documento Activos
-- Create date: 03/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodTipDocumento,
		a.vDescripcion,
		a.siEstado
	FROM dbo.MaeTipDocumento a
	WHERE a.siEstado = 1;
 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarTrabajadorPorArea]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ListarTrabajadorPorArea] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_ListarTrabajadorPorArea]
(
 @iCodArea INT,
 @siJefe int
)
AS
BEGIN
	SELECT 
		T.[iCodTrabajador], [vNumDocumento], [vNombres], [vCargo], [vUbicacion], [iAnexo], [vEmail],
		[vApePaterno], [vApeMaterno], [siEsJefe], [iCodArea], [iCodTipDocIdentidad], [vCelular],
		U.WEBUSR
	FROM [dbo].[MaeTrabajador] T
		INNER JOIN DBO.SegUsuario U
		ON U.iCodTrabajador = T.iCodTrabajador
	WHERE 
		iCodArea = @iCodArea
    AND (T.siEsJefe = @siJefe or @siJefe = 99)
	AND U.siEstado= 1
	AND T.siEstado = 1

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_RegistrarEmpresa] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MAE_RegistrarEmpresa]
  @vAbreviatura VARCHAR(20) = NULL
 ,@vDescripcion VARCHAR(20) = NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_RegistrarEmpresa]
-- Objetivo:	Registrar una Empresa
-- Módulo:		Mantenimientos
-- Input:		@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

    INSERT INTO dbo.MaeEmpresa(     
     vAbreviatura
    ,vDescripcion
    ,siEstado
    ,vUsuCreacion
    ,dFecCreacion
    ,vHstCreacion
	,vInsCreacion
	,vLgnCreacion
	,vRolCreacion
 )
 VALUES(     
     RTRIM(LTRIM(@vAbreviatura))
    ,RTRIM(LTRIM(@vDescripcion))
    ,1
    ,@vUsuCreacion
    ,GETDATE()
    ,@vHstCreacion
    ,@vInsCreacion
    ,@vLgnCreacion
    ,@vRolCreacion
 )
  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodEmpresa) FROM dbo.MaeEmpresa 

 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Trabajador]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Trabajador] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MAE_Trabajador]
(
@piCodTrabajador INT
)
AS
BEGIN
	SELECT 
	tr.[iCodTrabajador], [vNumDocumento], [vNombres], [vCargo], [vUbicacion], [iAnexo], [vEmail],
	[vApePaterno], [vApeMaterno], [siEsJefe], TR.[iCodArea], [iCodTipDocIdentidad], [vCelular],ARE.vDescripcion AS DESCRIPCIONAREA,SEG.WEBUSR
	FROM [dbo].[MaeTrabajador]	 TR
	LEFT  JOIN SegUsuario SEG  ON  TR.iCodTrabajador  = seg.iCodTrabajador
	INNER  JOIN MaeArea ARE  ON TR.iCodArea = ARE.iCodArea
	WHERE tr.iCodTrabajador=@piCodTrabajador
	AND TR.siEstado=1
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_MaeTrabajador_qry01]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[up_MaeTrabajador_qry01] AS' 
END
GO

ALTER PROCEDURE [dbo].[up_MaeTrabajador_qry01]
(
@piCodTrabajador INT
)
AS
BEGIN
	SELECT 
	[iCodTrabajador], [vNumDocumento], [vNombres], [vCargo], [vUbicacion], [iAnexo], [vEmail],
	[vApePaterno], [vApeMaterno], [siEsJefe], [iCodArea], [iCodTipDocIdentidad], [vCelular]
	FROM [dbo].[MaeTrabajador]	
	WHERE iCodTrabajador=@piCodTrabajador
	AND siEstado=1
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarDocAdjunto]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ActualizarDocAdjunto] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ActualizarDocAdjunto]
  @iCodDocAdjunto INT 
 ,@iCodLaserfiche  INT = NULL
 ,@iCodDocumento INT 
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocAdjunto]
-- Objetivo:	Registrar Adjuntos a un Documento
-- Módulo:		Movimientos
-- Input:		@iCodLaserfiche - Código de Documento en LaserFiche
--				@iCodDocumento -  Codigo de Documento
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
	SET @onFlagOK = 0;

	UPDATE dbo.MovDocAdjunto
	SET iCodLaserfiche = @iCodLaserfiche,
		vUsuActualizacion = @vUsuActualizacion,
		dFecActualizacion = GETDATE(),
		vHstActualizacion = @@SERVERNAME,
		vInsCreacion = @@SERVICENAME,
		vRolActualizacion = @vRolActualizacion,
		vLgnActualizacion = @vLgnActualizacion	
	WHERE iCodDocAdjunto = @iCodDocAdjunto
		AND iCodDocumento = @iCodDocumento

	SET @onFlagOK = 1;

 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ActualizarDocumento] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ActualizarDocumento]
 @iCodDocumento int
 ,@iCodSecuencial INT = NULL
 ,@iAnio INT = NULL
 ,@siEstado SMALLINT 
 ,@siOrigen SMALLINT = NULL 
 ,@vNumDocumento VARCHAR(100) = NULL
 ,@siEstDocumento SMALLINT = NULL
 ,@dFecDocumento DATETIME = NULL
 ,@dFecRegistro DATETIME = NULL
 ,@dFecDerivacion DATETIME = NULL
 ,@vAsunto VARCHAR(1000) = NULL
 ,@siPrioridad SMALLINT = NULL
 ,@vReferencia VARCHAR(1000) = NULL
 ,@dFecRecepcion DATETIME = NULL
 ,@siPlazo SMALLINT = NULL
 ,@dFecPlazo DATETIME = NULL
 ,@vObservaciones VARCHAR(1000) = NULL
 ,@iCodTipDocumento INT 
 ,@iCodRepresentante INT
 ,@iCodArea INT
 ,@iCodTrabajador INT
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocumento]
-- Objetivo:	Registrar un nuevo Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
--				@iCodSecuencial -  Número de Secuencial
--				@iAnio		  - Anio Correlativo
--				@siOrigen	  - Estado del Registro: 1 - Activo; 0 - Inactivo
--				@vNumDocumento	  - Número de Documento
--				@siEstDocumento	  - Estado del Documento
--				@dFecDocumento	  - Fecha del Documento
--				@dFecRegistro	  - Fecha de Registro
--				@vAsunto			- Asunto
--				@siPrioridad		- Indicador de Prioridad
--				@vReferencia		- Referencia
--				@dFecRecepcion	  - Fecha de Recepción
--				@siPlazo			- Indicador de Plazo
--				@dFecPlazo			- Fecha de Plazo
--				@vObservaciones		- Observaciones
--				@iCodTipDocumento	- Codigo Tipo Documento
--				@iCodRepresentante  - Código de Representante
--				@iCodArea			- Código de Área
--				@iCodTrabajador	    - Codigo de Trabajador
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
	--SELECT * 
	--FROM MaeTrabajador @iCodArea


		SET @onFlagOK = 0;

		UPDATE dbo.MovDocumento
		SET siEstDocumento = @siEstDocumento,
			dFecDocumento = @dFecDocumento,
			--dFecRegistro = @dFecRegistro,
			vAsunto = @vAsunto,
			siPrioridad = @siPrioridad,
			vReferencia = @vReferencia,
		    dFecRecepcion = @dFecRecepcion,
			siPlazo = @siPlazo,
			dFecPlazo = @dFecPlazo,
			vObservaciones = @vObservaciones,
			--iCodTipDocumento = @iCodTipDocumento,
			iCodRepresentante = @iCodRepresentante,
			iCodArea = @iCodArea,
			iCodTrabajador = @iCodTrabajador,
			vUsuActualizacion = @vUsuActualizacion,
			dFecActualizacion = GETDATE(),
			vHstActualizacion = @@SERVERNAME,
			vInsCreacion = @@SERVICENAME,
			vLgnActualizacion = @vUsuActualizacion,
			vRolActualizacion = @vRolActualizacion,
			dFecDerivacion=@dFecDerivacion
		WHERE iCodDocumento = @iCodDocumento

		SET @onFlagOK = 1
		


 END



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_AnularDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_AnularDocumento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_AnularDocumento]
  @iCodDocumento INT,
  @vUsuAnulador varchar(50),
  @vComentario varchar(1000)
 ,@onFlagOK INT OUTPUT--
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_AnularDocumento]
-- Objetivo:	Anular un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 24/02/20
-- =============================================
BEGIN
		declare @iCodMovAnterior int,
				@iCodArea int,
				@iCodTrabajador int,
				@newEstadoDocumento int=3;
				
		UPDATE MovDocumento
		set dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuAnulador,
		siEstDocumento=@newEstadoDocumento
		WHERE iCodDocumento = @iCodDocumento;

		--mov anterior
		SELECT  TOP 1 @iCodMovAnterior=iCodDocMovimiento
		FROM MovDocMovimiento
		WHERE iCodDocumento = @iCodDocumento
		ORDER BY iCodDocMovimiento DESC;
		--datos adicionales del doc
		SELECT 
		@iCodArea= iCodArea,
		@iCodTrabajador=iCodTrabajador
		FROM MovDocumento
		WHERE iCodDocumento = @iCodDocumento
		--insertando mov
		insert into MovDocMovimiento(siestado,iCodMovAnterior,iCodDocumento,iCodArea,iCodTrabajador,iEstadoDocumento,iTipoMovimiento
		, vusucreacion,dfeccreacion,vHstCreacion)
		values(1,@iCodMovAnterior,@iCodDocumento,@iCodArea,@iCodTrabajador,@newEstadoDocumento, 6
		, @vUsuAnulador, getdate(), @vUsuAnulador)
		--insertando comentario
		insert into MovDocComentario(vcomentario, siestado, icodDocmovimiento,iCodDocumento, vusucreacion,dfeccreacion,vHstCreacion)
		values(@vComentario , 1, SCOPE_IDENTITY(),@iCodDocumento, @vUsuAnulador, getdate(), @vUsuAnulador);

		 IF @@ROWCOUNT  > 0 
		 BEGIN
			 set @onFlagOK=1;
		 END

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_CambiarEstadoDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_CambiarEstadoDocumento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_CambiarEstadoDocumento]
  @iCodDocumento INT,
  @newEstadoDocumento INT,
  @vUsuario varchar(50)
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[[UP_MOV_CambiarEstadoDocumento]]
-- Objetivo:	Cambio Estado Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		declare @iCodMovAnterior int,
				@iCodArea int,
				@iCodTrabajador int;

		UPDATE MovDocumento
		set 		
		siEstDocumento=@newEstadoDocumento,
		dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario
		WHERE iCodDocumento = @iCodDocumento;


		IF @@ROWCOUNT  > 0 
			SELECT @onFlagOK=@iCodDocumento
		ELSE
			SET @onFlagOK=0

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]
  @iCodDocumentoDerivacion INT,
  @newEstadoDocumento INT,
  @vUsuario varchar(50)
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]
-- Objetivo:	Cambio Estado Documento Derivación
-- Módulo:		Movimientos
-- Input:		@iCodDocumentoDerivacion - Código de Documento derivación
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		declare @iCodMovAnterior int,
				@iCodArea int,
				@iCodTrabajador int;

		UPDATE MovDocumentoDerivacion
		set 		
		siEstadoDerivacion=@newEstadoDocumento,
		dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario
		WHERE iCodDocumentoDerivacion = @iCodDocumentoDerivacion;


		IF @@ROWCOUNT  > 0 
			SELECT @onFlagOK=@iCodDocumentoDerivacion
		ELSE
			SET @onFlagOK=0

END



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DerivarDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_DerivarDocumento] AS' 
END
GO

ALTER   PROCEDURE [dbo].[UP_MOV_DerivarDocumento]
  @iCodArea int
, @iCodDocumento int
, @iCodTrabajador int
,@iTipoAccion int
, @vUsuCreacion varchar(100) = NULL
, @vLgnCreacion varchar(20) = NULL
, @vRolCreacion varchar(20) = NULL
,@vHstCreacion VARCHAR(20) = NULL
,@vInsCreacion VARCHAR(50) = NULL
, @vUsuActualizacion varchar(100) = NULL
, @vLgnActualizacion varchar(20) = NULL
, @vRolActualizacion varchar(20) = NULL
,@vHstActualizacion VARCHAR(20) = NULL
,@vInsActualizacion VARCHAR(50) = NULL
, @onFlagOK int OUTPUT
AS
-- =============================================
-- Nombre:  [dbo].[UP_MOV_DerivarDocumentoV2]
-- Objetivo:	Registrar Derivacion Documento
-- Módulo:		Movimientos
-- Input:		@iCodArea - Código de Documento
-- Input:		@iCodTrabajador - Código de Trabajador
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
  DECLARE 
		  @esConCopia  int,
		  @existeMovDocumento int,
	      @existeDerivado int,
	      @iCodMovAnterior int,
		  @newEstadoDocumentoProceso int=6,
		  @siEstadoDerivacionPendiente int = 2,
		  @EstadoDocumentoDerivado int = 4,
		  @icodDocDerivacion int,
		  @DerivadoOrigenInterno int,
		  @OrigenInterno int = 1,
		  @OrigenExterno int = 2
  SET @onFlagOK = 0;
  -- VALIDAMOS QUE EXISTA EL REGISTRO EN DOCUMENTO
  SELECT
    @existeMovDocumento = COUNT(1)
  FROM MovDocumento
  WHERE iCodArea = @iCodArea
  AND iCodDocumento = @iCodDocumento;
  --VALIDAMOS SI EL EXISTE UN DOCUMENTO EN COPIA
  SELECT @esConCopia = count(1)
  FROM MovDocumentoDerivacion
  where iCodDocumento = @iCodDocumento
  and iCodResponsable = @iCodTrabajador
  AND siTipoAcceso = 2;
   --VALIDAMOS SI EL ORIGEN ES INTERNO
	SELECT @DerivadoOrigenInterno = siOrigen FROM MovDocumento WHERE iCodDocumento = @iCodDocumento and siEstDocumento = @siEstadoDerivacionPendiente;

  -- SI EXISTE EL DOCUMENTO EN COPIA , ACTUALIZAMOS EL TIPO DE ACCESO Y EL ESTADO DE DERIVACION
  IF (@esConCopia > 0)
  BEGIN
   -- obtenemos el cod de derivacion de ese documento
   select @icodDocDerivacion = iCodDocumentoDerivacion from MovDocumentoDerivacion
		  where  iCodDocumento =@iCodDocumento and siEstadoDerivacion is null
		  and iCodResponsable = @iCodTrabajador;
   
   UPDATE MovDocumentoDerivacion
		SET siEstadoDerivacion = @siEstadoDerivacionPendiente,
		    siTipoAcceso = 1,
			iTipoAccion = @iTipoAccion,
			vHstActualizacion = @@SERVERNAME,
			vInsActualizacion = @@servicename,
			vLgnActualizacion = @vLgnActualizacion,
			vRolActualizacion = @vRolActualizacion
   where iCodDocumento = @iCodDocumento and siEstadoDerivacion is null
		  and iCodResponsable = @iCodTrabajador;

	 UPDATE MovDocumento
		SET siEstDocumento = @newEstadoDocumentoProceso,
		  
			vHstActualizacion = @@SERVERNAME,
			vInsActualizacion = @@servicename,
			vLgnActualizacion = @vLgnActualizacion,
			vRolActualizacion = @vRolActualizacion
   where iCodDocumento = @iCodDocumento;
 
  -- INSERTA EL MOVIMIENTO

  EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
  @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
  @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT
 

  END 
  ELSE
  BEGIN
  --VALIDAMOS SI ES UN DOCUMENTO DERIVADO
  SELECT @existeDerivado = count(1)
  FROM MovDocumentoDerivacion
  where iCodDocumento = @iCodDocumento
  and iCodResponsable = (select iCodTrabajador From SegUsuario where WEBUSR = @vUsuCreacion) 
  AND siTipoAcceso = 1;

  IF (@existeDerivado > 0)
  BEGIN

  SELECT @icodDocDerivacion = iCodDocumentoDerivacion FROM MovDocumentoDerivacion WHERE iCodDocumento=@iCodDocumento AND iCodResponsable = (select iCodTrabajador From SegUsuario where WEBUSR = @vUsuCreacion) ;;

 --VALIDAMOS SI EL ORIGEN ES INTERNO, PARA ACTUALIZAR EL ESTADO DEL DOCUMENTO A ENN PROCESO
	
	IF(@OrigenInterno = @DerivadoOrigenInterno)
	begin 
		UPDATE MovDocumento
		SET 
			siEstDocumento = @newEstadoDocumentoProceso,
			vUsuActualizacion = @vUsuActualizacion,
			dFecActualizacion = GETDATE(),
			vHstActualizacion = @@SERVERNAME,
			vInsCreacion = @@SERVICENAME,
			vLgnActualizacion = @vUsuActualizacion,
			vRolActualizacion = @vRolActualizacion
		WHERE iCodDocumento = @iCodDocumento;

		/*  EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
  @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
  @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT
  */
	end

 -- SI EXISTE UN DOCUMENTO EN LA TABLA DERIVADO (ACCESO 1) SE ACTUALIZA A DERIVADO. (1 ITERACION)
  UPDATE MovDocumentoDerivacion
		SET siEstadoDerivacion = @EstadoDocumentoDerivado,
			iTipoAccion = @iTipoAccion,
			vHstActualizacion = @@SERVERNAME,
			vInsActualizacion = @@servicename,
			vLgnActualizacion = @vLgnActualizacion,
			vRolActualizacion = @vRolActualizacion
  where iCodDocumento = @iCodDocumento
  and iCodResponsable = (select iCodTrabajador From SegUsuario where WEBUSR = @vUsuCreacion) ;

   
  EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
  @EstadoDocumentoDerivado/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
  @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT

 
   INSERT INTO MovDocumentoDerivacion
   (iCodDocumento,siEstado,iCodArea,iCodResponsable,siEstadoDerivacion,siTipoAcceso,iTipoAccion,dFecDerivacion,vUsuCreacion,dFecCreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
    VALUES
   (@iCodDocumento, 1, @iCodArea, @iCodTrabajador, @siEstadoDerivacionPendiente, 1,@iTipoAccion, GETDATE(),@vUsuCreacion, GETDATE(), @@SERVERNAME, @@SERVICENAME, @vLgnCreacion, @vRolCreacion);
   
	set @icodDocDerivacion = SCOPE_IDENTITY();

   EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
 @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
  @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT

  END
  ELSE
  BEGIN
  IF (@existeMovDocumento > 0)
  BEGIN
  -- SI DERIVAS A TU MISMA AREA ACTUALIZAMOS  EL ESTADO DEL DOCUMENTO A EN PROCESO

	  UPDATE MovDocumento
    SET
        siEstDocumento = @newEstadoDocumentoProceso,
        vUsuActualizacion = @vUsuActualizacion,
        dFecActualizacion = GETDATE(),
        vHstActualizacion = @@SERVERNAME,
        vInsCreacion = @@SERVICENAME,
        vLgnActualizacion = @vUsuActualizacion,
        vRolActualizacion = @vRolActualizacion
    WHERE iCodDocumento = @iCodDocumento;
	

	 INSERT INTO MovDocumentoDerivacion
   (iCodDocumento,siEstado,iCodArea,iCodResponsable,siEstadoDerivacion,siTipoAcceso,iTipoAccion,dFecDerivacion,vUsuCreacion,dFecCreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
    VALUES
   (@iCodDocumento, 1, @iCodArea, @iCodTrabajador,@siEstadoDerivacionPendiente, 1,@iTipoAccion, GETDATE(),@vUsuCreacion, GETDATE(), @@SERVERNAME, @@SERVICENAME, @vLgnCreacion, @vRolCreacion);

	set @icodDocDerivacion = SCOPE_IDENTITY();

	EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
	  @iCodDocumento,@icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,@iCodTrabajador/*@iCodTrabajador*/,
	  @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,1/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
	  null/*@iCodAreaDerivacion*/,null/*@iCodResponsableDerivacion*/,
	  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT
	

  END
  ELSE
  BEGIN
  -- SI SE DERIVA A OTRA AREA SOLO ACTUALIZAMOS EL ESTADO DEL DOCUMENTO A EN PROCESO
	 UPDATE MovDocumento
    SET 
        siEstDocumento = @newEstadoDocumentoProceso,
        vUsuActualizacion = @vUsuActualizacion,
        dFecActualizacion = GETDATE(),
        vHstActualizacion = @@SERVERNAME,
        vInsCreacion = @@SERVICENAME,
        vLgnActualizacion = @vUsuActualizacion,
        vRolActualizacion = @vRolActualizacion
    WHERE iCodDocumento = @iCodDocumento;

    INSERT INTO MovDocumentoDerivacion
		(iCodDocumento,siEstado,iCodArea,iCodResponsable,siEstadoDerivacion,siTipoAcceso,dFecDerivacion,iTipoAccion,vUsuCreacion,dFecCreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		 VALUES
		(@iCodDocumento, 1, @iCodArea, @iCodTrabajador, @siEstadoDerivacionPendiente, 1, GETDATE(),@iTipoAccion,@vUsuCreacion, GETDATE(), @@SERVERNAME, @@SERVICENAME, @vLgnCreacion, @vRolCreacion);

	set @icodDocDerivacion = SCOPE_IDENTITY();

	 EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
	 @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
	 @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
	 @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
	 @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT
	
  END
  END
  END
END



GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DerivarDocumentoV2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_DerivarDocumentoV2] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_DerivarDocumentoV2]
  @iCodArea int
, @iCodDocumento int
, @iCodTrabajador int
, @iTipoAccion int
, @vUsuCreacion varchar(100) = NULL
, @vLgnCreacion varchar(20) = NULL
, @vRolCreacion varchar(20) = NULL
,@vHstCreacion VARCHAR(20) = NULL
,@vInsCreacion VARCHAR(50) = NULL
, @vUsuActualizacion varchar(100) = NULL
, @vLgnActualizacion varchar(20) = NULL
, @vRolActualizacion varchar(20) = NULL
,@vHstActualizacion VARCHAR(20) = NULL
,@vInsActualizacion VARCHAR(50) = NULL
, @onFlagOK int OUTPUT
AS
-- =============================================
-- Nombre:  [dbo].[UP_MOV_DerivarDocumentoV2]
-- Objetivo:	Registrar Derivacion Documento
-- Módulo:		Movimientos
-- Input:		@iCodArea - Código de Documento
-- Input:		@iCodTrabajador - Código de Trabajador
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
  DECLARE 
		  @esConCopia  int,
		  @existeMovDocumento int,
	      @existeDerivado int,
	      @iCodMovAnterior int,
		  @newEstadoDocumentoProceso int=6,
		  @siEstadoDerivacionPendiente int = 2,
		  @EstadoDocumentoDerivado int = 4,
		  @icodDocDerivacion int
  SET @onFlagOK = 0;
  -- VALIDAMOS QUE EXISTA EL REGISTRO EN DOCUMENTO
  SELECT
    @existeMovDocumento = COUNT(1)
  FROM MovDocumento
  WHERE iCodArea = @iCodArea
  AND iCodDocumento = @iCodDocumento;
  --VALIDAMOS SI EL EXISTE UN DOCUMENTO EN COPIA
  SELECT @esConCopia = count(1)
  FROM MovDocumentoDerivacion
  where iCodDocumento = @iCodDocumento
  and iCodResponsable = @iCodTrabajador
  AND siTipoAcceso = 2;
  -- SI EXISTE EL DOCUMENTO EN COPIA , ACTUALIZAMOS EL TIPO DE ACCESO Y EL ESTADO DE DERIVACION
  IF (@esConCopia > 0)
  BEGIN
   -- obtenemos el cod de derivacion de ese documento
   select @icodDocDerivacion = iCodDocumentoDerivacion from MovDocumentoDerivacion
		  where  iCodDocumento =@iCodDocumento and siEstadoDerivacion is null
		  and iCodResponsable = @iCodTrabajador;
   
   UPDATE MovDocumentoDerivacion
		SET siEstadoDerivacion = @siEstadoDerivacionPendiente,
		    siTipoAcceso = 1,
			vHstActualizacion = @@SERVERNAME,
			vInsActualizacion = @@servicename,
			vLgnActualizacion = @vLgnActualizacion,
			vRolActualizacion = @vRolActualizacion
   where iCodDocumento = @iCodDocumento and siEstadoDerivacion is null
		  and iCodResponsable = @iCodTrabajador;
 
  -- INSERTA EL MOVIMIENTO

  EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
  @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,null/*@tipoMovimiento*/,null /*@iEstadoDocumento*/,
  @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT


  /*insert into MovDocMovimiento
		(siestado,dFecRecepcion,dFecLectura,iCodMovAnterior,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,
		iCodDocumento,iCodArea,iCodTrabajador,iCodDocDerivacion,iCodAreaDerivacion,iCodResponsableDerivacion,siEstadoDerivacion, vusucreacion,dfeccreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		
  SELECT 1,dFecRecepcion,dFecLectura,iCodDocMovimiento,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,
     dFecFirma,iCodDocumento,iCodArea,iCodTrabajador,@icodDocDerivacion,@iCodArea,@iCodTrabajador,@siEstadoDerivacionPendiente, @vUsuCreacion,getdate(), @@SERVERNAME,@@servicename,@vLgnCreacion,@vLgnCreacion
  FROM MovDocMovimiento WHERE iCodDocMovimiento = (
 							select min(iCodDocMovimiento) from MovDocMovimiento
 							where iCodDocumento=@iCodDocumento
 							and siEstado=1)*/

  END 
  ELSE
  BEGIN
  --VALIDAMOS SI ES UN DOCUMENTO DERIVADO
  SELECT @existeDerivado = count(1)
  FROM MovDocumentoDerivacion
  where iCodDocumento = @iCodDocumento
  and iCodResponsable = (select iCodTrabajador From SegUsuario where WEBUSR = @vUsuCreacion) 
  AND siTipoAcceso = 1;

  IF (@existeDerivado > 0)
  BEGIN
 -- SI EXISTE UN DOCUMENTO EN LA TABLA DERIVADO (ACCESO 1) SE ACTUALIZA A DERIVADO. (1 ITERACION)
  UPDATE MovDocumentoDerivacion
		SET siEstadoDerivacion = @EstadoDocumentoDerivado,
			vHstActualizacion = @@SERVERNAME,
			vInsActualizacion = @@servicename,
			vLgnActualizacion = @vLgnActualizacion,
			vRolActualizacion = @vRolActualizacion
  where iCodDocumento = @iCodDocumento
  and iCodResponsable = (select iCodTrabajador From SegUsuario where WEBUSR = @vUsuCreacion) ;
  -- INSERTA EL MOVIMIENTO

  EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
  @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,null /*@iEstadoDocumento*/,
  @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT

  /* insert into MovDocMovimiento
		(siestado,dFecRecepcion,dFecLectura,iCodMovAnterior,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,iTipoMovimiento,
		iCodDocumento,iCodArea,iCodTrabajador,iCodDocDerivacion,iCodAreaDerivacion,iCodResponsableDerivacion,siEstadoDerivacion, vusucreacion,dfeccreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		
 SELECT 1,dFecRecepcion,dFecLectura,iCodDocMovimiento,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,2,--(Derivado a otra area)
     iCodDocumento,iCodArea,iCodTrabajador,@icodDocDerivacion,@iCodArea,@iCodTrabajador,@siEstadoDerivacionPendiente, @vUsuCreacion,getdate(), @@SERVERNAME,@@servicename,@vLgnCreacion,@vLgnCreacion
  FROM MovDocMovimiento WHERE iCodDocMovimiento = (
 							select min(iCodDocMovimiento) from MovDocMovimiento
 							where iCodDocumento=@iCodDocumento
 							and siEstado=1)
   SELECT @onFlagOK =  SCOPE_IDENTITY();*/
   -- Y SE CREA EL NUEVO DOCUMENTO DERIVACION, CON ESTADO PENDIENTE (2 ITERACION)
   INSERT INTO MovDocumentoDerivacion
   (iCodDocumento,siEstado,iCodArea,iCodResponsable,siEstadoDerivacion,siTipoAcceso,dFecDerivacion,vUsuCreacion,dFecCreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
    VALUES
   (@iCodDocumento, 1, @iCodArea, @iCodTrabajador, @siEstadoDerivacionPendiente, 1, GETDATE(),@vUsuCreacion, GETDATE(), @@SERVERNAME, @@SERVICENAME, @vLgnCreacion, @vRolCreacion);

  END
  ELSE
  BEGIN
  IF (@existeMovDocumento > 0)
  BEGIN
  -- SI DERIVAS A TU MISMA AREA ACTUALIZAMOS EL DOCUMENTO CON EL NUEVO TRABAJADOR Y EL ESTADO DEL DOCUMENTO A EN PROCESO
    UPDATE MovDocumento
    SET iCodTrabajador = @iCodTrabajador,
        siEstDocumento = @newEstadoDocumentoProceso,
        vUsuActualizacion = @vUsuActualizacion,
        dFecActualizacion = GETDATE(),
        vHstActualizacion = @@SERVERNAME,
        vInsCreacion = @@SERVICENAME,
        vLgnActualizacion = @vUsuActualizacion,
        vRolActualizacion = @vRolActualizacion
    WHERE iCodDocumento = @iCodDocumento;

	  EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
	  @iCodDocumento, null/*@icodDocDerivacion*/ ,null/*@iCodArea*/,@iCodTrabajador/*@iCodTrabajador*/,
	  null/*@siEstadoDerivacionPendiente*/,1/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
	  null/*@iCodAreaDerivacion*/,null/*@iCodResponsableDerivacion*/,
	  @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT

	/*insert into MovDocMovimiento
		(siestado,dFecRecepcion,dFecLectura,iCodMovAnterior,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,iTipoMovimiento,
		iCodDocumento,iCodArea,iCodTrabajador,iEstadoDocumento, vusucreacion,dfeccreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		
		 SELECT 1,dFecRecepcion,dFecLectura,iCodDocMovimiento,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma, 1,--(DerivadoMismaArea)
	     iCodDocumento,iCodArea,@iCodTrabajador,@newEstadoDocumentoProceso, @vUsuCreacion,getdate(), @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion
		 FROM MovDocMovimiento WHERE iCodDocMovimiento = (
									select min(iCodDocMovimiento) from MovDocMovimiento
									where iCodDocumento=@iCodDocumento
									and siEstado=1)
	 SELECT @onFlagOK =  SCOPE_IDENTITY();*/

  END
  ELSE
  BEGIN
  -- SI SE DERIVA A OTRA AREA SOLO ACTUALIZAMOS EL ESTADO DEL DOCUMENTO A EN PROCESO
	 UPDATE MovDocumento
    SET 
        siEstDocumento = @newEstadoDocumentoProceso,
        vUsuActualizacion = @vUsuActualizacion,
        dFecActualizacion = GETDATE(),
        vHstActualizacion = @@SERVERNAME,
        vInsCreacion = @@SERVICENAME,
        vLgnActualizacion = @vUsuActualizacion,
        vRolActualizacion = @vRolActualizacion
    WHERE iCodDocumento = @iCodDocumento;

    INSERT INTO MovDocumentoDerivacion
		(iCodDocumento,siEstado,iCodArea,iCodResponsable,siEstadoDerivacion,siTipoAcceso,dFecDerivacion,vUsuCreacion,dFecCreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		 VALUES
		(@iCodDocumento, 1, @iCodArea, @iCodTrabajador, @siEstadoDerivacionPendiente, 1, GETDATE(),@vUsuCreacion, GETDATE(), @@SERVERNAME, @@SERVICENAME, @vLgnCreacion, @vRolCreacion);

	set @icodDocDerivacion = SCOPE_IDENTITY();

    EXEC [UP_MOV_RegistrarDocMovimientoDerivacion]
	 @iCodDocumento, @icodDocDerivacion/*@icodDocDerivacion*/ ,null/*@iCodArea*/,null/*@iCodTrabajador*/,
	 @siEstadoDerivacionPendiente/*@siEstadoDerivacionPendiente*/,2/*@tipoMovimiento*/,@newEstadoDocumentoProceso /*@iEstadoDocumento*/,
	 @iCodArea/*@iCodAreaDerivacion*/,@iCodTrabajador/*@iCodResponsableDerivacion*/,
	 @vUsuCreacion, @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion, @onFlagOK OUTPUT

	/*insert into MovDocMovimiento
		(siestado,dFecRecepcion,dFecLectura,iCodMovAnterior,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,iTipoMovimiento,
		iCodDocumento,iCodArea,iCodTrabajador,iEstadoDocumento,iCodDocDerivacion,iCodAreaDerivacion,iCodResponsableDerivacion,siEstadoDerivacion, vusucreacion,dfeccreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		
		 SELECT 1,dFecRecepcion,dFecLectura,iCodDocMovimiento,siPlazo,dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,2, -- (Derivado Otra Area)
	     iCodDocumento,iCodArea,iCodTrabajador,@newEstadoDocumentoProceso,@icodDocDerivacion,@iCodArea,@iCodTrabajador,@siEstadoDerivacionPendiente, @vUsuCreacion,getdate(), @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion
		 FROM MovDocMovimiento WHERE iCodDocMovimiento = (
									select min(iCodDocMovimiento) from MovDocMovimiento
									where iCodDocumento=@iCodDocumento
									and siEstado=1)

    SELECT @onFlagOK =  SCOPE_IDENTITY();*/
  END
  END
  END
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DevolverDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_DevolverDocumento] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UP_MOV_DevolverDocumento]
  @iCodDocumento INT,
  @iCodDocumentoDerivacion INT,
  @vUsuario varchar(50),
  @vComentario varchar(500)
 ,@onFlagOK INT OUTPUT--
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_DevolverDocumento]
-- Objetivo:	Devolver un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		declare @iCodMovAnterior int,
				@iCodArea int,
				@iCodTrabajador int,
				@newEstadoDocumento int = null,
				@newEstadoDoc int = null
				
		SELECT @newEstadoDoc =  siEstDocumento from MovDocumento where iCodDocumento = @iCodDocumento;
		SELECT @newEstadoDocumento =  siEstadoDerivacion from MovDocumentoDerivacion where iCodDocumento = @iCodDocumentoDerivacion;

		if(@iCodDocumentoDerivacion > 0)
		begin
		set @newEstadoDocumento= 7;
		update MovDocumentoDerivacion 
		set siEstadoDerivacion =@newEstadoDocumento where  iCodDocumentoDerivacion = @iCodDocumentoDerivacion ;
		end
		else
		begin
		set @newEstadoDoc = 7;
		UPDATE MovDocumento
		set dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario,
		siEstDocumento=@newEstadoDoc
		WHERE iCodDocumento = @iCodDocumento;
		end
		--mov anterior
		SELECT  TOP 1 @iCodMovAnterior=iCodDocMovimiento
		FROM MovDocMovimiento
		WHERE iCodDocumento = @iCodDocumento
		ORDER BY iCodDocMovimiento DESC;
		--datos adicionales del doc
		SELECT 
		@iCodArea= iCodArea,
		@iCodTrabajador=iCodTrabajador
		FROM MovDocumento
		WHERE iCodDocumento = @iCodDocumento
		--insertando mov
		insert into MovDocMovimiento(siestado,iCodMovAnterior,iCodDocumento,iCodDocDerivacion,iCodArea,iCodTrabajador,iEstadoDocumento,siEstadoDerivacion,iTipoMovimiento
		, vusucreacion,dfeccreacion,vHstCreacion)
		values(1,@iCodMovAnterior,@iCodDocumento,@iCodDocumentoDerivacion,@iCodArea,@iCodTrabajador,@newEstadoDoc,@newEstadoDocumento,5 --(Tipo devolver)
		, @vUsuario, getdate(), @vUsuario)
		--insertando comentario
		insert into MovDocComentario(vcomentario, siestado, icodDocmovimiento, vusucreacion,dfeccreacion,vHstCreacion,iCodDocumento,iCodDocumentoDerivacion)
		values(@vComentario , 1, SCOPE_IDENTITY(), @vUsuario, getdate(), @vUsuario,@iCodDocumento,@iCodDocumentoDerivacion)

		
		IF @@ROWCOUNT  > 0 
			SELECT @onFlagOK=@iCodDocumento
		ELSE
			SET @onFlagOK=0

END


go


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ValidarDevolverDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ValidarDevolverDocumento] AS' 
END
GO
ALTER  PROCEDURE [dbo].[UP_MOV_ValidarDevolverDocumento]
  @iCodDocumento INT
 ,@onFlagOK INT OUTPUT--
 ,@onMensaje varchar(5000) OUTPUT--
 ,@onCodDocDerivados varchar(5000) OUTPUT--
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_DevolverDocumento]
-- Objetivo:	Devolver un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		declare @valido bit=1;
		
		declare @mensajeAreasRes varchar(MAX)='';
		declare @CodDerivados varchar(MAX)='';

		select 
		@mensajeAreasRes =@mensajeAreasRes+ '- '+tra.vNombres+' '+tra.vApePaterno + '(' +area.vDescripcion +')<br/>',
		@CodDerivados = @CodDerivados + '|' + CAST(der.iCodDocumentoDerivacion AS VARCHAR(10))
		from MovDocumentoDerivacion der
		inner join MaeArea area on area.iCodArea=der.iCodArea
		inner join MaeTrabajador tra on tra.iCodTrabajador=der.iCodResponsable
		where der.iCodDocumento=@iCodDocumento
		and der.siEstadoDerivacion in(select iCodParametro from ParParametro where icodgrupo=1 and vvalor='PEN')
		and der.siTipoAcceso=1
		and der.siEstado=1


		if LEN(@mensajeAreasRes)>0
		begin 
			set @valido=0;
			set @onMensaje=@mensajeAreasRes;
			set @onFlagOK=-2;
			set @onCodDocDerivados = @CodDerivados
		end
		else
		begin
			set @valido=1;
			set @onFlagOK=1;
		end

		SELECT @onFlagOK;
END




go


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_DevolverDocumentoInterno]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_DevolverDocumentoInterno] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_DevolverDocumentoInterno]
  @iCodDocumento INT,
  @vUsuario varchar(50)
 ,@onFlagOK INT OUTPUT--
 ,@onMensaje varchar(5000) OUTPUT--
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_DevolverDocumento]
-- Objetivo:	Devolver un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		
		if EXISTS(select 1 from MovDocumentoDerivacion
		where iCodDocumento=@iCodDocumento
		and siEstadoDerivacion in(select iCodParametro from ParParametro where icodgrupo=1 and vvalor='PEN')
		and siTipoAcceso=1
		and siEstado=1
		)
		begin 
			set @onMensaje='Todavía existen áreas derivadas pendientes.';

			select iCodDocumento, iCodDocumentoDerivacion, iCodArea, iCodResponsable from MovDocumentoDerivacion
			where iCodDocumento=@iCodDocumento
			and siEstadoDerivacion in(select iCodParametro from ParParametro where icodgrupo=1 and vvalor='PEN')
			and siTipoAcceso=1
			and siEstado=1

		end

		declare @iCodMovAnterior int,
				@iCodArea int,
				@iCodTrabajador int,
				@newEstadoDocumento int=7;
				
		UPDATE MovDocumento
		set dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario,
		siEstDocumento=@newEstadoDocumento
		WHERE iCodDocumento = @iCodDocumento;

		
		
		IF @@ROWCOUNT  > 0 
			set @onFlagOK=@iCodDocumento
		ELSE
			SET @onFlagOK=0
END




GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EliminarAnexo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_EliminarAnexo] AS' 
END
GO


ALTER PROCEDURE [dbo].[UP_MOV_EliminarAnexo]
  @iCodLaserFiche INT 
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_EliminarAnexo]
-- Objetivo:	Elimina los anexos asociados a un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

	IF @iCodLaserFiche > 0

	BEGIN

		SET @onFlagOK = 0;

		DELETE FROM MovDocAnexo
		WHERE iCodLaserfiche = @iCodLaserFiche;

		SET @onFlagOK = 1
				
	END

 END



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EliminarDerivacionCC]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_EliminarDerivacionCC] AS' 
END
GO


ALTER PROCEDURE [dbo].[UP_MOV_EliminarDerivacionCC]
  @iCodDocumento INT 
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_EliminarAnexo]
-- Objetivo:	Elimina los cc asociados a un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/02/20
-- =============================================
BEGIN

	IF @iCodDocumento > 0

	BEGIN

		SET @onFlagOK = 0;

		DELETE FROM MovDocumentoDerivacion
		WHERE iCodDocumento = @iCodDocumento
		and   siTipoAcceso = 2;

		SET @onFlagOK = 1
				
	END

 END



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_FinalizarDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_FinalizarDocumento] AS' 
END
GO
/*
alter table MovDocMovimiento
add iEstadoDocumento int
GO
*/
ALTER PROCEDURE [dbo].[UP_MOV_FinalizarDocumento]
  @iCodDocumento INT,
  @vUsuario varchar(50),
  @vComentario varchar(500)
 ,@onFlagOK INT OUTPUT--
 ,@onMensaje varchar(5000) OUTPUT--
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_DevolverDocumento]
-- Objetivo:	Devolver un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		
		if EXISTS(select 1 from MovDocumentoDerivacion
		where iCodDocumento=@iCodDocumento
		and siEstadoDerivacion in(select iCodParametro from ParParametro where icodgrupo=1 and vvalor='PEN')
		and siTipoAcceso=1
		and siEstado=1
		)
		begin 
			set @onMensaje='Todavía existen áreas derivadas pendientes.';

			select iCodDocumento, iCodDocumentoDerivacion, iCodArea, iCodResponsable from MovDocumentoDerivacion
			where iCodDocumento=@iCodDocumento
			and siEstadoDerivacion in(select iCodParametro from ParParametro where icodgrupo=1 and vvalor='PEN')
			and siTipoAcceso=1
			and siEstado=1

		end
		
		declare @iCodMovAnterior int,
				@iCodArea int,
				@iCodTrabajador int,
				@newEstadoDocumento int=5;
				
		UPDATE MovDocumento
		set dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario,
		siEstDocumento=@newEstadoDocumento
		WHERE iCodDocumento = @iCodDocumento;

		
		
		IF @@ROWCOUNT  > 0 
			set @onFlagOK=@iCodDocumento
		ELSE
			SET @onFlagOK=0
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_InactivarDocumentosDerivados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_InactivarDocumentosDerivados] AS' 
END
GO




ALTER PROCEDURE [dbo].[UP_MOV_InactivarDocumentosDerivados]
(
  @iCodDocumentoDerivacion  INT = NULL
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN

SET @onFlagOK = 0;

update MovDocumentoDerivacion
set 
siEstadoDerivacion=0,
vUsuActualizacion=@vUsuActualizacion,
dFecActualizacion=getdate(),
vHstActualizacion=@vHstActualizacion, 
vInsActualizacion=@vInsActualizacion, 
vLgnActualizacion=@vLgnActualizacion, 
vRolActualizacion=@vRolActualizacion
where
@iCodDocumentoDerivacion=iCodDocumentoDerivacion

SET @onFlagOK = 1;

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaEntrada]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada] AS' 
END
GO

ALTER PROC [dbo].[UP_MOV_Listar_BandejaEntrada] (
@vTextoBusqueda VARCHAR(100),
@dFechaDesde datetime,
@dFechaHasta datetime,
@iTipoBandeja int,
@iCodTrabajador int,
@vCodPerfil varchar(50),
@iPagina int,
@iTamPagina int
)
AS
/*
Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3

Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4
*/
BEGIN

	DECLARE @EstadoIngresado INT

	SELECT 
		@EstadoIngresado = icodParametro 
	FROM ParParametro WHERE iCodGrupo=1 AND vValor IN ('ING');

	with areasTrab as
	(
		select iCodArea from [dbo].[MaeTrabajador] where  iCodTrabajador= @iCodTrabajador
	),
	dataTodo as
	(
		SELECT 
			 a.iCodDocumento,a.siEstado AS Estado
			,a.siEstDocumento as EstadoDocumento
			,a.siPrioridad AS Prioridad
			,CASE WHEN siOrigen=1	THEN a.dFecRegistro
									ELSE a.dFecDerivacion END	AS RecibidoDate
			,CASE WHEN siOrigen=1	THEN CONVERT(VARCHAR(10), a.dFecRegistro, 103) 
									ELSE CONVERT(VARCHAR(10), a.dFecDerivacion, 103)  END	AS Recibido
			,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
			,a.iCodSecuencial AS Reg
			,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
			,ISNULL(a.vAsunto, '') AS DocumentoAsunto
			,MR.iCodRepresentante AS CodRepresentante
			,ISNULL(mr.vNombres,c.vDescripcion) AS Remitente
			,c.vAbreviatura AS Derivado
			,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
			,a.siPlazo
			,ma.iCodDocumento as ExisteAdjunto
			,a.vCorrelativo as Correlativo
			,E.iCodEmpresa AS CodEmpresa
			,E.vDescripcion AS NombreEmpresa
			,a.dFecRegistro				
			,c.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,isnull(priori.vCampo,'') vEstadoPrioridad
			,estDoc.iOrden OrdenEstado
			,priori.iOrden OrdenPrioridad
			,a.iCodTrabajador
			,d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador			
			,a.dFecPlazo
			,a.siOrigen
			,concat(a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',mr.vNombres) AS FILTRO ,
					f.iCodDocumentoFirma
		FROM [dbo].[MovDocumento] a WITH(NOLOCK)
		INNER JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		INNER JOIN [dbo].[MaeArea] c WITH(NOLOCK) ON a.iCodArea = c.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] d WITH(NOLOCK) ON a.iCodTrabajador = d.iCodTrabajador
		LEFT join [dbo].[MaeRepresentante] mr WITH(NOLOCK) ON a.iCodRepresentante = mr.iCodRepresentante
		LEFT JOIN MaeEmpresa E WITH(NOLOCK) ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
		left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto WITH(NOLOCK)) ma on a.iCodDocumento = ma.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
		left join ParParametro priori WITH(NOLOCK) on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	    LEFT JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma  WITH(NOLOCK)GROUP BY iCodDocumento) f on a.iCodDocumento = f.iCodDocumento
		WHERE 
		a.siEstado=1
		and dFecRegistro >= cast(@dFechaDesde as date)
		and dFecRegistro < cast(DATEADD(DAY, 1, @dFechaHasta) as date)		
	),
	dataResponsablesDoc as
	(
		select 	
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DA' as TipoDocIng
			,a.iCodDocumentoFirma
		from dataTodo a
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.EstadoDocumento and estDoc.iCodParametro<>0
		where
		a.iCodArea in (select iCodArea from areasTrab)
		and
		(
			isnull(a.iCodTrabajador,-1) = @iCodTrabajador
		)
	),
	dataResponsablesDerivacion as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,DD.iCodDocumentoDerivacion 
			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesCC as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DC' as TipoDocIng
			/*,a.iCodDocumentoFirma*/
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		where
			dd.siEstado=1
			and dd.siTipoAcceso=2
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResultado as
	(
		select *,0 iCodDocumentoDerivacion 
		from dataResponsablesDoc
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select * from dataResponsablesDerivacion
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDoc
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select *,0 iCodDocumentoFirma, 0 iCodDocumentoDerivacion
		from dataResponsablesCC
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesDoc
				)
	), dataresultadofin as (
	select 
	a.*, 
	dbo.UF_MOV_OBTENERNOMJEFE(iCodArea) Jefe,  
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento=2 THEN 1 ELSE 0 END HabilitarSeguimiento,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarDerivar,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN'))
	THEN 1 ELSE 0 END HabilitarDevolver,
	case when TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarFinalizar,
	case when 
		(TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO')))
		OR
		(TipoDocIng = 'DD' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN')))
	THEN 1 ELSE 0 END HabilitarRegistrarAvance
	--(select count(1) from dataResultado)TotalRegistros 
	from dataResultado A
	where
	upper(A.FILTRO)
				LIKE '%' + upper(@vTextoBusqueda) + '%' 
	AND
	(
			(		--RECIBIDOS
				@iTipoBandeja=1 AND 
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER')) 
						and siOrigen=2
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						and siOrigen=1
					)
				)
			)		--CON PLAZO
			OR
			(
				@iTipoBandeja=2 AND 			
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						and siOrigen=2
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						and siOrigen=1
					)
				)
				/*AND
				a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in ('PEN','DER'))
				and datediff(day,getdate(), A.dFecPlazo)>=1 */
				AND A.siPlazo > 0
			)
		)
		)SELECT *,(select count(1) from dataresultadofin)TotalRegistros  FROM  dataresultadofin
	order by OrdenEstado ASC, OrdenPrioridad ASC, RecibidoDate desc
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_Documento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_Documento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_Listar_Documento] (
@pDescripcion VARCHAR(100),
@FechaInicio DATETIME ,
@FechaFin DATETIME 
)
AS
/*

Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3


Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4

*/
BEGIN
	SELECT a.iCodDocumento,a.siEstado AS Estado
	    ,a.siEstDocumento as EstadoDocumento
		,a.siPrioridad AS Prioridad
		,CONVERT(VARCHAR(10), a.dFecRegistro, 103) AS Recibido
		,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
		,a.iCodSecuencial AS Reg
		,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
		,ISNULL(a.vAsunto, '') AS DocumentoAsunto
		,MR.iCodRepresentante AS CodRepresentante
		,mr.vNombres AS Remitente
		,c.vAbreviatura AS Derivado
		,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
		,ma.iCodDocumento as ExisteAdjunto
		,a.vCorrelativo as Correlativo
		,E.iCodEmpresa AS CodEmpresa
		,E.vDescripcion AS NombreEmpresa
		, d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador
		,dbo.UF_MOV_OBTENERNOMJEFE(a.iCodArea) Jefe
		,isnull(estDoc.vCampo,'') vEstadoDescripcion
		,isnull(priori.vCampo,'') vEstadoPrioridad
		,A.vUsuCreacion
	FROM [dbo].[MovDocumento] a
	INNER JOIN [dbo].[MaeTipDocumento] b ON a.iCodTipDocumento = b.iCodTipDocumento
	INNER JOIN [dbo].[MaeArea] c ON a.iCodArea = c.iCodArea
	INNER JOIN [dbo].[MaeTrabajador] d ON a.iCodTrabajador = d.iCodTrabajador
	inner join [dbo].[MaeRepresentante] mr ON a.iCodRepresentante = mr.iCodRepresentante
	INNER JOIN MaeEmpresa E ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
	left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto) ma on a.iCodDocumento = ma.iCodDocumento
	left join ParParametro estDoc on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
	left join ParParametro priori on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	WHERE
		CONCAT(
					a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',isnull(estDoc.vCampo,''),' ',mr.vNombres
			)	
			LIKE '%' + @pDescripcion+ '%' 
    AND 
	a.dFecRegistro between cast(@FechaInicio as datetime)
	and dateadd(ms, -3, (dateadd(day, +1, convert(varchar, cast(@FechaFin as datetime), 101))))
	ORDER BY estDoc.iOrden asc,priori.iOrden ASC,dFecRegistro desc

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_SeguimientoDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_SeguimientoDocumento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_Listar_SeguimientoDocumento]
@iCodDocumento int,
@iCodDocMovimiento int = null
AS
select * from 
(
	--crean el documento
	select a.iCodDocMovimiento,
	a.iCodMovAnterior,
	a.iCodDocDerivacion,
	b.vNombres + ' ' +b.vApePaterno+' '+b.vApeMaterno Colaborador,
	c.vDescripcion Area,
	CONVERT(VARCHAR(10), a.dFecRecepcion, 103) FechaRecepcion,
	'n1' TipoReg,
	'' Comentario
	,a.iEstadoDocumento
	,est.vCampo EstadoDescripcion
	,b.iCodTrabajador ParaCodTrabajador
	,usu.iCodTrabajador DeCodTrabajador
	,usutra.vNombres + ' ' +usutra.vApePaterno+' '+usutra.vApeMaterno ColaboradorCreacion
	,a.iTipoMovimiento
	from MovDocMovimiento a
	inner join MovDocumento doc on a.iCodDocumento=doc.iCodDocumento
	inner join maetrabajador b on a.iCodTrabajador=b.iCodTrabajador
	inner join maearea c on c.icodarea=a.icodarea
	inner join ParParametro est on est.icodgrupo=1 and est.iCodParametro=a.iEstadoDocumento
	left join SegUsuario usu on usu.WEBUSR=a.vUsuCreacion
	left join MaeTrabajador usutra on usutra.iCodTrabajador=usu.iCodTrabajador
	where a.icoddocumento=@iCodDocumento and 
		(
		a.iEstadoDocumento in(1,2)
		or 
		a.iCodDocDerivacion is null and a.iTipoMovimiento in(1)
		)
	and a.siEstado=1
	union 
	select a.iCodDocMovimiento,
	a.iCodMovAnterior,
	a.iCodDocDerivacion,
	case when a.iCodDocDerivacion is null then
			b.vNombres + ' ' +b.vApePaterno+' '+b.vApeMaterno 
		else 
			traDer.vNombres + ' ' +traDer.vApePaterno+' '+traDer.vApeMaterno  end Colaborador,
	case when a.iCodDocDerivacion is null then
			c.vDescripcion
		else 
			areaDer.vDescripcion end As vDescripcion,
	CONVERT(VARCHAR(10), a.dFecRecepcion, 103) FechaRecepcion,
	'n11' TipoReg,
	com.vComentario Comentario
	,case when a.iCodDocDerivacion is null then
			a.iEstadoDocumento 
		else 
			a.siEstadoDerivacion end As iEstadoDocumento
	,case when a.iCodDocDerivacion is null then
			est.vCampo 
		else 
			estDer.vCampo  end As EstadoDescripcion
	,b.iCodTrabajador ParaCodTrabajador
	,usu.iCodTrabajador DeCodTrabajador
	,usutra.vNombres + ' ' +usutra.vApePaterno+' '+usutra.vApeMaterno ColaboradorCreacion
	,a.iTipoMovimiento
	from MovDocMovimiento a
	inner join maetrabajador b on a.iCodTrabajador=b.iCodTrabajador
	inner join maearea c on c.icodarea=a.icodarea	
	inner join ParParametro est on est.icodgrupo=1 and est.iCodParametro=a.iEstadoDocumento
	left join SegUsuario usu on usu.WEBUSR=a.vUsuCreacion
	left join MaeTrabajador usutra on usutra.iCodTrabajador=usu.iCodTrabajador
	left join ParParametro estDer on estDer.icodgrupo=1 and estDer.iCodParametro=a.siEstadoDerivacion
	left join maetrabajador traDer on traDer.iCodTrabajador=a.iCodResponsableDerivacion
	left join maearea areaDer on areaDer.icodarea=a.iCodAreaDerivacion	
	left join MovDocComentario com on com.iCodDocMovimiento= a.iCodDocMovimiento
	where a.icoddocumento=@iCodDocumento and a.iEstadoDocumento in(4,6,7) and a.siEstado=1 
	--and areaDer.iCodArea<> a.iCodArea
	union
	select a.iCodDocMovimiento,
	a.iCodMovAnterior,
	a.iCodDocDerivacion,
	b.vNombres + ' ' +b.vApePaterno+' '+b.vApeMaterno Colaborador,
	c.vDescripcion,
	CONVERT(VARCHAR(10), a.dFecCreacion, 103)+' '+convert(varchar, a.dFecCreacion,108) dFecRecepcion,
	'n2' TipoReg,
	d.vComentario as Comentario
	,a.iEstadoDocumento
	,est.vCampo EstadoDescripcion
	,b.iCodTrabajador ParaCodTrabajador
	,usu.iCodTrabajador DeCodTrabajador
	,usutra.vNombres + ' ' +usutra.vApePaterno+' '+usutra.vApeMaterno ColaboradorCreacion
	,a.iTipoMovimiento
	from MovDocMovimiento a
	inner join maetrabajador b on a.iCodTrabajador=b.iCodTrabajador
	inner join maearea c on c.icodarea=a.icodarea
	left join MovDocComentario d on d.iCodDocMovimiento=a.iCodDocMovimiento
	inner join ParParametro est on est.icodgrupo=1 and est.iCodParametro=a.iEstadoDocumento
	left join SegUsuario usu on usu.WEBUSR=a.vUsuCreacion
	left join MaeTrabajador usutra on usutra.iCodTrabajador=usu.iCodTrabajador
	where a.icoddocumento=@iCodDocumento and a.iEstadoDocumento in (3,5)
	and d.siEstado=1 and a.siEstado=1
) T 
where @iCodDocMovimiento is null or iCodMovAnterior=@iCodDocMovimiento
order by TipoReg asc
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCorrelativoAnxInicial]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerCorrelativoAnxInicial] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_ObtenerCorrelativoAnxInicial]
  @iCodDocumento INT,
  @iTipoArchivo int
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[[UP_MOV_ObtenerCorrelativoAnxInicial]]
-- Objetivo:	Obtener el correlativo para los nombres de los archivos
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN

	select max(iSecuencia)+1  iSecuencia from MovDocAnexo 
	where iCodDocumento=@iCodDocumento
	and iTipoArchivo=@iTipoArchivo 
	and siEstado=1

	SET @onFlagOK=1

 END





GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumento]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT 
			a.[iCodDocumento],
			a.[iCodSecuencial],
			a.[vCorrelativo],
			a.[iAnio],
			a.[siEstado],
			a.[siOrigen],
			a.[vNumDocumento],
			a.[siEstDocumento],
			a.[dFecDocumento],
			a.[dFecRegistro],
			a.[vAsunto],
			a.[siPrioridad],
			a.[vReferencia],
			a.[dFecRecepcion],
			a.[siPlazo],
			a.[dFecPlazo],
			a.[vObservaciones],
			a.[iCodTipDocumento],
			b.vDescripcion as vTipoDocumento,
			a.[iCodRepresentante],
			c.vNombres as vRepresentante,
			a.[iCodArea],
			d.vDescripcion as vArea,
			a.[iCodTrabajador],
			e.vNombres as vNombreTrab,
			e.vApePaterno as vApePaternoTrab,
			e.vApeMaterno as vApeMaternoTrab,
			e.siFirma,
			em.iCodEmpresa,
			em.vDescripcion as vDescripcionEmpresa,
			ADJ.iCodDocAdjunto,
			a.[vUsuCreacion],
			a.[dFecCreacion],
			a.[vHstCreacion],
			a.[vInsCreacion],
			a.[vLgnCreacion],
			a.[vRolCreacion],
			a.[vUsuActualizacion],
			a.[dFecActualizacion],
			a.[vHstActualizacion],
			a.[vInsActualizacion],
			a.[vLgnActualizacion],
			a.[vRolActualizacion],
			(left(convert(varchar, a.[dFecDocumento], 106),6)+'. '+cast(year(a.[dFecDocumento]) as varchar)+' '+right(convert(varchar, a.[dFecDocumento], 100),7))vFecDocumento,
			tre.vNombres+' '+tre.vApePaterno+' '+tre.vApeMaterno vUsuarioRegistro,
			tre.iCodTrabajador AS CodResponsable,
			iCodUltimoMovimiento = (select max(iCodDocMovimiento) from MovDocMovimiento
									where iCodDocumento=@iCodDocumento
									and siEstado=1
									),
			(select isnull(max(icoddocumento),0) from MovDocumentoFirma where iCodDocumento = @iCodDocumento) vDocumentoFirmado									
		FROM [dbo].[MovDocumento] a
		INNER JOIN [dbo].[MaeTipDocumento] b
		ON a.iCodTipDocumento=b.iCodTipDocumento
		LEFT JOIN [dbo].[MaeRepresentante] c
		ON a.iCodRepresentante=c.iCodRepresentante
		INNER JOIN [dbo].[MaeArea] d
		ON a.iCodArea=d.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] e
		ON a.iCodTrabajador=e.iCodTrabajador
		LEFT JOIN [dbo].[MaeEmpresa] em
		on c.iCodEmpresa = em.iCodEmpresa
		LEFT JOIN [SegUsuario] ure on a.vUsuCreacion=ure.WEBUSR and ure.siEstado=1
		inner join [MaeTrabajador] tre on ure.iCodTrabajador=tre.iCodTrabajador
		LEFT JOIN MovDocAdjunto ADJ ON ADJ.iCodDocumento=A.iCodDocumento
		WHERE a.iCodDocumento=@iCodDocumento
END



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosAdjuntos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAdjuntos] AS' 
END
GO


ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAdjuntos]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentosAdjuntos]
-- Objetivo:	Obtener Documentos Adjuntos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Adjuntos
-- Create date: 07/02/2020
-- =============================================
BEGIN
SELECT [iCodDocAdjunto]
      ,[iCodLaserfiche]
      ,[siEstado]
      ,[iCodDocumento]
      ,[vUsuCreacion]
      ,[dFecCreacion]
      ,[vHstCreacion]
      ,[vInsCreacion]
      ,[vLgnCreacion]
      ,[vRolCreacion]
      ,[vUsuActualizacion]
      ,[dFecActualizacion]
      ,[vHstActualizacion]
      ,[vInsActualizacion]
      ,[vLgnActualizacion]
      ,[vRolActualizacion]
  FROM [dbo].[MovDocAdjunto]
  WHERE iCodDocumento=@iCodDocumento
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosAnexos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAnexos] AS' 
END
GO


ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosAnexos]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT ma.[iCodDocAnexo]
      ,ma.[iCodLaserfiche]
      ,ma.[siEstado]
      ,ma.[iCodDocMovimiento]
	  ,ma.[vNombre]
	  ,ma.[iSecuencia]
      ,ma.[vUsuCreacion]
      ,ma.[dFecCreacion]
      ,ma.[vHstCreacion]
      ,ma.[vInsCreacion]
      ,ma.[vLgnCreacion]
      ,ma.[vRolCreacion]
      ,ma.[vUsuActualizacion]
      ,ma.[dFecActualizacion]
      ,ma.[vHstActualizacion]
      ,ma.[vInsActualizacion]
      ,ma.[vLgnActualizacion]
      ,ma.[vRolActualizacion]
	  ,ma.[iTipoArchivo]
  FROM [dbo].[MovDocAnexo] ma
  WHERE ma.iCodDocumento = @iCodDocumento 
  and siEstado=1
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT        
dd.iCodDocumentoDerivacion, 
dd.iCodDocumento, 
dd.siEstado, 
dd.iCodArea, 
dd.iCodResponsable, 
dd.siEstadoDerivacion, 
dd.siTipoAcceso, 
dd.dFecDerivacion, 
dd.vUsuCreacion, 
dd.dFecCreacion, 
dd.vHstCreacion, 
dd.vInsCreacion, 
dd.vLgnCreacion, 
dd.vRolCreacion, 
dd.vUsuActualizacion, 
dd.dFecActualizacion, 
dd.vHstActualizacion, 
dd.vInsActualizacion, 
dd.vLgnActualizacion, 
dd.vRolActualizacion,
ma.vDescripcion as NombreArea,
mt.vNombres+' '+mt.vApePaterno+' '+mt.vApeMaterno as NombreResponsable,
SEG.WEBUSR WEBUSRRESPONSABLE
FROM            
MovDocumentoDerivacion dd
left join MaeArea ma on dd.iCodArea=ma.iCodArea
left join MaeTrabajador mt on dd.iCodResponsable=mt.iCodTrabajador
LEFT JOIN SegUsuario SEG ON MT.iCodTrabajador=SEG.iCodTrabajador
WHERE dd.iCodDocumento = @iCodDocumento 
and  dd.siEstado = 1;
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorId]
(
	 @iCodDocumentoDerivacion INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT        
dd.iCodDocumentoDerivacion, 
dd.iCodDocumento, 
dd.siEstado, 
dd.iCodArea, 
dd.iCodResponsable, 
dd.siEstadoDerivacion, 
dd.siTipoAcceso, 
dd.dFecDerivacion, 
dd.vUsuCreacion, 
dd.dFecCreacion, 
dd.vHstCreacion, 
dd.vInsCreacion, 
dd.vLgnCreacion, 
dd.vRolCreacion, 
dd.vUsuActualizacion, 
dd.dFecActualizacion, 
dd.vHstActualizacion, 
dd.vInsActualizacion, 
dd.vLgnActualizacion, 
dd.vRolActualizacion,
ma.vDescripcion as NombreArea,
mt.vNombres+' '+mt.vApePaterno+' '+mt.vApeMaterno as NombreResponsable,
PAR.vCampo AS TipoAccion,
(SELECT ISNULL(MAX(iCodDocumentoFirma),0) FROM MovDocumentoFirma WHERE iCodDerivacion = @iCodDocumentoDerivacion) AS iCodDocumentoFirma
FROM            
MovDocumentoDerivacion dd
LEFT JOIN MaeArea ma on dd.iCodArea=ma.iCodArea
LEFT JOIN MaeTrabajador mt on dd.iCodResponsable=mt.iCodTrabajador
LEFT JOIN (SELECT * FROM ParParametro where iCodGrupo=8) PAR ON PAR.iCodParametro = DD.iTipoAccion
WHERE dd.iCodDocumentoDerivacion = @iCodDocumentoDerivacion 
END






GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorTrabajador]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorTrabajador] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorTrabajador]
(
	@iCodDocumento INT,
	@iCodTrabajador int
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentosDerivadosDocumento]
-- Objetivo:	Obtener Documentos Derivados x Documento y trabajador
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 10/03/2020
-- =============================================
BEGIN
SELECT '' [iCodDocumento]
      ,'' [siEstado]
      ,'' [iCodArea]
      ,'' [iCodResponsable]
      ,'' [siEstadoDerivacion]
      ,1 [siTipoAcceso]
      ,'' [dFecDerivacion]
	  , 'si' [ResponsableDerivacion]
	  ,mt.[vNombres]
	  ,mt.[vApeMaterno]
	  ,mt.[vApePaterno]
      ,'' [vUsuCreacion]
      ,'' [dFecCreacion]
      ,'' [vHstCreacion]
      ,'' [vInsCreacion]
      ,'' [vLgnCreacion]
      ,'' [vRolCreacion]
      ,'' [vUsuActualizacion]
      ,'' [dFecActualizacion]
      ,'' [vHstActualizacion]
      ,'' [vInsActualizacion]
      ,'' [vLgnActualizacion]
      ,'' [vRolActualizacion]
FROM MovDocumento doc
inner join MaeTrabajador mt on doc.iCodTrabajador = mt.iCodTrabajador
WHERE iCodDocumento = @iCodDocumento  AND doc.iCodTrabajador = @iCodTrabajador
AND DOC.siEstado  =  1

UNION ALL

SELECT        
	  dd.[iCodDocumento]
      ,dd.[siEstado]
      ,dd.[iCodArea]
      ,dd.[iCodResponsable]
      ,dd.[siEstadoDerivacion]
      ,dd.[siTipoAcceso]
      ,dd.[dFecDerivacion]
	  ,'no' [ResponsableDerivacion]
	  ,mt.[vNombres]
	  ,mt.[vApeMaterno]
	  ,mt.[vApePaterno]
      ,dd.[vUsuCreacion]
      ,dd.[dFecCreacion]
      ,dd.[vHstCreacion]
      ,dd.[vInsCreacion]
      ,dd.[vLgnCreacion]
      ,dd.[vRolCreacion]
      ,dd.[vUsuActualizacion]
      ,dd.[dFecActualizacion]
      ,dd.[vHstActualizacion]
      ,dd.[vInsActualizacion]
      ,dd.[vLgnActualizacion]
      ,dd.[vRolActualizacion]
FROM            
MovDocumentoDerivacion dd
inner join MaeTrabajador mt on dd.iCodResponsable = mt.iCodTrabajador
WHERE dd.iCodDocumento = @iCodDocumento AND DD.iCodResponsable = @iCodTrabajador AND   DD.siEstado  = 1

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerUltimoMovimiento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerUltimoMovimiento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerUltimoMovimiento]
  @iCodDocumento INT
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerUltimoMovimiento]
-- Objetivo:	Obtener el id del último movimiento
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN

	select max(iCodDocMovimiento) iCodDocMovimiento from MovDocMovimiento
	where iCodDocumento=@iCodDocumento
	and siEstado=1

	SET @onFlagOK=1

 END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocAdjunto]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocAdjunto] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocAdjunto]
  @iCodLaserfiche  INT = NULL
 ,@iCodDocumento INT 
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocAdjunto]
-- Objetivo:	Registrar Adjuntos a un Documento
-- Módulo:		Movimientos
-- Input:		@iCodLaserfiche - Código de Documento en LaserFiche
--				@iCodDocumento -  Codigo de Documento
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

	INSERT INTO dbo.MovDocAdjunto(iCodLaserfiche,
									iCodDocumento,
									siEstado,									
									vUsuCreacion,
									dFecCreacion,
									vHstCreacion,
									vInsCreacion,
									vLgnCreacion,
									vRolCreacion)
									VALUES(									
									@iCodLaserfiche,
									@iCodDocumento,
									1,									
									@vUsuCreacion,
									GETDATE(),
									@@SERVERNAME,
									@@SERVICENAME,
									@vLgnCreacion,
									@vRolCreacion
									)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocAdjunto) FROM dbo.MovDocAdjunto 
			ELSE
				SET @onFlagOK=0

 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocAnexo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocAnexo] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocAnexo]
  @iCodLaserfiche  INT = NULL
 ,@iCodDocMovimiento INT
 ,@iCodDocumento INT 
 ,@iSecuencia INT 
 ,@iTipoArchivo int=1
 ,@vNombre VARCHAR(50) = NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocAnexo]
-- Objetivo:	Registrar Anexos a un Documento
-- Módulo:		Movimientos
-- Input:		@iCodLaserfiche - Código de Documento en LaserFiche
--				@iCodDocMovimiento -  Codigo de Documento Movimiento
--				@iCodDocumento -  Codigo de Documento
--				@iSecuencia - Secuencial de anexo
--				@vNombre - Nombre Documento
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN

	INSERT INTO dbo.MovDocAnexo(iCodLaserfiche,				
									siEstado,		
									iCodDocMovimiento,
									iCodDocumento,
									vNombre,
									iSecuencia,
									vUsuCreacion,
									dFecCreacion,
									vHstCreacion,
									vInsCreacion,
									vLgnCreacion,
									vRolCreacion,
									iTipoArchivo)
									VALUES(									
									@iCodLaserfiche,
									1,	
									@iCodDocMovimiento,
									@iCodDocumento,
									@vNombre,
									@iSecuencia,
									@vUsuCreacion,
									GETDATE(),
									@@SERVERNAME,
									@@SERVICENAME,
									@vLgnCreacion,
									@vRolCreacion,
									@iTipoArchivo 
									)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocAnexo) FROM dbo.MovDocAnexo 
			ELSE
				SET @onFlagOK=0

 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocComentario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocComentario] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocComentario]
  @vComentario  VARCHAR(1000) = NULL
 ,@iCodDocMovimiento INT = NULL
 ,@iCodDocumento INT
 ,@iCodDocDerivacion INT=NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(100) = NULL
 ,@vInsCreacion VARCHAR(100) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocComentario]
-- Objetivo:	Registrar Comentarios de los Documentos
-- Módulo:		Movimientos
-- Input:		@vComentario - Descripción del Comentario
--				@iCodDocMovimiento -  Codigo de Movimiento
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 07/02/20
-- =============================================
BEGIN

	INSERT INTO dbo.MovDocComentario(vComentario,
									siEstado,
									iCodDocMovimiento,
									iCodDocumento,
									iCodDocumentoDerivacion,
									vUsuCreacion,
									dFecCreacion,
									vHstCreacion,
									vInsCreacion,
									vLgnCreacion,
									vRolCreacion)
									VALUES(									
									@vComentario,
									1,
									@iCodDocMovimiento,
									@iCodDocumento,
									@iCodDocDerivacion,
									@vUsuCreacion,
									GETDATE(),
									@@SERVERNAME,
									@@SERVICENAME,
									@vLgnCreacion,
									@vRolCreacion
									)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocComentario) FROM dbo.MovDocComentario 
			ELSE
				SET @onFlagOK=0

 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocMovimiento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento]
  @iCodDocumento INT
 ,@iCodArea INT
 ,@iCodTrabajador INT
 ,@dFecRecepcion DATETIME = NULL
 ,@dFecLectura DATETIME = NULL
 ,@siPlazo SMALLINT
 ,@dFecPlazo DATETIME = NULL
 ,@siNivel SMALLINT
 ,@siEstFirma INT
 ,@dFecDerivacion DATETIME=NULL
 ,@dFecVisado DATETIME = NULL
 ,@siEstVisado SMALLINT
 ,@dFecFirma DATETIME = NULL
 ,@iCodDocDerivacion int =NULL
 ,@iCodAreaDerivacion int =NULL
 ,@iCodResponsableDerivacion int= NULL
 ,@siEstadoDerivacion int =NULL
 ,@iTipoMovimiento int =NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocMovimiento]
-- Objetivo:	Registrar Movimiento de los Documentos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código del Documento
--				@iCodArea -  Codigo de Área
--				@iCodTrabajador - Código de Trabajador
--				@dFecRecepcion - Fecha de Recepción
--				@dFecLectura - Fecha de Lectura
--				@siPlazo : Indicador de Plazo
--				@dFecPlazo : Fecha de Plazo
--				@siNivel : Indicador de Nivel
--				@siEstFirma: Indicador de Firma
--				@dFecVisado : Fecha de Visado
--				@siEstVisado : Indicador de Estado Visado
--				@dFecFirma : Fecha de Firma
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 07/02/20
-- =============================================
DECLARE @iCodMovAnterior INT;
BEGIN

	SET @iCodMovAnterior = (SELECT MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento WHERE iCodDocumento=@iCodDocumento AND siEstado=1)
	
	declare @estadoDoc int ;
	select @estadoDoc = siEstDocumento
	from MovDocumento
	where iCodDocumento=@iCodDocumento

	INSERT INTO dbo.MovDocMovimiento(siEstado,
									dFecRecepcion,
									dFecLectura,
									iCodMovAnterior,
									siPlazo,
									dFecPlazo,
									siNivel,
									siEstFirma,
									dFecVisado,
									siEstVisado,
									dFecFirma,
									iCodDocumento,
									iCodArea,
									iCodTrabajador,
									iCodDocDerivacion,
									iCodAreaDerivacion,
									iCodResponsableDerivacion,
									siEstadoDerivacion,
									iTipoMovimiento,
									vUsuCreacion,
									dFecCreacion,
									vHstCreacion,
									vInsCreacion,
									vLgnCreacion,
									vRolCreacion,
									dFecDerivacion,
									iEstadoDocumento)
									VALUES(									
									1,
									@dFecRecepcion,
									@dFecLectura,
									@iCodMovAnterior,
									@siPlazo,
									@dFecPlazo,
									@siNivel,
									@siEstFirma,
									@dFecVisado,
									@siEstVisado,
									@dFecFirma,
									@iCodDocumento,
									@iCodArea,
									@iCodTrabajador,
   								    @iCodDocDerivacion,
									@iCodAreaDerivacion,
									@iCodResponsableDerivacion,
									@siEstadoDerivacion,
									@iTipoMovimiento,
									@vUsuCreacion,
									GETDATE(),
									@@SERVERNAME,
									@@SERVICENAME,
									@vLgnCreacion,
									@vRolCreacion,
									@dFecDerivacion,
									@estadoDoc
									)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento 
			ELSE
				SET @onFlagOK=0

 END




GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocMovimientoDerivacion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimientoDerivacion] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimientoDerivacion]
  @iCodDocumento INT
 ,@icodDocDerivacion INT
 ,@iCodArea INT
 ,@iCodTrabajador INT
 ,@siEstadoDerivacionPendiente  INT
 ,@tipoMovimiento INT
 ,@iEstadoDocumento int
 ,@iCodAreaDerivacion  int
 ,@iCodResponsableDerivacion int
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocMovimiento]
-- Objetivo:	Registrar Movimiento de los Documentos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código del Documento
--				@iCodArea -  Codigo de Área
--				@iCodTrabajador - Código de Trabajador
--				@dFecRecepcion - Fecha de Recepción
--				@dFecLectura - Fecha de Lectura
--				@siPlazo : Indicador de Plazo
--				@dFecPlazo : Fecha de Plazo
--				@siNivel : Indicador de Nivel
--				@siEstFirma: Indicador de Firma
--				@dFecVisado : Fecha de Visado
--				@siEstVisado : Indicador de Estado Visado
--				@dFecFirma : Fecha de Firma
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 07/02/20
-- ======================  =======================
BEGIN


		insert into MovDocMovimiento
		(siestado,dFecRecepcion,dFecLectura,iCodMovAnterior,siPlazo,
		dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,iTipoMovimiento,
		iCodDocumento,iCodArea,iCodTrabajador,iEstadoDocumento,iCodDocDerivacion,iCodAreaDerivacion,
		iCodResponsableDerivacion,siEstadoDerivacion, 
		vusucreacion,dfeccreacion,vHstCreacion,vInsCreacion,vLgnCreacion,vRolCreacion)
		
		SELECT 1,dFecRecepcion,dFecLectura,iCodDocMovimiento,siPlazo,
		dFecPlazo,siNivel,siEstFirma,dFecVisado,siEstVisado,dFecFirma,@tipoMovimiento,
		iCodDocumento,iCodArea,ISNULL(@iCodTrabajador,iCodTrabajador),@iEstadoDocumento,@icodDocDerivacion,@iCodAreaDerivacion,
		@iCodResponsableDerivacion,@siEstadoDerivacionPendiente,
		@vUsuCreacion,getdate(), @@SERVERNAME,@@servicename,@vLgnCreacion,@vRolCreacion
		FROM MovDocMovimiento WHERE iCodDocMovimiento = (
 							select min(iCodDocMovimiento) from MovDocMovimiento
 							where iCodDocumento=@iCodDocumento
 							and siEstado=1)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento 
			ELSE
				SET @onFlagOK=0

 END




GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumento]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocumento]
GO

CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumento]
  @iCodSecuencial INT = NULL--2
 ,@vCorrelativo VARCHAR(100)= NULL--3
 ,@iAnio INT = NULL--4
 ,@siEstado SMALLINT --5
 ,@siOrigen SMALLINT = NULL --6
 ,@vNumDocumento VARCHAR(100) = NULL--7
 ,@siEstDocumento SMALLINT = NULL--8
 ,@dFecDocumento DATETIME = NULL--9
 ,@dFecRegistro DATETIME = NULL--10
 ,@dFecDerivacion DATETIME=NULL
 ,@vAsunto VARCHAR(1000) = NULL--11
 ,@siPrioridad SMALLINT = NULL--12
 ,@vReferencia VARCHAR(1000) = NULL--13
 ,@dFecRecepcion DATETIME = NULL--14
 ,@siPlazo SMALLINT = NULL--15
 ,@dFecPlazo DATETIME = NULL--16
 ,@vObservaciones VARCHAR(1000) = NULL--17
 ,@iCodTipDocumento INT --18
 ,@iCodRepresentante INT--19
 ,@iCodArea INT--20
 ,@iCodTrabajador INT--21
 ,@vUsuCreacion VARCHAR(100) = NULL--22
 ,@vHstCreacion VARCHAR(20) = NULL--23
 ,@vInsCreacion VARCHAR(50) = NULL--24
 ,@vLgnCreacion VARCHAR(20) = NULL--25
 ,@vRolCreacion VARCHAR(20) = NULL--26
 ,@onFlagOK INT OUTPUT--27
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocumento]
-- Objetivo:	Registrar un nuevo Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
--				@iCodSecuencial -  Número de Secuencia
--				@iAnio		  - Anio Correlativo
--				@siOrigen	  - Estado del Registro: 1 - Activo; 0 - Inactivo
--				@vNumDocumento	  - Número de Documento
--				@siEstDocumento	  - Estado del Documento
--				@dFecDocumento	  - Fecha del Documento
--				@dFecRegistro	  - Fecha de Registro
--				@vAsunto			- Asunto
--				@siPrioridad		- Indicador de Prioridad
--				@vReferencia		- Referencia
--				@dFecRecepcion	  - Fecha de Recepción
--				@siPlazo			- Indicador de Plazo
--				@dFecPlazo			- Fecha de Plazo
--				@vObservaciones		- Observaciones
--				@iCodTipDocumento	- Codigo Tipo Documento
--				@iCodRepresentante  - Código de Representante
--				@iCodArea			- Código de Área
--				@iCodTrabajador	    - Codigo de Trabajador
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

	

		INSERT INTO dbo.MovDocumento(iCodSecuencial,iAnio,siEstado,
									siOrigen,vNumDocumento,siEstDocumento,
									dFecDocumento,dFecRegistro,vAsunto,
									siPrioridad,vReferencia,dFecRecepcion,
									siPlazo,dFecPlazo,vObservaciones,
									iCodTipDocumento,iCodRepresentante,iCodArea,
									iCodTrabajador,vUsuCreacion,dFecCreacion,
									vHstCreacion,vInsCreacion,vLgnCreacion,
									vRolCreacion,vCorrelativo,dFecDerivacion,siFlagFirmando)
									VALUES(									
									@iCodSecuencial,@iAnio,1,
									@siOrigen,@vNumDocumento,@siEstDocumento,
									@dFecDocumento,getdate(),@vAsunto,
									@siPrioridad,@vReferencia,@dFecRecepcion,
									@siPlazo,@dFecPlazo,@vObservaciones,
									@iCodTipDocumento,@iCodRepresentante,@iCodArea,
									@iCodTrabajador,@vUsuCreacion,GETDATE(),
									@@SERVERNAME,@@servicename,@vLgnCreacion,
									@vRolCreacion,@vCorrelativo,@dFecDerivacion,0
									)




		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocumento) FROM dbo.MovDocumento 
			ELSE
				SET @onFlagOK=0


 END



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumento_qry01]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumento_qry01] AS' 
END
GO
ALTER PROC [dbo].[UP_MOV_RegistrarDocumento_qry01] (@pDescripcion VARCHAR(100))
AS
/*

Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3


Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4

*/
BEGIN
	SELECT a.siEstado AS Estado
		,a.siPrioridad AS Prioridad
		,CONVERT(VARCHAR(10), a.dFecRegistro, 103) AS Recibido
		--,a.iCorrelativo AS Reg
		,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
		,ISNULL(a.vAsunto, '') AS DocumentoAsunto
		,d.vApePaterno + ' ' + d.vApeMaterno + ', ' + d.vNombres AS Remitente
		,c.vAbreviatura AS Derivado
		,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
	FROM [dbo].[MovDocumento] a
	INNER JOIN [dbo].[MaeTipDocumento] b ON a.iCodTipDocumento = b.iCodTipDocumento
	INNER JOIN [dbo].[MaeArea] c ON a.iCodArea = c.iCodArea
	INNER JOIN [dbo].[MaeTrabajador] d ON a.iCodTrabajador = d.iCodTrabajador
	WHERE ISNULL(b.vDescripcion, '') + ' ' + ISNULL(a.vNumDocumento, '') + ' ' + ISNULL(a.vAsunto, '') + ' ' + d.vApePaterno + ' ' + d.vApeMaterno + ' ' + d.vNombres + ' ' + c.vAbreviatura LIKE '%' + @pDescripcion + '%'
    AND  DATEDIFF(DD,a.dFecRegistro,GETDATE())<=30
	ORDER BY a.dFecRegistro DESC

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocumentosDerivados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocumentosDerivados] AS' 
END
GO


ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocumentosDerivados]
(
  @iCodDocumento INT
 ,@iCodArea INT 
 ,@iCodResponsable INT 
 ,@siTipoAcceso SMALLINT
 ,@dFecDerivacion datetime
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
  DECLARE @estadoDerivacion int = NULL
  IF(@siTipoAcceso =  1)
  BEGIN
  SET @estadoDerivacion = 2
  END
  

INSERT INTO MovDocumentoDerivacion
(
 iCodDocumento, 
siEstado, iCodArea,
iCodResponsable, siEstadoDerivacion, 
siTipoAcceso, dFecDerivacion, 
vUsuCreacion, dFecCreacion, 
vHstCreacion, vInsCreacion, 
vLgnCreacion, vRolCreacion
)
VALUES        
(
 @iCodDocumento, 
1, @iCodArea, 
@iCodResponsable, @estadoDerivacion, 
@siTipoAcceso, @dFecDerivacion,
@vUsuCreacion,GETDATE(),
@@SERVERNAME,@@SERVICENAME,
@vLgnCreacion,@vRolCreacion
)

IF @@ROWCOUNT  > 0 
	SELECT @onFlagOK=MAX(iCodDocumentoDerivacion) FROM dbo.MovDocumentoDerivacion 
ELSE
	SET @onFlagOK=0

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ValidarFinalizarDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ValidarFinalizarDocumento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ValidarFinalizarDocumento]
  @iCodDocumento INT,
  @vUsuario varchar(50),
  @vComentario varchar(500)
 ,@onFlagOK INT OUTPUT--
 ,@onMensaje varchar(5000) OUTPUT--
 ,@onCodDocDerivados varchar(5000) OUTPUT--
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_DevolverDocumento]
-- Objetivo:	Devolver un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		declare @valido bit=1;
		
		declare @mensajeAreasRes varchar(MAX)='';
		declare @CodDerivados varchar(MAX)='';

		select 
		@mensajeAreasRes =@mensajeAreasRes+ '- '+tra.vNombres+' '+tra.vApePaterno + '(' +area.vDescripcion +')<br/>',
		@CodDerivados = @CodDerivados + '|' + CAST(der.iCodDocumentoDerivacion AS VARCHAR(10))
		from MovDocumentoDerivacion der
		inner join MaeArea area on area.iCodArea=der.iCodArea
		inner join MaeTrabajador tra on tra.iCodTrabajador=der.iCodResponsable
		where der.iCodDocumento=@iCodDocumento
		and der.siEstadoDerivacion in(select iCodParametro from ParParametro where icodgrupo=1 and vvalor='PEN')
		and der.siTipoAcceso=1
		and der.siEstado=1


		if LEN(@mensajeAreasRes)>0
		begin 
			set @valido=0;
			set @onMensaje=@mensajeAreasRes;
			set @onFlagOK=-2;
			set @onCodDocDerivados = @CodDerivados
		end
		else
		begin
			set @valido=1;
			set @onFlagOK=1;
		end

		SELECT @onFlagOK;
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_Obtener_Usuario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[up_Obtener_Usuario] AS' 
END
GO
ALTER PROCEDURE [dbo].[up_Obtener_Usuario]
(
@pWebusr VARCHAR(20)
)
AS
BEGIN
	SELECT
	[WEBUSR] ,[iCodTrabajador] 
	FROM [dbo].[SegUsuario]
	WHERE
	WEBUSR=@pWebusr 
	AND siEstado=1
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_Obtener_Usuario_qry01]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[up_Obtener_Usuario_qry01] AS' 
END
GO
ALTER PROCEDURE [dbo].[up_Obtener_Usuario_qry01]
(
@pWebusr VARCHAR(20)
)
AS
BEGIN
	SELECT
	[WEBUSR] ,[iCodTrabajador] 
	FROM [dbo].[SegUsuario]
	WHERE
	WEBUSR=@pWebusr 
	AND siEstado=1
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_PAR_Listar_Parametros_x_Grupo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_PAR_Listar_Parametros_x_Grupo] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_PAR_Listar_Parametros_x_Grupo] 
@iCodGrupo AS INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_PAR_Listar_Parametros_x_Grupo]
-- Objetivo:	Listar Parametros por Grupo
-- Módulo:		Maestros
-- Input:		@CodGrupo
-- Output:  
--			Listado de Parámetros por Grupo
-- Create date: 03/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodGrupo,
		a.iCodParametro,
		a.vCampo,
		a.siEstado
	FROM dbo.ParParametro a
	WHERE a.siEstado = 1 and a.iCodGrupo=@iCodGrupo
		  AND a.iCodParametro > 0;
 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[up_SegUsuario_qry01]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[up_SegUsuario_qry01] AS' 
END
GO
ALTER PROCEDURE [dbo].[up_SegUsuario_qry01]
(
@pWebusr VARCHAR(20)
)
AS
BEGIN
	SELECT
	[WEBUSR] ,[iCodTrabajador] 
	FROM [dbo].[SegUsuario]
	WHERE
	WEBUSR=@pWebusr 
	AND siEstado=1
END

GO









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ObtenerFeriado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ObtenerFeriado] AS' 
END
GO
alter procedure UP_MAE_ObtenerFeriado
@iCodFeriado int
as

SELECT        iCodFeriado, dFecha, vDescripcion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, 
                         vLgnActualizacion, vRolActualizacion, iTipoFeriado
FROM            MaeFeriado
where iCodFeriado =@iCodFeriado 

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_BuscarFeriados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_BuscarFeriados] AS' 
END
GO
alter procedure [dbo].[UP_MAE_BuscarFeriados]
@texto varchar(100),
@iPagina int,
@iTamPagina int
as
with data as(
SELECT        
iCodFeriado, dFecha, vDescripcion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, 
vLgnActualizacion, vRolActualizacion, iTipoFeriado,par.vCampo AS TipoFeriadoDescripcion 
FROM            
MaeFeriado mae
INNER  JOIN (SELECT vCampo,vValor FROM  ParParametro where  iCodGrupo=7) par on mae.iTipoFeriado = par.vValor
where 
cast(iTipoFeriado as varchar(100))+''+convert(varchar,dFecha,103)+''+vDescripcion like '%'+@texto+'%'
)
select 
*
,(select count(1) from data)TotalRegistros 
from data
order by iCodFeriado ASC
OFFSET (@iPagina-1)*@iTamPagina ROWS
FETCH NEXT @iTamPagina ROWS ONLY;

go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarFeriado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_RegistrarFeriado] AS' 
END
GO
alter procedure UP_MAE_RegistrarFeriado
@dFecha datetime=NULL,
@vDescripcion varchar(100)=NULL,
@siEstado smallint=NULL,
@iTipoFeriado int=NULL,
@vUsuCreacion varchar(50)=NULL,
@dFecCreacion datetime=NULL,
@vHstCreacion varchar(50)=NULL,
@vInsCreacion varchar(50)=NULL,
@vLgnCreacion varchar(50)=NULL
,@vRolCreacion varchar(100)=NULL,
@vUsuActualizacion varchar(100)=NULL,
@dFecActualizacion datetime=NULL,
@vHstActualizacion varchar(50)=NULL,
@vInsActualizacion varchar(50)=NULL,
@vLgnActualizacion varchar(50)=NULL,
@vRolActualizacion varchar(50)=NULL,
@onFlagOK INT OUTPUT
as

set @vHstCreacion=@@SERVERNAME;
set @vInsCreacion=@@SERVICENAME;

if(select count(*) from MaeFeriado where siEstado=1 and convert(varchar, dFecha,103)=convert(varchar,@dFecha,103))>0
begin
	set @onFlagOK=-2;
	return 
end

INSERT INTO MaeFeriado
(dFecha, 
vDescripcion, siEstado, 
vUsuCreacion, dFecCreacion, 
vHstCreacion, vInsCreacion, 
vLgnCreacion, vRolCreacion,
iTipoFeriado)
VALUES        
(@dFecha,
@vDescripcion,@siEstado,
@vUsuCreacion,getdate(),
@vHstCreacion,@vInsCreacion,
@vLgnCreacion,@vRolCreacion,
@iTipoFeriado)

set @onFlagOK= SCOPE_IDENTITY();

go


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarFeriado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ActualizarFeriado] AS' 
END
GO

alter procedure UP_MAE_ActualizarFeriado
@iCodFeriado int=NULL,
@dFecha datetime=NULL,
@vDescripcion varchar(100)=NULL,
@siEstado smallint=NULL,
@iTipoFeriado int=NULL,
@vUsuCreacion varchar(50)=NULL,
@dFecCreacion datetime=NULL,
@vHstCreacion varchar(50)=NULL,
@vInsCreacion varchar(50)=NULL,
@vLgnCreacion varchar(50)=NULL
,@vRolCreacion varchar(100)=NULL,
@vUsuActualizacion varchar(100)=NULL,
@dFecActualizacion datetime=NULL,
@vHstActualizacion varchar(50)=NULL,
@vInsActualizacion varchar(50)=NULL,
@vLgnActualizacion varchar(50)=NULL,
@vRolActualizacion varchar(50)=NULL
,@onFlagOK INT OUTPUT
as

set @vHstActualizacion=@@SERVERNAME;
set @vInsActualizacion=@@SERVICENAME;

if(select count(*) from MaeFeriado where siEstado=1 
	and convert(varchar, dFecha,103)=convert(varchar,@dFecha,103)
	and iCodFeriado<>@iCodFeriado
	)>0
begin
	set @onFlagOK=-2;
	return 
end

UPDATE       MaeFeriado
SET                dFecha = @dFecha, vDescripcion = @vDescripcion, siEstado = @siEstado, vUsuActualizacion = @vUsuActualizacion, dFecActualizacion = getdate(), vHstActualizacion = @vHstActualizacion, 
                         vInsActualizacion = @vInsActualizacion, vLgnActualizacion = @vLgnActualizacion, vRolActualizacion = @vRolActualizacion, iTipoFeriado = @iTipoFeriado
where iCodFeriado=@iCodFeriado

set @onFlagOK= @iCodFeriado;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_EliminarFeriado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_EliminarFeriado] AS' 
END
GO

alter procedure UP_MAE_EliminarFeriado
@iCodFeriado int
  ,@vUsuActualizacion varchar(50)=NULL 
  ,@vLgnActualizacion varchar(50)=NULL 
 ,@onFlagOK INT OUTPUT
as
update MaeFeriado
set siEstado=0,
vUsuActualizacion = @vUsuActualizacion,
vLgnActualizacion = @vLgnActualizacion,
dFecActualizacion = GETDATE(),
vInsActualizacion =  @@SERVICENAME,
vHstActualizacion = @@SERVERNAME
where iCodFeriado=@iCodFeriado

set @onFlagOK= @iCodFeriado;


go




------------------------------


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ObtenerAsuntoEstandarizado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ObtenerAsuntoEstandarizado] AS' 
END
GO

alter procedure UP_MAE_ObtenerAsuntoEstandarizado
@iCodAsunto int
as
SELECT        iCodAsunto, vDescripcion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, vLgnActualizacion, 
                         vRolActualizacion
FROM            MaeAsunto
where iCodAsunto =@iCodAsunto

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_BuscarAsuntoEstandarizado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_BuscarAsuntoEstandarizado] AS' 
END
GO

alter procedure UP_MAE_BuscarAsuntoEstandarizado
@texto varchar(100),
@iPagina int,
@iTamPagina int
as

with data as(
SELECT        iCodAsunto, vDescripcion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, vLgnActualizacion, 
                         vRolActualizacion
FROM            MaeAsunto
where 
cast(iCodAsunto as varchar(100))+''+convert(varchar,dFecCreacion,103)+''+vDescripcion like '%'+@texto+'%'
)
select 
*
,(select count(1) from data)TotalRegistros 
from data
order by iCodAsunto ASC
OFFSET (@iPagina-1)*@iTamPagina ROWS
FETCH NEXT @iTamPagina ROWS ONLY;
go

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarAsuntoEstandarizado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_RegistrarAsuntoEstandarizado] AS' 
END
GO

alter procedure UP_MAE_RegistrarAsuntoEstandarizado
@vDescripcion varchar(100)=NULL,
@siEstado smallint=NULL,
@vUsuCreacion varchar(50)=NULL,
@dFecCreacion datetime=NULL,
@vHstCreacion varchar(50)=NULL,
@vInsCreacion varchar(50)=NULL,
@vLgnCreacion varchar(50)=NULL,
@vRolCreacion varchar(50)=NULL,
@vUsuActualizacion varchar(50)=NULL,
@dFecActualizacion datetime=NULL,
@vHstActualizacion varchar(50)=NULL,
@vInsActualizacion varchar(50)=NULL,
@vLgnActualizacion varchar(50)=NULL,
@vRolActualizacion varchar(50)=NULL
 ,@onFlagOK INT OUTPUT
as
set @vHstCreacion=@@SERVERNAME;
set @vInsCreacion=@@SERVICENAME;
INSERT INTO MaeAsunto
                         (vDescripcion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion)
VALUES        (@vDescripcion, @siEstado, @vUsuCreacion, getdate(), @vHstCreacion, @vInsCreacion, @vLgnCreacion, @vRolCreacion)

set @onFlagOK= SCOPE_IDENTITY();

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarAsuntoEstandarizado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ActualizarAsuntoEstandarizado] AS' 
END
GO

alter procedure UP_MAE_ActualizarAsuntoEstandarizado
@iCodAsunto int=NULL,
@vDescripcion varchar(100)=NULL,
@siEstado smallint=NULL,
@vUsuCreacion varchar(50)=NULL,
@dFecCreacion datetime=NULL,
@vHstCreacion varchar(50)=NULL,
@vInsCreacion varchar(50)=NULL,
@vLgnCreacion varchar(50)=NULL,
@vRolCreacion varchar(50)=NULL,
@vUsuActualizacion varchar(50)=NULL,
@dFecActualizacion datetime=NULL,
@vHstActualizacion varchar(50)=NULL,
@vInsActualizacion varchar(50)=NULL,
@vLgnActualizacion varchar(50)=NULL,
@vRolActualizacion varchar(50)=NULL
 ,@onFlagOK INT OUTPUT
as
set @vHstActualizacion=@@SERVERNAME;
set @vInsActualizacion=@@SERVICENAME;
UPDATE       MaeAsunto
SET                vDescripcion = @vDescripcion, siEstado = @siEstado, vUsuActualizacion = @vUsuActualizacion, dFecActualizacion = getdate(), vHstActualizacion = @vHstActualizacion, 
                         vInsActualizacion = @vInsActualizacion, vLgnActualizacion = @vLgnActualizacion, vRolActualizacion = @vRolActualizacion
where
iCodAsunto = @iCodAsunto

set @onFlagOK= @iCodAsunto;
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_EliminarAsuntoEstandarizado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_EliminarAsuntoEstandarizado] AS' 
END
GO

ALTER procedure [dbo].[UP_MAE_EliminarAsuntoEstandarizado]
@iCodAsunto int,
 @vUsuActualizacion varchar(50)=NULL 
,@vLgnActualizacion varchar(50)=NULL, 
@onFlagOK int output
as
update MaeAsunto
set siEstado=0,
vUsuActualizacion = @vUsuActualizacion,
vLgnActualizacion = @vLgnActualizacion,
vInsActualizacion =  @@SERVICENAME,
dFecActualizacion = GETDATE(),
vHstActualizacion = @@SERVERNAME
where iCodAsunto=@iCodAsunto

set @onFlagOK =1

go


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ObtenerMaeEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ObtenerMaeEmpresa] AS' 
END
GO

alter procedure UP_MAE_ObtenerMaeEmpresa
@iCodEmpresa int
as
SELECT        iCodEmpresa, vAbreviatura, vDescripcion,vDireccion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, 
                         vLgnActualizacion, vRolActualizacion
FROM            MaeEmpresa
where iCodEmpresa =@iCodEmpresa

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_BuscarMaeEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_BuscarMaeEmpresa] AS' 
END
GO

alter  procedure [dbo].[UP_MAE_BuscarMaeEmpresa]
@texto varchar(100),
@iPagina int,
@iTamPagina int
as
with data as (
SELECT        iCodEmpresa, vAbreviatura, vDescripcion,vDireccion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, 
                         vLgnActualizacion, vRolActualizacion
FROM            MaeEmpresa
WHERE 
cast(iCodEmpresa as varchar(100))+''+convert(varchar,dFecCreacion,103)+''+ISNULL(vAbreviatura,'')+''+vDescripcion like '%'+@texto+'%'
)
select 
*
,(select count(1) from data)TotalRegistros 
from data
order by iCodEmpresa ASC
OFFSET (@iPagina-1)*@iTamPagina ROWS
FETCH NEXT @iTamPagina ROWS ONLY;


go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarMaeEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_RegistrarMaeEmpresa] AS' 
END
GO

alter procedure [dbo].[UP_MAE_RegistrarMaeEmpresa]
	@vAbreviatura varchar(50)=NULL ,
	@vDescripcion varchar(100)=NULL ,
	@vDireccion varchar(200)=NULL ,
	@siEstado smallint=NULL ,
	@vUsuCreacion varchar(50)=NULL ,
	@dFecCreacion datetime=NULL ,
	@vHstCreacion varchar(50)=NULL ,
	@vInsCreacion varchar(50)=NULL ,
	@vLgnCreacion varchar(50)=NULL ,
	@vRolCreacion varchar(50)=NULL ,
	@vUsuActualizacion varchar(50)=NULL ,
	@dFecActualizacion datetime=NULL ,
	@vHstActualizacion varchar(50)=NULL ,
	@vInsActualizacion varchar(50)=NULL ,
	@vLgnActualizacion varchar(50)=NULL ,
	@vRolActualizacion varchar(50)=NULL
	 ,@onFlagOK INT OUTPUT
as
set @vHstCreacion=@@SERVERNAME;
set @vInsCreacion=@@SERVICENAME;
INSERT INTO MaeEmpresa
                         ( vAbreviatura, vDescripcion,vDireccion, siEstado, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, vLgnCreacion, vRolCreacion)
VALUES        (@vAbreviatura, @vDescripcion, @vDireccion,@siEstado, @vUsuCreacion, getdate(), @vHstCreacion, @vInsCreacion, @vLgnCreacion, @vRolCreacion)

set @onFlagOK= SCOPE_IDENTITY();

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarMaeEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ActualizarMaeEmpresa] AS' 
END
GO
alter procedure [dbo].[UP_MAE_ActualizarMaeEmpresa]
	@iCodEmpresa int=NULL,
	@vAbreviatura varchar(50)=NULL ,
	@vDescripcion varchar(100)=NULL ,
	@vDireccion varchar(200)=NULL ,
	@siEstado smallint=NULL ,
	@vUsuCreacion varchar(50)=NULL ,
	@dFecCreacion datetime=NULL ,
	@vHstCreacion varchar(50)=NULL ,
	@vInsCreacion varchar(50)=NULL ,
	@vLgnCreacion varchar(50)=NULL ,
	@vRolCreacion varchar(50)=NULL ,
	@vUsuActualizacion varchar(50)=NULL ,
	@dFecActualizacion datetime=NULL ,
	@vHstActualizacion varchar(50)=NULL ,
	@vInsActualizacion varchar(50)=NULL ,
	@vLgnActualizacion varchar(50)=NULL ,
	@vRolActualizacion varchar(50)=NULL
	,@onFlagOK INT OUTPUT
as
set @vHstActualizacion=@@SERVERNAME;
set @vInsActualizacion=@@SERVICENAME;

UPDATE       MaeEmpresa
SET                vAbreviatura = @vAbreviatura, vDescripcion = @vDescripcion,vDireccion=@vDireccion, siEstado = @siEstado, vUsuActualizacion = @vUsuActualizacion, dFecActualizacion = getdate(), vHstActualizacion = @vHstActualizacion, 
                         vInsActualizacion = @vInsActualizacion, vLgnActualizacion = @vLgnActualizacion, vRolActualizacion = @vRolActualizacion
where
iCodEmpresa = @iCodEmpresa

set @onFlagOK=@iCodEmpresa


GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_EliminarMaeEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_EliminarMaeEmpresa] AS' 
END
GO
ALTER procedure [dbo].[UP_MAE_EliminarMaeEmpresa]
@iCodEmpresa int
  ,@vUsuActualizacion varchar(50)=NULL 
  ,@vLgnActualizacion varchar(50)=NULL 
,@onFlagOK INT OUTPUT
as
update MaeEmpresa
set siEstado=0,
vUsuActualizacion = @vUsuActualizacion,
vLgnActualizacion = @vLgnActualizacion,
dFecActualizacion = GETDATE(),
vInsActualizacion =  @@SERVICENAME,
vHstActualizacion = @@SERVERNAME
where
iCodEmpresa = @iCodEmpresa

set @onFlagOK=@iCodEmpresa
go

-------------------

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ObtenerMaeRepresentantePorNumDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ObtenerMaeRepresentantePorNumDocumento] AS' 
END
GO
alter  procedure [dbo].[UP_MAE_ObtenerMaeRepresentantePorNumDocumento]
@NumDocumento VARCHAR(20)
AS
SELECT  
iCodRepresentante 
FROM      
MaeRepresentante
WHERE vNumDocumento =@NumDocumento

go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ObtenerMaeRepresentante]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ObtenerMaeRepresentante] AS' 
END
GO
alter procedure UP_MAE_ObtenerMaeRepresentante
@iCodRepresentante int
as
SELECT        iCodRepresentante, vNumDocumento, vNombres, siEstado, siTipRepresentante, vDireccion, vTelefono, vCelular, vEmail, iCodEmpresa, iCodTipDocIdentidad,vAnexo, vFax, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, 
                         vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, vLgnActualizacion, vRolActualizacion
FROM            MaeRepresentante
where iCodRepresentante =@iCodRepresentante

go
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_BuscarMaeRepresentante]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_BuscarMaeRepresentante] AS' 
END
GO
alter procedure UP_MAE_BuscarMaeRepresentante
@texto varchar(100),
@iCodEmpresa int,
@iPagina int,
@iTamPagina int
as

with data as (
SELECT        iCodRepresentante, vNumDocumento, vNombres, siEstado, siTipRepresentante, vDireccion, vTelefono, vCelular, vEmail, iCodEmpresa, iCodTipDocIdentidad,vAnexo, vFax, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, 
                         vLgnCreacion, vRolCreacion, vUsuActualizacion, dFecActualizacion, vHstActualizacion, vInsActualizacion, vLgnActualizacion, vRolActualizacion
FROM            MaeRepresentante
WHERE 
cast(iCodRepresentante as varchar(100))+''+convert(varchar,dFecCreacion,103)+''+vNumDocumento+''+vNombres+''+ISNULL(vDireccion,'')+''+ISNULL(vTelefono,'')+''+ISNULL(vCelular,'')+''+ISNULL(vEmail,'') like '%'+@texto+'%'
and iCodEmpresa=@iCodEmpresa

)
select 
*
,(select count(1) from data)TotalRegistros 
from data
order by iCodRepresentante ASC
OFFSET (@iPagina-1)*@iTamPagina ROWS
FETCH NEXT @iTamPagina ROWS ONLY;

go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarMaeRepresentante]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_RegistrarMaeRepresentante] AS' 
END
GO
alter procedure UP_MAE_RegistrarMaeRepresentante
	@vNumDocumento varchar(50)=NULL,
	@vNombres varchar(200)=NULL,
	@siEstado smallint=NULL,
	@siTipRepresentante smallint=NULL,
	@vDireccion varchar(500)=NULL,
	@vTelefono varchar(50)=NULL,
	@vCelular varchar(50)=NULL,
	@vEmail varchar(100)=NULL,
	@iCodEmpresa int=NULL,
	@iCodTipDocIdentidad int=NULL,
	@vAnexo varchar(100)=NULL,
	@vFax varchar(100)=NULL,
	@vUsuCreacion varchar(100)=NULL,
	@dFecCreacion datetime=NULL,
	@vHstCreacion varchar(50)=NULL,
	@vInsCreacion varchar(50)=NULL,
	@vLgnCreacion varchar(50)=NULL,
	@vRolCreacion varchar(50)=NULL,
	@vUsuActualizacion varchar(100)=NULL,
	@dFecActualizacion datetime=NULL,
	@vHstActualizacion varchar(50)=NULL,
	@vInsActualizacion varchar(50)=NULL,
	@vLgnActualizacion varchar(50)=NULL,
	@vRolActualizacion varchar(50)=NULL
	,@onFlagOK INT OUTPUT
as
set @vHstCreacion=@@SERVERNAME;
set @vInsCreacion=@@SERVICENAME;

INSERT INTO MaeRepresentante
                         ( vNumDocumento, vNombres, siEstado, siTipRepresentante, vDireccion, vTelefono, vCelular, vEmail, iCodEmpresa, iCodTipDocIdentidad, vAnexo, vFax, vUsuCreacion, dFecCreacion, vHstCreacion, vInsCreacion, 
                         vLgnCreacion, vRolCreacion)
VALUES        ( @vNumDocumento, @vNombres, @siEstado, @siTipRepresentante, @vDireccion, @vTelefono, @vCelular, @vEmail, @iCodEmpresa, @iCodTipDocIdentidad, @vAnexo, @vFax, @vUsuCreacion, getdate(), 
                         @vHstCreacion, @vInsCreacion, @vLgnCreacion, @vRolCreacion)

set @onFlagOK= SCOPE_IDENTITY();

go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarMaeRepresentante]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_ActualizarMaeRepresentante] AS' 
END
GO
alter procedure UP_MAE_ActualizarMaeRepresentante
	@iCodRepresentante int=NULL,
	@vNumDocumento varchar(50)=NULL,
	@vNombres varchar(200)=NULL,
	@siEstado smallint=NULL,
	@siTipRepresentante smallint=NULL,
	@vDireccion varchar(500)=NULL,
	@vTelefono varchar(50)=NULL,
	@vCelular varchar(50)=NULL,
	@vEmail varchar(100)=NULL,
	@iCodEmpresa int=NULL,
	@iCodTipDocIdentidad int=NULL,
	@vAnexo varchar(100)=NULL,
	@vFax varchar(100)=NULL,
	@vUsuCreacion varchar(100)=NULL,
	@dFecCreacion datetime=NULL,
	@vHstCreacion varchar(50)=NULL,
	@vInsCreacion varchar(50)=NULL,
	@vLgnCreacion varchar(50)=NULL,
	@vRolCreacion varchar(50)=NULL,
	@vUsuActualizacion varchar(100)=NULL,
	@dFecActualizacion datetime=NULL,
	@vHstActualizacion varchar(50)=NULL,
	@vInsActualizacion varchar(50)=NULL,
	@vLgnActualizacion varchar(50)=NULL,
	@vRolActualizacion varchar(50)=NULL
	,@onFlagOK INT OUTPUT
as
set @vHstActualizacion=@@SERVERNAME;
set @vInsActualizacion=@@SERVICENAME;

UPDATE       MaeRepresentante
SET                vNumDocumento = @vNumDocumento, vNombres = @vNombres, siEstado = @siEstado, siTipRepresentante = @siTipRepresentante, vDireccion = @vDireccion, 
                         vTelefono = @vTelefono, vCelular = @vCelular, vEmail = @vEmail, iCodEmpresa = @iCodEmpresa, iCodTipDocIdentidad = @iCodTipDocIdentidad, vAnexo=@vAnexo, vFax=@vFax, vUsuActualizacion = @vUsuActualizacion, dFecActualizacion = getdate(), 
                         vHstActualizacion = @vHstActualizacion, vInsActualizacion = @vInsActualizacion, vLgnActualizacion = @vLgnActualizacion, vRolActualizacion = @vRolActualizacion
where
iCodRepresentante = @iCodRepresentante

set @onFlagOK=@iCodRepresentante

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_EliminarMaeRepresentante]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_EliminarMaeRepresentante] AS' 
END
GO
ALTER procedure [dbo].[UP_MAE_EliminarMaeRepresentante]
@iCodRepresentante int
  ,@vUsuActualizacion varchar(50)=NULL 
  ,@vLgnActualizacion varchar(50)=NULL 
	,@onFlagOK INT OUTPUT
as
update MaeRepresentante
set siEstado=0,
vUsuActualizacion = @vUsuActualizacion,
vLgnActualizacion = @vLgnActualizacion,
dFecActualizacion = GETDATE(),
vInsActualizacion =  @@SERVICENAME,
vHstActualizacion = @@SERVERNAME
where
iCodRepresentante = @iCodRepresentante

set @onFlagOK=@iCodRepresentante
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_TipDocIdentidad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MAE_Listar_TipDocIdentidad] AS' 
END
GO

ALTER PROCEDURE UP_MAE_Listar_TipDocIdentidad 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_TipDocIdentidad]
-- Objetivo:	Listar Tipo Documento Identidad
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Tipos de Documento Identidad
-- Create date: 19/03/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodTipDocIdentidad,
		A.vAbreviatura,
		a.vDescripcion,
		a.siEstado
	FROM dbo.MaeTipDocIdentidad a
	WHERE a.siEstado = 1;
 
 END


GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_PAR_Listar_Parametros_x_Grupo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_PAR_Listar_Parametros_x_Grupo] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_PAR_Listar_Parametros_x_Grupo] 
@iCodGrupo AS INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_PAR_Listar_Parametros_x_Grupo]
-- Objetivo:	Listar Parametros por Grupo
-- Módulo:		Maestros
-- Input:		@CodGrupo
-- Output:  
--			Listado de Parámetros por Grupo
-- Create date: 03/02/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodGrupo,
		a.iCodParametro,
		a.vCampo,
		a.siEstado,
		a.vValor
	FROM dbo.ParParametro a
	WHERE a.siEstado = 1 and a.iCodGrupo=@iCodGrupo
		  AND a.iCodParametro > 0;
 
 END
 GO
 
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_AreasPaginado]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].UP_MAE_Listar_AreasPaginado
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_AreasPaginado]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].UP_MAE_Listar_AreasPaginado
GO



CREATE PROCEDURE [dbo].[UP_MAE_Listar_AreasPaginado] 
 @NumeroPagina INT,
 @tamaniopagina INT,
 @texto VARCHAR(20)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_AreaPaginado]
-- Objetivo:	Listar areas
-- Módulo:		Maestros
-- Input:		@NumeroPagina int , @tamaniopagina int , @texto varchar(20)
-- Output:  
-- Create date: 15/04/20
-- =============================================
BEGIN
		WITH AREA AS (SELECT 
		a.iCodArea,
		a.vAbreviatura,
		a.vDescripcion,
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado
	FROM dbo.MaeArea a
	WHERE  UPPER(vDescripcion) like '%'+ UPPER(@texto) +'%'
	)


SELECT iCodArea
      ,vAbreviatura
      ,vDescripcion
      ,siEstado
      ,vUsuCreacion
      ,dFecCreacion
	  ,vUsuActualizacion
      ,dFecActualizacion
	  ,(SELECT COUNT(1) FROM AREA) TotalRegistros
	  FROM AREA
	  ORDER BY iCodArea
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;

 
 END 
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Obtener_Areas]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Obtener_Areas]
GO

create PROCEDURE [dbo].[UP_MAE_Obtener_Areas] 
@iCodArea  INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Obtener_Areas]
-- Objetivo:	Listar areas por id
-- Módulo:		Maestros
-- Input:		@iCodArea int 
-- Output:  
-- Listado de area por Id
-- Create date: 15/04/20
-- =============================================
BEGIN
	
	SELECT 
		a.iCodArea,
		a.vAbreviatura,
		a.vDescripcion,
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado
	FROM dbo.MaeArea a
	WHERE  iCodArea = @iCodArea
 
 END
 GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarArea]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_RegistrarArea]
GO

CREATE PROCEDURE [dbo].[UP_MAE_RegistrarArea]
   @vAbreviatura VARCHAR(20) = NULL
 ,@vDescripcion VARCHAR(100) = NULL
 ,@Estado INT
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_RegistrarArea]
-- Objetivo:	Registrar area
-- Módulo:		Mantenimientos
-- Input:		@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 16/04/20
-- =============================================
BEGIN

         
    INSERT INTO dbo.MaeArea(     
     vAbreviatura
    ,vDescripcion
    ,siEstado
    ,vUsuCreacion
    ,dFecCreacion
    ,vHstCreacion
	,vInsCreacion
	,vLgnCreacion
	,vRolCreacion
 )
 VALUES(     
     @vAbreviatura
    ,@vDescripcion
    ,@Estado
    ,@vUsuCreacion
    ,GETDATE()
    ,@@SERVERNAME
    ,@@servicename
    ,@vLgnCreacion
    ,@vRolCreacion
 )
  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodArea) FROM [dbo].[MaeArea]

 END
 GO
 
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarArea]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_ActualizarArea]
GO


CREATE PROCEDURE [dbo].[UP_MAE_ActualizarArea]
  @iCodArea INT
 ,@vAbreviatura VARCHAR(20) = NULL
 ,@vDescripcion VARCHAR(100) = NULL
 ,@Estado INT
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vHstActualizacion VARCHAR(20) = NULL
 ,@vInsActualizacion VARCHAR(50) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_ActualizarArea]
-- Objetivo:	Actualizar una area
-- Módulo:		Mantenimientos
-- Input:		@iCodArea - Codigo Area
--				@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuActualizacion - Campo Auditoria Usuario Creación
--				@vHstActualizacion - Campo Auditoría Host
--				@vInsActualizacion - Campo Auditoría Instancia
--				@vLgnActualizacion - Campo Auditoría Login
--				@vRolActualizacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 16/04/20
-- =============================================
BEGIN


    UPDATE dbo.[MaeArea] SET vAbreviatura = @vAbreviatura,vDescripcion= @vDescripcion,
							   vUsuActualizacion = @vUsuActualizacion,
							   siEstado = @Estado,
								dFecActualizacion = GETDATE(),
								vHstActualizacion = @@SERVERNAME,
								vInsCreacion = @@SERVICENAME,
								vLgnActualizacion = @vUsuActualizacion,
								vRolActualizacion = @vRolActualizacion
								WHERE  iCodArea = @iCodArea

  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodArea) FROM dbo.[MaeArea] 

 END
 GO
 
 
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_EliminarArea]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_EliminarArea]
GO


CREATE PROCEDURE [dbo].[UP_MAE_EliminarArea]
  @iCodArea INT
    ,@vUsuActualizacion varchar(50)=NULL 
  ,@vLgnActualizacion varchar(50)=NULL 
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_EliminarArea]
-- Objetivo:	Actualizar un Tipo Documento
-- Módulo:		Mantenimientos
-- Input:		@iCodArea - Codigo area
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 16/04/20
-- =============================================
BEGIN

    UPDATE [dbo].[MaeArea] SET  siEstado = 0,
								vUsuActualizacion = @vUsuActualizacion,
								vLgnActualizacion = @vLgnActualizacion,
								dFecActualizacion = GETDATE(),
								vInsActualizacion =  @@SERVICENAME,
							    vHstActualizacion = @@SERVERNAME
    WHERE [iCodArea] = @iCodArea
	  
    SET @onFlagOK=@iCodArea

 END
 GO
 
 


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Reporte_MesaPartes]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_Reporte_MesaPartes] 
GO

CREATE PROCEDURE [dbo].[UP_Reporte_MesaPartes] 
@vCodArea  VARCHAR(MAX),
@vIdTipoDocumento   VARCHAR(MAX),
@vCodEstadoDocumento   VARCHAR(MAX),
@fFechaInicio datetime,
@fFechaFin datetime,
@NumeroPagina INT,
@tamaniopagina INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_Reporte_MesaPartes] 
-- Objetivo:    Reporte de Mesa de Partes por area, fecha inicio , fecha fin y tipo Documento
-- Módulo:		Reportes
-- Input:		@vCodArea varchar,  @vIdTipoDocumento varchar , @fFechaInicio datetime, @fFechaFin datetime
-- Output:  
-- Reporte Mesa de Partes.
-- Create date: 21/04/20
-- =============================================
BEGIN

WITH REPORTE AS (
SELECT   

MovDoc.vNumDocumento Nro_Documento,
MovDoc.vCorrelativo  Nro_Tramite,
MaeRep.vNombres  + ' ('+MaeEmp.vDescripcion+')' Institucion ,
FORMAT(MovDoc.dFecDerivacion , 'dd-MM-yyyy HH:mm:ss') FechaDerivacion,
MovDoc.[vAsunto] SumillaAsunto,
MaeTrab.vNombres +' '+  MaeTrab.vApePaterno + ' ' + MaeTrab.vApeMaterno AS Responsable ,
 '' FirmaSello, MaArea.iCodArea  CodArea , MaArea.vDescripcion DesArea,
 Par.vCampo EstadoDoc
FROM [dbo].[MovDocumento] MovDoc 
INNER JOIN  DBO.MaeTrabajador MaeTrab  ON MovDoc.iCodTrabajador = MaeTrab.iCodTrabajador
INNER JOIN  [dbo].MaeRepresentante MaeRep ON  MovDoc.iCodRepresentante   =  MaeRep.iCodRepresentante
INNER JOIN  [dbo].[MaeEmpresa] MaeEmp ON  MaeRep.iCodEmpresa = MaeEmp.iCodEmpresa
inner join  [dbo].MaeArea MaArea on  MovDoc.iCodArea = MaArea.iCodArea 
INNER JOIN  [dbo].[ParParametro] Par on MovDoc.siEstDocumento = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE MovDoc.siEstado = 1 
AND MovDoc.siOrigen = 2
AND MovDoc.dFecDerivacion IS NOT NULL
AND ( MovDoc.iCodArea in (SELECT * FROM dbo.Split(@vCodArea, ','))    OR @vCodArea= '0') 
AND ( MovDoc.iCodTipDocumento in (SELECT * FROM  dbo.Split(@vIdTipoDocumento, ','))  OR @vIdTipoDocumento= '0' ) 
AND ( MovDoc.siEstDocumento in (SELECT * FROM  dbo.Split(@vCodEstadoDocumento, ','))  OR @vCodEstadoDocumento= '0' ) 
AND ( CONVERT(VARCHAR, MovDoc.dFecDerivacion, 112)  BETWEEN CONVERT(VARCHAR,convert(datetime, @fFechaInicio),112)  AND CONVERT(VARCHAR, CONVERT(DATETIME,  @fFechaFin) , 112) )
)


SELECT Nro_Documento
      ,Nro_Tramite
      ,Institucion
      ,FechaDerivacion
      ,SumillaAsunto
      ,Responsable
	  ,FirmaSello
	  ,CodArea
	  ,DesArea
	  ,EstadoDoc
	  ,(SELECT COUNT(*) FROM REPORTE) TotalRegistros
	  FROM REPORTE
	  ORDER BY FechaDerivacion
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
	   

END

 GO
 
 

 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_TrabajadorPaginado]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_TrabajadorPaginado]
GO

CREATE  PROCEDURE [dbo].[UP_MAE_Listar_TrabajadorPaginado] 
 @NumeroPagina INT,
 @tamaniopagina INT,
 @texto VARCHAR(20)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_AreaPaginado]
-- Objetivo:	Listar Trabajador
-- Módulo:		Maestros
-- Input:		@NumeroPagina int , @tamaniopagina int , @texto varchar(20)
-- Output:  
-- Create date: 15/04/20
-- =============================================
BEGIN
		WITH TRABAJADOR AS (SELECT 
		a.iCodTrabajador,
		a.vNumDocumento,
		a.vNombres + ' ' + a.vApePaterno + ' ' + a.vApeMaterno as Nombres,
		a.vCargo,
		ar.vDescripcion vArea,
		td.vDescripcion vTipoDocumento,
		a.vEmail,
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado,
		a.siEsJefe,
		a.siFirma,
		isnull(SEG.WEBUSR,'')  webUsr
	FROM dbo.[MaeTrabajador] a INNER JOIN dbo.MaeArea ar ON ar.iCodArea = a.iCodArea
	INNER JOIN dbo.MaeTipDocIdentidad td ON td.[iCodTipDocIdentidad] = a.[iCodTipDocIdentidad]
	LEFT JOIN [dbo].[SegUsuario] seg ON seg.iCodTrabajador = A.iCodTrabajador
	WHERE  UPPER(a.vNombres + ' ' + a.vApePaterno + ' ' + a.vApeMaterno) like '%'+ UPPER(@texto) +'%'
	or (UPPER(ar.vDescripcion) like '%'+ UPPER(@texto) +'%')
	or (UPPER(a.vNumDocumento) like '%'+ UPPER(@texto) +'%')
	)
	
SELECT 
		iCodTrabajador,
		vNumDocumento,
		Nombres,
		vCargo,
		vArea,
		vTipoDocumento,
		vEmail,
		dFecCreacion,
		vUsuCreacion,
		dFecActualizacion,
		vUsuActualizacion,
		siEstado,
		siEsJefe,
		siFirma,
		webUsr,
		(SELECT COUNT(1) 	FROM TRABAJADOR )TotalRegistros	  
	  FROM TRABAJADOR
	  ORDER BY iCodTrabajador
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
	  
 END 

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Obtener_Trabajador_WebUsr]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Obtener_Trabajador_WebUsr]
GO


create PROCEDURE [dbo].[UP_MAE_Obtener_Trabajador_WebUsr] 
@webusr  VARCHAR(20)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Obtener_Trabajador]
-- Objetivo:	Listar trabajador por Id
-- Módulo:		Maestros
-- Input:		@iCodTrabajador int 
-- Output:  
-- Listado de trabajador por Id
-- Create date: 04/05/2020
-- =============================================
BEGIN
	
	 SELECT 
		a.iCodTrabajador,
		a.vNumDocumento,
		a.vNombres + ' ' + a.vApePaterno + ' ' + a.vApeMaterno as Nombres,
		a.vNombres as Nombre ,
		a.vApePaterno ,
		a.vApeMaterno,
		a.vCargo,
		ar.vDescripcion vArea,
		ar.iCodArea,
		td.vDescripcion vTipoDocumento,
		a.[vEmail],
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado,
		a.vUbicacion,
		a.iAnexo,
		a.iCodArea,
		a.iCodTipDocIdentidad,
		a.vCelular,
		a.siEsJefe,
		a.vApePaterno + ' ' + a.vApeMaterno  ApeCompletos, 
		isnull(seg.WEBUSR,'')  webUsr
		FROM dbo.[MaeTrabajador] a INNER JOIN dbo.MaeArea ar ON ar.iCodArea = a.iCodArea
	INNER JOIN dbo.MaeTipDocIdentidad td ON td.[iCodTipDocIdentidad] = a.[iCodTipDocIdentidad]
	LEFT JOIN [dbo].[SegUsuario] seg ON seg.iCodTrabajador = A.iCodTrabajador
	where seg.WEBUSR = @webusr 
 
 END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Obtener_Trabajador]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Obtener_Trabajador]
GO

CREATE PROCEDURE [dbo].[UP_MAE_Obtener_Trabajador] 
@iCodTrabajador  INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Obtener_Trabajador]
-- Objetivo:	Listar trabajador por Id
-- Módulo:		Maestros
-- Input:		@iCodTrabajador int 
-- Output:  
-- Listado de trabajador por Id
-- Create date: 04/05/2020
-- =============================================
BEGIN
	
	 SELECT 
		a.iCodTrabajador,
		a.vNumDocumento,
		a.vNombres + ' ' + a.vApePaterno + ' ' + a.vApeMaterno as Nombres,
		a.vNombres as Nombre ,
		a.vApePaterno ,
		a.vApeMaterno,
		a.vCargo,
		ar.vDescripcion vArea,
		td.vDescripcion vTipoDocumento,
		a.[vEmail],
		a.dFecCreacion,
		a.vUsuCreacion,
		a.dFecActualizacion,
		a.vUsuActualizacion,
		a.siEstado,
		a.vUbicacion,
		a.iAnexo,
		a.iCodArea,
		a.iCodTipDocIdentidad,
		a.vCelular,
		a.siEsJefe,
		a.siFirma,
		a.vApePaterno + ' ' + a.vApeMaterno  ApeCompletos, 
		isnull(seg.WEBUSR,'')  webUsr
		FROM dbo.[MaeTrabajador] a INNER JOIN dbo.MaeArea ar ON ar.iCodArea = a.iCodArea
	INNER JOIN dbo.MaeTipDocIdentidad td ON td.[iCodTipDocIdentidad] = a.[iCodTipDocIdentidad]
	LEFT JOIN [dbo].[SegUsuario] seg ON seg.iCodTrabajador = A.iCodTrabajador
	where a.iCodTrabajador = @iCodTrabajador
 
 END

 GO


 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_RegistrarTrabajador]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_RegistrarTrabajador]
GO


CREATE PROCEDURE [dbo].[UP_MAE_RegistrarTrabajador]
@vNumDocumento VARCHAR(20) = NULL
,@vNombres VARCHAR(50) = NULL
,@vApePaterno VARCHAR(20) = NULL
,@vApeMaterno VARCHAR(20) = NULL
,@vCargo VARCHAR(50) = NULL
,@vUbicacion VARCHAR(20) = NULL
,@iAnexo INT 
,@vEmail VARCHAR(50) = NULL
,@Estado INT
,@Jefe INT
,@Firma INT
,@iCodArea INT
,@iCodTipoDocIdentidad INT
,@vCelular VARCHAR(20)
,@usvWebUsr VARCHAR(20) 
,@vUsuCreacion VARCHAR(100) = NULL
,@vHstCreacion VARCHAR(20) = NULL
,@vLgnCreacion VARCHAR(20) = NULL
,@vRolCreacion VARCHAR(20) = NULL
,@iCodPerfil INT = NULL
,@vPerfil varchar(20) = NULL
,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_RegistrarTrabajador]
-- Objetivo:	Registrar Trabajador
-- Módulo:		Mantenimientos
-- Input        @vNumDocumento - Numero de documento
--              @vNombres -- Nombre
---             @vApePaterno -- apellido paterno
---             @vApeMaterno  -- apellido materno
--              @vCargo -- Cargo
---             @vUbicacion = ubicacion
---             @iAnexo = anexo
--              @vEmail  -- Email
--              @Estado -- Estado
--              @Jefe -- Identificador de Jefe.
--              @iCodArea  -- Codigo de area
---             @iCodTipoDocIdentidad -- codigo de tipo documento
--              @vCelular -- numero de celular
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 04/05/2020
-- =============================================
BEGIN

	
		INSERT INTO [dbo].[MaeTrabajador](         
		vNumDocumento,
		vNombres ,
		vApePaterno ,
		vApeMaterno ,
		vCargo,
		vUbicacion   , 
		iAnexo   ,
		vEmail ,
		siEstado  ,
		siEsJefe , 
		siFirma,
		iCodArea ,
		iCodTipDocIdentidad,
		vCelular , 
		vUsuCreacion,
		dFecCreacion,
		vHstCreacion
		,vInsCreacion
		,vLgnCreacion
		,vRolCreacion
		 )
		 VALUES(         
		@vNumDocumento,
		@vNombres,
		@vApePaterno,
		@vApeMaterno,
		@vCargo,
		@vUbicacion , 
		@iAnexo ,
		@vEmail,
		@Estado,
		@Jefe, 
		@Firma,
		@iCodArea,
		@iCodTipoDocIdentidad,
		@vCelular     
		,@vUsuCreacion,
		 GETDATE(),
		@@SERVERNAME,
		@@servicename,
		@vLgnCreacion,
		@vRolCreacion
		 )
		
	   IF @@ROWCOUNT  > 0 
			 SELECT @onFlagOK=MAX(iCodTrabajador) FROM [dbo].[MaeTrabajador]

			 declare @Cod int
			 set @Cod = (select max( isnull([iCodUsuario],0)) + 1 from  [dbo].[SegUsuario] )

			 
			INSERT INTO [dbo].[SegUsuario]
           ([iCodUsuario]
           ,[iCodPerfil]
           ,[dFecAcceso]
           ,[siEstado]
           ,[iCodTrabajador]
            ,[WEBUSR]
           ,[vCodPerfil]
           ,[vUsuCreacion]
           ,[dFecCreacion]
           ,[vHstCreacion]
           ,[vInsCreacion]
               
           )		   
			VALUES(         
			@Cod,
			1,
			GETDATE(),
			1,
			@onFlagOK,
			upper(LTRIM(@usvWebUsr)),
			'ADMIN',
			@vUsuCreacion ,
			GETDATE(),
			@@SERVERNAME,
			@@servicename
		
			)
	
 END

 GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ActualizarTrabajador]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_ActualizarTrabajador]
GO


CREATE  PROCEDURE [dbo].[UP_MAE_ActualizarTrabajador]
@iCodTrabajador INT
,@vNumDocumento VARCHAR(20) = NULL
,@vNombres VARCHAR(50) = NULL
,@vApePaterno VARCHAR(20) = NULL
,@vApeMaterno VARCHAR(20) = NULL
,@vCargo VARCHAR(50) = NULL
,@vUbicacion VARCHAR(20) = NULL
,@iAnexo INT 
,@vEmail VARCHAR(50) = NULL
,@Estado INT
,@Jefe INT
,@Firma INT
,@iCodArea INT
,@iCodTipoDocIdentidad INT
,@vCelular VARCHAR(20)
,@vUsuActualizacion VARCHAR(100) = NULL
,@vLgnActualizacion VARCHAR(20) = NULL
,@vRolActualizacion VARCHAR(20) = NULL
,@iCodPerfil INT = NULL
,@vPerfil varchar(20) = NULL
,@vWebUsr VARCHAR(20) = NULL
,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_ActualizarTrabajador]
-- Objetivo:	Actualizar un trabajador
-- Módulo:		Mantenimientos
-- Input:		@@iCodTrabajador - Codigo trabajador
--              @vNumDocumento - Numero de documento
--              @vNombres -- Nombre
---             @vApePaterno -- apellido paterno
---             @vApeMaterno  -- apellido materno
--              @vCargo -- Cargo
---             @vUbicacion = ubicacion
---             @iAnexo = anexo
--              @vEmail  -- Email
--              @Estado -- Estado
--              @Jefe -- Identificador de Jefe.
--              @iCodArea  -- Codigo de area
---             @iCodTipoDocIdentidad -- codigo de tipo documento
--              @vCelular -- numero de celular
--				@vAbreviatura - Campo Abreviatura
--				@vDescripcion - Campo Descripción
--				@vUsuActualizacion - Campo Auditoria Usuario Creación
--				@vHstActualizacion - Campo Auditoría Host
--				@vInsActualizacion - Campo Auditoría Instancia
--				@vLgnActualizacion - Campo Auditoría Login
--				@vRolActualizacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 04/05/2020
-- =============================================
BEGIN


    UPDATE dbo.[MaeTrabajador] 
	SET
	            vNumDocumento= @vNumDocumento,
				vNombres= @vNombres,
				vApePaterno= @vApePaterno,
				vApeMaterno= @vApeMaterno,
				vCargo= @vCargo,
				vUbicacion = @vUbicacion , 
				iAnexo = @iAnexo ,
				vEmail= @vEmail,
				siEstado = @Estado,
				siEsJefe= @Jefe, 
				siFirma = @Firma,
				iCodArea= @iCodArea,
				iCodTipDocIdentidad= @iCodTipoDocIdentidad,
				vCelular = @vCelular ,
				vUsuActualizacion = @vUsuActualizacion,
				dFecActualizacion = GETDATE(),
				vHstActualizacion = @@SERVERNAME,
				vInsActualizacion= @@SERVICENAME,
				vLgnActualizacion = @vUsuActualizacion,
				vRolActualizacion = @vRolActualizacion				
				WHERE  iCodTrabajador = @iCodTrabajador

  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX([iCodTrabajador]) FROM dbo.[MaeTrabajador] 
	 	 
UPDATE [dbo].[SegUsuario]
   SET     
		[iCodPerfil]= 1      
		,[siEstado] = @Estado      
		,[WEBUSR] = UPPER(LTRIM(@vWebUsr))
		,[vCodPerfil] = 'ADMIN'  
		,[vUsuActualizacion] = @vUsuActualizacion
		,[dFecActualizacion] = GETDATE()
		,[vHstActualizacion] =  @@SERVERNAME
		,[vInsActualizacion] =  @@SERVICENAME
		,[vLgnActualizacion] = @vUsuActualizacion
		,[vRolActualizacion] = @vRolActualizacion
	WHERE  iCodTrabajador = @iCodTrabajador
	
 END

 GO
 

 
 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_EliminarTrabajador]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE  [dbo].[UP_MAE_EliminarTrabajador]
GO


CREATE PROCEDURE [dbo].[UP_MAE_EliminarTrabajador]
  @iCodTrabajador INT,
  @vUsuActualizacion varchar(50)=NULL ,
  @vLgnActualizacion varchar(50)=NULL 
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_EliminarTrabajador]
-- Objetivo:	elimina trabajador 
-- Módulo:		Mantenimientos
-- Input:		@iCodTrabajador - Codigo trabajador
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 04/05/2020
-- =============================================
BEGIN


UPDATE dbo.[MaeTrabajador] SET  siEstado = 0,
								vUsuActualizacion = @vUsuActualizacion,
								vLgnActualizacion = @vLgnActualizacion,
								 dFecActualizacion = GETDATE(),
								vInsActualizacion =  @@SERVICENAME,
							    vHstActualizacion = @@SERVERNAME
WHERE iCodTrabajador = @iCodTrabajador
	  

UPDATE [dbo].[SegUsuario]
   SET  vUsuActualizacion = @vUsuActualizacion,
		vLgnActualizacion = @vLgnActualizacion,
		 dFecActualizacion = GETDATE(),
		vInsActualizacion =  @@SERVICENAME,
		vHstActualizacion = @@SERVERNAME, 
		[siEstado] = 0
   WHERE  iCodTrabajador = @iCodTrabajador
	
    SET @onFlagOK=@iCodTrabajador

 END
 GO


 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_ListarTipDocIdentitdad]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE  [dbo].[UP_MAE_ListarTipDocIdentitdad]
GO

CREATE PROCEDURE [dbo].[UP_MAE_ListarTipDocIdentitdad]
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Area]
-- Objetivo:	Listar Tipo Documento Identidad
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Tipo Documentos Identidad	
-- Create date: 05/05/2020
-- =============================================
BEGIN
	
	SELECT 
		a.iCodTipDocIdentidad,
		a.vDescripcion,
		a.siEstado
	FROM [dbo].[MaeTipDocIdentidad] a
	WHERE a.siEstado = 1;
 
 END
GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Mae_ObtenerJefePorArea]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_Mae_ObtenerJefePorArea]
GO


CREATE PROCEDURE [dbo].[UP_Mae_ObtenerJefePorArea]
  @iCodArea INT ,
  @iOperador INT ,
  @iCodTrabajador INT = NULL ,
  @iEstado INT = NULL
 
AS
-- =============================================
-- Nombre:	[dbo].[UP_Mae_ObtenerJefePorArea]
-- Objetivo:	Obtener si existe jefe por area
-- Módulo:		Movimientos
-- Input:		@iCodArea - Código de Area 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 09/05/2020
-- =============================================
BEGIN

IF (@iOperador = 1)
BEGIN

  SELECT count(1) Cantidad FROM [dbo].[MaeTrabajador] WHERE  siEsJefe = 1 AND  siEstado=1 AND  iCodArea= @iCodArea  

END

IF (@iOperador = 2 AND @iEstado = 1)
BEGIN

  SELECT count(1) Cantidad FROM [dbo].[MaeTrabajador] WHERE iCodTrabajador <>  @iCodTrabajador AND siEsJefe = 1 AND   siEstado=@iEstado AND  iCodArea= @iCodArea  

END 

END
 GO

 
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Mae_ObtenerWebUserDuplicado]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_Mae_ObtenerWebUserDuplicado]
GO


CREATE PROCEDURE [dbo].[UP_Mae_ObtenerWebUserDuplicado]
  @vWebUsr varchar(20),
  @iOperador INT ,
  @iCodTrabajador INT = NULL 
AS
-- =============================================
-- Nombre:	[dbo].[UP_Mae_ObtenerJefePorArea]
-- Objetivo:	Obtener webuser duplicados
-- Módulo:		Movimientos
-- Input:		@iCodArea - Código de Area 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 09/05/2020
-- =============================================
BEGIN

IF(@iOperador=1)
 BEGIN
  select count(1) Cantidad from [dbo].[SegUsuario] where [siEstado] =1 AND  [WEBUSR]= @vWebUsr  
END 
IF(@iOperador=2)
 BEGIN
  select count(1) Cantidad from [dbo].[SegUsuario] where [iCodTrabajador] <> @iCodTrabajador AND siEstado=1 AND  [WEBUSR]= @vWebUsr  
END 

END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerEstadoDocumentoMovimiento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerEstadoDocumentoMovimiento]
GO



CREATE PROCEDURE [dbo].[UP_MOV_ObtenerEstadoDocumentoMovimiento]
  @iCodDocumento INT,
  @iOperador INT

AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerEstadoDocumentoMovimiento]
-- Objetivo:	Obtener el estado del documento
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 12/05/2020
-- =============================================
BEGIN

IF @iOperador = 1
BEGIN

    SELECT TOP  1 a.siEstDocumento as iEstadoDocumento , 
	( select SEGU.iCodTrabajador from [dbo].[SegUsuario] SEGU where  a.vUsuCreacion = SEGU.WEBUSR) iCodTrabajador
	, MOVD.iCodTrabajador as iCodTrabajadorInter  ,  a.siOrigen
	,a.vCorrelativo    
	 FROM  MovDocMovimiento MOVD INNER JOIN [dbo].[SegUsuario] SEGU
	ON  MOVD.vUsuCreacion = SEGU.WEBUSR	INNER JOIN  [MovDocumento] a  ON MOVD.iCodDocumento= a.iCodDocumento
	INNER JOIN [dbo].[MaeTrabajador] TRAB ON   MOVD.iCodTrabajador = TRAB.iCodTrabajador	
	WHERE  MOVD.iCodDocumento = @iCodDocumento 	and MOVD.siEstado=1
	ORDER BY 1 DESC 

	END
 END
 GO



 
 
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDetalleCorreoAvance]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoAvance]
GO

CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoAvance]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDetalleCorreoAvance]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 13/05/2020
-- =============================================
BEGIN
		SELECT	  a.iCodDocumento,
			a.siPlazo,
			a.dFecPlazo,
			a.siPrioridad,
			a.siOrigen,

			CASE WHEN A.siOrigen = 2  THEN R.vNombres
				 WHEN A.siOrigen = 1 THEN (SELECT TOP 1  TJEFE.vApePaterno + ' '+ TJEFE.vApeMaterno + ' '  + TJEFE.vNombres
											FROM MaeTrabajador TJEFE WHERE iCodArea = ARE.iCodArea AND siEsJefe = 1)   END AS REPRESENTANTE,

			CASE WHEN A.siOrigen = 2  THEN  E.vDescripcion
				 WHEN A.siOrigen = 1 THEN ARE.vDescripcion END AS EMPRESA,
			T.vApePaterno + ' '+ T.vApeMaterno + ' '  + t.vNombres AS DERIVADOPOR
		FROM [dbo].[MovDocumento] a
		
		LEFT JOIN MaeRepresentante r on a.iCodRepresentante = r.iCodRepresentante
		INNER JOIN SegUsuario S ON S.WEBUSR = a.vUsuCreacion
		INNER JOIN MaeTrabajador T ON T.iCodTrabajador = S.iCodTrabajador
		LEFT JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		LEFT JOIN MaeArea ARE ON T.iCodArea = ARE.iCodArea
		where a.iCodDocumento=@iCodDocumento
		ORDER BY  a.dFecCreacion DESC
END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Reporte_AreaFechaEstado]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_Reporte_AreaFechaEstado] 
GO


CREATE PROCEDURE [dbo].[UP_Reporte_AreaFechaEstado] 
@vCodArea  VARCHAR(MAX),
@vIdTipoDocumento   VARCHAR(MAX),
@vCodEstadoDocumento   VARCHAR(MAX),
@fFechaInicio datetime,
@fFechaFin datetime,
@NumeroPagina INT,
@tamaniopagina INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_Reporte_MesaPartes] 
-- Objetivo:    Reporte de Mesa de Partes por area, fecha inicio , fecha fin y tipo Documento
-- Módulo:		Reportes
-- Input:		@vCodArea varchar,  @vIdTipoDocumento varchar , @fFechaInicio datetime, @fFechaFin datetime
-- Output:  
-- Reporte Mesa de Partes.
-- Create date: 15/05/20
-- =============================================
BEGIN

WITH REPORTE AS (
SELECT   
MovDoc.vCorrelativo  Nro_Tramite,
FORMAT(MovDoc.dFecDocumento , 'dd-MM-yyyy HH:mm:ss') FechaDocumento,
MovDoc.[vAsunto] Asunto,
MaeTrab.vNombres +' '+  MaeTrab.vApePaterno + ' ' + MaeTrab.vApeMaterno AS Responsable ,
 '' FirmaSello, MaArea.iCodArea  CodArea , MaArea.vDescripcion DesArea,
 Par.vCampo EstadoDoc
FROM [dbo].[MovDocumento] MovDoc 
INNER JOIN  DBO.MaeTrabajador MaeTrab  ON MovDoc.iCodTrabajador = MaeTrab.iCodTrabajador
inner join  [dbo].MaeArea MaArea on  MovDoc.iCodArea = MaArea.iCodArea 
INNER JOIN  [dbo].[ParParametro] Par on MovDoc.siEstDocumento = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE MovDoc.siEstado = 1 
AND MovDoc.siOrigen = 1
AND ( MovDoc.iCodArea in (SELECT * FROM dbo.Split(@vCodArea, ','))    OR @vCodArea= '0') 
AND ( MovDoc.iCodTipDocumento in (SELECT * FROM  dbo.Split(@vIdTipoDocumento, ','))  OR @vIdTipoDocumento= '0' ) 
AND ( MovDoc.siEstDocumento in (SELECT * FROM  dbo.Split(@vCodEstadoDocumento, ','))  OR @vCodEstadoDocumento= '0' ) 
AND ( CONVERT(VARCHAR, MovDoc.dFecDocumento, 112)  BETWEEN CONVERT(VARCHAR,convert(datetime, @fFechaInicio),112)  AND CONVERT(VARCHAR, CONVERT(DATETIME,  @fFechaFin) , 112) )
)

SELECT 
       Nro_Tramite 
	  ,FechaDocumento   
      ,Asunto
      ,Responsable
	  ,FirmaSello
	  ,CodArea
	  ,DesArea
	  ,EstadoDoc
	  ,(SELECT COUNT(*) FROM REPORTE) TotalRegistros
	  FROM REPORTE
	  ORDER BY Nro_Tramite ,DesArea
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
	   

END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_ListaAños]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_ListaAños] 
GO


CREATE PROCEDURE [dbo].[UP_ListaAños] 
AS

-- =============================================
-- Nombre:	[dbo].[UP_Reporte_SeguimientoDocumentos] 
-- Objetivo:   Listar los años  
-- Módulo:		Reportes
-- Input:		
-- Output:   
-- Reporte Mesa de Partes.
-- Create date: 19/05/20
-- =============================================

BEGIN

SELECT  DISTINCT  DATEPART(YEAR, DFECDOCUMENTO) AÑO FROM  [dbo].[MovDocumento] MovDoc  

END 
GO




IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Reporte_SeguimientoDocumentos]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_Reporte_SeguimientoDocumentos] 
GO


CREATE PROCEDURE [dbo].[UP_Reporte_SeguimientoDocumentos] 
@vNroTramite VARCHAR(100),
@vAño CHAR(4),
@vCodArea  VARCHAR(MAX),
@vIdTipoDocumento   VARCHAR(MAX),
@NumeroPagina INT,
@tamaniopagina INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_Reporte_SeguimientoDocumentos] 
-- Objetivo:    Reporte de documento de entrada para seguimiento 
-- Módulo:		Reportes
-- Input:		@vNroTramite varchar,@vAño CHAR, @vCodArea VARCHAR,   @vIdTipoDocumento varchar , @NumeroPagina INT , @tamaniopagina INT
-- Output:   
-- Reporte Mesa de Partes.
-- Create date: 19/05/20
-- =============================================
BEGIN

WITH REPORTE AS (

SELECT   
--MovDoc.iCodDocumento ,
--MovDoc.siOrigen ,
MovDoc.vCorrelativo  Nro_Tramite,
FORMAT(MovDoc.dFecDocumento , 'dd-MM-yyyy HH:mm:ss') FechaDocumento,
(CASE MovDoc.siOrigen WHEN 1 THEN 
(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea )
WHEN 2 THEN 
(SELECT ISNULL( ME.vDescripcion,'') + ' - ' + ISNULL( UPPER(MR.vNombres) ,'') 
FROM [dbo].[MaeRepresentante] MR  
INNER JOIN  MaeEmpresa ME ON MR.iCodEmpresa = ME.iCodEmpresa 
WHERE MR.iCodRepresentante = MovDoc.iCodRepresentante ) 
ELSE '' END) Remitente,


(CASE MovDoc.siOrigen WHEN 1 THEN   
''
 
WHEN 2 THEN 
(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea ) + ' - ' + 
(SELECT UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno)
FROM [dbo].[MaeTrabajador] MTR  WHERE MTR.iCodTrabajador = MovDoc.iCodTrabajador)

ELSE '' END) Destinatario,

MovDoc.[vAsunto] Asunto,
MovDoc.vObservaciones Observacion,

--- EXTERNO : 2 , INTERNO : 1
 Par.vCampo EstadoDoc,
 MovDoc.siEstDocumento  IdEstadoDoc
FROM [dbo].[MovDocumento] MovDoc 
INNER JOIN  DBO.MaeTrabajador MaeTrab  ON MovDoc.iCodTrabajador = MaeTrab.iCodTrabajador
INNER JOIN  [dbo].MaeArea MaArea on  MovDoc.iCodArea = MaArea.iCodArea 
INNER JOIN  [dbo].[ParParametro] Par on MovDoc.siEstDocumento = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE MovDoc.siEstado = 1 
AND ( MovDoc.iCodArea in (SELECT * FROM dbo.Split(@vCodArea, ','))    OR @vCodArea= '0') 
AND ( MovDoc.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( MovDoc.iCodTipDocumento in (SELECT * FROM  dbo.Split(@vIdTipoDocumento, ','))  OR @vIdTipoDocumento= '0' ) 
AND ( DATEPART(YEAR,MovDoc.dFecDocumento) = @vAño OR @vAño= '0' ) 


UNION ALL

SELECT   
--MovDoc.iCodDocumento ,
--MovDoc.siOrigen ,
MovDoc.vCorrelativo  Nro_Tramite,
FORMAT(MovDoc.dFecDocumento , 'dd-MM-yyyy HH:mm:ss') FechaDocumento,
(CASE MovDoc.siOrigen WHEN 1 THEN 
(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea )
WHEN 2 THEN 

(SELECT  UPPER(MAE.vDescripcion) +' - '+ UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno) 
 FROM  
MaeArea MAE 
INNER JOIN [dbo].[MaeTrabajador] MTR ON MTR.iCodTrabajador  = MovDer.iCodResponsable 
WHERE  MAE.iCodArea = MovDer.iCodArea  )

ELSE '' END) Remitente,

(CASE MovDoc.siOrigen WHEN 1 THEN   

(SELECT  UPPER(MAE.vDescripcion) +' - '+ UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno) 
 FROM  
MaeArea MAE 
INNER JOIN [dbo].[MaeTrabajador] MTR ON MTR.iCodTrabajador  = MovDer.iCodResponsable 
WHERE  MAE.iCodArea = MovDer.iCodArea  )

WHEN 2 THEN 

(SELECT ISNULL(UPPER(vDescripcion),'')  FROM [dbo].[MaeArea]
WHERE iCodArea = MovDoc.iCodArea ) + ' - ' + 
(SELECT UPPER( MTR.vNombres) + ' ' + UPPER(MTR.vApePaterno) + ' ' + UPPER(MTR.vApeMaterno)
FROM [dbo].[MaeTrabajador] MTR  WHERE MTR.iCodTrabajador = MovDoc.iCodTrabajador)
ELSE '' END) Destinatario,

MovDoc.[vAsunto] Asunto,
MovDoc.vObservaciones Observacion,

--- EXTERNO : 2 , INTERNO : 1
 Par.vCampo EstadoDoc,
 MovDer.siEstadoDerivacion   IdEstadoDoc
FROM [dbo].[MovDocumento] MovDoc 
INNER JOIN  [dbo].MovDocumentoDerivacion MovDer ON MovDoc.iCodDocumento = MovDer.iCodDocumento AND MovDoc.dFecDerivacion IS NOT NULL
AND MovDer.siTipoAcceso= 1 /*AND MovDer.iTipoAccion IS NOT NULL*/
INNER JOIN  DBO.MaeTrabajador MaeTrab  ON MovDoc.iCodTrabajador = MaeTrab.iCodTrabajador
INNER JOIN  [dbo].MaeArea MaArea on  MovDoc.iCodArea = MaArea.iCodArea 
INNER JOIN  [dbo].[ParParametro] Par on MovDer.siEstadoDerivacion = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE MovDoc.siEstado = 1 AND MovDer.siEstado= 1
AND ( MovDoc.iCodArea in (SELECT * FROM dbo.Split(@vCodArea, ','))    OR @vCodArea= '0') 
AND ( MovDoc.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( MovDoc.iCodTipDocumento in (SELECT * FROM  dbo.Split(@vIdTipoDocumento, ','))  OR @vIdTipoDocumento= '0' ) 
AND ( DATEPART(YEAR,MovDoc.dFecDocumento) = @vAño OR @vAño= '0' ) 
)

SELECT 
     --iCodDocumento
     --,siOrigen
	 Nro_Tramite 
	,FechaDocumento     
	,Remitente
	,Destinatario
	,Asunto
	,Observacion
	,EstadoDoc	
	,IdEstadoDoc  
	  ,(SELECT COUNT(*) FROM REPORTE) TotalRegistros
	  FROM REPORTE
	   ORDER BY  Nro_Tramite,Remitente
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only;;
	   

END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MAE_Listar_Email_PorAreaJefe]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MAE_Listar_Email_PorAreaJefe] 
GO


CREATE PROCEDURE [dbo].[UP_MAE_Listar_Email_PorAreaJefe]
@iCodArea AS INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MAE_Listar_Empresa]
-- Objetivo:	Listar Empresas
-- Módulo:		Maestros
-- Input:		
-- Output:  
--			Listado de Empresas	
-- Create date: 04/02/20
-- =============================================
DECLARE @CodTrabajador  INT
BEGIN
	
	SELECT @CodTrabajador = iCodTrabajador FROM MaeTrabajador WHERE iCodArea = @iCodArea AND siEsJefe = 1;
	

	SELECT 
	vEmail FROM MaeTrabajador WHERE iCodTrabajador = @CodTrabajador
	and siEstado = 1
 END

GO
 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarCodLaserficheAdjuntos]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ActualizarCodLaserficheAdjuntos]
GO


CREATE  PROCEDURE [dbo].[UP_MOV_ActualizarCodLaserficheAdjuntos]
  @codLaserFicheOld INT 
 ,@codLaserFicheNew  INT 
 ,@vUsuActualizacion VARCHAR(100) = NULL
 ,@vLgnActualizacion VARCHAR(20) = NULL
 ,@vRolActualizacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ActualizarCodLaserficheAdjuntos]
-- Objetivo:	Registrar Adjuntos a un Documento
-- Módulo:		Movimientos
-- Input:		@iCodLaserfiche - Código de Documento en LaserFiche
--				@iCodDocumento -  Codigo de Documento
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN
	
	SET @onFlagOK = 0;


	UPDATE dbo.MovDocAdjunto
	SET iCodLaserfiche = @codLaserFicheNew,
		vUsuActualizacion = @vUsuActualizacion,
		dFecActualizacion = GETDATE(),
		vHstActualizacion = @@SERVERNAME,
		vInsCreacion = @@SERVICENAME,
		vRolActualizacion = @vRolActualizacion,
		vLgnActualizacion = @vLgnActualizacion	
	WHERE iCodLaserfiche = @codLaserFicheOld

	SET @onFlagOK = 1;

 END

GO



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCorrelativoPorIdLaserfiche]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerCorrelativoPorIdLaserfiche]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ObtenerCorrelativoPorIdLaserfiche]
  @IdLaserfiche INT ,
  @vCorrelativo VARCHAR(50) OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[[[UP_MOV_ObtenerIdLaserfichePorCorrelativo]]]
-- Objetivo:	Obtener el Id LaserFiche por Correlativo
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN
DECLARE @codDocumento INT

	SELECT @vCorrelativo = DOC.vCorrelativo FROM MovDocumento DOC
	INNER JOIN MovDocAdjunto  ADJ  ON DOC.iCodDocumento = ADJ.iCodDocumento
	WHERE ADJ.iCodLaserfiche = @IdLaserfiche

 END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]
GO


CREATE  PROCEDURE [dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]
  @IdLaserfiche INT,
  @CodDocumento INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]
-- Objetivo:	Obtener el Id de documento por codigo Laserfiche
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN
	
	SELECT @CodDocumento = iCodDocumento
	FROM MovDocAdjunto ADJ
	WHERE ADJ.iCodLaserfiche = @IdLaserfiche



 END

GO 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocFirma]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocFirma]
GO


CREATE  PROCEDURE [dbo].[UP_MOV_RegistrarDocFirma]
  @CodDocumento INT 
  ,@CodAdjunto INT
  ,@CodLaserFiche INT 
  ,@CodDerivacion INT = NULL
  ,@CodTrabajador INT
  ,@CodArea INT 
  ,@Posx INT 
  ,@Posy INT 
  ,@Comentario  VARCHAR(1000) = NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocFirma]
-- Objetivo:	Registrar un Documento Firma
-- Módulo:		Mantenimientos
-- Input:		
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host[CodArea][CodDerivacion]
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 27/01/20
-- =============================================
BEGIN

    INSERT INTO dbo.MovDocumentoFirma(     
    iCodDocumento
	,iCodAdjunto
    ,iCodLaserFiche
    ,iCodDerivacion
    ,iCodTrabajador
    ,iCodArea
    ,dFechaFirma
	,vComentario
	,siEstado
	,iPosx
	,iPosy
	,vUsuCreacion
	,dFecCreacion
	,vHstCreacion
	,vInsCreacion
	,vLgnCreacion
	,vRolCreacion

 )
 VALUES(     
   @CodDocumento
	,@CodAdjunto
    ,@CodLaserFiche
    ,@CodDerivacion
    ,@CodTrabajador
    ,@CodArea
    ,GETDATE()
	,@Comentario
	,1
	,@Posx
	,@Posy
	,@vUsuCreacion
	,GETDATE()
	,@@servername
	,@@servicename
	,@vLgnCreacion
	,@vRolCreacion
 )
  IF @@ROWCOUNT  > 0 
     SELECT @onFlagOK=MAX(iCodDocumentoFirma) FROM dbo.MovDocumentoFirma 

 END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ValidarFirmante]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ValidarFirmante]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ValidarFirmante]
  @iCodDocumento INT,
  @iCodTrabajador varchar(50),
  @onFirma INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ValidarFirmante]
-- Objetivo:	Valida si un trabajador es firmante
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		SELECT @onFirma=COUNT(1) FROM MOVDOCUMENTOFIRMA
		WHERE ICODDOCUMENTO = @iCodDocumento
		AND ICODTRABAJADOR = @iCodTrabajador
END
GO 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ActualizarEstadoFirmaDocumento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ActualizarEstadoFirmaDocumento]
GO


CREATE  PROCEDURE [dbo].[UP_MOV_ActualizarEstadoFirmaDocumento]
  @CodDocumento INT,
  @Atencion INT,
  @onRpta INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[[UP_MOV_ActualizarEstadoFirmaDocumento]]
-- Objetivo:	Actualiza el estado en el proceso de Firma Digital de un documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Input:		@Atencion - Código de Atención
-- Output		@onRpta	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		Update MovDocumento SET siFlagFirmando = @Atencion
		WHERE iCodDocumento = @CodDocumento

		SET @onRpta = 1
END



GO 


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerEstadoFirmaDocumento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerEstadoFirmaDocumento]
GO


CREATE  PROCEDURE [dbo].[UP_MOV_ObtenerEstadoFirmaDocumento]
  @CodDocumento INT,
  @onRpta INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[[UP_MOV_ObtenerEstadoFirmaDocumento]]
-- Objetivo:	Obtener el flag estado de firma documento
-- Módulo:		documento
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onRpta	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
		SELECT @onRpta =siFlagFirmando FROM MovDocumento
		WHERE iCodDocumento=@CodDocumento;
END


GO 


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCantidadFirmasDocumento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerCantidadFirmasDocumento]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ObtenerCantidadFirmasDocumento]
  @CodDocumento INT,
  @onCantFirma INT OUTPUT,
  @onPosx INT OUTPUT,
  @onPosy INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerCantidadFirmasDocumento]
-- Objetivo:	Obtener el contador de firmas digitales que tiene un documento
-- Módulo:		documento
-- Input:		@iCodDocumento - Código de Documento
-- Output		@onRpta	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN
DECLARE @codDocFima INT

		SELECT @onCantFirma =COUNT(1) FROM MovDocumentoFirma
		WHERE iCodDocumento=@CodDocumento;

		SELECT @codDocFima=max(ISNULL(iCodDocumentoFirma,0)) from MovDocumentoFirma 
		WHERE iCodDocumento = @CodDocumento;
		
		if(@codDocFima <> null or @codDocFima <> '')
		BEGIN
		SELECT @onPosx=ISNULL(iPosx,0) FROM MovDocumentoFirma
		WHERE iCodDocumentoFirma = ISNULL(@codDocFima,0);

		SELECT @onPosy=ISNULL(iPosy,0) FROM MovDocumentoFirma
		WHERE iCodDocumentoFirma =ISNULL(@codDocFima,0);
		END ELSE BEGIN

			SET @onPosx =0;
			SET @onPosy =0;
		END
END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerCodLaserFichePorCodDocumento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerCodLaserFichePorCodDocumento]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ObtenerCodLaserFichePorCodDocumento]
  @CodDocumento INT,
  @IdLaserfiche INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerCodDocumentoPorCodLaserFIche]
-- Objetivo:	Obtener el Id de documento por codigo Laserfiche
-- Módulo:		Movimientos
-- Input:		@@iCodDocumento - Código de Documento 
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 19/02/2020
-- =============================================
BEGIN
	
	SELECT @IdLaserfiche = iCodLaserfiche 
	FROM MovDocAdjunto ADJ
	WHERE ADJ.ICodDocumento = @CodDocumento



 END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
(
	 @iCodDocumento INT ,
	 @siTipoAcceso INT
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
-- Objetivo:	Obtener Documentos DocumentosDerivadosPorCodDocumento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Input:		@siTipoAcceso - Tipo Acceso
-- Output		Entidad Documentos Derivados
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT        
dd.iCodDocumentoDerivacion, 
dd.iCodDocumento, 
dd.siEstado, 
dd.iCodArea, 
dd.iCodResponsable, 
dd.siEstadoDerivacion, 
dd.siTipoAcceso, 
dd.dFecDerivacion, 
dd.vUsuCreacion, 
dd.dFecCreacion, 
dd.vHstCreacion, 
dd.vInsCreacion, 
dd.vLgnCreacion, 
dd.vRolCreacion, 
dd.vUsuActualizacion, 
dd.dFecActualizacion, 
dd.vHstActualizacion, 
dd.vInsActualizacion, 
dd.vLgnActualizacion, 
dd.vRolActualizacion,
DD.iTipoAccion
FROM            
MovDocumentoDerivacion dd
WHERE dd.iCodDocumento = @iCodDocumento 
AND DD.siTipoAcceso = @siTipoAcceso
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_EliminarDocumentoDerivacion]') AND TYPE in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_MOV_EliminarDocumentoDerivacion]
GO


CREATE PROCEDURE [dbo].[UP_MOV_EliminarDocumentoDerivacion]
  @iCodDocumentoDerivacion INT,
  @vUsuario varchar(50)
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_CambiarEstadoDocumentoDerivacion]
-- Objetivo:	Cambio Estado Documento Derivación
-- Módulo:		Movimientos
-- Input:		@iCodDocumentoDerivacion - Código de Documento derivación
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 03/03/20
-- =============================================
BEGIN

		UPDATE MovDocumentoDerivacion
		set 		
		siEstado=0,
		dFecActualizacion=getdate(),
		vUsuActualizacion=@vUsuario
		WHERE iCodDocumentoDerivacion = @iCodDocumentoDerivacion AND  siTipoAcceso = 1;


		
		SELECT @onFlagOK=@iCodDocumentoDerivacion
		

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerPrimerCodMovimientoDocumento]') AND TYPE in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_MOV_ObtenerPrimerCodMovimientoDocumento]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ObtenerPrimerCodMovimientoDocumento]
( 
	@CodDocumento INT,
	@EstadoDocumento INT,
	@CodDocMovimiento INT OUTPUT
)
AS
BEGIN
	
	SELECT @CodDocMovimiento = MIN(iCodDocMovimiento)
	FROM MovDocMovimiento MO
	WHERE MO.iCodDocumento = @CodDocumento
		AND (@EstadoDocumento IS NOT NULL AND MO.iEstadoDocumento = @EstadoDocumento)

END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_Reporte_AtencionDocumentos]') AND TYPE in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UP_Reporte_AtencionDocumentos]
GO


CREATE PROCEDURE [dbo].[UP_Reporte_AtencionDocumentos] 
@vNroTramite  VARCHAR(MAX),
@fFechaInicio datetime,
@fFechaFin datetime,
@vCodAreaResponsable  VARCHAR(MAX),
@vCodAreaDerivada  VARCHAR(MAX),
@vCodTipoDocumento  VARCHAR(MAX),
@vEstadoDocumento VARCHAR(MAX),
@siFirmado VARCHAR(MAX),
@NumeroPagina INT,
@tamaniopagina INT
AS
-- =============================================
-- Nombre:	[dbo].[UP_Reporte_AtencionDocumentos] 
-- Objetivo:    Reporte de Atencion de Documentos
-- Módulo:		Reportes
-- Input:		@vCodArea varchar,  @vIdTipoDocumento varchar , @fFechaInicio datetime, @fFechaFin datetime
-- Output:  
--  Reporte de Atencion de Documentos
-- Create date: 15/05/20
-- =============================================
BEGIN

WITH REPORTE AS (

--DOCUMENTO
SELECT 
	DOC.vCorrelativo,
	CASE WHEN REP.iCodRepresentante IS NULL THEN AREA.vDescripcion + ' - ' + TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres ELSE
		REP.vNombres + ' - ' + EMP.vDescripcion END Remitente,
	CASE WHEN DOC.siOrigen  = 1 THEN ''ELSE AREA.vDescripcion + ' - ' + TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres END Destinatario,
	DOC.vAsunto,
	FIRM.vComentario,
	FIRM.iCodDocumentoFirma,
	FIRM.dFechaFirma,
	DOC.siEstDocumento,
	par.vCampo DescripcionEstado
FROM MovDocumento DOC 
INNER JOIN MaeArea AREA ON DOC.iCodArea = AREA.iCodArea
LEFT JOIN MaeRepresentante REP ON DOC.iCodRepresentante = REP.iCodRepresentante
LEFT JOIN MaeEmpresa EMP ON REP.iCodEmpresa=EMP.iCodEmpresa
INNER JOIN MaeTrabajador TRA ON DOC.iCodTrabajador = TRA.iCodTrabajador
LEFT JOIN MovDocumentoFirma FIRM ON DOC.iCodDocumento = FIRM.iCodDocumento AND FIRM.iCodDerivacion IS NULL
INNER JOIN  [dbo].[ParParametro] Par on DOC.siEstDocumento = Par.iCodParametro AND Par.iCodGrupo= 1
WHERE ( DOC.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( DOC.iCodArea IN (SELECT * FROM dbo.Split(@vCodAreaResponsable, ',')) OR 'TODOS' = @vCodAreaResponsable)
AND ( DOC.iCodTipDocumento IN (SELECT * FROM dbo.Split(@vCodTipoDocumento, ',')) OR 'TODOS' = @vCodTipoDocumento)
AND ( DOC.siEstDocumento IN (SELECT * FROM dbo.Split(@vEstadoDocumento, ',')) OR 'TODOS' = @vEstadoDocumento)
AND ((CASE WHEN FIRM.iCodDocumentoFirma IS NULL  THEN '0' 
		 WHEN FIRM.iCodDocumentoFirma IS NOT NULL THEN '1' 
		END ) = @siFirmado OR  'TODOS' = @siFirmado)
AND dFecDocumento >= cast(@fFechaInicio AS DATE)
AND dFecDocumento < cast(DATEADD(DAY, 1, @fFechaFin) AS DATE)
UNION ALL
--DERIVACIONES
SELECT 
	DOC.vCorrelativo,
	AREA.vDescripcion + ' - ' + TRADOC.vApePaterno + ' ' + TRADOC.vApeMaterno + ' ' + TRADOC.vNombres  Remitente,
	TRADER.vDescripcion +' - '+TRA.vApePaterno + ' ' + TRA.vApeMaterno + ' ' + TRA.vNombres Destinatario,
	DOC.vAsunto,
	FIRM.vComentario,
	FIRM.iCodDocumentoFirma,
	FIRM.dFechaFirma,
	DER.siEstadoDerivacion,
	par.vCampo DescripcionEstado
FROM MovDocumentoDerivacion DER
INNER JOIN MovDocumento DOC ON DER.iCodDocumento = DOC.iCodDocumento
INNER JOIN MaeArea AREA ON DOC.iCodArea = AREA.iCodArea
INNER JOIN MaeTrabajador TRA ON DER.iCodResponsable = TRA.iCodTrabajador
LEFT JOIN MovDocumentoFirma FIRM ON DER.iCodDocumentoDerivacion = FIRM.iCodDerivacion 
INNER JOIN  [dbo].[ParParametro] Par on DER.siEstadoDerivacion = Par.iCodParametro AND Par.iCodGrupo= 1
INNER JOIN MaeTrabajador TRADOC ON DOC.iCodTrabajador = TRADOC.iCodTrabajador
INNER JOIN MAEAREA TRADER ON TRA.iCodArea = TRADER.iCodArea
WHERE ( DOC.vCorrelativo like '%'+  @vNroTramite  + '%'    )
AND ( DOC.iCodArea IN (SELECT * FROM dbo.Split(@vCodAreaResponsable, ',')) OR 'TODOS' = @vCodAreaResponsable)
AND ( DER.iCodArea IN (SELECT * FROM dbo.Split(@vCodAreaDerivada, ',')) OR 'TODOS' = @vCodAreaDerivada)
AND ( DOC.iCodTipDocumento IN (SELECT * FROM dbo.Split(@vCodTipoDocumento, ',')) OR 'TODOS' = @vCodTipoDocumento)
AND ( DER.siEstadoDerivacion IN (SELECT * FROM dbo.Split(@vEstadoDocumento, ',')) OR 'TODOS' = @vEstadoDocumento)
AND dFecDocumento >= cast(@fFechaInicio AS DATE)
AND dFecDocumento < cast(DATEADD(DAY, 1, @fFechaFin) AS DATE)
AND ((CASE WHEN FIRM.iCodDocumentoFirma IS NULL  THEN '0'
		 WHEN FIRM.iCodDocumentoFirma IS NOT NULL THEN '1' END) = @siFirmado OR 'TODOS' = @siFirmado)
)

SELECT 
      *,(SELECT COUNT(*) FROM REPORTE) TotalRegistros FROM REPORTE
	  ORDER BY 1,2,3
	  offset (@NumeroPagina-1) * @tamaniopagina ROWS FETCH NEXT @tamaniopagina ROWS only; 
END



GO