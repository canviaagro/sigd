USE [DBAgrSIGD]
GO


INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
           (13, 0, '', NULL, NULL, 'FORMATO DOCUMENTO PARA SIED',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (13, 1, 0, NULL, NULL, 'ORIGINAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (13, 2, 1, NULL, NULL, 'COPIA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)  
GO


INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
		   (2, 3, 'INS', NULL, NULL, 'INTERNO SIED',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (2, 4, 'EXTS', NULL, NULL, 'EXTERNO SIED',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)  
GO

INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
		   (14, 0, '', NULL, NULL, 'TIPO DOCUMENTO SIED EXTERNO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (14, 1, '01', 'OFIE', NULL, 'OFICIO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (14, 2, '02', 'CARE', NULL, 'CARTA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),  
		   (15, 0, '', NULL, NULL, 'TIPO DOCUMENTO SIED INTERNO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (15, 1, '1', 'OFII', NULL, 'OFICIO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (15, 2, '2', 'OFCI', NULL, 'OFICIO CIRCULAR',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER) ,
		   (15, 3, '3', 'HDEI', NULL, 'HOJA DE ENVÌO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
GO


INSERT INTO [dbo].[ParParametro] ([iCodGrupo], [iCodParametro], [vValor], [vValor2],[iOrden], [vCampo],[siEstado],[vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion]
           , [vLgnCreacion], [vRolCreacion])
     VALUES
		   (16, 0, '', NULL, NULL, 'ASUNTOS SIED',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 1, '1', NULL, NULL, 'ESTADOS FINANCIEROS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 3, '3', NULL, NULL, 'NULIDAD',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),  
		   (16, 4, '4', NULL, NULL, 'ACTAS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 5, '5', NULL, NULL, 'ACUERDOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 6, '6', NULL, NULL, 'APELACIONES / NOTIFICACIONES ',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 7, '7', NULL, NULL, 'CESES / PENSIONISTAS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 8, '8', NULL, NULL, 'DESIGNACIONES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 9, '9', NULL, NULL, 'CIRCULARES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 10, '10', NULL, NULL, 'CONTRATOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 11, '11', NULL, NULL, 'CONVENIO LAUDO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 12, '12', NULL, NULL, 'CURRICULOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 13, '13', NULL, NULL, 'DENUNCIA / MEMORIAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 14, '14', NULL, NULL, 'DONACIONES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 16, '16', NULL, NULL, 'FACTURAS / RECIBOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 17, '17', NULL, NULL, 'INVITACIONES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 18, '18', NULL, NULL, 'MANUALES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 19, '19', NULL, NULL, 'MEMORIAS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 21, '21', NULL, NULL, 'PLAN ANUAL',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 22, '22', NULL, NULL, 'PRESUPUESTO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 23, '23', NULL, NULL, 'RESOLUCIONES',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 24, '24', NULL, NULL, 'SOLICITUD',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 25, '25', NULL, NULL, 'VOUCHER',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 27, '27', NULL, NULL, 'CARTAS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 28, '28', NULL, NULL, 'INFORMES DE AUDITORIA',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 29, '29', NULL, NULL, 'OTROS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 30, '30', NULL, NULL, 'OFICIOS',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER),
		   (16, 31, '31', NULL, NULL, 'HOJA DE ENVIO',1,SYSTEM_USER,GETDATE(), @@servername, @@servicename, SYSTEM_USER, USER)
 GO
 


SET IDENTITY_INSERT [dbo].[MaeTipDocIdentidad] ON 
GO
  IF NOT EXISTS (SELECT * FROM [MaeTipDocIdentidad] 
                   WHERE [iCodTipDocIdentidad] = 2)
   BEGIN
INSERT [dbo].[MaeTipDocIdentidad] ([iCodTipDocIdentidad], [vAbreviatura], [vDescripcion], [siEstado], [vUsuCreacion], [dFecCreacion], [vHstCreacion], [vInsCreacion], [vLgnCreacion], [vRolCreacion], [vUsuActualizacion], [dFecActualizacion], [vHstActualizacion]) 
VALUES (2, N'RUC', N'RUC', 1, N'ADMIN', GETDATE(), N'10.0.0.1', N'DBAGRSIGD', N'mguere', 'Administrador',  NULL, NULL, NULL)
END
GO
SET IDENTITY_INSERT [dbo].[MaeTipDocIdentidad] OFF
GO



