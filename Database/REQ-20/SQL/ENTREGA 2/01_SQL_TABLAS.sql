USE [DBAgrSIGD]
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeUsuarioSIED]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeUsuarioSIED](
	[iCodUsuarioSIED] [bigint] IDENTITY(1,1) NOT NULL,
	[iCodTrabajador] bigint not null,
	[WEBUSR] [varchar](20) NOT NULL,
	[vNombre] [varchar](50) NOT NULL,
	[vApePaterno] [varchar](20) NOT NULL,
	[vApeMaterno] [varchar](20) NOT NULL,
	[vEmail] [varchar](50) NULL,
	[vNroDoc] [varchar](20) NOT NULL,
	[vUsuCreacion] [varchar](100) NULL,
	[dFecCreacion] [datetime] NULL,
	[vHstCreacion] [varchar](20) NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](20) NULL,
	[vRolCreacion] [varchar](20) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](20) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](20) NULL,
	[vRolActualizacion] [varchar](20) NULL,
 CONSTRAINT [PK_MaeUsuarioSIED] PRIMARY KEY CLUSTERED 
(
	[iCodUsuarioSIED] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogUsuarioSIED]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LogUsuarioSIED](
	[iCodLogUsuarioSIED] [bigint] IDENTITY(1,1) NOT NULL,
	[iCodUsuarioSIED] [bigint] NOT NULL FOREIGN KEY REFERENCES [dbo].[MaeUsuarioSIED](iCodUsuarioSIED),
	[WEBUSR] [varchar](20) NOT NULL,
	[vNombre] [varchar](50) NOT NULL,
	[vApePaterno] [varchar](20) NOT NULL,
	[vApeMaterno] [varchar](20) NOT NULL,
	[vEmail] [varchar](50) NULL,
	[vNroDoc] [varchar](20) NOT NULL,
	[vUsuCreacion] [varchar](100) NULL,
	[dFecCreacion] [datetime] NULL,
	[vHstCreacion] [varchar](20) NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](20) NULL,
	[vRolCreacion] [varchar](20) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](20) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](20) NULL,
	[vRolActualizacion] [varchar](20) NULL,
 CONSTRAINT [PK_LogUsuarioSIED] PRIMARY KEY CLUSTERED 
(
	[iCodLogUsuarioSIED] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeRepresentanteLegal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeRepresentanteLegal](
	[iCodRepresentanteLegal] [bigint] IDENTITY(1,1) NOT NULL,
	[vNombre] [varchar](50) NOT NULL,
	[vApePaterno] [varchar](20) NOT NULL,
	[vApeMaterno] [varchar](20) NOT NULL,
	[vEmail] [varchar](50) NULL,
	[vNroDoc] [varchar](20) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NULL,
	[dFecCreacion] [datetime] NULL,
	[vHstCreacion] [varchar](20) NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](20) NULL,
	[vRolCreacion] [varchar](20) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](20) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](20) NULL,
	[vRolActualizacion] [varchar](20) NULL,
 CONSTRAINT [PK_MaeRepresentanteLegal] PRIMARY KEY CLUSTERED 
(
	[iCodRepresentanteLegal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogRepresentanteLegal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LogRepresentanteLegal](
	[iCodLogRepresentanteLegal] [bigint] IDENTITY(1,1) NOT NULL,
	[iCodRepresentanteLegal] [bigint] NOT NULL FOREIGN KEY REFERENCES [dbo].[MaeRepresentanteLegal]([iCodRepresentanteLegal]),
	[vNombre] [varchar](50) NOT NULL,
	[vApePaterno] [varchar](20) NOT NULL,
	[vApeMaterno] [varchar](20) NOT NULL,
	[vEmail] [varchar](50) NULL,
	[vNroDoc] [varchar](20) NOT NULL,
	[siEstado] [smallint] NOT NULL,
	[vUsuCreacion] [varchar](100) NULL,
	[dFecCreacion] [datetime] NULL,
	[vHstCreacion] [varchar](20) NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](20) NULL,
	[vRolCreacion] [varchar](20) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](20) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](20) NULL,
	[vRolActualizacion] [varchar](20) NULL,
 CONSTRAINT [PK_LogRepresentanteLegal] PRIMARY KEY CLUSTERED 
(
	[iCodLogRepresentanteLegal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaeSecuencialSIED]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MaeSecuencialSIED](
	[iAnio] [int] NULL,
	[siEstado] [int] NOT NULL,
	[iCorrelativo] [int] NULL,
	[iCodSecuencial] [int] NOT NULL,
	[iCodArea] [int] NULL,
	[iCodTipDocumento] [int] NULL,
	[vUsuCreacion] [varchar](100) NOT NULL,
	[dFecCreacion] [datetime] NOT NULL,
	[vHstCreacion] [varchar](20) NOT NULL,
	[vInsCreacion] [varchar](50) NULL,
	[vLgnCreacion] [varchar](20) NULL,
	[vRolCreacion] [varchar](20) NULL,
	[vUsuActualizacion] [varchar](100) NULL,
	[dFecActualizacion] [datetime] NULL,
	[vHstActualizacion] [varchar](20) NULL,
	[vInsActualizacion] [varchar](50) NULL,
	[vLgnActualizacion] [varchar](20) NULL,
	[vRolActualizacion] [varchar](20) NULL,
	[vCorrelativo] [varchar](50) NULL,
	[siOrigen] [smallint] NULL,
	[fUsado] [bit] NULL,
 CONSTRAINT [PK_MAESECUENCIALSIED] PRIMARY KEY CLUSTERED 
(
	[iCodSecuencial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO




ALTER TABLE MovDocumento ALTER COLUMN iCodTipDocumento INT  NULL;
ALTER TABLE MovDocMovimiento ALTER COLUMN iCodArea INT  NULL;
ALTER TABLE MovDocMovimiento ALTER COLUMN iCodTrabajador INT  NULL;



ALTER TABLE MovDocumento ADD iCodTipDocumentoSIED  varchar(5) null;
ALTER TABLE MovDocumento ADD iCodTipAsuntoSIED  varchar(5) null;
ALTER TABLE MovDocumento ADD iCodRespuestaSIED   varchar(20) null;
ALTER TABLE MovDocumento ADD iCodDocumentoRespuestaSIED   INT null;
ALTER TABLE MovDocumento ADD iCodCuoRespuestaSIED varchar(100)  null;
ALTER TABLE MovDocumento ADD siFlagSIED  INT null;
ALTER TABLE MovDocumento ADD vMensajeSIED  varchar(250) null;

ALTER TABLE MovDocumentoDerivacion ALTER COLUMN iCodArea INT  NULL;
ALTER TABLE MovDocumentoDerivacion ALTER COLUMN iCodResponsable INT  NULL;

ALTER TABLE MovDocumentoDerivacion ADD iCodEntidadSIED  int null;
ALTER TABLE MovDocumentoDerivacion ADD vNombreEntidadSIED  varchar(200) null;
ALTER TABLE MovDocumentoDerivacion ADD vRucSIED  varchar(20) null;
ALTER TABLE MovDocumentoDerivacion ADD iFormatoDocumentoSIED varchar(2) null;


