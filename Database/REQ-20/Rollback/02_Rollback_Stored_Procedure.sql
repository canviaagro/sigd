DROP PROCEDURE UP_MAE_ListarTrabajadores;  
GO 

DROP PROCEDURE UP_MAE_ListarUsuarioSIED;  
GO   

DROP PROCEDURE UP_MAE_GrabarUsuarioSIED;  
GO   


DROP PROCEDURE UP_MAE_ActualizarUsuarioSIED;  
GO   

DROP PROCEDURE UP_Log_UsuarioSIED;  
GO 

DROP PROCEDURE UP_MAE_ListarRepresentanteLegal;  
GO 

DROP PROCEDURE UP_Log_UsuarioRepresentanteLegal;  
GO 

DROP PROCEDURE UP_MAE_ActualizarRepresentanteLegal;  
GO 

DROP PROCEDURE UP_MAE_GrabarRepresentanteLegal;  
GO

DROP PROCEDURE UP_MOV_RegistrarDocumentoSIED;  
GO

DROP PROCEDURE UP_MOV_RegistrarDocumentosDerivadosSIED;  
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_RegistrarDocMovimiento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento] AS' 
END
GO

ALTER PROCEDURE [dbo].[UP_MOV_RegistrarDocMovimiento]
  @iCodDocumento INT
 ,@iCodArea INT
 ,@iCodTrabajador INT
 ,@dFecRecepcion DATETIME = NULL
 ,@dFecLectura DATETIME = NULL
 ,@siPlazo SMALLINT
 ,@dFecPlazo DATETIME = NULL
 ,@siNivel SMALLINT
 ,@siEstFirma INT
 ,@dFecDerivacion DATETIME=NULL
 ,@dFecVisado DATETIME = NULL
 ,@siEstVisado SMALLINT
 ,@dFecFirma DATETIME = NULL
 ,@iCodDocDerivacion int =NULL
 ,@iCodAreaDerivacion int =NULL
 ,@iCodResponsableDerivacion int= NULL
 ,@siEstadoDerivacion int =NULL
 ,@iTipoMovimiento int =NULL
 ,@vUsuCreacion VARCHAR(100) = NULL
 ,@vHstCreacion VARCHAR(20) = NULL
 ,@vInsCreacion VARCHAR(50) = NULL
 ,@vLgnCreacion VARCHAR(20) = NULL
 ,@vRolCreacion VARCHAR(20) = NULL
 ,@onFlagOK INT OUTPUT
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_RegistrarDocMovimiento]
-- Objetivo:	Registrar Movimiento de los Documentos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código del Documento
--				@iCodArea -  Codigo de Área
--				@iCodTrabajador - Código de Trabajador
--				@dFecRecepcion - Fecha de Recepción
--				@dFecLectura - Fecha de Lectura
--				@siPlazo : Indicador de Plazo
--				@dFecPlazo : Fecha de Plazo
--				@siNivel : Indicador de Nivel
--				@siEstFirma: Indicador de Firma
--				@dFecVisado : Fecha de Visado
--				@siEstVisado : Indicador de Estado Visado
--				@dFecFirma : Fecha de Firma
--				@vUsuCreacion - Campo Auditoria Usuario Creación
--				@vHstCreacion - Campo Auditoría Host
--				@vInsCreacion - Campo Auditoría Instancia
--				@vLgnCreacion - Campo Auditoría Login
--				@vRolCreacion - Campo Auditoría Rol
-- Output		@onFlagOK	- Indicador de Salida
-- Create date: 07/02/20
-- =============================================
DECLARE @iCodMovAnterior INT;
BEGIN

	SET @iCodMovAnterior = (SELECT MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento WHERE iCodDocumento=@iCodDocumento AND siEstado=1)
	
	declare @estadoDoc int ;
	select @estadoDoc = siEstDocumento
	from MovDocumento
	where iCodDocumento=@iCodDocumento

	INSERT INTO dbo.MovDocMovimiento(siEstado,
									dFecRecepcion,
									dFecLectura,
									iCodMovAnterior,
									siPlazo,
									dFecPlazo,
									siNivel,
									siEstFirma,
									dFecVisado,
									siEstVisado,
									dFecFirma,
									iCodDocumento,
									iCodArea,
									iCodTrabajador,
									iCodDocDerivacion,
									iCodAreaDerivacion,
									iCodResponsableDerivacion,
									siEstadoDerivacion,
									iTipoMovimiento,
									vUsuCreacion,
									dFecCreacion,
									vHstCreacion,
									vInsCreacion,
									vLgnCreacion,
									vRolCreacion,
									dFecDerivacion,
									iEstadoDocumento)
									VALUES(									
									1,
									@dFecRecepcion,
									@dFecLectura,
									@iCodMovAnterior,
									@siPlazo,
									@dFecPlazo,
									@siNivel,
									@siEstFirma,
									@dFecVisado,
									@siEstVisado,
									@dFecFirma,
									@iCodDocumento,
									@iCodArea,
									@iCodTrabajador,
   								    @iCodDocDerivacion,
									@iCodAreaDerivacion,
									@iCodResponsableDerivacion,
									@siEstadoDerivacion,
									@iTipoMovimiento,
									@vUsuCreacion,
									GETDATE(),
									@@SERVERNAME,
									@@SERVICENAME,
									@vLgnCreacion,
									@vRolCreacion,
									@dFecDerivacion,
									@estadoDoc
									)



		  IF @@ROWCOUNT  > 0 
				SELECT @onFlagOK=MAX(iCodDocMovimiento) FROM dbo.MovDocMovimiento 
			ELSE
				SET @onFlagOK=0

 END
GO

DROP FUNCTION [dbo].[UF_MOV_OBTENERCORRELATIVOSIED];
GO


DROP PROCEDURE UP_MAE_Generar_SecuencialSIED;
GO

DROP PROCEDURE UP_MOV_Listar_BandejaEnvioSIED;
GO

DROP PROCEDURE UP_MOV_ObtenerDocumentEnvioSIED;
GO

DROP PROCEDURE UP_MOV_ObtenerDocumentosDerivadosEmpresasSIED;
GO

DROP PROCEDURE UP_MOV_ActualizarDocumentoEnvioSIED;
GO

DROP PROCEDURE UP_MOV_EliminarDocumentoDerivacionEnvioSIED;
GO

DROP PROCEDURE UP_MOV_ObtenerTotalDerivaciones;
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_BandejaEntrada]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_BandejaEntrada] AS' 
END
GO

ALTER PROC [dbo].[UP_MOV_Listar_BandejaEntrada] (
@vTextoBusqueda VARCHAR(100),
@dFechaDesde datetime,
@dFechaHasta datetime,
@iTipoBandeja int,
@iCodTrabajador int,
@vCodPerfil varchar(50),
@iPagina int,
@iTamPagina int
)
AS
/*
Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3

Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4
*/
BEGIN

	DECLARE @EstadoIngresado INT

	SELECT 
		@EstadoIngresado = icodParametro 
	FROM ParParametro WHERE iCodGrupo=1 AND vValor IN ('ING');

	with areasTrab as
	(
		select iCodArea from [dbo].[MaeTrabajador] where  iCodTrabajador= @iCodTrabajador
	),
	dataTodo as
	(
		SELECT 
			 a.iCodDocumento,a.siEstado AS Estado
			,a.siEstDocumento as EstadoDocumento
			,a.siPrioridad AS Prioridad
			,CASE WHEN siOrigen=1	THEN a.dFecRegistro
									ELSE a.dFecDerivacion END	AS RecibidoDate
			,CASE WHEN siOrigen=1	THEN CONVERT(VARCHAR(10), a.dFecRegistro, 103) 
									ELSE CONVERT(VARCHAR(10), a.dFecDerivacion, 103)  END	AS Recibido
			,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
			,a.iCodSecuencial AS Reg
			,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
			,ISNULL(a.vAsunto, '') AS DocumentoAsunto
			,MR.iCodRepresentante AS CodRepresentante
			,ISNULL(mr.vNombres,c.vDescripcion) AS Remitente
			,c.vAbreviatura AS Derivado
			,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
			,a.siPlazo
			,ma.iCodDocumento as ExisteAdjunto
			,a.vCorrelativo as Correlativo
			,E.iCodEmpresa AS CodEmpresa
			,E.vDescripcion AS NombreEmpresa
			,a.dFecRegistro				
			,c.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,isnull(priori.vCampo,'') vEstadoPrioridad
			,estDoc.iOrden OrdenEstado
			,priori.iOrden OrdenPrioridad
			,a.iCodTrabajador
			,d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador			
			,a.dFecPlazo
			,a.siOrigen
			,concat(a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',mr.vNombres) AS FILTRO ,
					f.iCodDocumentoFirma
		FROM [dbo].[MovDocumento] a WITH(NOLOCK)
		INNER JOIN [dbo].[MaeTipDocumento] b WITH(NOLOCK) ON a.iCodTipDocumento = b.iCodTipDocumento
		INNER JOIN [dbo].[MaeArea] c WITH(NOLOCK) ON a.iCodArea = c.iCodArea
		INNER JOIN [dbo].[MaeTrabajador] d WITH(NOLOCK) ON a.iCodTrabajador = d.iCodTrabajador
		LEFT join [dbo].[MaeRepresentante] mr WITH(NOLOCK) ON a.iCodRepresentante = mr.iCodRepresentante
		LEFT JOIN MaeEmpresa E WITH(NOLOCK) ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
		left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto WITH(NOLOCK)) ma on a.iCodDocumento = ma.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
		left join ParParametro priori WITH(NOLOCK) on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	    LEFT JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma  WITH(NOLOCK)GROUP BY iCodDocumento) f on a.iCodDocumento = f.iCodDocumento
		WHERE 
		a.siEstado=1
		and dFecRegistro >= cast(@dFechaDesde as date)
		and dFecRegistro < cast(DATEADD(DAY, 1, @dFechaHasta) as date)		
	),
	dataResponsablesDoc as
	(
		select 	
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DA' as TipoDocIng
			,a.iCodDocumentoFirma
		from dataTodo a
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.EstadoDocumento and estDoc.iCodParametro<>0
		where
		a.iCodArea in (select iCodArea from areasTrab)
		and
		(
			isnull(a.iCodTrabajador,-1) = @iCodTrabajador
		)
	),
	dataResponsablesDerivacion as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,dd.siEstadoDerivacion as EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,isnull(estDoc.vCampo,'') vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DD' as TipoDocIng
			,a.iCodDocumentoFirma
			,DD.iCodDocumentoDerivacion 
			
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		INNER JOIN (SELECT MAX(ICODDOCUMENTOFIRMA)ICODDOCUMENTOFIRMA,iCodDocumento FROM MovDocumentoFirma GROUP BY iCodDocumento) F ON DD.iCodDocumento = F.iCodDocumento
		where
				dd.siEstado=1
			and dd.siTipoAcceso=1
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResponsablesCC as
	(
		select 
			 a.iCodDocumento
			,a.Estado
			,a.EstadoDocumento
			,a.Prioridad
			,a.RecibidoDate
			,a.Recibido
			,a.Recepcion
			,a.Reg
			,a.Documento
			,a.DocumentoAsunto
			,a.CodRepresentante
			,a.Remitente
			,a.Derivado
			,a.Plazo
			,a.siPlazo
			,a.ExisteAdjunto
			,a.Correlativo
			,a.CodEmpresa
			,a.NombreEmpresa
			,a.dFecRegistro				
			,a.iCodArea
			,a.vEstadoDescripcion
			,a.vEstadoPrioridad
			,a.OrdenEstado
			,a.OrdenPrioridad
			,a.iCodTrabajador
			,a.NombreTrabajador
			,a.dFecPlazo
			,a.siOrigen
			,CONCAT(A.FILTRO,' ',isnull(estDoc.vCampo,'')) AS FILTRO
			, 'DC' as TipoDocIng
			/*,a.iCodDocumentoFirma*/
		from dataTodo a
		inner join MovDocumentoDerivacion dd on dd.iCodDocumento=a.iCodDocumento
		left join ParParametro estDoc WITH(NOLOCK) on estDoc.iCodGrupo=1 and estDoc.iCodParametro=dd.siEstadoDerivacion and estDoc.iCodParametro<>0
		where
			dd.siEstado=1
			and dd.siTipoAcceso=2
			and
			(
				isnull(dd.iCodResponsable,-1) = @iCodTrabajador
			)
			and dd.iCodArea in (select iCodArea from areasTrab)
			/*27032020*/
			AND A.EstadoDocumento NOT IN(@EstadoIngresado)
	),
	dataResultado as
	(
		select *,0 iCodDocumentoDerivacion 
		from dataResponsablesDoc
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select * from dataResponsablesDerivacion
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDoc
					union 
					select iCodDocumento from dataResponsablesCC
				)
		union
		select *,0 iCodDocumentoFirma, 0 iCodDocumentoDerivacion
		from dataResponsablesCC
		where icodDocumento not in(
					select iCodDocumento from dataResponsablesDerivacion
					union 
					select iCodDocumento from dataResponsablesDoc
				)
	), dataresultadofin as (
	select 
	a.*, 
	dbo.UF_MOV_OBTENERNOMJEFE(iCodArea) Jefe,  
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento=2 THEN 1 ELSE 0 END HabilitarSeguimiento,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarDerivar,
	case when (TipoDocIng = 'DA' OR TipoDocIng = 'DD') AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN'))
	THEN 1 ELSE 0 END HabilitarDevolver,
	case when TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO'))
	THEN 1 ELSE 0 END HabilitarFinalizar,
	case when 
		(TipoDocIng = 'DA' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO')))
		OR
		(TipoDocIng = 'DD' AND A.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN')))
	THEN 1 ELSE 0 END HabilitarRegistrarAvance
	--(select count(1) from dataResultado)TotalRegistros 
	from dataResultado A
	where
	upper(A.FILTRO)
				LIKE '%' + upper(@vTextoBusqueda) + '%' 
	AND
	(
			(		--RECIBIDOS
				@iTipoBandeja=1 AND 
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER')) 
						and siOrigen=2
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						and siOrigen=1
					)
				)
			)		--CON PLAZO
			OR
			(
				@iTipoBandeja=2 AND 			
				(
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in('PEN','PRO','DEV','ATE','DER','ANU')) 
						and siOrigen=2
					)
					OR
					(
						a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor IN ('ING','PEN','DEV','PRO','ATE','DER','ANU'))
						and siOrigen=1
					)
				)
				/*AND
				a.EstadoDocumento in (select icodParametro from ParParametro where iCodGrupo=1 and vValor in ('PEN','DER'))
				and datediff(day,getdate(), A.dFecPlazo)>=1 */
				AND A.siPlazo > 0
			)
		)
		)SELECT *,(select count(1) from dataresultadofin)TotalRegistros  FROM  dataresultadofin
	order by OrdenEstado ASC, OrdenPrioridad ASC, RecibidoDate desc
	OFFSET (@iPagina-1)*@iTamPagina ROWS
	FETCH NEXT @iTamPagina ROWS ONLY;
END

GO

DROP PROCEDURE UP_MOV_AnularDerivacion;
GO

DROP PROCEDURE [dbo].[UP_MOV_RegistrarDocumentoRecepcionSIED];
GO

DROP PROCEDURE [dbo].[UP_MOV_Listar_BandejaRecepcionSIED];
GO

DROP PROCEDURE UP_MOV_ObtenerDocumentoRecepcionSIED;
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivados]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].ma.[UP_MOV_ObtenerDocumentosAnexos]
-- Objetivo:	Obtener Documentos Anexos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documentos Anexos
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT        
dd.iCodDocumentoDerivacion, 
dd.iCodDocumento, 
dd.siEstado, 
dd.iCodArea, 
dd.iCodResponsable, 
dd.siEstadoDerivacion, 
dd.siTipoAcceso, 
dd.dFecDerivacion, 
dd.vUsuCreacion, 
dd.dFecCreacion, 
dd.vHstCreacion, 
dd.vInsCreacion, 
dd.vLgnCreacion, 
dd.vRolCreacion, 
dd.vUsuActualizacion, 
dd.dFecActualizacion, 
dd.vHstActualizacion, 
dd.vInsActualizacion, 
dd.vLgnActualizacion, 
dd.vRolActualizacion,
ma.vDescripcion as NombreArea,
mt.vNombres+' '+mt.vApePaterno+' '+mt.vApeMaterno as NombreResponsable,
SEG.WEBUSR WEBUSRRESPONSABLE
FROM            
MovDocumentoDerivacion dd
left join MaeArea ma on dd.iCodArea=ma.iCodArea
left join MaeTrabajador mt on dd.iCodResponsable=mt.iCodTrabajador
LEFT JOIN SegUsuario SEG ON MT.iCodTrabajador=SEG.iCodTrabajador
WHERE dd.iCodDocumento = @iCodDocumento 
and  dd.siEstado = 1;
END


GO

DROP PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoAnularEnvioSIED];
GO

DROP PROCEDURE [dbo].[UP_MAE_Listar_EmailResponsable_PorCodDocumento];
GO


DROP PROCEDURE [dbo].[UP_MOV_ObtenerOrigen];
GO

DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentEnvioSIED_ServiceSIED];
GO

DROP PROCEDURE [dbo].[UP_MOV_ActualizarDocumentoEnvioSIEDService];
GO


DROP PROCEDURE [dbo].[UP_MOV_ObtenerCodLaserFIcheAnexosPorCodDocumento]
GO

DROP PROCEDURE [dbo].[UP_MAE_Listar_AreaSIED];
GO

DROP PROCEDURE [dbo].[UP_MAE_ListarTrabajadorPorAreaSIED];
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_Listar_Documento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UP_MOV_Listar_Documento] AS' 
END
GO
ALTER PROCEDURE [dbo].[UP_MOV_Listar_Documento] (
@pDescripcion VARCHAR(100),
@FechaInicio DATETIME ,
@FechaFin DATETIME 
)
AS
/*

Etiqueta - Estado del documento:
Ingresado  1
Pendiente  2
Atendido   3


Icono - Prioridad del documento:
(↓) Baja 1
(-) Media 2
(↑) Alta 3
(!) Urgente 4

*/
BEGIN
	SELECT a.iCodDocumento,a.siEstado AS Estado
	    ,a.siEstDocumento as EstadoDocumento
		,a.siPrioridad AS Prioridad
		,CONVERT(VARCHAR(10), a.dFecRegistro, 103) AS Recibido
		,CONVERT(VARCHAR(10), a.dFecRecepcion, 103) AS Recepcion
		,a.iCodSecuencial AS Reg
		,b.vDescripcion + ' ' + a.vNumDocumento  AS Documento
		,ISNULL(a.vAsunto, '') AS DocumentoAsunto
		,MR.iCodRepresentante AS CodRepresentante
		,mr.vNombres AS Remitente
		,c.vAbreviatura AS Derivado
		,CAST(a.siPlazo AS VARCHAR(10)) + ' día(s) ' + CONVERT(VARCHAR(10), a.dFecPlazo, 103) AS Plazo
		,ma.iCodDocumento as ExisteAdjunto
		,a.vCorrelativo as Correlativo
		,E.iCodEmpresa AS CodEmpresa
		,E.vDescripcion AS NombreEmpresa
		, d.vNombres+' '+d.vApePaterno+' '+d.vApeMaterno NombreTrabajador
		,dbo.UF_MOV_OBTENERNOMJEFE(a.iCodArea) Jefe
		,isnull(estDoc.vCampo,'') vEstadoDescripcion
		,isnull(priori.vCampo,'') vEstadoPrioridad
		,A.vUsuCreacion
	FROM [dbo].[MovDocumento] a
	INNER JOIN [dbo].[MaeTipDocumento] b ON a.iCodTipDocumento = b.iCodTipDocumento
	INNER JOIN [dbo].[MaeArea] c ON a.iCodArea = c.iCodArea
	INNER JOIN [dbo].[MaeTrabajador] d ON a.iCodTrabajador = d.iCodTrabajador
	inner join [dbo].[MaeRepresentante] mr ON a.iCodRepresentante = mr.iCodRepresentante
	INNER JOIN MaeEmpresa E ON E.siEstado = 1 AND E.iCodEmpresa = mr.iCodEmpresa 
	left join  (SELECT distinct iCodDocumento fROM MovDocAdjunto) ma on a.iCodDocumento = ma.iCodDocumento
	left join ParParametro estDoc on estDoc.iCodGrupo=1 and estDoc.iCodParametro=a.siEstDocumento and estDoc.iCodParametro<>0
	left join ParParametro priori on priori.iCodGrupo=3 and priori.iCodParametro=a.siPrioridad and priori.iCodParametro<>0
	WHERE
		CONCAT(
					a.vCorrelativo, ' ', E.vDescripcion , ' ',
					b.vAbreviatura, '',b.vDescripcion, '', ISNULL(a.vNumDocumento, ''), ' ',
					ISNULL(b.vDescripcion, ''), ' ',  ISNULL(a.vAsunto, ''), ' ', 
					isnull(priori.vCampo,''),' ',isnull(estDoc.vCampo,''),' ',mr.vNombres
			)	
			LIKE '%' + @pDescripcion+ '%' 
    AND 
	a.dFecRegistro between cast(@FechaInicio as datetime)
	and dateadd(ms, -3, (dateadd(day, +1, convert(varchar, cast(@FechaFin as datetime), 101))))
	ORDER BY estDoc.iOrden asc,priori.iOrden ASC,dFecRegistro desc

END


GO


IF OBJECT_ID('[dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]') IS NOT NULL
BEGIN
	DROP PROCEDURE [UP_MOV_ObtenerDetalleCorreoDerivacion]
END
GO
CREATE  PROCEDURE [dbo].[UP_MOV_ObtenerDetalleCorreoDerivacion]
(
	@iCodDocumento INT 
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumento]
-- Objetivo:	Obtener Datos de un Documento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Output		Entidad Documento
-- Create date: 07/02/2020
-- =============================================
BEGIN
		SELECT	  a.iCodDocumento,
			a.siPlazo,
			a.dFecPlazo,
			a.siPrioridad,
			a.siOrigen,
			CASE WHEN A.siOrigen = 2  THEN R.vNombres
				 WHEN A.siOrigen = 1 THEN (SELECT  TJEFE.vApePaterno + ' '+ TJEFE.vApeMaterno + ' '  + TJEFE.vNombres
											FROM MaeTrabajador TJEFE WHERE iCodArea = ARE.iCodArea AND siEsJefe = 1)   END AS REPRESENTANTE,
			CASE WHEN A.siOrigen = 2  THEN  E.vDescripcion
				 WHEN A.siOrigen = 1 THEN ARE.vDescripcion END AS EMPRESA,
			T.vApePaterno + ' '+ T.vApeMaterno + ' '  + t.vNombres AS DERIVADOPOR,
			par.vCampo TipoAccion
		FROM [dbo].[MovDocumento] a
		INNER JOIN MovDocumentoDerivacion DER ON A.iCodDocumento =  DER.iCodDocumento AND DER.siTipoAcceso = 1 --AND ISNULL(A.vUsuActualizacion,A.vUsuCreacion) = ISNULL(DER.vUsuActualizacion,DER.vUsuCreacion)
		LEFT JOIN MaeRepresentante r on a.iCodRepresentante = r.iCodRepresentante
		INNER JOIN SegUsuario S ON S.WEBUSR = DER.vUsuCreacion
		INNER JOIN MaeTrabajador T ON T.iCodTrabajador = S.iCodTrabajador
		LEFT JOIN MaeEmpresa E ON R.iCodEmpresa = E.iCodEmpresa
		LEFT JOIN MaeArea ARE ON T.iCodArea = ARE.iCodArea
		LEFT JOIN (SELECT * FROM ParParametro where iCodGrupo=8) PAR ON PAR.iCodParametro = der.iTipoAccion
		where a.iCodDocumento=@iCodDocumento
		ORDER BY  DER.dFecCreacion DESC
END
GO

IF OBJECT_ID('[dbo].[UP_MOV_Listar_CabeceraSeguimiento]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimiento]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimiento]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_CabeceraSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

WITH SEGUIMIENTO AS (
	
	SELECT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	mov.dFecCreacion AS FECHA,
	MOV.iEstadoDocumento,
	MOV.iCodDocMovimiento,
	CASE WHEN MOV.iEstadoDocumento  IN (5,7) THEN doc.iCodDocumento ELSE 0  END iCodDocAuxiliar
	FROM MovDocMovimiento MOV
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario USU on DOC.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON MOV.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  MOV.iCodTrabajador = TRAPARA.iCodTrabajador 
	WHERE MOV.iCodDocumento = @iCodDocumento AND iEstadoDocumento <> 6 AND Mov.iCodDocDerivacion is null 

	UNION ALL

	SELECT DISTINCT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	ARE.vDescripcion AS AREADESDE,
	TRAPARA.vApePaterno + ' ' + TRAPARA.vApeMaterno +  ' ' + TRAPARA.vNombres AS NOMBREPARA,
	AREPARA.vDescripcion AS AREAPARA,
	ISNULL(DOC.dFecActualizacion,DOC.dFecCreacion) AS FECHA,
	MOV.iEstadoDocumento,
	0 iCodDocMovimiento,
	MOV.iCodDocumento iCodDocAuxiliar
	FROM MovDocumento DOC 
	INNER JOIN MovDocMovimiento MOV ON MOV.iCodDocumento = DOC.iCodDocumento AND MOV.iEstadoDocumento = 6
	INNER JOIN SegUsuario USU on DOC.vUsuCreacion=USU.WEBUSR and USU.siEstado=1
	INNER JOIN MaeTrabajador TRA ON USU.iCodTrabajador = TRA.iCodTrabajador 
	INNER JOIN MaeArea ARE ON TRA.iCodArea = ARE.iCodArea
	INNER JOIN MaeArea AREPARA ON DOC.iCodArea = AREPARA.iCodArea 
	INNER JOIN MaeTrabajador TRAPARA ON  DOC.iCodTrabajador = TRAPARA.iCodTrabajador 
	WHERE MOV.iCodDocumento = @iCodDocumento AND MOV.iEstadoDocumento = 6 AND Mov.iTipoMovimiento IN (0,2,1,3))
	SELECT * FROM SEGUIMIENTO ORDER BY FECHA ASC
 END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]') AND TYPE in (N'P', N'PC'))
DROP PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
GO


CREATE PROCEDURE [dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
(
	 @iCodDocumento INT ,
	 @siTipoAcceso INT
)
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_ObtenerDocumentosDerivadosPorCodDocumento]
-- Objetivo:	Obtener Documentos DocumentosDerivadosPorCodDocumento
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento		
-- Input:		@siTipoAcceso - Tipo Acceso
-- Output		Entidad Documentos Derivados
-- Create date: 19/02/2020
-- =============================================
BEGIN
SELECT        
dd.iCodDocumentoDerivacion, 
dd.iCodDocumento, 
dd.siEstado, 
dd.iCodArea, 
dd.iCodResponsable, 
dd.siEstadoDerivacion, 
dd.siTipoAcceso, 
dd.dFecDerivacion, 
dd.vUsuCreacion, 
dd.dFecCreacion, 
dd.vHstCreacion, 
dd.vInsCreacion, 
dd.vLgnCreacion, 
dd.vRolCreacion, 
dd.vUsuActualizacion, 
dd.dFecActualizacion, 
dd.vHstActualizacion, 
dd.vInsActualizacion, 
dd.vLgnActualizacion, 
dd.vRolActualizacion,
DD.iTipoAccion
FROM            
MovDocumentoDerivacion dd
WHERE dd.iCodDocumento = @iCodDocumento 
AND DD.siTipoAcceso = @siTipoAcceso
END

GO



IF OBJECT_ID('[dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]') IS NOT NULL
BEGIN
	DROP PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]
END
GO

CREATE PROCEDURE [dbo].[UP_MOV_Listar_CabeceraSeguimientoInterno]
  @iCodDocumento INT 
AS
-- =============================================
-- Nombre:	[dbo].[UP_MOV_Listar_CabeceraSeguimiento]
-- Objetivo:	Lista la cabecera de los seguimientos
-- Módulo:		Movimientos
-- Input:		@iCodDocumento - Código de Documento
-- Create date: 27/02/20
-- =============================================
BEGIN

WITH CABECERA AS (
select 
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	MAEA.vDescripcion AS AREADESDE,
	MOV.vCodigosInternos AS AREAPARA,
	MOV.iEstadoDocumento,
	(select max(dfecCreacion) from  MovDocMovimiento where  iCodDocumento= @iCodDocumento  and iEstadoDocumento =  Mov.iEstadoDocumento )  AS FECHA,
	cast(MOV.iCodDocDerivacion as varchar)iCodDocDerivacion
	FROM MovDocMovimiento MOV 
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario seg ON DOC.vUsuCreacion = SEG.WEBUSR 
	INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
	INNER JOIN MaeArea MAEA ON DOC.iCodArea = MAEA.iCodArea
	INNER JOIN MaeTrabajador TRAPARA ON MOV.iCodTrabajador = TRAPARA.iCodTrabajador
	WHERE MOV.iCodDocumento = @iCodDocumento AND MOV.vCodigosInternos  IS NOT NULL
--	ORDER BY MOV.iCodDocDerivacion DESC OFFSET 0 ROWS
), 
DERIVACIONCODIGOS AS (
SELECT AREAPARA,iEstadoDocumento, icodDocderivacion = STUFF((
    SELECT N' |' + icodDocderivacion FROM CABECERA
    WHERE AREAPARA = x.AREAPARA AND
		   iEstadoDocumento = x.iEstadoDocumento-- AND iEstadoDocumento <>1
    FOR XML PATH(''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 2, N'')
FROM CABECERA AS x
GROUP BY AREAPARA,iEstadoDocumento
--ORDER BY icodDocderivacion DESC OFFSET 0 ROWS
),  CABFIN AS  (
SELECT  DISTINCT
CAB.NOMBREDESDE,
CAB.AREADESDE,
CAB.AREAPARA,
CAB.iEstadoDocumento,
MOV.dFecCreacion FECHA,
CAB.icodDocderivacion AS iCodDerivacionesInternas,
0 as iCodDocumento
FROM  CABECERA CAB 
INNER JOIN MovDocMovimiento MOV ON CAB.iCodDocDerivacion = MOV.iCodDocDerivacion
WHERE  cab.iEstadoDocumento = CASE WHEN  CHARINDEX('|',cab.AREAPARA) > 0 THEN  '' ELSE 1 END 
UNION  ALL
SELECT  DISTINCT
CAB.NOMBREDESDE,
CAB.AREADESDE,
CAB.AREAPARA,
CAB.iEstadoDocumento,
CAB.FECHA,
DER.icodDocderivacion AS iCodDerivacionesInternas,
0 as iCodDocumento
FROM  CABECERA CAB 
INNER  JOIN DERIVACIONCODIGOS DER
ON CAB.AREAPARA = DER.AREAPARA AND CAB.iEstadoDocumento =  DER.iEstadoDocumento
WHERE  cab.iEstadoDocumento <> CASE WHEN  CHARINDEX('|',cab.AREAPARA) > 0 THEN  '' ELSE 1 END 
AND CAB.iEstadoDocumento = DER.iEstadoDocumento
--ORDER BY DER.icodDocderivacion DESC

UNION ALL

SELECT DISTINCT
	TRA.vApePaterno + ' ' + TRA.vApeMaterno +  ' ' + TRA.vNombres AS NOMBREDESDE,
	MAEA.vDescripcion AS AREADESDE,
	(select mm.vCodigosInternos from MovDocMovimiento mm where iCodDocumento =  @iCodDocumento 
	and  iCodDocMovimiento = (select max(iCodDocMovimiento) from MovDocMovimiento where iCodDocumento =  @iCodDocumento
	and vCodigosInternos is not null))  AS AREAPARA,
	MOV.iEstadoDocumento,
	(select max(dfecCreacion) from  MovDocMovimiento where  iCodDocumento= @iCodDocumento  and iEstadoDocumento =  Mov.iEstadoDocumento )  AS FECHA,
	null iCodDerivacionesInternas,
	doc.iCodDocumento
	FROM MovDocMovimiento MOV 
	INNER JOIN MovDocumento DOC ON MOV.iCodDocumento = DOC.iCodDocumento
	INNER JOIN SegUsuario seg ON DOC.vUsuCreacion = SEG.WEBUSR 
	INNER JOIN MaeTrabajador TRA ON SEG.iCodTrabajador = TRA.iCodTrabajador
	INNER JOIN MaeArea MAEA ON DOC.iCodArea = MAEA.iCodArea
	INNER JOIN MaeTrabajador TRAPARA ON MOV.iCodTrabajador = TRAPARA.iCodTrabajador
	WHERE MOV.iCodDocumento = @iCodDocumento and mov.iEstadoDocumento IN(6,5,3,7) AND Mov.iTipoMovimiento IN (0,2,1,4,3,6,5,8)
	)SELECT * FROM CABFIN  ORDER  BY case when iCodDerivacionesInternas is null then 'zzzzzz' else iCodDerivacionesInternas end
END


GO