DROP TABLE [dbo].[LogUsuarioSIED];
DROP TABLE [dbo].[LogRepresentanteLegal];
DROP TABLE [dbo].[MaeUsuarioSIED];
DROP TABLE [dbo].[MaeRepresentanteLegal];
DROP TABLE [dbo].[MaeSecuencialSIED];

ALTER TABLE MovDocumento DROP COLUMN iCodTipDocumentoSIED;
ALTER TABLE MovDocumento DROP COLUMN iCodTipAsuntoSIED;
ALTER TABLE MovDocumento DROP COLUMN iCodRespuestaSIED;
ALTER TABLE MovDocumento DROP COLUMN iCodDocumentoRespuestaSIED;
ALTER TABLE MovDocumento DROP COLUMN iCodCuoRespuestaSIED;
ALTER TABLE MovDocumento DROP COLUMN siFlagSIED;
ALTER TABLE MovDocumento DROP COLUMN vMensajeSIED;


ALTER TABLE MovDocumentoDerivacion DROP COLUMN iCodEntidadSIED;
ALTER TABLE MovDocumentoDerivacion DROP COLUMN vNombreEntidadSIED;
ALTER TABLE MovDocumentoDerivacion DROP COLUMN vRucSIED;
ALTER TABLE MovDocumentoDerivacion DROP COLUMN iFormatoDocumentoSIED;

