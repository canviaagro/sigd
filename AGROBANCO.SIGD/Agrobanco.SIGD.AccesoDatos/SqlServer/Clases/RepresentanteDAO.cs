﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class RepresentanteDAO : IRepresentanteDAO
    {

        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public RepresentanteDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método Público para el buscar Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        public List<Representante> BuscarRepresentante(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            totalRegistros = 0;
            Representante objEntidad = null;
            List<Representante> lstRepresentante = new List<Representante>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_BuscarMaeRepresentante]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = null;
                    param = new SqlParameter("@texto", SqlDbType.VarChar, 100);
                    param.Value = filtro.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iCodEmpresa";
                    param.Value = filtro.Param;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = filtro.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = filtro.TamanioPagina;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Representante();

                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.NumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.Nombres = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["siTipRepresentante"])) objEntidad.TipRepresentante = Convert.ToInt16(reader["siTipRepresentante"]);
                                if (!Convert.IsDBNull(reader["vDireccion"])) objEntidad.Direccion = Convert.ToString(reader["vDireccion"]);
                                if (!Convert.IsDBNull(reader["vTelefono"])) objEntidad.Telefono = Convert.ToString(reader["vTelefono"]);
                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.Celular = Convert.ToString(reader["vCelular"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.Email = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.CodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vAnexo"])) objEntidad.Anexo = Convert.ToString(reader["vAnexo"]);
                                if (!Convert.IsDBNull(reader["vFax"])) objEntidad.Fax = Convert.ToString(reader["vFax"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FecCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HstCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InsCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LgnCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FecActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HstActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InsActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LgnActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                lstRepresentante.Add(objEntidad);
                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al buscar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstRepresentante;
        }

        /// <summary>
        /// Método Público para el obtener un(a) Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        public Representante ObtenerRepresentante(Representante input)
        {
            Representante objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerMaeRepresentante]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;
                    param = new SqlParameter("@iCodRepresentante", SqlDbType.Int, 10);
                    param.Value = input.CodRepresentante;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Representante();

                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                if (!Convert.IsDBNull(reader["vNumDocumento"])) objEntidad.NumDocumento = Convert.ToString(reader["vNumDocumento"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.Nombres = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["siTipRepresentante"])) objEntidad.TipRepresentante = Convert.ToInt16(reader["siTipRepresentante"]);
                                if (!Convert.IsDBNull(reader["vDireccion"])) objEntidad.Direccion = Convert.ToString(reader["vDireccion"]);
                                if (!Convert.IsDBNull(reader["vTelefono"])) objEntidad.Telefono = Convert.ToString(reader["vTelefono"]);
                                if (!Convert.IsDBNull(reader["vCelular"])) objEntidad.Celular = Convert.ToString(reader["vCelular"]);
                                if (!Convert.IsDBNull(reader["vEmail"])) objEntidad.Email = Convert.ToString(reader["vEmail"]);
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.CodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vAnexo"])) objEntidad.Anexo = Convert.ToString(reader["vAnexo"]);
                                if (!Convert.IsDBNull(reader["vFax"])) objEntidad.Fax = Convert.ToString(reader["vFax"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FecCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HstCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InsCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LgnCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FecActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HstActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InsActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LgnActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return objEntidad;
        }

        /// <summary>
        /// Método Público para el obtener un(a) Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        public Representante ValidarRepresentantePorNumDocumento(Representante input)
        {
            int cantidad = 0;
            Representante rep = new Representante();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerMaeRepresentantePorNumDocumento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;
                    param = new SqlParameter("@NumDocumento", SqlDbType.VarChar, 20);
                    param.Value = input.NumDocumento;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                              
                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) rep.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return rep;
        }

        /// <summary>
        /// Método Público para Registrar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Representante objRepresentante)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_RegistrarMaeRepresentante]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.NumDocumento;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vNombres", SqlDbType.VarChar, 200);
                        param.Value = objRepresentante.Nombres;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objRepresentante.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siTipRepresentante", SqlDbType.SmallInt);
                        param.Value = objRepresentante.TipRepresentante;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDireccion", SqlDbType.VarChar, 500);
                        param.Value = objRepresentante.Direccion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vTelefono", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.Telefono;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vCelular", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.Celular;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vEmail", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.Email;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@iCodEmpresa", SqlDbType.Int);
                        param.Value = objRepresentante.CodEmpresa;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@iCodTipDocIdentidad", SqlDbType.Int);
                        param.Value = objRepresentante.CodTipDocIdentidad;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vAnexo", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.Anexo;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vFax", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.Fax;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.UsuCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objRepresentante.FecCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.HstCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objRepresentante.InsCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.LgnCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.UsuActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.VarChar, 1);
                        param.Value = objRepresentante.FecActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.HstActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objRepresentante.InsActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.LgnActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.RolActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al registrar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Actualizar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Representante objRepresentante)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_ActualizarMaeRepresentante]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodRepresentante", SqlDbType.Int);
                        param.Value = objRepresentante.CodRepresentante;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vNumDocumento", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.NumDocumento;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vNombres", SqlDbType.VarChar, 200);
                        param.Value = objRepresentante.Nombres;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objRepresentante.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siTipRepresentante", SqlDbType.SmallInt);
                        param.Value = objRepresentante.TipRepresentante;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDireccion", SqlDbType.VarChar, 500);
                        param.Value = objRepresentante.Direccion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vTelefono", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.Telefono;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vCelular", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.Celular;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vEmail", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.Email;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@iCodEmpresa", SqlDbType.Int);
                        param.Value = objRepresentante.CodEmpresa;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@iCodTipDocIdentidad", SqlDbType.Int);
                        param.Value = objRepresentante.CodTipDocIdentidad;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vAnexo", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.Anexo;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vFax", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.Fax;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.UsuCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objRepresentante.FecCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.HstCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objRepresentante.InsCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.LgnCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 100);
                        param.Value = objRepresentante.UsuActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.VarChar, 1);
                        param.Value = objRepresentante.FecActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.HstActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objRepresentante.InsActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.LgnActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objRepresentante.RolActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Actualizar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Eliminar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Representante objRepresentante)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_EliminarMaeRepresentante]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodRepresentante", SqlDbType.Int, 10);
                        param.Value = objRepresentante.CodRepresentante;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                        param.Value = objRepresentante.UsuActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                        param.Value = objRepresentante.LgnActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Eliminar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }
        #endregion
    }
}
