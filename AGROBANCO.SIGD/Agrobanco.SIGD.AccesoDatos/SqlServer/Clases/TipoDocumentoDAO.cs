﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
   public class TipoDocumentoDAO :ITipoDocumentoDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public TipoDocumentoDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }


        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para el Listado de Tipo de Documentos Activos
        /// </summary>
        /// <returns>Retorna Lista genérica de Tipos de Documentos Activos</returns>  
        public List<TipoDocumentoDTO> ListarTipoDocumento()
        {
            TipoDocumentoDTO objEntidad = null;
            List<TipoDocumentoDTO> lstTipoDoc = new List<TipoDocumentoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Tipo_Documento]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new TipoDocumentoDTO();
                                if (!Convert.IsDBNull(reader["iCodTipDocumento"])) objEntidad.CodTipoDocumento = Convert.ToInt32(reader["iCodTipDocumento"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstTipoDoc.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Tipo de Documentos");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstTipoDoc;
        }


        #endregion
    }
}
