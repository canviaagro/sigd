﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    
    public class TrabajadorDAO: ITrabajadorDAO
    {

        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor
        public TrabajadorDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        public int ActualizarTrabajador(TrabajadorDTO objTrabajador)
        {
            throw new NotImplementedException();
        }

        public int EliminarTrabajador(TrabajadorDTO objTrabajador)
        {
            throw new NotImplementedException();
        }

        public List<TrabajadorDTO> ListarTrabajador()
        {
            throw new NotImplementedException();
        }

        public List<TrabajadorDTO> ListarTrabajadorPaginado(TrabajadorDTO objParams, out int totalRegistros)
        {
            throw new NotImplementedException();
        }

        public TrabajadorDTO ObtenerTabajadorPorId(TrabajadorDTO objParams)
        {
            throw new NotImplementedException();
        }

        public int RegistrarTrabajador(TrabajadorDTO objTrabajador)
        {
            throw new NotImplementedException();
        }

        #endregion



    }
}
