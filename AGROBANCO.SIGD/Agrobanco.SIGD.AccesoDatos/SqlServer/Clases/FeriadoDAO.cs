﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class FeriadoDAO: IFeriadosDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public FeriadoDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método Público para el Listado de Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        public List<Feriado> BuscarFeriado(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            totalRegistros = 0;
            Feriado objEntidad = null;
            List<Feriado> lstFeriado = new List<Feriado>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_BuscarFeriados]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = null;
                    param = new SqlParameter("@texto", SqlDbType.VarChar, 100);
                    param.Value = filtro.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = filtro.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = filtro.TamanioPagina;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Feriado();

                                if (!Convert.IsDBNull(reader["iCodFeriado"])) objEntidad.CodFeriado = Convert.ToInt32(reader["iCodFeriado"]);
                                if (!Convert.IsDBNull(reader["dFecha"])) objEntidad.Fecha = Convert.ToDateTime(reader["dFecha"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FecCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HstCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InsCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LgnCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FecActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HstActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InsActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LgnActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["iTipoFeriado"])) objEntidad.TipoFeriado = Convert.ToInt32(reader["iTipoFeriado"]);
                                if (!Convert.IsDBNull(reader["TipoFeriadoDescripcion"])) objEntidad.TipoFeriadoDescripcion = Convert.ToString(reader["TipoFeriadoDescripcion"]);
                                lstFeriado.Add(objEntidad);
                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al buscar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstFeriado;
        }

        /// <summary>
        /// Método Público para el obtener un(a) Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        public Feriado ObtenerFeriado(Feriado input)
        {
            Feriado objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerFeriado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;
                    param = new SqlParameter("@iCodFeriado", SqlDbType.Int, 10);
                    param.Value = input.CodFeriado;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Feriado();

                                if (!Convert.IsDBNull(reader["iCodFeriado"])) objEntidad.CodFeriado = Convert.ToInt32(reader["iCodFeriado"]);
                                if (!Convert.IsDBNull(reader["dFecha"])) objEntidad.Fecha = Convert.ToDateTime(reader["dFecha"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FecCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HstCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InsCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LgnCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FecActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HstActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InsActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LgnActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                if (!Convert.IsDBNull(reader["iTipoFeriado"])) objEntidad.TipoFeriado = Convert.ToInt32(reader["iTipoFeriado"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return objEntidad;
        }
        /// <summary>
        /// Método Público para Registrar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Feriado objFeriado)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_RegistrarFeriado]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@dFecha", SqlDbType.DateTime);
                        param.Value = objFeriado.Fecha;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDescripcion", SqlDbType.VarChar, 100);
                        param.Value = objFeriado.Descripcion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objFeriado.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 100);
                        param.Value = objFeriado.UsuCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objFeriado.FecCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.HstCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objFeriado.InsCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.LgnCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 100);
                        param.Value = objFeriado.UsuActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.DateTime);
                        param.Value = objFeriado.FecActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.HstActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objFeriado.InsActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.LgnActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.RolActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@iTipoFeriado", SqlDbType.Int);
                        param.Value = objFeriado.TipoFeriado;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al registrar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Actualizar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Feriado objFeriado)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_ActualizarFeriado]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodFeriado", SqlDbType.Int);
                        param.Value = objFeriado.CodFeriado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecha", SqlDbType.DateTime);
                        param.Value = objFeriado.Fecha;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDescripcion", SqlDbType.VarChar, 100);
                        param.Value = objFeriado.Descripcion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objFeriado.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 100);
                        param.Value = objFeriado.UsuCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objFeriado.FecCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.HstCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objFeriado.InsCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.LgnCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 100);
                        param.Value = objFeriado.UsuActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.DateTime);
                        param.Value = objFeriado.FecActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.HstActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objFeriado.InsActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.LgnActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objFeriado.RolActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@iTipoFeriado", SqlDbType.Int);
                        param.Value = objFeriado.TipoFeriado;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Actualizar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Eliminar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Feriado objFeriado)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_EliminarFeriado]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodFeriado", SqlDbType.Int, 10);
                        param.Value = objFeriado.CodFeriado;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                        param.Value = objFeriado.UsuActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                        param.Value = objFeriado.LgnActualizacion;
                        cmd.Parameters.Add(param);


                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Eliminar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }
        #endregion
    }
}
