﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Entidades.Entidades;
using System.Data;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class EmpresaDAO : IEmpresaDAO
    {

        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public EmpresaDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion


        #region Métodos Públicos

        /// <summary>
        /// Método público para el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna el Identificador de la Nueva Empresa</returns>  
        public int RegistrarEmpresa(Empresa objEmpresa)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_RegistrarEmpresa]", conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter param = new SqlParameter();

                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vAbreviatura";
                        param.Value = objEmpresa.Abreviatura;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vDescripcion";
                        param.Value = objEmpresa.Descripcion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vDescripcion";
                        param.Value = objEmpresa.Direccion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vUsuCreacion";
                        param.Value = objEmpresa.UsuarioCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vHstCreacion";
                        param.Value = objEmpresa.HostCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vInsCreacion";
                        param.Value = objEmpresa.InstanciaCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vLgnCreacion";
                        param.Value = objEmpresa.LoginCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.SqlDbType = SqlDbType.VarChar;
                        param.ParameterName = "@vRolCreacion";
                        param.Value = objEmpresa.RolCreacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);

                        throw new Exception("Error al Registrar la Empresa");
                    }
                    finally
                    {
                        cmd.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }

            }

            return nRpta;
        }

        /// <summary>
        /// Interface para el Listado de Empresas
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresas</returns> 
        public List<EmpresaDTO> ListarEmpresa()
        {
            EmpresaDTO objEntidad = null;
            List<EmpresaDTO> lstEmpresa = new List<EmpresaDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Empresa]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new EmpresaDTO();
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["vDireccion"])) objEntidad.Direccion = Convert.ToString(reader["vDireccion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstEmpresa.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Empresas");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstEmpresa;
        }

        /// <summary>
        /// Método Público para el Listado de Representantes por Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Representantes por Empresa</returns>    
        public List<RepresentanteDTO> ListarRepresentantePorEmpresa(int codEmpresa)
        {
            RepresentanteDTO objEntidad = null;
            List<RepresentanteDTO> lstRepresentante = new List<RepresentanteDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Representante_x_Emp]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@CodEmpresa";
                    param.Value = codEmpresa;
                    param.SqlDbType = SqlDbType.Int;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new RepresentanteDTO();
                                if (!Convert.IsDBNull(reader["iCodRepresentante"])) objEntidad.CodRepresentante = Convert.ToInt32(reader["iCodRepresentante"]);
                                if (!Convert.IsDBNull(reader["vNombres"])) objEntidad.Nombre = Convert.ToString(reader["vNombres"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstRepresentante.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Representantes por Empresa");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstRepresentante;
        }



        /// <summary>
        /// Método Público para el Listado de Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        public List<Empresa> BuscarEmpresa(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            totalRegistros = 0;
            Empresa objEntidad = null;
            List<Empresa> lstEmpresa = new List<Empresa>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_BuscarMaeEmpresa]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = null;
                    param = new SqlParameter("@texto", SqlDbType.VarChar, 100);
                    param.Value = filtro.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = filtro.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = filtro.TamanioPagina;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Empresa();

                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.IdEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = Convert.ToString(reader["vAbreviatura"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["vDireccion"])) objEntidad.Direccion = Convert.ToString(reader["vDireccion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                lstEmpresa.Add(objEntidad);
                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al buscar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstEmpresa;
        }

        /// <summary>
        /// Método Público para el obtener un(a) Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        public Empresa ObtenerEmpresa(Empresa input)
        {
            Empresa objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerMaeEmpresa]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;
                    param = new SqlParameter("@iCodEmpresa", SqlDbType.Int, 10);
                    param.Value = input.CodEmpresa;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Empresa();

                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.IdEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["iCodEmpresa"])) objEntidad.CodEmpresa = Convert.ToInt32(reader["iCodEmpresa"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = Convert.ToString(reader["vAbreviatura"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["vDireccion"])) objEntidad.Direccion = Convert.ToString(reader["vDireccion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return objEntidad;
        }
        /// <summary>
        /// Método Público para Registrar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Empresa objEmpresa)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_RegistrarMaeEmpresa]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;
                        param = new SqlParameter("@vAbreviatura", SqlDbType.VarChar, 20);
                        param.Value = objEmpresa.Abreviatura;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDescripcion", SqlDbType.VarChar, 100);
                        param.Value = objEmpresa.Descripcion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDireccion", SqlDbType.VarChar, 200);
                        param.Value = objEmpresa.Direccion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objEmpresa.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.UsuarioCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objEmpresa.FechaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.HostCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.InstanciaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.LoginCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.UsuarioActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.DateTime);
                        param.Value = objEmpresa.FechaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.HostActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.InstanciaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.LoginActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.RolActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al registrar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Actualizar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Empresa objEmpresa)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_ActualizarMaeEmpresa]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodEmpresa", SqlDbType.Int);
                        param.Value = objEmpresa.CodEmpresa;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vAbreviatura", SqlDbType.VarChar, 20);
                        param.Value = objEmpresa.Abreviatura;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDescripcion", SqlDbType.VarChar, 100);
                        param.Value = objEmpresa.Descripcion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDireccion", SqlDbType.VarChar, 200);
                        param.Value = objEmpresa.Direccion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objEmpresa.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.UsuarioCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objEmpresa.FechaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.HostCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.InstanciaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objEmpresa.LoginCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.UsuarioActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.DateTime);
                        param.Value = objEmpresa.FechaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.HostActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.InstanciaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.LoginActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objEmpresa.RolActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Actualizar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Eliminar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Empresa objEmpresa)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_EliminarMaeEmpresa]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodEmpresa", SqlDbType.Int, 10);
                        param.Value = objEmpresa.CodEmpresa;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                        param.Value = objEmpresa.UsuarioActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                        param.Value = objEmpresa.LoginActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Eliminar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }


        #endregion

    }
}
