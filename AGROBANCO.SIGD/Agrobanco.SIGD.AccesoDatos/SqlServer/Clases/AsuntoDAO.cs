﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class AsuntoDAO : IAsuntoDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public AsuntoDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para el Listado de Asuntos
        /// </summary>
        /// <returns>Retorna Lista genérica de Asuntos</returns>  
        public List<AsuntoDTO> ListarAsunto()
        {
            AsuntoDTO objEntidad = null;
            List<AsuntoDTO> lstAsunto = new List<AsuntoDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_Asunto]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new AsuntoDTO();
                                if (!Convert.IsDBNull(reader["iCodAsunto"])) objEntidad.CodAsunto = Convert.ToInt32(reader["iCodAsunto"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstAsunto.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Asuntos");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstAsunto;
        }



        /// <summary>
        /// Método Público para el Listado de Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        public List<Asunto> BuscarAsunto(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            totalRegistros = 0;
            Asunto objEntidad = null;
            List<Asunto> lstAsunto = new List<Asunto>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_BuscarAsuntoEstandarizado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = null;
                    param = new SqlParameter("@texto", SqlDbType.VarChar, 100);
                    param.Value = filtro.TextoBusqueda;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iPagina";
                    param.Value = filtro.Pagina;
                    cmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.SqlDbType = SqlDbType.Int;
                    param.ParameterName = "@iTamPagina";
                    param.Value = filtro.TamanioPagina;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Asunto();

                                if (!Convert.IsDBNull(reader["iCodAsunto"])) objEntidad.CodAsunto = Convert.ToInt32(reader["iCodAsunto"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                                lstAsunto.Add(objEntidad);


                                totalRegistros = Convert.ToInt32(reader["TotalRegistros"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al buscar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstAsunto;
        }

        /// <summary>
        /// Método Público para el obtener un(a) Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        public Asunto ObtenerAsunto(Asunto input)
        {
            Asunto objEntidad = null;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_ObtenerAsuntoEstandarizado]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = null;
                    param = new SqlParameter("@iCodAsunto", SqlDbType.Int, 10);
                    param.Value = input.CodAsunto;
                    cmd.Parameters.Add(param);

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new Asunto();

                                if (!Convert.IsDBNull(reader["iCodAsunto"])) objEntidad.CodAsunto = Convert.ToInt32(reader["iCodAsunto"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = Convert.ToInt16(reader["siEstado"]);
                                if (!Convert.IsDBNull(reader["vUsuCreacion"])) objEntidad.UsuarioCreacion = Convert.ToString(reader["vUsuCreacion"]);
                                if (!Convert.IsDBNull(reader["dFecCreacion"])) objEntidad.FechaCreacion = Convert.ToDateTime(reader["dFecCreacion"]);
                                if (!Convert.IsDBNull(reader["vHstCreacion"])) objEntidad.HostCreacion = Convert.ToString(reader["vHstCreacion"]);
                                if (!Convert.IsDBNull(reader["vInsCreacion"])) objEntidad.InstanciaCreacion = Convert.ToString(reader["vInsCreacion"]);
                                if (!Convert.IsDBNull(reader["vLgnCreacion"])) objEntidad.LoginCreacion = Convert.ToString(reader["vLgnCreacion"]);
                                if (!Convert.IsDBNull(reader["vRolCreacion"])) objEntidad.RolCreacion = Convert.ToString(reader["vRolCreacion"]);
                                if (!Convert.IsDBNull(reader["vUsuActualizacion"])) objEntidad.UsuarioActualizacion = Convert.ToString(reader["vUsuActualizacion"]);
                                if (!Convert.IsDBNull(reader["dFecActualizacion"])) objEntidad.FechaActualizacion = Convert.ToDateTime(reader["dFecActualizacion"]);
                                if (!Convert.IsDBNull(reader["vHstActualizacion"])) objEntidad.HostActualizacion = Convert.ToString(reader["vHstActualizacion"]);
                                if (!Convert.IsDBNull(reader["vInsActualizacion"])) objEntidad.InstanciaActualizacion = Convert.ToString(reader["vInsActualizacion"]);
                                if (!Convert.IsDBNull(reader["vLgnActualizacion"])) objEntidad.LoginActualizacion = Convert.ToString(reader["vLgnActualizacion"]);
                                if (!Convert.IsDBNull(reader["vRolActualizacion"])) objEntidad.RolActualizacion = Convert.ToString(reader["vRolActualizacion"]);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return objEntidad;
        }
        /// <summary>
        /// Método Público para Registrar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Asunto objAsunto)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_RegistrarAsuntoEstandarizado]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;
                        
                        param = new SqlParameter("@vDescripcion", SqlDbType.VarChar, 1000);
                        param.Value = objAsunto.Descripcion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objAsunto.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 100);
                        param.Value = objAsunto.UsuarioCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objAsunto.FechaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.HostCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objAsunto.InstanciaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.LoginCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 100);
                        param.Value = objAsunto.UsuarioActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.DateTime);
                        param.Value = objAsunto.FechaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.HostActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objAsunto.InstanciaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.LoginActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.RolActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al registrar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Actualizar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Asunto objAsunto)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_ActualizarAsuntoEstandarizado]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodAsunto", SqlDbType.Int);
                        param.Value = objAsunto.CodAsunto;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vDescripcion", SqlDbType.VarChar, 1000);
                        param.Value = objAsunto.Descripcion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@siEstado", SqlDbType.SmallInt);
                        param.Value = objAsunto.Estado;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuCreacion", SqlDbType.VarChar, 100);
                        param.Value = objAsunto.UsuarioCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecCreacion", SqlDbType.DateTime);
                        param.Value = objAsunto.FechaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstCreacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.HostCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsCreacion", SqlDbType.VarChar, 50);
                        param.Value = objAsunto.InstanciaCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnCreacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.LoginCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolCreacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.RolCreacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar, 100);
                        param.Value = objAsunto.UsuarioActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@dFecActualizacion", SqlDbType.DateTime);
                        param.Value = objAsunto.FechaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vHstActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.HostActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vInsActualizacion", SqlDbType.VarChar, 50);
                        param.Value = objAsunto.InstanciaActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.LoginActualizacion;
                        cmd.Parameters.Add(param);
                        param = new SqlParameter("@vRolActualizacion", SqlDbType.VarChar, 20);
                        param.Value = objAsunto.RolActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Actualizar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }

        /// <summary>
        /// Método Público para Eliminar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Asunto objAsunto)
        {
            int nRpta = 0;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = "[dbo].[UP_MAE_EliminarAsuntoEstandarizado]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        SqlParameter param = null;

                        param = new SqlParameter("@iCodAsunto", SqlDbType.Int, 10);
                        param.Value = objAsunto.CodAsunto;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vUsuActualizacion", SqlDbType.VarChar);
                        param.Value = objAsunto.UsuarioActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter("@vLgnActualizacion", SqlDbType.VarChar);
                        param.Value = objAsunto.LoginActualizacion;
                        cmd.Parameters.Add(param);

                        param = new SqlParameter();
                        param.Direction = ParameterDirection.Output;
                        param.SqlDbType = SqlDbType.Int;
                        param.ParameterName = "@onFlagOK";
                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        nRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        nRpta = -1;
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Eliminar");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                }
            }

            return nRpta;
        }
        #endregion
    }
}
