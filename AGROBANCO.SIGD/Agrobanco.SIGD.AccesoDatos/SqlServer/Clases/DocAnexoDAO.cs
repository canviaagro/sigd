﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class DocAnexoDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        public DocAnexoDAO() { }
        public DocAnexoDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        /// <summary>
        /// Método privado para el registro de los anexos de un documento
        /// </summary>
        /// <returns>Retorna indicador de registro de un nuevo anexo</returns>  
        public int InsertarDocAnexo(DocAnexo objDocAnexo, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_RegistrarDocAnexo]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodLaserfiche", SqlDbType.Int);
            param.Value = objDocAnexo.CodLaserfiche;
            cmd.Parameters.Add(param);

            param = new SqlParameter("@iCodDocMovimiento", SqlDbType.Int);
            param.Value = objDocAnexo.CodDocumentoMovimiento;
            cmd.Parameters.Add(param);


            param = new SqlParameter();
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@iCodDocumento";
            param.Value = objDocAnexo.CodDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vNombre";
            param.Value = objDocAnexo.NombreDocumento;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@iSecuencia";
            param.Value = objDocAnexo.secuencia;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@iTipoArchivo";
            param.Value = objDocAnexo.TipoArchivo;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vUsuCreacion";
            param.Value = objDocAnexo.UsuarioCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vHstCreacion";
            param.Value = objDocAnexo.HostCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vInsCreacion";
            param.Value = objDocAnexo.InstanciaCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vLgnCreacion";
            param.Value = objDocAnexo.LoginCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.SqlDbType = SqlDbType.VarChar;
            param.ParameterName = "@vRolCreacion";
            param.Value = objDocAnexo.RolCreacion;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        /// <summary>
        /// Método privado para eliminacion de un anexo
        /// </summary>
        /// <returns>Retorna indicador de eliminacion de un nuevo anexo</returns>  
        public int EliminarDocAnexo(DocAnexo objDocAnexo, SqlCommand cmd)
        {
            int onRpta = -1;

            cmd.CommandText = "[dbo].[UP_MOV_EliminarAnexo]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();

            SqlParameter param = new SqlParameter("@iCodLaserFiche", SqlDbType.Int);
            param.Value = objDocAnexo.CodLaserfiche;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.Int;
            param.ParameterName = "@onFlagOK";
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();

            onRpta = Convert.ToInt32(cmd.Parameters["@onFlagOK"].Value.ToString());

            return onRpta;

        }

        public string GuardarArchivosTemporalesAnexo(List<DocAnexo> LstAnexos, string sufijo, int correlativoinicial=1)
        {
            if (LstAnexos.Count == 0 || Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]) == true)
                return "";

            string guidTmp = Guid.NewGuid().ToString().Replace("-", "");
            string directoryPath = @String.Format(@"\{0}\SGDTEMP",
                            ApplicationKeys.LaserFicheFolderBase);

            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                lfhelper.GetOrCreateFolderInfo(directoryPath);
                //ANEXOS ADJUNTOS
                int cc = correlativoinicial;
                foreach (var item in LstAnexos)
                {
                    string fileName = "";
                    byte[] bytes = Convert.FromBase64String(item.FileArray);
                    string extension = System.IO.Path.GetExtension(item.NombreDocumento);
                    var nombreAnexo = guidTmp.ToString() + "_" + sufijo + cc + extension;
                    string documentPath = String.Format(@"\{0}\{1}", directoryPath, nombreAnexo);

                    MemoryStream file = new MemoryStream(bytes);
                    item.CodLaserfiche = lfhelper.RegisterFileWithoutMetaData(
                        file,
                        documentPath,
                        ApplicationKeys.Laserfiche_REG,
                        out fileName,
                        contenType: TypesConstants.GetTypeMIME(extension.Replace(".",""))
                        );
                    //Insertando Anexos
                    item.secuencia = cc;

                    cc++;
                }
                lfhelper.Dispose();
            }
            return guidTmp;
        }

        public void LimpiarCarpetaAnexos(List<DocAnexo> lstAnexos)
        {
            if (lstAnexos.Count == 0 || Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]) == true)
                return;

            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in lstAnexos)
                    if (item.CodLaserfiche > 0)
                        lfhelper.DeleteDocumentIfExistv2(item.CodLaserfiche);
            }
        }
        public void MoverArchivosAnexosTemporales(List<DocAnexo> lstDocAnexo, string correlativoDoc, string carpeta, string sufijo, int correlativoInicio = 1)
        {
            if (lstDocAnexo.Count == 0 || Convert.ToBoolean(ConfigurationManager.AppSettings["IgnorarLaserFiche"]) == true)
                return;
            string directoryPath = @String.Format(@"\{0}\{1}",
                            ApplicationKeys.LaserFicheFolderBase,
                            correlativoDoc);
            string directoryPathAnexos = @String.Format(@"\{0}\{1}", directoryPath, carpeta);

            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                lfhelper.GetOrCreateFolderInfo(directoryPathAnexos);
                //ANEXOS TIPO ADJUNTOS
                if (lstDocAnexo.Count > 0)
                {
                    int cc = correlativoInicio;
                    foreach (var item in lstDocAnexo)
                    {
                        var nombreAnexo = correlativoDoc.ToString() + "_" + sufijo + cc + System.IO.Path.GetExtension(item.NombreDocumento);
                        string documentPath = String.Format(@"\{0}\{1}", directoryPathAnexos, nombreAnexo);
                        lfhelper.MoveDocument(item.CodLaserfiche, documentPath);
                        cc++;
                    }
                }
                lfhelper.Dispose();
            }
        }


    }
}
