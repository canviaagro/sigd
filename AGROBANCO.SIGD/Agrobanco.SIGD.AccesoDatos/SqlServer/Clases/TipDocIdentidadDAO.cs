﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
   public class TipDocIdentidadDAO : ITipDocIdentidadDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public TipDocIdentidadDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }


        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para el Listado de Tipo de Documentos Identidad
        /// </summary>
        /// <returns>Retorna Lista genérica de Tipos de Documentos Activos</returns>  
        public List<TipDocIdentidad> ListarTipDocIdentidad()
        {
            TipDocIdentidad objEntidad = null;
            List<TipDocIdentidad> lstTipoDoc = new List<TipDocIdentidad>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_MAE_Listar_TipDocIdentidad]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                objEntidad = new TipDocIdentidad();
                                if (!Convert.IsDBNull(reader["iCodTipDocIdentidad"])) objEntidad.iCodTipDocIdentidad = Convert.ToInt32(reader["iCodTipDocIdentidad"]);
                                if (!Convert.IsDBNull(reader["vAbreviatura"])) objEntidad.Abreviatura = Convert.ToString(reader["vAbreviatura"]);
                                if (!Convert.IsDBNull(reader["vDescripcion"])) objEntidad.Descripcion = Convert.ToString(reader["vDescripcion"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                lstTipoDoc.Add(objEntidad);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Tipo de Documentos");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();

                    }
                }
            }
            return lstTipoDoc;
        }


        #endregion
    }
}
