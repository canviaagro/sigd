﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Clases
{
    public class ParametroDAO : IParametroDAO
    {
        public const int RESULTADO_OK = 1;
        public const int RESULTADO_ERROR = -1;
        public string cadenaConexion;

        #region Constructor

        public ParametroDAO(string strCadenaConexion)
        {
            cadenaConexion = strCadenaConexion;
        }
        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para obtener Parametros por Grupo
        /// </summary>
        /// <returns>Retorna el Listado de Parámetros según el Grupo enviado como parámetro</returns> 
        public List<ParametroDTO> ListarParametrosPorGrupo(int codGrupo)
        {
            ParametroDTO objEntidad = null;
            List<ParametroDTO> lstParametro = new List<ParametroDTO>();
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("[dbo].[UP_PAR_Listar_Parametros_x_Grupo]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@iCodGrupo";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = codGrupo;
                    cmd.Parameters.Add(param);
                    
                    try
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                objEntidad = new ParametroDTO();
                                if (!Convert.IsDBNull(reader["iCodGrupo"])) objEntidad.CodGrupo = Convert.ToInt32(reader["iCodGrupo"]);
                                if (!Convert.IsDBNull(reader["iCodParametro"])) objEntidad.CodParametro = Convert.ToInt32(reader["iCodParametro"]);
                                if (!Convert.IsDBNull(reader["vCampo"])) objEntidad.Campo = Convert.ToString(reader["vCampo"]);
                                if (!Convert.IsDBNull(reader["siEstado"])) objEntidad.Estado = short.Parse(reader["siEstado"].ToString());
                                if (!Convert.IsDBNull(reader["vValor"])) objEntidad.Valor = Convert.ToString(reader["vValor"].ToString());
                                lstParametro.Add(objEntidad);
                            }

                        }
                    }
                    catch(Exception ex){
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                        //Generando Archivo Log
                        new LogWriter(ex.Message);
                        throw new Exception("Error al Obtener el Listado de Parámetros por Grupo");
                    }
                    finally
                    {
                        conn.Close();
                        conn.Dispose();
                        cmd.Dispose();
                    }
                   
                }
            }
            return lstParametro;
        }

        #endregion
    }
}
