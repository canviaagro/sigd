﻿using Agrobanco.SIGD.Entidades.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
   public interface ITipoDocumentoDAO
    {
        /// <summary>
        /// Interface para el Listado de Tipo de Documentos Activos
        /// </summary>
        /// <returns>Retorna Lista genérica de Tipos de Documentos Activos</returns>     
        List<TipoDocumentoDTO> ListarTipoDocumento();
    }
}
