﻿using Agrobanco.SIGD.Entidades.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
   public interface IParametroDAO
    {
        /// <summary>
        /// Interface para obtener Parametros por Grupo
        /// </summary>
        /// <returns>Retorna el Listado de Parámetros según el Grupo enviado como parámetro</returns>     
        List<ParametroDTO> ListarParametrosPorGrupo(int codGrupo);
    }
}
