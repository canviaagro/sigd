﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IRepresentanteDAO
    {
        /// <summary>
        /// Método Público para el buscar Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        List<Representante> BuscarRepresentante(FiltroMantenimientoBaseDTO filtro, out int totalRegistros);

        /// <summary>
        /// Método Público para el obtener un(a) Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        Representante ObtenerRepresentante(Representante input);

        /// <summary>
        /// Método Público para Registrar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Registrar(Representante objRepresentante);

        /// <summary>
        /// Método Público para Actualizar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Actualizar(Representante objRepresentante);

        /// <summary>
        /// Método Público para Eliminar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Eliminar(Representante objRepresentante);

        /// <summary>
        /// Método Público para Eliminar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        Representante ValidarRepresentantePorNumDocumento(Representante objRepresentante);
    }
}
