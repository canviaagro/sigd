﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
   public interface ISecuenciaDAO
    {

        /// <summary>
        /// Método Público para listar Secuencias
        /// </summary>
        /// <returns>Retorna Lista genérica de Secuencias</returns>  
        List<Secuencia> ListarSecuencia(FiltroMantenimientoBaseDTO filtro, out int totalRegistros);
        /// <summary>
        /// Método Público registrar Secuencias
        /// </summary>
        /// <returns>Retorna indicador Registro de Secuencias</returns>  
        int RegistrarSecuencia(SecuenciaDTO objDocumento);
        /// <summary>
        /// Método Público actualizar Secuencias
        /// </summary>
        /// <returns>Retorna indicador actualizacion de Secuencias</returns>  
        int ActualizarSecuencia(SecuenciaDTO objDocumento);
        /// <summary>
        /// Método Público actualizar Secuencias
        /// </summary>
        /// <returns>Retorna indicador actualizacion de Secuencias</returns>  
        int EliminarSecuencia(SecuenciaDTO objDocumento);
        /// <summary>
        /// Método Público registrar Secuencias
        /// </summary>
        /// <returns>Retorna indicador Registro de Secuencias</returns>  
        SecuenciaDTO ObtenerSecuencia(SecuenciaDTO objDocumento);
        /// <summary>
        /// Método Público obtener correlativo
        /// </summary>
        /// <returns>Retorna correlativo</returns>  
        String ObtenerCorrelativo(SecuenciaDTO objDocumento);

    }
}
