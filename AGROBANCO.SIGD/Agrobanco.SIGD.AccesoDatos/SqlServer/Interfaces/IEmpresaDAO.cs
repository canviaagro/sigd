﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IEmpresaDAO
    {
        /// <summary>
        /// Interface para el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna el Identificador de la Nueva Empresa</returns>     
        int RegistrarEmpresa(Empresa objEmpresa);

        /// <summary>
        /// Interface para el Listado de Empresas
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresas</returns>     
        List<EmpresaDTO> ListarEmpresa();

        /// <summary>
        /// Interface para el Listado de Representantes por Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Representantes por Empresa</returns>     
        List<RepresentanteDTO> ListarRepresentantePorEmpresa(int codEmpresa);

        /// <summary>
        /// Método Público para el Listado de Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        List<Empresa> BuscarEmpresa(FiltroMantenimientoBaseDTO filtro, out int totalRegistros);

        /// <summary>
        /// Método Público para el obtener un(a) Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        Empresa ObtenerEmpresa(Empresa input);

        /// <summary>
        /// Método Público para Registrar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Registrar(Empresa objEmpresa);

        /// <summary>
        /// Método Público para Actualizar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Actualizar(Empresa objEmpresa);

        /// <summary>
        /// Método Público para Eliminar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Eliminar(Empresa objEmpresa);

    }
}
