﻿using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IMaeTrabajadorDAO
    {
        /// <summary>
        /// Interface para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>     
        EMaeTrabajador GetTrabajador(EMaeTrabajador EObj);
        /// <summary>
        /// Interface para el Listado de Trabajadores Por Area
        /// </summary>
        /// <returns>Retorna Lista genérica de Trabajador por Area</returns>  
        List<TrabajadorDTO> GetTrabajadorPorArea(EMaeTrabajador EObj);
        List<TrabajadorDTO> GetTrabajadorPorAreaSIED();
        List<TrabajadorDTO> GetTrabajadores();
        /// <summary>
        /// Interface para el Listado de Trabajadores
        /// </summary>
        /// <returns>Retorna Lista genérica de Trabajadores</returns>     
        List<TrabajadorDTO> ListarTrabajador();

        List<TrabajadorDTO> ListarTrabajadorPaginado(TrabajadorDTO objParams, out int totalRegistros);

        TrabajadorDTO ObtenerTabajadorPorId(TrabajadorDTO objParams); 

         TrabajadorDTO ObtenerTabajadorPorWEBUSR(TrabajadorDTO objParams);

        int RegistrarTrabajador(TrabajadorDTO objArea);

        int ActualizarTrabajador(TrabajadorDTO objArea);

        int EliminarTrabajador(TrabajadorDTO objArea);

        List<TrabajadorDTO> ListarTipDocIdentidad();
    }
}
