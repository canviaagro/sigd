﻿using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.SIGD.Entidades.DTO;


namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
   public interface ITrabajadorDAO
    {

        /// <summary>
        /// Interface para el Listado de Trabajador
        /// </summary>
        /// <returns>Retorna Trabajador genérica de trabajador</returns>     
        List<TrabajadorDTO> ListarTrabajador();

        List<TrabajadorDTO> ListarTrabajadorPaginado(TrabajadorDTO objParams, out int totalRegistros);

        TrabajadorDTO ObtenerTabajadorPorId(TrabajadorDTO objParams);

        int RegistrarTrabajador(TrabajadorDTO objTrabajador);

        int ActualizarTrabajador(TrabajadorDTO objTrabajador);

        int EliminarTrabajador(TrabajadorDTO objTrabajador);
    }
}
