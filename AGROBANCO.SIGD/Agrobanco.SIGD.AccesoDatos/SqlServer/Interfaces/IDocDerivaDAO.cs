﻿using Agrobanco.SIGD.Entidades.Entidades;
using System;       
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface IDocDerivaDAO
    {
        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        List<DocDerivacion> GetDocumentoDerivado(DocDerivacion EObj);
        /// <summary>
        /// Método Público para el Obtener Datos de un Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        List<DocDerivacion> GetDocumentoDerivadoEmpresasSIED(DocDerivacion EObj);
        /// <summary>
        /// Método Público para el Registro de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento Derivación</returns>  
        /// <summary>
        /// Método Público para el Registro de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento Derivación</returns>  
        int RegistrarDocumentoDerivacion(DocDerivacion objDocDerivacion);
        /// <summary>
        /// Método Público para el Inactivar de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de Inactivar del Documento Derivación</returns>  
        /// <summary>
        /// Método Público para Inactivar de un Documento Derivación
        /// </summary>
        /// <returns>Retorna el Indicador de Inactivar del Documento Derivación</returns>  
        int InactivarDocumentoDerivacion(DocDerivacion objDocDerivacion, out string outCorrelativoDoc);

        int AnularDocumentoDerivacionSinComentario(DocDerivacion docDerivacion, string comentario);
        int AnularDocumentoDerivacion(DocDerivacion docDerivacion, string comentario);
    }
}
