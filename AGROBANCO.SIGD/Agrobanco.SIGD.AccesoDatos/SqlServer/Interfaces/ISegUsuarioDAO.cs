﻿using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces
{
    public interface ISegUsuarioDAO
    {
        /// <summary>
        /// Interface para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>     
        ESegUsuario GetUsuario(ESegUsuario EObj);
    }
}
