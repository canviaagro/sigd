﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegPerfil
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegPerfil()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public ESegPerfil ObtenerSegPerfil(ESegPerfil EObj)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("P_VCODPERFIL", 20, EObj.vCodPerfil);
            lDataParam[1] = _db2DataContext.CreateParameter("P_VCODSISTEMA", 20, EObj.vCodSistema);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGPERFIL_QRY01", out IDataReader reader, lDataParam);
            ESegPerfil _EObj = new ESegPerfil();
            while (reader.Read())
            {
                _EObj.vCodPerfil = reader.GetString("VCODPERFIL");
                _EObj.vCodSistema = reader.GetString("VCODSISTEMA");
                _EObj.vNombre = reader.GetString("VNOMBRE");
                _EObj.vDescripcion = reader.GetString("VDESCRIPCION");
            }

            return _EObj;
        }

    }
}
