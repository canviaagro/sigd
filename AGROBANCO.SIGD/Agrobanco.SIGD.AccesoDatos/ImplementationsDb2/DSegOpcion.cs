﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegOpcion
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegOpcion()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public ESegOpcion ObtenerSegOpcion(ESegOpcion EObj)
        {
            var lDataParam = new iDB2Parameter[3];
            lDataParam[0] = _db2DataContext.CreateParameter("P_VCODOPCION", 20, EObj.vCodOpcion);
            lDataParam[1] = _db2DataContext.CreateParameter("P_VCODMODULO", 20, EObj.vCodModulo);
            lDataParam[2] = _db2DataContext.CreateParameter("P_VCODSISTEMA", 20, EObj.vCodSistema);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGOPCION_QRY01", out IDataReader reader, lDataParam);
            ESegOpcion _EObj = new ESegOpcion();
            while (reader.Read())
            {
                _EObj.vCodOpcion = reader.GetString("VCODOPCION");
                _EObj.vCodModulo = reader.GetString("VCODMODULO");
                _EObj.vCodSistema = reader.GetString("VCODSISTEMA");
                _EObj.vNombre = reader.GetString("VNOMBRE");
                _EObj.vDescripcion = reader.GetString("VDESCRIPCION");
                _EObj.iNivel = reader.GetIntOrNull("INIVEL");
                _EObj.vCodOpcPadre = reader.GetString("VCODOPCPADRE");
                _EObj.vURL = reader.GetString("VURL");
                break;
            }

            return _EObj;
        }

    }
}
