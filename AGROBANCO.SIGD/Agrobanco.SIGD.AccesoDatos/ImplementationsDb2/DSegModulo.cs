﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegModulo
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegModulo()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public ESegModulo ObtenerSegModulo(ESegModulo EObj)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("P_CODMODULO", 20, EObj.vCodModulo);
            lDataParam[1] = _db2DataContext.CreateParameter("P_VCODSISTEMA", 20, EObj.vCodSistema);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGMODULO_QRY01", out IDataReader reader, lDataParam);
            ESegModulo _EObj = new ESegModulo();
            while (reader.Read())
            {
                _EObj.vCodModulo = reader.GetString("VCODMODULO");
                _EObj.vCodSistema = reader.GetString("VCODSISTEMA");
                _EObj.vNombre = reader.GetString("VNOMBRE");
                _EObj.vDescripcion = reader.GetString("VDESCRIPCION");
                _EObj.iNivel = reader.GetIntOrNull("INIVEL");
                _EObj.vCodModPadre = reader.GetString("VCODMODPADRE");
                _EObj.vURL = reader.GetString("VURL");
                break;
            }

            return _EObj;
        }

    }
}
