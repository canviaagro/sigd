﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class CntrlwebData
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public CntrlwebData()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public ECntrlweb LoginUsuario(ELoginRequest login)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("P_USU_LOGIN", 50, login.Username);
            lDataParam[1] = _db2DataContext.CreateParameter("P_USU_PW", 50, login.Password);
            
            // aqui agregar la contraseña
            _db2DataContext.RunStoredProcedure(library + ".PA_CNTRLWEB_LOGIN", out IDataReader reader, lDataParam);
            ECntrlweb ObjECntrlweb = new ECntrlweb();
            while (reader.Read())
            {
                ObjECntrlweb.webusr = reader.GetString("WEBUSR");
                ObjECntrlweb.webacp = login.Password;
                break;
            }

            return ObjECntrlweb;
        }
        public ECntrlweb ConsultarUsuario(ELoginRequest login)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("P_USU_LOGIN", 50, login.Username);
            // aqui agregar la contraseña
            _db2DataContext.RunStoredProcedure(library + ".PA_CNTRLWEB_CONSULTAR", out IDataReader reader, lDataParam);
            ECntrlweb ObjECntrlweb = new ECntrlweb();
            while (reader.Read())
            {
                ObjECntrlweb.webusr = reader.GetString("WEBUSR");
                break;
            }

            return ObjECntrlweb;
        }

    }
}
