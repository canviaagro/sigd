﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegPermiso
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegPermiso()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public List<ESegPermiso> ObtenerPermiso(ESegPermiso EObj)
        {
            var lDataParam = new iDB2Parameter[2];
            lDataParam[0] = _db2DataContext.CreateParameter("P_VCODPERFIL", 20, EObj.vCodPerfil);
            lDataParam[1] = _db2DataContext.CreateParameter("P_VCODSISTEMA", 20, EObj.vCodSistema);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGPERMISO_QRY01", out IDataReader reader, lDataParam);
            ESegPermiso _EObj = new ESegPermiso();
            List<ESegPermiso> _ListEObj = new List<ESegPermiso>();
            while (reader.Read())
            {
                _EObj = new ESegPermiso();
                _EObj.vCodPermiso = reader.GetString("VCODPERMISO");
                _EObj.vCodOpcion = reader.GetString("VCODOPCION");
                _EObj.vCodModulo = reader.GetString("VCODMODULO");
                _EObj.vCodSistema = reader.GetString("VCODSISTEMA");
                _EObj.vCodAccion = reader.GetString("VCODACCION");
                _EObj.vCodPerfil = reader.GetString("VCODPERFIL");
                _EObj.Modulo = new ESegModulo();
                _EObj.Modulo.vCodModulo = reader.GetString("VCODMODULO"); ;
                _EObj.Modulo.vNombre = reader.GetString("VNOMBREMODULO");
                _EObj.Modulo.vURL = reader.GetString("VURLMODULO");
                _EObj.Modulo.vEstiloIcono = reader.GetString("vEstiloIconoModulo");
                _EObj.Opcion = new ESegOpcion();
                _EObj.Opcion.vNombre = reader.GetString("VNOMBREOPCION");
                _EObj.Opcion.vURL = reader.GetString("VURLOPCION");
                _EObj.Opcion.vEstiloIcono = reader.GetString("vEstiloIconoOpcion");
                _EObj.Permitido = (reader.GetInt16("Permitido") == 1 ? true : false);

                _ListEObj.Add(_EObj);
            }

            return _ListEObj;
        }

    }
}
