﻿using Agrobanco.SIGD.AccesoDatos.DataDb2;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.Data;

namespace Agrobanco.SIGD.AccesoDatos.ImplementationsDb2
{
    public class DSegAccion
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public DSegAccion()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = ApplicationKeys.Db2Esquema };
            library = ApplicationKeys.Db2_Library;
        }

        public ESegAccion ObtenerSegAccion(ESegAccion EObj)
        {
            var lDataParam = new iDB2Parameter[1];
            lDataParam[0] = _db2DataContext.CreateParameter("P_VCODACCION", 20, EObj.vCodAccion);
            _db2DataContext.RunStoredProcedure(library + ".PA_SEGACCION_QRY01", out IDataReader reader, lDataParam);
            ESegAccion _EObj = new ESegAccion();
            while (reader.Read())
            {
                _EObj.vCodAccion = reader.GetString("VCODACCION");
                _EObj.vNombre = reader.GetString("VNOMBRE");
                _EObj.vDescripcion = reader.GetString("VDESCRIPCION");
                break;
            }

            return _EObj;
        }

    }
}
