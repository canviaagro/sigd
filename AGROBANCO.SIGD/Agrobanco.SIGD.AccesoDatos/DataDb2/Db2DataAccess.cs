﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data;
using System.Xml;
//using Agrobanco.PlataformaEvaluacionMasiva.Infraestructure.Settings;
//using Agrobanco.PlataformaEvaluacionMasiva.Persistence.Common;
using Agrobanco.Security.Criptography;
using Agrobanco.SIGD.Comun;
using IBM.Data.DB2.iSeries;
using Microsoft.Win32;

namespace Agrobanco.SIGD.AccesoDatos.DataDb2
{
    public class Db2DataAccess : IDataAccess
    {
        private readonly string _connectionString;
#pragma warning disable CS0649
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
#pragma warning restore CS0649
        private iDB2Connection _dbConnection;

        [NonSerialized]
        private iDB2DataAdapter _dbDataAdapter;

        [NonSerialized]
        private iDB2Command _dbCommand;
        private bool _isAutoConnectionMgmt = false;

        [NonSerialized]
        private iDB2Transaction _dbTransaction;

        [NonSerialized]
        private iDB2CommandBuilder _dbCommandBuilder;


        public bool AutoConnection
        {
            get => (_isAutoConnectionMgmt);
            set => _isAutoConnectionMgmt = value;
        }

        public string DbEsquema { get; set; }

        private int Db2ApplicationNameEnableEncrip = Convert.ToInt16(ApplicationKeys.Db2ApplicationNameEnableEncrip);
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;

        public Db2DataAccess()
        {
            try
            {
                var conectionStringBuilder = new iDB2ConnectionStringBuilder();
                _isAutoConnectionMgmt = true;
                Compose();
                _decriptService.Application = ApplicationKeys.Db2RegeditFolder;
                _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;

                if (Db2ApplicationNameEnableEncrip == EnableEncriptHab)
                {
                    conectionStringBuilder = new iDB2ConnectionStringBuilder
                    {
                        DataSource = _decriptService.ReadValue("Server"),
                        Database = _decriptService.ReadValue("Database"),
                        UserID = _decriptService.ReadValue("Usuario"),
                        Password = _decriptService.ReadValue("Clave"),
                      /*    DataSource = ApplicationKeys.Db2_Server,
                         Database = ApplicationKeys.Db2_DataBase,
                         UserID = ApplicationKeys.Db2_Usuario,
                         Password = ApplicationKeys.Db2_Password,   */
                        //MaximumPoolSize = 4,
                        //CheckConnectionOnOpen=true                                
                        Pooling = false// --desabilitando demoraria 

                    };
                }else if (Db2ApplicationNameEnableEncrip == EnableEncriptDes)
                {
                    conectionStringBuilder = new iDB2ConnectionStringBuilder
                    {
                        DataSource = ReadKey("Server"),
                        Database = ReadKey("Database"),
                        UserID = ReadKey("Usuario"),
                        Password = ReadKey("Clave"),
                        Pooling = false
                    };
                }
                 _connectionString = conectionStringBuilder.ConnectionString;
                this.AutoConnection = true;
            }
            catch (Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "Conexion DB2" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
        }

        private  string ReadKey(string value)
        {
            Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\"+ ApplicationKeys.Db2RegeditFolder);
            string valueret = "";
            if (masterKey != null)
            {
                valueret = masterKey.GetValue(value).ToString();
            }

            masterKey.Close();
            return valueret;
        }

        ~Db2DataAccess()
        {
            this.Dispose();
        }


        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void OpenConnection()
        {
            try
            {
                if (_dbConnection != null)
                {
                    if (_dbConnection.State != ConnectionState.Open)
                    {
                        _dbConnection = new iDB2Connection(_connectionString);
                        _dbConnection.Open();
                    }
                    else
                    {
                        // else nothing
                    }
                }
                else
                {
                    _dbConnection = new iDB2Connection(_connectionString);
                    _dbConnection.Open();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void CloseConnection()
        {
            try
            {
                if (_dbConnection != null)
                {
                    if (_dbConnection.State != ConnectionState.Closed)
                    {
                        _dbConnection.Close();
                    }
                    else
                    {
                        // else nothing
                    }
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataSet(string strDbQuery, out DataSet dsResult)
        {
            dsResult = new DataSet();

            try
            {
                this.OpenConnection();

                _dbCommand = new iDB2Command(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataSet(string strDbQuery, DataSet dsResult)
        {
            try
            {
                this.OpenConnection();

                _dbCommand = new iDB2Command(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataTable(string strDbQuery, out DataTable dtResult)
        {
            dtResult = new DataTable();
            try
            {
                this.OpenConnection();
                _dbCommand = new iDB2Command(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataTable(string strDbQuery, DataTable dtResult)
        {
            try
            {
                this.OpenConnection();
                _dbCommand = new iDB2Command(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetDataReader(string strDbQuery, out IDataReader drResult)
        {
            drResult = null;
            iDB2Connection dbConnection = new iDB2Connection(this._connectionString);

            iDB2Command dbCommand = new iDB2Command(strDbQuery, dbConnection);
            try
            {
                dbCommand.CommandType = CommandType.Text;

                dbCommand.Connection.Open();

                drResult = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception)
            {
                if (dbCommand.Connection.State.Equals(ConnectionState.Open))
                    dbCommand.Connection.Close();
                throw;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetScalarValue(string strDbQuery, out string strResult)
        {
            strResult = string.Empty;
            try
            {
                this.OpenConnection();

                _dbCommand = new iDB2Command(strDbQuery, _dbConnection);

                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                strResult = _dbCommand.ExecuteScalar().ToString();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void GetXmlReader(string strDbQuery, out XmlReader xmlReaderResult)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void ExecuteQuery(string strDbQuery, out int intRowsAffected)
        {
            intRowsAffected = 0;

            try
            {
                this.OpenConnection();

                _dbCommand = new iDB2Command(strDbQuery, _dbConnection);
                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }

                intRowsAffected = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void BatchUpdate(DataSet dataSetToBeUpdated)
        {
            if (this._dbDataAdapter != null)
            {
                this.OpenConnection();

                if (_dbTransaction != null)
                {
                    if (_dbDataAdapter.UpdateCommand != null)
                    {
                        _dbDataAdapter.UpdateCommand.Connection = _dbConnection;
                        _dbDataAdapter.UpdateCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.DeleteCommand != null)
                    {
                        _dbDataAdapter.DeleteCommand.Connection = _dbConnection;
                        _dbDataAdapter.DeleteCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.InsertCommand != null)
                    {
                        _dbDataAdapter.InsertCommand.Connection = _dbConnection;
                        _dbDataAdapter.InsertCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.SelectCommand != null)
                    {
                        _dbDataAdapter.SelectCommand.Connection = _dbConnection;
                        _dbDataAdapter.SelectCommand.Transaction = _dbTransaction;
                    }
                }
                else
                {
                    // else nothing
                }

                this._dbDataAdapter.Update(dataSetToBeUpdated);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }

            }
            else
            {
                throw new Exception("The Data Access Component Object was destroyed, Dababase could not be updated.");
            }
        }

        public void BatchUpdate(DataTable dataTableToBeUpdated)
        {
            if (this._dbDataAdapter != null)
            {
                this.OpenConnection();

                if (_dbTransaction != null)
                {
                    if (_dbDataAdapter.UpdateCommand != null)
                    {
                        _dbDataAdapter.UpdateCommand.Connection = _dbConnection;
                        _dbDataAdapter.UpdateCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.DeleteCommand != null)
                    {
                        _dbDataAdapter.DeleteCommand.Connection = _dbConnection;
                        _dbDataAdapter.DeleteCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.InsertCommand != null)
                    {
                        _dbDataAdapter.InsertCommand.Connection = _dbConnection;
                        _dbDataAdapter.InsertCommand.Transaction = _dbTransaction;
                    }
                    if (_dbDataAdapter.SelectCommand != null)
                    {
                        _dbDataAdapter.SelectCommand.Connection = _dbConnection;
                        _dbDataAdapter.SelectCommand.Transaction = _dbTransaction;
                    }
                }
                else
                {
                    // else nothing
                }

                this._dbDataAdapter.Update(dataTableToBeUpdated);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }

            }
            else
            {
                throw new Exception("The Data Access Component Object was destroyed, Dababase could not be updated.");
            }
        }

        public int MannualBatchUpdate(DataTable dataTableToBeUpdated, IDbCommand insertCommand, IDbCommand updateCommand, IDbCommand deleteCommand)
        {
            this.OpenConnection();

            _dbDataAdapter = new iDB2DataAdapter();

            if (insertCommand != null)
            {
                _dbDataAdapter.InsertCommand = (iDB2Command)insertCommand;
                _dbDataAdapter.InsertCommand.Connection = this._dbConnection;
                if (_dbTransaction != null)
                {
                    _dbDataAdapter.InsertCommand.Transaction = _dbTransaction;
                }
            }
            if (updateCommand != null)
            {
                _dbDataAdapter.UpdateCommand = (iDB2Command)updateCommand;
                _dbDataAdapter.UpdateCommand.Connection = this._dbConnection;
                if (_dbTransaction != null)
                {
                    _dbDataAdapter.UpdateCommand.Transaction = _dbTransaction;
                }
            }
            if (deleteCommand != null)
            {
                _dbDataAdapter.DeleteCommand = (iDB2Command)deleteCommand;
                _dbDataAdapter.DeleteCommand.Connection = this._dbConnection;
                if (_dbTransaction != null)
                {
                    _dbDataAdapter.DeleteCommand.Transaction = _dbTransaction;
                }
            }

            int intAffectedRows = 0;
            intAffectedRows = _dbDataAdapter.Update(dataTableToBeUpdated);

            if (_isAutoConnectionMgmt)
            {
                this.CloseConnection();
            }
            return intAffectedRows;
        }

        public IDbCommand CreateCommand(string strQueryOrSp, CommandType commandType)
        {
            IDbCommand command = new iDB2Command();
            if (strQueryOrSp != null && !strQueryOrSp.Trim().Equals(string.Empty))
            {
                command.CommandText = strQueryOrSp;
            }
            else
            {
                throw new InvalidOperationException("Invalid Command Text.");
            }

            if (!commandType.Equals(null))
            {
                command.CommandType = commandType;
            }
            else
            {
                throw new InvalidOperationException("Invalid Command Type.");
            }

            return (IDbCommand)command;
        }

        public IDataParameter CreateParameter(string paramName, DbType paramType, int paramSize, ParameterDirection paramDirection, string sourceColumn)
        {
            if (paramName == null || paramName.Trim().Equals(string.Empty))
            {
                throw new InvalidOperationException("Invalid Parameter Name.");
            }

            if (paramType.Equals(null))
            {
                throw new InvalidOperationException("Invalid Parameter Type.");
            }

            if (paramDirection.Equals(null))
            {
                paramDirection = ParameterDirection.Input;
            }

            iDB2Parameter parameter = new iDB2Parameter();
            parameter.ParameterName = paramName;
            parameter.DbType = paramType;
            parameter.Size = paramSize;
            parameter.Direction = paramDirection;

            if (sourceColumn != null && !sourceColumn.Trim().Equals(string.Empty))
            {
                parameter.SourceColumn = sourceColumn;
            }

            return ((IDbDataParameter)parameter);

        }

        private void CreateCommand(string strProcedureName, iDB2Parameter[] arrDbParameter, int time_out = 30)
        {
            try
            {
                this.OpenConnection();
                _dbCommand = new iDB2Command(strProcedureName, _dbConnection);
                _dbCommand.CommandType = CommandType.StoredProcedure;
                _dbCommand.CommandTimeout = time_out;
                if (_dbTransaction != null)
                {
                    _dbCommand.Transaction = _dbTransaction;
                }
                else
                {
                    // else nothing
                }

                if (arrDbParameter != null)
                {
                    foreach (iDB2Parameter parameterLoop in arrDbParameter)
                    {
                        _dbCommand.Parameters.Add(parameterLoop);
                    }
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName)
        {
            try
            {
                this.CreateCommand(strProcedureName, null);
                _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }
        public void RunStoredProcedurex(string strProcedureName, IDataParameter[] arrDbParameter)
        {
            try
            {

                //string connString = "DataSource=SYSTEM;UserID=USER;Password=PASSWORD";
                //iDB2Connection conn = new iDB2Connection(connString);
                //conn.Open();

                string cmdString = "CRTPF FILE(AGRCYFILES/WLFW001) RCDLEN(100)";
                string commandText = "CALL QSYS.QCMDEXC('" + cmdString + "', " + cmdString.Length.ToString("0000000000") + ".00000" + ")";

                //iDB2Command cmd = new iDB2Command(cmdText, conn);
                //cmd.ExecuteNonQuery();
                //cmd.Dispose();

                //conn.Close();


                //var commandText = $"AGRCYFILES.WLFW001";
                this.CreateCommand(commandText, (iDB2Parameter[])arrDbParameter);

                int n = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }
        public void RunStoredProcedure1(string strProcedureName, IDataParameter[] arrDbParameter, int time_out)
        {
            try
            {
                //var commandText = $"{DbEsquema}.{strProcedureName}";
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter, time_out);

                int n = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, IDataParameter[] arrDbParameter)
        {
            try
            {
                //var commandText = $"{DbEsquema}.{strProcedureName}";
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }
        public string RunPrograma(string strProcedureName, string par1, string par2)
        {
            string resp = "";
            try
            {
                //var commandText = $"{DbEsquema}.{strProcedureName}";
                //var commandText =strProcedureName;
                //this.CreateCommand(commandText, (iDB2Parameter[])arrDbParameter);

                //this.OpenConnection();
                //_dbCommand = new iDB2Command(strProcedureName, _dbConnection);

                //this.CreateCommand(strProcedureName, CommandType.StoredProcedure);
                //_dbCommand.Parameters.Add("@PARM1", iDB2DbType.iDB2Char, 336);
                //_dbCommand.Parameters["@PARM1"].Value = cabecera;
                //_dbCommand.Parameters["@PARM1"].Direction = ParameterDirection.InputOutput;
                //_dbCommand.Parameters.Add("@PARM2", iDB2DbType.iDB2Char, 30000);
                //_dbCommand.Parameters["@PARM2"].Value = detalle;
                //_dbCommand.Parameters["@PARM2"].Direction = ParameterDirection.InputOutput;
                // _dbCommand.ExecuteNonQuery();
                //  string resp = _dbCommand.Parameters["@PARM2"].Value.ToString();
                this.OpenConnection();
                using (var cmd = _dbConnection.CreateCommand())
                {
                    // _dbConnection.Open();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = strProcedureName;// "AGRUSRLIB.T20000";
                    cmd.Parameters.Add("@PARAM1", iDB2DbType.iDB2Char, 336);
                    cmd.Parameters["@PARAM1"].Value = par1;
                    cmd.Parameters["@PARAM1"].Direction = ParameterDirection.InputOutput;
                    cmd.Parameters.Add("@PARAM2", iDB2DbType.iDB2Char, 30000);
                    cmd.Parameters["@PARAM2"].Value = par2;
                    cmd.Parameters["@PARAM2"].Direction = ParameterDirection.InputOutput;
                    cmd.ExecuteNonQuery();
                    resp = cmd.Parameters["@PARAM2"].Value.ToString();
                }

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
            return resp;
        }
        public void RunStoredProcedure(string strProcedureName, out int intAffectedRows)
        {
            intAffectedRows = 0;
            try
            {
                this.CreateCommand(strProcedureName, null);
                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }
        public void RunStoredProcedureExec(string strProcedureName, out int intAffectedRows, IDataParameter[] arrDbParameter)
        {
            intAffectedRows = 0;
            try
            {
                //var commandText = $"{DbEsquema}.{strProcedureName}";
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);
                // this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }



        public void RunStoredProcedure(string strProcedureName, out int intAffectedRows, IDataParameter[] arrDbParameter)
        {
            intAffectedRows = 0;
            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out int intAffectedRows, IDataParameter[] arrDbParameter, int intCommandTimeOut)
        {
            //int intOldCommandTimeOut;
            intAffectedRows = 0;
            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);
                intAffectedRows = _dbCommand.ExecuteNonQuery();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out object objScalarResult)
        {
            objScalarResult = null;
            try
            {
                this.CreateCommand(strProcedureName, null);
                objScalarResult = _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out object objScalarResult, IDataParameter[] arrDbParameter)
        {
            objScalarResult = null;
            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                objScalarResult = _dbCommand.ExecuteScalar();

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out IDataReader drResult)
        {
            drResult = null;

            try
            {
                //var commandText = $"{DbEsquema}.{strProcedureName}";
                iDB2Connection dbConnection = new iDB2Connection(this._connectionString);
                iDB2Command dbCommand = new iDB2Command(strProcedureName, dbConnection);

                dbCommand.CommandType = CommandType.StoredProcedure;

                dbCommand.Connection.Open();

                drResult = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out IDataReader drResult, IDataParameter[] arrDbParameter)
        {
            drResult = null;

            try
            {
                //var commandText = $"{DbEsquema}.{strProcedureName}";
                iDB2Connection dbConnection = new iDB2Connection(this._connectionString);
                iDB2Command dbCommand = new iDB2Command(strProcedureName, dbConnection);

                dbCommand.CommandType = CommandType.StoredProcedure;

                if (arrDbParameter != null)
                {
                    foreach (iDB2Parameter parameterLoop in arrDbParameter)
                    {
                        dbCommand.Parameters.Add(parameterLoop);
                    }
                }

                dbCommand.Connection.Open();
                drResult = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataSet dsResult)
        {
            dsResult = new DataSet();

            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataSet dsResult)
        {
            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataSet dsResult, IDataParameter[] arrDbParameter)
        {
            dsResult = new DataSet();
            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataSet dsReturn, IDataParameter[] arrDbParameter)
        {
            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dsReturn);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataTable dtReturn)
        {
            dtReturn = new DataTable();
            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtReturn);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataTable dtResult)
        {
            try
            {
                this.CreateCommand(strProcedureName, null);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out DataTable dtResult, IDataParameter[] arrDbParameter)
        {
            dtResult = new DataTable();

            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, DataTable dtResult, IDataParameter[] arrDbParameter)
        {
            try
            {
                this.CreateCommand(strProcedureName, (iDB2Parameter[])arrDbParameter);

                _dbDataAdapter = new iDB2DataAdapter();

                _dbDataAdapter.SelectCommand = _dbCommand;

                _dbCommandBuilder = new iDB2CommandBuilder(_dbDataAdapter);

                _dbDataAdapter.Fill(dtResult);

                if (_isAutoConnectionMgmt)
                {
                    this.CloseConnection();
                }
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void RunStoredProcedure(string strProcedureName, out XmlReader xmlReaderResult)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void RunStoredProcedure(string strProcedureName, out XmlReader xmlReaderResult, IDataParameter[] arrDbParameter)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void BeginTransaction(IsolationLevel objIsoLevel)
        {
            try
            {
                this.OpenConnection();
                _dbTransaction = _dbConnection.BeginTransaction(objIsoLevel);
            }
            catch (InvalidOperationException except)
            {
                throw except;
            }
            catch (iDB2Exception except)
            {
                throw except;
            }
            catch (Exception except)
            {
                throw except;
            }
        }

        public void BeginTransaction(string strNameOfTransaction, IsolationLevel objIsoLevel)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void RollbackTransaction()
        {
            if (_dbTransaction != null)
            {
                try
                {
                    _dbTransaction.Rollback();
                }
                catch (InvalidOperationException except)
                {
                    throw except;
                }
                catch (iDB2Exception except)
                {
                    throw except;
                }
                catch (Exception except)
                {
                    throw except;
                }
            }
            else
            {
                Exception objException = new Exception("Transaction Object was disposed, Can not Rollback");
                throw objException;
            }
        }

        public void RollbackTransaction(string strNameOfTransactionOrSavePoint)
        {
            throw new Exception("This method is not supported by DBClient.");
        }

        public void CommitTransaction()
        {
            if (_dbTransaction != null)
            {
                try
                {
                    _dbTransaction.Commit();
                }
                catch (InvalidOperationException except)
                {
                    throw except;
                }
                catch (iDB2Exception except)
                {
                    throw except;
                }
                catch (Exception except)
                {
                    throw except;
                }
            }
            else
            {
                Exception objException = new Exception("Transaction Object was disposed, Can not Commit");
                throw objException;
            }
        }

        public void SetSavePoint(string strSavePointName)
        {
            throw new Exception("This method is not supported by DBClient.");
        }



        internal iDB2Parameter CreateParameter(string name, int size, object value = null, int scale = -1, iDB2DbType dataType = iDB2DbType.iDB2VarChar, ParameterDirection direction = ParameterDirection.Input)
        {
            var parameter = new iDB2Parameter(name, dataType);

            if (size != -1)
                parameter.Size = size;

            if (scale != -1)
                parameter.Scale = Convert.ToByte(scale);

            parameter.Direction = direction;
            //parameter.Value = value;

            parameter.Value = SetDbNullIfDefaultValue(value, dataType);
            return parameter;
        }

        private object SetDbNullIfDefaultValue(object value, iDB2DbType dataType)
        {
            object returnValue = DBNull.Value;
            switch (dataType)
            {
                case iDB2DbType.iDB2Clob:
                    returnValue = value;
                    break;
                case iDB2DbType.iDB2Date:
                    returnValue = value ?? DBNull.Value;
                    break;
                case iDB2DbType.iDB2Decimal:
                    returnValue = value;
                    break;
                case iDB2DbType.iDB2Integer:
                    returnValue = value;
                    break;
                case iDB2DbType.iDB2Numeric:
                    returnValue = value;
                    break;
                case iDB2DbType.iDB2VarChar:
                    returnValue = string.IsNullOrEmpty((string)value) ? DBNull.Value : value;
                    break;
            }
            return returnValue;
        }
    }
}
