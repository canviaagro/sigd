﻿using System;
using System.Configuration;

namespace Agrobanco.SIGD.AccesoDatos.DataDb2
{
    public static class ApplicationKeys
    {

        /*INICIO--------------PARAMETROS DB2-------------*/

        public static string Db2_Server => ConfigurationManager.AppSettings["Server"];
        public static string Db2_DataBase => ConfigurationManager.AppSettings["Database"];
        public static string Db2_Usuario => ConfigurationManager.AppSettings["Usuario"];
        public static string Db2_Password => ConfigurationManager.AppSettings["Password"];

        public static string Db2_Library => ConfigurationManager.AppSettings["Library"];

        /*FIN--------------PARAMETROS DB2-------------*/

        public static string Db2ApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:Db2ApplicationNameEnableEncrip"];
        public static string Db2RegeditFolder => ConfigurationManager.AppSettings["config:Db2ApplicationName"];
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];

        public static string RutaTemporales => ConfigurationManager.AppSettings["config:RutaTemporales"];

        public static string FtpServer => ConfigurationManager.AppSettings["config:FtpServerImagen"];
        public static string FtpUser => ConfigurationManager.AppSettings["config:FtpUser"];
        public static string FtpPassword => ConfigurationManager.AppSettings["config:FtpPassword"];
        public static string CadenaConexionExcel => ConfigurationManager.AppSettings["Excel"];


        public static string MaximoFilas => ConfigurationManager.AppSettings["MaxFilasFI"];


        
        public static string PlantillaAgrupamiento => ConfigurationManager.AppSettings["config:Agrupamiento"];

        public static string PlantillaProspecto => ConfigurationManager.AppSettings["config:ProspectoPlantilla"];
        public static string VersionProspecto => ConfigurationManager.AppSettings["config:ProspectoVersion"];

        public static string FlujoCajaHojaFlujoCaja => ConfigurationManager.AppSettings["config:CargarFlujoCaja"];
        public static string FlujoCajaHojaResumen => ConfigurationManager.AppSettings["config:CargarFlujoCajaResumen"];


        public static string LaserFicheRegeditFolder => ConfigurationManager.AppSettings["config:LFApplicationName"];
        public static string LaserFicheRepository => ConfigurationManager.AppSettings["config:LfRepositorio"];
        public static string LaserFicheFolderBase => ConfigurationManager.AppSettings["config:LfFolderDocument"];
        public static string LaserFicheTemplateName => ConfigurationManager.AppSettings["config:LfTemplate"];


        public static string Db2Esquema => ConfigurationManager.AppSettings["config:DbEschema"];
        public static string ws_usuario => ConfigurationManager.AppSettings["config:usuario_ws"];
        public static string ws_clave => ConfigurationManager.AppSettings["config:clave_ws"];
        public static string ws_key => ConfigurationManager.AppSettings["config:key_ws"];
        public static string ws_servicio => ConfigurationManager.AppSettings["config:servicio_ws"];
        public static string ws_ruta => ConfigurationManager.AppSettings["config:ruta_log"];
        public static string ws_urlapi => ConfigurationManager.AppSettings["config:url_apiMEG"];

        public static string FirmaRepositoryPath => ConfigurationManager.AppSettings["config:FirmaRepoPath"];
    }

}
