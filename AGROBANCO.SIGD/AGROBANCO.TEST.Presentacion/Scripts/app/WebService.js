﻿$(document).ready(function () {
    var id = 1;
    var adjuntos = [];
    $("#btnRegistrar").click(function (e) {
        e.preventDefault(); 
        $("#divregistrar").hide();
        $("#divloading").show();
        adjuntos = [];
        getValuesAdjuntosDinamicos();
        id = 1;
        var tramiteDocumentario = {};
        tramiteDocumentario.TipoEntidad = $("#TipoEntidad").val();
        tramiteDocumentario.Entidad = $("#Entidad").val();
        tramiteDocumentario.NumeroDocumento = $("#NumeroDocumento").val();
        tramiteDocumentario.FechaDocumento = $("#FechaDocumento").val();
        tramiteDocumentario.DirigidoA = $("#DirigidoA").val();
        tramiteDocumentario.Referencia = $("#Referencia").val();
        tramiteDocumentario.Asunto = $("#Asunto").val();
        tramiteDocumentario.Prioridad = $("#Prioridad").val();
        tramiteDocumentario.Plazo = $("#Plazo").val();
        tramiteDocumentario.Indicacion = $("#Indicacion").val();
        tramiteDocumentario.OtrasIndicaciones = $("#OtrasIndicaciones").val();
        tramiteDocumentario.anexos = adjuntos;
        $.ajax({
            url: "/Home/registrarDocumentoSIED",
            method: "POST",
            data: tramiteDocumentario,
            success: function (data) {
                alert(data);
                location.reload();
                $("#divloading").hide();
                $("#divregistrar").show();
            },
            error: function () {
                alert("Ajax call failed");
                $("#divloading").hide();
                $("#divregistrar").show();
            }
        })

    });


    $("#btnAgregarAdjuntos").click(function (e) {
        
        e.preventDefault();
        var adjhtml = '<div class="divadjuntos" id="divadjuntos'+id+'"><div class="form-group row">' +
            '<label for= "inputEmail3" class= "col-sm-2 col-form-label">Almacen Archivo ' + id +'</label>' +
            '<div class="col-sm-10">' +
            '<input class="form-control AlmacenArchivo" id="AlmacenArchivo'+id+'">' +
            '</div>' +
            '</div>' +
            '<div class="form-group row">' +
            '<label for="inputEmail3" class="col-sm-2 col-form-label">Descripcion ' + id +'</label>' +
            '<div class="col-sm-10">' +
            '<input class="form-control Descripcion" id="Descripción'+id+'">' +
            ' </div>' +
            ' </div>' +
            ' <div class="form-group row">' +
            '  <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre Físico ' + id +'</label>' +
            '   <div class="col-sm-10">' +
            '      <input class="form-control NombreFisico" id="NombreFisico'+id+'">' +
            ' </div>' +
            '</div></div>';
        id++;
        $("#divAdjuntosAdicionales").append(adjhtml);
    });

    function getValuesAdjuntosDinamicos() {
        for (var x = 1; x <= $(".divadjuntos").length; x++) {
            var obj = {
                AlmacenArchivo: $("#AlmacenArchivo"+x).val(),
                Descripcion: $("#Descripcion" + x).val(),
                NombreFIsico: $("#NombreFisico" + x).val()
            };
            adjuntos.push(obj);
        }
    }
});
