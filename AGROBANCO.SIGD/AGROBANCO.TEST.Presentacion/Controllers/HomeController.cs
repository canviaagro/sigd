﻿using Agrobanco.SIGD.Entidades.WebService;
using System.Web.Mvc;
using ItemAnexo = AGROBANCO.TEST.Presentacion.WSDocumentoSIGD.ItemAnexo;

namespace AGROBANCO.TEST.Presentacion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.----test";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult registrarDocumentoSIED(TramiteDocumentario tramiteDocumentario)
        {
            WSDocumentoSIGD.WSDocumentoSIGD objService = new WSDocumentoSIGD.WSDocumentoSIGD();
            ItemAnexo[] Anexos = new ItemAnexo[tramiteDocumentario.Anexos.Count];
            for (var x =0;x< tramiteDocumentario.Anexos.Count;x++)
            {
                Anexos[x] = new ItemAnexo
                {
                    AlmacenArchivo = tramiteDocumentario.Anexos[x].AlmacenArchivo,
                    Descripcion = tramiteDocumentario.Anexos[x].Descripcion,
                    NombreFIsico = tramiteDocumentario.Anexos[x].NombreFIsico
                };
                
            }
            
            return Json(objService.WM_RegistrarTransaccion(tramiteDocumentario.TipoEntidad, tramiteDocumentario.Entidad, tramiteDocumentario.NumeroDocumento,
                                                            tramiteDocumentario.FechaDocumento, tramiteDocumentario.DirigidoA, tramiteDocumentario.Referencia, tramiteDocumentario.Asunto,
                                                            tramiteDocumentario.Prioridad, tramiteDocumentario.Plazo, tramiteDocumentario.Indicacion, tramiteDocumentario.OtrasIndicaciones,
                                                            Anexos));
        }
    }
}