﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Web;
using System.Web.Script.Serialization;

namespace Agrobanco.SIGD.WebService.ConsumoAPI
{
    public  class PeticionesAPI
    {
        public static UsuarioSIED GetUsuarioSIED()
        {
            ServicePointManager.ServerCertificateValidationCallback = new
            RemoteCertificateValidationCallback
            (
               delegate { return true; }
            );
            UsuarioSIED obj = new UsuarioSIED();
            var url = ConfigurationManager.AppSettings["WebApiUrl"]+ "UsuarioSIEDExternal/ObtenerUsuarioSIEDExternal";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return obj;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();

                            JavaScriptSerializer js = new JavaScriptSerializer();
                            UsuarioSIED usObject = js.Deserialize<UsuarioSIED>(responseBody);

                            obj.WebUsr = usObject.WebUsr;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                GenerarLog objLOG = new GenerarLog();
                string detalleError = method + " - " +
                    DateTime.Now + "\n" + "\n" +
                    ex.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
 
            }
            return obj;
        }

    }
}