﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Comun.SFTP;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades.WebService;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.WebService.ConsumoAPI;
using Agrobanco.SIGD.WebService.Util;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using static Agrobanco.SIGD.Comun.Enumerados;
using static Agrobanco.SIGD.WebService.Util.ConstantesWS;

namespace Agrobanco.SIGD.WSDocumentoSIGD
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://royalsystems.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSDocumentoSIGD : System.Web.Services.WebService
    {

        [WebMethod]
        public string WM_RegistrarTransaccion(string TipoEntidad, string Entidad, string NumeroDocumento, string FechaDocumento,
                                              string DirigidoA, string Referencia, string Asunto, string Prioridad, string Plazo,
                                              string Indicacion, string OtrasIndicaciones, List<ItemAnexo> Anexos)
        {      
           try {
                TramiteDocumentario TramiteDocumentario = new TramiteDocumentario();
                TramiteDocumentario.TipoEntidad = TipoEntidad;
                TramiteDocumentario.Entidad = Entidad;
                TramiteDocumentario.NumeroDocumento = NumeroDocumento;
                TramiteDocumentario.FechaDocumento = FechaDocumento;
                TramiteDocumentario.DirigidoA = DirigidoA;
                TramiteDocumentario.Referencia = Referencia;
                TramiteDocumentario.Asunto = Asunto;
                TramiteDocumentario.Prioridad = Prioridad;
                TramiteDocumentario.Plazo = Plazo;
                TramiteDocumentario.Indicacion = Indicacion;
                TramiteDocumentario.OtrasIndicaciones = OtrasIndicaciones;
                TramiteDocumentario.Anexos = Anexos;

                Context.Response.AddHeader("Host", Context.Request.Headers["Host"]);
                Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                if (validarRegistroDocumento(TramiteDocumentario, out string adjuntoprincipalVal) == "")
                 {
                    int repOnFlagOk = 0, newTPflagOK = 0;
                    string tipDocDesconocido = "TIPO DOCUMENTO DESCONOCIDO – FONAFE";
                    string newTipoDocumento = "";
                     var webUsr = PeticionesAPI.GetUsuarioSIED().WebUsr;
                     TrabajadorDTO objTrab = new TrabajadorDTO();
                     Representante objRep = new Representante();
                     objRep.NumDocumento = TramiteDocumentario.Entidad;
                     objTrab.WEBUSR = webUsr;
                     Documento objDoc = new Documento();
                     RepresentanteBL objRepBl = new RepresentanteBL();
                     DocumentoBL objDocBL = new DocumentoBL();
                     EmpresaBL objEmpBL = new EmpresaBL();
                     var plazovalido = Convert.ToInt16(IsNumeric(String.IsNullOrWhiteSpace(TramiteDocumentario.Plazo) ?"0": TramiteDocumentario.Plazo) == false ? "0" : (String.IsNullOrWhiteSpace(TramiteDocumentario.Plazo) ? "0" : TramiteDocumentario.Plazo)); 
                     DateTime fechaPlazo = objDocBL.CalcularFechaPlazoDocumento(plazovalido, DateTime.Today);
                     TrabajadorBL objTrabbl = new TrabajadorBL();
                     objTrab = objTrabbl.ObtenerTabajadorPorWEBUSR(objTrab);
                    if (String.IsNullOrWhiteSpace(TramiteDocumentario.NumeroDocumento))
                    {
                         newTipoDocumento = tipDocDesconocido;
                    }
                    else
                    {
                        newTipoDocumento= validarTipoDocumento( TramiteDocumentario.NumeroDocumento);
                    }
                     
                    TipoDocumentoDTO objTD = new TipoDocumentoDTO();
                    objTD.Descripcion = newTipoDocumento;
                     var existeTipoDocumento = objDocBL.ObtenerTipoDocumentoPorDescripcion(objTD);
                    if (existeTipoDocumento==null)
                    {
                        newTPflagOK = Convert.ToInt32(ConfigurationManager.AppSettings["config:TipoDocumentoNoValido"]);
                        /*TipoDocumentoDTO objTPDTO1 = new TipoDocumentoDTO();
                        objTPDTO1.TextoBusqueda = "";
                        objTPDTO1.Pagina = 1;
                        objTPDTO1.TamanioPagina = int.MaxValue;
                        var ocurrencia = objDocBL.ListarTiposDocumento(objTPDTO1,out int TOTAL);
                        
                        TipoDocumentoDTO objTPDTO = new TipoDocumentoDTO();
                        objTPDTO.Descripcion = String.IsNullOrWhiteSpace(TramiteDocumentario.NumeroDocumento)==true? tipDocDesconocido : newTipoDocumento;
                        objTPDTO.Abreviatura = "TIP"+ (TOTAL+1);
                        objTPDTO.UsuarioCreacion = webUsr;
                        objTPDTO.Estado = Convert.ToInt16(Estado.Activo);
                        newTPflagOK = objDocBL.RegistrarTipoDocumento(objTPDTO);*/
                    }
                     if (String.IsNullOrWhiteSpace(TramiteDocumentario.Entidad))
                     {
                         repOnFlagOk = 0;
                     }
                     else {
                         var existeRepresentante = objRepBl.ValidarRepresentantePorNumDocumento(objRep);
                         repOnFlagOk = existeRepresentante.CodRepresentante;
                     }

                     if (repOnFlagOk == 0)
                     {
                        repOnFlagOk = Convert.ToInt32(ConfigurationManager.AppSettings["config:RemitenteNoValido"]);
                        /*Empresa empNew = new Empresa();
                        empNew.Descripcion = "REMITENTE DESCONOCIDO – FONAFE";
                        empNew.Direccion = "DIRECCION DESCONOCIDA – FONAFE";
                        empNew.Estado = Convert.ToInt16(Estado.Activo);
                        empNew.UsuarioCreacion = webUsr;
                        var empOnFlagOk = objEmpBL.Registrar(empNew);

                        Representante repNew = new Representante();
                        repNew.NumDocumento = TramiteDocumentario.Entidad;
                        repNew.Nombres = "REPRESENTANTE DESCONOCIDO – FONAFE";
                        repNew.Estado = Convert.ToInt16(Estado.Activo);
                        repNew.TipRepresentante = Convert.ToInt16(TipoRepresentante.JURIDICO);
                        repNew.CodEmpresa = empOnFlagOk;
                        repNew.CodTipDocIdentidad = Convert.ToInt16(TipoDocumentoIden.RUC);
                        repNew.UsuCreacion = webUsr;
                        repOnFlagOk = objRepBl.Registrar(repNew);*/
                    }
                     DateTime myDateTime = DateTime.Now;
                     objDoc.Anio = myDateTime.Year;
                     objDoc.Origen = Convert.ToInt16(TipoOrigen.EXTERNO);
                     objDoc.NumDocumento = TramiteDocumentario.NumeroDocumento;
                     objDoc.EstadoDocumento = Convert.ToInt16(EstadoDocumento.PENDIENTE);
                     objDoc.FechaDocumento = Convert.ToDateTime(validarFechaDocumento(TramiteDocumentario.FechaDocumento));
                     objDoc.Asunto = TramiteDocumentario.Asunto;
                     objDoc.Prioridad = Convert.ToInt16(validarPrioridad(TramiteDocumentario.Prioridad));
                     objDoc.Referencia ="SIED DIRIGIADO A :"+ TramiteDocumentario.DirigidoA+ "<br/> " + TramiteDocumentario.Referencia;
                     objDoc.CodArea = objTrab.iCodArea;
                     objDoc.CodTrabajador = objTrab.iCodTrabajador;
                     objDoc.FechaRecepcion = DateTime.Today;
                     objDoc.Plazo = Convert.ToInt16(plazovalido);
                     objDoc.FechaPlazo = fechaPlazo;
                     objDoc.Observaciones = "INDICACIÓN FONAFE :" + validarIndicacion(TramiteDocumentario.Indicacion) + "<br/> OTRAS INDICACIONES :" + TramiteDocumentario.OtrasIndicaciones;
                     objDoc.CodTipoDocumento = existeTipoDocumento==null?Convert.ToInt16(newTPflagOK): Convert.ToInt16(existeTipoDocumento.CodTipoDocumento);
                     objDoc.CodRepresentante = repOnFlagOk;
                     objDoc.UsuarioCreacion = webUsr;
                     objDoc.FechaCreacion = DateTime.Today;
                     objDoc.LoginCreacion = webUsr;
                     objDoc.LstDocAdjunto = new List<DocAdjunto>();
                     objDoc.LstDocAnexo = new List<DocAnexo>();
                     if (TramiteDocumentario.Anexos != null)
                     {
                         objDoc.LstDocAdjunto.Add(new DocAdjunto { FileArray = SFTP.DowloadFile(adjuntoprincipalVal) });
                         for (var i = 0; i < TramiteDocumentario.Anexos.Count; i++)
                         {
                             if (validarAnexos(TramiteDocumentario.Anexos[i].NombreFIsico))
                             {
                                var pesoAnexo = SFTP.getSizeFile(TramiteDocumentario.Anexos[i].NombreFIsico) ;
                                if ((pesoAnexo <= 5 * 1024 * 1024) && (pesoAnexo >0)) {
                                    objDoc.LstDocAnexo.Add(new DocAnexo
                                    {
                                        FileArray = SFTP.DowloadFile(TramiteDocumentario.Anexos[i].NombreFIsico),
                                        NombreDocumento = nombreArchivo(TramiteDocumentario.Anexos[i].NombreFIsico)
                                    });
                                }
                             }
                         }
                     }

                     int rpta = objDocBL.RegistrarDocumentoRecepcionSIED(objDoc, out string correlativo, out int RespuestaCorreo);
                      

                     return rpta.ToString();
                 }
                 else
                 {
                     return validarRegistroDocumento(TramiteDocumentario,out string adjuntoprincipalVal2);
                 }
             }
             catch (Exception e)
             {
                 StackTrace stackTrace = new StackTrace();
                 var method = stackTrace.GetFrame(0).GetMethod().Name;
                 GenerarLog objLOG = new GenerarLog();
                 string detalleError = method + " - " +
                     DateTime.Now + "\n" + "\n" +
                     e.Message + "\n" +
                     "-------------------------------------------------------------------------------------------------------------";
                 objLOG.GenerarArchivoLog(detalleError);
                 return e.Message;
             }
        }







        private Boolean validarAnexos(string nombre)
        {
            var esAnexo = false;
            string fileName = nombre;
            int fileExtPos = fileName.LastIndexOf(".");
            if (fileExtPos >= 0)
                fileName = fileName.Substring(0, fileExtPos);

            var carEspecial = fileName.IndexOf("_");
            if(carEspecial != -1)
            {
                for (var i = carEspecial+1; i < fileName.Length; i++)
                {
                    if (IsNumeric(fileName[i].ToString()))
                    {
                        esAnexo = true;
                    }
                    else
                    {
                        esAnexo = false;
                        break;
                    }
                }
            }
            return esAnexo;
        }

        private bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }

        private string nombreArchivo(string path)
        {
            var nombreArchivo = "";
            var carEspecial = path.LastIndexOf("/");
            if (carEspecial != -1)
            {
                for (var i = carEspecial + 1; i < path.Length; i++)
                {
                    nombreArchivo = nombreArchivo + path[i];
                }
            }
            return nombreArchivo;
        }

        private string validarRegistroDocumento(TramiteDocumentario TramiteDocumentario,out string adjuntoPrincialVal)
        {
            adjuntoPrincialVal = "";
            var msgValdacion = "";
            var ocurrenciasAdjPrincipal = 0;          
            string adjuntoPrincipal = "";
            var adjunto = "";
            decimal sizeFilePrincipal = 0;
            //validando doc principal sea pdf y no existe mas de 1
            if (TramiteDocumentario.Anexos != null)
            {
                for (var i = 0; i < TramiteDocumentario.Anexos.Count; i++)
                {
                    adjunto = TramiteDocumentario.Anexos[i].NombreFIsico;
                    if (!validarAnexos(adjunto))
                    {
                        ocurrenciasAdjPrincipal++;
                        if (ocurrenciasAdjPrincipal == 1)
                        {
                            adjuntoPrincipal = adjunto;
                            adjuntoPrincialVal = adjuntoPrincipal;
                            sizeFilePrincipal = SFTP.getSizeFile(adjuntoPrincipal);
                        }
                    }/*anexos son opcionales
                    else
                    {
                        decimal sizeFile = 0;
                        sizeFile = SFTP.getSizeFile(adjunto);
                        if (sizeFile >= 5 * 1024 * 1024)
                        {
                            msgValdacion = Convert.ToInt16(AdjuntoPrincipalPeso.codigoError).ToString();
                        }
                    }*/
                }
            }
            if(ocurrenciasAdjPrincipal == 0 )
            {
                msgValdacion = Convert.ToInt16(AdjuntoPrincipalSinAdjunto.codigoError).ToString();
                return msgValdacion;
            }
            if (ocurrenciasAdjPrincipal >= 1 )
            {
              string extension = "";
              int fileExtPos = adjuntoPrincipal.LastIndexOf(".");
              if (fileExtPos >= 0)
                   extension = adjuntoPrincipal.Substring(fileExtPos,adjuntoPrincipal.Length-fileExtPos );
                if (extension.ToUpper() != ".PDF")
                {
                    msgValdacion = Convert.ToInt16(AdjuntoPrincipalFormatoIncorrecto.codigoError).ToString();
                    return msgValdacion;
                }
            }
            if (sizeFilePrincipal >= 5 * 1024 * 1024)
            {
                msgValdacion = Convert.ToInt16(AdjuntoPrincipalPeso.codigoError).ToString();
                return msgValdacion;
            }
            else if(sizeFilePrincipal == 0)
            {
                msgValdacion = Convert.ToInt16(AdjuntoPrincipalNoExiste.codigoError).ToString();
                return msgValdacion;
            }
            return msgValdacion;
        }

        private string validarFechaDocumento(string date)
        {
            DateTime d;

            bool chValidity = DateTime.TryParseExact(
               date,
               "dd/MM/yyyy",
               CultureInfo.InvariantCulture,
               DateTimeStyles.None,
               out d);
            if (!chValidity)
            {
                return DateTime.Today.ToShortDateString();
            }
            else
            {
                return date;
            }
        }

        private string validarPrioridad(string valor)
        {
            IParametroBL objServicioParam = new ParametroBL();
            var lst = objServicioParam.ListarParametrosPorGrupo(Convert.ToInt16(GrupoParametros.PRIORIDAD));
            var lstExist = lst.Where(c => c.CodParametro.ToString() == valor).ToList();
            if(lstExist.Count > 0)
            {
                return valor;
            }
            else
            {
                return  Convert.ToInt16(EnumPrioridad.NORMAL).ToString();
            }
        }

        private string validarIndicacion(string valor)
        {
            IParametroBL objServicioParam = new ParametroBL();
            var lst = objServicioParam.ListarParametrosPorGrupo(Convert.ToInt16(GrupoParametros.INDICACIONES));
            var lstExist = lst.Where(c => c.CodParametro.ToString() == valor).ToList();
            if (lstExist.Count > 0)
            {
                var campo = lstExist.Where(y => y.CodParametro.ToString() == valor).ToList();
                return campo[0].Campo;
            }
            else
            {
                var campo = lst.Where(z => z.CodParametro == (Convert.ToInt16(Indicacion.OTROS))).ToList();
                return campo[0].Campo;
            }
        }

        private string validarTipoDocumento(string valor)
        {
            string cadena = "NRO";
            int posicion = valor.ToUpper().IndexOf(cadena);
            int newposicion = valor.ToUpper().Substring(0, posicion).ToString().IndexOf("SIED");
            return valor.ToUpper().Substring(0, newposicion).TrimEnd();
        }
    }
}
