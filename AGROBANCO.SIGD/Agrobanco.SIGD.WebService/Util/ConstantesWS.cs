﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.WebService.Util
{
    public class ConstantesWS
    {
        public static string AdjuntoPrincipalFormatoIncorrectoMsg = "El adjunto principal tiene que ser un PDF";
        public static string AdjuntoPrincipalPesoMsg = "El adjunto principal no debe superar los 5MB";
        public static string AdjuntoPrincipalSinAdjuntoMsg = "La trama debe contener un adjunto principal";
        public static string AdjuntoPrincipalNoExisteMsg = "El adjunto principal no existe en la ruta especificada";

        public enum AdjuntoPrincipalFormatoIncorrecto
        {
            codigoError = -100,
        }

        public enum AdjuntoPrincipalPeso
        {
            codigoError = -110,
        }

        public enum AdjuntoPrincipalSinAdjunto
        {
            codigoError = -120,
        }

        public enum AdjuntoPrincipalNoExiste
        {
            codigoError = -130,
        }
    }
}