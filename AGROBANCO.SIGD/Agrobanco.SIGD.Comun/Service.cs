﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using System.Net.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using System.Web.Script.Serialization;

namespace Agrobanco.SIGD.Comun
{
    public class Service
    {
        private string _response = string.Empty;
        static HttpClient client = new HttpClient();

        static Service()
        {
            //client.BaseAddress = new Uri("https://localhost:44307/api/");
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["WebApiUrl"].ToString());            
            //client.DefaultRequestHeaders.Add("Authorization", "Bearer", "");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public string Response { get { return _response; } }

        public T HttpGetJson<T>(string url, string autorization)
        {
            _response = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "GET";
                req.ContentType = "application/json";
                string token = autorization;
                req.Headers.Add("Authorization", token);
                
                req.Timeout = 600000;
                //timeout
              
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                _response = _json;

                //dynamic ActvEco_json_ = JObject.Parse(_json.ToString()); //sirve para ordenar el json
                //var data_ = ActvEco_json_.Permisos;
                //_response = ActvEco_json_.ToString();
                T _lista = JsonComponent.Deserialize<T>(_response.ToString());
                
                 return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public T HttpGetJsonExternal<T>(string url)
        {
            _response = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "GET";
                req.ContentType = "application/json";
                //string token = autorization;
                //req.Headers.Add("Authorization", token);

                req.Timeout = 600000;
                //timeout

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                _response = _json;

                //dynamic ActvEco_json_ = JObject.Parse(_json.ToString()); //sirve para ordenar el json
                //var data_ = ActvEco_json_.Permisos;
                //_response = ActvEco_json_.ToString();
                T _lista = JsonComponent.Deserialize<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public T HttpGetJsonExternalCredentials<T>(string url,string usuario, string clave)
        {
            _response = string.Empty;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );

                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                // string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("SWSIED_USER:1234567"));
                string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(usuario+":"+clave));
                GenerarLog g = new GenerarLog();
                g.GenerarArchivoLog("CREDENCIALES " + credentials);
                req.Headers.Add("Authorization", "Basic " + credentials);
                req.Method = "GET";
                req.ContentType = "application/json";
                //string token = autorization;
                //req.Headers.Add("Authorization", token);

                req.Timeout = 600000;
                //timeout

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                g.GenerarArchivoLog("httperesponse " + res.ToString());
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                g.GenerarArchivoLog("JSON " + _json);
                _response = _json;

                //dynamic ActvEco_json_ = JObject.Parse(_json.ToString()); //sirve para ordenar el json
                //var data_ = ActvEco_json_.Permisos;
                //_response = ActvEco_json_.ToString();
                T _lista = JsonComponent.Deserialize<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                GenerarLog g = new GenerarLog();
                g.GenerarArchivoLog("ERROR " + ex.Message);
                g.GenerarArchivoLog("ERROR2 " + ex);
                throw ex;
            }
        }



        public T HttpPostJsonExternalCredentials<T>(string url, Object data, string usuario, string clave)
        {
            _response = string.Empty;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
                RemoteCertificateValidationCallback
                (
                   delegate { return true; }
                );
                var jsonData = new JavaScriptSerializer().Serialize(data);
                // Prueba de obtención de alumno vía HTTP GET
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                // string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("SWSIED_USER:1234567"));
                string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(usuario + ":" + clave));
                req.Headers.Add("Authorization", "Basic " + credentials);
                req.Method = "post";
                req.ContentType = "application/json";
                
                //string token = autorization;
                //req.Headers.Add("Authorization", token);

                req.Timeout = 600000;
                //timeout

                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    string json = jsonData;

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    _response = streamReader.ReadToEnd();
                }
                //dynamic ActvEco_json_ = JObject.Parse(_json.ToString()); //sirve para ordenar el json
                //var data_ = ActvEco_json_.Permisos;
                //_response = ActvEco_json_.ToString();
                T _lista = JsonComponent.Deserialize<T>(_response.ToString());

                return _lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<T> PostAsyncExternal<T>(object doc, string apiURL)
        {
            //client = new HttpClient();

            T result = default(T);

            HttpResponseMessage response = await client.PostAsJsonAsync(
                apiURL, doc);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();
            }
            else
            {
                throw new ApplicationException(string.Format("Response message is not OK. Issues in action: {0}", apiURL));
            }

            return result;
        }

        public static async Task<T> PostAsync<T>(object doc, string apiURL, string token)
        {
            //

            T result = default(T);
            if (!string.IsNullOrEmpty(token))
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", token);
            }
            HttpResponseMessage response = await client.PostAsJsonAsync(
                apiURL, doc);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();
            }
            else
            {
                throw new ApplicationException(string.Format("Response message is not OK. Issues in action: {0}", apiURL));
            }

            return result;
		}

        public static async Task<T> PostFormDataAsync<T>(MultipartFormDataContent formData, string apiURL, string token)
        {
            //client = new HttpClient();

            T result = default(T);
            if (!string.IsNullOrEmpty(token))
            {
                client.DefaultRequestHeaders.Add("Authorization", token);
            }

            HttpResponseMessage response = await client.PostAsync(
                apiURL, formData);
            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();
            }
            else
            {
                throw new ApplicationException(string.Format("Response message is not OK. Issues in action: {0}", apiURL));
            }

            return result;
        }

        public static async Task<T> PostAsync<T>(string apiURL, string token)
        {
            //client.DefaultRequestHeaders.Add("Authorization", token);
            HttpResponseMessage response = await client.PostAsJsonAsync(
                apiURL, string.Empty);
            T result = default(T);

            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();
            }
                        
            return result;
        }

        public static async Task<T> GetAsync<T>(string apiURL, string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", token);
            }
            HttpResponseMessage response = await client.GetAsync(apiURL).ConfigureAwait(false);
            T result = default(T);

            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();                
            }
            else
            {
                throw new ApplicationException(string.Format("Response message is not OK. Issues in action: {0}", apiURL));
            }

            return result;
        }


        public static async Task<T> GetAsyncExternal<T>(string apiURL)
        {
            HttpResponseMessage response = await client.GetAsync(apiURL).ConfigureAwait(false);
            T result = default(T);

            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<T>();
            }
            else
            {
                throw new ApplicationException(string.Format("Response message is not OK. Issues in action: {0}", apiURL));
            }

            return result;
        }

        public string HttpPostJson(string url, string body, string token = "")
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new
               RemoteCertificateValidationCallback
               (
                  delegate { return true; }
               );

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "application/json";
                //req.Headers.Add("Authorization", token);
                req.Timeout = 600000;
                StreamWriter requestWriter = new StreamWriter(req.GetRequestStream());

                requestWriter.Write(body);
                requestWriter.Close();
                requestWriter = null;

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string _json = reader.ReadToEnd();
                return _json;

            }
            catch (Exception ex)
            {
                throw ex;
                //return ex.Message;
            }

        }
    }
}
