﻿using Agrobanco.Security.Criptography;
using Agrobanco.SIGD.Comun;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Utilities
{
    public class EnvioCorreo
    {
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
        private int CorreoEnableEncript = Convert.ToInt16(ApplicationKeys.CorreoApplicationNameEnableEncrip);
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;

        public void CorreoAcces(out string _from, out string _userName, out string _password, out string _hostName, out int _port)
        {
            _from = "";
            _userName = "";
            _password = "";
            _hostName = "";
            _port = 0;
            try
            {
                Compose();
                _decriptService.Application = ApplicationKeys.CorreoApplicationName;
                _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;
                if (CorreoEnableEncript == EnableEncriptHab)
                {
                    _from = _decriptService.ReadValue("From");// _decriptService.ReadValue("Usuario");
                    _userName = _decriptService.ReadValue("Usuario");// _decriptService.ReadValue("Usuario");
                    _password = _decriptService.ReadValue("Clave"); //_decriptService.ReadValue("Clave");
                    _hostName = _decriptService.ReadValue("Host");//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(_decriptService.ReadValue("Puerto"));

                }
                else if (CorreoEnableEncript == EnableEncriptDes)
                {
                    _from = ReadKey("From", ApplicationKeys.CorreoApplicationName);// _decriptService.ReadValue("Usuario");
                    _userName = ReadKey("Usuario", ApplicationKeys.CorreoApplicationName);// _decriptService.ReadValue("Usuario");
                    _password = ReadKey("Clave", ApplicationKeys.CorreoApplicationName); //_decriptService.ReadValue("Clave");
                    _hostName = ReadKey("Host", ApplicationKeys.CorreoApplicationName);//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(ReadKey("Puerto", ApplicationKeys.CorreoApplicationName));
                }
            }
            catch (Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "Conexion CORREO" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
        }

        private void Compose()
        {
            var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
            var catalog = new AggregateCatalog(encryptCatalog);
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }



        public int EnviarCorreo(List<String> para, List<String> copia, string cuerpo)
        {
          CorreoAcces(out string _from, out string _userName, out string _password, out string _hostName, out int _port);

          int rpta = -1;
          MailMessage message = new MailMessage();
          message.From = new MailAddress(_from);
            try
           {
               var body = cuerpo;
               foreach(var item in para)
                {
                    message.To.Add(new MailAddress(item));
                }
                if (copia != null)
                {
                    foreach (var item in copia)
                    {
                        message.CC.Add(new MailAddress(item));
                    }
                }
               
               message.Subject = ConfigurationManager.AppSettings["ASUNTO_CORREO"].ToString();
                message.Body = body;
               message.IsBodyHtml = true;

                var smtpClient = new SmtpClient(_hostName)
                {
                    Port = _port,
                    Credentials = new NetworkCredential(_userName, _password)
                };
                smtpClient.Send(message);
                rpta = 1;
            }
            catch (Exception e)
           {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "Envio Correo" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLogApi(detalleError);
               return rpta = -1;
           }
           return rpta;
       }


        private string ReadKey(string value, string folder)
        {
            Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\" + folder);
            string valueret = "";
            if (masterKey != null)
            {
                valueret = masterKey.GetValue(value).ToString();
            }
            masterKey.Close();
            return valueret;
        }
    }
}
