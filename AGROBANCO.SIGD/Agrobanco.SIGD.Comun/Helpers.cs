﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Comun
{
    public static class Helpers
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
    (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);

            return result;
        }

        public static void ConvertToModel<T>(FormCollection form, T obj)
        {
            foreach (string item in form.AllKeys)
            {
                string keyItem = char.ToUpper(item[0]).ToString() + item.Substring(1);

                PropertyInfo prop = obj.GetType().GetProperty(keyItem);

                if (prop != null)
                {
                    var valorInput = form[item];

                    if (!string.IsNullOrEmpty(valorInput))
                    {
                        TypeCode type = System.Type.GetTypeCode(prop.PropertyType);

                        switch (type)
                        {
                            case TypeCode.Int16:
                                prop.SetValue(obj, Convert.ToInt16(valorInput), null);
                                break;
                            case TypeCode.Int32:
                                prop.SetValue(obj, Convert.ToInt32(valorInput), null);
                                break;
                            case TypeCode.Int64:
                                prop.SetValue(obj, Convert.ToInt64(valorInput), null);
                                break;
                            case TypeCode.Decimal:
                                prop.SetValue(obj, Convert.ToDecimal(valorInput), null);
                                break;
                            case TypeCode.String:
                                prop.SetValue(obj, Convert.ToString(valorInput), null);
                                break;
                            case TypeCode.DateTime:

                                DateTime fecha = DateTime.ParseExact(valorInput, "dd/MM/yyyy",
                                    System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                prop.SetValue(obj, fecha, null);
                                break;
                        }
                        //prop.SetValue(obj, valorInput, BindingFlags.Public, Type., null, System.Globalization.CultureInfo.CurrentCulture);

                    }
                }
            }
        }
        public static void ConvertToModelV2<T>(FormCollection form, T obj)
        {
            foreach (string item in form.AllKeys)
            {
                string keyItem = char.ToUpper(item[0]).ToString() + item.Substring(1);
                string keyItemClean = keyItem.Replace("Edit", string.Empty).Trim();
                //keyItemClean = keyItemClean.Replace("_RA", string.Empty).Trim();

                PropertyInfo prop = obj.GetType().GetProperty(keyItemClean);

                if (prop != null)
                {
                    var valorInput = form[item];

                    if (!string.IsNullOrEmpty(valorInput))
                    {
                        TypeCode type = System.Type.GetTypeCode(prop.PropertyType);

                        switch (type)
                        {
                            case TypeCode.Int16:
                                prop.SetValue(obj, Convert.ToInt16(valorInput), null);
                                break;
                            case TypeCode.Int32:
                                prop.SetValue(obj, Convert.ToInt32(valorInput), null);
                                break;
                            case TypeCode.Int64:
                                prop.SetValue(obj, Convert.ToInt64(valorInput), null);
                                break;
                            case TypeCode.Decimal:
                                prop.SetValue(obj, Convert.ToDecimal(valorInput), null);
                                break;
                            case TypeCode.String:
                                prop.SetValue(obj, Convert.ToString(valorInput), null);
                                break;
                            case TypeCode.DateTime:

                                DateTime fecha = DateTime.ParseExact(valorInput, "dd/MM/yyyy",
                                    System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                prop.SetValue(obj, fecha, null);
                                break;
                        }
                        //prop.SetValue(obj, valorInput, BindingFlags.Public, Type., null, System.Globalization.CultureInfo.CurrentCulture);

                    }
                }
            }
        }
        public static void ConvertToModelV3<T>(FormCollection form, T obj, Dictionary<string, string> reemplazarkeys = null)
        {
            foreach (string item in form.AllKeys)
            {
                string keyItem = char.ToUpper(item[0]).ToString() + item.Substring(1);
                //string keyItemClean = keyItem.Replace("Edit", string.Empty).Trim();
                //keyItemClean = keyItemClean.Replace("_RA", string.Empty).Trim();
                string keyItemClean = keyItem;
                if (reemplazarkeys != null)
                    foreach (var itemkey in reemplazarkeys)
                    {
                        keyItemClean = keyItemClean.Replace(itemkey.Key, itemkey.Value).Trim();
                    }

                PropertyInfo prop = obj.GetType().GetProperty(keyItemClean);

                if (prop != null)
                {
                    var valorInput = form[item];

                    if (!string.IsNullOrEmpty(valorInput))
                    {
                        TypeCode type = System.Type.GetTypeCode(prop.PropertyType);

                        switch (type)
                        {
                            case TypeCode.Int16:
                                prop.SetValue(obj, Convert.ToInt16(valorInput), null);
                                break;
                            case TypeCode.Int32:
                                prop.SetValue(obj, Convert.ToInt32(valorInput), null);
                                break;
                            case TypeCode.Int64:
                                prop.SetValue(obj, Convert.ToInt64(valorInput), null);
                                break;
                            case TypeCode.Decimal:
                                prop.SetValue(obj, Convert.ToDecimal(valorInput), null);
                                break;
                            case TypeCode.String:
                                prop.SetValue(obj, Convert.ToString(valorInput), null);
                                break;
                            case TypeCode.DateTime:
                                DateTime fecha = DateTime.ParseExact(valorInput, "dd/MM/yyyy",
                                    System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                prop.SetValue(obj, fecha, null);
                                break;
                            default:

                                var t = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                                var safeValue = (valorInput == null) ? null : Convert.ChangeType(valorInput, t);
                                prop.SetValue(obj, safeValue);
                                break;
                        }
                        //prop.SetValue(obj, valorInput, BindingFlags.Public, Type., null, System.Globalization.CultureInfo.CurrentCulture);

                    }
                }
            }
        }

        public static string ClearString(string searchString)
        {
            if (String.IsNullOrEmpty(searchString))
                return "";
            else
                return searchString.Replace("+", "").Replace("*", "").Replace("/", "").Replace("-", "").Trim();
        }

        public static string SerializeObject<T>(T objeto)
        {
            return JsonConvert.SerializeObject(objeto, Formatting.Indented); ;
        }
        public static T DeserializeObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
