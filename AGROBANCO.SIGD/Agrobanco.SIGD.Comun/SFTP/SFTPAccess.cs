﻿using Agrobanco.Security.Criptography;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun.SFTP
{
    public class SFTPAccess
    {
        [Import(typeof(IValueReader), AllowRecomposition = true)]
        private IValueReader _decriptService;
        private int SFTPFicheEnableEncrip = Convert.ToInt16(ApplicationKeys.SFTPApplicationNameEnableEncrip);
        private int SIEDEnableEncrip = Convert.ToInt16(ApplicationKeys.SIEDApplicationNameEnableEncrip);
        private int EnableEncriptHab = 1;
        private int EnableEncriptDes = 0;


        public void SFTPAccessConnection(out string _userName, out string _password, out string _hostName, out int _port)
        {
            _userName = "";
            _password = "";
            _hostName = "";
            _port = 0;
            try
            {
                Compose();
                _decriptService.Application = ApplicationKeys.SFTPApplicationName;
                _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;
                if (SFTPFicheEnableEncrip == EnableEncriptHab)
                {
                    _userName = _decriptService.ReadValue("Usuario");// _decriptService.ReadValue("Usuario");
                    _password = _decriptService.ReadValue("Clave"); //_decriptService.ReadValue("Clave");
                    _hostName = _decriptService.ReadValue("Host");//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(_decriptService.ReadValue("Puerto"));

                }
                else if (SFTPFicheEnableEncrip == EnableEncriptDes)
                {
                    _userName = ReadKey("Usuario", ApplicationKeys.SFTPApplicationName);// _decriptService.ReadValue("Usuario");
                    _password = ReadKey("Clave", ApplicationKeys.SFTPApplicationName); //_decriptService.ReadValue("Clave");
                    _hostName = ReadKey("Host", ApplicationKeys.SFTPApplicationName);//_decriptService.ReadValue("Server"); // "41.50.13.193";
                    _port = Convert.ToInt32(ReadKey("Puerto", ApplicationKeys.SFTPApplicationName));
                }
            }
            catch (Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "Conexion SFTP" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
        }


        public void ServicesSiedAccessConnection(out string _userName, out string _password)
        {
            _userName = "";
            _password = "";
            try{
                Compose();
                _decriptService.Application = ApplicationKeys.SIEDApplicationName;
                _decriptService.ClaveEncriptado = ApplicationKeys.RegeditPass;
                if (SIEDEnableEncrip == EnableEncriptHab)
                {
                    _userName = _decriptService.ReadValue("Usuario");
                    _password = _decriptService.ReadValue("Clave");

                }
                else if (SIEDEnableEncrip == EnableEncriptDes)
                {
                    _userName = ReadKey("Usuario", ApplicationKeys.SIEDApplicationName);
                    _password = ReadKey("Clave", ApplicationKeys.SIEDApplicationName);
                }
            }
            catch (Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "Conexion SIED" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
        }


        private void Compose()
      {
          var encryptCatalog = new AssemblyCatalog(System.Reflection.Assembly.GetAssembly(typeof(IValueReader)));
          var catalog = new AggregateCatalog(encryptCatalog);
          var container = new CompositionContainer(catalog);
          container.ComposeParts(this);
      }


    private string ReadKey(string value, string folder)
      {
          Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
          RegistryKey masterKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\AGROBANCO\\" + folder);
          string valueret = "";
          if (masterKey != null)
          {
              valueret = masterKey.GetValue(value).ToString();
          }
          masterKey.Close();
          return valueret;
      }
    }
}
