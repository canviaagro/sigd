﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun.SFTP
{
    public static class SFTP
    {
        private static string host = "";
        private static int port = 0;
        private static string username = "";
        private static string password = "";

        public static int  UploadSFTPFileFromStream(string sourcefile, string destinationpath, Stream streamFile)
        {
            int rpta = -1;
            try
            {
                SFTPAccess sftpAccess = new SFTPAccess();
                sftpAccess.SFTPAccessConnection(out username, out password, out host, out port);
                using (SftpClient client = new SftpClient(host, port, username, password))
                {
                    client.Connect();

                    if (!client.Exists(destinationpath))
                    {
                        client.CreateDirectory(destinationpath);
                    }

                    client.ChangeDirectory(destinationpath);

                    //using (FileStream fs = new FileStream()
                    //{
                    client.BufferSize = 8 * 1024;
                    client.UploadFile(streamFile, Path.GetFileName(sourcefile));
                    //client.UploadFile()
                    //}      
                    rpta = 1;
                }
            }catch(Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "SFTP" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
            return rpta;
        }

        public static string DowloadFile(string path)
        {
            SFTPAccess sftpAccess = new SFTPAccess();
            sftpAccess.SFTPAccessConnection(out username,out password,out host,out port);
            string base64String = "";
            try
            {
                using (SftpClient client = new SftpClient(host, port, username, password))
                {
                    client.Connect();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        SftpFile file = client.Get(path);
                        var size = file.Attributes.Size;
                        client.DownloadFile(path, stream);
                        byte[] imageBytes = stream.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                    client.Disconnect();
                }
            }
            catch (Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "SFTP" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
            return base64String;
        }

        public static decimal getSizeFile(string path)
        {
            decimal size = 0;
            try
            {
                SFTPAccess sftpAccess = new SFTPAccess();
                sftpAccess.SFTPAccessConnection(out username, out password, out host, out port);
                using (SftpClient client = new SftpClient(host, port, username, password))
                {
                    client.Connect();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        SftpFile file = client.Get(path);
                        size = file.Attributes.Size;
                    }
                    client.Disconnect();
                }
            }
            catch (Exception e)
            {
                GenerarLog objLOG = new GenerarLog();
                string detalleError = "SFTP" + " - " +
                    DateTime.Now + "\n" + "\n" +
                    e.Message + "\n" +
                    "-------------------------------------------------------------------------------------------------------------";
                objLOG.GenerarArchivoLog(detalleError);
            }
            return size;
        }


   
    }
}
    
