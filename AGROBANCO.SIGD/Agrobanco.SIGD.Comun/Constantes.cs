﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
   public class Constantes
    {
        public static int RESPUESTA_OK = 1;
        public static int RESPUESTA_ERROR = 0;

        public static int RESPUESTA_SESION_EXPIRADA = 2;
        public static string MENSAJE_ERROR_LOGIN = "Petición no válida.";
        public static string LaserficheFileExtensionDefault = "pdf";
        public static string MENSAJE_ATENIDO_AUTOMATICO_INTERNO = ConfigurationManager.AppSettings["MENSAJE_ATENDIDO_AUTOMATICO_INTERNO"];
        public static string MENSAJE_DEVUELTO_AUTOMATICO_DEVUELTO = ConfigurationManager.AppSettings["MENSAJE_DEVUELTO_AUTOMATICO_INTERNO"];
        public static string MENSAJE_ATENIDO_AUTOMATICO = ConfigurationManager.AppSettings["MENSAJE_ATENDIDO_AUTOMATICO"];
        public static string MENSAJE_COMENTARIO_DERIVADO_ANULADO = "Documento principal anulado";
    }
}
