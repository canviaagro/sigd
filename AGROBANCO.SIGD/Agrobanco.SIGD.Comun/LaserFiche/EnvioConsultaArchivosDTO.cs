﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class EnvioConsultaArchivosDTO
    {
        public List<DocumentoItemFileDTO> ListaDocumentos { get; set; }
        public List<int> ListaDocumentosAnexoAdjunto { get; set; }
    }
}
