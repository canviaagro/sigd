﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class EnvioRegistrarDocumentosDTO
    {
        public List<RegistraDocumentoDTO> Documentos { get; set; }
        public Dictionary<string, Object> Metadata { get; set; }

    }
}
