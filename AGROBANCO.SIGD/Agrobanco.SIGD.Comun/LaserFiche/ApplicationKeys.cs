﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public static class ApplicationKeys
    {
    
        public static string LaserFicheServer => ConfigurationManager.AppSettings["config:Hostname"];
        public static string LaserFicheUser=> ConfigurationManager.AppSettings["config:User"];
        public static string LaserFichePassword => ConfigurationManager.AppSettings["config:Password"];
        public static string LaserFicheRepository => ConfigurationManager.AppSettings["config:LfRepositorio"];
        public static string LaserFicheFolderBase => ConfigurationManager.AppSettings["config:LfFolderDocument"];
        public static string LaserFicheTemplateName => ConfigurationManager.AppSettings["config:LfTemplate"];

        public static string LaserFicheFolder => ConfigurationManager.AppSettings["config:LFApplicationName"];
        public static string LaserFicheEnableEncrip => ConfigurationManager.AppSettings["config:LaserFicheEnableEncrip"];
        public static string SqlRegeditFolder => ConfigurationManager.AppSettings["config:SqlApplicationName"];
        public static string RegeditPass => ConfigurationManager.AppSettings["config:Regeditkey"];
        public static string SQLApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:SQLApplicationNameEnableEncrip"];


        public static string SFTPApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:SFTPApplicationNameEnableEncrip"];
        public static string SFTPApplicationName => ConfigurationManager.AppSettings["config:SFTPApplicationName"];

        public static string UrlAPISGS => ConfigurationManager.AppSettings["UrlAPISGS"];


        public static string SIEDApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:SIEDApplicationNameEnableEncrip"];
        public static string SIEDApplicationName => ConfigurationManager.AppSettings["config:SIEDApplicationName"];

        public static string serviceSIEDListarFirmante => ConfigurationManager.AppSettings["serviceSIEDListarFirmante"];
        public static string serviceSIEDRegistrarDocumento => ConfigurationManager.AppSettings["serviceSIEDRegistrarDocumento"];
        public static string serviceSIEDListarEmpresas => ConfigurationManager.AppSettings["serviceSIEDListarEmpresas"]; 
        public static string RUCAGROBANCO => ConfigurationManager.AppSettings["RUCAGROBANCO"];
        public static string rutaSFTPSubidaHome => ConfigurationManager.AppSettings["rutaSFTPSubidaHome"];
        public static string rutaSFTPSubidaEmpresas => ConfigurationManager.AppSettings["rutaSFTPSubidaEmpresas"];
        public static string rutaSFTPCodSTD => ConfigurationManager.AppSettings["rutaSFTPCodSTD"];

        public static string CorreoApplicationNameEnableEncrip => ConfigurationManager.AppSettings["config:CorreoApplicationNameEnableEncrip"];
        public static string CorreoApplicationName => ConfigurationManager.AppSettings["config:CorreoApplicationName"];

        public static string Laserfiche_REG => ConfigurationManager.AppSettings["LASERFICHE_REG"];

    }
}
