﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class RespuestaConsultaArchivosDTO : RespuestaBaseDTO
    {
        public List<ConsultaArchivoDTO> ListaDocumentos { get; set; }

    }
}
