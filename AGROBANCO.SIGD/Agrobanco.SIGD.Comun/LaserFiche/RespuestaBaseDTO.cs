﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class RespuestaBaseDTO
    {
        public bool Exito { get; set; }
        public string Mensaje { get; set; }
        public List<MensajeDTO> ListaMensajes { get; set; }
    }
    public class MensajeDTO
    {
        public string Mensaje { get; set; }
        public string Id { get; set; }

    }
}
