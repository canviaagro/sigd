﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class FotoModel
    {
        public int IdColImg { get; set; }
        public string CtaCod { get; set; }
        public string ImgCod { get; set; }
        public string NroSolicitud { get; set; }
        public string NroDocumento { get; set; }
        public string NombreCliente { get; set; }
        public string FechaRegistro { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Observacion { get; set; }
        public string Estado { get; set; }
        public string UltActualizacion { get; set; }
        public string FechaFoto { get; set; }
        public string imgBase64 { get; set; }
        public string FechaVinculacion { get; set; }
        public int IdLaserFiche { get; set; }
        public string Credito { get; set; }
        public string Cultivo { get; set; }
        public string Agencia { get; set; }
        public string Funcionario { get; set; }
        public decimal Distancia { get; set; }
        public int EvalId { get; set; }

    }
}
