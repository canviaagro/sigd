﻿
using Laserfiche.RepositoryAccess;
using System;
using System.Collections.Generic;
using System.IO;

namespace Agrobanco.SIGD.Comun
{
    public class LaserFiche
    {

        public int codLaserFiche { get; set; }
        //private Session _sessionLf;


        //private Session GetSession()
        //{
        //    if (_sessionLf is null)
        //        _sessionLf = new Session();
        //    return _sessionLf;
        //}


        public RespuestaConsultaArchivosDTO ConsultaArchivos(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string archivoBase64 = null;
            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    var stream = laserFicheDocumentAccess.GetDocument(item.Id, out fileName, out extension, out peso);
                    archivoBase64 = HelperIT.MemoryStreamToBase64(stream);

                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        ArchivoBase64 = archivoBase64,
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }



        public RespuestaConsultaArchivosDTO ConsultaArchivosIMG(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string archivoBase64 = null;
            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    var stream = laserFicheDocumentAccess.GetImage(item.Id, out fileName);
                    archivoBase64 = HelperIT.MemoryStreamToBase64(stream);

                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        ArchivoBase64 = archivoBase64,
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }

        public RespuestaConsultaArchivosDTO ConsultaDefinicionArchivos(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    laserFicheDocumentAccess.GetDefinitionDocument(item.Id, out fileName, out extension, out peso);

                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo = item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }
        public RespuestaConsultaArchivosDTO ConsultaArchivosBytes(EnvioConsultaArchivosDTO envio)
        {
            RespuestaConsultaArchivosDTO respuesta = new RespuestaConsultaArchivosDTO();
            respuesta.ListaDocumentos = new List<ConsultaArchivoDTO>();

            string fileName = "", extension = "";
            decimal peso = 0;
            using (var laserFicheDocumentAccess = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                foreach (var item in envio.ListaDocumentos)
                {
                    var stream = laserFicheDocumentAccess.GetDocument(item.Id, out fileName, out extension, out peso);


                    respuesta.ListaDocumentos.Add(new ConsultaArchivoDTO()
                    {
                        IdDocumento = item.Id,
                        ArchivoBytes = stream.ToArray(),
                        NombreArchivo = fileName,
                        Extension = extension,
                        Peso = peso,
                        TipoArchivo=item.TipoArchivo
                    });
                }
                laserFicheDocumentAccess.Dispose();
            }
            respuesta.Exito = true;
            return respuesta;
        }

        public int CargarDocumentoLaserFiche(Stream stream, string fileExtension, string tipo, string NumeroSolicitud, string NumeroDocumento, out string fileName)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
            var metadata = new Dictionary<string, object>
            {
                {"NumeroSolicitud", NumeroSolicitud},
                {"DocumentoCliente", NumeroDocumento}
            };
            stream.Position = 0;
            var ms = new MemoryStream();
            stream.CopyTo(ms);
            ms.Position = 0;
            var destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}\{NumeroSolicitud}";
            lfhelper.GetOrCreateFolderInfo(destinPath);
            var documentPath = $@"{destinPath}\IV_{NumeroDocumento}.{fileExtension}";
            switch (tipo)
            {
                case "1": documentPath = $@"{destinPath}\FC_{NumeroDocumento}.{fileExtension}"; break;
                case "2": documentPath = $@"{destinPath}\IV_{NumeroDocumento}.{fileExtension}"; break;
                default: documentPath = $@"{destinPath}\OTRO_{NumeroDocumento}.{fileExtension}"; break;
            }

            var ldDocumentId = lfhelper.RegisterFileWithMetaData(ms, metadata, documentPath, ApplicationKeys.Laserfiche_REG, ApplicationKeys.LaserFicheTemplateName, out fileName, contenType: TypesConstants.GetTypeMIME(fileExtension));

            lfhelper.Dispose();
            lfhelper = null;
            return ldDocumentId;
        }

        public int CargarDocumentoLaserFichev2(Stream stream, string fileExtension, string tipo, string numeroDocumento, string correlativo, out string fileName)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
            /*var metadata = new Dictionary<string, object>
            {
                {"NumeroDocumento", numeroDocumento},
                {"Correlativo", correlativo}
            };*/
            stream.Position = 0;
            var ms = new MemoryStream();
            stream.CopyTo(ms);
            ms.Position = 0;
            var destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{correlativo}\{numeroDocumento}";
            lfhelper.GetOrCreateFolderInfo(destinPath);
            var documentPath = $@"{destinPath}\{numeroDocumento}.{fileExtension}";
            /*switch (tipo)
            {
                case "1": documentPath = $@"{destinPath}\FC_{numeroDocumento}.{fileExtension}"; break;
                case "2": documentPath = $@"{destinPath}\IV_{NumeroDocumento}.{fileExtension}"; break;
                default: documentPath = $@"{destinPath}\OTRO_{NumeroDocumento}.{fileExtension}"; break;
            }
            */
            var ldDocumentId = lfhelper.RegisterFileWithoutMetaData(ms, documentPath, ApplicationKeys.Laserfiche_REG, out fileName, contenType: TypesConstants.GetTypeMIME(fileExtension));

            lfhelper.Dispose();
            lfhelper = null;
            return ldDocumentId;
        }

        public int CargarDocumentoLaserFichev3(Stream stream, string fileExtension, string tipo, string numeroDocumento, string nombreDocAnexo, string correlativo, bool anexo, out string fileName)
        {
            int ldDocumentId = -1;
            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                stream.Position = 0;
                var ms = new MemoryStream();
                stream.CopyTo(ms);
                ms.Position = 0;
                var destinPath = "";
                var documentPath = "";
                if (!anexo)
                {
                    destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{correlativo}";
                    lfhelper.GetOrCreateFolderInfo(destinPath);
                    documentPath = $@"{destinPath}\{correlativo}.{fileExtension}";
                }
                else if (anexo)
                {
                    destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{correlativo}\{"Anexos"}";
                    lfhelper.GetOrCreateFolderInfo(destinPath);
                    documentPath = $@"{destinPath}\{nombreDocAnexo}";
                }
                ldDocumentId = lfhelper.RegisterFileWithoutMetaData(ms, documentPath, ApplicationKeys.Laserfiche_REG, out fileName, contenType: TypesConstants.GetTypeMIME(fileExtension));

                lfhelper.Dispose();
            }
            return ldDocumentId;
        }

        public void VincularFotosLaserFiche(string NumeroSolicitud, string NumeroDocumento, List<FotoModel> lstIdImgVin, List<FotoModel> lstIdImgLib = null)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);
            var fileExtension = "jpg";

            var destinPath = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}\{NumeroSolicitud}";

            lfhelper.GetOrCreateFolderInfo(destinPath);
            foreach (FotoModel IdImg in lstIdImgVin)
            {
                var documentPathVin = $@"{destinPath}\GR_{NumeroDocumento}.{fileExtension}";
                lfhelper.MoveDocument(IdImg.IdLaserFiche, documentPathVin);
            }

            if (lstIdImgLib != null)
            {
                var destinPathLib = $@"\{ApplicationKeys.LaserFicheFolderBase}\{NumeroDocumento}";
                lfhelper.GetOrCreateFolderInfo(destinPathLib);
                foreach (FotoModel IdImg in lstIdImgLib)
                {
                    var documentPathLib = $@"{destinPathLib}\GR_{NumeroDocumento}.{fileExtension}";
                    lfhelper.MoveDocument(IdImg.IdLaserFiche, documentPathLib);
                }
            }
            lfhelper.Dispose();
            lfhelper = null;
        }

        public void EliminarDocumento(int idCodLaserfiche)
        {
            using (var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository))
            {
                //lfhelper.DeleteDocumentIfExist(path);
                lfhelper.DeleteDocumentIfExistv2(idCodLaserfiche);
            }
        }

        public void EliminarDocumentov2(List<LaserFiche> idCodLaserfiche)
        {
            var lfhelper = new LaserFicheDocumentAccess(ApplicationKeys.LaserFicheRepository);

            //lfhelper.DeleteDocumentIfExist(path);
            lfhelper.DeleteDocumentIfExistv3(idCodLaserfiche);
        }
    }
}
