﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Comun
{
    public class Enumerados
    {
        public enum GrupoParametros
        {
            ESTADO_DOCUMENTO = 1,
            ORIGEN = 2,
            PRIORIDAD = 3,
            ESTADO = 5,
            TIPO_REPRESENTANTE = 6,
            TIPO_FERIADO = 7,
            ACCION = 8,
            JEFE =9,
            INDICACIONES = 10,
            AFIRMACION = 11,
            MOTIVOFIMA = 12,
            FORMATODOCUMENTO = 13,
            TIPODOCUMENTOEXTERNO = 14,
            TIPODOCUMENTOINTERNO = 15,
            ASUNTOSSIED = 16
        }
        public enum DestinoSied
        {
            ENVIO = 1,
            RECEPCION = 2
        }

        public enum Estado
        {
            Activo = 1,
            Inactivo = 0
        }

        public enum EstadoDocumento
        {
            INGRESADO = 1,
            PENDIENTE = 2,
            ANULADO = 3,
            DERIVADO = 4,
            ATENDIDO = 5,
            EN_PROCESO = 6,
            DEVUELTO = 7
        }
        public enum FlagUsadoSecuencial
        {
            USADO = 1,
            SINUSO = 0
        }
        public enum TipoOrigen
        {
            INTERNO = 1,
            EXTERNO = 2,
            INTERNOSIED = 3,
            EXTERNOSIED = 4
        }
        public enum TipoArchivos
        {
            Anexos = 1,
            Adjuntos = 2,
            Cargo = 3
        }
        public enum EnumPrioridad
        {
            BAJA = 1,
            NORMAL = 2,
            URGENTE = 3,
            MUYURGENTE = 4
        }
        public enum EnumTipoAcceso
        {
            ConCopia = 2,
            Derivado = 1,
            EmpresaSIED = 3
        }
        public enum EnumTipoMovimiento
        {
            Registro = 0,
            DerivacionArea = 1,
            DerivacionOtraArea = 2,
            RegistrarAvance = 3,
            Finalizar = 4,
            Devolver = 5,
            Anular = 6,
            FinalizarAutomatico = 7,
            DevolverAutomatico = 8
        }

        public enum EnumJefe
        {
            Si =1,
            No =0
        }
        public enum TipoDocumentoIden
        {
           DNI = 1,
           RUC = 2
        }
        public enum TipoRepresentante
        {
            NATURAL = 1,
            JURIDICO = 2
        }
        public enum Indicacion
        {
            COORDINAR = 1,
            TOMARACCION = 2,
            EMITIRRESPUESTAPERSONAL = 3,
            EMITIRPROYECTODERESPUESTA = 4,
            PARACONOCIMIENTO = 5,
            PARASEGUIMIENTO =6,
            OTROS = 7
        }
    }
}
