﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class RegistrarDerivacionFinalizarDTO
    {
        public int CodDocumento { get; set; }
        public int? CodDocDerivacion { get; set; }
        public string Comentario { get; set; }
        public int? CodArea { get; set; }
        public int? CodResponsable { get; set; }
        #region"Seguridad"
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }
        #endregion
    }
}
