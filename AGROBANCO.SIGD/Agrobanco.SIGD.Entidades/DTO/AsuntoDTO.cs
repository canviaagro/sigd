﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
   public class AsuntoDTO
    {
        public int CodAsunto { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }

    }
}
