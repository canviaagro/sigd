﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class DocumentoBandejaEntradaDTO
    {
        public int TipoBandeja { get; set; }
        public string TextoBusqueda { get; set; }
        public int Pagina { get; set; }
        public int TamanioPagina { get; set; }

        public DateTime FechaDesde { get; set; }

        public DateTime FechaHasta { get; set; }
        public int CodTrabajador { get; set; }
        public string CodPerfil { get; set; }

    }
}
