﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class ReportesDTO
    {

        public class ReporteMesaParteDTO
        {
            #region Reporte de Mesa de Partes
            public string vNumDocumento { get; set; }
            public string vCorrelativo { get; set; }
            public string Institucion { get; set; }
            public string FechaDerivacion { get; set; }
            public string vAsunto { get; set; }
            public string Responsable { get; set; }
            public string FirmaSello { get; set; }
            public int iCodArea { get; set; }
            public int iCodTipDocumento { get; set; }
            public DateTime fecInicio { get; set; }
            public DateTime fecFinal { get; set; }
            public int numPagina { get; set; }
            public int tamPagina { get; set; }
            public int Pagina { get; set; }
            public string parCodArea { get; set; }
            public string parCodTipDoc { get; set; }
            public string DesArea { get; set; }
            public string CodArea { get; set; }
            public string EstadoDoc { get; set; }
            public string FechaDocumento { get; set; }
            public string vAño { get; set; }
            public string vOpe { get; set; }
            public string vRemitente { get; set; }
            public string vDestinatario { get; set; }
            public string vObservacion { get; set; }
            public int idestado { get; set; }
            #endregion
        }

        public class ReporteAtencionDocumentoDTO
        {
            #region Reporte de Atencion De Documentos
          
            public string vCorrelativo { get; set; }
            public DateTime fecInicio { get; set; }
            public DateTime fecFinal { get; set; }
            public string CodAreaResponsable { get; set;}
            public string CodAreaDerivada { get; set; }
            public string CodTipoDocumento { get; set; }
            public string CodEstadoDoc { get; set; }
            public string siFirma { get; set; }
            public string Remitente { get; set; }
            public string Destinatario { get; set; }
            public string Asunto { get; set; }
            public string Comentarios { get; set; }
            public DateTime? fechaFirma { get; set; }
            public string DescripcionEstado { get; set; }
            public int numPagina { get; set; }
            public int tamPagina { get; set; }
            public int Pagina { get; set; }
            #endregion
        }

    }
}
