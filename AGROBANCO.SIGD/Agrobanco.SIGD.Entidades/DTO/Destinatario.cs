﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class Destinatario
    {
        public string Tipo { get; set; }
        public string CodEmpresa { get; set; }
        public int CodArea { get; set; }
        public string NombreArea { get; set; }
        public int? CodResponsable { get; set; }
        
        public string DescripcionEmpresa { get; set; }

        public string NombreContacto { get; set; }
    }
}
