﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Agrobanco.SIGD.Entidades
{
    [DataContract]
    public class RespuestaOperacionServicio
    {
        [DataMember]
        public int Resultado { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Error { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public string Informacion { get; set; }

        [DataMember]
        public string correlativo { get; set; }

        [DataMember]
        public int ResultadoCorreo { get; set; }

        [DataMember]
        public int posx { get; set; }

        [DataMember]
        public int posy { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Object data { set; get; }

        [DataMember]
        public List<Entidades.DocDerivacion>  lstDocDerivacion{ get; set; }

        [DataMember]
        public List<int> listEnteros { get; set; }
    }
}
