﻿using Agrobanco.SIGD.Entidades.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades
{
    public class ParametrosMesaPartesDTO
    {
        public List<AreaDTO> LstArea { get; set; }
        public List<ParametroDTO> LstOrigen { get; set; }
        public List<ParametroDTO> LstPrioridad { get; set; }
        public List<TipoDocumentoDTO> LstTipoDocumento { get; set; }

        public List<ParametroDTO> LstTipoDocumentoExterno { get; set; }
        public List<ParametroDTO> LstTipoDocumentoInterno { get; set; }
    }
}
