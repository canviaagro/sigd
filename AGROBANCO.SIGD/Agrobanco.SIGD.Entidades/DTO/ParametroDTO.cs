﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
   public class ParametroDTO
    {
        public int CodGrupo { get; set; }
        public int CodParametro { get; set; }
        public string Valor { get; set; }
        public string Campo { get; set; }
        public short Estado { get; set; }
    }
}
