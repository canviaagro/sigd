﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class ParametrosFeriadoDTO
    {
        public List<ParametroDTO> ListaTipoFeriado { get; set; }
        public List<ParametroDTO> ListaEstados { get; set; }
    }
}
