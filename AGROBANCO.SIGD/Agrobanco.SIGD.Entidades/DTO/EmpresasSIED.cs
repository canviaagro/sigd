﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class EmpresasSIED
    {

        public int codEmpresa { get; set; }
        public string nomEmpresaCorto { get; set; }
        public string nombreEmpresa { get; set; }
        public string rucEmpresa { get; set; }
        public string tipoEmpresa { get; set; }
        public string CodFormatoDocumentoEmpresas { get; set; }
    }
}