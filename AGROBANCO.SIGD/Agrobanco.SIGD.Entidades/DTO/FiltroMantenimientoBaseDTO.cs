﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.DTO
{
    public class FiltroMantenimientoBaseDTO
    {
        public string TextoBusqueda { get; set; }
        public int Pagina { get; set; }
        public int TamanioPagina { get; set; }
        public int Param { get; set; }
    }
}
