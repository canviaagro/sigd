﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class Feriado
    {
        public int? CodFeriado { get; set; }

        public DateTime? Fecha { get; set; }

        public String Descripcion { get; set; }

        public short? Estado { get; set; }

        public String UsuCreacion { get; set; }

        public DateTime? FecCreacion { get; set; }

        public String HstCreacion { get; set; }

        public String InsCreacion { get; set; }

        public String LgnCreacion { get; set; }

        public String RolCreacion { get; set; }

        public String UsuActualizacion { get; set; }

        public DateTime? FecActualizacion { get; set; }

        public String HstActualizacion { get; set; }

        public String InsActualizacion { get; set; }

        public String LgnActualizacion { get; set; }

        public String RolActualizacion { get; set; }

        public int? TipoFeriado { get; set; }

        public string TipoFeriadoDescripcion { get; set; }
    }
}
