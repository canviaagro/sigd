﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class TipDocIdentidad
    {
        public int iCodTipDocIdentidad { get; set; }
        public string Abreviatura { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
    }
}
