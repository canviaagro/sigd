﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
   public class Secuencia
    {
        #region Campos de Tabla
        
        public int anio { get; set; }
        public int Estado { get; set; }
        public int correlativo { get; set; }
        public int CodSecuencial { get; set; }
        public int? CodArea { get; set; }
        public int? CodTipoDocumento { get; set; }
        public string ValorCorrelativo { get; set; }
        public int Origen { get; set; }
        public int flagUsado { get; set; }

        #endregion
        public string DescripcionArea { get; set; }
        public string DescripcionTipoDocumento { get; set; }
        #region Propiedades Auditoria
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion
    }
}
