﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class UsuarioSIED
    {
        #region Campos de Tabla

        //Campos de la Tabla
        public int CodUsuarioSIED { get; set; }
        public string WebUsr { get; set; }
        public string Nombre { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string Email { get; set; }
        public string NroDoc { get; set; }

        //Propiedades de Auditoría
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion
    }
}
