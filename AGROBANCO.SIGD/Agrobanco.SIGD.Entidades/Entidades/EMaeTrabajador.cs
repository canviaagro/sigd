﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class EMaeTrabajador
    {
        public int iCodTrabajador { get; set; }
        public string vNumDocumento { get; set; }
        public string vNombres { get; set; }
        public string vCargo { get; set; }
        public string vUbicacion { get; set; }
        public int iAnexo { get; set; }
        public string vEmail { get; set; }
        public string vApePaterno { get; set; }
        public string vApeMaterno { get; set; }
        public Int16 siEsJefe { get; set; }
        public int iCodArea { get; set; }
        public int iCodTipDocIdentidad { get; set; }
        public string vCelular { get; set; }

        public  string DescripcionArea { get; set; }
        public string WEBUSR { get; set; }
    }
}
