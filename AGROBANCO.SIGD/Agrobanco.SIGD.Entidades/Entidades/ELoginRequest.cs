﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class ELoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
