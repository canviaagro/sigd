﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class Reportes
    {
        #region Reporte de Mesa de Partes
        public string vNumDocumento { get; set; }
        public string vCorrelativo { get; set; }
        public string Institucion { get; set; }
        public string FechaDerivacion { get; set; }
        public string vAsunto { get; set; }
        public string Responsable { get; set; }
        public string FirmaSello { get; set; }
        #endregion


    }
}
