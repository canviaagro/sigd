﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class DocDerivacion
    {
        #region Campos Tabla
        public int CodDocumentoDerivacion { get; set; }
        public int CodDocumento { get; set; }
        public short Estado { get; set; }
        public int CodArea { get; set; }
        public int CodResponsable { get; set; }
        public short EstadoDerivacion { get; set; }
        public short TipoAcceso { get; set; }
        public DateTime? FecDerivacion { get; set; }

        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion
        public string Responsable { get; set; }
        public string NombreArea { get; set; }
        public string NombreResponsable { get; set; }
        public string ApellidoMatResponsable { get; set; }
        public string WEBUsrResponsable { get; set; }
        public string ApellidoPatResponsable { get; set; }
        public string TipoAccion { get; set; }
        public int CodDocumentoFirma { get; set; }
        //SIED
        public string NombreEntidadSIED { get; set; }
        public string rucSied { get; set; }
        public string FormatoDocSied { get; set; }
        public int CodEntidadSied { get; set; }
        public string Comentarios { get; set; }
    }
}