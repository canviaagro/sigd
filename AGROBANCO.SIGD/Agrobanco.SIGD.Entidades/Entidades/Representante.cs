﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class Representante
    {

        public int CodRepresentante { get; set; }

        public String NumDocumento { get; set; }

        public String Nombres { get; set; }

        public short? Estado { get; set; }

        public short? TipRepresentante { get; set; }

        public String Direccion { get; set; }

        public String Telefono { get; set; }

        public String Celular { get; set; }

        public String Email { get; set; }

        public int? CodEmpresa { get; set; }

        public int? CodTipDocIdentidad { get; set; }
        public string Anexo { get; set; }
        public string Fax { get; set; }
        public String UsuCreacion { get; set; }

        public DateTime? FecCreacion { get; set; }

        public String HstCreacion { get; set; }

        public String InsCreacion { get; set; }

        public String LgnCreacion { get; set; }

        public String RolCreacion { get; set; }

        public String UsuActualizacion { get; set; }

        public DateTime? FecActualizacion { get; set; }

        public String HstActualizacion { get; set; }

        public String InsActualizacion { get; set; }

        public String LgnActualizacion { get; set; }

        public String RolActualizacion { get; set; }

    }
}
