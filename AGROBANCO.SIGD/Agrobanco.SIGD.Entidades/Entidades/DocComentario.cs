﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
   public class DocComentario
    {
        #region Campos de Tabla

        public int CodDocComentario { get; set; }
        public int CodDocMovimiento { get; set; }
        public int CodDocumento { get; set; }
        public string Comentario { get; set; }
        public short Estado { get; set; }

        public int? CodDocDerivacion { get; set; }
        #endregion


        #region Propiedades Auditoria
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion
    }
}
