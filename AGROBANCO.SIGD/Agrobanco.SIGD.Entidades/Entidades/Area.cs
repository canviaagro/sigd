﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class Area
    {

        //NUEVO COMENTARIO

        #region Campos de Tabla

        public int IdArea { get; set; }
        public string Abreviatura { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }

        #endregion


        #region Propiedades Auditoria
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion
    }
}
