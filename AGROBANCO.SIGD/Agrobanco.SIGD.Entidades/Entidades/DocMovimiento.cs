﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.Entidades
{
    public class DocMovimiento
    {
        #region Campos de Tabla

        public int CodDocMovimiento { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public DateTime FechaLectura { get; set; }
        public DateTime? FechaDerivacion { get; set; }
        public int? CodMovAnterior { get; set; }
        public short Plazo { get; set; }
        public DateTime FechaPlazo { get; set; }
        public short Nivel { get; set; }
        public short IndicadorFirma { get; set; }
        public DateTime FechaVisado { get; set; }
        public short IndicadorVisado { get; set; }
        public DateTime FechaFirma { get; set; }
        public int CodDocumento { get; set; }
        public int CodArea { get; set; }
        public int CodTrabajador { get; set; }
        public short Estado { get; set; }

        public short EstadoDocumento { get; set; }

        public int? CodDocDerivacion { get; set; }
        public int? CodAreaDerivacion { get; set; }
        public int? CodResponsableDerivacion { get; set; }
        public int? siEstadoDerivacion { get; set; }
        public int? TipoMovimiento { get; set; }


        #endregion


        #region Propiedades Auditoria
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion

        public DocComentario objDocComentario { get; set; }
    }
}
