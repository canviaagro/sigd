﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.EntidadesDb2
{
    public class ESegAsignacion
    {
        public string vCodAsignacion { get; set; }
        public string vCodPerfil { get; set; }
        public string vCodSistema { get; set; }
        public string webusr { get; set; }
    }
}   
