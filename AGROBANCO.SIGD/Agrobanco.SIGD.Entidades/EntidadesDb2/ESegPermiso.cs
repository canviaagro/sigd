﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.EntidadesDb2
{
    public class ESegPermiso
    {
        public string vCodPermiso { get; set; }
        public string vCodOpcion { get; set; }
        public string vCodModulo { get; set; }
        public string vCodSistema { get; set; }
        public string vCodAccion { get; set; }
        public string vCodPerfil { get; set; }
        public ESegModulo Modulo { get; set; }
        public ESegOpcion Opcion { get; set; }
        public ESegAccion Accion { get; set; }
        public bool Permitido { get; set; }
    }
}
