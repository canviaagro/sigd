﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EN = Agrobanco.SIGD.Entidades.Entidades;

namespace Agrobanco.SIGD.Entidades.EntidadesDb2
{
    public class EUsuario
    {
        public EN.EMaeTrabajador MaeTrabajador { get; set; }
        public ESegPerfil Perfil{ get; set; }
        public ESegSistema Sistema { get; set; }
        public List<ESegPermiso> Permisos { get; set; }
        public List<Modulo> ListaOpcionesMenu { get; set; }

        public class Modulo
        {
            public string IdPerfil { get; set; }
            public string NombrePerfil { get; set; }
            public string IdAplicacion { get; set; }
            public string NombreAplicacion { get; set; }
            public string UrlAplicacion { get; set; }
            public string IdOpcion { get; set; }
            public string NombreOpcion { get; set; }
            public string CorrelativoOpcion { get; set; }
            public string DescripcionAplicacion { get; set; }

            public string EstadoOpcion { get; set; }

            public string RutaFisicaIcono { get; set; }
            public string UrlOpcion { get; set; }
            public string TipoOpcion { get; set; }
            public string Controller { get; set; }
            public string Action { get; set; }
            public string TipoIconoCodigo { get; set; }
            public string TipoIconoDescripcion { get; set; }
            public string EstadoLogicoOpcion { get; set; }
            public string IdRelacion { get; set; }
        }
    }
}
