﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.WebService
{
    public class ItemAnexo
    {
        public string AlmacenArchivo { get; set; }
        public string Descripcion { get; set; }
        public string NombreFIsico { get; set; }
    }
}
