﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Entidades.WebService
{
    public class TramiteDocumentario
    {
        public string TipoEntidad { get; set; }
        public string Entidad { get; set; }
        public string NumeroDocumento { get; set; }
        public string FechaDocumento { get; set; }
        public string DirigidoA { get; set; }
        public string Referencia { get; set; }
        public string Asunto { get; set; }
        public string Prioridad { get; set; }
        public string Plazo { get; set; }
        public string Indicacion { get; set; }
        public string OtrasIndicaciones { get; set; }
        public List<ItemAnexo> Anexos { get; set; }
    }
}
