﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.Comun;
using System.Web.Http;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.API.Filter;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/trabajador")]
    public class TrabajadorController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        /// <summary>
        /// Servicio API que lista los trabajadores por Area
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetTrabajadorArea/{codArea}/{siEsJefe}")]
        public List<TrabajadorDTO> GetTrabajadorArea([FromUri] int codArea, [FromUri] short siEsJefe)
        {
             TrabajadorBL NObj = new TrabajadorBL();
                EMaeTrabajador EObj = new EMaeTrabajador();
            try
            {
               
                EObj.iCodArea = codArea;
                EObj.siEsJefe = siEsJefe;
               
            }catch(Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            } return NObj.ListarTrabajadorPorArea(EObj);
        }


        [HttpGet]
        [Route("GetTrabajadorAreaSIED")]
        public List<TrabajadorDTO> GetTrabajadorAreaSIED()
        {
            TrabajadorBL NObj = new TrabajadorBL();
            var result = new List<TrabajadorDTO>();
            try
            {

                 result = NObj.ListarTrabajadorPorAreaSIED();

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return result;
        }

        [HttpGet]
        [Route("GetTrabajadores")]
        public List<TrabajadorDTO> GetTrabajadores()
        {
            TrabajadorBL NObj = new TrabajadorBL();
            EMaeTrabajador EObj = new EMaeTrabajador();
            var lista = new List<TrabajadorDTO>();
            try
            {
                lista = NObj.GetTrabajadores();
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return lista;
        }


        //Inicio


        [HttpGet]
        [Route("GetListaTrabajador/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<TrabajadorDTO> GetListaTrabajador([FromUri]string search, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<TrabajadorDTO> salida = new ResultadoBusquedaDTO<TrabajadorDTO>();
            try
            {
                var searchString = (search == "VACIO") ? "" : search;
               
                TrabajadorBL NObj = new TrabajadorBL();
                TrabajadorDTO EObj = new TrabajadorDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = searchString;
                EObj.TamanioPagina = pagesize;
                int totalRegistros = 0;
                salida.Lista = NObj.ListarTrabajadorPaginado(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
               
            } catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            } return salida;
        }


        /// <summary>
        /// Servicio API que obtiene Datos de areas
        /// </summary>
        /// <returns>Retorna la Entidad Trabajador Por ID</returns>   
        [HttpGet]
        [Route("ObtenerTrabajadorID")]
        public RespuestaOperacionServicio ObtenerTrabajadorID(int id)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                ITrabajador objServicio = new TrabajadorBL();
                TrabajadorDTO DocEObj = new TrabajadorDTO();
                DocEObj.iCodTrabajador = id;
                rpta.data = objServicio.ObtenerTabajadorPorId(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("RegistrarTrabajador")]
        public RespuestaOperacionServicio RegistrarTrabajador([FromBody]TrabajadorDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;            

                ITrabajador objServicio = new TrabajadorBL();
                TrabajadorDTO DocEObj = new TrabajadorDTO();
                DocEObj.vNumDocumento = obj.vNumDocumento;
                DocEObj.vNombres = obj.vNombres;
                DocEObj.vApePaterno = obj.vApePaterno;
                DocEObj.vApeMaterno = obj.vApeMaterno;
                DocEObj.vCargo = obj.vCargo;
                DocEObj.vUbicacion = obj.vUbicacion;
                DocEObj.iAnexo = obj.iAnexo;
                DocEObj.vEmail = obj.vEmail;
                DocEObj.siEstado = obj.siEstado;
                DocEObj.siEsJefe = obj.siEsJefe;
                DocEObj.siFirma = obj.siFirma;
                DocEObj.iCodArea = obj.iCodArea;
                DocEObj.iCodTipDocIdentidad = obj.iCodTipDocIdentidad;
                DocEObj.vCelular = obj.vCelular;              
                DocEObj.vUsuCreacion = nameuser;
                DocEObj.vLgnCreacion = nameuser;
                DocEObj.iCodPerfil = obj.iCodPerfil;
                DocEObj.vCodPerfil = obj.vCodPerfil;
                DocEObj.WEBUSR = obj.WEBUSR;
                DocEObj.iOperador = obj.iOperador;
                rpta.data = objServicio.RegistrarTrabajador(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        
        [HttpPost]
        [Route("ActualizarTrabajador")]
        public RespuestaOperacionServicio ActualizarTrabajador([FromBody]TrabajadorDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {                                               
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                ITrabajador objServicio = new TrabajadorBL();
                TrabajadorDTO DocEObj = new TrabajadorDTO();
                DocEObj.iCodTrabajador = obj.iCodTrabajador;
                DocEObj.vNumDocumento = obj.vNumDocumento;
                DocEObj.vNombres = obj.vNombres;
                DocEObj.vApePaterno = obj.vApePaterno;
                DocEObj.vApeMaterno = obj.vApeMaterno;
                DocEObj.vCargo = obj.vCargo;
                DocEObj.vUbicacion = obj.vUbicacion;
                DocEObj.iAnexo = obj.iAnexo;
                DocEObj.vEmail = obj.vEmail;
                DocEObj.siEstado = obj.siEstado;
                DocEObj.siEsJefe = obj.siEsJefe;
                DocEObj.siFirma = obj.siFirma;
                DocEObj.iCodArea = obj.iCodArea;
                DocEObj.iCodTipDocIdentidad = obj.iCodTipDocIdentidad;
                DocEObj.vCelular = obj.vCelular;
                DocEObj.vUsuActualizacion = nameuser;
                DocEObj.vLgnActualizacion = nameuser;
                DocEObj.iCodPerfil = obj.iCodPerfil;
                DocEObj.vCodPerfil = obj.vCodPerfil;
                DocEObj.WEBUSR = obj.WEBUSR;
                DocEObj.iOperador = obj.iOperador;
                rpta.data = objServicio.ActualizarTrabajador(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("EliminarTrabajador")]
        public RespuestaOperacionServicio EliminarTrabajador([FromBody]TrabajadorDTO obj)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                ITrabajador objServicio = new TrabajadorBL();
                TrabajadorDTO DocEObj = new TrabajadorDTO();
                DocEObj.iCodTrabajador = obj.iCodTrabajador;
                DocEObj.vUsuActualizacion = nameuser;
                DocEObj.vLgnActualizacion = nameuser;

                rpta.data = objServicio.EliminarTrabajador(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        private T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
            return result;
        }


        /// <summary>
        /// Servicio API que realiza el Listado de Áreas
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ListarTipDocIdentidad")]
        public RespuestaOperacionServicio ListarTipDocIdentidad()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                ITrabajador objServicio = new TrabajadorBL();
                rpta.data = objServicio.ListarTipDocIdentidad();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

    }
}