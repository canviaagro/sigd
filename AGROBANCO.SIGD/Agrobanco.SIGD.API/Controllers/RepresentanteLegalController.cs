﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.Comun;
using System.Web.Http;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.API.Filter;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{
    [Authorize]
    [ErrorFilterApi]
    [System.Web.Http.RoutePrefix("api/RepresentanteLegal")]

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RepresentanteLegalController : ApiController
    {


        /// <summary>
        /// Servicio API que obtiene Datos de Representante Legal
        /// </summary>
        /// <returns>Retorna la Entidad Representante Legal</returns>   
        [HttpGet]
        [Route("ObtenerRepresentanteLegal")]
        public RespuestaOperacionServicio ObtenerRepresentanteLegal()
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IRepresentanteLegal objServicio = new RepresentanteLegalBL();
                rpta.data = objServicio.ObtenerRepresentanteLegal();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpPost]
        [Route("Grabar")]
        public RespuestaOperacionServicio Grabar([FromBody]RepresentanteLegalDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                IRepresentanteLegal objServicio = new RepresentanteLegalBL();
                rpta.data = objServicio.GrabarRepresentanteLegal(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("Editar")]
        public RespuestaOperacionServicio Editar([FromBody]RepresentanteLegalDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                obj.UsuarioActualizacion = nameuser;
                obj.LoginActualizacion = nameuser;

                IRepresentanteLegal objServicio = new RepresentanteLegalBL();
                rpta.data = objServicio.EditarRepresentanteLegal(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

    }
}