﻿using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/Representante")]
    public class RepresentanteController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]


        /// <summary>
        /// Método Público para el buscar Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>   
        [HttpGet]
        [Route("Find/{codEmpresa}/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<Representante> BuscarRepresentante(
            [FromUri]string search,
            [FromUri]int codEmpresa,
            [FromUri]int page,
            [FromUri]int pagesize
            )
        {
            search = (search == "VACIO" ? "" : search);
            ResultadoBusquedaDTO<Representante> salida = new ResultadoBusquedaDTO<Representante>();
            try
            {
                IRepresentanteBL objServicio = new RepresentanteBL();
                FiltroMantenimientoBaseDTO EObj = new FiltroMantenimientoBaseDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;
                EObj.Param = codEmpresa;

                int totalRegistros = 0;
                salida.Lista = objServicio.BuscarRepresentante(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
                salida.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;

        }

        /// <summary>
        /// Método Público para el obtener un(a) Representante
        /// </summary>
        /// <returns>Retorna Lista genérica de Representante</returns>  
        [HttpGet]
        [Route("ObtenerRepresentante")]
        public RespuestaOperacionServicio ObtenerRepresentante(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IRepresentanteBL objServicio = new RepresentanteBL();
                Representante eObj = new Representante();

                eObj.CodRepresentante = id;
                rpta.data = objServicio.ObtenerRepresentante(eObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Método Público para Registrar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Registrar")]
        public RespuestaOperacionServicio Registrar([FromBody]Representante objRepresentante)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objRepresentante.UsuCreacion = nameuser;
                objRepresentante.LgnCreacion = nameuser;

                IRepresentanteBL objServicio = new RepresentanteBL();
                rpta.data = objServicio.Registrar(objRepresentante);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Actualizar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Actualizar")]
        public RespuestaOperacionServicio Actualizar([FromBody]Representante objRepresentante)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objRepresentante.UsuActualizacion = nameuser;
                objRepresentante.LgnActualizacion = nameuser;

                IRepresentanteBL objServicio = new RepresentanteBL();
                rpta.data = objServicio.Actualizar(objRepresentante);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Eliminar Representante
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Eliminar")]
        public RespuestaOperacionServicio Eliminar([FromBody]Representante objRepresentante)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objRepresentante.UsuActualizacion = nameuser;
                objRepresentante.LgnActualizacion = nameuser;

                IRepresentanteBL objServicio = new RepresentanteBL();
                rpta.data = objServicio.Eliminar(objRepresentante);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

    }
}
