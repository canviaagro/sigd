﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using Agrobanco.SIGD.API.Filter;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/reportes")]
    public class ReportesController : ApiController
    {
        /// <summary>
        /// Servicio API que devuelve lista de documentos Externos
        /// </summary>
        /// <param name="codarea"></param>
        /// <param name="codtipdoc"></param>
        /// <param name="fecinicio"></param>
        /// <param name="fecfinal"></param>
        /// <param name="codEstadoDocumento"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [EnableCors(origins: "*", headers: "*", methods: "*")]      
        
        [HttpGet]       
        [Route("GetReporteMesaPartes/{codarea}/{codtipdoc}/{fecinicio}/{fecfinal}/{codEstadoDocumento}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO> GetReporteMesaPartes([FromUri]string codarea, [FromUri]string codtipdoc, [FromUri]string fecinicio , [FromUri]string fecfinal, [FromUri]string codEstadoDocumento, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO> salida = new ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO>();
            try
            {
                var codigoArea = (codarea == "") ? "" : codarea;
                var codigoTipDoc = (codtipdoc == "") ? "" : codtipdoc;
                ReportesBL NObj = new ReportesBL();
                ReportesDTO.ReporteMesaParteDTO EObj = new ReportesDTO.ReporteMesaParteDTO();
                EObj.Pagina = page;
                EObj.parCodArea = codigoArea;
                EObj.parCodTipDoc = codigoTipDoc;
                EObj.tamPagina = pagesize;
                EObj.fecInicio = Convert.ToDateTime(fecinicio);
                EObj.fecFinal = Convert.ToDateTime(fecfinal);
                EObj.EstadoDoc = codEstadoDocumento;
                int totalRegistros = 0;
                salida.Lista = NObj.ReporteMesaPartes(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);

            }
            return salida;
        }

        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

        /// <summary>
        /// Servicio API que devuelve lista de documentos internos
        /// </summary>
        /// <param name="codarea"></param>
        /// <param name="codtipdoc"></param>
        /// <param name="fecinicio"></param>
        /// <param name="fecfinal"></param>
        /// <param name="codEstadoDocumento"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("GetReporteAreaFechaEstado/{codarea}/{codtipdoc}/{fecinicio}/{fecfinal}/{codEstadoDocumento}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO> GetReporteAreaFechaEstado([FromUri]string codarea, [FromUri]string codtipdoc, [FromUri]string fecinicio, [FromUri]string fecfinal, [FromUri]string codEstadoDocumento, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO> salida = new ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO>();
            try
            {
                var codigoArea = (codarea == "") ? "" : codarea;
                var codigoTipDoc = (codtipdoc == "") ? "" : codtipdoc;
                ReportesBL NObj = new ReportesBL();
                ReportesDTO.ReporteMesaParteDTO EObj = new ReportesDTO.ReporteMesaParteDTO();
                EObj.Pagina = page;
                EObj.parCodArea = codigoArea;
                EObj.parCodTipDoc = codigoTipDoc;
                EObj.tamPagina = pagesize;
                EObj.fecInicio = Convert.ToDateTime(fecinicio);
                EObj.fecFinal = Convert.ToDateTime(fecfinal);
                EObj.EstadoDoc = codEstadoDocumento;
                int totalRegistros = 0;
                salida.Lista = NObj.ReporteAreaFechaEstado(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);

            }
            return salida;
        }

        /// <summary>
        /// Servicio API que devuelve lista de documentos de entrada 
        /// </summary>
        /// <param name="codarea"></param>
        /// <param name="codtipdoc"></param>
        /// <param name="fecinicio"></param>
        /// <param name="fecfinal"></param>
        /// <param name="codEstadoDocumento"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("GetReporteSeguimientoDocumentos/{nrotramite}/{anio}/{codarea}/{codtipdoc}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO> GetReporteSeguimientoDocumentos([FromUri]string  nrotramite, [FromUri]string anio, [FromUri]string codarea, [FromUri]string codtipdoc, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO> salida = new ResultadoBusquedaDTO<ReportesDTO.ReporteMesaParteDTO>();
            try
            {
                var codigoArea = (codarea == "") ? "" : codarea;
                var codigoTipDoc = (codtipdoc == "") ? "" : codtipdoc;
                var nrotramite1 = (nrotramite == "VACIO") ? "" : nrotramite;
                ReportesBL NObj = new ReportesBL();
                ReportesDTO.ReporteMesaParteDTO EObj = new ReportesDTO.ReporteMesaParteDTO();
                EObj.Pagina = page;
                EObj.parCodArea = codigoArea;
                EObj.parCodTipDoc = codigoTipDoc;
                EObj.tamPagina = pagesize;
                EObj.vCorrelativo = nrotramite1;
                EObj.vAño = anio;

                int totalRegistros = 0;
                salida.Lista = NObj.ReporteSeguimientoDocumentos(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);

            }
            return salida;
        }



        [HttpGet]
        [Route("ListarAnios")]
        public RespuestaOperacionServicio ListarAnios()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            try
            {
                IReportesBL objServicio = new ReportesBL();
                rpta.data = objServicio.ReporteListarAño();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;

            }
            return rpta;

        }

        [HttpGet]
       [Route("GetReporteAtencionDocumentos/{nrotramite}/{fecInicio}/{fecFinal}/{codAreaResponsable}/{codAreaDerivada}/{codTipDoc}/{EstadoDoc}/{cboFirmado}/{page}/{pageSize}")]
        public ResultadoBusquedaDTO<ReportesDTO.ReporteAtencionDocumentoDTO> GetReporteAtencionDocumentos([FromUri]string nrotramite, [FromUri]string fecInicio, [FromUri]string fecFinal, [FromUri]string codAreaResponsable,
                                                                                                  [FromUri]string codAreaDerivada, [FromUri]string codTipDoc, [FromUri]string EstadoDoc, 
                                                                                                  [FromUri]string cboFirmado, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<ReportesDTO.ReporteAtencionDocumentoDTO> salida = new ResultadoBusquedaDTO<ReportesDTO.ReporteAtencionDocumentoDTO>();
            try
            {
                nrotramite = (nrotramite == "VACIO") ? "" : nrotramite;
                ReportesBL NObj = new ReportesBL();
                ReportesDTO.ReporteAtencionDocumentoDTO EObj = new ReportesDTO.ReporteAtencionDocumentoDTO();
                EObj.Pagina = page;
                EObj.tamPagina = pagesize;

                EObj.vCorrelativo = nrotramite;
                EObj.fecInicio = Convert.ToDateTime(fecInicio);
                EObj.fecFinal = Convert.ToDateTime(fecFinal);
                EObj.CodAreaResponsable = codAreaResponsable;
                EObj.CodAreaDerivada = codAreaDerivada;
                EObj.CodTipoDocumento = codTipDoc;
                EObj.CodEstadoDoc = EstadoDoc;
                EObj.siFirma = cboFirmado;

                int totalRegistros = 0;
                salida.Lista = NObj.ReporteAtencionDocumento(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);

            }
            return salida;
        }


    }
}
