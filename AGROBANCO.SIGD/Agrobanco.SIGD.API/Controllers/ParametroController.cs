﻿using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/parametro")]
    public class ParametroController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]

        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos - Mesa de Partes
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosMP")]
        public RespuestaOperacionServicio ListarParametrosMesaPartes()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/

                IAreaBL objServicio = new AreaBL();               
                ParametrosMesaPartesDTO objParametros = new ParametrosMesaPartesDTO();
                objParametros.LstArea = objServicio.ListarArea();

                IParametroBL objServicioParam = new ParametroBL();
                objParametros.LstOrigen = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ORIGEN);
                objParametros.LstPrioridad = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.PRIORIDAD);

                ITipoDocumentoBL objServicioTipoDoc = new TipoDocumentoBL();
                objParametros.LstTipoDocumento = objServicioTipoDoc.ListarTipoDocumento();

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos - Mesa de Partes
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosSIED")]
        public RespuestaOperacionServicio GetParametrosSIED()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/

                IAreaBL objServicio = new AreaBL();
                ParametrosMesaPartesDTO objParametros = new ParametrosMesaPartesDTO();
                objParametros.LstArea = objServicio.ListarArea();

                IParametroBL objServicioParam = new ParametroBL();
                objParametros.LstOrigen = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ORIGEN);
                objParametros.LstPrioridad = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.PRIORIDAD);

                objParametros.LstTipoDocumentoExterno = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.TIPODOCUMENTOEXTERNO);
                objParametros.LstTipoDocumentoInterno = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.TIPODOCUMENTOINTERNO);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosListaTipoDocumento")]
        public RespuestaOperacionServicio GetParametrosTiposDocumento()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosMesaPartesDTO objParametros = new ParametrosMesaPartesDTO();
                ITipoDocumentoBL objServicioTipoDoc = new TipoDocumentoBL();
                objParametros.LstTipoDocumento = objServicioTipoDoc.ListarTipoDocumento();

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }


        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosComunOrigen")]
        public RespuestaOperacionServicio GetParametrosComunOrigen()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ORIGEN);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosComunOrigenNoSiIED")]
        public RespuestaOperacionServicio GetParametrosComunOrigenNoSiIED()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ORIGEN);
                objParametros.ListaEstados = objParametros.ListaEstados.FindAll(x => (x.CodParametro == 1 || x.CodParametro ==2));
                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("CargarParametrosAsuntosSIED")]
        public RespuestaOperacionServicio CargarParametrosAsuntosSIED()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ASUNTOSSIED);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosMotivoFirma")]
        public RespuestaOperacionServicio GetParametrosMotivoFirma()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.MOTIVOFIMA);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosFormatoDocumento")]
        public RespuestaOperacionServicio GetParametrosFormatoDocumento()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.FORMATODOCUMENTO);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosComunAccion")]
        public RespuestaOperacionServicio GetParametrosComunAccion()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ACCION);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosComuEstadoDocumento")]
        public RespuestaOperacionServicio GetParametrosComuEstadoDocumento()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ESTADO_DOCUMENTO);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos -Comun
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetParametrosComun")]
        public RespuestaOperacionServicio GetParametrosComun()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();                
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ESTADO);
                
                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        [HttpGet]
        [Route("GetParametrosRepresentante")]
        public RespuestaOperacionServicio GetParametrosRepresentante()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosRepresentanteDTO objParametros = new ParametrosRepresentanteDTO();
                IParametroBL objServicioParam = new ParametroBL();
                ITipDocIdentidadBL objTipDocIdentidadBL = new TipDocIdentidadBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ESTADO);
                objParametros.ListaTipDocIdentidad = objTipDocIdentidadBL.ListarTipDocIdentidad();
                objParametros.ListaTipoRepresentante = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.TIPO_REPRESENTANTE);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        [HttpGet]
        [Route("GetParametrosFeriado")]
        public RespuestaOperacionServicio GetParametrosFeriado()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosFeriadoDTO objParametros = new ParametrosFeriadoDTO();
                IParametroBL objServicioParam = new ParametroBL();
                ITipDocIdentidadBL objTipDocIdentidadBL = new TipDocIdentidadBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.ESTADO);
                objParametros.ListaTipoFeriado = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.TIPO_FERIADO);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }


        [HttpGet]
        [Route("GetParametrosEstadoJefe")]
        public RespuestaOperacionServicio GetParametrosEstadoJefe()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.JEFE);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }


        [HttpGet]
        [Route("GetParametrosAfirmacion")]
        public RespuestaOperacionServicio GetParametrosAfirmacion()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {

                IAreaBL objServicio = new AreaBL();
                ParametrosComunDTO objParametros = new ParametrosComunDTO();
                IParametroBL objServicioParam = new ParametroBL();
                objParametros.ListaEstados = objServicioParam.ListarParametrosPorGrupo((int)Enumerados.GrupoParametros.AFIRMACION);

                rpta.data = objParametros;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }
    }
}
