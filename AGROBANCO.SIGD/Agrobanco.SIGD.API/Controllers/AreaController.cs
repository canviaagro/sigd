﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.IO;
using System.Web;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/area")]
    public class AreaController : ApiController
    {
        
        [EnableCors(origins: "*", headers: "*", methods: "*")]

       
        /// <summary>
        /// Servicio API que realiza el Listado de Áreas
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("Get")]
        public RespuestaOperacionServicio ListarArea()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            try
            {
                IAreaBL objServicio = new AreaBL();
                rpta.data = objServicio.ListarArea();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
               
            }
            return rpta;

        }

        /// <summary>
        /// Servicio API que realiza el Listado de Áreas del usuario SIED
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetSIED")]
        public RespuestaOperacionServicio GetSIED()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            try
            {
                IAreaBL objServicio = new AreaBL();
                rpta.data = objServicio.ListarAreaSIED();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;

            }
            return rpta;

        }


        [HttpGet]
        [Route("GetListaAreas/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<AreaDTO> GetListaAreas([FromUri]string search, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<AreaDTO> salida = new ResultadoBusquedaDTO<AreaDTO>();
            AreaBL NObj = new AreaBL();
            AreaDTO EObj = new AreaDTO();
               
            try
            {
                var searchString = (search == "VACIO") ? "" : search;
                 EObj.Pagina = page;
                EObj.TextoBusqueda = searchString;
                EObj.TamanioPagina = pagesize;
                int totalRegistros = 0;
                salida.Lista = NObj.ListarAreasBandeja(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
               
            }catch(Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return salida;
        }


        /// <summary>
        /// Servicio API que obtiene Datos de areas
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerAreaID")]
        public RespuestaOperacionServicio ObtenerAreaID(int id)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            try
            {
                IAreaBL objServicio = new AreaBL();
                AreaDTO DocEObj = new AreaDTO();
                DocEObj.CodArea = id;
                rpta.data = objServicio.ObtenerAreasPorId(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("RegistrarArea")]
        public RespuestaOperacionServicio RegistrarArea([FromBody]AreaDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                IAreaBL objServicio = new AreaBL();
                AreaDTO DocEObj = new AreaDTO();
                DocEObj.Abreviatura = obj.Abreviatura;
                DocEObj.Descripcion = obj.Descripcion;
                DocEObj.Estado = obj.Estado;
                DocEObj.UsuarioCreacion = nameuser;
                DocEObj.LoginCreacion = nameuser;
                rpta.data = objServicio.RegistrarArea(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method =stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpPost]
        [Route("EliminarArea")]
        public RespuestaOperacionServicio EliminarArea([FromBody]AreaDTO obj)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            try
            {
                IAreaBL objServicio = new AreaBL();
                AreaDTO DocEObj = new AreaDTO();
                DocEObj.CodArea = obj.CodArea;

                DocEObj.UsuarioActualizacion = nameuser;
                DocEObj.LoginActualizacion = nameuser;
                rpta.data = objServicio.EliminarArea(DocEObj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }



        [HttpPost]
        [Route("ActualizarArea")]
        public RespuestaOperacionServicio ActualizarArea([FromBody]AreaDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();  
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                obj.UsuarioActualizacion = nameuser;
                obj.LoginActualizacion = nameuser;
                IAreaBL objServicio = new AreaBL();
                rpta.data = objServicio.ActualizarArea(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        private void generarlog(Exception ex,string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }
    }
}
