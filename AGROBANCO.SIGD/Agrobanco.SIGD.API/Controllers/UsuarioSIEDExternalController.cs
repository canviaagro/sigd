﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.Comun;
using System.Web.Http;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.API.Filter;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{
    [Authorize]
    [ErrorFilterApi]
    [System.Web.Http.RoutePrefix("api/UsuarioSIEDExternal")]

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsuarioSIEDExternalController : ApiController
    {


        /// <summary>
        /// Servicio API que obtiene Datos de areas
        /// </summary>
        /// <returns>Retorna la Entidad Trabajador Por ID</returns>   
        [AllowAnonymous]
        [HttpGet]
        [Route("ObtenerUsuarioSIEDExternal")]
        public Object ObtenerUsuarioSIED()
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IUsuarioSIED objServicio = new UsuarioSIEDBL();
                rpta.data = objServicio.ObtenerUsuarioSIED();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta.data;
        }


  
        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

    }
}