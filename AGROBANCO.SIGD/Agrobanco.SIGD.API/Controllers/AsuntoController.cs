﻿using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/asunto")]
    public class AsuntoController : ApiController
    {
        [HttpGet]
        [Route("GetAsunto")]
        public RespuestaOperacionServicio ListarAsunto()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                IAsuntoBL objServicioAsunto = new AsuntoBL();
                rpta.data = objServicioAsunto.ListarAsunto();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }



        /// <summary>
        /// Método Público para el buscar Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>   
        [HttpGet]
        [Route("Find/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<Asunto> BuscarAsunto(
            [FromUri]string search,
            [FromUri]int page,
            [FromUri]int pagesize
            )
        {
            search = (search == "VACIO" ? "" : search);
            ResultadoBusquedaDTO<Asunto> salida = new ResultadoBusquedaDTO<Asunto>();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                IAsuntoBL objServicio = new AsuntoBL();
                FiltroMantenimientoBaseDTO EObj = new FiltroMantenimientoBaseDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;


                int totalRegistros = 0;
                salida.Lista = objServicio.BuscarAsunto(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
                salida.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;

        }

        /// <summary>
        /// Método Público para el obtener un(a) Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        [HttpGet]
        [Route("ObtenerAsunto")]
        public RespuestaOperacionServicio ObtenerAsunto(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                IAsuntoBL objServicio = new AsuntoBL();
                Asunto eObj = new Asunto();

                eObj.CodAsunto = id;
                rpta.data = objServicio.ObtenerAsunto(eObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Método Público para Registrar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Registrar")]
        public RespuestaOperacionServicio Registrar([FromBody]Asunto objAsunto)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objAsunto.UsuarioCreacion = nameuser;
                objAsunto.LoginCreacion = nameuser;
                IAsuntoBL objServicio = new AsuntoBL();
                rpta.data = objServicio.Registrar(objAsunto);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Actualizar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Actualizar")]
        public RespuestaOperacionServicio Actualizar([FromBody]Asunto objAsunto)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objAsunto.UsuarioActualizacion = nameuser;
                objAsunto.LoginActualizacion = nameuser;

                IAsuntoBL objServicio = new AsuntoBL();
                rpta.data = objServicio.Actualizar(objAsunto);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Eliminar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Eliminar")]
        public RespuestaOperacionServicio Eliminar([FromBody]Asunto objAsunto)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objAsunto.UsuarioActualizacion = nameuser;
                objAsunto.LoginActualizacion = nameuser;

                IAsuntoBL objServicio = new AsuntoBL();
                rpta.data = objServicio.Eliminar(objAsunto);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        private void generarlog( Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }
    }
}