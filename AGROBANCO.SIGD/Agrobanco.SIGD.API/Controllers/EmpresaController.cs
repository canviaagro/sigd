﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.Comun;
using System.Web.Http;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.API.Filter;
using System.Diagnostics;

namespace Agrobanco.SIGD.API.Controllers
{

    [ErrorFilterApi]
    [RoutePrefix("api/empresa")]
    public class EmpresaController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]

        /// <summary>
        /// Servicio API que realiza el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("crear")]
        public RespuestaOperacionServicio RegistrarEmpresa([FromBody]Empresa objEmpresa)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/           

                IEmpresaBL objServicio = new EmpresaBL();
                rpta.data = objServicio.RegistrarEmpresa(objEmpresa);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos - Mesa de Partes
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetTrabajadorEmp/{codEmpresa}")]
        public RespuestaOperacionServicio ListaTrabajadorPorEmpresa([FromUri] int codEmpresa)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/

                IEmpresaBL objServicioEmpresa = new EmpresaBL();
                rpta.data = objServicioEmpresa.ListarRepresentantePorEmpresa(codEmpresa); 
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        /// <summary>
        /// Servicio API que retorna los parámetros para cargar objetos - Mesa de Partes
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetEmpresa")]
        public RespuestaOperacionServicio ListarEmpresa()
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/

                IEmpresaBL objServicioEmpresa = new EmpresaBL();
                rpta.data = objServicioEmpresa.ListarEmpresa();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }



        /// <summary>
        /// Método Público para el buscar Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>   
        [HttpGet]
        [Route("Find/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<Empresa> BuscarEmpresa(
            [FromUri]string search,
            [FromUri]int page,
            [FromUri]int pagesize
            )
        {
            search = (search == "VACIO" ? "" : search);
            ResultadoBusquedaDTO<Empresa> salida = new ResultadoBusquedaDTO<Empresa>();
            try
            {
                IEmpresaBL objServicio = new EmpresaBL();
                FiltroMantenimientoBaseDTO EObj = new FiltroMantenimientoBaseDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;


                int totalRegistros = 0;
                salida.Lista = objServicio.BuscarEmpresa(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
                salida.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;

        }

        /// <summary>
        /// Método Público para el obtener un(a) Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        [HttpGet]
        [Route("ObtenerEmpresa")]
        public RespuestaOperacionServicio ObtenerEmpresa(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IEmpresaBL objServicio = new EmpresaBL();
                Empresa eObj = new Empresa();

                eObj.CodEmpresa = id;
                rpta.data = objServicio.ObtenerEmpresa(eObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Método Público para Registrar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Registrar")]
        public RespuestaOperacionServicio Registrar([FromBody]Empresa objEmpresa)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objEmpresa.UsuarioCreacion = nameuser;
                objEmpresa.LoginCreacion = nameuser;

                IEmpresaBL objServicio = new EmpresaBL();
                rpta.data = objServicio.Registrar(objEmpresa);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Actualizar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Actualizar")]
        public RespuestaOperacionServicio Actualizar([FromBody]Empresa objEmpresa)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objEmpresa.UsuarioActualizacion = nameuser;
                objEmpresa.LoginActualizacion = nameuser;

                IEmpresaBL objServicio = new EmpresaBL();
                rpta.data = objServicio.Actualizar(objEmpresa);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Eliminar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Eliminar")]
        public RespuestaOperacionServicio Eliminar([FromBody]Empresa objEmpresa)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objEmpresa.UsuarioActualizacion = nameuser;
                objEmpresa.LoginActualizacion = nameuser;

                IEmpresaBL objServicio = new EmpresaBL();
                rpta.data = objServicio.Eliminar(objEmpresa);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

    }
}
