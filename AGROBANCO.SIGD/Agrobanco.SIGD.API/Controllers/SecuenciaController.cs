﻿using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [RoutePrefix("api/secuencia")]
    public class SecuenciaController : ApiController
    {
        /// <summary>
        /// Método Público para el Listar Secuencias
        /// </summary>
        /// <returns>Retorna Lista genérica de Secuencias</returns>   
        [HttpGet]
        [Route("Find/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<Secuencia> ListarSecuencia(
            [FromUri]string search,
            [FromUri]int page,
            [FromUri]int pagesize
            )
        {
            search = (search == "VACIO" ? "" : search);
            ResultadoBusquedaDTO<Secuencia> salida = new ResultadoBusquedaDTO<Secuencia>();
            try
            {
                ISecuenciaBL objServicio = new SecuenciaBL();
                FiltroMantenimientoBaseDTO EObj = new FiltroMantenimientoBaseDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;


                int totalRegistros = 0;
                salida.Lista = objServicio.ListarSecuencia(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
                salida.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;

        }
        /// <summary>
        /// Método Público para el Registrar Secuencias
        /// </summary>
        /// <returns>Retorna indicador de registro de Secuencias</returns>   
        [HttpPost]
        [Route("RegistrarSecuencia")]
        public RespuestaOperacionServicio RegistrarSecuencia([FromBody]SecuenciaDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                obj.UsuarioActualizacion = nameuser;


                ISecuenciaBL objServicio = new SecuenciaBL();
                rpta.data = objServicio.RegistrarSecuencia(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Método Público para el Registrar Secuencias
        /// </summary>
        /// <returns>Retorna indicador de registro de Secuencias</returns>   
        [HttpPost]
        [Route("EditarSecuencial")]
        public RespuestaOperacionServicio EditarSecuencial([FromBody]SecuenciaDTO obj)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                obj.UsuarioCreacion = nameuser;
                obj.LoginCreacion = nameuser;
                obj.UsuarioActualizacion = nameuser;


                ISecuenciaBL objServicio = new SecuenciaBL();
                rpta.data = objServicio.ActualizarSecuencia(obj);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que obtiene Datos de un tipo Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerSecuencia")]
        public RespuestaOperacionServicio ObtenerSecuencia(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                ISecuenciaBL objServicio = new SecuenciaBL();
                SecuenciaDTO DocEObj = new SecuenciaDTO();
                DocEObj.CodSecuencial = id;

                rpta.data = objServicio.ObtenerSecuencia(DocEObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que Elimina una secuencia
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("EliminarSecuencia")]
        public RespuestaOperacionServicio EliminarSecuencia([FromBody]SecuenciaDTO obj)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                ISecuenciaBL objServicio = new SecuenciaBL();
                SecuenciaDTO DocEObj = new SecuenciaDTO();
                DocEObj.CodSecuencial = obj.CodSecuencial;
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                DocEObj.UsuarioActualizacion = nameuser;
                DocEObj.LoginActualizacion = nameuser;
        
                rpta.data = objServicio.EliminarSecuencia(DocEObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API para Obtener Correlativo
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("ObtenerCorrelativo")]
        public RespuestaOperacionServicio ObtenerCorrelativo([FromBody]SecuenciaDTO obj)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                ISecuenciaBL objServicio = new SecuenciaBL();
   
                rpta.data = objServicio.ObtenerCorrelativo(obj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }
    }

}