﻿using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Agrobanco.SIGD.API.Controllers
{

    [ErrorFilterApi]
    [RoutePrefix("api/Feriado")]
    public class FeriadoController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]


        /// <summary>
        /// Método Público para el buscar Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>   
        [HttpGet]
        [Route("Find/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<Feriado> BuscarFeriado(
            [FromUri]string search,
            [FromUri]int page,
            [FromUri]int pagesize
            )
        {
            search = (search == "VACIO" ? "" : search);
            ResultadoBusquedaDTO<Feriado> salida = new ResultadoBusquedaDTO<Feriado>();
            try
            {
                IFeriadoBL objServicio = new FeriadoBL();
                FiltroMantenimientoBaseDTO EObj = new FiltroMantenimientoBaseDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;


                int totalRegistros = 0;
                salida.Lista = objServicio.BuscarFeriado(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
                salida.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;

        }

        /// <summary>
        /// Método Público para el obtener un(a) Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        [HttpGet]
        [Route("ObtenerFeriado")]
        public RespuestaOperacionServicio ObtenerFeriado(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IFeriadoBL objServicio = new FeriadoBL();
                Feriado eObj = new Feriado();

                eObj.CodFeriado = id;
                rpta.data = objServicio.ObtenerFeriado(eObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Método Público para Registrar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Registrar")]
        public RespuestaOperacionServicio Registrar([FromBody]Feriado objFeriado)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objFeriado.UsuCreacion = nameuser;
                objFeriado.LgnCreacion = nameuser;

                IFeriadoBL objServicio = new FeriadoBL();
                rpta.data = objServicio.Registrar(objFeriado);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Actualizar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Actualizar")]
        public RespuestaOperacionServicio Actualizar([FromBody]Feriado objFeriado)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objFeriado.UsuActualizacion = nameuser;
                objFeriado.LgnActualizacion = nameuser;

                IFeriadoBL objServicio = new FeriadoBL();
                rpta.data = objServicio.Actualizar(objFeriado);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Método Público para Eliminar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        [HttpPost]
        [Route("Eliminar")]
        public RespuestaOperacionServicio Eliminar([FromBody]Feriado objFeriado)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objFeriado.UsuActualizacion = nameuser;
                objFeriado.LgnActualizacion = nameuser;

                IFeriadoBL objServicio = new FeriadoBL();
                rpta.data = objServicio.Eliminar(objFeriado);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        private void generarlog(Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }
    }
}
