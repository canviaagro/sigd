﻿using Agrobanco.SIGD.API.Filter;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Agrobanco.SIGD.API.Controllers
{
    [ErrorFilterApi]
    [Authorize]
    [RoutePrefix("api/documento")]
    public class DocumentoController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]

        /// <summary>
        /// Servicio API que obtiene Datos de un tipo Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerTipoDocumento")]
        public RespuestaOperacionServicio ObtenerTipoDocumento(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                TipoDocumentoDTO DocEObj = new TipoDocumentoDTO();
                DocEObj.CodTipoDocumento = id;

                rpta.data = objServicio.ObtenerTipoDocumento(DocEObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que Elimina un tipo Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("EliminarTipoDocumento")]
        public RespuestaOperacionServicio EliminarTipoDocumento([FromBody]TipoDocumentoDTO obj)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                TipoDocumentoDTO DocEObj = new TipoDocumentoDTO();
                DocEObj.CodTipoDocumento = obj.CodTipoDocumento;
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                DocEObj.UsuarioActualizacion = nameuser;
                DocEObj.LoginActualizacion = nameuser;

                rpta.data = objServicio.EliminarTipoDocumento(DocEObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que obtiene Datos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerDocumento")]
        public RespuestaOperacionServicio ObtenerDocumento(int CodDocumento)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                Documento DocEObj = new Documento();
                DocEObj.CodDocumento = CodDocumento;
                string informacion = "";
                rpta.data = objServicio.ObtenerDocumento(DocEObj, out informacion);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.Informacion = informacion;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        
        [HttpGet]
        [Route("ObtenerDocumentoRecepcionSIED")]
        public RespuestaOperacionServicio ObtenerDocumentoRecepcionSIED(int CodDocumento)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                Documento DocEObj = new Documento();
                DocEObj.CodDocumento = CodDocumento;
                string informacion = "";
                rpta.data = objServicio.ObtenerDocumentoRecepcionSIED(DocEObj, out informacion);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.Informacion = informacion;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que obtiene Datos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoDetalleDerivacion")]
        public RespuestaOperacionServicio ObtenerDocumentoDetalleDerivacion(int CodDocumentoDer)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                string informacion = "";
                rpta.data = objServicio.GetDocumentoDerivadoPorId(CodDocumentoDer);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.Informacion = informacion;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que obtiene los Anexos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>  
        /// <summary>
        /// Servicio API que obtiene Datos del comentario
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetDocumentoAnexos")]
        public ResultadoBusquedaDTO<DocAnexo> GetDocumentoAnexos(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocAnexo> salida = new ResultadoBusquedaDTO<DocAnexo>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                DocAnexo obj = new DocAnexo();
                obj.CodDocumento = id;
                salida.Lista = objServicio.GetDocumentoAnexos(obj);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }
        
        /// <summary>
        /// Servicio API que obtiene los Anexos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>  
        /// <summary>
        /// Servicio API que obtiene Datos del comentario
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetDocumentoAnexosNoLF")]
        public ResultadoBusquedaDTO<DocAnexo> GetDocumentoAnexosNoLF(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocAnexo> salida = new ResultadoBusquedaDTO<DocAnexo>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                DocAnexo obj = new DocAnexo();
                obj.CodDocumento = id;
                salida.Lista = objServicio.GetDocumentoAnexosNoLF(obj);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }
        /// <summary>
        /// Servicio API que obtiene Datos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoInterno")]
        public RespuestaOperacionServicio ObtenerDocumentoInterno(int CodDocumento)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                Documento DocEObj = new Documento();
                DocEObj.CodDocumento = CodDocumento;
                string informacion = "";
                rpta.data = objServicio.ObtenerDocumentoInterno(DocEObj, out informacion);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.Informacion = informacion;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que obtiene Datos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoSIED")]
        public RespuestaOperacionServicio ObtenerDocumentoSIED(int CodDocumento)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                Documento DocEObj = new Documento();
                DocEObj.CodDocumento = CodDocumento;
                string informacion = "";
                rpta.data = objServicio.ObtenerDocumentoSIED(DocEObj, out informacion);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.Informacion = informacion;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        /// <summary>
        /// Servicio API que obtiene Datos de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoSIED_SeviceEnvio")]
        public RespuestaOperacionServicio ObtenerDocumentoSIED_SeviceEnvio(int CodDocumento)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                Documento DocEObj = new Documento();
                DocEObj.CodDocumento = CodDocumento;
                rpta.data = objServicio.ObtenerDocumentoSIED_SeviceEnvio(DocEObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

       [HttpGet]
        [Route("GetTiposDocumentos/{search}/{page}/{pagesize}")]
        public ResultadoBusquedaDTO<TipoDocumentoDTO> GetTiposDocumentos([FromUri]string search, [FromUri]int page, [FromUri]int pagesize)
        {
            ResultadoBusquedaDTO<TipoDocumentoDTO> salida = new ResultadoBusquedaDTO<TipoDocumentoDTO>();
            try
            {
                var searchString = (search == "VACIO") ? "" : search;
                DocumentoBL NObj = new DocumentoBL();
                TipoDocumentoDTO EObj = new TipoDocumentoDTO();
                EObj.Pagina = page;
                EObj.TextoBusqueda = searchString;
                EObj.TamanioPagina = pagesize;
                int totalRegistros = 0;
                salida.Lista = NObj.ListarTiposDocumento(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
               
            }catch(Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return salida;
        }

        /// <summary>
        /// Servicio API que obtiene Datos de un DocumentoDerivado x Trabajador
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoDerivadoxTrabajador")]
        public RespuestaOperacionServicio ObtenerDocumentoDerivadoxTrabajador(int CodDocumento, int CodTrabajador)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                DocDerivacion DocEObj = new DocDerivacion();
                DocEObj.CodDocumento = CodDocumento;
                DocEObj.CodResponsable = CodTrabajador;
                rpta.data = objServicio.ObtenerDocumentoDerivadoxTrabajador(DocEObj);

                rpta.Resultado = Constantes.RESPUESTA_OK;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que obtiene Datos de un DocumentoDerivado x Trabajador
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpGet]
        [Route("GetDocumentosDerivadosPorCodDocumento")]
        public RespuestaOperacionServicio GetDocumentosDerivadosPorCodDocumento(int CodDocumento, int TipoAcceso)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                var data = objServicio.GetDocumentosDerivadosPorCodDocumento(CodDocumento, TipoAcceso);
                rpta.data = data;
                rpta.lstDocDerivacion = data;
                rpta.Resultado = Constantes.RESPUESTA_OK;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        
        /// <summary>
        /// Servicio API que obtiene Datos de un DocumentoDerivado x Trabajador
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("FinalizarDocSIED")]
        public RespuestaOperacionServicio FinalizarDocSIED([FromBody]Documento objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;

                var data = objServicio.FinalizarDocumentoEnvioSIED(objDocumento);
                rpta.data = data;

                rpta.Resultado = Constantes.RESPUESTA_OK;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpGet]
        [Route("DescargarArchivoLaserfiche/{id}")]
        public RespuestaOperacionServicio DescargarArchivoLaserfiche([FromBody]int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.DescargarArchivoLaserfiche(id).ListaDocumentos.FirstOrDefault();
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que obtiene Documento digital
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoLaserFicher")]
        public RespuestaOperacionServicio ObtenerDocumentoLaserFicher(int CodLaserFicher)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                RespuestaConsultaArchivosDTO ResEObj = new RespuestaConsultaArchivosDTO();
                List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                ListaDocLaser.Add(new DocumentoItemFileDTO()
                {
                    Id = CodLaserFicher
                });

                EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                laserEObj.ListaDocumentos = ListaDocLaser;

                LaserFiche laserObj = new LaserFiche();
                ResEObj = laserObj.ConsultaArchivos(laserEObj);

                rpta.data = ResEObj;
                rpta.Resultado = Constantes.RESPUESTA_OK;

                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que obtiene Documento digital
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [Route("ObtenerDocumentoLaserFicherDoc")]
        public RespuestaOperacionServicio ObtenerDocumentoLaserFicherDoc(int CodDocument)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                RespuestaConsultaArchivosDTO ResEObj = new RespuestaConsultaArchivosDTO();
                List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                IDocumentoBL objServicio = new DocumentoBL();
                var CodLaserFicher = objServicio.ObtenerCodLaserFIchePorCodDocumento(CodDocument);


                ListaDocLaser.Add(new DocumentoItemFileDTO()
                {
                    Id = CodLaserFicher
                });

                EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                laserEObj.ListaDocumentos = ListaDocLaser;

                LaserFiche laserObj = new LaserFiche();
                ResEObj = laserObj.ConsultaArchivos(laserEObj);

                rpta.data = ResEObj;
                rpta.Resultado = Constantes.RESPUESTA_OK;

                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que Actualiza la atencion de un Documento en FIrma
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [Route("DocumentoAtencionFirma")]
        public RespuestaOperacionServicio DocumentoAtencionFirma(int CodDocumento,int Atencion)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.DocumentoAtencionFirma(CodDocumento, Atencion);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que Obtiene el estado de firma de documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [Route("ObtenerEstadoFirmaDocumento")]
        public RespuestaOperacionServicio ObtenerEstadoFirmaDocumento(int CodDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ObtenerEstadoFirmaDocumento(CodDocumento);

                rpta.Resultado = Constantes.RESPUESTA_OK;
                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        /// <summary>
        /// Servicio API que Obtiene el estado de firma de documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [AllowAnonymous]
        [Route("GetCantidadFirmas")]
        public RespuestaOperacionServicio GetCantidadFirmas(int CodDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ObtenerCantidadFirmas(CodDocumento, out int posx, out int posy);
                rpta.posx = posx;
                rpta.posy = posy;
                rpta.Resultado = Constantes.RESPUESTA_OK;
                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que Obtiene el origen del documento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [AllowAnonymous]
        [Route("ObtenerOrigen")]
        public RespuestaOperacionServicio ObtenerOrigen(int CodDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ObtenerCantidadFirmas(CodDocumento, out int posx, out int posy);
                rpta.posx = posx;
                rpta.posy = posy;
                rpta.Resultado = Constantes.RESPUESTA_OK;
                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        /// <summary>
        /// Servicio API que Obtiene el codigo de laserfiche por codigo dedocumento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [AllowAnonymous]
        [Route("GetCodLaserFIchePorCodDocumento")]
        public RespuestaOperacionServicio GetCodLaserFIchePorCodDocumento(int CodDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                RespuestaConsultaArchivosDTO ResEObj = new RespuestaConsultaArchivosDTO();
                List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                IDocumentoBL objServicio = new DocumentoBL();
                var CodLaserFicher = objServicio.ObtenerCodLaserFIchePorCodDocumento(CodDocumento);

                rpta.data = CodLaserFicher;
                rpta.Resultado = Constantes.RESPUESTA_OK;
                
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que Obtiene el codigo de laserfiche por codigo dedocumento
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [HttpGet]
        [AllowAnonymous]
        [Route("GetCodLaserFIcheAnexosPorCodDocumento")]
        public RespuestaOperacionServicio GetCodLaserFIcheAnexosPorCodDocumento(int CodDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                var CodLaserFicher = objServicio.ObtenerCodLaserFIcheAnexosPorCodDocumento(CodDocumento);

                rpta.listEnteros = CodLaserFicher;
                rpta.Resultado = Constantes.RESPUESTA_OK;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        /// <summary>
        /// Servicio API que Sube Documento digital
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaConsultaArchivosDTO</returns>   
        [AllowAnonymous]
        [HttpPost]
        [Route("SubirDocumentoLaserFicher")]
        public RespuestaOperacionServicio SubirDocumentoLaserFicher([FromBody]Documento objDoc)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDoc.UsuarioCreacion = nameuser;
                objDoc.LoginCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.SubirDocumentoLaserFicher(objDoc);
                rpta.Resultado = Constantes.RESPUESTA_OK;
                 }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        

        /// <summary>
        /// Servicio API que realiza el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("registrarBandejaEntrada")]
        public RespuestaOperacionServicio RegistrarDocumentoBandejaEntrada([FromBody]Documento objDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                //var bytes = Convert.FromBase64String(objDocumento.FileArray);
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;


                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarDocumentoBandejaEntrada(objDocumento, out string outCorrelativoDoc);
                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.correlativo = outCorrelativoDoc;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        

        /// <summary>
        /// Servicio API que realiza el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("registrarBandejaEnvioSIED")]
        public RespuestaOperacionServicio registrarBandejaEnvioSIED([FromBody]Documento objDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                //var bytes = Convert.FromBase64String(objDocumento.FileArray);
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;


                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarDocumentoBandejaEnvioSIED(objDocumento, out string outCorrelativoDoc);
                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.correlativo = outCorrelativoDoc;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        /// <summary>
        /// Servicio API que realiza el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("registrar")]
        public RespuestaOperacionServicio RegistrarDocumento([FromBody]Documento objDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                //var bytes = Convert.FromBase64String(objDocumento.FileArray);
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;

                //if ((bytes.Length / 1024) / 1024 > Convert.ToInt32(Configuracion.MaxLengthArchivo))
                //{

                //    rpta.Error = "El Archivo supera el tamaño permitido";
                //    rpta.Resultado = Constantes.RESPUESTA_ERROR;
                //    return rpta;
                //}

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarDocumento(objDocumento, out string outCorrelativoDoc, out int RespuestaCorreo);
                rpta.ResultadoCorreo = RespuestaCorreo;
                rpta.Resultado = Constantes.RESPUESTA_OK;
                rpta.correlativo = outCorrelativoDoc;
            }
            catch (Exception ex)
            {

                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        /// <summary>
        /// Servicio API que realiza el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna la Entidad RespuestaOperacionServicio</returns>   
        [HttpPost]
        [Route("registraravance")]
        public RespuestaOperacionServicio RegistrarAvance([FromBody]RegistrarAvanceDTO registrarAvance)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                registrarAvance.UsuarioCreacion = nameuser;
                registrarAvance.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarAvance(registrarAvance, out int resultadoCorreo);
                rpta.ResultadoCorreo = resultadoCorreo;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }


        [HttpPost]
        [Route("anular")]
        public RespuestaOperacionServicio AnularDocumento([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.AnularDocumento(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        [HttpPost]
        [Route("AnularDocumentoYDerivados")]
        public RespuestaOperacionServicio AnularDocumentoYDerivados([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.AnularDocumentoYDerivados(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        [HttpPost]
        [Route("AnularDocumentoEnvioSIED")]
        public RespuestaOperacionServicio AnularDocumentoEnvioSIED([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.AnularDocumentoEnvioSIED(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        [HttpPost]
        [Route("AnularDocumentoRecepcionSIED")]
        public RespuestaOperacionServicio AnularDocumentoRecepcionSIED([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.AnularDocumentoRecepcionSIED(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        

       [HttpPost]
        [Route("RegistrarTipoDocumento")]
        public RespuestaOperacionServicio RegistrarTipoDocumento([FromBody]TipoDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;
                objDocumento.UsuarioActualizacion = nameuser;


                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarTipoDocumento(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("ActualizarTipoDocumento")]
        public RespuestaOperacionServicio ActualizarTipoDocumento([FromBody]TipoDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;
                objDocumento.UsuarioActualizacion = nameuser;
                objDocumento.LoginActualizacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ActualizarTipoDocumento(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        [HttpPost]
        [Route("derivar")]
        public RespuestaOperacionServicio DerivarDocumento([FromBody]DerivarDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;
                objDocumento.UsuarioActualizacion = nameuser;
                objDocumento.LoginActualizacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.DerivarDocumento(objDocumento,out int resultadoCorreo);
                rpta.ResultadoCorreo = resultadoCorreo;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }
        
            [HttpPost]
        [Route("FinalizarDocumentoBandejaEntrada")]
        public RespuestaOperacionServicio FinalizarDocumentoBandejaEntrada([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                string mensaje = "";
                var status = objServicio.FinalizarDocumentoBandejaEntrada(objDocumento, out mensaje,out int RespuestaCorreo);
                rpta.data = status;
                rpta.ResultadoCorreo = RespuestaCorreo;
                if (status == -2)
                {
                    rpta.Informacion = mensaje;
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
                else
                {
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpPost]
        [Route("finalizar")]
        public RespuestaOperacionServicio FinalizarDocumento([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                string mensaje = "";
                var status = objServicio.FinalizarDocumento(objDocumento, out mensaje);
                rpta.data = status;

                if (status == -2)
                {
                    rpta.Informacion = mensaje;
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
                else
                {
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }
        [HttpPost]
        [Route("validarfinalizar")]
        public RespuestaOperacionServicio ValidarFinalizarDocumento([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                string mensaje = "";
                string codDerivados = "";
                var status = objServicio.ValidarFinalizarDocumento(objDocumento, out mensaje, out  codDerivados);
                rpta.data = status;

                if (status == -2)
                {
                    rpta.Informacion = mensaje;
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
                else
                {
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpGet]
        [Route("ValidarDevolverDocumento")]
        public RespuestaOperacionServicio ValidarDevolverDocumento(int codDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                ComentarioDocumentoDTO objDocumento = new ComentarioDocumentoDTO();
                objDocumento.CodDocumento = codDocumento;
                IDocumentoBL objServicio = new DocumentoBL();
                string mensaje = "";
                string codDerivados = "";
                var status = objServicio.ValidarDevolverDocumento(objDocumento, out mensaje, out codDerivados);
                rpta.data = status;

                if (status == -2)
                {
                    rpta.Informacion = mensaje;
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
                else
                {
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpGet]
        [Route("ValidarFirmante")]
        public RespuestaOperacionServicio ValidarFirmante(int codDocumento,int codTrabajador)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                var status = objServicio.ValidarFirmante(codDocumento, codTrabajador);
                rpta.data = status;

                if (status == -1)
                {
                    rpta.Resultado = Constantes.RESPUESTA_ERROR;
                }
                else
                {
                    rpta.Resultado = Constantes.RESPUESTA_OK;
                }
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [HttpPost]
        [Route("devolver")]
        public RespuestaOperacionServicio DevolverDocumento([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.DevolverDocumento(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {

                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }


        [HttpPost]
        [Route("devolverInterno")]
        public RespuestaOperacionServicio DevolverDocumentoInterno([FromBody]ComentarioDocumentoDTO objDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;
                objDocumento.UsuarioCreacion = nameuser;
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.DevolverDocumentoInterno(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        [HttpPost]
        [Route("ActualizarDocumentoInterno")]
        public RespuestaOperacionServicio ActualizarDocumentoInterno([FromBody]Documento objDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                //var bytes = Convert.FromBase64String(objDocumento.FileArray);
                objDocumento.UsuarioActualizacion = nameuser;
                objDocumento.LoginActualizacion = nameuser;
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;
                //if ((bytes.Length / 1024) / 1024 > Convert.ToInt32(Configuracion.MaxLengthArchivo))
                //{

                //    rpta.Error = "El Archivo supera el tamaño permitido";
                //    rpta.Resultado = Constantes.RESPUESTA_ERROR;
                //    return rpta;
                //}

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ActualizarDocumentoInterno(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        
        [HttpPost]
        [Route("ActualizarDocumentoEnvioSIED")]
        public RespuestaOperacionServicio ActualizarDocumentoEnvioSIED([FromBody]Documento objDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                objDocumento.UsuarioActualizacion = nameuser;
                objDocumento.LoginActualizacion = nameuser;
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ActualizarDocumentoInternoEnvioSIED(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        [HttpPost]
        [Route("Actualizar")]
        public RespuestaOperacionServicio ActualizarDocumento([FromBody]Documento objDocumento)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                //var bytes = Convert.FromBase64String(objDocumento.FileArray);
                objDocumento.UsuarioActualizacion = nameuser;
                objDocumento.LoginActualizacion = nameuser;
                objDocumento.UsuarioCreacion = nameuser;
                objDocumento.LoginCreacion = nameuser;
                //if ((bytes.Length / 1024) / 1024 > Convert.ToInt32(Configuracion.MaxLengthArchivo))
                //{

                //    rpta.Error = "El Archivo supera el tamaño permitido";
                //    rpta.Resultado = Constantes.RESPUESTA_ERROR;
                //    return rpta;
                //}

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ActualizarDocumento(objDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        //[HttpPost]
        //[Route("registrarDoc")]
        //public RespuestaOperacionServicio RegistrarDocumento2([FromBody]Documento objDocumento)
        //{

        //    RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
        //    try
        //    {
        //        /* IMPLEMENTACIÓN DE TOKEN*/
        //        //string token = ObtenerToken();
        //        //Token objToken = new Token() { ValorToken = token };
        //        //ITokenBL objValidacionToken = new TokenBL();
        //        //if (!objValidacionToken.OnAuthorization(ref objToken))
        //        //{
        //        //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
        //        //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
        //        //    return rpta;
        //        //};
        //        /*FIN IMPLEMENTACIÓN DE TOKEN*/
        //        if (!Request.Content.IsMimeMultipartContent("attachment"))
        //        {
        //            HttpResponseMessage resp = Request.CreateResponse(HttpStatusCode.InternalServerError);
        //        }

        //        var req = HttpContext.Current.Request;
        //        bool SubmittedFile = (req.Files.Count != 0);

        //        string vals = req.Form.GetValues("documento").First();

        //        IDocumentoBL objServicio = new DocumentoBL();
        //        rpta.data = objServicio.RegistrarDocumento(objDocumento, out string outCorrelativoDoc);
        //        rpta.Resultado = Constantes.RESPUESTA_OK;
        //    }
        //    catch (Exception ex)
        //    {
        //        rpta.Resultado = Constantes.RESPUESTA_ERROR;
        //        rpta.Error = ex.Message;
        //    }
        //    return rpta;

        //}

        // GET: api/Documento/5
        public List<DocumentoBandejaDTO> Get()
        {
            DocumentoBL NObj = new DocumentoBL();
            DocumentoBandejaParamsDTO EObj = new DocumentoBandejaParamsDTO();
            try
            {
                EObj.Descripcion = string.Empty;
                EObj.FechaFin = DateTime.Today;
                EObj.FechaInicio = EObj.FechaFin.AddDays(-30);
            }catch(Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return NObj.ListarBandejaMesaPartes(EObj);
        }

        [HttpGet]
        [Route("GetDocumentoBandeja/{id}/{fechaIni}/{fechaFin}")]
        public List<DocumentoBandejaDTO> Get([FromUri]string id, [FromUri]DateTime fechaIni, [FromUri]DateTime fechaFin)
        {
            DocumentoBL NObj = new DocumentoBL();
            DocumentoBandejaParamsDTO EObj = new DocumentoBandejaParamsDTO();
            try
            {
                var searchString = (id == "LISTARTODOS") ? "" : id;
                EObj.Descripcion = searchString;
                EObj.FechaInicio = fechaIni;
                EObj.FechaFin = fechaFin;
            }catch(Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return NObj.ListarBandejaMesaPartes(EObj);
        }
        [HttpGet]
        [Route("GetDocumentoBandejaEntrada/{tipobandeja}/{search}/{fechaRegistroDesde}/{fechaRegistroHasta}/{page}/{pagesize}/{iCodTrabajador}/{vCodPerfil}")]
        public ResultadoBusquedaDTO<DocumentoBandejaDTO> GetDocumentoBandejaEntrada([FromUri]int tipobandeja,
            [FromUri]string search, [FromUri] DateTime fechaRegistroDesde,
            [FromUri]DateTime fechaRegistroHasta, [FromUri]int page,
            [FromUri]int pagesize, [FromUri] int iCodTrabajador, [FromUri] string vCodPerfil)
        {
            ResultadoBusquedaDTO<DocumentoBandejaDTO> salida = new ResultadoBusquedaDTO<DocumentoBandejaDTO>();
            DocumentoBL NObj = new DocumentoBL();
            DocumentoBandejaEntradaDTO EObj = new DocumentoBandejaEntradaDTO();
             
            try
            {
                search = (search == "VACIO" ? "" : search);
                  EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;
                EObj.FechaDesde = fechaRegistroDesde;
                EObj.FechaHasta = fechaRegistroHasta;
                EObj.TipoBandeja = tipobandeja;
                EObj.CodTrabajador = iCodTrabajador;
                EObj.CodPerfil = vCodPerfil;
                int totalRegistros = 0;
                salida.Lista = NObj.ListarBandejaEntrada(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }catch(Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return salida;
        }


        [HttpGet]
        [Route("GetDocumentoBandejaRecepcionSIED/{tipobandeja}/{search}/{fechaRegistroDesde}/{fechaRegistroHasta}/{page}/{pagesize}/{iCodTrabajador}/{vCodPerfil}")]
        public ResultadoBusquedaDTO<DocumentoBandejaDTO> GetDocumentoBandejaRecepcionSIED([FromUri]int tipobandeja,
        [FromUri]string search, [FromUri] DateTime fechaRegistroDesde,
        [FromUri]DateTime fechaRegistroHasta, [FromUri]int page,
        [FromUri]int pagesize, [FromUri] int iCodTrabajador, [FromUri] string vCodPerfil)
            {
                ResultadoBusquedaDTO<DocumentoBandejaDTO> salida = new ResultadoBusquedaDTO<DocumentoBandejaDTO>();
            DocumentoBL NObj = new DocumentoBL();
            DocumentoBandejaEntradaDTO EObj = new DocumentoBandejaEntradaDTO();

            try
            {
                search = (search == "VACIO" ? "" : search);
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;
                EObj.FechaDesde = fechaRegistroDesde;
                EObj.FechaHasta = fechaRegistroHasta;
                EObj.TipoBandeja = tipobandeja;
                EObj.CodTrabajador = iCodTrabajador;
                EObj.CodPerfil = vCodPerfil;
                int totalRegistros = 0;
                salida.Lista = NObj.ListarBandejaRecepcionSIEDFONAFE(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return salida;
        }


        [HttpGet]
        [Route("GetDocumentoBandejaEnvioSIED/{search}/{fechaRegistroDesde}/{fechaRegistroHasta}/{page}/{pagesize}/{iCodTrabajador}")]
        public ResultadoBusquedaDTO<DocumentoBandejaDTO> GetDocumentoBandejaEnvioSIED(
         [FromUri]string search, [FromUri] DateTime fechaRegistroDesde,
         [FromUri]DateTime fechaRegistroHasta, [FromUri]int page,
         [FromUri]int pagesize, [FromUri] int iCodTrabajador)
        {
            ResultadoBusquedaDTO<DocumentoBandejaDTO> salida = new ResultadoBusquedaDTO<DocumentoBandejaDTO>();
            DocumentoBL NObj = new DocumentoBL();
            DocumentoBandejaEntradaDTO EObj = new DocumentoBandejaEntradaDTO();

            try
            {
                search = (search == "VACIO" ? "" : search);
                EObj.Pagina = page;
                EObj.TextoBusqueda = search;
                EObj.TamanioPagina = pagesize;
                EObj.FechaDesde = fechaRegistroDesde;
                EObj.FechaHasta = fechaRegistroHasta;
                EObj.CodTrabajador = iCodTrabajador;
                int totalRegistros = 0;
                salida.Lista = NObj.ListarBandejaEnvioSied(EObj, out totalRegistros);
                salida.TotalRegistros = totalRegistros;
                salida.PaginaActual = page;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
            }
            return salida;
        }


        /// <summary>
        /// Servicio API que obtiene Datos del seguimiento de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetSeguimientoDocumento")]
        public ResultadoBusquedaDTO<DocumentoSeguimientoDTO> GetSeguimientoDocumento(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocumentoSeguimientoDTO> salida = new ResultadoBusquedaDTO<DocumentoSeguimientoDTO>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                salida.Lista = objServicio.GetSeguimientoDocumento(id);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }


        /// <summary>
        /// Servicio API que obtiene Datos del seguimiento de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetCabeceraSeguimiento")]
        public ResultadoBusquedaDTO<DocumentoSeguimientoDTO> GetCabeceraSeguimiento(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocumentoSeguimientoDTO> salida = new ResultadoBusquedaDTO<DocumentoSeguimientoDTO>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                salida.Lista = objServicio.GetCabeceraSeguimiento(id);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }

        /// <summary>
        /// Servicio API que obtiene Datos del seguimiento de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetCabeceraSeguimientoInterno")]
        public ResultadoBusquedaDTO<DocumentoSeguimientoDTO> GetCabeceraSeguimientoInterno(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocumentoSeguimientoDTO> salida = new ResultadoBusquedaDTO<DocumentoSeguimientoDTO>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                salida.Lista = objServicio.GetCabeceraSeguimientoInterno(id);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }

        /// <summary>
        /// Servicio API que obtiene Datos del seguimiento de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetDocumentoDerivado")]
        public ResultadoBusquedaDTO<DocDerivacion> GetDocumentoDerivado(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocDerivacion> salida = new ResultadoBusquedaDTO<DocDerivacion>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                DocDerivacion doc = new DocDerivacion();
                doc.CodDocumento = id;
                salida.Lista = objServicio.ObtenerDocumentoDerivado(doc);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }

        /// <summary>
        /// Servicio API que obtiene Datos del seguimiento de un Documento
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetDetalleSeguimiento")]
        public ResultadoBusquedaDTO<DocumentoSeguimientoDTO> GetDetalleSeguimiento(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<DocumentoSeguimientoDTO> salida = new ResultadoBusquedaDTO<DocumentoSeguimientoDTO>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                salida.Lista = objServicio.GetDetalleSeguimiento(id);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }

        /// <summary>
        /// Servicio API que obtiene Datos del comentario
        /// </summary>
        /// <returns>Retorna la Entidad ResultadoBusquedaDTO</returns>   
        [HttpGet]
        [Route("GetComentariosDocumento")]
        public ResultadoBusquedaDTO<ComentarioDocumentoDTO> GetComentariosDocumento(int id)
        {
            //A este metodo falta agregarle un parametro que controle si se desea buscar del laserfiche, para solo usarlo cuando es necesario.
            ResultadoBusquedaDTO<ComentarioDocumentoDTO> salida = new ResultadoBusquedaDTO<ComentarioDocumentoDTO>();
            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                salida.Lista = objServicio.GetComentariosDocumento(id);
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                salida.Resultado = Constantes.RESPUESTA_ERROR;
                salida.Error = ex.Message;
            }
            return salida;
        }

        // PUT: api/Documento/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Documento/5
        public void Delete(int id)
        {
        }
        [HttpGet]
        [Route("GetPlazoDoc/{cantPlazo}/{fechaIni}")]
        public RespuestaOperacionServicio CalcularFechaPlazoDocumento([FromUri]int cantPlazo, [FromUri]DateTime fechaIni)
        {

            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                /* IMPLEMENTACIÓN DE TOKEN*/
                //string token = ObtenerToken();
                //Token objToken = new Token() { ValorToken = token };
                //ITokenBL objValidacionToken = new TokenBL();
                //if (!objValidacionToken.OnAuthorization(ref objToken))
                //{
                //    rpta.Resultado = Constants.RESPUESTA_SESION_EXPIRADA;
                //    rpta.Error = Constants.MENSAJE_ERROR_LOGIN;
                //    return rpta;
                //};
                /*FIN IMPLEMENTACIÓN DE TOKEN*/

                IDocumentoBL objServicio = new DocumentoBL();
                DateTime fechaPlazo = objServicio.CalcularFechaPlazoDocumento(cantPlazo, fechaIni);

                rpta.data = fechaPlazo;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("EliminarDocLaserfiche/{idCodLaserfiche}")]
        public RespuestaOperacionServicio EliminarDocumentoLaserfiche([FromUri]int idCodLaserfiche)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();

            try
            {
                LaserFiche objLaserFiche = new LaserFiche();
                objLaserFiche.EliminarDocumento(idCodLaserfiche);

                //rpta.data = fechaPlazo;
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog( ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }

            return rpta;
        }
        private void generarlog( Exception ex, string method)
        {
            GenerarLog log = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = method + " - " +
                nameuser
                + " " + DateTime.Now + "\n" + "\n" +
               ex.Message + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            log.GenerarArchivoLogApi(detalleError);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("ObtenerDocumentoLaserfiche/{idCodLaserfiche}")]
        public RespuestaOperacionServicio ObtenerDocumentoLaserfiche([FromUri]int idCodLaserfiche)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                RespuestaConsultaArchivosDTO ResEObj = new RespuestaConsultaArchivosDTO();
                List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                ListaDocLaser.Add(new DocumentoItemFileDTO()
                {
                    Id = idCodLaserfiche
                });

                EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                laserEObj.ListaDocumentos = ListaDocLaser;

                LaserFiche laserObj = new LaserFiche();
                ResEObj = laserObj.ConsultaArchivos(laserEObj);

                rpta.data = ResEObj;
                rpta.Resultado = Constantes.RESPUESTA_OK;

                return rpta;
                

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }


        [AllowAnonymous]
        [HttpGet]
        [Route("ObtenerDocumentoLaserficheIMG/{idCodLaserfiche}")]
        public RespuestaOperacionServicio ObtenerDocumentoLaserficheIMG([FromUri]int idCodLaserfiche)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                RespuestaConsultaArchivosDTO ResEObj = new RespuestaConsultaArchivosDTO();
                List<DocumentoItemFileDTO> ListaDocLaser = new List<DocumentoItemFileDTO>();
                ListaDocLaser.Add(new DocumentoItemFileDTO()
                {
                    Id = idCodLaserfiche
                });

                EnvioConsultaArchivosDTO laserEObj = new EnvioConsultaArchivosDTO();
                laserEObj.ListaDocumentos = ListaDocLaser;

                LaserFiche laserObj = new LaserFiche();
                ResEObj = laserObj.ConsultaArchivosIMG(laserEObj);

                rpta.data = ResEObj;
                rpta.Resultado = Constantes.RESPUESTA_OK;

                return rpta;

            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }
            return rpta;
        }

        [HttpPost]
        [Route("registrarcargo")]
        public RespuestaOperacionServicio RegistrarCargoDocumento([FromBody]RegistroCargoDTO registroCargo)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                registroCargo.UsuarioCreacion = nameuser;
                registroCargo.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarCargo(registroCargo);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }

            return rpta;
        }

        [HttpPost]
        [Route("registrarcargoSIED")]
        public RespuestaOperacionServicio RegistrarCargoDocumentoSIED([FromBody]RegistroCargoDTO registroCargo)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();
            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                registroCargo.UsuarioCreacion = nameuser;
                registroCargo.LoginCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.RegistrarCargoSIED(registroCargo);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }

            return rpta;
        }

        [HttpGet]
        [Route("ObtenerTotalDerivaciones/{codDocumento}")]
        public RespuestaOperacionServicio ObtenerTotalDerivaciones([FromUri]int codDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();

            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                int tipoAcceso = 1;//Tipo Acceso : 1 => Derivación distinta a Con Copia
                rpta.data = objServicio.ObtenerTotalDerivaciones(codDocumento, tipoAcceso);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }

            return rpta;
        }

        [HttpPost]
        [Route("AnularDerivacion")]
        public RespuestaOperacionServicio AnularDerivacion([FromBody]DocDerivacion docDerivacion)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();

            try
            {
                var user = HttpContext.Current.User;
                var nameuser = user.Identity.Name;

                docDerivacion.UsuarioCreacion = nameuser;
                docDerivacion.LoginCreacion = nameuser;
                docDerivacion.RolCreacion = nameuser;

                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.AnularDerivacion(docDerivacion, docDerivacion.Comentarios);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }

            return rpta;
        }

        [HttpGet]
        [Route("ObtenerEstado/{codDocumento}")]
        public RespuestaOperacionServicio ObtenerEstado([FromUri]int codDocumento)
        {
            RespuestaOperacionServicio rpta = new RespuestaOperacionServicio();

            try
            {
                IDocumentoBL objServicio = new DocumentoBL();
                rpta.data = objServicio.ObtenerEstado(codDocumento);
                rpta.Resultado = Constantes.RESPUESTA_OK;
            }
            catch (Exception ex)
            {
                StackTrace stackTrace = new StackTrace();
                var method = stackTrace.GetFrame(0).GetMethod().Name;
                generarlog(ex, method);
                rpta.Resultado = Constantes.RESPUESTA_ERROR;
                rpta.Error = ex.Message;
            }

            return rpta;
        }

    }
}
    