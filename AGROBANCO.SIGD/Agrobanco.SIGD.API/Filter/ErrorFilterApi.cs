﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Agrobanco.SIGD.API.Filter
{
    public class ErrorFilterApi : HandleErrorAttribute
    {

        public override void OnException(ExceptionContext filterContext)
        {
            GenerarLog objLOG = new GenerarLog();
            var user = HttpContext.Current.User;
            var nameuser = user.Identity.Name;
            string detalleError = filterContext.Controller.ToString() + " - " +
                nameuser +
                " " + DateTime.Now + "\n" + "\n" +
               filterContext.Exception.ToString() + "\n" +
                "-------------------------------------------------------------------------------------------------------------";
            objLOG.GenerarArchivoLogApi(detalleError);

            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                var httpException = filterContext.Exception as HttpException;
                var errorCode = 500;
                var routeValues = new
                {
                    action = "ErrorAjax",
                    controller = "Error"
                };

                if (httpException != null)
                {
                    errorCode = httpException.ErrorCode;

                    switch (errorCode)
                    {
                        case 404:
                            routeValues = new
                            {
                                action = "NoEncontradoAjax",
                                controller = "Error"
                            };

                            break;
                        case 403:
                            routeValues = new
                            {
                                action = "AccesoRestringidoAjax",
                                controller = "Error"
                            };

                            break;
                    }
                }

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(routeValues));
                filterContext.ExceptionHandled = true;
            }
            else
            {
                var routeValues = new
                {
                    action = "NoEncontrado",
                    controller = "Error"
                };

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(routeValues));
                filterContext.ExceptionHandled = true;
            }

            base.OnException(filterContext);
        }
    }
}