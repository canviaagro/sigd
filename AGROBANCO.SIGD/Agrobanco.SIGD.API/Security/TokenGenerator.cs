﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Agrobanco.SIGD.API.Security
{
    public static class TokenGenerator
    {
        public static string GenerateTokenJwt(string username, string password)
        {
            //TODO: appsetting for Demo JWT - protect correctly this settings
            var secretKey = ConfigurationManager.AppSettings["JWT_SECRET_KEY"];
            var audienceToken = ConfigurationManager.AppSettings["JWT_AUDIENCE_TOKEN"];
            var issuerToken = ConfigurationManager.AppSettings["JWT_ISSUER_TOKEN"];
            var expireTime = ConfigurationManager.AppSettings["JWT_EXPIRE_MINUTES"];

            var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(secretKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            // create a claimsIdentity 
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.Name, username)
                //,new Claim("Password", password)
            });


            // create token to the user 
            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
                audience: audienceToken,
                issuer: issuerToken,
                subject: claimsIdentity,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
                signingCredentials: signingCredentials);

            var jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);

            //DateTime dtt = DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime));

            //var handler = new JwtSecurityTokenHandler();
            //var token = handler.ReadJwtToken(jwtTokenString);
            //var tokenvalor = token.Payload.Exp.Value;
            //DateTime runtimeKnowsThisIsUtc = DateTime.SpecifyKind(tokenvalor, DateTimeKind.Utc);
            //int valnum = 1000;
            //DateTime dtttt = new DateTime(tokenvalor * valnum);
            //DateTime dtttt2 = Convert.ToDateTime(tokenvalor * valnum);

            return jwtTokenString;
        }
    }
}