﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.API.Models
{
    public class Trabajador
    {
        public int IdTrabajador { get; set; }
        public string Nombres { get; set; }
    }
}