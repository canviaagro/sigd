﻿

$(document).ready(function () {

    var urlBase = $("#hdUrlBase").val();
    var fechaIniDefault;
    var fechaFinDefault;
    var fechaDocDefaultEdit;
    var fechaRecepcionDefaultEdit;
    var _fechaRecepcion;
    var _fechaDocumento;
    var ResultRemitente = [];
    var ResultAreas = [];
    var confirm = true;
    var confirmEdit = true;
    var _EstadoDocumento;
    var areasTODevolucion = "";
    var areasCCDevolucion = "";

    var mesaPartes = (function () {

        var inputPlazo = $(".input-plazo");
        var inputPlazoEdit = $(".input-plazoEdit");
        var inputFechaPlazo = $(".input-fecha-plazo")[0];
        var inputFechaPlazoEdit = $(".input-fecha-plazoEdit")[0];
        var inputDatePicker = $("input.datepicker");
        var lblFechaDefault = $("label.lbl-fecha-default");
        var inputFechaRecepcion = $("#FechaRecepcion");
        var inputFechaDocumento = $("#fechaDocumento");
        var inputFechaRecepcionEdit = $("#FechaRecepcionEdit");
        var inputfechaDocumentoEdit = $("#fechaDocumentoEdit");
        var remitenteAutoComplete = $("input[name='remitente'].autocomplete");
        var asuntoAutoComplete = $("input[name='asunto'].autocomplete");
        var asuntoAutoCompleteEdit = $("input[name='asuntoEdit'].autocomplete");
        var hdCodRemitente = $("#hdCodRemitente");
        var hdDescripcionRemitente = $("#hdDescripcionRemitente");
        var cboRemitidoPor = $("#remitidoPor");
        var hdCodArea = $("#hdCodArea");
        var hdCodAreaCC = $("#hdCodAreaCC");
        var hdCodAreaCCEdit = $("hdCodAreaCCEdit");
        var hdCodAsunto = $("#hdCodAsunto");
        var hdCodAsuntoEdit = $("#hdCodAsuntoEdit");
        var inputAreas = $(".chips-autocomplete.chips-areas input");
        var btnLimpiarAdjunto = $("#btnLimpiarAdjunto");
        var btnLimpiarAnexoEdit = $("#btnLimpiarAnexoEdit");
        var btnLimpiarAnexo = $("#btnLimpiarAnexo");
        var btnLimpiarAdjuntoEdit = $("#btnLimpiarAdjuntoEdit");
        var inputAdjunto = $("#adjuntarArchivo");
        var inputAnexo = $("#adjuntarAnexo");
        var inputAdjuntoEdit = $("#adjuntarArchivoEdit");
        var inputAnexoEdit = $("#adjuntarAnexoEdit");
        var fechaRegistroDesde = $("#FechaRegistroDesde");
        var fechaRegistroHasta = $("#FechaRegistroHasta");
        var btnCancelarEdit = $("#btnCancelarEdit");
        var btnCancelar = $("#btnCancelar");
        var btnAnular = $("#btnAnular");
        var btnGuardar = $("#btnGuardar");
        var btnEdit = $("#btnEdit");
        var btnAgree = $("#btnAgree");

        var init = function () {
            remitenteAutoComplete.autocomplete({
                onAutocomplete: onEmpresaAutocomplete,
                data: {}
            });

            asuntoAutoComplete.autocomplete({
                onAutocomplete: onAsuntoAutocomplete,
                data: {}
            });

            asuntoAutoCompleteEdit.autocomplete({
                onAutocomplete: onAsuntoAutocomplete,
                data: {}
            });

            inputPlazo.on("change", plazoChange);
            inputPlazoEdit.on("change", plazoChangeEdit);
            inputFechaRecepcion.on("change", fechaRecepcionChange);
            inputFechaRecepcion.on("blur", fechaRecepcionChange);
            // remitenteAutoComplete.on("blur", remitenteBlur);
            asuntoAutoComplete.on("blur", asuntoBlur);
            btnLimpiarAdjunto.on("click", btnLimpiarAdjuntoClick);
            btnLimpiarAnexoEdit.on("click", btnLimpiarAnexoEditClick);
            btnLimpiarAnexo.on("click", btnLimpiarAnexoClick);
            btnLimpiarAdjuntoEdit.on("click", btnLimpiarAdjuntoEditClick);
            inputAdjunto.on("change", adjuntoChange);
            inputAnexo.on("change", anexoChange);
            inputAdjuntoEdit.on("change", adjuntoChange);
            inputAnexoEdit.on("change", anexoChangeEdit);
            fechaRegistroDesde.on("change", fechaRegIniChange);
            fechaRegistroHasta.on("change", fechaRegFinChange);
            cboRemitidoPor.on("change", cboRemitidoPorChange);
            btnGuardar.on("click", btnGuardarClick);
            btnEdit.on("click", btnEditClick);
            btnAgree.on("click", btnAgreeClick);
            btnCancelarEdit.on("click", btnCancelarEditClick);
            btnAnular.on("click", btnAnularClick);
            btnCancelar.on("click", btnCancelarClick);
            inputFechaRecepcion.on("change", validacionFechasChange);
            inputFechaDocumento.on("change", validacionFechasChange);
            inputFechaRecepcionEdit.on("change", fechaRecepcionChangeEdit);
            inputFechaRecepcionEdit.on("blur", fechaRecepcionChangeEdit);
            inputFechaRecepcionEdit.on("change", validacionFechasEditChange);
            inputfechaDocumentoEdit.on("change", validacionFechasEditChange);
            cargarFormNuevoDocumento();

        };

        var cargarFormNuevoDocumento = function () {
            
            var fechaHoy = new Date();
            var plazoDefault = parseInt($(inputPlazo).val());
            var currentMonth = fechaHoy.getMonth() + 1;
            var currentDay = fechaHoy.getDate();
            //var resultDate = new Date();
            //resultDate.setDate(fechaHoy.getDate() + plazoDefault);
            //var resultDay = resultDate.getDate();
            //var resultMonth = resultDate.getMonth() + 1;

            var strFecha = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaHoy.getFullYear();
            //var strFechaPlazoDefault = lpad((resultDay).toString(), "0", 2) + "/" + lpad(resultMonth.toString(), "0", 2) + "/" + resultDate.getFullYear();

            updateFechaPlazo(plazoDefault, fechaHoy);
            //cargarEmpresas();
            cargarRemitente();
            cargarAreas();
            cargarAsuntos();

            $(inputDatePicker).val(strFecha);
            //$(inputFechaPlazo).val(strFechaPlazoDefault);
            $(lblFechaDefault).addClass("active");

            var newdate = new Date(fechaHoy);

            newdate.setDate(fechaHoy.getDate() - 30);

            var dd = newdate.getDate().toString();
            dd = dd.length > 1 ? dd : '0' + dd;

            var mm = (newdate.getMonth() + 1).toString();
            mm = mm.length > 1 ? mm : '0' + mm;

            var y = newdate.getFullYear();
            var someFormattedDate = dd + '/' + mm + '/' + y;
            _fechaDocumento = strFecha;
            _fechaRecepcion = _fechaDocumento;
            $("#FechaRegistroDesde").val(someFormattedDate);
            validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
            fechaIniDefault = $("#FechaRegistroDesde").val(); 
            fechaFinDefault = $("#FechaRegistroHasta").val(); 
        };

        var onChipAdd = function (e) {

            validarInputChips('.chips-areas');
            validarCantDestinatarosChipsTO('.chips-areas');
            validarInputChipsCCTO('.chips-areas', '.chips-areasCC');

            $("#chipTO").find('input').val("");
            $("#chipCC").find('input').val("");

            if ($("#chipTO").find("div").length > 1) {
                $("#chipTO").find("div")[1].remove();
            } else {

                var value = $("#chipTO").find(".chip").first().text();
                var str = value.split("|");
                var codigo = str[0];
                var descripcion = codigo.replace("close", "").trim();

                if (descripcion != "") {
                    for (var x = 0; x < ResultAreas.length; x++) {
                        var valor = (ResultAreas[x].descripcionArea).indexOf(descripcion);
                        if (valor != -1) {
                            codigo = ResultAreas[x].codigoArea.toString();
                            break;
                        } else {
                            codigo = "";
                        }
                    }
                }

                if (codigo.toString().length > 0) {
                    $(".chips-autocomplete.chips-areas input").prop("required", false);
                }

                if (codigo.toString().length == 0) {
                    $(".chips-autocomplete.chips-areas input").prop("required", true);
                }

                hdCodArea.val(codigo);
            }
        };



        var onChipAddRemitente = function (e) {
            hdCodRemitente.val("");
            hdDescripcionRemitente.val("");
            limpiarCboRemitidoPor();
            if (validarInputChips('.chips-remitente') == 0) {
            }

            if ($("#chipRemitente").find("div").length > 1) {
                $("#chipRemitente").find("div")[1].remove();
            } else {
                var value = $("#chipRemitente").find(".chip").first().text();
                var str = value.split("|");
                var codigo = str[0];
                var descripcion = codigo.replace("close", "").trim();

                if (descripcion != "") {
                    for (var x = 0; x < ResultRemitente.length; x++) {
                        var valor = (ResultRemitente[x].descripcionRemitente).indexOf(descripcion);
                        if (valor != -1) {
                            codigo = ResultRemitente[x].codigoRemitente.toString();
                            hdDescripcionRemitente.val(descripcion);
                            break;
                        } else {
                            codigo = "";
                        }
                    }
                }
                

                if (codigo.toString().length > 0) {
                    obtenerTrabajadoresPorEmpresa(bindTrabajadoresCombo, codigo);
                    $(".chips-autocomplete.chips-remitente input").prop("required", false);
                }

                if (codigo.toString().length == 0) {

                    $(".chips-autocomplete.chips-remitente input").prop("required", true);
                }

                hdCodRemitente.val(codigo);
            }
        };


        var blurChip = function () {
            $("#chipRemitente").find("input").val("");
            $("#chipTO").find('input').val("");
            $("#chipCC").find('input').val("");
            $("#chipCCEdit").find('input').val("");
        }

        var onChipAddCC = function (e) {
            hdCodAreaCC.val("");
            if (validarInputChips('.chips-areasCC') == 0) {
                return false;
            }
            validarInputChipsTOCC('.chips-areas', '.chips-areasCC');

            var chipsCC = M.Chips.getInstance($('.chips-areasCC')).chipsData;
            var codigosCC = "";
            if (chipsCC.length > 0) {
                var chipsCCtag = M.Chips.getInstance($('.chips-areasCC')).chipsData;
                for (var x = 0; x < chipsCC.length; x++) {
                    for (var y = 0; y < ResultAreas.length; y++) {
                        var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                        if (valor != -1) {
                            codigo = ResultAreas[y].codigoArea.toString();
                            codigosCC = codigosCC + ((codigosCC.length > 0) ? '|' : "") + codigo;
                        }
                    }
                }
            }
            hdCodAreaCC.val(codigosCC);
            if (codigosCC.toString().length > 0) {
                $(".chips-autocomplete.chips-areasCC input").prop("required", false);
            }

            if (codigosCC.toString().length == 0) {
                $(".chips-autocomplete.chips-areasCC input").prop("required", true);
            }

            //  hdCodAreaCC.val(codigo);
        };

        var plazoChange = function () {
            var plazo = inputPlazo[0].value;
            if (plazo > 365) {
                plazo = 365;
            }
            var strFecRecepcion = inputFechaRecepcion[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazo(plazo, fechaInicio);
        }
        var plazoChangeEdit = function () {
            var plazo = inputPlazoEdit[0].value;
            if (plazo > 365) {
                plazo = 365;
            }
            var strFecRecepcion = inputFechaRecepcionEdit[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazoEdit(plazo, fechaInicio);
        }
        var fechaRecepcionChange = function () {
            var plazo = inputPlazo[0].value;
            var strFecRecepcion = inputFechaRecepcion[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazo(plazo, fechaInicio);
        };

        var fechaRecepcionChangeEdit = function () {
            var plazo = inputPlazoEdit[0].value;
            var strFecRecepcion = inputFechaRecepcionEdit[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazoEdit(plazo, fechaInicio);
        };

        var updateFechaPlazo = function (plazo, fechaInicio) {
            var params = JSON.stringify({
                plazo: plazo,
                fechaInicio: fechaInicio
            });

            obtenerFechaPlazo(plazo, fechaInicio, function (result) {
                if (result.Resultado == 1) {
                    var fechaPlazo = new Date(parseInt(result.data.toString().substr(6)));

                    var currentMonth = (fechaPlazo.getMonth() + 1);
                    var currentDay = fechaPlazo.getDate();

                    var strFechaPlazo = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaPlazo.getFullYear();

                    $(inputFechaPlazo).val(strFechaPlazo);
                }
            });
        };

        var updateFechaPlazoEdit = function (plazo, fechaInicio) {
            obtenerFechaPlazo(plazo, fechaInicio, function (result) {
                if (result.Resultado === 1) {
                    var fechaPlazo = new Date(parseInt(result.data.toString().substr(6)));

                    var currentMonth = (fechaPlazo.getMonth() + 1);
                    var currentDay = fechaPlazo.getDate();

                    var strFechaPlazo = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaPlazo.getFullYear();

                    $(inputFechaPlazoEdit).val(strFechaPlazo);
                }
            });
        };
        var lpad = function (str, padString, length) {
            //var str = this;
            while (str.length < length)
                str = padString + str;
            return str;
        };



        var bindEmpresasAutoComplete = function (obj) {
            //var elem = $("#frmNuevoDocumento input[name='remitente'].autocomplete")[0];

            //var instance = M.Autocomplete.getInstance(remitenteAutoComplete);
            //var result = {};
            //for (var i = 0; i < obj.length; i++) {                
            //    result[(obj[i].CodEmpresa + "|" + obj[i].Descripcion).toString()] = null;
            //}

            //instance.updateData(result);
        };

        var cargarEmpresas = function () {
            obtenerEmpresas(bindEmpresasAutoComplete);
        };

        var onEmpresaAutocomplete = function (e) {
            var str = e.split("|");
            var codigo = str[0];
            hdCodRemitente.val(codigo);
            cargarTrabajadores(codigo);
        };

        var remitenteBlur = function () {
            remitenteAutoComplete.val(remitenteAutoComplete.val().trim());
            var remitente = remitenteAutoComplete.val();
            var arrCodRemitente = remitente.split('|');

            var limpiarCboRemitidoPor = (hdCodRemitente.val() == arrCodRemitente[0]) ? false : true;

            if (remitenteAutoComplete.val().length == 0 || limpiarCboRemitidoPor) {
                cboRemitidoPor.empty();
                cboRemitidoPor.append("<option value=''>Seleccione</option>");
                cboRemitidoPor.formSelect();
            }
        };

        var limpiarCboRemitidoPor = function () {
            cboRemitidoPor.empty();
            cboRemitidoPor.append("<option value=''>Seleccione</option>");
            cboRemitidoPor.formSelect();
        }

        var asuntoBlur = function () {

            var insAsunto = M.Autocomplete.getInstance(asuntoAutoComplete);

            console.log(insAsunto);
        };



        var bindTrabajadoresCombo = function (trabajadores) {
            cboRemitidoPor.empty();
            cboRemitidoPor.append("<option value=''>Seleccione</option>")
            $.each(trabajadores, function (ind, val) {
                cboRemitidoPor.append("<option value='" + val.CodRepresentante + "'>" + val.Nombre + "</option>")
            });
            cboRemitidoPor.formSelect();
        };



        var cargarTrabajadores = function (codEmpresa) {

            var remitente = $("#remitente").val();

            obtenerTrabajadoresPorEmpresa(bindTrabajadoresCombo, codEmpresa);
        }

        var cargarRemitente = function () {

            obtenerEmpresas(function (data) {
                ResultRemitente = [];
                var result = {};
                for (var i = 0; i < data.length; i++) {
                    var remitente = {
                        codigoRemitente: data[i].CodEmpresa,
                        descripcionRemitente: data[i].Descripcion
                    }
                    result[(data[i].Descripcion).toString()] = null;
                    ResultRemitente.push(remitente);
                }

                $('#chipRemitente').chips({
                    onChipAdd: onChipAddRemitente,
                    onChipDelete: onChipAddRemitente,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });

                $(".chips-autocomplete.chips-remitente input").prop("required", true);
                $(".chips-autocomplete.chips-remitente input").on("blur", blurChip);
            });
        };

        var cargarAreas = function () {

            obtenerAreas(function (data) {
                ResultAreas = [];
                var result = {};
                for (var i = 0; i < data.length; i++) {
                    var area = {
                        codigoArea: data[i].CodArea,
                        descripcionArea: data[i].Descripcion
                    }
                    result[(data[i].Descripcion).toString()] = null;
                    ResultAreas.push(area);
                }



                $('#chipTO').chips({
                    onChipAdd: onChipAdd,
                    onChipDelete: onChipAdd,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });

                $('#chipCC').chips({
                    onChipAdd: onChipAddCC,
                    onChipDelete: onChipAddCC,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });


                $(".chips-autocomplete.chips-areas input").prop("required", true);
                $(".chips-autocomplete.chips-areas input").on("blur", blurChip);
                $(".chips-autocomplete.chips-areasCC input").on("blur", blurChip);
                $(".chips-autocomplete.chips-areasCCEdit input").on("blur", blurChip);
            });
        };



        var cargarAsuntos = function () {
            obtenerAsuntos(function (data) {
                var result = {};
                for (var i = 0; i < data.length; i++) {
                    result[(data[i].Descripcion).toString()] = null;
                }

                var instance = M.Autocomplete.getInstance(asuntoAutoComplete);
                instance.updateData(result);
                var instanceEdit = M.Autocomplete.getInstance(asuntoAutoCompleteEdit);
                instanceEdit.updateData(result);

            });
        };



        var onAsuntoAutocomplete = function (e) {
            var str = e.split("|");
            var codigo = str[0];
            hdCodAsunto.val(codigo);
            hdCodAsuntoEdit.val(codigo);
        };

        var btnLimpiarAnexoEditClick = function () {
            $("#adjuntarAnexoEdit").val('');
            $("#adjuntarAnexoAuxEdit").val('');
        }
        var btnLimpiarAnexoClick = function () {
            $("#adjuntarAnexo").val('');
            $("#adjuntarAnexoAux").val('');
        }

        var btnLimpiarAdjuntoClick = function () {
            $("#adjuntarArchivo").val('');
            $("#adjuntarArchivoAux").val('');
            $("#estadoDocumento").attr("data-badge-caption", "INGRESADO");
        };

        var btnLimpiarAdjuntoEditClick = function () {
            $("#adjuntarArchivoEdit").val('');
            $("#adjuntarArchivoAuxEdit").val('');
            $("#estadoDocumentoEdit").attr("data-badge-caption", "INGRESADO");
        };

        var adjuntoChange = function (event) {

            var file = event.target.files[0];
            var formatoOK = true;
            var tamanioOK = true;

            if (file == null)
                return;

            if (!file.type.match('application/pdf')) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("Solo se permiten archivos en formatos .PDF");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAdjuntoClick();
                formatoOK = false;
                //$("#form-id").get(0).reset(); //the tricky part is to "empty" the input file here I reset the form.
                return false;
            }

            if (file.size >= 5 * 1024 * 1024) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("Tamaño máximo permitido 5MB");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAdjuntoClick();
                tamanioOK = false;
                return false;
            }

            if (formatoOK && tamanioOK) {
                $("#estadoDocumento").attr("data-badge-caption", "PENDIENTE");
                $("#estadoDocumentoEdit").attr("data-badge-caption", "PENDIENTE");
            }
        };


        var anexoChange = function (event) {

            var file = inputAnexo[0].files;
            var sumaAnexos = 0;
            if (file == null)
                return;

            for (var x = 0; x < file.length; x++) {
                    sumaAnexos = sumaAnexos + inputAnexo[0].files[x].size;  
            }

            if (sumaAnexos >= 5 * 1024 * 1024) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("El total máximo permitido debe ser 5MB");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAnexoClick();
                tamanioOK = false;
                return false;
            }

        };
        var validacionFechasChange = function () {

            var validarformatofechaRecep = validarFormatoFecha(inputFechaRecepcion.val());
            var validarformatofechaDoc = validarFormatoFecha(inputFechaDocumento.val());
            if (validarformatofechaRecep == true && validarformatofechaDoc == true) {

                if (inputFechaRecepcion.val() == "") {
                    inputFechaRecepcion.val(_fechaDocumento);
                }

                if (inputFechaDocumento.val() == "") {
                    inputFechaDocumento.val(_fechaDocumento);
                }

                if (fechaComparar(inputFechaDocumento.val()) > fechaComparar(inputFechaRecepcion.val())) {
                    inputFechaRecepcion.val(inputFechaDocumento.val());
                }

                validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
            } else {

                if (!validarformatofechaRecep) {
                    inputFechaRecepcion.val(_fechaDocumento);
                    if (fechaComparar(inputFechaDocumento.val()) > fechaComparar(inputFechaRecepcion.val())) {
                        inputFechaRecepcion.val(inputFechaDocumento.val());
                    }
                    validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
                } else if (!validarformatofechaDoc) {
                    inputFechaDocumento.val(_fechaDocumento);
                    validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
                }
                return false;
            }

        };

        var validacionFechasEditChange = function () {

            var validarformatofechaRecep = validarFormatoFecha(inputFechaRecepcionEdit.val());
            var validarformatofechaDoc = validarFormatoFecha(inputfechaDocumentoEdit.val());
            if (validarformatofechaRecep == true && validarformatofechaDoc == true) {

                if (inputFechaRecepcionEdit.val() == "") {
                    inputFechaRecepcionEdit.val(fechaRecepcionDefaultEdit);
                }

                if (inputfechaDocumentoEdit.val() == "") {
                    inputfechaDocumentoEdit.val(fechaDocDefaultEdit);
                }

                if (fechaComparar(inputfechaDocumentoEdit.val()) > fechaComparar(inputFechaRecepcionEdit.val())) {
                    inputFechaRecepcionEdit.val(inputfechaDocumentoEdit.val());
                }

                validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
            } else {

                if (!validarformatofechaRecep) {
                    inputFechaRecepcionEdit.val(fechaRecepcionDefaultEdit);
                    if (fechaComparar(inputfechaDocumentoEdit.val()) > fechaComparar(inputFechaRecepcionEdit.val())) {
                        inputFechaRecepcionEdit.val(inputfechaDocumentoEdit.val());
                    }
                    validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
                } else if (!validarformatofechaDoc) {
                    inputfechaDocumentoEdit.val(fechaDocDefaultEdit);
                    validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
                }
                return false;
            }
        };
        var anexoChangeEdit = function (event) {

            var file = inputAnexoEdit[0].files;
            var sumaAnexos = 0;
            if (file == null)
                return;

            for (var x = 0; x < file.length; x++) {
                sumaAnexos = sumaAnexos + inputAnexoEdit[0].files[x].size;
            }

            if (sumaAnexos >= 5 * 1024 * 1024) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("El total máximo permitido debe ser 5MB");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAnexoClick();
                tamanioOK = false;
                return false;
            }


        };

        var fechaRegIniChange = function () {
            
            var fecIni = $("#FechaRegistroDesde").val().split("/");
            var fecFin = $("#FechaRegistroHasta").val().split("/");

            if (fecIni == "") {
                $("#FechaRegistroDesde").val(fechaIniDefault);
            } else {
                fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
                fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
                if (fecIni > fecFin) {

                    $('#ptv-alert-error').modal();
                    $('.p-desc-error').text("La fecha de inicio no puede ser mayor a la fecha fin.");
                    $('#ptv-alert-error').modal('open');
                    $("#FechaRegistroDesde").val($("#FechaRegistroHasta").val());
                }
            }
        }

        var fechaRegFinChange = function () {
            var fecIni = $("#FechaRegistroDesde").val().split("/");
            var fecFin = $("#FechaRegistroHasta").val().split("/");
            if (fecFin == "") {
                $("#FechaRegistroHasta").val(fechaFinDefault);
            } else {
                fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
                fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
                if (fecIni > fecFin) {
                    $('#ptv-alert-error').modal();
                    $('.p-desc-error').text("La fecha fin no puede ser menor a la fecha de inicio.");
                    $('#ptv-alert-error').modal('open');
                    $("#FechaRegistroHasta").val($("#FechaRegistroDesde").val());
                }
            }
        }

        var cboRemitidoPorChange = function () {
            $("#remitidoDescripcionPor").val($("#" + this.id + " option[value='" + this.value + "']").text());
        }
        var btnAnularClick = function (event) {
            event.preventDefault();
            var mensaje = "";

            if ($("#txtAnularComentario").val() == "") {
                mensaje = "Debe ingresar un comentario.";
            }
            if (($("#txtAnularComentario").val().length < 15 || $("#txtAnularComentario").val().length > 200) && mensaje.length == 0) {
                mensaje = "El comentario no debe ser menor que 15 y mayor a 200 caracteres.";
            }
            if (mensaje.length > 0) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text(mensaje);
                $('#ptv-alert-error').modal('open');
                return false;
            }

            var data = new Object();
            data.CodDocumento = $("#hdCodDocumentoAnular").val();
            data.Comentario = $("#txtAnularComentario").val();

            EjecutarAjax("POST", "/Documento/AnularDocumento", data, function () {
                window.location.href = urlBase + "/MesaPartes";
                //window.location.href = window.location.origin + "/MesaPartes";
            }, "El documento se anuló correctamente");

            return false;
        };

        var btnGuardarClick = function (event) {

            if ($('#frmNuevoDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmNuevoDocumento')[0].checkValidity()) {
                return true;
            }
            validarJefeAsignadoAjax("Registrar");

            if (confirm) {
                confirm = false;
                $('.p-desc-confirmacion').text('¿Está seguro de registrar el documento?');
                $('#ptv-alert-confirmacion').modal('open');
            }
        };

        var validarJefeAsignadoAjax = function (tipo) {
            var codAreas = [];
            var stringMsg = "";
            if (tipo == "Registrar") {
                codAreas.push(((hdCodArea.val() + '|').concat(hdCodAreaCC.val())).split('|'));
            }
            if (tipo == "Editar") {
                codAreas.push((($("#hdCodAreaEdit").val() + '|').concat($("#hdCodAreaCCEdit").val())).split('|'));
            }


            for (var i = 0; i < codAreas[0].length; i++) {
                if (codAreas[0][i] != "") {
                    var url = urlBase + "/Trabajador/ListaTrabajadorPorArea";
                    //var url = document.location.origin + "/Trabajador/ListaTrabajadorPorArea";
                    url += "?codArea=" + codAreas[0][i] + "&siEsJefe=" + 1;
                    $.ajax({
                        type: 'GET',
                        enctype: 'multipart/form-data',
                        url: url,
                        async: false,
                        processData: false,
                        contentType: false,
                        cache: false,
                        timeout: 600000,
                        success: function (result) {
                            if (result.length == 0) {
                                var empresa = "";
                                for (var x = 0; x < ResultAreas.length; x++) {
                                   if (((ResultAreas[x].codigoArea.toString()).indexOf(codAreas[0][i])) != -1) {
                                        empresa = ResultAreas[x].descripcionArea;
                                        break;
                                    }
                                }

                                var str = 'El área ' + empresa + ' no tiene jefe asignado</br>';
                                if (stringMsg.length > 0) {
                                    var e = (str.indexOf(stringMsg));
                                    if (e != -1) {
                                        str = "";
                                    }
                                }
                                stringMsg = stringMsg.concat(str);
                            }
                        },
                        complete: function () {
                        },
                        error: function (e, b, c) {
                            console.log(e.responseText);
                        }
                    });
                }
            }
            if (tipo == "Registrar" && stringMsg.length > 0) {
                confirm = false;

                $('.p-desc-error').html(stringMsg);
                $('#ptv-alert-error').modal('open');
            } else if (tipo == "Registrar" && stringMsg.length == 0) {
                validarAdjuntos();
            }

            if (tipo == "Editar" && stringMsg.length > 0) {
                confirmEdit = false;
                $('.p-desc-error').text(stringMsg);
                $('#ptv-alert-error').modal('open');
            } else if (tipo == "Editar" && stringMsg.length == 0) {
                if (_EstadoDocumento != 7) {
                    validarAdjuntosEdit();
                }
            }

        };

        var registrarDocumentoAjax = function () {
            // Get form
            var form = $('#frmNuevoDocumento')[0];
            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelar").prop("disabled", true);
            $("#modal_loading_agro").attr("style", "display:block");
            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: urlBase + "/MesaPartes/RegistrarDocumento",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
              //  timeout: 600000,
                success: function (result) {

                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                                window.location.href = urlBase + "/MesaPartes";
                                //window.location.href = window.location.origin + "/MesaPartes";
                            }
                        });
                        if (result.ResultadoCorreo > 0) {
                            $('.p-descripcion').text("El documento se grabó correctamente");
                        } else if (result.ResultadoCorreo <= 0 && inputAdjunto.get(0).files.length > 0) {
                            $('.p-descripcion').text("No se pudo enviar el correo, pero el documento se grabó correctamente");
                        } else {
                            $('.p-descripcion').text("El documento se grabó correctamente");
                        }
                        
                        $('.p-otradescripcion').text("N° Trámite " + result.correlativo );
                        $('#ptv-alert-ok').modal('open');
                        $(this).prop("disabled", false);
                    }

                    if (result.Resultado !== 1) {
                        $('#ptv-alert-error').modal({
                            onCloseEnd: function () {
                                window.location.href = urlBase + "/MesaPartes";
                            }
                        });
                        $('.p-desc-error').text("Hubo problemas en el registro.");
                        $('#ptv-alert-error').modal('open');
                        $(this).prop("disabled", false);
                    }

                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                },
                error: function (e, b, c) {
                    //$('#ptv-alert-error').modal({
                    //    onCloseEnd: function () {
                    //        window.location.href = window.location.origin + "/MesaPartes";
                    //    }
                    //});
                    $('.p-desc-error').text("Hubo problemas en la aplicación.");
                    $('#ptv-alert-error').modal('open');
                    $(this).prop("disabled", false);
                    console.log(e.responseText);
                }
            });
        };

        var editarDocumentoAjax = function () {
            $("#modal_loading_agro").attr("style", "display:block");
            // Get form
            var form = $('#frmEditDocumento')[0];

            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelarEdit").prop("disabled", true);


            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: urlBase + "/MesaPartes/EditarDocumento",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (result) {

                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                                window.location.href = urlBase + "/MesaPartes";
                            }
                        });
                        $('.p-descripcion').text("El documento se actualizó correctamente");
                        $('#ptv-alert-ok').modal('open');
                    }

                    if (result.Resultado != 1) {
                        $('#ptv-alert-error').modal({
                            onCloseEnd: function () {
                                window.location.href = urlBase + "/MesaPartes";
                            }
                        });
                        $('.p-desc-error').text("Hubo problemas en el registro.");
                        $('#ptv-alert-error').modal('open');
                    }

                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                },
                error: function (e, b, c) {
                    $('#ptv-alert-error').modal({
                        onCloseEnd: function () {
                            window.location.href = urlBase + "/MesaPartes";
                        }
                    });
                    $('.p-desc-error').text("Hubo problemas en la aplicación.");
                    $('#ptv-alert-error').modal('open');
                    console.log(e.responseText);
                }
            });
        }

        var validarAdjuntosEdit = function () {
            var strValidarAdjuntos = "";
            if (inputAnexoEdit.get(0).files.length == 0
                && (inputAdjuntoEdit.get(0).files.length) == 0 && $("#AnexosIngresados")[0].children.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto y anexos?'
                confirmEdit = false;
            } else if (inputAnexoEdit.get(0).files.length == 0 && $("#AnexosIngresados")[0].children.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin anexos?'
                confirmEdit = false;
            } else if (inputAdjuntoEdit.get(0).files.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto?'
                confirmEdit = false;
            } else {
                confirmEdit = true;
            }
            if (confirmEdit == false) {
                $('.p-desc-confirmacion').text(strValidarAdjuntos);
                $('#ptv-alert-confirmacion').modal('open');
            }
        }

        var validarAdjuntos = function () {
            var strValidarAdjuntos = "";
            if (inputAnexo.get(0).files.length == 0
                && (inputAdjunto.get(0).files.length) == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto y anexos?'
                confirm = false;
            } else if (inputAnexo.get(0).files.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin anexos?'
                confirm = false;
            } else if (inputAdjunto.get(0).files.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto?'
                confirm = false;
            } else {
                confirm = true;
            }
            if (confirm == false) {
                $('.p-desc-confirmacion').text(strValidarAdjuntos);
                $('#ptv-alert-confirmacion').modal('open');
            }
        }

        var btnEditClick = function (event) {

            if ($('#frmEditDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmEditDocumento')[0].checkValidity()) {
                return true;
            }

            validarJefeAsignadoAjax("Editar");
            if (_EstadoDocumento == 7) {
                validarDevolucionAreaTO();
            }


            if (confirmEdit == false) {
                return false;
            }

            if (confirmEdit) {
                confirmEdit = false;
                $('.p-desc-confirmacion').text('¿Está seguro de actualizar el documento?');
                $('#ptv-alert-confirmacion').modal('open');
            }
        };

        var btnCancelarEditClick = function () {
            $('#ptv-DocumentoMesaParteseEdit').modal('close');
            return false;
        }

        var validarDevolucionAreaCC = function () {
            var areasCC = areasCCDevolucion.toString().split("|").join("");
            var hdareasCC = $("#hdCodAreaCCEdit").val().split("|").join("");
            if (hdareasCC.length > 0) {
                var validacion = areasCC.indexOf(hdareasCC);
                if (validacion == -1) {
                    confirmEdit = true;
                } else {
                    confirmEdit = false;
                    $('.p-desc-error').text("El área Con Copia no puede ser el mismo, por favor seleccione un área distinta");
                    $('#ptv-alert-error').modal('open');
                }
            }
        }

        var validarDevolucionAreaTO = function () {

            var validacion = areasTODevolucion.toString().indexOf($("#hdCodAreaEdit").val());
            if (validacion == -1) {
                confirmEdit = true;
            } else {
                confirmEdit = false;
                $('.p-desc-error').text("Debe asignar un área distinta al que previamente se asignó");
                $('#ptv-alert-error').modal('open');
            }

        }


        var btnCancelarClick = function () {
            $('#modal-nuevo').modal('close');
            return false;
        }

        var btnAgreeClick = function () {
            if (!confirm) {
                registrarDocumentoAjax();
                confirm = true;
            }
            if (!confirmEdit) {
                editarDocumentoAjax();
                confirmEdit = true;
            }
        }

        ////////////////////
        $(document).on("click", "#idVerDocumento", function () {
            var _codDocumento = $(this).data('id');
          
            //var url = document.location.origin + "/MesaPartes/VerDocumento";
            var url = urlBase + "/MesaPartes/VerDocumento";


            cargarPagina(url, "#ptv-DocumentoMesaPartes", function () {

                //var url = document.location.origin + "/Documento/ObtenerDocumento";
                var url = urlBase + "/Documento/ObtenerDocumento";
                url += "?codDocumento=" + _codDocumento;

                $("#modal_loading_agro").attr("style", "display:block");
                $("#tbDetalle").attr("style", "display:none");
                $("#hdCodDocumentoAnular").val(_codDocumento);

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,

                    success: function (result) {
                        var date = new Date(result.data.FechaPlazo);
                        var _dia = '00' + date.getDate().toString();
                        _dia = _dia.substring(_dia.length - 2, _dia.length);
                        var _mes = '00' + (date.getMonth() + 1).toString();
                        _mes = _mes.substring(_mes.length - 2, _mes.length);

                        var fPlazo = _dia + "/" + _mes + "/" + date.getFullYear();

                        $("#mNumDocumento").text(result.data.NumDocumento);
                        $("#mCorrelativo").html("Nro.Trámite : " + result.data.Correlativo);
                        $("#mNombreTrab").html(result.data.DescripcionEmpresa + " - " + result.data.Representante);
                        $("#mOrigen").html(result.data.Origen === 1 ? "INTERNO" : "EXTERNO");
                        $("#mAnio").html(result.data.Anio);
                        $("#mFechaPlazo").html(fPlazo + ' (' + result.data.Plazo + ' días)');



                        $("#mEstadoDocumento").html(fc_ObtenerEstadoDocumentoHtml(result.data.EstadoDocumento, true));

                        var _stPri_Baja = '<span class="new badge icon-badge badge-blue badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">arrow_downward</i></span> <span class="c-blue prioridad-texto-detalle"><label class="label color-blue">Prioridad baja</label></span>';
                        var _stPri_Media = '<span class="new badge icon-badge badge-green badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">remove</i> </span> <span class="c-green prioridad-texto-detalle"><label class="label color-green">Prioridad normal</label></span>';
                        var _stPri_Alta = '<span class="new badge icon-badge badge-yellow badge-small" data-badge-caption=""><i class="material-icons icon-detalle">arrow_upward</i></span> <span class="c-yellow prioridad-texto-detalle"><label class="label color-yellow">Prioridad urgente</label></span>';
                        var _stPri_Urgente = '<span class="new badge icon-badge badge-pink badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">priority_high</i></span> <span class="c-pink prioridad-texto-detalle"><label class="label color-pink">Prioridad muy urgente</label></span>';
                        var _stPrioridad = _stPri_Baja;

                        switch (result.data.Prioridad) {
                            case 1: //baja
                                _stPrioridad = _stPri_Baja;
                                break;
                            case 2://media
                                _stPrioridad = _stPri_Media;
                                break;
                            case 3://alta
                                _stPrioridad = _stPri_Alta;
                                break;
                            case 4://urgente
                                _stPrioridad = _stPri_Urgente;
                                break;
                        }
                        $("#mPrioridad").html(_stPrioridad);
                        console.log(result.data);
                        $("#mFechaRecepcion").html(formatfecha(result.data.FechaRecepcion));
                        $("#mRepresentante").html(result.data.UsuarioRegistro);//result.data.NombreTrab + ' ' + result.data.ApePaternoTrab + ' ' + result.data.ApeMaternoTrab);
                        $("#DestinatarioTO").html('<div class="col s10 m7">' + result.data.Area + ' - ' + result.data.NombreTrab + ' ' + result.data.ApePaternoTrab + ' ' + result.data.ApeMaternoTrab+'</div><div class="col s2 m2"><span class="new badge badge-green" data-badge-caption="TO"></span></div>');
                        $("#DestinatarioCC").html(fc_ObtenerDestinatariosCC(result.data.LstDocDerivacion));
                        $("#mAsunto").html(result.data.Asunto);
                        $("#mReferencia").html(result.data.Referencia === null ? "" : result.data.Referencia);
                        $("#mFechaDocumento").html(formatfecha(result.data.FechaDocumento));
                        $("#mObservaciones").html(result.data.Observaciones === null ? "" : result.data.Observaciones);

                        var laserFicher = result.data.ArchivosLaserFiche;
                        var htmlLaserFicher = '', htmlLaserFicherAnexo = '', htmlLaserFicherAnexoAdjunto = '';
                        var htmlBtZIP = '';

                        var iniAnexo = 0;
                        var htmlBtn = "";
                        var cc = 0;
                        if (result.data.LstDocAdjunto.length > 0) {
                            if (laserFicher !== null) {
                                cc = 0;
                                for (var i = 0; i < 1; i++) {
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtn = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicher = htmlLaserFicher + htmlBtn;
                                        cc++;
                                    }

                                }
                                iniAnexo = 1;
                            }
                        }
                        if (laserFicher != null) {
                            for (var i = iniAnexo; i < laserFicher.ListaDocumentos.length; i++) {
                                var cc = 0;
                                if (laserFicher.ListaDocumentos[i].TipoArchivo === 2) {
                                    /*CAMBIAR POR ANEXOS - ADJUNTOS*/
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtnAnexo = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicherAnexoAdjunto = htmlLaserFicherAnexoAdjunto + htmlBtnAnexo;
                                        cc++;
                                    }
                                    if (cc > 1) {
                                        htmlBtZIP = '<a class="a-link" href=' + "'" + urlBase + "/BandejaEntrada/ExportarArchivosZIP?codDocumento=" + _codDocumento + "'" + ';">' +
                                            '<i class="material-icons prefix">description</i>' +
                                            '<span>Descargar Todo</span></a>';

                                    }
                                }

                                if (laserFicher.ListaDocumentos[i].TipoArchivo === 1)
                                { 
                                    /*CAMBIAR POR ANEXOS*/
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtnAnexo = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicherAnexo = htmlLaserFicherAnexo + htmlBtnAnexo;
                                        cc++;
                                    }
                                    if (cc > 1) {
                                        htmlBtZIP = '<a class="a-link" href=' + "'/BandejaEntrada/ExportarArchivosZIP?codDocumento=" + _codDocumento + "'" + ';">' +
                                            '<i class="material-icons prefix">description</i>' +
                                            '<span>Descargar Todo</span></a>';

                                    }
                                }
                            }
                        }

                        $("#mDescargarTodos").html(htmlBtZIP);
                        $("#mArchivosLaserFiche").html(htmlLaserFicher);
                        $("#mArchivosLaserFicheAnexos").html(htmlLaserFicherAnexo);
                        $("#mArchivosLaserFicheAnexosAdjuntos").html(htmlLaserFicherAnexoAdjunto);
                        var htmBtmAnular = "";
                        var htmlBtnPDFCargoRecep = "";

                        if (result.data.EstadoDocumento == 1)//igual a ingresado a anula
                        {
                            htmBtmAnular = '<button data-target="modal-confirm-anular" class="modal-trigger btn waves-effect waves-light btn-warning modal-action modal-close" >' +
                                '<span>Anular</span>' +
                                '<i class="material-icons prefix">delete</i>' +
                                '</button>  ';
                        }

                        if (result.data.EstadoDocumento != 1)
                        {
                            htmlBtnPDFCargoRecep = '<button class="btn waves-effect waves-light btn-warning"  type="submit" name="action" onclick= "location.href=' + "'" + urlBase + "/Documento/ExportarPDF_CargoRecepcion?codDocumento=" + _codDocumento + "'" + ';" name="action">' +
                                '<span>Hoja de Cargo  </span>' +
                                '<i class="material-icons prefix">description</i>' +
                                '</button>  ';
                        }

                        

                        var htmlBtnPDFHojaResumen = '<button class="btn waves-effect waves-light btn-warning" type="submit" name="action" onclick= "location.href=' + "'" + urlBase + "/Documento/ExportarPDF_HojaResumen?codDocumento=" + _codDocumento + "'" + ';" name="action">' +
                            '<span>Hoja de Resumen</span>' +
                            '<i class="material-icons prefix">navigation</i>' +
                            '</button>   ';

                        var htmlBtns = htmBtmAnular + htmlBtnPDFCargoRecep + htmlBtnPDFHojaResumen;

                        $("#mSeccionExportarPDF").html(htmlBtns);

                    },
                    complete: function () {
                        $('#modal_loading_agro').attr("style", "display:none");
                        $("#tbDetalle").attr("style", "display:block");

                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });


               /*var urlPestaniaSeguimiento = document.location.origin + "/Documento/GetSeguimientoDocumento";
                urlPestaniaSeguimiento += "?codDocumento=" + _codDocumento;
                $.ajax({
                    url: urlPestaniaSeguimiento,
                    //dataType: "json",
                    type: "GET",
                    //contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {
                        $("#tbSeguimiento").html(result);
                        $("#ptv-DocumentoMesaPartes").find('.collapsible').collapsible();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(ajaxOptions + ":" + thrownError);
                    }
                });*/
                //var urlPestaniaSeguimiento = document.location.origin + "/Documento/GetSeguimiento";
                var urlPestaniaSeguimiento = urlBase + "/Documento/GetSeguimiento";
                urlPestaniaSeguimiento += "?codDocumento=" + _codDocumento +"&origen="+2;
                $.ajax({
                    url: urlPestaniaSeguimiento,
                    //dataType: "json",
                    type: "GET",
                    //contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {
                        $("#tbSeguimientoCab").html(result);
                       $("#ptv-DocumentoMesaPartes").find('.collapsible').collapsible();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(ajaxOptions + ":" + thrownError);
                    }
                })

                var elem = $('.tabs');
                elem.tabs();
                var instance_tabs = M.Tabs.getInstance(elem);
                instance_tabs.select("tbDetalle");
            });
        });


        $(document).on("click", "#idDocumentoEdit", function () {
            var _codDocumento = $(this).data('id');
            _EstadoDocumento = $(this).data('estado');
            //si el documento tiene esta como devuelto
            if (_EstadoDocumento == 7) {
                $(".deshabilitarDocumentoDevuelto").addClass("disabled");
                $("#btnLimpiarAdjuntoEdit").unbind();
                $("#btnLimpiarAnexoEdit").unbind();
                $("#divcomentarios").show();
            } else {
                $(".deshabilitarDocumentoDevuelto").removeClass("disabled");
                $("#btnLimpiarAdjuntoEdit").bind();
                $("#btnLimpiarAnexoEdit").bind();
                $("#divcomentarios").hide();
            }


            var _codRepresentante = "";
            var _to = "";
            var _CC = [];
            var url = urlBase + "/Documento/ObtenerDocumento";
            //var url = document.location.origin + "/Documento/ObtenerDocumento";
            var arrAnexosEliminados = [];
            var _remitenteEdit = "";
            var cboRemitidoPorEdit = $("#remitidoPorEdit");

            url += "?codDocumento=" + _codDocumento;
            $("#modal_loading_agro").attr("style", "display:block");
            $("#tbDetalle").attr("style", "display:none");

            $.ajax({
                url: url,
                dataType: "json",
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    var date = new Date(result.data.FechaPlazo);
                    var _dia = '00' + date.getDate().toString();
                    _dia = _dia.substring(_dia.length - 2, _dia.length);
                    var _mes = '00' + (date.getMonth() + 1).toString();
                    _mes = _mes.substring(_mes.length - 2, _mes.length);
                    var fPlazo = _dia + "/" + _mes + "/" + date.getFullYear();
                    var _codigosCC = "";


                    $("#CorrelativoEdit").val(result.data.Correlativo);
                    $("#hdCodDocumentoEdit").val(_codDocumento);

                    $("#NumDocumentoEdit").val(result.data.NumDocumento);

                    $("#fechaDocumentoEdit").val(formatfecha(result.data.FechaDocumento));
                    fechaDocDefaultEdit = formatfecha(result.data.FechaDocumento);
                    
                    $("#FechaRecepcionEdit").val(formatfecha(result.data.FechaRecepcion));
                    fechaRecepcionDefaultEdit = formatfecha(result.data.FechaRecepcion);

                    $("#plazoEdit").val(result.data.Plazo);

                    $("#fechaPlazoEdit").val(formatfecha(result.data.FechaPlazo));

                    $("#prioridadEdit").val(result.data.Prioridad);
                    $('#prioridadEdit').formSelect();

                    $("#codTipoDocumentoEdit").val(result.data.CodTipoDocumento);
                    $("#codTipoDocumentoEdit").formSelect();

                    $("#asuntoEdit").val(result.data.Asunto);
                    $("#referenciaEdit").val(result.data.Referencia);
                    $("#observacionesEdit").val(result.data.Observaciones);
                    $(".adjuntarArchivoAuxEdit").val("");
                    $(".lblactive").addClass("active");

                    $("#hdCodRemitenteEdit").val(result.data.CodEmpresa);
                    $("#hdCodAreaEdit").val(result.data.CodArea);
                    areasTODevolucion = result.data.CodArea;
                    $("#estadoDocumentoEdit").attr("data-badge-caption", "INGRESADO");
                    _remitenteEdit = result.data.DescripcionEmpresa;
                    _to = result.data.Area;
                    _codRepresentante = result.data.CodRepresentante;
                    obtenerTrabajadoresPorEmpresaEdit(bindTrabajadoresComboEditCarga, result.data.CodEmpresa);

                    if (result.data.LstDocDerivacion != null) {
                        var codigosCC = "";
                        _codigosCC = result.data.LstDocDerivacion;
                        for (var x = 0; x < _codigosCC.length; x++) {
                            codigosCC = (codigosCC + ((codigosCC.length > 0) ? '|' : "") + result.data.LstDocDerivacion[x].CodArea).trim();
                        }
                    }
                    $("#hdCodAreaCCEdit").val(codigosCC);
                    areasCCDevolucion = codigosCC;

                    cargarRemitenteEdit();
                    cargarToCCEdit(_codigosCC);

                    var laserFicher = result.data.ArchivosLaserFiche;
                    var iniAnexo = 0;
                    if (result.data.LstDocAdjunto.length > 0) {
                        $("#hdDocumentoAdjunto").val((result.data.LstDocAdjunto[0].CodDocAdjunto));
                        if (result.data.EstadoDocumento ==7) {
                            $("#estadoDocumentoEdit").attr("data-badge-caption", "DEVUELTO");
                            $("#iEstadoDocumentoEdit").val("7");
                        } else {
                            $("#estadoDocumentoEdit").attr("data-badge-caption", "PENDIENTE");
                        }
                        $(".adjuntarArchivoAuxEdit").val(result.data.ArchivosLaserFiche.ListaDocumentos[0].NombreArchivo);
                        iniAnexo = 1;
                    }
                    if (laserFicher != null) {
                        var btnAnexosIngresados = "";
                        var htmlAnexosIngresados = "";
                        for (var i = iniAnexo; i < laserFicher.ListaDocumentos.length; i++) {


                            btnAnexosIngresados = '<button class="waves-effect waves-light btnAnexoIngresado btn-warv2" style="cursor: pointer;" id=' + laserFicher.ListaDocumentos[i].IdDocumento + ' >' +
                                '	<i class="material-icons right">close</i>' +
                                '	<span>' + laserFicher.ListaDocumentos[i].NombreArchivo + '</span>' +
                                '</button>&nbsp';
                            htmlAnexosIngresados = htmlAnexosIngresados + btnAnexosIngresados;
                        }
                        $(".AnexosIngresados").html(htmlAnexosIngresados);
                    }
                    validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                    $("#tbDetalle").attr("style", "display:block");
                    $(".btnAnexoIngresado").click(function () {
                        $(this).remove();
                        var id = $(this).attr('id');
                        arrAnexosEliminados.push(id);
                        $("#hdAnexosElimiados").val(arrAnexosEliminados);
                    });

                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            $("#remitidoPorEdit").change(function () {
                $("#remitidoDescripcionPorEdit").val($("#" + this.id + " option[value='" + this.value + "']").text());
            });

            var cargarToCCEdit = function (_codigosCC) {
                obtenerAreas(function (data) {
                    ResultAreas = [];
                    var CCExist = [];
                    var result = {};
                    for (var i = 0; i < data.length; i++) {
                        var area = {
                            codigoArea: data[i].CodArea,
                            descripcionArea: data[i].Descripcion
                        }
                        result[(data[i].Descripcion).toString()] = null;
                        ResultAreas.push(area);
                    }

                    if (_codigosCC != null) {
                        for (var x = 0; x < _codigosCC.length; x++) {
                            for (var y = 0; y < ResultAreas.length; y++) {
                                if (((_codigosCC[x].CodArea.toString()).indexOf(ResultAreas[y].codigoArea.toString())) != -1) {
                                    var obj = { tag: ResultAreas[y].descripcionArea };
                                    CCExist.push(obj)
                                }
                            }
                        }
                    }

                    $('#chipTOEdit').chips({
                        onChipAdd: onChipAddEdit,
                        onChipDelete: onChipAddEdit,
                        autocompleteOptions: {
                            data: result,
                            limit: Infinity,
                            minLength: 1
                        },
                        data: [{
                            tag: _to,
                        }]
                    });

                    $('#chipCCEdit').chips({
                        onChipAdd: onChipAddCCEdit,
                        onChipDelete: onChipAddCCEdit, 
                        autocompleteOptions: {
                            data: result,
                            limit: Infinity,
                            minLength: 1
                        },
                        data: CCExist
                    });


                    $(".chips-autocomplete.chips-areasEdit input").on("blur", blurChip);
                    $(".chips-autocomplete.chips-areasCCEdit input").on("blur", blurChip);
                });
            }

            var cargarRemitenteEdit = function () {
                obtenerEmpresas(function (data) {
                    ResultRemitente = [];
                    var result = {};
                    for (var i = 0; i < data.length; i++) {
                        var remitente = {
                            codigoRemitente: data[i].CodEmpresa,
                            descripcionRemitente: data[i].Descripcion
                        }
                        result[(data[i].Descripcion).toString()] = null;
                        ResultRemitente.push(remitente);
                    }
                    $("#hdDescripcionRemitenteEdit").val(_remitenteEdit);
                    $('#chipRemitenteEdit').chips({
                        onChipAdd: onChipAddRemitenteEdit,
                        onChipDelete: onChipAddRemitenteEdit,
                        autocompleteOptions: {
                            data: result,
                            limit: Infinity,
                            minLength: 1
                        },
                        data: [{
                            tag: _remitenteEdit,
                        }]
                    });

                    if (_EstadoDocumento == 7) {
                        $('#chipRemitenteEdit input').prop("disabled", true);
                        $('#chipRemitenteEdit').find(".chip").find("i").remove();
                    }

                    $(".chips-autocomplete.chips-remitenteEdit input").prop("required", false);
                    $(".chips-autocomplete.chips-remitenteEdit input").on("blur", blurChip);
                });
            };

            var obtenerEmpresas = function (callback) {

                $.ajax({
                    url: urlBase + "/Maestros/ObtenerEmpresas",
                    //url: document.location.origin + "/Maestros/ObtenerEmpresas",
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado == 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };

            var obtenerAreas = function (callback) {
                $.ajax({
                    url: urlBase + "/Maestros/ObtenerAreas",
                    //url: document.location.origin + "/Maestros/ObtenerAreas",
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado === 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };

            var onChipAddRemitenteEdit = function (e) {
                $("#hdDescripcionRemitenteEdit").val();
                limpiarCboRemitidoPor();
                if (validarInputChips('.chips-remitenteEdit') == 0) {
                }

                $("#chipRemitenteEdit").find('input').val("");
                _codRepresentante = "";
                if ($("#chipRemitenteEdit").find("div").length > 1) {
                    $("#chipRemitenteEdit").find("div")[1].remove();
                } else {

                    var value = $("#chipRemitenteEdit").find(".chip").first().text();
                    var str = value.split("|");
                    var codigo = str[0];
                    var descripcion = codigo.replace("close", "").trim();

                    if (descripcion != "") {
                        for (var x = 0; x < ResultRemitente.length; x++) {
                            var valor = (ResultRemitente[x].descripcionRemitente).indexOf(descripcion);
                            if (valor != -1) {
                                codigo = ResultRemitente[x].codigoRemitente.toString();
                                $("#hdDescripcionRemitenteEdit").val(descripcion);
                                break;
                            } else {
                                codigo = "";
                            }
                        }
                    }


                    if (codigo.toString().length > 0) {
                        obtenerTrabajadoresPorEmpresa(bindTrabajadoresComboEditCarga, codigo);
                        $(".chips-autocomplete.chips-remitenteEdit input").prop("required", false);
                    }

                    if (codigo.toString().length == 0) {
                        limpiarCboRemitidoPor();
                        $(".chips-autocomplete.chips-remitenteEdit input").prop("required", true);
                    }

                    $("#hdCodRemitenteEdit").val(codigo);
                }
            };

            var onChipAddEdit = function (e) {

                if (validarInputChips('.chips-areasEdit') == 0) {
                }

                validarInputChipsCCTO('.chips-areasEdit', '.chips-areasCCEdit');

                $("#chipTOEdit").find('input').val("");
                $("#chipCCEdit").find('input').val("");

                if ($("#chipTOEdit").find("div").length > 1) {
                    $("#chipTOEdit").find("div")[1].remove();
                } else {

                    var value = $("#chipTOEdit").find(".chip").first().text();
                    var str = value.split("|");
                    var codigo = str[0];
                    var descripcion = codigo.replace("close", "").trim();

                    if (descripcion !== "") {
                        for (var x = 0; x < ResultRemitente.length; x++) {
                            var valor = (ResultAreas[x].descripcionArea).indexOf(descripcion);
                            if (valor !== -1) {
                                codigo = ResultAreas[x].codigoArea.toString();
                                break;
                            } else {
                                codigo = "";
                            }
                        }
                    }

                    if (codigo.toString().length > 0) {
                        $(".chips-autocomplete.chips-areasEdit input").prop("required", false);
                    }

                    if (codigo.toString().length == 0) {
                        $(".chips-autocomplete.chips-areasEdit input").prop("required", true);
                    }

                    $("#hdCodAreaEdit").val(codigo);
                }
            };

            var onChipAddCCEdit = function (e) {

                $("#hdCodAreaCCEdit").val("");
                if (validarInputChips('.chips-areasCCEdit') == 0) {
                    return false;
                }
                validarInputChipsTOCC('.chips-areasEdit', '.chips-areasCCEdit');

                var chipsCC = M.Chips.getInstance($('.chips-areasCCEdit')).chipsData;
                var codigosCC = "";
                if (chipsCC.length > 0) {
                    var chipsCCtag = M.Chips.getInstance($('.chips-areasCCEdit')).chipsData;
                    for (var x = 0; x < chipsCC.length; x++) {
                        for (var y = 0; y < ResultAreas.length; y++) {
                            var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                            if (valor != -1) {
                                codigo = ResultAreas[y].codigoArea.toString();
                                codigosCC = codigosCC + ((codigosCC.length > 0) ? '|' : "") + codigo;
                            }
                        }
                    }
                }
                $("#hdCodAreaCCEdit").val(codigosCC);
                if (codigosCC.toString().length > 0) {
                    $(".chips-autocomplete.chips-areasCCEdit input").prop("required", false);
                }

                if (codigosCC.toString().length == 0) {
                    $(".chips-autocomplete.chips-areasCCEdit input").prop("required", true);
                }
            };

            var blurChip = function () {

                $("#chipRemitenteEdit").find("input").val("");
                $("#chipTOEdit").find('input').val("");

            }

            var limpiarCboRemitidoPor = function () {
                cboRemitidoPorEdit.empty();
                cboRemitidoPorEdit.append("<option value=''>Seleccione</option>");
                cboRemitidoPorEdit.formSelect();
            }

            var obtenerTrabajadoresPorEmpresa = function (callback, idEmpresa) {

                var url = urlBase + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                //var url = document.location.origin + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                url += "?codEmpresa=" + idEmpresa;

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado == 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };

            var bindTrabajadoresComboEditCarga = function (trabajadores) {
                cboRemitidoPorEdit.empty();
                cboRemitidoPorEdit.append("<option value=''>Seleccione</option>");
                $.each(trabajadores, function (ind, val) {
                    cboRemitidoPorEdit.append("<option value='" + val.CodRepresentante + "'>" + val.Nombre + "</option>");
                });
                cboRemitidoPorEdit.formSelect();

                if (_codRepresentante != "") {
                    cboRemitidoPorEdit.val(_codRepresentante);
                    $("#remitidoDescripcionPorEdit").val($("#remitidoPorEdit option[value='" + _codRepresentante + "']").text());
                    cboRemitidoPorEdit.formSelect();
                }
            }

            var obtenerTrabajadoresPorEmpresaEdit = function (callback, idEmpresa) {
                var url = urlBase + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                //var url = document.location.origin + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                url += "?codEmpresa=" + idEmpresa;

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado == 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };
        });


        /////////////////////

        return {
            init: init
        };

    })();

    mesaPartes.init();
});