﻿var lpad = function (str, padString, length) {
    //var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};

function getDiaHoyStr() {
    var fechaHoy = new Date();
    var currentMonth = fechaHoy.getMonth() + 1;
    var currentDay = fechaHoy.getDate();
    var strFecha = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaHoy.getFullYear();
    return strFecha;
}
var reg = 0;
function RegularInputsMaterial() {

    $("input.datepicker").each(function (x) { if ($(this).val().length > 0 && $(this).next().hasClass("active") === false) { $(this).next().addClass("active"); } });

    $("input").each(function (x) { if ($(this).val().length > 0 && $(this).prev().hasClass("active") === false && $(this).prev().attr("for") !== undefined) { $(this).prev().addClass("active"); } });

}

function getAgregarDiasHoyStr(diasAdd) {
    var fechaHoy = new Date();
    var newdate = new Date(fechaHoy);
    newdate.setDate(fechaHoy.getDate() + diasAdd);
    var dd = newdate.getDate().toString();
    dd = dd.length > 1 ? dd : '0' + day;
    var mm = (newdate.getMonth() + 1).toString();
    mm = mm.length > 1 ? mm : '0' + mm;
    var y = newdate.getFullYear();
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}

function validarMinMax2Fechas(objFechaMin, objFechaMax) {
    var elems = $(objFechaMin);
    var instances = M.Datepicker.getInstance(elems);
    instances.options.minDate = stringToDate($(objFechaMax).val(), 'dd/MM/yyyy', '/');
    elems = $(objFechaMax);
    instances = M.Datepicker.getInstance(elems);
    instances.options.maxDate = stringToDate($(objFechaMin).val(), 'dd/MM/yyyy', '/');
}

function formatfecha(data) {
    var dateRecep = new Date(data);
    var _diaRecep = '00' + dateRecep.getDate().toString();
    _diaRecep = _diaRecep.substring(_diaRecep.length - 2, _diaRecep.length);
    var _mesRecep = '00' + (dateRecep.getMonth() + 1).toString();
    _mesRecep = _mesRecep.substring(_mesRecep.length - 2, _mesRecep.length);

    var fRecep = _diaRecep + "/" + _mesRecep + "/" + dateRecep.getFullYear();
    return fRecep;
}
function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    return formatedDate;
}
function isNull(val, valnull) {
    if (valnull === undefined) {
        if (val === null || val === undefined || val === "") {
            return true;
        }
        return false;
    }
    else {
        if (val === null || val === undefined || val === "") {
            return valnull;
        }
    }
    return val;
}

RegularInputsMaterial();
setTimeout(RegularInputsMaterial, 1000);
setTimeout(RegularInputsMaterial, 2000);
setTimeout(RegularInputsMaterial, 3000);
setTimeout(RegularInputsMaterial, 5000);

function EjecutarAjaxV2(tipo, url, data, function_ok, mensaje) {
    var urlBase = $("#hdUrlBase").val();
    $("#modal_loading_agro").attr("style", "display:block");
    $(this).prop("disabled", true);
    $("#btnCancelar").prop("disabled", true);


    $.ajax({
        type: tipo,//'POST',
        //enctype: 'multipart/form-data',
        dataType: "json",
        contentType: "application/json",
        url: urlBase + url,//"/BandejaEntrada/RegistrarDocumento",
        data: JSON.stringify(data),
        //processData: false,
        headers: { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val() },
        cache: false,
        timeout: 60000,
        success: function (result) {

            if (result.Resultado === 1) {
                $('#ptv-alert-ok').modal({
                    onCloseEnd: function () {
                        function_ok();
                    }
                });
                if (mensaje.length > 0) {
                    $('.p-descripcion').text(mensaje);
                };
                $("#btnCancelar").prop("disabled", false);
                $('#ptv-alert-ok').modal('open');
               
            }

            if (result.Resultado != 1) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $("#btnCancelar").prop("disabled", false);
                $('.p-desc-error').text(result.Error);
                $('#ptv-alert-error').modal('open');
               
            }

        },
        complete: function () {
            $('#modal_loading_agro').attr("style", "display:none");
        },
        error: function (e, b, c) {
            $('#ptv-alert-error').modal({
                onCloseEnd: function () {
                }
            });
            $('.p-desc-error').text("Hubo problemas en la aplicación.");
            $('#ptv-alert-error').modal('open');
            console.log(e.responseText);
        }
    });

}


function EjecutarAjax(tipo, url, data, function_ok, mensaje) {
    $("#modal_loading_agro").attr("style", "display:block");
    $(this).prop("disabled", true);
    $("#btnCancelar").prop("disabled", true);
    var urlBase = $("#hdUrlBase").val();

    $.ajax({
        type: tipo,//'POST',
        //enctype: 'multipart/form-data',
        dataType: "json",
        contentType: "application/json",
        url: urlBase+url,//"/BandejaEntrada/RegistrarDocumento",
        //url: url,//"/BandejaEntrada/RegistrarDocumento",
        data: JSON.stringify(data),
        //processData: false,
        headers: { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val() },
        cache: false,
        timeout: 120000,
        success: function (result) {

            if (result.Resultado === 1) {
                $('#ptv-alert-ok').modal({
                    onCloseEnd: function () {
                        function_ok();
                    }
                });
                if (mensaje.length > 0) {
                    $('.p-descripcion').text(mensaje);
                }
                $('#ptv-alert-ok').modal('open');
            }

            if (result.Resultado != 1) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                        location.reload();
                    }
                });
                $('.p-desc-error').text("Hubo problemas en el registro.");
                $('#ptv-alert-error').modal('open');
            }

        },
        complete: function () {
            $('#modal_loading_agro').attr("style", "display:none");
        },
        error: function (e, b, c) {
            $('#ptv-alert-error').modal({
                onCloseEnd: function () {
                    location.reload();
                }
            });
            $('.p-desc-error').text("Hubo problemas en la aplicación.");
            $('#ptv-alert-error').modal('open');
            $("#btnCancelar").prop("disabled", false);
            console.log(e.responseText);
        }
    });

}


function EjecutarAjaxRespuestaCorreo(tipo, url, data, function_ok, mensaje) {
    $("#modal_loading_agro").attr("style", "display:block");
    $(this).prop("disabled", true);
    $("#btnCancelar").prop("disabled", true);
    var urlBase = $("#hdUrlBase").val();

    $.ajax({
        type: tipo,//'POST',
        //enctype: 'multipart/form-data',
        dataType: "json",
        contentType: "application/json",
        url: urlBase + url,//"/BandejaEntrada/RegistrarDocumento",
        //url: url,//"/BandejaEntrada/RegistrarDocumento",
        data: JSON.stringify(data),
        //processData: false,
        headers: { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val() },
        cache: false,
        timeout: 120000,
        success: function (result) {

            if (result.Resultado === 1) {
                $('#ptv-alert-ok').modal({
                    onCloseEnd: function () {
                        function_ok();
                    }
                });
                if (mensaje.length > 0 && result.ResultadoCorreo > 0) {
                    $('.p-descripcion').text(mensaje);
                } else if (mensaje.length > 0 && result.ResultadoCorreo <= 0) {
                    $('.p-descripcion').text("No se envió el correo, pero " + mensaje.toLowerCase());
                }
                $('#ptv-alert-ok').modal('open');
            }

            if (result.Resultado != 1) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text("Hubo problemas en el registro.");
                $('#ptv-alert-error').modal('open');
            }

        },
        complete: function () {
            $('#modal_loading_agro').attr("style", "display:none");
        },
        error: function (e, b, c) {
            $('#ptv-alert-error').modal({
                onCloseEnd: function () {
                }
            });
            $('.p-desc-error').text("Hubo problemas en la aplicación.");
            $('#ptv-alert-error').modal('open');
            $("#btnCancelar").prop("disabled", false);
            console.log(e.responseText);
        }
    });

}

function validarCantDestinatarosChipsTO(id) {

    var inputData = M.Chips.getInstance($(id)).chipsData;

    if (inputData.length > 1) {
        for (var x = 1; x <= inputData.length; x++) {
            M.Chips.getInstance($(id)).deleteChip(x);
        }
    }

}

function validarInputChips(id) {
    var cantValidos = 0;
    var inputData = M.Chips.getInstance($(id)).chipsData;
    for (i = inputData.length - 1; i >= 0; i--) {
        var existeDesc = false;
        var inputDesc = inputData[i];
        $.each(
            M.Chips.getInstance($(id)).options.autocompleteOptions.data,
            function (x) {
                if (x == inputDesc.tag) {
                    existeDesc = true;
                }
            });
        if (!existeDesc) {
            M.Chips.getInstance($(id)).deleteChip(i);
        }
        else {
            cantValidos++;
        }
    }
    return cantValidos;
}


function validarInputChipsTipoEmpresa(id, ResultAreas) {
    var valid = 1;
    var inputData = M.Chips.getInstance($(id)).chipsData;
    for (i = inputData.length - 1; i >= 0; i--) {
        var inputDesc = inputData[i];
        for (var y = 0; y < ResultAreas.length; y++) {
            var valor = (inputDesc.tag) == (ResultAreas[y].descripcionArea);
            if (valor) {
                tipoEmpresaVal = ResultAreas[y].tipoEmpresa.toString();
                //si el tipo de empresa es interna
                if (tipoEmpresaVal == "1") {
                    $(".p-descripcion-inf").text("Solo puede agregar empresas Internas");
                    $("#ptv-alert-inf").modal('open');
                    M.Chips.getInstance($(id)).deleteChip(i);
                    valid = -1;
                   
                }
            }
        }
    }
    return valid;
}


function validarInputChipsTipoEmpresaEditInterna(id, ResultAreas) {
    var valid = 1;
    var inputData = M.Chips.getInstance($(id)).chipsData;
    for (i = inputData.length - 1; i >= 0; i--) {
        var inputDesc = inputData[i];
        for (var y = 0; y < ResultAreas.length; y++) {
            var valor = (inputDesc.tag) == (ResultAreas[y].descripcionArea);
            if (valor ) {
                tipoEmpresaVal = ResultAreas[y].tipoEmpresa.toString();
                //si el tipo de empresa es interna
                if (tipoEmpresaVal == "1") {
                    $(".p-descripcion-inf").text("El correlativo de este documento se ha generado para empresas internas, por favor agregar emrpresas internas.");
                    $("#ptv-alert-inf").modal('open');
                    M.Chips.getInstance($(id)).deleteChip(i);
                    valid = -1;
                    break;
                }
            }
        }
    }
    return valid;
}


function validarInputChipsTipoEmpresaEditExterna(id, ResultAreas) {
    var valid = 1;
    var inputData = M.Chips.getInstance($(id)).chipsData;
    for (i = inputData.length - 1; i >= 0; i--) {
        var inputDesc = inputData[i];
        for (var y = 0; y < ResultAreas.length; y++) {
            var valor = (inputDesc.tag) == (ResultAreas[y].descripcionArea);
            if (valor) {
                tipoEmpresaVal = ResultAreas[y].tipoEmpresa.toString();
                //si el tipo de empresa es interna
                if (tipoEmpresaVal == "0") {
                    $(".p-descripcion-inf").text("El correlativo de este documento se ha generado para empresas externas, por favor agregar la empresa externa.");
                    $("#ptv-alert-inf").modal('open');
                    M.Chips.getInstance($(id)).deleteChip(i);
                    valid = -1;
                    break;
                }
            }
        }
    }
    return valid;
}


function validarInputChipsTOCC(idTO, idCC) {
    var inputDataTO = M.Chips.getInstance($(idTO)).chipsData;
    var inputDataCC = M.Chips.getInstance($(idCC)).chipsData;
    for (var x = 0; x < inputDataCC.length; x++) {
        for (var y = 0; y < inputDataTO.length; y++) {
            var empresa = (inputDataCC[x].tag);
            var encontro = ((inputDataCC[x].tag).indexOf(inputDataTO[y].tag));
            if (encontro != -1) {
                M.Chips.getInstance($(idCC)).deleteChip(x);
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("El área " + empresa + " ya se encuentra en el destinatario TO");
                $('#ptv-alert-error').modal('open');
                return false;
            }
        }
    }
}

function validarInputChipsCCTO(idTO, idCC) {
    var inputDataTO = M.Chips.getInstance($(idTO)).chipsData;
    var inputDataCC = M.Chips.getInstance($(idCC)).chipsData;
    for (var x = 0; x < inputDataTO.length; x++) {
        for (var y = 0; y < inputDataCC.length; y++) {
            var empresa = (inputDataTO[x].tag);
            var encontro = ((inputDataTO[x].tag).indexOf(inputDataCC[y].tag));
            if (encontro != -1) {
                M.Chips.getInstance($(idTO)).deleteChip(x);
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("El área " + empresa + " ya se encuentra entre los destinatarios CC");
                $('#ptv-alert-error').modal('open');
                return false;
            }
        }
    }
}

function parseBoolean(valor) {
    var salida = false;
    if (valor === "True" || valor === true || valor === "1" || valor === "true" || valor === 1) {
        salida = true;
    }
    return salida;
}

function cargarPagina(url, div_replace, function_ok) {
    $.ajax({
        url: url,
        //dataType: "json",
        type: "GET",
        //contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $(div_replace).html(result);

            function_ok();
        }
    });
}


function archivosPermitidos(tipoarchivo) {
    var valido = false;
    tipoarchivo = tipoarchivo.toLowerCase();
    if (tipoarchivo === "pdf"
        || tipoarchivo === "doc" || tipoarchivo === "docx"
        || tipoarchivo === "msg"
        || tipoarchivo === "xls" || tipoarchivo === "xlsx"
        || tipoarchivo === "text" || tipoarchivo === "txt"
        || tipoarchivo === "jpeg" || tipoarchivo === "jpg") {
        valido = true;
    }
    return valido;
};

function validarUnArchivoAdjunto(event) {

    var file = event.target.files[0];
    //var formatoOK = true;
    //var tamanioOK = true;

    if (file === null)
        return;
    if (file.name != null) {
        var extension = $(file.name.split('.')).last()[0];

        if (!archivosPermitidos(extension)) {
            $('#ptv-alert-error').modal();
            $('.p-desc-error').text("Solo se permiten archivos en formatos PDF,doc,docx,xls,xlsx,text,txt,jpeg,jpg, msg");
            $('#ptv-alert-error').modal('open');
            //btnLimpiarAdjuntoClick();
            formatoOK = false;
            //$("#form-id").get(0).reset(); //the tricky part is to "empty" the input file here I reset the form.
            return false;
        }

        if (file.size >= 5 * 1024 * 1024) {
            $('#ptv-alert-error').modal();
            $('.p-desc-error').text("Tamaño máximo permitido 5MB");
            $('#ptv-alert-error').modal('open');
            //btnLimpiarAdjuntoClick();
            tamanioOK = false;
            return false;
        }
    }
}



function miConfirm(mensaje, function_ok) {

    $('.p-desc-confirmacion').text(mensaje);
    $('#ptv-alert-confirmacion').modal('open');

    $("#btnAgree").off("click").on("click", function () {
        function_ok(true);
    });
    $("#btnCloseConfirm").off("click").on("click", function () {
        function_ok(false);
    });
    $("#btnCancelConfirm").off("click").on("click", function () {
        function_ok(false);
    });
}
function miConfirm2(mensaje, function_ok) {

    $('.p-desc-confirmacion2').text(mensaje);
    $('#ptv-alert-confirmacion2').modal('open');

    $("#btnAgree").off("click").on("click", function () {
        function_ok(true);
    });
    $("#btnCloseConfirm").off("click").on("click", function () {
        function_ok(false);
    });
    $("#btnCancelConfirm").off("click").on("click", function () {
        function_ok(false);
    });
}
function EjecutarAjaxForm(obj, url, idform, function_ok, function_error) {
    var urlBase = $("#hdUrlBase").val();
    $("#modal_loading_agro").attr("style", "display:block");
    // Get form
    var form = $(idform)[0];
    var data = new FormData(form);
    $(obj).prop("disabled", true);
    //$("#btnCancelar_RA").prop("disabled", true);


    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: urlBase + url,
        //url: url,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (result) {
            function_ok(result);
            $(obj).prop("disabled", false);

        },
        complete: function () {
            $('#modal_loading_agro').attr("style", "display:none");
            $(obj).prop("disabled", false);
        },
        error: function (e, b, c) {
            function_error(e, b, c);
            $(obj).prop("disabled", false);
        }
    });
}
function EjecutarAjaxPost(obj, url, data, function_ok, function_error) {
    var urlBase = $("#hdUrlBase").val();
    $("#modal_loading_agro").attr("style", "display:block");
    $(obj).prop("disabled", true);
    //$("#btnCancelar_RA").prop("disabled", true);


    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        //url: url,
        url: urlBase + url,
        data: JSON.stringify(data),
        headers: { __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val() },
        timeout: 60000,
        cache: false,
        success: function (result) {
            function_ok(result);
            $(obj).prop("disabled", false);
        },
        complete: function () {
            $('#modal_loading_agro').attr("style", "display:none");
            $(obj).prop("disabled", false);
        },
        error: function (e, b, c) {
            function_error(e, b, c);
            $(obj).prop("disabled", false);
        }
    });
    
}
function MsgInformacion(mensaje, function_close) {
    $('#ptv-alert-ok').modal({
        onCloseEnd: function_close
    });
    $('.p-descripcion').text(mensaje);
    $('#ptv-alert-ok').modal('open');
}
function MsgInformacion2(mensaje, function_close) {
    $('#ptv-alert-ok2').modal({
        onCloseEnd: function_close
    });
    $('.p-descripcion2').text(mensaje);
    $('#ptv-alert-ok2').modal('open');
}

function MsgError(mensaje) {
    $('#ptv-alert-error').modal({
        onCloseEnd: function () {
            location.reload();
        }
    });
    $('.p-desc-error').text(mensaje);
    $('#ptv-alert-error').modal('open');
}

function validarFormatoFecha(campo) {
    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    if ((campo.match(RegExPattern)) && (campo != '')) {
        return true;
    } else {
        return false;
    }


}


function fechaComparar(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}