﻿$(document).ready(function () {
    
    var urlBase = $("#hdUrlBase").val();
    var ResultRemitente = [];
    var ResultAreas = [];
    var confirm = true;
    var confirmEdit = true;
    var confirmRegistrarAvance = true;
    var confirmFinalizar = true;
    var confirmDerivar = true;
    var confirmDevolver = true;
    var add = 0;
    var codDocumentoAderivar = 0;
    var correlativoDerivar = "";
    var esJefeDerivarSied = false;
    var codDocDerivacion = 0;
    var derivadosComent = new Object();
    var _codDocumentoVER = "";
    var codRepresentandoFinalizar = "";
    var OrigenFinalizar = "";

    var bandejaEntrada = (function () {

        var inputPlazo = $(".input-plazo");
        var inputPlazoEdit = $(".input-plazoEdit");
        var inputFechaPlazo = $(".input-fecha-plazo")[0];
        var inputFechaPlazoEdit = $(".input-fecha-plazoEdit")[0];
        var inputDatePicker = $("input.datepicker");
        var lblFechaDefault = $("label.lbl-fecha-default");
        var inputFechaRecepcion = $("#FechaRecepcion");
        var inputFechaDocumento = $("#fechaDocumento");
        var inputFechaRecepcionEdit = $("#FechaRecepcionEdit");
        var inputfechaDocumentoEdit = $("#fechaDocumentoEdit");
        //var remitenteAutoComplete = $("input[name='remitente'].autocomplete");
        var asuntoAutoComplete = $("input[name='asunto'].autocomplete");
        var asuntoAutoCompleteEdit = $("input[name='asuntoEdit'].autocomplete");
        //var hdCodRemitente = $("#hdCodRemitente");
        var cboRemitidoPor = $("#remitidoPor");
        var hdCodArea = $("#hdCodArea");
        var hdCodAreaCC = $("#hdCodAreaCC");
        var hdCodAreaCCEdit = $("hdCodAreaCCEdit");
        var hdCodAreasDerivacion = $("#hdCodAreasDerivacion");
        var hdCodAsunto = $("#hdCodAsunto");
        var hdCodAsuntoEdit = $("#hdCodAsuntoEdit");
        var inputAreas = $(".chips-autocomplete.chips-areas input");
        var btnLimpiarAdjunto = $("#btnLimpiarAdjunto");
        var btnLimpiarAnexoEdit = $("#btnLimpiarAnexoEdit");
        var btnLimpiarAnexo = $("#btnLimpiarAnexo");
        var btnLimpiarAdjuntoEdit = $("#btnLimpiarAdjuntoEdit");
        var inputAdjunto = $("#adjuntarArchivo");
        var inputAnexo = $("#adjuntarAnexo");
        var inputAdjuntoEdit = $("#adjuntarArchivoEdit");
        var inputAnexoEdit = $("#adjuntarAnexoEdit");
        var fechaRegistroDesde = $("#FechaRegistroDesde");
        var fechaRegistroHasta = $("#FechaRegistroHasta");
        var btnCancelarEdit = $("#btnCancelarEdit");
        var btnCancelar = $("#btnCancelar");
        var btnAnular = $("#btnAnular");
        var btnDevolver = $("#btnDevolver");
        var btnFinalizar = $("#btnFinalizar");
        var adjuntarAnexo_FI = $("#adjuntarAnexo_FI");
        var btnGuardar = $("#btnGuardar");
        var btnRegistrarAvance = $("#btnRegistrarAvance");
        var btnEdit = $("#btnEdit");
        var btnAgree = $("#btnAgree");
        var btnGuardarDerivacion = $("#btnGuardarDerivacion");
        var btnCancelar = $("#btnCancelar");
        var btnCancelarModalDerivar = $("#btnCancelarModalDerivar");
        var cboMotivoFirma = $("#cbMotivoFirma");

        var init = function () {
          /*  remitenteAutoComplete.autocomplete({
                onAutocomplete: onEmpresaAutocomplete,
                data: {}
            });*/

            asuntoAutoComplete.autocomplete({
                onAutocomplete: onAsuntoAutocomplete,
                data: {}
            });

            asuntoAutoCompleteEdit.autocomplete({
                onAutocomplete: onAsuntoAutocomplete,
                data: {}
            });

            inputPlazo.on("change", plazoChange);
            inputPlazoEdit.on("change", plazoChangeEdit);
            inputFechaRecepcion.on("change", fechaRecepcionChange);
            inputFechaRecepcion.on("blur", fechaRecepcionChange);
            asuntoAutoComplete.on("blur", asuntoBlur);
            btnLimpiarAdjunto.on("click", btnLimpiarAdjuntoClick);
            btnLimpiarAnexoEdit.on("click", btnLimpiarAnexoEditClick);
            btnLimpiarAnexo.on("click", btnLimpiarAnexoClick);
            btnLimpiarAdjuntoEdit.on("click", btnLimpiarAdjuntoEditClick);
            inputAdjunto.on("change", adjuntoChange);
            inputAnexo.on("change", anexoChange);
            inputAdjuntoEdit.on("change", adjuntoChange);

            inputAnexoEdit.on("change", anexoChangeEdit);
            fechaRegistroDesde.on("change", fechaRegIniChange);
            fechaRegistroHasta.on("change", fechaRegFinChange);
            btnGuardar.on("click", btnGuardarClick);
            btnRegistrarAvance.on("click", btnRegistrarAvanceClick);
            btnEdit.on("click", btnEditClick);
            btnAgree.on("click", btnAgreeClick);
            btnCancelarEdit.on("click", btnCancelarEditClick);
            btnAnular.on("click", btnAnularClick);
            btnFinalizar.on("click", btnFinalizarClick);
            adjuntarAnexo_FI.on("change", validarUnArchivoAdjunto);
            btnCancelar.on("click", btnCancelarClick);
            inputFechaRecepcion.on("change", validacionFechasChange);
            inputFechaDocumento.on("change", validacionFechasChange);
            inputFechaRecepcionEdit.on("change", validacionFechasEditChange);
            inputFechaRecepcionEdit.on("change", fechaRecepcionChangeEdit);
            inputFechaRecepcionEdit.on("blur", fechaRecepcionChangeEdit);
            inputfechaDocumentoEdit.on("change", validacionFechasEditChange);
            btnDevolver.on("click", btnDevolverClick);
            btnGuardarDerivacion.on("click", btnGuardarDerivacionClick);
            btnCancelarModalDerivar.on("click", btnCancelarModalDerivarClick);
            cargarFormNuevoDocumento();
            cargarSelectMotivoFirma(cboMotivoFirma);
        };
        
        var cargarFormNuevoDocumento = function () {
            var fechaHoy = new Date();
            var plazoDefault = parseInt($(inputPlazo).val());
            var currentMonth = fechaHoy.getMonth() + 1;
            var currentDay = fechaHoy.getDate();

            var strFecha = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaHoy.getFullYear();

            updateFechaPlazo(plazoDefault, fechaHoy);
           /* cargarRemitente();*/
            cargarAreas();
            cargarAsuntos();
            
            $(inputDatePicker).val(strFecha);
            $(lblFechaDefault).addClass("active");

            var newdate = new Date(fechaHoy);

            newdate.setDate(fechaHoy.getDate() - 30);

            var dd = newdate.getDate().toString();
            dd = dd.length > 1 ? dd : '0' + dd;

            var mm = (newdate.getMonth() + 1).toString();
            mm = mm.length > 1 ? mm : '0' + mm;

            var y = newdate.getFullYear();
            var someFormattedDate = dd + '/' + mm + '/' + y;

            $("#FechaRegistroDesde").val(someFormattedDate);

            validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);

        };

        var onChipAdd = function (e) {

            validarInputChips('.chips-areas');
            validarInputChipsCCTO('.chips-areas', '.chips-areasCC');

            $("#chipTO").find('input').val("");
            $("#chipCC").find('input').val("");

           /* if ($("#chipTO").find("div").length > 1) {
                $("#chipTO").find("div")[1].remove();
            } else {*/

                var chipsTO = M.Chips.getInstance($('.chips-areas')).chipsData;
                var codigoTO = "";
                if (chipsTO.length > 0) {
                    var chipsCCtag = M.Chips.getInstance($('.chips-areas')).chipsData;
                    for (var x = 0; x < chipsTO.length; x++) {
                        for (var y = 0; y < ResultAreas.length; y++) {
                            var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                            if (valor != -1) {
                                codigo = ResultAreas[y].codigoArea.toString();
                                codigoTO = codigoTO + ((codigoTO.length > 0) ? '|' : "") + codigo;
                            }
                        }
                    }
                }

            if (codigoTO.toString().length > 0) {
                    $(".chips-autocomplete.chips-areas input").prop("required", false);
                }

            if (codigoTO.toString().length == 0) {
                    $(".chips-autocomplete.chips-areas input").prop("required", true);
                }

            hdCodArea.val(codigoTO);
           /* }*/
        };

        var onChipAddRemitente = function (e) {
            hdCodRemitente.val("");
            limpiarCboRemitidoPor();
            validarInputChips('.chips-remitente');

            if ($("#chipRemitente").find("div").length > 1) {
                $("#chipRemitente").find("div")[1].remove();
            } else {
                var value = $("#chipRemitente").find(".chip").first().text();
                var str = value.split("|");
                var codigo = str[0];
                var descripcion = codigo.replace("close", "").trim();

                if (descripcion != "") {
                    for (var x = 0; x < ResultRemitente.length; x++) {
                        var valor = (ResultRemitente[x].descripcionRemitente).indexOf(descripcion);
                        if (valor != -1) {
                            codigo = ResultRemitente[x].codigoRemitente.toString();
                            break;
                        } else {
                            codigo = "";
                        }
                    }
                }

                if (codigo.toString().length > 0) {
                    obtenerTrabajadoresPorEmpresa(bindTrabajadoresCombo, codigo);
                    $(".chips-autocomplete.chips-remitente input").prop("required", false);
                }

                if (codigo.toString().length == 0) {

                    $(".chips-autocomplete.chips-remitente input").prop("required", true);
                }

                hdCodRemitente.val(codigo);
            }
        };

        var blurChip = function () {
            $("#chipsTrabajadores").find("input").val("");
            $("#chipsDerivacion").find("input").val("");
            $("#chipRemitente").find("input").val("");
            $("#chipTO").find('input').val("");
            $("#chipCC").find('input').val("");
            $("#chipCCEdit").find('input').val("");
        }

        var onChipAddCC = function (e) {
            hdCodAreaCC.val("");
            validarInputChips('.chips-areasCC');
            
            validarInputChipsTOCC('.chips-areas', '.chips-areasCC');

            var chipsCC = M.Chips.getInstance($('.chips-areasCC')).chipsData;
            var codigosCC = "";
            if (chipsCC.length > 0) {
                var chipsCCtag = M.Chips.getInstance($('.chips-areasCC')).chipsData;
                for (var x = 0; x < chipsCC.length; x++) {
                    for (var y = 0; y < ResultAreas.length; y++) {
                        var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                        if (valor != -1) {
                            codigo = ResultAreas[y].codigoArea.toString();
                            codigosCC = codigosCC + ((codigosCC.length > 0) ? '|' : "") + codigo;
                        }
                    }
                }
            }
            hdCodAreaCC.val(codigosCC);
            $(".chips-autocomplete.chips-areasCC input").prop("required", false);
          /*  if (codigosCC.toString().length > 0) {
                $(".chips-autocomplete.chips-areasCC input").prop("required", false);
            }

            if (codigosCC.toString().length == 0) {
                $(".chips-autocomplete.chips-areasCC input").prop("required", true);
            }*/
        };

        var onChipDerivacion = function (e) {
            hdCodAreasDerivacion.val("");
            validarInputChips('.chips-areasDerivacion');

            var chipsCC = M.Chips.getInstance($('.chips-areasDerivacion')).chipsData;
            var codigosCC = "";
            if (chipsCC.length > 0) {
                var chipsCCtag = M.Chips.getInstance($('.chips-areasDerivacion')).chipsData;
                for (var x = 0; x < chipsCC.length; x++) {
                    for (var y = 0; y < ResultAreas.length; y++) {
                        var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                        if (valor != -1) {
                            codigo = ResultAreas[y].codigoArea.toString();
                            codigosCC = codigosCC + ((codigosCC.length > 0) ? '|' : "") + codigo;
                        }
                    }
                }
            }
            hdCodAreasDerivacion.val(codigosCC);
            if (codigosCC.toString().length > 0) {
                $(".chips-autocomplete.chips-areasDerivacion input").prop("required", false);
            }

            if (codigosCC.toString().length == 0) {
                $(".chips-autocomplete.chips-areasDerivacion input").prop("required", true);
            }

            cargarChipTrabajadores(codigosCC);
        };

        var plazoChange = function () {
            var plazo = inputPlazo[0].value;
            if (plazo > 365) {
                plazo = 365;
            }
            var strFecRecepcion = inputFechaRecepcion[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazo(plazo, fechaInicio);
        }

        var plazoChangeEdit = function () {
            var plazo = inputPlazoEdit[0].value;
            if (plazo > 365) {
                plazo = 365;
            }
            var strFecRecepcion = inputFechaRecepcionEdit[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazoEdit(plazo, fechaInicio);
        }

        var fechaRecepcionChange = function () {
            var plazo = inputPlazo[0].value;
            var strFecRecepcion = inputFechaRecepcion[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazo(plazo, fechaInicio);
        };

        var fechaRecepcionChangeEdit = function () {
            var plazo = inputPlazoEdit[0].value;
            var strFecRecepcion = inputFechaRecepcionEdit[0].value;
            var arrFecRecep = strFecRecepcion.split('/');
            var fechaInicio = new Date(arrFecRecep[2], (parseInt(arrFecRecep[1]) - 1), arrFecRecep[0]);

            updateFechaPlazoEdit(plazo, fechaInicio);
        };

        var updateFechaPlazo = function (plazo, fechaInicio) {
            var params = JSON.stringify({
                plazo: plazo,
                fechaInicio: fechaInicio
            });

            obtenerFechaPlazo(plazo, fechaInicio, function (result) {
                if (result.Resultado == 1) {
                    var fechaPlazo = new Date(parseInt(result.data.toString().substr(6)));

                    var currentMonth = (fechaPlazo.getMonth() + 1);
                    var currentDay = fechaPlazo.getDate();

                    var strFechaPlazo = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaPlazo.getFullYear();

                    $(inputFechaPlazo).val(strFechaPlazo);
                }
            });
        };

        var updateFechaPlazoEdit = function (plazo, fechaInicio) {
            obtenerFechaPlazo(plazo, fechaInicio, function (result) {
                if (result.Resultado === 1) {
                    var fechaPlazo = new Date(parseInt(result.data.toString().substr(6)));

                    var currentMonth = (fechaPlazo.getMonth() + 1);
                    var currentDay = fechaPlazo.getDate();

                    var strFechaPlazo = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaPlazo.getFullYear();

                    $(inputFechaPlazoEdit).val(strFechaPlazo);
                }
            });
        };

        var lpad = function (str, padString, length) {
            //var str = this;
            while (str.length < length)
                str = padString + str;
            return str;
        };

        var bindEmpresasAutoComplete = function (obj) {
            //var elem = $("#frmNuevoDocumento input[name='remitente'].autocomplete")[0];

            //var instance = M.Autocomplete.getInstance(remitenteAutoComplete);
            //var result = {};
            //for (var i = 0; i < obj.length; i++) {                
            //    result[(obj[i].CodEmpresa + "|" + obj[i].Descripcion).toString()] = null;
            //}

            //instance.updateData(result);
        };

        var cargarEmpresas = function () {
            obtenerEmpresas(bindEmpresasAutoComplete);
        };

        var onEmpresaAutocomplete = function (e) {
            var str = e.split("|");
            var codigo = str[0];
            hdCodRemitente.val(codigo);
            cargarTrabajadores(codigo);
        };

        var remitenteBlur = function () {
            remitenteAutoComplete.val(remitenteAutoComplete.val().trim());
            var remitente = remitenteAutoComplete.val();
            var arrCodRemitente = remitente.split('|');

            var limpiarCboRemitidoPor = (hdCodRemitente.val() == arrCodRemitente[0]) ? false : true;

            if (remitenteAutoComplete.val().length == 0 || limpiarCboRemitidoPor) {
                cboRemitidoPor.empty();
                cboRemitidoPor.append("<option value=''>Seleccione</option>");
                cboRemitidoPor.formSelect();
            }
        };

        var limpiarCboRemitidoPor = function () {
            cboRemitidoPor.empty();
            cboRemitidoPor.append("<option value=''>Seleccione</option>");
            cboRemitidoPor.formSelect();
        }

        var asuntoBlur = function () {

            var insAsunto = M.Autocomplete.getInstance(asuntoAutoComplete);

            console.log(insAsunto);
        };

        var bindTrabajadoresCombo = function (trabajadores) {
            cboRemitidoPor.empty();
            cboRemitidoPor.append("<option value=''>Seleccione</option>")
            $.each(trabajadores, function (ind, val) {
                cboRemitidoPor.append("<option value='" + val.CodRepresentante + "'>" + val.Nombre + "</option>")
            });
            cboRemitidoPor.formSelect();
        };

        var cargarTrabajadores = function (codEmpresa) {

            var remitente = $("#remitente").val();

            obtenerTrabajadoresPorEmpresa(bindTrabajadoresCombo, codEmpresa);
        }

        var cargarRemitente = function () {

            obtenerEmpresas(function (data) {
                ResultRemitente = [];
                var result = {};
                for (var i = 0; i < data.length; i++) {
                    var remitente = {
                        codigoRemitente: data[i].CodEmpresa,
                        descripcionRemitente: data[i].Descripcion
                    }
                    result[(data[i].Descripcion).toString()] = null;
                    ResultRemitente.push(remitente);
                }

                $('#chipRemitente').chips({
                    onChipAdd: onChipAddRemitente,
                    onChipDelete: onChipAddRemitente,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });

                $(".chips-autocomplete.chips-remitente input").prop("required", true);
                $(".chips-autocomplete.chips-remitente input").on("blur", blurChip);
            });
        };

        var cargarAreas = function () {

            obtenerAreas(function (data) {
                ResultAreas = [];
                var result = {};
                for (var i = 0; i < data.length; i++) {
                    var area = {
                        codigoArea: data[i].CodArea,
                        descripcionArea: data[i].Descripcion
                    }
                    result[(data[i].Descripcion).toString()] = null;
                    ResultAreas.push(area);
                }



                $('#chipTO').chips({
                    onChipAdd: onChipAdd,
                    onChipDelete: onChipAdd,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });

                $('#chipCC').chips({
                    onChipAdd: onChipAddCC,
                    onChipDelete: onChipAddCC,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });

                $('#chipsDerivacion').chips({
                    onChipAdd: onChipDerivacion,
                    onChipDelete: onChipDerivacion,
                    autocompleteOptions: {
                        data: result,
                        limit: Infinity,
                        minLength: 1
                    }
                });


                $(".chips-autocomplete.chips-areas input").prop("required", true);
                $(".chips-autocomplete.chips-areas input").on("blur", blurChip);
                $(".chips-autocomplete.chips-areasCC input").on("blur", blurChip);
                $(".chips-autocomplete.chips-areasCCEdit input").on("blur", blurChip);
                $(".chips-autocomplete.chips-areasDerivacion input").prop("required", true);
                $(".chips-autocomplete.chips-areasDerivacion input").on("blur", blurChip);
            });
        };

        var cargarAsuntos = function () {
            obtenerAsuntos(function (data) {
                var result = {};
                for (var i = 0; i < data.length; i++) {
                    result[(data[i].Descripcion).toString()] = null;
                }

                var instance = M.Autocomplete.getInstance(asuntoAutoComplete);
                instance.updateData(result);
                var instanceEdit = M.Autocomplete.getInstance(asuntoAutoCompleteEdit);
                instanceEdit.updateData(result);

            });
        };

        var onAsuntoAutocomplete = function (e) {
            var str = e.split("|");
            var codigo = str[0];
            hdCodAsunto.val(codigo);
            hdCodAsuntoEdit.val(codigo);
        };

        var btnLimpiarAnexoEditClick = function () {
            $("#adjuntarAnexoEdit").val('');
            $("#adjuntarAnexoAuxEdit").val('');
        }

        var btnLimpiarAnexoClick = function () {
            $("#adjuntarAnexo").val('');
            $("#adjuntarAnexoAux").val('');
        }

        var btnLimpiarAdjuntoClick = function () {
            $("#adjuntarArchivo").val('');
            $("#adjuntarArchivoAux").val('');
            $("#estadoDocumento").attr("data-badge-caption", "INGRESADO");
        };

        var btnLimpiarAdjuntoEditClick = function () {
            $("#adjuntarArchivoEdit").val('');
            $("#adjuntarArchivoAuxEdit").val('');
            $("#estadoDocumentoEdit").attr("data-badge-caption", "INGRESADO");
        };

        var anexoChange = function (event) {

            var file = inputAnexo[0].files;
            var sumaAnexos = 0;
            if (file == null)
                return;

            for (var x = 0; x < file.length; x++) {
                sumaAnexos = sumaAnexos + inputAnexo[0].files[x].size;
            }

            if (sumaAnexos >= 5 * 1024 * 1024) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("El total máximo permitido debe ser 5MB");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAnexoClick();
                tamanioOK = false;
                return false;
            }
        };

        var validacionFechasChange = function () {
            var validarformatofechaRecep = validarFormatoFecha(inputFechaRecepcion.val());
            var validarformatofechaDoc = validarFormatoFecha(inputFechaDocumento.val());
            if (validarformatofechaRecep == true && validarformatofechaDoc == true) {

                if (inputFechaRecepcion.val() == "") {
                    inputFechaRecepcion.val(_fechaDocumento);
                }

                if (inputFechaDocumento.val() == "") {
                    inputFechaDocumento.val(_fechaDocumento);
                }

                if (fechaComparar(inputFechaDocumento.val()) > fechaComparar(inputFechaRecepcion.val())) {
                    inputFechaRecepcion.val(inputFechaDocumento.val());
                }

                validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
            } else {

                if (!validarformatofechaRecep) {
                    inputFechaRecepcion.val(_fechaDocumento);
                    if (fechaComparar(inputFechaDocumento.val()) > fechaComparar(inputFechaRecepcion.val())) {
                        inputFechaRecepcion.val(inputFechaDocumento.val());
                    }
                    validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
                } else if (!validarformatofechaDoc) {
                    inputFechaDocumento.val(_fechaDocumento);
                    validarMinMax2Fechas(inputFechaRecepcion, inputFechaDocumento);
                }
                return false;
            }
            
        };

        var validacionFechasEditChange = function () {
            var validarformatofechaRecep = validarFormatoFecha(inputFechaRecepcionEdit.val());
            var validarformatofechaDoc = validarFormatoFecha(inputfechaDocumentoEdit.val());
            if (validarformatofechaRecep == true && validarformatofechaDoc == true) {

                if (inputFechaRecepcionEdit.val() == "") {
                    inputFechaRecepcionEdit.val(fechaRecepcionDefaultEdit);
                }

                if (inputfechaDocumentoEdit.val() == "") {
                    inputfechaDocumentoEdit.val(fechaDocDefaultEdit);
                }

                if (fechaComparar(inputfechaDocumentoEdit.val()) > fechaComparar(inputFechaRecepcionEdit.val())) {
                    inputFechaRecepcionEdit.val(inputfechaDocumentoEdit.val());
                }

                validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
            } else {

                if (!validarformatofechaRecep) {
                    inputFechaRecepcionEdit.val(fechaRecepcionDefaultEdit);
                    if (inputfechaDocumentoEdit.val() > inputFechaRecepcionEdit.val()) {
                        inputFechaRecepcionEdit.val(inputfechaDocumentoEdit.val());
                    }
                    validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
                } else if (!validarformatofechaDoc) {
                    inputfechaDocumentoEdit.val(fechaDocDefaultEdit);
                    validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
                }
                return false;
            }
        };

        var anexoChangeEdit = function (event) {

            var file = inputAnexoEdit[0].files;
            var sumaAnexos = 0;
            if (file == null)
                return;

            for (var x = 0; x < file.length; x++) {
                sumaAnexos = sumaAnexos + inputAnexoEdit[0].files[x].size;
            }

            if (sumaAnexos >= 5 * 1024 * 1024) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("El total máximo permitido debe ser 5MB");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAnexoClick();
                tamanioOK = false;
                return false;
            }
        };

        var fechaRegIniChange = function () {
            var fecIni = $("#FechaRegistroDesde").val().split("/");
            var fecFin = $("#FechaRegistroHasta").val().split("/");
            fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
            fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
            if (fecIni > fecFin) {

                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("La fecha de inicio no puede ser mayor a la fecha fin.");
                $('#ptv-alert-error').modal('open');
                $("#FechaRegistroDesde").val($("#FechaRegistroHasta").val());
            }

        }

        var fechaRegFinChange = function () {
            var fecIni = $("#FechaRegistroDesde").val().split("/");
            var fecFin = $("#FechaRegistroHasta").val().split("/");
            fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
            fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
            if (fecIni > fecFin) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("La fecha fin no puede ser menor a la fecha de inicio.");
                $('#ptv-alert-error').modal('open');
                $("#FechaRegistroHasta").val($("#FechaRegistroDesde").val());
            }

        }

        var btnAnularClick = function (event) {
            event.preventDefault();
            var mensaje = "";

            if ($("#txtAnularComentario").val() == "") {
                mensaje = "Debe ingresar un comentario.";
            }
 
            if (mensaje.length > 0) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text(mensaje);
                $('#ptv-alert-error').modal('open');
                return false;
            }

            var data = new Object();
            data.CodDocumento = $("#hdCodDocumentoAnular").val();
            data.Comentario = $("#txtAnularComentario").val();

            EjecutarAjax("POST", "/Documento/AnularDocumentoYDerivados", data, function () {
                window.location.href = window.location.origin + window.location.pathname;
            }, "El documento se anuló correctamente");

            return false;
        };

        var btnFinalizarClick = function (event) {
            event.preventDefault();
            var mensaje = "";
            
            $.ajax({
                url: urlBase + "/Documento/GetEstadoDocumento?codDocumento=" + $("#hdCodDocumentoFinalizar").val(),
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        if (result.data == 3) {
                            mensaje = "El documento principal se encuentra anulado, no puede finalizar el documento.";
                        }
                    }
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            if ($("#txtFinalizarComentario").val() == "") {
                mensaje = "Debe ingresar un comentario.";
            }
            if (($("#txtFinalizarComentario").val().length < 15 || $("#txtFinalizarComentario").val().length > 200) && mensaje.length == 0) {
                mensaje = "El comentario no debe ser menor que 15 y mayor a 200 caracteres.";
            }
            if (mensaje.length > 0) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text(mensaje);
                $('#ptv-alert-error').modal('open');
                return false;
            }
            var request = new Object();
            request.comentario = new Object();
            request.comentario.CodDocumento = $("#hdCodDocumentoFinalizar").val();
            request.comentario.Comentario = $("#txtFinalizarComentario").val();


            $("#modal_loading_agro").attr("style", "display:block");
            // Get form
            var form = $('#frmFinalizarDocumento')[0];

            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelar_RA").prop("disabled", true);

            //functionEstadosDocAjax($(this), "/BandejaEntrada/ValidarFinalizarDocumento", data,
            functionEstadosDocAjax($(this), urlBase+"/BandejaEntrada/ValidarFinalizarDocumento", data,
                function (result) {

                    if (result.Resultado === 1) {
                        if (result.Informacion !== null && result.Informacion.length > 0) {
                            confirmFinalizar = false;
                            $('.p-desc-confirmacion').html("Existen áreas pendientes de atender ¿Desea continuar con el registro?<br/>" + result.Informacion);
                            $('#ptv-alert-confirmacion').modal('open');
                        }
                        else {
                            if (confirm) {
                                confirmFinalizar = false;
                                $('.p-desc-confirmacion').html('¿Documento sin derivaciones pendientes, está seguro de finalizar el documento?');
                                $('#ptv-alert-confirmacion').modal('open');
                            }

                        }
                    }
                    else {
                        $('.p-desc-error').text("Hubo problemas en la aplicación.");
                        $('#ptv-alert-error').modal('open');
                    }
                });

            return false;
        };

        var btnGuardarDerivacionClick = function (event) {

            var bAnulado = false;
            $.ajax({
                url: urlBase + "/Documento/GetEstadoDocumento?codDocumento=" + codDocumentoAderivar,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        if (result.data == 3) {
                            $('#ptv-alert-error').modal();
                            $('.p-desc-error').text("El documento principal se encuentra anulado, no puede continuar con el flujo de derivación.");
                            $('#ptv-alert-error').modal('open');
                            bAnulado = true;
                        }
                    }
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            if ($('#frmDerivarDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmDerivarDocumento')[0].checkValidity() || bAnulado) {
                return true;
            }
            

            derivadosComent = new Object();
            var regex = /(\d+)/g;
            var Derivados = [];
            //recorremos primeros las areas(para este alcance la area solo es 1)
            for (var y = 0; y < M.FormSelect.init(document.querySelectorAll('.selectAreas'), null).length; y++) {
                var objDerivados = {
                    CodAccion:  '',
                    CodArea: '',
                    CodTrabajadores: '',
                };
                var idArea = M.FormSelect.init(document.querySelectorAll('.selectAreas'), null)[y].el.id;
                var idAccion = M.FormSelect.init(document.querySelectorAll('.selectAccion'), null)[y].el.id;
                objDerivados.CodArea = M.FormSelect.init(document.querySelectorAll('#' + idArea), null)[0].getSelectedValues();
                objDerivados.CodAccion = M.FormSelect.init(document.querySelectorAll('#' + idAccion), null)[0].getSelectedValues();
                //obtenemos el id de dicha area para pasarlo a los trabajadores
                var idtrabajador = (idArea.match(regex));
                //recorremos los trabajadores sin multiple de dicha area
                for (var z = 0; z < M.FormSelect.init(document.querySelectorAll('#selectTrabajadores' + idtrabajador), null).length; z++) {
                    var valuetrabajador = M.FormSelect.init(document.querySelectorAll('#selectTrabajadores' + idtrabajador), null)[z].getSelectedValues();
                    if (valuetrabajador.length > 0) {
                        objDerivados.CodTrabajadores = valuetrabajador;
                    }
                }
                //recorremos los trabajadores con multiple de dicha area
                for (var g = 0; g < M.FormSelect.init(document.querySelectorAll('#selectTrabajadoresm' + idtrabajador), null).length; g++) {
                    var valuetrabajador = M.FormSelect.init(document.querySelectorAll('#selectTrabajadoresm' + idtrabajador), null)[g].getSelectedValues();
                    if (valuetrabajador.length > 0) {
                        objDerivados.CodTrabajadores = valuetrabajador;
                    }
                }

                Derivados.push(objDerivados);
            }

            derivadosComent.CodDocumento = codDocumentoAderivar;
            derivadosComent.DerivarDocumento = Derivados;
            derivadosComent.comentario = $("#txtDerivacionComentario").val();
            derivadosComent.correlativo = correlativoDerivar;
            console.log(derivadosComent);


            //VALIDAMOS QUE NO SE DERIVE AL MISMO TRABAJADOR
            var arrCodTrabajador = [];
            for (var k = 0; k < derivadosComent.DerivarDocumento.length; k++) {
                for (var y = 0; y < derivadosComent.DerivarDocumento[k].CodTrabajadores.length; y++) {
                    arrCodTrabajador.push(derivadosComent.DerivarDocumento[k].CodTrabajadores[y]);
                }
            }
            var trabajadoresConDerivacion = '';
            var arrtrabajadoresConDerivacion = [];
            var trabajadoresConDerivacionCC = '';
            var responsableDocumento = 'no';
            //validando en la tabla derivacion
            for (var z = 0; z < arrCodTrabajador.length; z++) {
                $.ajax({
                    url: urlBase + "/Documento/ObtenerDocumentoDerivadoxTrabajador?CodDocumento=" + codDocumentoAderivar + "&CodTrabajador=" + arrCodTrabajador[z],
                    //url: document.location.origin + "/Documento/ObtenerDocumentoDerivadoxTrabajador?CodDocumento=" + codDocumentoAderivar + "&CodTrabajador=" + arrCodTrabajador[z],
                    dataType: "json",
                    async: false,
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {
                        if (result.data != null) {
                            if (result.data.TipoAcceso == 1) {
                                trabajadoresConDerivacion = trabajadoresConDerivacion + (result.data.NombreResponsable + ' ' + result.data.ApellidoPatResponsable + ' ' + result.data.ApellidoMatResponsable + ', ')
                                arrtrabajadoresConDerivacion.push(result.data.NombreResponsable + ' ' + result.data.ApellidoPatResponsable + ' ' + result.data.ApellidoMatResponsable);
                                if (result.data.Responsable == 'si') {
                                    responsableDocumento = 'si';
                                }
                            } else if (result.data.TipoAcceso == 2) {
                                trabajadoresConDerivacionCC = trabajadoresConDerivacionCC + (result.data.NombreResponsable + ' ' + result.data.ApellidoPatResponsable + ' ' + result.data.ApellidoMatResponsable + ', ')
                            }
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            }

            if (trabajadoresConDerivacion.length > 0) {
                confirmDerivar = false;
                if (arrtrabajadoresConDerivacion.length == 1) {

                    if (responsableDocumento == 'si') {
                        $('#ptv-alert-error').modal();
                        $('.p-desc-error').text("El usuario " + trabajadoresConDerivacion + " ya es el responsable del documento. No se puede realizar esta acción.");
                        $('#ptv-alert-error').modal('open');
                    } else {
                        $('#ptv-alert-error').modal();
                        $('.p-desc-error').text("El trabajador " + trabajadoresConDerivacion + " ya posee este documento derivado");
                        $('#ptv-alert-error').modal('open');
                    }
                } else {
                    $('#ptv-alert-error').modal();
                    $('.p-desc-error').text("Los trabajadores " + trabajadoresConDerivacion + " ya poseen este documento derivado");
                    $('#ptv-alert-error').modal('open');
                }
                return false;
            }

            validarDerivacionCC(trabajadoresConDerivacionCC);

            if (confirmDerivar) {
                confirmDerivar = false;
                $('.p-desc-confirmacion').text('¿Está seguro de derivar el documento?');
                $('#ptv-alert-confirmacion').modal('open');
            }

        };

        var RegistrarDerivarDocumentoAjax = function () {
            EjecutarAjaxRespuestaCorreo("POST", "/Documento/DerivarDocumento", derivadosComent, function () {
                /*window.location.href = window.location.origin + "/BandejaEntrada";*/
                //window.location.href = window.location.origin + window.location.pathname;
                window.location.href = window.location.origin + window.location.pathname;
            }, "El documento fue derivado satisfactoriamente");
        }

        var validarDerivacionCC = function (trabajadoresConDerivacionCC) {
            var strValidarCC = "";
            if (trabajadoresConDerivacionCC.length > 0) {
                strValidarCC = 'Los destinatarios ' + trabajadoresConDerivacionCC + ' tiene el documento en copia, al derivar se actualizará su acceso ¿Está seguro de continuar?';
                confirmDerivar = false;
            } else {
                confirmDerivar = true;
            }
            if (confirmDerivar == false) {
                $('.p-desc-confirmacion').text(strValidarCC);
                $('#ptv-alert-confirmacion').modal('open');
            }
        }

        var btnDevolverClick = function (event) {
            event.preventDefault();
            var mensaje = "";

            $.ajax({
                url: urlBase + "/Documento/GetEstadoDocumento?codDocumento=" + _codDocumentoVER,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        if (result.data == 3) {
                            mensaje = "El documento principal se encuentra anulado, no puede devolver el documento.";
                        }
                    }
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            if ($("#txtDevolverComentario").val() == "") {
                mensaje = "Debe ingresar un comentario.";
            }
            if (($("#txtDevolverComentario").val().length < 15 || $("#txtDevolverComentario").val().length > 200) && mensaje.length == 0) {
                mensaje = "El comentario no debe ser menor que 15 y mayor a 200 caracteres.";
            }
            if (mensaje.length > 0) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text(mensaje);
                $('#ptv-alert-error').modal('open');
                return false;
            }
            if (codDocDerivacion > 0) {
                if (confirm) {
                    confirmDevolver = false;
                    $('.p-desc-confirmacion').html('¿Está seguro de devolver el documento?');
                    $('#ptv-alert-confirmacion').modal('open');
                }
            } else {
                var url = urlBase + "/Documento/ValidarDevolverDocumento";
                //var url = document.location.origin + "/Documento/ValidarDevolverDocumento";
                url += "?codDocumento=" + _codDocumentoVER;
                $.ajax({
                    type: 'GET',
                    enctype: 'multipart/form-data',
                    url: url,
                    async: false,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (result) {
                        if (result.Resultado === 1) {
                            if (result.Informacion !== null && result.Informacion.length > 0) {
                                confirmDevolver = false;
                                $('.p-desc-confirmacion').html("Existen áreas pendientes de atender ¿Desea continuar con la  devolución?<br/>" + result.Informacion);
                                $('#ptv-alert-confirmacion').modal('open');
                            }
                            else {
                                if (confirmDevolver) {
                                    confirmDevolver = false;
                                    $('.p-desc-confirmacion').html('¿Documento sin derivaciones pendientes, está seguro de devolver el documento?');
                                    $('#ptv-alert-confirmacion').modal('open');
                                }
                            }
                        }
                    },
                    complete: function () {
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            }

        };

        var RegistrarDevolucionAjax = function () {


            var request = new Object();
            request.comentario = new Object();
            request.comentario.CodDocumento = $("#hdCodDocumentoDevolver").val();
            request.comentario.Comentario = $("#txtDevolverComentario").val();
            request.comentario.CodDocDerivacion = codDocDerivacion;

            if (codDocDerivacion > 0) {
                EjecutarAjax("POST", "/Documento/DevolverDocumento", request, function () {
                    /*window.location.href = window.location.origin + "/BandejaEntrada";*/
                    //window.location.href = window.location.origin + window.location.pathname;
                    window.location.href = window.location.origin + window.location.pathname;
                }, "El documento fue devuelto satisfactoriamente");
            } else {
                EjecutarAjax("POST", "/Documento/DevolverDocumentoInterno", request, function () {
                    /*window.location.href = window.location.origin + "/BandejaEntrada";*/
                    window.location.href = window.location.origin + window.location.pathname;
                    //window.location.href = window.location.origin + window.location.pathname;
                }, "El documento fue devuelto satisfactoriamente");
            }
            return false;
        }

        var btnGuardarClick = function (event) {

            if ($('#frmNuevoDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmNuevoDocumento')[0].checkValidity()) {
                return true;
            }
            validarDestino();
            if (confirm) {
                validarJefeAsignadoAjax("Registrar");
            }

            if (confirm) {
                confirm = false;
                $('.p-desc-confirmacion').text('¿Está seguro de registrar el documento?');
                $('#ptv-alert-confirmacion').modal('open');
            }
        };

        var validarDestino = function () {
            var codAreaSeleccionadas = [];
            codAreaSeleccionadas.push(((hdCodArea.val() + '|').concat(hdCodAreaCC.val())).split('|'));

            for (var x = 0; x < codAreaSeleccionadas.length; x++) {
                if (codAreaSeleccionadas[x] != "") {
                    if (codAreaSeleccionadas[x].indexOf($("#hdnCodAreaLogin").val()) != -1) {

                        $('.p-desc-error').html("No puede enviar el documento al área a la que pertenece");
                        $('#ptv-alert-error').modal('open');
                        confirm = false;
                    } else {
                        confirm = true;
                    }
                }
            }
        };

        var validarDestinoEdit = function () {
            var codAreaSeleccionadas = [];
            codAreaSeleccionadas.push((($("#hdCodAreaEdit").val() + '|').concat($("#hdCodAreaCCEdit").val())).split('|'));

            for (var x = 0; x < codAreaSeleccionadas.length; x++) {
                if (codAreaSeleccionadas[x] != "") {
                    if (codAreaSeleccionadas[x].indexOf($("#hdnCodAreaLogin").val()) != -1) {

                        $('.p-desc-error').html("No puede enviar el documento al área a la que pertenece");
                        $('#ptv-alert-error').modal('open');
                        confirmEdit = false;
                    } else {
                        confirmEdit = true;
                    }
                }
            }
        };

        var btnRegistrarAvanceClick = function (event) {
            var mensaje = "";
            
            $.ajax({
                url: urlBase + "/Documento/GetEstadoDocumento?codDocumento=" + $("#hdCodDocumento_RA").val(),
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        if (result.data == 3) {
                            mensaje = "El documento principal se encuentra anulado, no puede registrar el avance.";
                        }
                    }
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            if ($("#comentario_RA").val() == "") {
                mensaje = "Debe ingresar un comentario.";
            }
            if (($("#comentario_RA").val().length < 15 || $("#comentario_RA").val().length > 200) && mensaje.length == 0) {
                mensaje = "El comentario no debe ser menor que 15 y mayor a 200 caracteres.";
            }
            if (mensaje.length > 0) {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text(mensaje);
                $('#ptv-alert-error').modal('open');
                return false;
            }

            if ($('#frmRegistrarAvance')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmRegistrarAvance')[0].checkValidity()) {
                return true;
            }
            if (confirm) {
                confirmRegistrarAvance = false;
                $('.p-desc-confirmacion').html('¿Está seguro de registrar avance?');
                $('#ptv-alert-confirmacion').modal('open');
            }
        };

        var validarJefeAsignadoAjax = function (tipo) {
            var codAreas = [];
            var stringMsg = "";
            if (tipo == "Registrar") {
                codAreas.push(((hdCodArea.val() + '|').concat(hdCodAreaCC.val())).split('|'));
            }
            if (tipo == "Editar") {
                codAreas.push((($("#hdCodAreaEdit").val() + '|').concat($("#hdCodAreaCCEdit").val())).split('|'));
            }


            for (var i = 0; i < codAreas[0].length; i++) {
                if (codAreas[0][i] != "") {
                    var url = urlBase  + "/Trabajador/ListaTrabajadorPorArea";
                    url += "?codArea=" + codAreas[0][i] + "&siEsJefe=" + 1;
                    $.ajax({
                        type: 'GET',
                        enctype: 'multipart/form-data',
                        url: url,
                        async: false,
                        processData: false,
                        contentType: false,
                        cache: false,
                        timeout: 600000,
                        success: function (result) {
                            if (result.length == 0) {
                                var empresa = "";
                                for (var x = 0; x < ResultAreas.length; x++) {
                                    //if (((codAreas[0][i]).indexOf(ResultAreas[x].codigoArea)) != -1) {
                                    if (((ResultAreas[x].codigoArea.toString()).indexOf(codAreas[0][i])) != -1) {
                                        empresa = ResultAreas[x].descripcionArea;
                                        break;
                                    }
                                }

                                var str = 'El área ' + empresa + ' no tiene jefe asignado </br>';
                                if (stringMsg.length > 0) {
                                    var e = (str.indexOf(stringMsg));
                                    if (e != -1) {
                                        str = "";
                                    }
                                }
                                stringMsg = stringMsg.concat(str);
                            }
                        },
                        complete: function () {
                        },
                        error: function (e, b, c) {
                            console.log(e.responseText);
                        }
                    });
                }

            }
            if (tipo == "Registrar" && stringMsg.length > 0) {
                confirm = false;

                $('.p-desc-error').html(stringMsg);
                $('#ptv-alert-error').modal('open');
            } else if (tipo == "Registrar" && stringMsg.length == 0) {
                validarAdjuntos();
            }

            if (tipo == "Editar" && stringMsg.length > 0) {
                confirmEdit = false;
                $('.p-desc-error').html(stringMsg);
                $('#ptv-alert-error').modal('open');
            } else if (tipo == "Editar" && stringMsg.length == 0) {
                validarAdjuntosEdit();
            }

        };

        var registrarDocumentoAjax = function () {
            // Get form
            var form = $('#frmNuevoDocumento')[0];
            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelar").prop("disabled", true);
            $("#modal_loading_agro").attr("style", "display:block");
            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: urlBase + "/BandejaEntrada/RegistrarDocumento",
                //url: "/BandejaEntrada/RegistrarDocumento",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (result) {

                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                             //   window.location.href = window.location.origin + "/BandejaEntrada";
                                window.location.href = window.location.origin + window.location.pathname;
                            }
                        });
                        $('.p-descripcion').text("El documento se grabó correctamente");
                        $('.p-otradescripcion').text("N° Trámite " + result.correlativo );
                        $('.p-otradescripcion2').text("Por favor proceder a firmar el documento para culminar el proceso de derivación");
                        $('#ptv-alert-ok').modal('open');
                        $(this).prop("disabled", false);
                    }

                    if (result.Resultado !== 1) {
                        $('#ptv-alert-error').modal({
                            onCloseEnd: function () {
                                window.location.href = window.location.origin + window.location.pathname;
                            }
                        });
                        $('.p-desc-error').text("Hubo problemas en el registro.");
                        $('#ptv-alert-error').modal('open');
                        $(this).prop("disabled", false);
                    }

                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                },
                error: function (e, b, c) {
                    $('#ptv-alert-error').modal({
                        onCloseEnd: function () {
                            location.reload();
                       }
                    });
                    $('.p-desc-error').text("Hubo problemas en la aplicación.");
                    $('#ptv-alert-error').modal('open');
                    $(this).prop("disabled", false);
                    console.log(e.responseText);
                }
            });
        };

        var registrarFinalizarAjax = function () {
            $("#modal_loading_agro").attr("style", "display:block");
            $("#codResponsableFinalizar").val(codRepresentandoFinalizar);
            $("#OrigenFinalizar").val(OrigenFinalizar);
            // Get form
            var form = $('#frmFinalizarDocumento')[0];

            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelar_RA").prop("disabled", true);


            functionEstadosDocAjax($(this), urlBase+"/BandejaEntrada/FinalizarDocumentoBandejaEntrada", data,
            //functionEstadosDocAjax($(this),"/BandejaEntrada/FinalizarDocumentoBandejaEntrada",data,
                function (result) {
                    if (result.Resultado === 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                               // window.location.href = window.location.origin + "/BandejaEntrada";
                                window.location.href = window.location.origin + window.location.pathname;
                            }
                        });
                        if (result.ResultadoCorreo > 0) {
                            $('.p-descripcion').text("Se finalizó el documento satisfactoriamente");
                        } else if (result.ResultadoCorreo <= 0) {
                            $('.p-descripcion').text("No se envió el correo, pero el documento ha sido finalizado satisfactoriamente");
                        } else {
                            $('.p-descripcion').text("Se finalizó el documento satisfactoriamente");
                        }
                        
                        $('#ptv-alert-ok').modal('open');
                    }
                    else {
                        $('.p-desc-error').text("Hubo problemas en la aplicación.");
                        $('#ptv-alert-error').modal('open');
                    }
                });
        };

        var functionEstadosDocAjax = function (obj, url, data, function_ok) {
            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (result) {
                    function_ok(result);
                    obj.prop("disabled", false);
                    $("#btnCancelar_RA").prop("disabled", false);
                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                    $("#btnCancelar_RA").prop("disabled", false);
                },
                error: function (e, b, c) {
                    $('#ptv-alert-error').modal({
                        onCloseEnd: function () {
                            //window.location.href = window.location.origin + "/BandejaEntrada";
                        }
                    });
                    $('.p-desc-error').text("Hubo problemas en la aplicación.");
                    $('#ptv-alert-error').modal('open');
                    console.log(e.responseText);
                    $("#btnCancelar_RA").prop("disabled", false);
                }
            });
        };

        var registrarAvanceDocumentoAjax = function () {
            $("#modal_loading_agro").attr("style", "display:block");
            // Get form
            var form = $('#frmRegistrarAvance')[0];

            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelar_RA").prop("disabled", true);


            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: urlBase+ "/BandejaEntrada/RegistrarAvance",
               // url: "/BandejaEntrada/RegistrarAvance",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (result) {

                    if (result.Resultado === 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                              //  window.location.href = window.location.origin + "/BandejaEntrada";
                                window.location.href = window.location.origin + window.location.pathname;
                            }
                        });
                        if (result.ResultadoCorreo > 0) {
                            $('.p-descripcion').text("Se registró el avance correctamente");
                        } else if (result.ResultadoCorreo <= 0) {
                            $('.p-descripcion').text("No se pudo enviar el correo,pero se registró el avance correctamente");
                        } else {
                            $('.p-descripcion').text("Se registró el avance correctamente");
                        }
                       
                        $('#ptv-alert-ok').modal('open');
                    }

                    if (result.Resultado !== 1) {
                        $('#ptv-alert-error').modal({
                            onCloseEnd: function () {
                                //window.location.href = window.location.origin + "/BandejaEntrada";
                            }
                        });
                        $('.p-desc-error').text("Hubo problemas en el registro.");
                        $('#ptv-alert-error').modal('open');
                    }

                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                },
                error: function (e, b, c) {
                    $('#ptv-alert-error').modal({
                        onCloseEnd: function () {
                            //window.location.href = window.location.origin + "/BandejaEntrada";
                        }
                    });
                    $('.p-desc-error').text("Hubo problemas en la aplicación.");
                    $('#ptv-alert-error').modal('open');
                    console.log(e.responseText);
                }
            });
        };

        var editarDocumentoAjax = function () {
            $("#modal_loading_agro").attr("style", "display:block");
            // Get form
            var form = $('#frmEditDocumento')[0];

            // Create an FormData object 
            var data = new FormData(form);
            $(this).prop("disabled", true);
            $("#btnCancelarEdit").prop("disabled", true);


            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: urlBase + "/BandejaEntrada/EditarDocumento",
                //url: "/BandejaEntrada/EditarDocumento",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (result) {

                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                              //  window.location.href = window.location.origin + "/BandejaEntrada";
                                //window.location.href = window.location.origin + window.location.pathname;
                                window.location.href = window.location.origin + window.location.pathname;
                            }
                        });
                        $('.p-descripcion').text("El documento se actualizó correctamente");
                        $('#ptv-alert-ok').modal('open');
                    }

                    if (result.Resultado != 1) {
                        $('#ptv-alert-error').modal({
                            onCloseEnd: function () {
                            //    window.location.href = window.location.origin + "/BandejaEntrada";
                                //window.location.href = window.location.origin + window.location.pathname;
                                window.location.href = window.location.origin + window.location.pathname;
                            }
                        });
                        $('.p-desc-error').text("Hubo problemas en el registro.");
                        $('#ptv-alert-error').modal('open');
                    }

                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                },
                error: function (e, b, c) {
                    $('#ptv-alert-error').modal({
                        onCloseEnd: function () {
                            //window.location.href = window.location.origin + "/BandejaEntrada";
                        }
                    });
                    $('.p-desc-error').text("Hubo problemas en la aplicación.");
                    $('#ptv-alert-error').modal('open');
                    console.log(e.responseText);
                }
            });
        }

        var validarAdjuntosEdit = function () {
            var strValidarAdjuntos = "";
            if (inputAnexoEdit.get(0).files.length == 0
                && (inputAdjuntoEdit.get(0).files.length) == 0 && $("#AnexosIngresados")[0].children.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto y anexos?'
                confirmEdit = false;
            } else if (inputAnexoEdit.get(0).files.length == 0 && $("#AnexosIngresados")[0].children.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin anexos?'
                confirmEdit = false;
            } else if (inputAdjuntoEdit.get(0).files.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto?'
                confirmEdit = false;
            } else {
                confirmEdit = true;
            }
            if (confirmEdit == false) {
                $('.p-desc-confirmacion').text(strValidarAdjuntos);
                $('#ptv-alert-confirmacion').modal('open');
            }
        }

        var validarAdjuntos = function () {
            var strValidarAdjuntos = "";
            if (inputAnexo.get(0).files.length == 0
                && (inputAdjunto.get(0).files.length) == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto y anexos?'
                confirm = false;
            } else if (inputAnexo.get(0).files.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin anexos?'
                confirm = false;
            } else if (inputAdjunto.get(0).files.length == 0) {
                strValidarAdjuntos = '¿Está seguro de guardar el documento sin adjunto?'
                confirm = false;
            } else {
                confirm = true;
            }
            if (confirm == false) {
                $('.p-desc-confirmacion').text(strValidarAdjuntos);
                $('#ptv-alert-confirmacion').modal('open');
            }
        }

        var btnEditClick = function (event) {

            if ($('#frmEditDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmEditDocumento')[0].checkValidity()) {
                return true;
            }
            validarDestinoEdit();
            if (confirmEdit) {
                validarJefeAsignadoAjax("Editar");
            }

            if (confirmEdit == false) {
                return false;
            }

            if (confirmEdit) {
                confirmEdit = false;
                $('.p-desc-confirmacion').text('¿Está seguro de actualizar el documento?');
                $('#ptv-alert-confirmacion').modal('open');
            }
        };

        var btnCancelarEditClick = function () {
          
            debugger
            $('#ptv-DocumentoMesaParteseEdit').modal('close');
            return false;
        }

        var btnCancelarModalDerivarClick = function () {

            debugger
            $('#ptv-DerivarDocumento').modal('close');
            return false;
        };

        var btnCancelarClick = function () {
          
            debugger
            $('#modal-nuevo').modal('close');
            return false;
        };

        var btnAgreeClick = function () {
            if (!confirm) {
                registrarDocumentoAjax();
                confirm = true;
            }
            else if (!confirmEdit) {
                editarDocumentoAjax();
                confirmEdit = true;
            }
            else if (!confirmRegistrarAvance) {
                registrarAvanceDocumentoAjax();
                confirmRegistrarAvance = true;
            }
            else if (!confirmFinalizar) {
                registrarFinalizarAjax();
                confirmFinalizar = true;
            } else if (!confirmDerivar) {
                RegistrarDerivarDocumentoAjax();
                confirmDerivar = true;
            } else if (!confirmDevolver) {
                RegistrarDevolucionAjax();
                confirmDevolver = true;
            }
        };

        var adjuntoChange = function (event) {

            var file = event.target.files[0];
            var formatoOK = true;
            var tamanioOK = true;

            if (file == null)
                return;

            if (!file.type.match('application/pdf')) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("Solo se permiten archivos en formatos .PDF");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAdjuntoClick();
                formatoOK = false;
                //$("#form-id").get(0).reset(); //the tricky part is to "empty" the input file here I reset the form.
                return false;
            }

            if (file.size >= 5 * 1024 * 1024) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("Tamaño máximo permitido 5MB");
                $('#ptv-alert-error').modal('open');
                btnLimpiarAdjuntoClick();
                tamanioOK = false;
                return false;
            }

            if (formatoOK && tamanioOK) {
                $("#estadoDocumento").attr("data-badge-caption", "PENDIENTE");
                $("#estadoDocumentoEdit").attr("data-badge-caption", "PENDIENTE");
            }
        };

        /////////////////////////////////

        $(document).on("click", "#idAtenderDocumento", function () {

            confirmRegistrarAvance = true;
            codDocDerivacion = $(this).data('coddocderivacion');
            $("#frmRegistrarAvance")[0].reset();
            $("#comentario_RA").focus();
            var _codDocumento = $(this).data('id');
            var _codDocDerivacion = $(this).data('coddocderivacion');
            var _estadoDocumento = $(this).data('estadodocumento');
            $("#txtFinalizarComentario").val("");
            $("#hdCodDocumento_RA").val(_codDocumento);
            if (_codDocDerivacion > 0) {
                $("#hdCodDocDerivacion_RA").val(_codDocDerivacion);
                var urlDerivacion = urlBase + "/Documento/ObtenerDocumentoDetalleDerivacion";
                //var url = document.location.origin + "/Documento/ObtenerDocumento";
                urlDerivacion += "?CodDocumentoDer=" + _codDocDerivacion;
                $.ajax({
                    url: urlDerivacion,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,

                    success: function (result) {
                        if (result.data.TipoAccion != null) {
                            $("#mTipoAccion").text("Acción a realizar : ");
                            $("#mTipoAccionb").text(result.data.TipoAccion);
                        }
                     }
                });


            }

            $("#adjuntarAnexo_RA").on("change", validarUnArchivoAdjunto);

            var url = urlBase + "/Documento/ObtenerDocumento";
            //var url = document.location.origin + "/Documento/ObtenerDocumento";
            url += "?codDocumento=" + _codDocumento;
            $.ajax({
                url: url,
                dataType: "json",
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,

                success: function (result) {
                    $("#mNumDocumento_RA").text(result.data.NumDocumento);
                    $("#mCorrelativo_RA").html("Nro.Trámite : " + result.data.Correlativo);

                    var _stPri_Baja = '<span class="new badge icon-badge badge-blue badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">arrow_downward</i></span> <span class="c-blue prioridad-texto-detalle"><label class="label color-blue">Prioridad baja</label></span>';
                    var _stPri_Media = '<span class="new badge icon-badge badge-green badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">remove</i> </span> <span class="c-green prioridad-texto-detalle"><label class="label color-green">Prioridad normal</label></span>';
                    var _stPri_Alta = '<span class="new badge icon-badge badge-yellow badge-small" data-badge-caption=""><i class="material-icons icon-detalle">arrow_upward</i></span> <span class="c-yellow prioridad-texto-detalle"><label class="label color-yellow">Prioridad urgente</label></span>';
                    var _stPri_Urgente = '<span class="new badge icon-badge badge-pink badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">priority_high</i></span> <span class="c-pink prioridad-texto-detalle"><label class="label color-pink">Prioridad muy urgente</label></span>';
                    var _stPrioridad = _stPri_Baja;

                    switch (result.data.Prioridad) {
                        case 1: //baja
                            _stPrioridad = _stPri_Baja;
                            break;
                        case 2://media
                            _stPrioridad = _stPri_Media;
                            break;
                        case 3://alta
                            _stPrioridad = _stPri_Alta;
                            break;
                        case 4://urgente
                            _stPrioridad = _stPri_Urgente;
                            break;
                    }
                    $("#mPrioridad_RA").html(_stPrioridad);
                    $("#mEstadoDocumento_RA").html(fc_ObtenerEstadoDocumentoHtml(_estadoDocumento, true));
                }
            });
        });

        $(document).on("click", "#idVerDocumento", function () {   
            var _codDocumento = $(this).data('id');
            _codDocumentoVER = $(this).data('id');
            var origen = $(this).data('origen');
            codDocDerivacion = $(this).data('coddocderivacion');
            var _HabilitarFinalizar = parseBoolean($(this).data('habilitarfinalizar'));
            var _HabilitarDevolver = parseBoolean($(this).data('habilitardevolver'));
            var _estadoDocumento = $(this).data('estadodocumento'); 
            var _objArgumentNombreArchivo = "";
            var _objArgumentIdLaserfiche = "";
            var _derivacionFirmado = "";
            var url = urlBase + "/BandejaEntrada/VerDocumento";
//            var url = document.location.origin + "/BandejaEntrada/VerDocumento";
            cargarPagina(url, "#ptv-DocumentoMesaPartes", function () {

                var url = urlBase + "/Documento/ObtenerDocumento";
                //var url = document.location.origin + "/Documento/ObtenerDocumento";
                url += "?codDocumento=" + _codDocumento;

                $("#modal_loading_agro").attr("style", "display:block");
                $("#tbDetalle").attr("style", "display:none");
                $("#hdCodDocumentoAnular").val(_codDocumento);
                $("#hdCodDocumentoDevolver").val(_codDocumento);
                $("#hdCodDocumentoFinalizar").val(_codDocumento);
                $("#codDocumentoVer").val(_codDocumento);
                $("#codDocumentoDerivacionVer").val(codDocDerivacion);
                if (codDocDerivacion > 0) {
                    var urlDerivacion = urlBase + "/Documento/ObtenerDocumentoDetalleDerivacion";
                    //var url = document.location.origin + "/Documento/ObtenerDocumento";
                    urlDerivacion += "?CodDocumentoDer=" + codDocDerivacion;
                    $.ajax({
                        url: urlDerivacion,
                        dataType: "json",
                        type: "GET",
                        async: false,
                        contentType: 'application/json; charset=utf-8',
                        cache: false,
                        success: function (result) {
                            _derivacionFirmado = result.data.CodDocumentoFirma;
                            if (result.data.TipoAccion != null) {
                                $("#divaccion").show();
                                $("#mTipoAccionVer").html('<b>' + result.data.TipoAccion + '</b>');
                            }
                        }
                    });
                }

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {
                        var date = new Date(result.data.FechaPlazo);
                        var _dia = '00' + date.getDate().toString();
                        _dia = _dia.substring(_dia.length - 2, _dia.length);
                        var _mes = '00' + (date.getMonth() + 1).toString();
                        _mes = _mes.substring(_mes.length - 2, _mes.length);

                        var fPlazo = _dia + "/" + _mes + "/" + date.getFullYear();
                       
                        $("#mNumDocumento").text(result.data.NumDocumento);
                        $("#mCorrelativo").html("Nro.Trámite : " + result.data.Correlativo);
                        
                        if (result.data.Origen === 1) {
                            /*INTERNO*/
                            var firmante = fc_validarFirmante(result.data.CodDocumento, result.data.CodTrabajador);
                            $("#mNombreTrab").html('<div class="col s9 m3"><b>' + result.data.Area + '</b></DIV>' + ((firmante == true) ? "<div class='col s1 m1'><img src='" + urlBase + "/Content/img/icon-firma.png' class='responsive-img logo-mini tooltipped' data-position='right' data-tooltip='Firma Realizada'></div>" : "") + '</div><div class="col s2 m2"></div></br></br><div class="col s10 m7"><span style="font-size: 11px;">Responsable del cierre de la atención del trámite.</span>');
                            /*responsable documento interno*/
                            $("#codResponsable").val(result.data.CodResponsalbe);

                        }
                        else {
                            /*EXTERNO*/
                            $("#mNombreTrab").html(result.data.CodEmpresa + ' ' + result.data.DescripcionEmpresa + " - " + result.data.CodRepresentante + ' ' + result.data.Representante);
                            $("#codResponsable").val(result.data.CodTrabajador);
                        }

                        origenVerDocumento = result.data.Origen;
                        $("#mOrigen").html(result.data.Origen === 1 ? "INTERNO" : "EXTERNO");
                        $("#mAnio").html(result.data.Anio);
                        $("#mFechaPlazo").html(fPlazo + ' (' + result.data.Plazo + ' días)');


                        $("#mEstadoDocumento").html(fc_ObtenerEstadoDocumentoHtml(result.data.EstadoDocumento, true));

                        var _stPri_Baja = '<span class="new badge icon-badge badge-blue badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">arrow_downward</i></span> <span class="c-blue prioridad-texto-detalle"><label class="label color-blue">Prioridad baja</label></span>';
                        var _stPri_Media = '<span class="new badge icon-badge badge-green badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">remove</i> </span> <span class="c-green prioridad-texto-detalle"><label class="label color-green">Prioridad normal</label></span>';
                        var _stPri_Alta = '<span class="new badge icon-badge badge-yellow badge-small" data-badge-caption=""><i class="material-icons icon-detalle">arrow_upward</i></span> <span class="c-yellow prioridad-texto-detalle"><label class="label color-yellow">Prioridad urgente</label></span>';
                        var _stPri_Urgente = '<span class="new badge icon-badge badge-pink badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">priority_high</i></span> <span class="c-pink prioridad-texto-detalle"><label class="label color-pink">Prioridad muy urgente</label></span>';
                        var _stPrioridad = _stPri_Baja;

                        switch (result.data.Prioridad) {
                            case 1: //baja
                                _stPrioridad = _stPri_Baja;
                                break;
                            case 2://media
                                _stPrioridad = _stPri_Media;
                                break;
                            case 3://alta
                                _stPrioridad = _stPri_Alta;
                                break;
                            case 4://urgente
                                _stPrioridad = _stPri_Urgente;
                                break;
                        }
                        $("#mPrioridad").html(_stPrioridad);

                        $("#mFechaRecepcion").html(formatfecha(result.data.FechaRecepcion));
                        OrigenFinalizar = result.data.Origen;
                        if (result.data.Origen == 1) {
                            codRepresentandoFinalizar = result.data.CodResponsalbe;
                        } else {
                            codRepresentandoFinalizar = result.data.CodTrabajador;
                        }
                        
                        $("#mRepresentante").html(result.data.UsuarioRegistro);//result.data.NombreTrab + ' ' + result.data.ApePaternoTrab + ' ' + result.data.ApeMaternoTrab);
                        if (result.data.Origen === 1) {
                            /*INTERNO*/
                        }
                        else {
                            /*EXTERNO*/
                            var firmante = fc_validarFirmante(result.data.CodDocumento, result.data.CodTrabajador);
                            $("#DestinatarioTO").html('<div class="col s10 m7">' + result.data.Area + ' - ' + result.data.NombreTrab + ' ' + result.data.ApePaternoTrab + ' ' + result.data.ApeMaternoTrab +
                                '</div><div class="col s2 m2"><span class="new badge badge-green" data-badge-caption="TO"></span></div>' + ((firmante == true) ? "<div class='col s1 m1'><img src='" + urlBase + "/Content/img/icon-firma.png' class='responsive-img logo-mini tooltipped' data-position='right' data-tooltip='Firma Realizada'></div>" : ""));
                            
                        }
                        if (_derivacionFirmado > 0) {
                            _derivacionFirmado = _derivacionFirmado;
                        } else if (_derivacionFirmado == 0 && codDocDerivacion==0) {
                            _derivacionFirmado = result.data.DocumentoFirmado;
                        }
                       
                        $("#DestinatarioCC").html(fc_ObtenerDestinatariosCC(result.data.LstDocDerivacion));
                        $("#mAsunto").html(result.data.Asunto);
                        $("#mReferencia").html(result.data.Referencia === null ? "" : result.data.Referencia);
                        $("#mFechaDocumento").html(formatfecha(result.data.FechaDocumento));
                        $("#mObservaciones").html(result.data.Observaciones === null ? "" : result.data.Observaciones);

                        var laserFicher = result.data.ArchivosLaserFiche;
                        var htmlLaserFicher = '', htmlLaserFicherAnexo = '', htmlLaserFicherAnexoAdjunto = '';
                        var htmlBtZIP = '';

                        var iniAnexo = 0;
                        var htmlBtn = "";
                        var cc = 0;
                        if (result.data.LstDocAdjunto.length > 0) {
                            if (laserFicher !== null) {
                                cc = 0;
                                for (var i = 0; i < 1; i++) {
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtn = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicherDoc?CodDocument=" + _codDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicher = htmlLaserFicher + htmlBtn;
                                        cc++;
                                        _objArgumentNombreArchivo = laserFicher.ListaDocumentos[i].NombreArchivo;
                                        _objArgumentIdLaserfiche = laserFicher.ListaDocumentos[i].IdDocumento;
                                    }

                                }
                                iniAnexo = 1;
                            }
                        }
                        if (laserFicher != null) {
                            for (var i = iniAnexo; i < laserFicher.ListaDocumentos.length; i++) {
                                var cc = 0;
                                var htmlBtnAnexo = "";
                                if (laserFicher.ListaDocumentos[i].TipoArchivo === 2) {
                                    /*CAMBIAR POR ANEXOS - ADJUNTOS*/
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtnAnexo = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicherAnexoAdjunto = htmlLaserFicherAnexoAdjunto + htmlBtnAnexo;
                                        cc++;
                                    }
                                    if (cc > 1) {
                                        htmlBtZIP = '<a class="a-link" href=' + "'/BandejaEntrada/ExportarArchivosZIP?codDocumento=" + _codDocumento + "'" + ';">' +
                                            '<i class="material-icons prefix">description</i>' +
                                            '<span>Descargar Todo</span></a>';

                                    }
                                }
                                else {
                                    if (laserFicher.ListaDocumentos[i].TipoArchivo === 1) {

                                        /*CAMBIAR POR ANEXOS*/
                                        if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                            htmlBtnAnexo = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                                '	<i class="material-icons prefix">description</i>' +
                                                '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                                '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                                '</button>   ';
                                            htmlLaserFicherAnexo = htmlLaserFicherAnexo + htmlBtnAnexo;
                                            cc++;
                                        }

                                        if (cc > 1) {
                                            htmlBtZIP = '<a class="a-link" href=' + "'/BandejaEntrada/ExportarArchivosZIP?codDocumento=" + _codDocumento + "'" + ';">' +
                                                '<i class="material-icons prefix">description</i>' +
                                                '<span>Descargar Todo</span></a>';
                                        }
                                    }                                    
                                }
                            }
                        }

                        $("#mDescargarTodos").html(htmlBtZIP);
                        $("#mArchivosLaserFiche").html(htmlLaserFicher);
                        $("#mArchivosLaserFicheAnexos").html(htmlLaserFicherAnexo);
                        $("#mArchivosLaserFicheAnexosAdjuntos").html(htmlLaserFicherAnexoAdjunto);

                        var htmBtmAnular = "";
                        var htmlBtnDevolver = "";
                        var htmlBtnFinalizar = "";
                        var htmlFirmar = "";
                        var htmlBtnPDFCargoRecep = "";
                        var htmlBtnPDFHojaResumen = "";

                        if (result.data.EstadoDocumento === 1)//igual a ingresado a anula
                        {
                            htmBtmAnular = '<button id="btnAnularForm" data-target="modal-confirm-anular" class="modal-trigger btn waves-effect waves-light btn-warning modal-action" >' +
                                '<span style="font-size: 80%">Anular</span>' +
                                '<i class="material-icons prefix">delete</i>' +
                                '</button>  ';
                        }
                        
                        if (result.data.EstadoDocumento != 1 && result.data.EstadoDocumento != 3) {
                            htmlBtnPDFCargoRecep = '<button class="btn waves-effect waves-light btn-warning"  type="submit" name="action" onclick= "location.href=' + "'" + urlBase + "/Documento/ExportarPDF_CargoRecepcion?codDocumento=" + _codDocumento + "'" + ';" name="action">' +
                                '<span style="font-size: 80%">Hoja de Cargo</span>' +
                                '<i class="material-icons prefix">description</i>' +
                                '</button>  ';
                        }

                        if (result.data.EstadoDocumento != 3) {
                            htmlBtnPDFHojaResumen = '<button class="btn waves-effect waves-light btn-warning" type="submit" name="action" onclick= "location.href=' + "'" + urlBase + "/Documento/ExportarPDF_HojaResumen?codDocumento=" + _codDocumento + "'" + ';" name="action">' +
                                '<span style="font-size: 80%">Hoja de Resumen</span>' +
                                '<i class="material-icons prefix">navigation</i>' +
                                '</button>   ';
                        }

                        if (_HabilitarDevolver === true && _derivacionFirmado == 0 && result.data.EstadoDocumento != 3)//Documento asignado o Documento derivado
                        {
                            htmlBtnDevolver = '<button id="btnDevolverForm" data-target="modal-confirm-devolver" class="modal-trigger btn waves-effect waves-light btn-warning modal-action">' +
                                '<span style="font-size: 80%">Devolver</span>' +
                                '<i class="material-icons prefix">send</i>' +
                                '</button>   ';
                        }

                        if (_HabilitarFinalizar === true && result.data.DocumentoFirmado > 0 && result.data.EstadoDocumento != 3)//Documento asignado
                        {
                            htmlBtnFinalizar = '<button id="btnFinalizarForm" data-target="modal-confirm-finalizar" class="modal-trigger btn waves-effect waves-light btn-warning modal-action">' +
                                '<span style="font-size: 80%">Finalizar</span>' +
                                '<i class="material-icons prefix">check</i>' +
                                '</button>';
                        }

                        if (result.data.siFirma === 1 && result.data.EstadoDocumento != 3)//Documento asignado
                        {
                            var esCC = true;
                            //validamos que el usuario no sea CC
                            for (var x = 0; x < $(".divcc").length; x++) {
                                if ($(".divcc")[x].innerText == $("#hdnWebUsr").val()) {
                                    esCC = false;
                                    break;
                                }
                            }

                            if (esCC) {
                                //INTERNRO - PROCESO - PENDIENTE
                                if (result.data.Origen == 1 && (result.data.EstadoDocumento == 6 || result.data.EstadoDocumento == 2) && _estadoDocumento!=7) {
                                    htmlFirmar = '<button onclick="IniciarProcesoFirma( \'' + _objArgumentNombreArchivo + '\',\'' + _objArgumentIdLaserfiche + '\');"  class="btn waves-effect waves-light btn-warning">' +
                                        '<span style="font-size: 80%">Firmar</span>' +
                                        '<i class="material-icons prefix">verified_user</i>' +
                                        '</button>';
                                    //EXTERNO EN PROCESO
                                } else if (result.data.Origen == 2 && (result.data.EstadoDocumento == 6 || result.data.EstadoDocumento == 2) && _estadoDocumento != 7) {
                                    htmlFirmar = '<button onclick="IniciarProcesoFirma( \'' + _objArgumentNombreArchivo + '\',\'' + _objArgumentIdLaserfiche + '\');" class="btn waves-effect waves-light btn-warning">' +
                                        '<span style="font-size: 80%">Firmar</span>' +
                                        '<i class="material-icons prefix">verified_user</i>' +
                                        '</button>';
                                }
                            }


                        }

                        var htmlBtns = htmlFirmar+ htmlBtnDevolver + htmlBtnFinalizar + htmBtmAnular + htmlBtnPDFCargoRecep + htmlBtnPDFHojaResumen ;

                        $("#mSeccionExportarPDF").html(htmlBtns);

                        $("#btnAnularForm,#btnFinalizarForm,#btnDevolverForm").click(function () {
                            document.getElementById("frmFinalizarDocumento").reset();
                            document.getElementById("frmDevolverDocumento").reset();
                            document.getElementById("frmAnularDocumento").reset();
                        });

                    },
                    complete: function () {
                        $('#modal_loading_agro').attr("style", "display:none");
                        $("#tbDetalle").attr("style", "display:block");

                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });

                var urlPestaniaSeguimiento = "";
                if (origen == 1) {
                    urlPestaniaSeguimiento =urlBase + "/Documento/GetSeguimientoInterno";
                    //urlPestaniaSeguimiento = document.location.origin + "/Documento/GetSeguimientoInterno";
                    urlPestaniaSeguimiento += "?codDocumento=" + _codDocumento;
                } else {
                    urlPestaniaSeguimiento = urlBase + "/Documento/GetSeguimiento";
                    //urlPestaniaSeguimiento = document.location.origin + "/Documento/GetSeguimiento";
                    urlPestaniaSeguimiento += "?codDocumento=" + _codDocumento + "&origen=" + 2;
                }
                $.ajax({
                    url: urlPestaniaSeguimiento,
                    //dataType: "json",
                    type: "GET",
                    //contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {
                        $("#tbSeguimientoCabBE").html(result);
                        $("#ptv-DocumentoMesaPartes").find('.collapsible').collapsible();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(ajaxOptions + ":" + thrownError);
                    }
                })

                var elem = $('.tabs');
                elem.tabs();
                var instance_tabs = M.Tabs.getInstance(elem);
                instance_tabs.select("tbDetalle");
            });
        });

        $(document).on("click", "#idVerDocumentoSIED", function () {
            var _codDocumento = $(this).data('id');
            _codDocumentoVER = $(this).data('id');
            var origen = $(this).data('origen');
            codDocDerivacion = $(this).data('coddocderivacion');
            var _HabilitarFinalizar = parseBoolean($(this).data('habilitarfinalizar'));
            var _HabilitarDevolver = parseBoolean($(this).data('habilitardevolver'));
            var _estadoDocumento = $(this).data('estadodocumento');
            var _objArgumentNombreArchivo = "";
            var _objArgumentIdLaserfiche = "";
            var _derivacionFirmado = "";
            var url = urlBase + "/BandejaEntrada/VerDocumentoSIED";
            //            var url = document.location.origin + "/BandejaEntrada/VerDocumento";
            cargarPagina(url, "#ptv-VerDocumentoBESIED", function () {

                //var url = urlBase + "/Documento/ObtenerDocumento";
                var url = urlBase + "/BandejaSIED/ObtenerDocumentoSIED";

                url += "?codDocumento=" + _codDocumento;

                $("#modal_loading_agro").attr("style", "display:block");
                $("#tbDetalleSIED").attr("style", "display:none");
                $("#hdCodDocumentoAnularSIED").val(_codDocumento);
                $("#hdCodDocumentoDevolverSIED").val(_codDocumento);
                $("#hdCodDocumentoFinalizarSIED").val(_codDocumento);
                $("#codDocumentoVerSIED").val(_codDocumento);
                $("#codDocumentoDerivacionVerSIED").val(codDocDerivacion);
                if (codDocDerivacion > 0) {
                    var urlDerivacion = urlBase + "/Documento/ObtenerDocumentoDetalleDerivacion";
                    //var url = document.location.origin + "/Documento/ObtenerDocumento";
                    urlDerivacion += "?CodDocumentoDer=" + codDocDerivacion;
                    $.ajax({
                        url: urlDerivacion,
                        dataType: "json",
                        type: "GET",
                        async: false,
                        contentType: 'application/json; charset=utf-8',
                        cache: false,
                        success: function (result) {
                            _derivacionFirmado = result.data.CodDocumentoFirma;
                            if (result.data.TipoAccion != null) {
                                $("#divaccionSIED").show();
                                $("#mTipoAccionVerSIED").html('<b>' + result.data.TipoAccion + '</b>');
                            }
                        }
                    });
                }

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {
                        var date = new Date(result.data.FechaPlazo);
                        var _dia = '00' + date.getDate().toString();
                        _dia = _dia.substring(_dia.length - 2, _dia.length);
                        var _mes = '00' + (date.getMonth() + 1).toString();
                        _mes = _mes.substring(_mes.length - 2, _mes.length);

                        var fPlazo = _dia + "/" + _mes + "/" + date.getFullYear();

                        $("#mNumDocumentoSIED").text(result.data.NumDocumento);
                        $("#mCorrelativoSIED").html("Nro.Trámite : " + result.data.Correlativo);


                        if (result.data.Origen === 1) {
                            /*INTERNO*/
                            var firmante = fc_validarFirmante(result.data.CodDocumento, result.data.CodTrabajador);
                            $("#mNombreTrabSIED").html('<div class="col s9 m3"><b>' + result.data.Area + '</b></DIV>' + ((firmante == true) ? "<div class='col s1 m1'><img src='" + urlBase + "/Content/img/icon-firma.png' class='responsive-img logo-mini tooltipped' data-position='right' data-tooltip='Firma Realizada'></div>" : "") + '</div><div class="col s2 m2"></div></br></br><div class="col s10 m7"><span style="font-size: 11px;">Responsable del cierre de la atención del trámite.</span>');
                            /*responsable documento interno*/
                            $("#codResponsableSIED").val(result.data.CodResponsalbe);

                        }
                        else {
                            /*EXTERNO*/

                            $("#codResponsableSIED").val(result.data.CodTrabajador);
                        }

                        $("#mNombreTrabSIED").html('<div class="col s10 m7">' + result.data.Area + ' - ' + result.data.NombreTrab + ' ' + result.data.ApePaternoTrab + ' ' + result.data.ApeMaternoTrab + '</div>');

                        origenVerDocumento = result.data.Origen;

                        var htmlOrigen = "SIED";

                        if (result.data.Origen === 3) {
                            htmlOrigen = "INTERNO " + htmlOrigen;
                        }

                        if (result.data.Origen === 4) {
                            htmlOrigen = "EXTERNO " + htmlOrigen;
                        }

                        $("#mOrigenSIED").html(htmlOrigen);
                        $("#mAnioSIED").html(result.data.Anio);
                        $("#mFechaPlazoSIED").html(fPlazo + ' (' + result.data.Plazo + ' días)');


                        $("#mEstadoDocumentoSIED").html(fc_ObtenerEstadoDocumentoHtml(result.data.EstadoDocumento, true));

                        var _stPri_Baja = '<span class="new badge icon-badge badge-blue badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">arrow_downward</i></span> <span class="c-blue prioridad-texto-detalle"><label class="label color-blue">Prioridad baja</label></span>';
                        var _stPri_Media = '<span class="new badge icon-badge badge-green badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">remove</i> </span> <span class="c-green prioridad-texto-detalle"><label class="label color-green">Prioridad normal</label></span>';
                        var _stPri_Alta = '<span class="new badge icon-badge badge-yellow badge-small" data-badge-caption=""><i class="material-icons icon-detalle">arrow_upward</i></span> <span class="c-yellow prioridad-texto-detalle"><label class="label color-yellow">Prioridad urgente</label></span>';
                        var _stPri_Urgente = '<span class="new badge icon-badge badge-pink badge-small" data-badge-caption=""> <i class="material-icons icon-detalle">priority_high</i></span> <span class="c-pink prioridad-texto-detalle"><label class="label color-pink">Prioridad muy urgente</label></span>';
                        var _stPrioridad = _stPri_Baja;

                        switch (result.data.Prioridad) {
                            case 1: //baja
                                _stPrioridad = _stPri_Baja;
                                break;
                            case 2://media
                                _stPrioridad = _stPri_Media;
                                break;
                            case 3://alta
                                _stPrioridad = _stPri_Alta;
                                break;
                            case 4://urgente
                                _stPrioridad = _stPri_Urgente;
                                break;
                        }
                        $("#mPrioridadSIED").html(_stPrioridad);

                        debugger;
                        var fechaRecepcion = "-";
                        if (result.data.FechaRecepcionSIED != null) {
                            fechaRecepcion = formatfecha(result.data.FechaRecepcionSIED);
                        }

                        $("#mFechaRecepcionSIED").html(fechaRecepcion);
                        OrigenFinalizar = result.data.Origen;
                        if (result.data.Origen == 1) {
                            codRepresentandoFinalizar = result.data.CodResponsalbe;
                        } else {
                            codRepresentandoFinalizar = result.data.CodTrabajador;
                        }

                        $("#mRepresentanteSIED").html(result.data.UsuarioRegistro);//result.data.NombreTrab + ' ' + result.data.ApePaternoTrab + ' ' + result.data.ApeMaternoTrab);

                        //if (result.data.Origen === 3) {
                        //    /*INTERNO*/
                        //}
                        //else {
                        //    /*EXTERNO*/
                        //    var firmante = fc_validarFirmante(result.data.CodDocumento, result.data.CodTrabajador);

                        //}

                        $("#DestinatarioTOSIED").html(fc_ObtenerDestinatariosSIED(result.data.LstDocDerivados));

                        if (_derivacionFirmado > 0) {
                            _derivacionFirmado = _derivacionFirmado;
                        } else if (_derivacionFirmado == 0 && codDocDerivacion == 0) {
                            _derivacionFirmado = result.data.DocumentoFirmado;
                        }

                        $("#DestinatarioCCSIED").html(fc_ObtenerDerivaciones(result.data.LstDocDerivacion));
                        $("#mAsuntoSIED").html(result.data.Asunto);
                        $("#mReferenciaSIED").html(result.data.Referencia === null ? "" : result.data.Referencia);
                        $("#mFechaDocumentoSIED").html(formatfecha(result.data.FechaDocumento));
                        $("#mObservacionesSIED").html(result.data.Observaciones === null ? "" : result.data.Observaciones);
                        if (result.data.Comentario.length > 0) {
                            $("#divAnulacionDocSIED").show();
                            $("#mAnulacionEnvioSIED").html(result.data.Comentario === null ? "" : result.data.Comentario);
                        }


                        var laserFicher = result.data.ArchivosLaserFiche;
                        var htmlLaserFicher = '', htmlLaserFicherAnexo = '', htmlLaserFicherAnexoAdjunto = '';
                        var htmlBtZIP = '';

                        var iniAnexo = 0;
                        var htmlBtn = "";
                        var cc = 0;
                        if (result.data.LstDocAdjunto.length > 0) {
                            if (laserFicher !== null) {
                                cc = 0;
                                for (var i = 0; i < 1; i++) {
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtn = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicherDoc?CodDocument=" + _codDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicher = htmlLaserFicher + htmlBtn;
                                        cc++;
                                        _objArgumentNombreArchivo = laserFicher.ListaDocumentos[i].NombreArchivo;
                                        _objArgumentIdLaserfiche = laserFicher.ListaDocumentos[i].IdDocumento;
                                    }

                                }
                                iniAnexo = 1;
                            }
                        }
                        if (laserFicher != null) {
                            for (var i = iniAnexo; i < laserFicher.ListaDocumentos.length; i++) {
                                var cc = 0;
                                var htmlBtnAnexo = "";
                                if (laserFicher.ListaDocumentos[i].TipoArchivo === 2) {
                                    /*CAMBIAR POR ANEXOS - ADJUNTOS*/
                                    if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                        htmlBtnAnexo = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                            '	<i class="material-icons prefix">description</i>' +
                                            '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                            '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                            '</button>   ';
                                        htmlLaserFicherAnexoAdjunto = htmlLaserFicherAnexoAdjunto + htmlBtnAnexo;
                                        cc++;
                                    }
                                    if (cc > 1) {
                                        htmlBtZIP = '<a class="a-link" href=' + "'/BandejaEntrada/ExportarArchivosZIP?codDocumento=" + _codDocumento + "'" + ';">' +
                                            '<i class="material-icons prefix">description</i>' +
                                            '<span>Descargar Todo</span></a>';

                                    }
                                }
                                else {
                                    if (laserFicher.ListaDocumentos[i].TipoArchivo === 1) {

                                        /*CAMBIAR POR ANEXOS*/
                                        if (!isNull(laserFicher.ListaDocumentos[i].NombreArchivo)) {
                                            htmlBtnAnexo = ' <button class="btn waves-effect waves-light btn-war" style="cursor: pointer;" onclick="location.href=' + "'" + urlBase + "/Documento/ExportarDocumentoLaserFicher?CodFiche=" + laserFicher.ListaDocumentos[i].IdDocumento + "'" + ';" name="action">' +
                                                '	<i class="material-icons prefix">description</i>' +
                                                '	<span>' + isNull(laserFicher.ListaDocumentos[i].NombreArchivo, 'Sin Nombre') + '</span> <br />' +
                                                '	<small>' + laserFicher.ListaDocumentos[i].Peso + ' KB </small>' +
                                                '</button>   ';
                                            htmlLaserFicherAnexo = htmlLaserFicherAnexo + htmlBtnAnexo;
                                            cc++;
                                        }

                                        if (cc > 1) {
                                            htmlBtZIP = '<a class="a-link" href=' + "'/BandejaEntrada/ExportarArchivosZIP?codDocumento=" + _codDocumento + "'" + ';">' +
                                                '<i class="material-icons prefix">description</i>' +
                                                '<span>Descargar Todo</span></a>';
                                        }
                                    }
                                }
                            }
                        }

                        $("#mDescargarTodosSIED").html(htmlBtZIP);
                        $("#mArchivosLaserFicheSIED").html(htmlLaserFicher);
                        $("#mArchivosLaserFicheAnexosSIED").html(htmlLaserFicherAnexo);
                        $("#mArchivosLaserFicheAnexosAdjuntosSIED").html(htmlLaserFicherAnexoAdjunto);

                        var htmBtmAnular = "", htmlBtnDevolver = "", htmlBtnFinalizar = ""; htmlFirmar = "";
                        var htmlBtnPDFCargoRecep = "";
                    /*    if (result.data.EstadoDocumento === 1)//igual a ingresado a anula
                        {
                            htmBtmAnular = '<button id="btnAnularForm" data-target="modal-confirm-anular" class="modal-trigger btn waves-effect waves-light btn-warning modal-action" >' +
                                '<span style="font-size: 80%">Anular</span>' +
                                '<i class="material-icons prefix">delete</i>' +
                                '</button>  ';
                        }*/

                       /* if (result.data.EstadoDocumento != 1) {
                            htmlBtnPDFCargoRecep = '<button class="btn waves-effect waves-light btn-warning"  type="submit" name="action" onclick= "location.href=' + "'" + urlBase + "/Documento/ExportarPDF_CargoRecepcion?codDocumento=" + _codDocumento + "'" + ';" name="action">' +
                                '<span style="font-size: 80%">Hoja de Cargo</span>' +
                                '<i class="material-icons prefix">description</i>' +
                                '</button>  ';
                        }*/

                        //var htmlBtnPDFHojaResumen = '<button class="btn waves-effect waves-light btn-warning" type="submit" name="action" onclick= "location.href=' + "'" + urlBase + "/Documento/ExportarPDF_HojaResumen?codDocumento=" + _codDocumento + "'" + ';" name="action">' +
                        //    '<span style="font-size: 80%">Hoja de Resumen</span>' +
                        //    '<i class="material-icons prefix">navigation</i>' +
                        //    '</button>   ';

                       /* if (_HabilitarDevolver === true && _derivacionFirmado == 0)//Documento asignado o Documento derivado
                        {
                            htmlBtnDevolver = '<button id="btnDevolverForm" data-target="modal-confirm-devolver" class="modal-trigger btn waves-effect waves-light btn-warning modal-action">' +
                                '<span style="font-size: 80%">Devolver</span>' +
                                '<i class="material-icons prefix">send</i>' +
                                '</button>   ';
                        }*/
                       /* if (_HabilitarFinalizar === true && result.data.DocumentoFirmado > 0)//Documento asignado
                        {
                            htmlBtnFinalizar = '<button id="btnFinalizarForm" data-target="modal-confirm-finalizar" class="modal-trigger btn waves-effect waves-light btn-warning modal-action">' +
                                '<span style="font-size: 80%">Finalizar</span>' +
                                '<i class="material-icons prefix">check</i>' +
                                '</button>';
                        }*/

                        if (result.data.siFirma === 1)//Documento asignado
                        {
                            var esCC = true;
                            //validamos que el usuario no sea CC
                            for (var x = 0; x < $(".divcc").length; x++) {
                                if ($(".divcc")[x].innerText == $("#hdnWebUsr").val()) {
                                    esCC = false;
                                    break;
                                }
                            }

                            if (esCC) {
                                //INTERNRO - PROCESO - PENDIENTE
                                if (result.data.Origen == 3 && (result.data.EstadoDocumento == 6 || result.data.EstadoDocumento == 2) && (_estadoDocumento != 7 && _estadoDocumento != 3)) {
                                    htmlFirmar = '<button onclick="IniciarProcesoFirmaSIED( \'' + _objArgumentNombreArchivo + '\',\'' + _objArgumentIdLaserfiche + '\');"  class="btn waves-effect waves-light btn-warning">' +
                                        '<span style="font-size: 80%">Firmar</span>' +
                                        '<i class="material-icons prefix">verified_user</i>' +
                                        '</button>';
                                    //EXTERNO EN PROCESO
                                } else if (result.data.Origen == 4 && (result.data.EstadoDocumento == 6 || result.data.EstadoDocumento == 2) && (_estadoDocumento != 7 && _estadoDocumento != 3)) {
                                    htmlFirmar = '<button onclick="IniciarProcesoFirmaSIED( \'' + _objArgumentNombreArchivo + '\',\'' + _objArgumentIdLaserfiche + '\');" class="btn waves-effect waves-light btn-warning">' +
                                        '<span style="font-size: 80%">Firmar</span>' +
                                        '<i class="material-icons prefix">verified_user</i>' +
                                        '</button>';
                                }
                            }


                        }

                        var htmlBtns = htmlFirmar + htmlBtnDevolver + htmlBtnFinalizar + htmBtmAnular + htmlBtnPDFCargoRecep;

                        $("#mSeccionExportarPDFSIED").html(htmlBtns);

                        $("#btnAnularForm,#btnFinalizarForm,#btnDevolverForm").click(function () {
                            document.getElementById("frmFinalizarDocumento").reset();
                            document.getElementById("frmDevolverDocumento").reset();
                            document.getElementById("frmAnularDocumento").reset();
                        });

                    },
                    complete: function () {
                        $('#modal_loading_agro').attr("style", "display:none");
                        $("#tbDetalleSIED").attr("style", "display:block");

                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });

                var elem = $('.tabs');
                elem.tabs();
                var instance_tabs = M.Tabs.getInstance(elem);
                instance_tabs.select("tbDetalleSIED");
            });
        });

        fc_ObtenerDestinatariosSIED = function (lstDocDerivacion) {
            var urlBase = $("#hdUrlBase").val();
            var destinatariosCC = "";
            if (lstDocDerivacion !== null) {
                for (var i = 0; i < lstDocDerivacion.length; i++) {
                    if (lstDocDerivacion[i].TipoAcceso === 3) {

                        destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreEntidadSIED + '</div> </div>';
                    }
                }
            };
            return destinatariosCC;
        }

        fc_ObtenerDerivaciones = function (lstDocDerivacion) {
            var urlBase = $("#hdUrlBase").val();
            var destinatariosCC = "";
            if (lstDocDerivacion !== null) {
                for (var i = 0; i < lstDocDerivacion.length; i++) {
                    if (lstDocDerivacion[i].TipoAcceso === 1) {

                        var firmante = fc_validarFirmante(lstDocDerivacion[i].CodDocumento, lstDocDerivacion[i].CodResponsable);

                        var htmlAnularDerivacion = "";

                      /*  if (lstDocDerivacion[i].EstadoDerivacion == 2)//PENDIENTE
                        {
                            htmlAnularDerivacion = "<div class='col s1 m1' > <i class='material-icons btn-anular-derivacion' data-id='" + lstDocDerivacion[i].CodDocumentoDerivacion + "' style='cursor: pointer;'>highlight_off</i></div >";
                        }*/

                        if (lstDocDerivacion[i].EstadoDerivacion == 3)//ANULADA
                        {
                            htmlAnularDerivacion = "<div class='col s1 m1' > <i class='material-icons btn-anular-derivacion tooltipped' style='cursor: pointer;' data-tooltip='" + lstDocDerivacion[i].Comentarios + "'>chat</i></div >";
                        }

                        destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreArea + ' - ' + lstDocDerivacion[i].NombreResponsable + '</div>' +
                            '<div class="col s2 m2"><span class="new badge badge-grey" data-badge-caption="DE"></span></div>'
                            + fc_ObtenerEstadoDocumentoHtml(lstDocDerivacion[i].EstadoDerivacion, false) + ((firmante == true) ? "<div class='col s1 m1'><img  src='" + urlBase + "/Content/img/icon-firma.png' class='responsive-img logo-mini tooltipped' data-position='right' data-tooltip='Firma Realizada'></div>" : htmlAnularDerivacion) + '</div >';
                    }
                    else if (lstDocDerivacion[i].TipoAcceso === 2) {
                        destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreArea + ' - ' + lstDocDerivacion[i].NombreResponsable + '</div>' +
                            '<div class="col s2 m2"><span class="new badge badge-grey" data-badge-caption="CC"></span></div></div>' +
                            '<div class="divcc" style="display:none">' + lstDocDerivacion[i].WEBUsrResponsable + '</div>';
                    }
                }
            };
            return destinatariosCC;
        }


        $(document).on("mouseover", ".tooltipped", function () {
            $('.tooltipped').tooltip();

        });

        $(document).on("click", "#idDerivarDocumento", function () {
            codDocumentoAderivar = $(this).data('id');
            coddocderivacion = $(this).data('coddocderivacion');
            correlativoDerivar = $(this).data('correlativo');
            esJefeDerivarSied = $(this).data('esjefe');
            $("#mCorrelativo_der").text('Nro.Trámite : ' + correlativoDerivar);
            if (coddocderivacion > 0) {
                var urlDerivacion = urlBase + "/Documento/ObtenerDocumentoDetalleDerivacion";
                //var url = document.location.origin + "/Documento/ObtenerDocumento";
                urlDerivacion += "?CodDocumentoDer=" + coddocderivacion;
                $.ajax({
                    url: urlDerivacion,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,

                    success: function (result) {
                        if (result.data.TipoAccion != null) {
                            $("#mTipoAccion_der").text("Acción a realizar : ");
                            $("#mTipoAccion_derb").text(result.data.TipoAccion);
                        }
                    }
                });
            }

            add = 0;
            $("#DivDerivacion").children().remove();

            htmlAddHtml = '<div class="col s1">' +
                '<a class="btn-floating btn-small waves-effect waves-light btnADD"><i class="material-icons">add</i></a>' +
                '</div>';

            htmlAdd = esJefeDerivarSied != true ? htmlAddHtml : '<div class="col s1" ></div >';

            htmlDerivar =
                '<div class="row">' +
                '<div class="input-field col s3" data-selectTrabajador="selectTrabajadores' + add + '" >' +
                '<select class="selectAreas select-require" id="selectAreas' + add + '" name="selectAreas' + add + '" required>' +
                '<option value="" disabled selected>Área</option>' +
                '</select>' +
                '<label>Área</label>' +
                '</div>' +
                '<div class="input-field col s4">' +
                '<div  id="divficticio' + add + '">' +
                '<select class="selectTrabajadoresFicticio">' +
                '<option value="" disabled selected>Trabajadores</option>' +
                '</select>' +
                '<label>Trabajadores</label>' +
                '</div>' +
                '<div style="display:none" class="divselect" id="divselect' + add + '">' +
                '<select class="selectTrabajadores' + add + ' select-require strabajadores" id="selectTrabajadores' + add + '" name="selectTrabajadores' + add + '" >' +
                '<option value="" disabled selected>Trabajadores</option>' +
                '</select>' +
                '<label>Trabajadores</label>' +
                '</div>' +
                '<div style="display:none" class="divselectmultiple" id="divselectmultiple' + add + '">' +
                '<select class="selectTrabajadores' + add + ' select-require strabajadores" id="selectTrabajadoresm' + add + '" name="selectTrabajadoresm' + add + '" multiple disabled>' +
                '</select>' +
                '<label>Trabajadores</label>' +
                '</div>' +
                '</div>' +
                '<div class="input-field col s4" data-selectTrabajador="selectTrabajadores' + add + '" >' +
                '<select class="selectAccion select-require" id="selectAccion' + add + '" name="selectAccion' + add + '" required>' +
                '<option value="" disabled selected>Acciones</option>' +
                '</select>' +
                '<label>Acciones a  realizar</label>' +
                '</div>' +
                 htmlAdd
                '</div>';


            $("#DivDerivacion").append(htmlDerivar);
            if (esJefeDerivarSied == true) {
                obtenerAreasSIEDllenarSelect("#selectAreas" + add);
            } else {
                obtenerAreasllenarSelect("#selectAreas" + add);
            }

            obtenerAreasllenarSelectAccion("#selectAccion" + add);
            $('select').formSelect();

        });

        $(document).on("click", ".btnADD", function () {
            add = add + 1;
            htmlDerivarADD =
                '<div class="row">' +
                '<div class="input-field col s3" data-selectTrabajador="selectTrabajadores' + add + '">' +
                '<select class="selectAreas select-require" id="selectAreas' + add + '"  name="selectAreas' + add + '" required>' +
                '<option value="" disabled selected>Área</option>' +
                '</select>' +
                '<label>Área</label>' +
                '</div>' +
                '<div class="input-field col s4">' +
                '<div  class="divficticio" id="divficticio' + add + '">' +
                '<select class="selectTrabajadoresFicticio">' +
                '<option value="" disabled selected>Trabajadores</option>' +
                '</select>' +
                '<label>Trabajadores</label>' +
                '</div>' +
                '<div style="display:none" class="divselect" id="divselect' + add + '">' +
                '<select class="selectTrabajadores' + add + ' select-require strabajadores" id="selectTrabajadores' + add + '" name="selectTrabajadores' + add + '" >' +
                '<option value="" disabled selected>Trabajadores</option>' +
                '</select>' +
                '<label>Trabajadores</label>' +
                '</div>' +
                '<div style="display:none" class="divselectmultiple" id="divselectmultiple' + add + '">' +
            '<select class="selectTrabajadores' + add + ' select-require strabajadores" id="selectTrabajadoresm' + add + '" name="selectTrabajadoresm' + add + '"multiple disabled>' +
                '</select>' +
                '<label>Trabajadores</label>' +
                '</div>' +
                '</div>' +
                '<div class="input-field col s4" data-selectTrabajador="selectTrabajadores' + add + '" >' +
                '<select class="selectAccion select-require" id="selectAccion' + add + '" name="selectAccion' + add + '" required>' +
                '<option value="" disabled selected>Acciones</option>' +
                '</select>' +
                '<label>Acciones a  realizar</label>' +
                '</div>' +
                '<div class="col s1">' +
                '<a class="btn-floating  btn-small btnEliminarSelect red"><i class="material-icons">close</i></a>' +
                '</div>' +
                '</div>';

            $("#DivDerivacion").append(htmlDerivarADD);
            htmlDerivarADD = '';
            obtenerAreasllenarSelect("#selectAreas" + add);
            obtenerAreasllenarSelectAccion("#selectAccion" + add);
            $('select').formSelect();
        });

        $(document).on("click", ".btnEliminarSelect", function () {
            this.parentElement.parentElement.remove();
        });

        $(document).on("change", ".selectAreas", function () {
            var sijefe = 99;
            var _codUsuarioLogin = $("#hdnCodTrabajadorLogin").val();
            var idselecttrabajador = this.parentElement.parentElement.attributes["data-selecttrabajador"].value;
            //validando que no ya se haya seleccionado previamente
            for (var x = 0; x < $("#DivDerivacion").find('.selectAreas').length; x++) {
                if (($("#DivDerivacion").find('.selectAreas')[x].value == this.value) && ($("#DivDerivacion").find('.selectAreas')[x].id != this.id)) {

                    $("#" + this.id).val($("#" + this.id + " option:first").val());
                    $("#" + this.id).formSelect();

                    $("." + idselecttrabajador).empty();
                    $("." + idselecttrabajador).append("<option value=''>Trabajadores</option>");
                    $("." + idselecttrabajador).formSelect();


                    $('#ptv-alert-error').modal();
                    $('.p-desc-error').text("El área que se seleccionó ya ha sido seleccionada anteriormente");
                    $('#ptv-alert-error').modal('open');
                    return false;
                }
            }

            if ($("#hdnCodAreaLogin").val() != this.value) {
                sijefe = 1;
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[0].id).hide();
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[1].id).hide();
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[2].id).show();

                $("#" + this.parentElement.parentElement.parentElement.children[1].children[2].children[0].children[3].id).prop("required", true);
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[1].children[0].children[3].id).prop("required", false);
            } else {
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[0].id).hide();
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[2].id).hide();
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[1].id).show();

                $("#" + this.parentElement.parentElement.parentElement.children[1].children[2].children[0].children[3].id).prop("required", false);
                $("#" + this.parentElement.parentElement.parentElement.children[1].children[1].children[0].children[3].id).prop("required", true);
            }


            if ($("." + idselecttrabajador)[0].length > 0) {
                $("." + idselecttrabajador).empty();
                $("." + idselecttrabajador).formSelect();
            }
            if (esJefeDerivarSied === true) {
                obtenerTrabajadoresPorAreaSIED("." + idselecttrabajador);
                var idselect = this.parentElement.parentElement.parentElement.children[1].children[2].children[0].children[2].id;

                $("#" + idselect).val($("#" + idselect + " option:first").val());
                //$('#' + idselect).attr('disabled', 'disabled');
                $("#" + idselect).formSelect();
            } else {
                obtenerTrabajadoresPorArea(this.value, "." + idselecttrabajador, _codUsuarioLogin, sijefe);
                if (sijefe == 1) {
                    var idselect = this.parentElement.parentElement.parentElement.children[1].children[2].children[0].children[2].id;

                    $("#" + idselect).val($("#" + idselect + " option:first").val());
                    //$('#' + idselect).attr('disabled', 'disabled');
                    $("#" + idselect).formSelect();
                }
            }
            $('select').formSelect();
        });

        $(document).on("click", "#idDocumentoEdit", function () {
            var _codDocumento = $(this).data('id');
            var _codRepresentante = "";
            var _to = "";
            var _CC = [];
            var url = urlBase + "/Documento/ObtenerDocumentoInterno";
           //var url = document.location.origin + "/Documento/ObtenerDocumentoInterno";
            var arrAnexosEliminados = [];
            var _remitenteEdit = "";
            var cboRemitidoPorEdit = $("#remitidoPorEdit");

            url += "?codDocumento=" + _codDocumento;
            $("#modal_loading_agro").attr("style", "display:block");
            $("#tbDetalle").attr("style", "display:none");

            $.ajax({
                url: url,
                dataType: "json",
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    var date = new Date(result.data.FechaPlazo);
                    var _dia = '00' + date.getDate().toString();
                    _dia = _dia.substring(_dia.length - 2, _dia.length);
                    var _mes = '00' + (date.getMonth() + 1).toString();
                    _mes = _mes.substring(_mes.length - 2, _mes.length);
                    var fPlazo = _dia + "/" + _mes + "/" + date.getFullYear();
                    var _codigosCC = "";
                    var _codigosTO = "";

                    $("#CorrelativoEdit").val(result.data.Correlativo);
                    $("#hdCodDocumentoEdit").val(_codDocumento);

                    $("#NumDocumentoEdit").val(result.data.NumDocumento);

                    $("#fechaDocumentoEdit").val(formatfecha(result.data.FechaDocumento));

                    $("#FechaRecepcionEdit").val(formatfecha(result.data.FechaRecepcion));

                    $("#plazoEdit").val(result.data.Plazo);

                    $("#fechaPlazoEdit").val(formatfecha(result.data.FechaPlazo));

                    $("#prioridadEdit").val(result.data.Prioridad);
                    $('#prioridadEdit').formSelect();

                    $("#codTipoDocumentoEdit").val(result.data.CodTipoDocumento);
                    $("#codTipoDocumentoEdit").formSelect();

                    $("#asuntoEdit").val(result.data.Asunto);
                    $("#referenciaEdit").val(result.data.Referencia);
                    $("#observacionesEdit").val(result.data.Observaciones);
                    $(".adjuntarArchivoAuxEdit").val("");
                    $(".lblactive").addClass("active");

                    $("#hdCodRemitenteEdit").val(result.data.CodEmpresa);
                  //  $("#hdCodAreaEdit").val(result.data.CodArea);
                    $("#estadoDocumentoEdit").attr("data-badge-caption", "INGRESADO");
                    _remitenteEdit = result.data.DescripcionEmpresa;
                   // _to = result.data.Area;
                    _codRepresentante = result.data.CodRepresentante;
                    obtenerTrabajadoresPorEmpresaEdit(bindTrabajadoresComboEditCarga, result.data.CodEmpresa);
                    
                    if (result.data.LstDocDerivados != null) {
                        var codigosTO = "";
                        var codigosDocumentosDerivacion = "";
                        _codigosTO = result.data.LstDocDerivados;
                        for (var x = 0; x < _codigosTO.length; x++) {
                            codigosTO = (codigosTO + ((codigosTO.length > 0) ? '|' : "") + result.data.LstDocDerivados[x].CodArea).trim();
                            codigosDocumentosDerivacion = (codigosDocumentosDerivacion + ((codigosDocumentosDerivacion.length > 0) ? '|' : "") + result.data.LstDocDerivados[x].CodDocumentoDerivacion).trim();
                        }
                    }

                    if (result.data.LstDocCC != null) {
                        var codigosCC = "";
                        _codigosCC = result.data.LstDocCC;
                        for (var x = 0; x < _codigosCC.length; x++) {
                            codigosCC = (codigosCC + ((codigosCC.length > 0) ? '|' : "") + result.data.LstDocCC[x].CodArea).trim();
                        }
                    }
                    $("#hdCodAreaCCEdit").val(codigosCC);
                    $("#hdCodAreaEdit").val(codigosTO);
                    $("#hdCodDerivadosInactivar").val(codigosDocumentosDerivacion);
                   // cargarRemitenteEdit();
                    cargarToCCEdit(_codigosCC, _codigosTO);

                    var laserFicher = result.data.ArchivosLaserFiche;
                    var iniAnexo = 0;
                    if (result.data.LstDocAdjunto.length > 0) {
                        $("#hdDocumentoAdjunto").val((result.data.LstDocAdjunto[0].CodDocAdjunto));
                        $("#estadoDocumentoEdit").attr("data-badge-caption", "PENDIENTE");
                        $(".adjuntarArchivoAuxEdit").val(result.data.ArchivosLaserFiche.ListaDocumentos[0].NombreArchivo);
                        iniAnexo = 1;
                    }
                    if (laserFicher != null) {
                        var btnAnexosIngresados = "";
                        var htmlAnexosIngresados = "";
                        for (var i = iniAnexo; i < laserFicher.ListaDocumentos.length; i++) {


                            btnAnexosIngresados = '<button class="waves-effect waves-light btnAnexoIngresado btn-warv2" style="cursor: pointer;" id=' + laserFicher.ListaDocumentos[i].IdDocumento + ' >' +
                                '	<i class="material-icons right">close</i>' +
                                '	<span>' + laserFicher.ListaDocumentos[i].NombreArchivo + '</span>' +
                                '</button>&nbsp';
                            htmlAnexosIngresados = htmlAnexosIngresados + btnAnexosIngresados;
                        }
                        $(".AnexosIngresados").html(htmlAnexosIngresados);
                    }
                    validarMinMax2Fechas(inputFechaRecepcionEdit, inputfechaDocumentoEdit);
                },
                complete: function () {
                    $('#modal_loading_agro').attr("style", "display:none");
                    $("#tbDetalle").attr("style", "display:block");
                    $(".btnAnexoIngresado").click(function () {
                        $(this).remove();
                        var id = $(this).attr('id');
                        arrAnexosEliminados.push(id);
                        $("#hdAnexosElimiados").val(arrAnexosEliminados);
                    });

                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            var cargarToCCEdit = function (_codigosCC, _codigosTO) {
                obtenerAreas(function (data) {
                    ResultAreas = [];
                    var CCExist = [];
                    var TOExist = [];
                    var result = {};
                    for (var i = 0; i < data.length; i++) {
                        var area = {
                            codigoArea: data[i].CodArea,
                            descripcionArea: data[i].Descripcion
                        }
                        result[(data[i].Descripcion).toString()] = null;
                        ResultAreas.push(area);
                    }

                    if (_codigosCC != null) {
                        for (var x = 0; x < _codigosCC.length; x++) {
                            for (var y = 0; y < ResultAreas.length; y++) {
                                if (((_codigosCC[x].CodArea.toString()).indexOf(ResultAreas[y].codigoArea.toString())) != -1) {
                                    var obj = { tag: ResultAreas[y].descripcionArea };
                                    CCExist.push(obj)
                                }
                            }
                        }
                    }

                    if (_codigosTO != null) {
                        for (var x = 0; x < _codigosTO.length; x++) {
                            for (var y = 0; y < ResultAreas.length; y++) {
                                if (((_codigosTO[x].CodArea.toString()).indexOf(ResultAreas[y].codigoArea.toString())) != -1) {
                                    var obj = { tag: ResultAreas[y].descripcionArea };
                                    TOExist.push(obj)
                                }
                            }
                        }
                    }


                    $('#chipTOEdit').chips({
                        onChipAdd: onChipAddEdit,
                        onChipDelete: onChipAddEdit,
                        autocompleteOptions: {
                            data: result,
                            limit: Infinity,
                            minLength: 1
                        },
                        data:  TOExist,
                    });

                    $('#chipCCEdit').chips({
                        onChipAdd: onChipAddCCEdit,
                        onChipDelete: onChipAddCCEdit,
                        autocompleteOptions: {
                            data: result,
                            limit: Infinity,
                            minLength: 1
                        },
                        data: CCExist
                    });


                    $(".chips-autocomplete.chips-areasEdit input").on("blur", blurChip);
                    $(".chips-autocomplete.chips-areasCCEdit input").on("blur", blurChip);
                });
            }

            var cargarRemitenteEdit = function () {
                obtenerEmpresas(function (data) {
                    ResultRemitente = [];
                    var result = {};
                    for (var i = 0; i < data.length; i++) {
                        var remitente = {
                            codigoRemitente: data[i].CodEmpresa,
                            descripcionRemitente: data[i].Descripcion
                        }
                        result[(data[i].Descripcion).toString()] = null;
                        ResultRemitente.push(remitente);
                    }

                    $('#chipRemitenteEdit').chips({
                        onChipAdd: onChipAddRemitenteEdit,
                        onChipDelete: onChipAddRemitenteEdit,
                        autocompleteOptions: {
                            data: result,
                            limit: Infinity,
                            minLength: 1
                        },
                        data: [{
                            tag: _remitenteEdit,
                        }]
                    });

                    $(".chips-autocomplete.chips-remitenteEdit input").prop("required", false);
                    $(".chips-autocomplete.chips-remitenteEdit input").on("blur", blurChip);
                });
            };

            var obtenerEmpresas = function (callback) {

                $.ajax({
                    url: urlBase + "/Maestros/ObtenerEmpresas",
                    //url: document.location.origin + "/Maestros/ObtenerEmpresas",
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado == 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };

            var obtenerAreas = function (callback) {
                $.ajax({
                    url: urlBase + "/Maestros/ObtenerAreas",
                    //url: document.location.origin + "/Maestros/ObtenerAreas",
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado === 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };

            var onChipAddRemitenteEdit = function (e) {

                limpiarCboRemitidoPor();
                validarInputChips('.chips-remitenteEdit');

                $("#chipRemitenteEdit").find('input').val("");
                _codRepresentante = "";
                if ($("#chipRemitenteEdit").find("div").length > 1) {
                    $("#chipRemitenteEdit").find("div")[1].remove();
                } else {

                    var value = $("#chipRemitenteEdit").find(".chip").first().text();
                    var str = value.split("|");
                    var codigo = str[0];
                    var descripcion = codigo.replace("close", "").trim();

                    if (descripcion != "") {
                        for (var x = 0; x < ResultRemitente.length; x++) {
                            var valor = (ResultRemitente[x].descripcionRemitente).indexOf(descripcion);
                            if (valor != -1) {
                                codigo = ResultRemitente[x].codigoRemitente.toString();
                                break;
                            } else {
                                codigo = "";
                            }
                        }
                    }


                    if (codigo.toString().length > 0) {
                        obtenerTrabajadoresPorEmpresa(bindTrabajadoresComboEditCarga, codigo);
                        $(".chips-autocomplete.chips-remitenteEdit input").prop("required", false);
                    }

                    if (codigo.toString().length == 0) {
                        limpiarCboRemitidoPor();
                        $(".chips-autocomplete.chips-remitenteEdit input").prop("required", true);
                    }

                    $("#hdCodRemitenteEdit").val(codigo);
                }
            };

            var onChipAddEdit = function (e) {

                validarInputChips('.chips-areasEdit');

                validarInputChipsCCTO('.chips-areasEdit', '.chips-areasCCEdit');

                    var chipsTO = M.Chips.getInstance($('.chips-areasEdit')).chipsData;
                    var codigoTO = "";
                    if (chipsTO.length > 0) {
                        var chipsCCtag = M.Chips.getInstance($('.chips-areasEdit')).chipsData;
                        for (var x = 0; x < chipsTO.length; x++) {
                            for (var y = 0; y < ResultAreas.length; y++) {
                                var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                                if (valor != -1) {
                                    codigo = ResultAreas[y].codigoArea.toString();
                                    codigoTO = codigoTO + ((codigoTO.length > 0) ? '|' : "") + codigo;
                                }
                            }
                        }
                    }


                if (codigoTO.toString().length > 0) {
                        $(".chips-autocomplete.chips-areasEdit input").prop("required", false);
                    }

                if (codigoTO.toString().length == 0) {
                        $(".chips-autocomplete.chips-areasEdit input").prop("required", true);
                    }

                    $("#hdCodAreaEdit").val(codigoTO);
                
            };

            var onChipAddCCEdit = function (e) {

                $("#hdCodAreaCCEdit").val("");
                if (validarInputChips('.chips-areasCCEdit') == 0) {
                    return false;
                }
                validarInputChipsTOCC('.chips-areasEdit', '.chips-areasCCEdit');

                var chipsCC = M.Chips.getInstance($('.chips-areasCCEdit')).chipsData;
                var codigosCC = "";
                if (chipsCC.length > 0) {
                    var chipsCCtag = M.Chips.getInstance($('.chips-areasCCEdit')).chipsData;
                    for (var x = 0; x < chipsCC.length; x++) {
                        for (var y = 0; y < ResultAreas.length; y++) {
                            var valor = (chipsCCtag[x].tag).indexOf(ResultAreas[y].descripcionArea);
                            if (valor != -1) {
                                codigo = ResultAreas[y].codigoArea.toString();
                                codigosCC = codigosCC + ((codigosCC.length > 0) ? '|' : "") + codigo;
                            }
                        }
                    }
                }
                $("#hdCodAreaCCEdit").val(codigosCC);
                $(".chips-autocomplete.chips-areasCCEdit input").prop("required", false);
             /*   if (codigosCC.toString().length > 0) {
                    $(".chips-autocomplete.chips-areasCCEdit input").prop("required", false);
                }

                if (codigosCC.toString().length == 0) {
                    $(".chips-autocomplete.chips-areasCCEdit input").prop("required", true);
                }*/
            };

            var blurChip = function () {

                $("#chipRemitenteEdit").find("input").val("");
                $("#chipTOEdit").find('input').val("");

            }

            var limpiarCboRemitidoPor = function () {
                cboRemitidoPorEdit.empty();
                cboRemitidoPorEdit.append("<option value=''>Seleccione</option>");
                cboRemitidoPorEdit.formSelect();
            }

            var obtenerTrabajadoresPorEmpresa = function (callback, idEmpresa) {

                var url = urlBase + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                //var url = document.location.origin + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                url += "?codEmpresa=" + idEmpresa;

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado == 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };


            var bindTrabajadoresComboEditCarga = function (trabajadores) {
                cboRemitidoPorEdit.empty();
                cboRemitidoPorEdit.append("<option value=''>Seleccione</option>");
                $.each(trabajadores, function (ind, val) {
                    cboRemitidoPorEdit.append("<option value='" + val.CodRepresentante + "'>" + val.Nombre + "</option>");
                });
                cboRemitidoPorEdit.formSelect();

                if (_codRepresentante != "") {
                    cboRemitidoPorEdit.val(_codRepresentante);
                    cboRemitidoPorEdit.formSelect();
                }
            }

            var obtenerTrabajadoresPorEmpresaEdit = function (callback, idEmpresa) {

                var url =  urlBase + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                //var url = document.location.origin + "/Maestros/ObtenerTrabajadoresPorEmpresa";
                url += "?codEmpresa=" + idEmpresa;

                $.ajax({
                    url: url,
                    dataType: "json",
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    cache: false,
                    success: function (result) {

                        if (result.Resultado == 1) {
                            callback(result.data);
                        }
                    },
                    error: function (e, b, c) {
                        console.log(e.responseText);
                    }
                });
            };
        });

        $(document).on("click", "#idDocumentoAnular", function () {
            var _iDerivaciones = null;
            var _sRotulo = "¿Está seguro de anular el documento?";           
            var _codDocumento = $(this).data('id');

            $.ajax({
                url: urlBase + "/Documento/GetDocumentosDerivadosPorCodDocumento?CodDocumento=" + _codDocumento + "&TipoAcceso=" + 1,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        _iDerivaciones = result.data;
                    }
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });

            $("#hdCodDocumentoAnular").val(_codDocumento);
            if (_iDerivaciones != null) _sRotulo = "El documento tiene derivaciones, ¿Está seguro de anular el documento y sus derivaciones?"
            $("#lblAnularRotulo").html(_sRotulo);
        });

        return {
            init: init
        };

    })();

    bandejaEntrada.init();    
});