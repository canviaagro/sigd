﻿$(document).ready(function () {
    var urlbase = $("#hdUrlBase").val();
    var Secuencia = new Object();
    Secuencia.CodSecuencial = "";
    Secuencia.anio = "";
    Secuencia.CodArea = "";
    Secuencia.CodTipoDocumento = "";
    Secuencia.Origen = "";
    Secuencia.correlativo = "";
    Secuencia.ValorCorrelativo = "";
    Secuencia.DescripcionArea = "";
    Secuencia.DescripcionTipoDocumento = "";   
    Secuencia.Estado = "";

    var confirmDelete = true;
    var SelectOrigen = $(".cbOrigen");
    var anio = $(".cbAnio");
    var SelectAreas = $(".cbAreas");
    var SelectTipoDocumento = $(".cbTipoDocumento");
    var secCorrelativo = $(".secCorrelativo");
    var ValorCorrelativo = $(".ValorCorrelativo");
    var lblcorrelativo = $(".lblcorrelativo");

    cargarSelectOrigenNoSied(SelectOrigen);
    obtenerAreasllenarSelect(SelectAreas);
    cargarSelectTipoDocumento(SelectTipoDocumento);
    obtenerEstadosSelect("#cbEstadoEdit");
    obtenerEstadosSelect("#cbEstado");

    $(document).on("click", "#idVerSecuencia", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/Secuenciales/Ver?id=" + id;

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            success: function (response) {
                $("#ptv-VerTipoDocumento").html(response);
            },
        });
    });

    $(anio).change(function () {
        Secuencia.anio = this.value;
        ValorCorrelativo.val("");
        secCorrelativo.val("");
        habilitarCorrelativo();
        return false
    });

    $(SelectAreas).change(function () {
        Secuencia.CodArea = this.value;
        Secuencia.DescripcionArea = $("#" + this.id + " option[value='" + this.value + "']").text();
        ValorCorrelativo.val("");
        secCorrelativo.val("");
        habilitarCorrelativo();
        return false;
    });

    $(SelectTipoDocumento).change(function () {
        Secuencia.CodTipoDocumento = this.value;
        Secuencia.DescripcionTipoDocumento = $("#" + this.id + " option[value='" + this.value + "']").text();
        ValorCorrelativo.val("");
        secCorrelativo.val("");
        habilitarCorrelativo();
        return false;
    });

    $(SelectOrigen).change(function () {
        ValorCorrelativo.val("");
        secCorrelativo.val("");
        Secuencia.Origen = this.value;
        if (Secuencia.Origen == 1) {

            restriccionesOrigenInterno();

        } else if ((Secuencia.Origen == 2)) {

            restriccionesOrigenExterno();

        } else {
            anio.prop("required", false); anio.prop("disabled", true);
            SelectAreas.prop("required", false); SelectAreas.prop("disabled", true);
            SelectTipoDocumento.prop("required", false); SelectTipoDocumento.prop("disabled", true);
            SelectAreas.formSelect(); SelectTipoDocumento.formSelect(); anio.formSelect();
        };
        return false;
    });

    $(document).on("click", ".btn-close", function () {
        $('#frmNuevoSecuencial')[0].reset();        
        return false;
    });

    $(document).on("click", "#btnCancelar", function () {
        $('#frmNuevoSecuencial')[0].reset();
        $('#ptv-NuevoSecuencial').modal('close');
        return false;
    });

    $(document).on("click", "#btnCancelarEdit", function () {
        $('#ptv-EditarSecuencial').modal('close');
        return false;
    });

    $(secCorrelativo).change(function () {
        lblcorrelativo.addClass('active');
        secCorrelativo.prop("disabled", true);
        Secuencia.correlativo = this.value;
        $.ajax({
            url: urlbase + "/Mantenimiento/Secuenciales/ObtenerCorrelativo",
            data: JSON.stringify(Secuencia),
            dataType: "json",
            async: false,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            cache: false,
            success: function (result) {
                if (result.Resultado == 1) {
                    Secuencia.ValorCorrelativo = (result.data);
                    ValorCorrelativo.val(result.data);
                    secCorrelativo.prop("disabled", false);
                }
            },
            error: function (e, b, c) {
                console.log(e.responseText);
            }
        });

       /* var str = "" + this.value;
        var pad = "0000"
        var ans = pad.substring(0, pad.length - str.length) + str
        if (ans != "0000") {
            ValorCorrelativo.val(ans + '-' + Secuencia.anio);
            Secuencia.ValorCorrelativo = (ans + '-' + Secuencia.anio);
        }*/
        return false;
    });


    var habilitarCorrelativo = function () {
        if (Secuencia.Origen == 1) {
            if (Secuencia.anio != "" && Secuencia.CodArea != "" && Secuencia.CodTipoDocumento != "") {
                secCorrelativo.prop("disabled", false);
            } else {
                ValorCorrelativo.val("");
                secCorrelativo.val("");
                secCorrelativo.prop("disabled", true);
            }
        } else if (Secuencia.Origen == 2) {
            if (Secuencia.anio.length > 0) {
                secCorrelativo.prop("disabled", false);
            } else {
                ValorCorrelativo.val("");
                secCorrelativo.val("");
                secCorrelativo.prop("disabled", true);
            }

        }
        return false;
    };

    $(document).on("click", "#btnNuevo", function (event) {
        Secuencia = new Object();
        $("#cbEstado").val("1");
        $("#cbEstado").formSelect();
        
        
        anio.prop("required", false); anio.prop("disabled", true);
        SelectAreas.prop("required", false); SelectAreas.prop("disabled", true);
        SelectTipoDocumento.prop("required", false); SelectTipoDocumento.prop("disabled", true);
        SelectAreas.formSelect(); SelectTipoDocumento.formSelect(); anio.formSelect();
    });

    $(document).on("click", "#btnGuardar", function (event) {
        Secuencia.Estado = $("#cbEstado").val();
        var form = $('#frmNuevoSecuencial');
        if (!form[0].checkValidity()) {
            return true;
        }
        event.preventDefault();
        EjecutarAjaxV2("POST", "/Secuenciales/RegistrarSecuencial", Secuencia, function () { window.location.href = urlbase + "/Mantenimiento/Secuenciales"; }, "Se registró la secuencia correctamente");
    });

    $(document).on("click", "#btnActualizar", function (event) {
        
        Secuencia.Estado = $('#cbEstadoEdit').val();
        var form = $('#frmNuevoSecuencialEdit');
        if (!form[0].checkValidity()) {
            return true;
        }
        event.preventDefault();
        EjecutarAjaxV2("POST", "/Secuenciales/EditarSecuencial", Secuencia, function () { window.location.href = urlbase + "/Mantenimiento/Secuenciales"; }, "Se actualizó la secuencia correctamente");
    });


    $(document).on("click", "#idEliminarSecuencia", function (event) {
        Secuencia.CodSecuencial = $(this).data('id');
        $('.p-desc-confirmacion').text('¿Está seguro de eliminar la Secuencia?');
        $('#ptv-alert-confirmacion').modal('open');
        confirmDelete = false;
    });

    $(document).on("click", "#btnAgree", function (event) {
        if (!confirmDelete) {
            EliminarSecuenciaAjax(Secuencia.CodSecuencial);
        }
    });

    function EliminarSecuenciaAjax(CodSecuencial) {
        Secuencia = new Object();
        Secuencia.CodSecuencial = CodSecuencial;
            $.ajax({
                url: urlbase + "/Mantenimiento/Secuenciales/Eliminar",
                data: JSON.stringify(Secuencia),
                dataType: "json",
                async: false,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                                window.location.href = urlbase + "/Mantenimiento/Secuenciales";
                            }
                        });
                        $('.p-descripcion').text("Se elimino la Secuencia correctamente");
                        $('#ptv-alert-ok').modal('open');
                    }
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });
        }


    $(document).on("click", "#idEditarSecuencia", function (event) {
            
            Secuencia = new Object();
            Secuencia.CodSecuencial = $(this).data('id');
            $.ajax({
                url: urlbase + "/Mantenimiento/Secuenciales/ObtenerSecuencia?id=" + Secuencia.CodSecuencial,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    $("#cbOrigenEdit").val(result.Origen);
                    Secuencia.Origen = result.Origen;
                    $('#cbEstadoEdit').val(result.Estado);
                    Secuencia.Estado = result.Estado;
                    lblcorrelativo.addClass('active');
                    $("#secCorrelativoEdit").prop("disabled", false);
                    if (result.Origen == 1) {
                        $("#cbAnioEdit").val(result.anio); Secuencia.anio = result.anio;
                        $("#cbAreasEdit").val(result.CodArea); Secuencia.CodArea = result.CodArea;
                        $("#cbTipoDocumentoEdit").val(result.CodTipoDocumento); Secuencia.CodTipoDocumento = result.CodTipoDocumento;
                        $("#secCorrelativoEdit").val(result.correlativo); Secuencia.correlativo = result.correlativo;
                        $("#ValorCorrelativoEdit").val(result.ValorCorrelativo); Secuencia.ValorCorrelativo = result.ValorCorrelativo;
                        restriccionesOrigenInterno();
                        $("#lblseccorrelativo").addClass("active");
                    } else if (result.Origen == 2) {
                        Secuencia.CodArea = "";
                        Secuencia.CodTipoDocumento = "";
                        $("#cbAnioEdit").val(result.anio); Secuencia.anio = result.anio;
                        $("#secCorrelativoEdit").val(result.correlativo); Secuencia.correlativo = result.correlativo;
                        $("#ValorCorrelativoEdit").val(result.ValorCorrelativo); Secuencia.ValorCorrelativo = result.ValorCorrelativo;
                        restriccionesOrigenExterno();
                    }
                    $('select').formSelect();
                },
                error: function (e, b, c) {
                    console.log(e.responseText);
                }
            });
            return false;
        });

    function restriccionesOrigenInterno() {
        anio.prop("required", true); anio.prop("disabled", false);
        SelectAreas.prop("required", true); SelectAreas.prop("disabled", false);
        SelectTipoDocumento.prop("required", true); SelectTipoDocumento.prop("disabled", false);
        SelectAreas.formSelect(); SelectTipoDocumento.formSelect(); anio.formSelect();
        return false;
    }
    function restriccionesOrigenExterno() {
        anio.prop("required", true); anio.prop("disabled", false);
        secCorrelativo.prop("disabled", false);
        SelectAreas.prop("required", false); SelectAreas.prop("disabled", true);
        SelectTipoDocumento.prop("required", false); SelectTipoDocumento.prop("disabled", true);
        SelectAreas.val("");
        SelectTipoDocumento.val("");
        SelectAreas.formSelect(); SelectTipoDocumento.formSelect(); anio.formSelect();
        return false;
    }
});