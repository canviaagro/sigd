﻿
var codEmpresa = "";
$(document).ready(function () {

    feature.refresh();
    var urlbase = $("#hdUrlBase").val();

    var elem = $('.tabs');
    if (elem.length > 0) {
        elem.tabs();
        var instance_tabs = M.Tabs.getInstance(elem);
        instance_tabs.select("Detalle");
    }
    $(document).on("click", "#btnVer", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/Empresas/Ver?id=" + id;

        cargarPagina(url, "#pop-n1_allwindow", function () {



            var elem = $('.tabs');
            elem.tabs();
            var instance_tabs = M.Tabs.getInstance(elem);
            instance_tabs.select("Detalle");


            url = urlbase + "/Mantenimiento/Representante?codEmpresa=" + id;
            cargarPagina(url, "#secRepresentantes", function () {
                feature.refresh();




            });


        });
    });
    console.log("1");
    $(document).off("keypress", "#txtsearchStringRepresentante").on("keypress", "#txtsearchStringRepresentante", function () {
        if (window.event.keyCode === 13) {

            window.event.preventDefault();

            alert("ok");
            return false;
        }
    });
    $(document).off("click", "#btnEditar").on("click", "#btnEditar", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/Empresas/Editar?id=" + id;

        window.location.href = url;
        //cargarPagina(url, "#pop-n1_allwindow", function () {
        //    feature.refresh();

        //    var elem = $('.tabs');
        //    elem.tabs();
        //    var instance_tabs = M.Tabs.getInstance(elem);
        //    instance_tabs.select("Detalle");



        //    url = document.location.origin + "/Mantenimiento/Representante?codEmpresa=" + id;
        //    cargarPagina(url, "#secRepresentantes", function () {
        //        feature.refresh();

        //    });



        //});
    });

    $(document).off("click", "#btnNuevo").on("click", "#btnNuevo", function () {
        var url = urlbase + "/Mantenimiento/Empresas/Nuevo";

        $('#frmRegistro').focus();
        
        cargarPagina(url, "#pop-n1_allwindow", function () {

            feature.refresh();


            var elem = $('.tabs');
            elem.tabs();
            var instance_tabs = M.Tabs.getInstance(elem);
            instance_tabs.select("Detalle");
        });
    });

    $(document).on("click", "#btnCancelar", function () {
        $('#pop-n1_allwindow').modal('close');
        return false;
    });
    $(document).off("click", "#btnEliminar").on("click", "#btnEliminar", function () {

        var obj = $(this);
        var data = new Object();
        data.Id = $(this).data('id');

        miConfirm('¿Está seguro de inactivar el registro?', function (ok) {
            if (ok === true) {
                EjecutarAjaxPost(obj, "/Mantenimiento/Empresas/Eliminar", data,
                    function (result) {
                        if (result.Resultado === 1) {
                            MsgInformacion("Se inactivó el registro correctamente", function () {
                                window.location.href = urlbase + "/Mantenimiento/Empresas";
                            });
                        }
                        else {
                            MsgError("Hubo problemas en el registro.");
                        }
                    },
                    function (e, b, c) {
                        MsgError("Hubo problemas en la aplicación.");
                    }
                );
            }
        });
        return false;
    });


    $(document).off("click", "#btnGuardar").on("click", "#btnGuardar", function () {
        var form = $('#frmRegistro');
        if (!form[0].checkValidity()) {
            return true;
        }
        event.preventDefault();
        var obj = $(this);
        miConfirm('¿Está seguro de grabar el registro?', function (ok) {
            if (ok === true) {
                EjecutarAjaxForm(obj, "/Mantenimiento/Empresas/Grabar", form,
                    function (result) {
                        if (result.Resultado === 1) {
                            MsgInformacion("Se registró correctamente", function () {
                                window.location.href = urlbase + "/Mantenimiento/Empresas";
                            });
                        }
                        else {
                            MsgError("Hubo problemas en el registro.");
                        }
                    },
                    function (e, b, c) {
                        MsgError("Hubo problemas en la aplicación.");
                    }
                );
            }
        });
        return false;
    });


});
