﻿$(document).ready(function () {

    var urlbase = $("#hdUrlBase").val();

    var manusuariosied = (function () {
        var selectEstado = $("#cbEstado");
        var nombre = $("#nombre");
        var apaterno = $("#apaterno");
        var amaterno = $("#amaterno");
        var email = $("#email");
        var nrodoc = $("#dni");
        var btnGuardar = $("#btnGuardar");
        var accion = $(".frmRepLegalAccion");

        var init = function () {
            btnGuardar.on("click", btnGuardarClick);


        };

        var btnGuardarClick = function (event, data) {
            var ejecucionExitosa = "";

            if ($('#frmRepresentanteLegal')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmRepresentanteLegal')[0].checkValidity()) {
                return true;
            }
            var obj = $(this);
            var data = new Object();
            data.Estado = selectEstado.val();
            data.Nombre = nombre.val();
            data.ApePaterno = apaterno.val();
            data.ApeMaterno = amaterno.val();
            data.Email = email.val();
            data.NroDoc = nrodoc.val();
            data.Accion = accion.val();

            if (accion.val() == "grabar") {
                ejecucionExitosa = "Se grabó el registro correctamente";
            } else {
                ejecucionExitosa = "Se actualizó el registro correctamente";
            }


            miConfirm('¿Está seguro de ' + accion.val() + ' el registro?', function (ok) {
                if (ok === true) {
                    EjecutarAjaxPost(obj, "/RepresentanteLegal/Grabar", data,
                        function (result) {
                            if (result.Resultado === 1) {
                                MsgInformacion(ejecucionExitosa, function () {
                                    window.location.href = urlbase + "/Mantenimiento/RepresentanteLegal";
                                });
                            }
                            else {
                                MsgError("Hubo problemas en el registro.");
                            }
                        },
                        function (e, b, c) {
                            MsgError("Hubo problemas en la aplicación.");
                        }
                    );
                }
            });     
        };

        return {
            init: init
        };

    })();
    manusuariosied.init();
});