﻿$(document).ready(function () {

    var codigoTipodocumento = '';
    var confirmDelete = true;
    var urlbase = $("#hdUrlBase").val();
    var tipodocumento = (function () {
        var abreviatura = $("#abreviatura");
        var descripcion = $("#descripcion");
        var abreviaturaEdit = $("#abreviaturaEdit");
        var descripcionEdit = $("#descripcionEdit");
        var btnGuardar = $("#btnGuardar");
        var btnCancelar = $("#btnCancelar");
        var btnCancelarEdicion = $("#btnCancelarEdicion");
        var btnEditar = $("#btnEditar");
        var CodTipoDocumento = $("#CodTipoDocumento");
        var FechaCreacion = $("#FechaCreacion");
        var codigoVer = $("#codigover");
        var descripcionver = $("#descripcionver");
        var fechaver = $("#fechaver");
        var usuario = $("#usuario");
        var fechaactualizacion = $("#fechaactualizacion");
        var usuarioact = $("#usuarioact");
        var estadover = $("#estadover");
        var abreviaturaver = $("#abreviaturaver");
        var EstadoActivo = 1;
        var EstadoInactivo = 0;
        var btnAgree = $("#btnAgree");

        var init = function () {
            btnGuardar.on("click", btnGuardarClick);
            btnEditar.on("click", btnEditarClick);
            btnCancelarEdicion.on("click", btnCancelarEdicionClick);
            btnCancelar.on("click", btnCancelarClick);
            btnAgree.on("click", btnAgreeClick);
            obtenerEstadosSelect("#cbEstadoNuevo");
            $('#cbEstadoNuevo').val(EstadoActivo);
            $('#cbEstadoNuevo').formSelect();
            FechaCreacion.val();
            $('#ptv-EditarTIpoDocumento .EditTipoDoc').addClass('active');
        };

        var btnCancelarEdicionClick = function () {
            $('#ptv-EditarTIpoDocumento').modal('close');
            return false;
        };

        var btnCancelarClick = function () {
            $('#ptv-NuevoTIpoDocumento').modal('close');
            return false;
        };

        var btnGuardarClick = function (event) {

            if ($('#frmNuevoTipoDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmNuevoTipoDocumento')[0].checkValidity()) {
                return true;
            }

            var data = new Object();
            data.Abreviatura = abreviatura.val();
            data.Descripcion = descripcion.val();
            data.Estado = $('#cbEstadoNuevo').val();

            EjecutarAjax("POST", "/Documento/RegistrarTipoDocumento", data, function () { window.location.href = urlbase + "/Mantenimiento/TiposDeDocumento"; }, "Se registró correctamente el tipo de documento");
        };

        var btnEditarClick = function (event ,data) {

            if ($('#frmEditarTipoDocumento')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmEditarTipoDocumento')[0].checkValidity()) {
                return true;
            }

            var data = new Object();
            data.Abreviatura = abreviaturaEdit.val();
            data.Descripcion = descripcionEdit.val();
            data.CodTipoDocumento = CodTipoDocumento.val();
            data.Estado = $('#cbEstadoEdit').val();

            EjecutarAjax("POST", "/Documento/ActualizarTipoDocumento", data, function () { window.location.href = urlbase + "/Mantenimiento/TiposDeDocumento"; }, "Se actualizó correctamente el tipo de documento");
        };

        $(document).on("click", "#idEditarTipoDocumento", function () {
            $('#cbEstadoEdit').empty();
            obtenerEstadosSelect("#cbEstadoEdit");
            codigoTipodocumento = $(this).data('id');

            $.ajax({
                url: urlbase + "/Mantenimiento/TiposDeDocumento/ObtenerTipoDocumento?id=" + codigoTipodocumento,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    abreviaturaEdit.val(result.Abreviatura);
                    descripcionEdit.val(result.Descripcion);
                    abreviaturaver.val(result.abreviaturaver);
                    CodTipoDocumento.val(codigoTipodocumento);
                    $('#cbEstadoEdit').val(result.Estado);
                    $('#cbEstadoEdit').formSelect();
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en el registro.");
                    console.log(e.responseText);
                }
            });

        });

        $(document).on("click", "#idVerTipoDocumento", function () {

            codigoTipodocumento = $(this).data('id');
            $.ajax({
                url: urlbase + "/Mantenimiento/TiposDeDocumento/ObtenerTipoDocumento?id=" + codigoTipodocumento,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    codigoVer.text(codigoTipodocumento);
                    descripcionver.text(result.Descripcion);
                    abreviaturaver.text(result.Abreviatura);
                    fechaver.text(result.sFechaCreacion);
                    usuario.text(result.UsuarioCreacion); 
                    fechaactualizacion.text((result.sFechaActualizacion == null) ? "" : result.sFechaActualizacion);
                    usuarioact.text((result.UsuarioActualizacion == null) ? "" : result.UsuarioActualizacion);
                    estadover.text((result.Estado==1)?"Activo":"Inactivo");
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en el registro.");
                    console.log(e.responseText);
                }
            });
          

        });

        $(document).on("click", "#idEliminarTipoDocumento", function () {
            codigoTipodocumento = $(this).data('id');
            $('.p-desc-confirmacion').text('¿Está seguro de inactivar el tipo documento?');
            $('#ptv-alert-confirmacion').modal('open');
            confirmDelete = false;
        });

        var btnAgreeClick = function () {
            if (!confirmDelete) {
                EliminarTipoDocumentoAjax(codigoTipodocumento);
            }
        }

        function EliminarTipoDocumentoAjax(CodTipoDocumento) {
            var TipoDocumento = new Object();
            TipoDocumento.CodTipoDocumento = CodTipoDocumento;
            $.ajax({
                url: urlbase + "/Mantenimiento/TiposDeDocumento/Eliminar",
                data: JSON.stringify(TipoDocumento),
                dataType: "json",
                async: false,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                                window.location.href = urlbase + "/Mantenimiento/TiposDeDocumento";
                            }
                        });
                        $('.p-descripcion').text("Se inactivó el registro correctamente");
                        $('#ptv-alert-ok').modal('open');
                    }
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en el registro.");
                    console.log(e.responseText);
                }
            });
        }

        return {
            init: init
        };

    })();

    tipodocumento.init();

});