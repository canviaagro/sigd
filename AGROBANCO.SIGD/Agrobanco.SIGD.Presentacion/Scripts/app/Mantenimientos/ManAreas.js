﻿$(document).ready(function () {

    var codigoArea = '';
    var confirmDelete = true;
    var urlbase = $("#hdUrlBase").val();

    var areas = (function () {
        var abreviatura = $("#abreviatura");
        var descripcion = $("#descripcion");
        var abreviaturaEdit = $("#abreviaturaEdit");
        var descripcionEdit = $("#descripcionEdit");
        var btnGuardar = $("#btnGuardar");
        var btnCancelarEdicion = $("#btnCancelarEdicion");
        var btnCancelar = $("#btnCancelar");
        var btnEditar = $("#btnEditar");
        var CodArea = $("#CodArea");
        var FechaCreacion = $("#FechaCreacion");
        var codigoVer = $("#codigover");
        var descripcionver = $("#descripcionver");
        var fechaver = $("#fechaver");
        var usuario = $("#usuario");
        var fechaactualizacion = $("#fechaactualizacion");
        var usuarioact = $("#usuarioact");
        var estadover = $("#estadover");
        var abreviaturaver = $("#abreviaturaver");
        var EstadoActivo = 1;
        var EstadoInactivo = 0;
        var btnAgree = $("#btnAgree");
        
        var init = function () {
            btnGuardar.on("click", btnGuardarClick);
            btnEditar.on("click", btnEditarClick);
            btnCancelarEdicion.on("click", btnCancelarEdicionClick);
            btnCancelar.on("click", btnCancelarClick);
            btnAgree.on("click", btnAgreeClick);
            obtenerEstadosSelect("#cbEstadoNuevo");
            $('#cbEstadoNuevo').val(EstadoActivo);
            $('#cbEstadoNuevo').formSelect();
            FechaCreacion.val();
            
        };


        var btnCancelarClick = function () {
            abreviatura.val("");
            descripcion.val("");
            //data.Estado = $('#cbEstadoNuevo').val();

            $('#ptv-NuevoTIpoDocumento').modal('close');
            return false;
        };


        var btnCancelarEdicionClick = function () {
            $('#ptv-EditarTIpoDocumento').modal('close');
            return false;
        };

        var btnGuardarClick = function (event) {

            if ($('#frmNuevaArea')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmNuevaArea')[0].checkValidity()) {
                return true;
            }

            var data = new Object();
            data.Abreviatura = abreviatura.val();
            data.Descripcion = descripcion.val();
            data.Estado = $('#cbEstadoNuevo').val();
            EjecutarAjax("POST", "/Areas/Registrar", data, function () { window.location.href = urlbase + "/Mantenimiento/Areas"; }, "Se registró correctamente el área");
        };

        var btnEditarClick = function (event, data) {

            if ($('#frmEditarArea')[0].checkValidity()) {
                event.preventDefault();
            }

            if (!$('#frmEditarArea')[0].checkValidity()) {
                return true;
            }

            var data = new Object();
            data.Abreviatura = abreviaturaEdit.val();
            data.Descripcion = descripcionEdit.val();
            data.CodArea = CodArea.val();
            data.Estado = $('#cbEstadoEdit').val();

            EjecutarAjax("POST", "/Areas/Actualizar", data, function () { window.location.href = urlbase + "/Mantenimiento/Areas"; }, "Se actualizó correctamente el área");
        };

        $(document).on("click", "#idEditarArea", function () {
            $('label').addClass("active");            
            $('#cbEstadoEdit option').remove();           
            
            obtenerEstadosSelect("#cbEstadoEdit");
            codigoArea = $(this).data('id');

            $.ajax({
                url: urlbase + "/Mantenimiento/Areas/ObtenerAreaID?id=" + codigoArea,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    abreviaturaEdit.val(result.Abreviatura);
                    descripcionEdit.val(result.Descripcion);
                    abreviaturaver.val(result.abreviaturaver);
                    CodArea.val(codigoArea);
                    $('#cbEstadoEdit').val(result.Estado);
                    $('#cbEstadoEdit').formSelect();
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en el registro.");
                    console.log(e.responseText);
                }
            });

        });

        $(document).on("click", "#idVerArea", function () {

            codigoArea = $(this).data('id');
            $.ajax({
                url: urlbase + "/Mantenimiento/Areas/ObtenerAreaID?id=" + codigoArea,
                dataType: "json",
                async: false,
                type: "GET",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    codigoVer.text(codigoArea);
                    descripcionver.text(result.Descripcion);
                    abreviaturaver.text(result.Abreviatura);
                    fechaver.text(result.sFechaCreacion);
                    usuario.text(result.UsuarioCreacion);
                    fechaactualizacion.text((result.sFechaActualizacion == null) ? "" : result.sFechaActualizacion);
                    usuarioact.text((result.UsuarioActualizacion == null) ? "" : result.UsuarioActualizacion);
                    estadover.text((result.Estado == 1) ? "Activo" : "Inactivo");
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en el registro.");
                    console.log(e.responseText);
                }
            });


        });

        $(document).on("click", "#idEliminarArea", function () {
            codigoArea = $(this).data('id');
            $('.p-desc-confirmacion').text('¿Está seguro de eliminar el Área?');
            $('#ptv-alert-confirmacion').modal('open');
            confirmDelete = false;

        });

        var btnAgreeClick = function () {
            if (!confirmDelete) {
                EliminarAreaAjax(codigoArea);
            }
        }

        function EliminarAreaAjax(codigoArea) {
            var Areas = new Object();
            Areas.CodArea = codigoArea;

            $.ajax({
                url: urlbase + "/Mantenimiento/Areas/Eliminar",
                data: JSON.stringify(Areas),
                dataType: "json",
                async: false,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                cache: false,
                success: function (result) {
                    if (result.Resultado == 1) {
                        $('#ptv-alert-ok').modal({
                            onCloseEnd: function () {
                                window.location.href = urlbase + "/Mantenimiento/Areas";
                            }
                        });
                        $('.p-descripcion').text("Se inactivó el registro correctamente");
                        $('#ptv-alert-ok').modal('open');
                    }
                },
                error: function (e, b, c) {
                    MsgError("Hubo problemas en el registro.");
                    console.log(e.responseText);
                }
            });
        }

        return {
            init: init
        };

    })();
    areas.init();
});