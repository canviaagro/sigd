﻿

$(document).ready(function () {
    var urlbase = $("#hdUrlBase").val();

    $(document).on("click", "#btnVer", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/AsuntosEstandar/Ver?id=" + id;

        cargarPagina(url, "#pop-n1", function () {


        });
    });
    $(document).on("click", "#btnEditar", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/AsuntosEstandar/Editar?id=" + id;
        cargarPagina(url, "#pop-n1", function () {
            feature.refresh();

        });
    });

    $(document).on("click", "#btnNuevo", function () {
        var url = urlbase + "/Mantenimiento/AsuntosEstandar/Nuevo";

        $('#frmRegistro').focus();
        cargarPagina(url, "#pop-n1", function () {

            feature.refresh();
        });
    });

    $(document).on("click", "#btnCancelar", function () {
        $('#pop-n1').modal('close');
        return false;
    });
    $(document).on("click", "#btnEliminar", function () {

        var obj = $(this);
        var data = new Object();
        data.Id = $(this).data('id');

        miConfirm('¿Está seguro de inactivar el registro?', function (ok) {
            if (ok === true) {
                EjecutarAjaxPost(obj, "/Mantenimiento/AsuntosEstandar/Eliminar", data,
                    function (result) {
                        if (result.Resultado === 1) {
                            MsgInformacion("Se inactivó el registro correctamente", function () {
                                window.location.href = urlbase + "/Mantenimiento/AsuntosEstandar";
                            });
                        }
                        else {
                            MsgError("Hubo problemas en el registro.");
                        }
                    },
                    function (e, b, c) {
                        MsgError("Hubo problemas en la aplicación.");
                    }
                );
            }
        });
        return false;
    });


    $(document).on("click", "#btnGuardar", function () {
        var form = $('#frmRegistro');
        if (!form[0].checkValidity()) {
            return true;
        }
        event.preventDefault();
        var obj = $(this);
        miConfirm('¿Está seguro de grabar el registro?', function (ok) {
            if (ok === true) {
                EjecutarAjaxForm(obj, "/Mantenimiento/AsuntosEstandar/Grabar", form,
                    function (result) {
                        if (result.Resultado === 1) {
                            MsgInformacion("Se registró correctamente", function () {
                                window.location.href = urlbase + "/Mantenimiento/AsuntosEstandar";
                            });
                        }
                        else {
                            MsgError("Hubo problemas en el registro.");
                        }
                    },
                    function (e, b, c) {
                        MsgError("Hubo problemas en la aplicación.");
                    }
                );
            }
        });
        return false;
    });

});
