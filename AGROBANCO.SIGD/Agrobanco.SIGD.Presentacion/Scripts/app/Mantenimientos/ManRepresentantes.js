﻿

$(document).ready(function () {
    var urlbase = $("#hdUrlBase").val();

    $(document).off("click", "#btnVerRep").on("click", "#btnVerRep", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/Representante/Ver?id=" + id;

        cargarPagina(url, "#pop-n1_representante_Ver", function () {


        });
    });
    $(document).off("click", "#btnEditarRep").on("click", "#btnEditarRep", function () {
        var id = $(this).data('id');
        var url = urlbase + "/Mantenimiento/Representante/Editar?id=" + id;
        cargarPagina(url, "#pop-n1_representante", function () {
            feature.refresh();

        });
    });
    $(document).off("click", "#btnNuevoRep").on("click", "#btnNuevoRep", function () {
        var url = urlbase + "/Mantenimiento/Representante/Nuevo?codEmpresa=" + codEmpresa;

        $('#frmRegistroRep').focus();
        cargarPagina(url, "#pop-n1_representante", function () {

            feature.refresh();
        });
    });

    $(document).on("click", "#btnCancelarRep", function () {
        $('#pop-n1_representante').modal('close');
        return false;
    });
    $(document).off("click", "#btnEliminarRep").on("click", "#btnEliminarRep", function () {

        var obj = $(this);
        var data = new Object();
        data.Id = $(this).data('id');

        miConfirm2('¿Está seguro de inactivar el registro?', function (ok) {
            if (ok === true) {
                EjecutarAjaxPost(obj, "/Mantenimiento/Representante/Eliminar", data,
                    function (result) {
                        if (result.Resultado === 1) {
                            MsgInformacion("Se inactivó el registro correctamente", function () {
                                window.location.href = urlbase + "/Mantenimiento/Empresas";
                            });
                        }
                        else {
                            MsgError("Hubo problemas en el registro.");
                        }
                    },
                    function (e, b, c) {
                        MsgError("Hubo problemas en la aplicación.");
                    }
                );
            }
        });
        return false;
    });


    $(document).off("click", "#btnGuardarRep").on("click", "#btnGuardarRep", function () {
        var form = $('#frmRegistroRep');
        if (!form[0].checkValidity()) {
            return true;
        }
        event.preventDefault();
        var obj = $(this);
        miConfirm2('¿Está seguro de grabar el registro?', function (ok) {
            if (ok === true) {
                EjecutarAjaxForm(obj, "/Mantenimiento/Representante/Grabar", form,
                    function (result) {
                        if (result.Resultado === 1) {
                            MsgInformacion("Se registró correctamente", function () {
                                window.location.href = urlbase + "/Mantenimiento/Empresas";
                            });
                        }
                        else {
                            MsgError("Hubo problemas en el registro.");
                        }
                    },
                    function (e, b, c) {
                        MsgError("Hubo problemas en la aplicación.");
                    }
                );
            }
        });
        return false;
    });


});
