﻿var obtenerAsuntos = function (callback) {
    var urlBase = $("#hdUrlBase").val() ;

    $.ajax({
        url: urlBase + "/Maestros/ObtenerAsuntos",
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function (result) {

            if (result.Resultado == 1) {
                callback(result.data);
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};


var obtenerTrabajadoresPorArea = function (data, id, iduserlogueado,sijefe) {
    var urlBase = $("#hdUrlBase").val();
    var url = urlBase + "/Trabajador/ListaTrabajadorPorArea";
    //var url = document.location.origin + "/Trabajador/ListaTrabajadorPorArea";
    url += "?codArea=" + data + "&siEsJefe=" + sijefe;
    $.ajax({
        url: url,
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result, function (key, registro) {
                if (registro.iCodTrabajador != iduserlogueado) {
                    $(id).append('<option value=' + registro.iCodTrabajador + '>' + (registro.vNombres + ' ' + registro.vApePaterno + ' ' + registro.vApeMaterno + ' ' + ((registro.siEsJefe == 1) ? '(JEFE)' : '')) + '</option>');
                }
            });
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });

};

var obtenerTrabajadoresPorAreaSIED = function (id) {
    var urlBase = $("#hdUrlBase").val();
    var url = urlBase + "/Trabajador/ListaTrabajadorPorAreaSIED";
    $.ajax({
        url: url,
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result, function (key, registro) {
   
                    $(id).append('<option value=' + registro.iCodTrabajador + '>' + (registro.vNombres + ' ' + registro.vApePaterno + ' ' + registro.vApeMaterno + ' ' + ((registro.siEsJefe == 1) ? '(JEFE)' : '')) + '</option>');

            });
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });

};

var obtenerTrabajadores = function ( id) {
    var urlBase = $("#hdUrlBase").val();
    var url = urlBase + "/Trabajador/ListaTrabajadores";
    $.ajax({
        url: url,
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result, function (key, registro) {
                $(id).append('<option value=' + registro.iCodTrabajador + ' data-nombre= ' + registro.vNombres + ' data-apepaterno= ' + registro.vApePaterno + ' data-apematerno= ' + registro.vApeMaterno + ' data-email= ' + registro.vEmail + ' data-numdoc= ' + registro.vNumDocumento + '>' + registro.WEBUSR + '</option>');
            });
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });

};


var cargarSelectTipoDocumento = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        //url: document.location.origin + "/Documento/CargarParametrosListaTipoDocumentoAjax",
        url: urlBase + "/Documento/CargarParametrosListaTipoDocumentoAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.LstTipoDocumento, function (key, registro) {
                $(id).append('<option value=' + registro.CodTipoDocumento+ '>' + registro.Descripcion + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};


var cargarSelectEstadosDocumento = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosComunEstadoDocumentoAjax",
//        url: document.location.origin + "/Documento/CargarParametrosComunEstadoDocumentoAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.CodParametro + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();

        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var cargarSelectOrigenNoSied = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosComunOrigenNoSIEDAjax",
        //url: document.location.origin + "/Documento/CargarParametrosComunOrigenAjax",
        dataType: "json",

        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.CodParametro + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};



var cargarSelectOrigen = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosComunOrigenAjax",
        //url: document.location.origin + "/Documento/CargarParametrosComunOrigenAjax",
        dataType: "json",

        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.CodParametro + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerAsuntosSIED = function (callback) {
    var urlBase = $("#hdUrlBase").val();

    $.ajax({
        url: urlBase + "/Documento/CargarParametrosAsuntosSIED",
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function (result) {

            if (result.ListaEstados.length > 0) {
                callback(result);
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};


var cargarSelectMotivoFirma = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosMotivoFirma",
        //url: document.location.origin + "/Documento/CargarParametrosComunOrigenAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.CodParametro + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var cargarFormatoDocumento = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosFormatoDocumento",
        //url: document.location.origin + "/Documento/CargarParametrosComunOrigenAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.Valor + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};


var obtenerEstadosSelect = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosComunAjax",
        //url: document.location.origin + "/Documento/CargarParametrosComunAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + ((registro.CodParametro==2)?0:1) + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerJefe = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametroEstadoJefe",
        //url: document.location.origin + "/Documento/CargarParametrosComunAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + ((registro.CodParametro == 2) ? 0 : 1) + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};




var obtenerAfirmacion = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametroAfirmacion",
        //url: document.location.origin + "/Documento/CargarParametrosComunAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.Valor  + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
            console.log(result);
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerAreasllenarSelect = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Maestros/ObtenerAreas",
        //url: document.location.origin + "/Maestros/ObtenerAreas",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.data, function (key, registro) {
                $(id).append('<option value=' + registro.CodArea + '>' + registro.Descripcion + '</option>');
            });
            $(id).formSelect();
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerAreasSIEDllenarSelect = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Maestros/ObtenerAreasSIED",
        //url: document.location.origin + "/Maestros/ObtenerAreas",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.data, function (key, registro) {
                $(id).append('<option value=' + registro.CodArea + '>' + registro.Descripcion + '</option>');
            });
            $(id).formSelect();
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerAniosSelect = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Reportes/ObtenerAnios",
        //url: document.location.origin + "/Maestros/ObtenerAreas",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.data, function (key, registro) {
                debugger
                $(id).append('<option value=' + registro.vAño + '>' + registro.vAño + '</option>');
            });
            $(id).formSelect();
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var cargarSelectTipoDocIdentidad = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Maestros/ObtenerTipDocIdentidad",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            
            $.each(result.data, function (key, registro) {
                
                $(id).append('<option value=' + registro.iCodTipDocIdentidad + '>' + registro.vDescripcion + '</option>');
            });
            $(id).formSelect();
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};



var obtenerAreasllenarSelectAccion = function (id) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Documento/CargarParametrosComunAccionAjax",
        //url: document.location.origin + "/Documento/CargarParametrosComunAccionAjax",
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            $.each(result.ListaEstados, function (key, registro) {
                $(id).append('<option value=' + registro.CodParametro + '>' + registro.Campo + '</option>');
            });
            $(id).formSelect();
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerAreas = function (callback) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Maestros/ObtenerAreas",
        //url: document.location.origin + "/Maestros/ObtenerAreas",
        dataType: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {

            if (result.Resultado === 1) {
                callback(result.data);
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerEmpresasFonafe = function (callback) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/BandejaSIED/ObteneEmpresasFONAFE",
        dataType: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {

            if (result.codigo === "0") {
                callback(result.listaEmpresa);
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};


var obtenerEmpresas = function (callback) {
    var urlBase = $("#hdUrlBase").val();
    $.ajax({
        url: urlBase + "/Maestros/ObtenerEmpresas",
        //url: document.location.origin + "/Maestros/ObtenerEmpresas",
        dataType: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {

            if (result.Resultado === 1) {
                callback(result.data);
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};


var obtenerTrabajadoresPorEmpresa = function (callback, idEmpresa) {
    var urlBase = $("#hdUrlBase").val();
    var url = urlBase + "/Maestros/ObtenerTrabajadoresPorEmpresa";
    //var url = document.location.origin + "/Maestros/ObtenerTrabajadoresPorEmpresa";
    url += "?codEmpresa=" + idEmpresa;

    $.ajax({
        url: url,
        dataType: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {

            if (result.Resultado === 1) {
                callback(result.data);
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
};

var obtenerFechaPlazo = function (plazo, fechaInicio, callback) {
    var urlBase = $("#hdUrlBase").val();
    var params = JSON.stringify({
        plazo: plazo,
        fechaInicio: fechaInicio
    });

    $.ajax({
        url: urlBase + "/Maestros/GetFechaPlazo",
        //url: document.location.origin + "/Maestros/GetFechaPlazo",
        dataType: "json",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        data: params,
        success: function (result) {
            callback(result);
        }
    });
};


fc_ObtenerEstadoDocumentoHtml = function (idestadodocumento, conLabel) {
    var _stDoc_label = '';
    if (conLabel) {
         _stDoc_label = '<label class="label css-left">Estado:</label>';
    } else {
        _stDoc_label = '';
    }
    var _stDoc_ingresado = _stDoc_label+'<span class="new badge badge-grey css-left" data-badge-caption="INGRESADO" style="font-weight: 300;background-color:#dfdfe3;color:#868a8e;"></span>';
    var _stDoc_pendiente = _stDoc_label +'<span class="new badge badge-pink css-left" data-badge-caption="PENDIENTE"></span>';
    var _stDoc_anulado = _stDoc_label +'<span class="new badge badge-grey css-left" data-badge-caption="ANULADO"></span>';
    var _stDoc_derivado = _stDoc_label +'<span class="new badge badge-yellow css-left" data-badge-caption="DERIVADO"></span>';
    var _stDoc_atendido = _stDoc_label +'<span class="new badge badge-blue css-left" data-badge-caption="ATENDIDO"></span>';
    var _stDoc_enproceso = _stDoc_label +'<span class="new badge badge-green css-left" data-badge-caption="EN PROCESO"></span>';
    var _stDoc_devuelto = _stDoc_label +'<span class="new badge badge-yellow css-left" data-badge-caption="DEVUELTO"></span>';
    var _EstadpDocumento = _stDoc_ingresado;
    switch (idestadodocumento) {
        case 1:
            _EstadpDocumento = _stDoc_ingresado;
            break;
        case 2:
            _EstadpDocumento = _stDoc_pendiente;
            break;
        case 3:
            _EstadpDocumento = _stDoc_anulado;
            break;
        case 4:
            _EstadpDocumento = _stDoc_derivado;
            break;
        case 5:
            _EstadpDocumento = _stDoc_atendido;
            break;
        case 6:
            _EstadpDocumento = _stDoc_enproceso;
            break;
        case 7:
            _EstadpDocumento = _stDoc_devuelto;
            break;
    }

    return _EstadpDocumento;
};

fc_ObtenerDestinatariosCC = function (lstDocDerivacion) {
    var urlBase = $("#hdUrlBase").val();
    var destinatariosCC = "";
    if (lstDocDerivacion !== null) {
        for (var i = 0; i < lstDocDerivacion.length; i++) {
            if (lstDocDerivacion[i].TipoAcceso === 1) {

                var firmante = fc_validarFirmante(lstDocDerivacion[i].CodDocumento, lstDocDerivacion[i].CodResponsable);

                destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreArea + ' - ' + lstDocDerivacion[i].NombreResponsable + '</div>' +
                    '<div class="col s2 m2"><span class="new badge badge-grey" data-badge-caption="DE"></span></div>'
                    + fc_ObtenerEstadoDocumentoHtml(lstDocDerivacion[i].EstadoDerivacion, false) + ((firmante == true) ? "<div class='col s1 m1'><img  src='" + urlBase + "/Content/img/icon-firma.png' class='responsive-img logo-mini tooltipped' data-position='right' data-tooltip='Firma Realizada'></div>" : "")+'</div >'
                    ;
            }
            else if (lstDocDerivacion[i].TipoAcceso === 2) {
                destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreArea + ' - ' + lstDocDerivacion[i].NombreResponsable + '</div>' +
                    '<div class="col s2 m2"><span class="new badge badge-grey" data-badge-caption="CC"></span></div></div>' +
                    '<div class="divcc" style="display:none">'+lstDocDerivacion[i].WEBUsrResponsable+'</div>';
            }
        }
    };
    return destinatariosCC;
}



fc_ObtenerDestinatariosRecepcionSIED = function (lstDocDerivacion) {
    var urlBase = $("#hdUrlBase").val();
    var destinatariosCC = "";
    if (lstDocDerivacion !== null) {
        for (var i = 0; i < lstDocDerivacion.length; i++) {
            if (lstDocDerivacion[i].TipoAcceso === 1) {

                var firmante = fc_validarFirmante(lstDocDerivacion[i].CodDocumento, lstDocDerivacion[i].CodResponsable);

                destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreArea + ' - ' + lstDocDerivacion[i].NombreResponsable + '</div>' +
                    '<div class="col s2 m2"><span class="new badge badge-grey" data-badge-caption="DE"></span></div>'
                    + fc_ObtenerEstadoDocumentoHtml(lstDocDerivacion[i].EstadoDerivacion, false) + ((firmante == true) ? "<div class='col s1 m1'><img  src='" + urlBase + "/Content/img/icon-firma.png' class='responsive-img logo-mini tooltipped' data-position='right' data-tooltip='Firma Realizada'></div>" : "") 
                    + (lstDocDerivacion[i].EstadoDerivacion == 3 ? "<div class='col s1 m1' > <i class='material-icons btn-anular-derivacion tooltipped' style='cursor: pointer;' data-tooltip='" + lstDocDerivacion[i].Comentarios + "'>chat</i></div >" : "") + '</div >'
                    ;
            }
            else if (lstDocDerivacion[i].TipoAcceso === 2) {
                destinatariosCC = destinatariosCC + '<div class="row"><div class="col s10 m7">' + lstDocDerivacion[i].NombreArea + ' - ' + lstDocDerivacion[i].NombreResponsable + '</div>' +
                    '<div class="col s2 m2"><span class="new badge badge-grey" data-badge-caption="CC"></span></div></div>' +
                    '<div class="divcc" style="display:none">' + lstDocDerivacion[i].WEBUsrResponsable + '</div>';
            }
        }
    };
    return destinatariosCC;
}

fc_validarFirmante = function (codDocumento,codTrabajador) {
    var urlBase = $("#hdUrlBase").val();
    var url = urlBase + "/Documento/ValidarFirmante";
    url += "?codDocumento=" + codDocumento + "&codTrabajador=" + codTrabajador;
    var esFirmante = false;
    $.ajax({
        url: url,
        dataType: "json",
        async: false,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (result) {
            if (result.data > 0) {
                esFirmante = true;
            }
        },
        error: function (e, b, c) {
            console.log(e.responseText);
        }
    });
    return esFirmante;
}