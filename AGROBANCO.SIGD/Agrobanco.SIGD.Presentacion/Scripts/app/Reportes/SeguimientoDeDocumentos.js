﻿$(document).ready(function () {


    var ReporteSeguimiento = (function () {


        //<input type="hidden" id="HD_nrotramite" name="HD_nrotramite" />
        //    <input type="hidden" id="HD_anio" name="HD_anio" />
        
        //cboAnoDoc

        var inputPlazo = $(".input-plazo");
        var inputDatePicker = $("input.datepicker");
        var lblFechaDefault = $("label.lbl-fecha-default");

        var SelectAreas = $("#cboArea");
        var SelectTipoDocumento = $("#cboTipoDocumento");
        var SelectcboAnoDoc = $("#cboAnoDoc");

        var CodArea = "";
        var CodTipoDocumento = "";
        var DescripcionArea = "";
        var longitudSelectEstado = 0;
        var varCodTipDoc = "";
        var varCodArea = "";
        var varNroTramite = "";
        var varAnio = "";
        $("#cboArea").val("");
        $("#cboTipoDocumento").val("");
        $("#cboAnoDoc").val("");
      

        var init = function () {
            //fechaInicio.on("change", fechaRegIniChange);
            //fechaFinal.on("change", fechaRegFinChange);

            var fechaHoy = new Date();
            var plazoDefault = parseInt($(inputPlazo).val());
            var currentMonth = fechaHoy.getMonth() + 1;
            var currentDay = fechaHoy.getDate();
            var strFecha = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaHoy.getFullYear();


            $(inputDatePicker).val(strFecha);
            $(lblFechaDefault).addClass("active");
            var newdate = new Date(fechaHoy);
            newdate.setDate(fechaHoy.getDate() - 30);
            var dd = newdate.getDate().toString();
            dd = dd.length > 1 ? dd : '0' + dd;
            var mm = (newdate.getMonth() + 1).toString();
            mm = mm.length > 1 ? mm : '0' + mm;

            var y = newdate.getFullYear();
            var someFormattedDate = dd + '/' + mm + '/' + y;
            $("#fecInicio").val(someFormattedDate);

            CodArea = "";
            CodTipoDocumento = "";
        };


        //function validarMinMax2Fechas(objFechaMin, objFechaMax) {
        //    var elems = $(objFechaMin);
        //    var instances = M.Datepicker.getInstance(elems);
        //    instances.options.minDate = stringToDate($(objFechaMax).val(), 'dd/MM/yyyy', '/');
        //    elems = $(objFechaMax);
        //    instances = M.Datepicker.getInstance(elems);
        //    instances.options.maxDate = stringToDate($(objFechaMin).val(), 'dd/MM/yyyy', '/');
        //}

        //var fechaRegIniChange = function () {

        //    var fecIni = $("#fecInicio").val().split("/");
        //    var fecFin = $("#fecFinal").val().split("/");
        //    fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
        //    fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
        //    if (fecIni > fecFin) {
        //        $('#ptv-alert-error').modal();
        //        $('.p-desc-error').text("La fecha de inicio no puede ser mayor a la fecha fin.");
        //        $('#ptv-alert-error').modal('open');
        //        $("#fecInicio").val($("#fecFinal").val());
        //    }
        //}

        //var fechaRegFinChange = function () {

        //    var fecIni = $("#fecInicio").val().split("/");
        //    var fecFin = $("#fecFinal").val().split("/");
        //    fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
        //    fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
        //    if (fecIni > fecFin) {
        //        $('#ptv-alert-error').modal();
        //        $('.p-desc-error').text("La fecha fin no puede ser menor a la fecha de inicio.");
        //        $('#ptv-alert-error').modal('open');
        //        $("#fecFinal").val($("#fecInicio").val());
        //    }
        //}

        obtenerAreasllenarSelect(SelectAreas);
        cargarSelectTipoDocumento(SelectTipoDocumento);
        obtenerAniosSelect(SelectcboAnoDoc);
        //cargarSelectEstadosDocumento(SelectEstadosDocumento);
        //longitudSelectEstado = $('#cboEstadoDocumento')[0].length;
        //Defacult Pendiente
        //$('#cboEstadoDocumento option[value=2]').attr('selected', 'selected');
        //$("#HD_codEstadoDoc").val("2");
        //$(SelectEstadosDocumento).append('<option value="0">Seleccione</option>');
        //$("#cboEstadoDocumento option[value=0]").attr('disabled', 'disabled')
        //$(SelectEstadosDocumento).formSelect();


        $('#cboArea').on('change', function () {
            varCodArea = "";
            varCodArea = fn_GetIDS(1);

            if (varCodArea.length > 0) {
                $('#cboArea option[value=""]').removeAttr('selected');
                varCodArea = varCodArea;
                $("#HD_codArea").val(varCodArea);
            } else {
                $('#cboArea option[value=""]').removeAttr('selected');
                varCodArea = "";
                $("#HD_codArea").val = "0";
            }
        });

        $('body').click(function (evt) {
            if (evt.target.className != "select-dropdown dropdown-trigger") {
                $("#cboTipoDocumento").formSelect();
                $("#cboArea").formSelect();
                $("#cboAnoDoc").formSelect();
                
            }

        });

        $('#cboTipoDocumento').on('change', function () {
            varCodTipDoc = "";
            varCodTipDoc = fn_GetIDS(2);

            if (varCodTipDoc.length > 0) {
                $('#cboTipoDocumento option[value=""]').removeAttr('selected');
                varCodTipDoc = varCodTipDoc;
                $("#HD_codTipDoc").val(varCodTipDoc);
            } else {
                varCodArea = "";
                $('#cboTipoDocumento option[value=""]').removeAttr('selected');
                $("#HD_codTipDoc").val = "0";
            }
        });


        $('#cboAnoDoc').on('change', function () {
            //varCodTipDoc = "";
            //varCodTipDoc = fn_GetIDS(2);
            debugger
            varAnio = "";
            varAnio = $('select[name="cboAnoDoc"] option:selected').text();
            //varAnio = $("#cboAnoDoc").val();
            if (varAnio == "Seleccione") {
                $("#HD_anio").val = "0";
            }
            else {
                $("#HD_anio").val = varAnio;
            }         


        });

      

        function fn_GetIDS(sw_Tipo) {
            var vc_Ul = "";
            if (sw_Tipo == 1) { vc_Ul = 'cboArea'; }
            else if (sw_Tipo == 2) { vc_Ul = 'cboTipoDocumento'; }

            var str = "";
            $("#" + vc_Ul + " option:selected").each(function () {
                str += $(this).val() + ",";
            });

            if (str.length > 0) {
                str = str.substring(0, str.length - 1);
            }
            return str;
        }

        $('#btnBuscar').on('click', function (event) {

            var varCodArea = fn_GetIDS(1);
            var varCodTipDoc = fn_GetIDS(2);

            if (varCodArea.length > 0) {
                varCodArea = varCodArea;
                $("#HD_codArea").val = varCodArea;
            } else {
                varCodArea = "";
                $("#HD_codArea").val = "0";
            }

            if (varCodTipDoc.length > 0) {
                varCodTipDoc = varCodTipDoc;
                $("#HD_codTipDoc").val = varCodTipDoc;
            } else {
                varCodTipDoc = "";
                $("#HD_codTipDoc").val = "0";
            }


            if (varCodArea.length > 0) {
            } else {
                $("#HD_codArea").val("0");
            }

            if (varCodTipDoc.length > 0) {
            } else {
                $("#HD_codTipDoc").val("0");
            }
            debugger
            
          
            if (varAnio == "Seleccione") {
                $("#HD_anio").val("0");
            }
            else {
                $("#HD_anio").val(varAnio);

            }
            varNroTramite = $("#nrotramite").val();
            $("#HD_nrotramite").val(varNroTramite);

        });

               
        $('#btnPaginacion').on('click', function (event) {
            //alert("hola");
            var varCodArea = fn_GetIDS(1);
            var varCodTipDoc = fn_GetIDS(2);
           

            if (varCodArea.length > 0) {
                varCodArea = varCodArea;
                $("#HD_codArea").val = varCodArea;
            } else {
                varCodArea = "";
                $("#HD_codArea").val = "0";
            }

            if (varCodTipDoc.length > 0) {
                varCodTipDoc = varCodTipDoc;
                $("#HD_codTipDoc").val = varCodTipDoc;
            } else {
                varCodTipDoc = "";
                $("#HD_codTipDoc").val = "0";
            }


            if (varCodArea.length > 0) {
            } else {
                $("#HD_codArea").val("0");
            }

            if (varCodTipDoc.length > 0) {
            } else {
                $("#HD_codTipDoc").val("0");
            }
            debugger

            
            if (varAnio == "Seleccione") {
                $("#HD_anio").val("0");
            }
            else {
                $("#HD_anio").val(varAnio);
            }

            varNroTramite = $("#nrotramite").val();
            $("#HD_nrotramite").val(varNroTramite);

        });


        $('#btnReiniciar').on('click', function (event) {
            debugger
            //obtenerAreasllenarSelect(SelectAreas);
            //cargarSelectTipoDocumento(SelectTipoDocumento);
            //obtenerAniosSelect(SelectcboAnoDoc);
            $("#cboAnoDoc").val("");
            $("#cboArea").val("");
            $("#cboTipoDocumento").val("");
            $("#nrotramite").val("");
            $("#HD_codArea").val("0");
            $("#HD_codTipDoc").val("0");
            $("#HD_anio").val("0");
            $("#HD_nrotramite").val("");
            //var varCodArea = fn_GetIDS(1);
            //var varCodTipDoc = fn_GetIDS(2);

            //if (varCodArea.length > 0) {
            //    varCodArea = varCodArea;
            //    $("#HD_codArea").val = varCodArea;
            //} else {
            //    varCodArea = "";
            //    $("#HD_codArea").val = "0";
            //}

            //if (varCodTipDoc.length > 0) {
            //    varCodTipDoc = varCodTipDoc;
            //    $("#HD_codTipDoc").val = varCodTipDoc;
            //} else {
            //    varCodTipDoc = "";
            //    $("#HD_codTipDoc").val = "0";
            //}


            //if (varCodArea.length > 0) {
            //} else {
            //    $("#HD_codArea").val("0");
            //}

            //if (varCodTipDoc.length > 0) {
            //} else {
            //    $("#HD_codTipDoc").val("0");
            //}
            //debugger


            //if (varAnio == "Seleccione") {
            //    $("#HD_anio").val("0");
            //}
            //else {
            //    $("#HD_anio").val(varAnio);

            //}
            //varNroTramite = $("#nrotramite").val();
            //$("#HD_nrotramite").val(varNroTramite);

        });

                              
        return {
            init: init
        };

    })();

    ReporteSeguimiento.init();

});


