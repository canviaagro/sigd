﻿$(document).ready(function () {


    var AtencionDocumento = (function () {
        var fechaInicio = $("#fecInicio");
        var fechaFinal = $("#fecFinal");
        var inputDatePicker = $("input.datepicker");
        var lblFechaDefault = $("label.lbl-fecha-default");

        var SelectAreaResponsable  = $("#cboAreaResponsable");
        var SelectAreaDerivada = $("#cboAreaDerivada");
        var SelectTipoDocumento = $("#cboTipoDocumento");
        var SelectEstadoDocumento = $("#cboEstadoDocumento");
        var SelectFirma = $("#cboFirmado");

        var init = function () {
            fechaInicio.on("change", fechaRegIniChange);
            fechaFinal.on("change", fechaRegFinChange);     

            var fechaHoy = new Date();
            var currentMonth = fechaHoy.getMonth() + 1;
            var currentDay = fechaHoy.getDate();
            var strFecha = lpad(currentDay.toString(), "0", 2) + "/" + lpad(currentMonth.toString(), "0", 2) + "/" + fechaHoy.getFullYear();

            $(inputDatePicker).val(strFecha);
            $(lblFechaDefault).addClass("active");
            var newdate = new Date(fechaHoy);
            newdate.setDate(fechaHoy.getDate() - 30);
            var dd = newdate.getDate().toString();
            dd = dd.length > 1 ? dd : '0' + dd;
            var mm = (newdate.getMonth() + 1).toString();
            mm = mm.length > 1 ? mm : '0' + mm;

            var y = newdate.getFullYear();
            var someFormattedDate = dd + '/' + mm + '/' + y;
            $("#fecInicio").val(someFormattedDate);

            CodArea = "";
            CodTipoDocumento = "";
        };

        obtenerAreasllenarSelect(SelectAreaResponsable);
        obtenerAreasllenarSelect(SelectAreaDerivada);
        cargarSelectTipoDocumento(SelectTipoDocumento);
        cargarSelectEstadosDocumento(SelectEstadoDocumento);
        obtenerAfirmacion(SelectFirma);

        $(SelectAreaResponsable).on('change', function () {
            var codAreaResponsable = M.FormSelect.getInstance(SelectAreaResponsable).getSelectedValues();
            if (codAreaResponsable.length > 0) {
                $('#cboAreaResponsable option[value=""]').removeAttr('selected');
            }
            $("#HD_codAreaResponsable").val(codAreaResponsable);
        });

        $(SelectAreaDerivada).on('change', function () {
            var codAreaDerivada = M.FormSelect.getInstance(SelectAreaDerivada).getSelectedValues();
            if (codAreaDerivada.length > 0) {
                $('#cboAreaDerivada option[value=""]').removeAttr('selected');
            }
            $("#HD_codAreaDerivada").val(codAreaDerivada);
        });

        $(SelectTipoDocumento).on('change', function () {
            var codTipoDocumeno = M.FormSelect.getInstance(SelectTipoDocumento).getSelectedValues();
            if (codTipoDocumeno.length > 0) {
                $('#cboTipoDocumento option[value=""]').removeAttr('selected');
            }
            $("#HD_codTipDoc").val(codTipoDocumeno);
        });

        $(SelectEstadoDocumento).on('change', function () {
            var codEstadoDocumento = M.FormSelect.getInstance(SelectEstadoDocumento).getSelectedValues();
            if (codEstadoDocumento.length > 0) {
                $('#cboEstadoDocumento option[value=""]').removeAttr('selected');
            }
            $("#HD_EstadoDoc").val(codEstadoDocumento);
        });


        $('body').click(function (evt) {
            if (evt.target.className != "select-dropdown dropdown-trigger") {
                $(SelectAreaResponsable).formSelect();
                $(SelectAreaDerivada).formSelect();
                $(SelectTipoDocumento).formSelect();
                $(SelectEstadoDocumento).formSelect();
            }

        });

        $(SelectTipoDocumento).on('change', function () {

        });

        var fechaRegIniChange = function () {

            var fecIni = $("#fecInicio").val().split("/");
            var fecFin = $("#fecFinal").val().split("/");
            fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
            fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
            if (fecIni > fecFin) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("La fecha de inicio no puede ser mayor a la fecha fin.");
                $('#ptv-alert-error').modal('open');
                $("#fecInicio").val($("#fecFinal").val());
            }
        }

        var fechaRegFinChange = function () {

            var fecIni = $("#fecInicio").val().split("/");
            var fecFin = $("#fecFinal").val().split("/");
            fecIni = new Date(fecIni[2], fecIni[1] - 1, fecIni[0], 0, 0, 0, 0);
            fecFin = new Date(fecFin[2], fecFin[1] - 1, fecFin[0], 0, 0, 0, 0);
            if (fecIni > fecFin) {
                $('#ptv-alert-error').modal();
                $('.p-desc-error').text("La fecha fin no puede ser menor a la fecha de inicio.");
                $('#ptv-alert-error').modal('open');
                $("#fecFinal").val($("#fecInicio").val());
            }
        } 


        $('#btnBuscar').on('click', function (event) {

            if (fechaInicio.val() == "" || fechaFinal.val() == "") {
                $('#ptv-alert-error').modal({
                    onCloseEnd: function () {
                    }
                });
                $('.p-desc-error').text("Por favor especificar un rango de fechas válidas");
                $('#ptv-alert-error').modal('open');
                return false;
            }
        });
        
                      
        return {
            init: init
        };

    })();

    AtencionDocumento.init();

});


