﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Utilities
{
    public class UrlApi
    {

        public static string ObtenerUrl(string Descripcion)
        {

            return ConfigurationUtilities.WebApiUrl + Descripcion;
        }


        public static string ObtenerUrlBase(string Descripcion)
        {

            return ConfigurationUtilities.urlbase + Descripcion;
        }
    }
}