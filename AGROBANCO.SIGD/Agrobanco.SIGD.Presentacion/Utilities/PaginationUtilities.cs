﻿using PagedList.Mvc;

namespace Agrobanco.SIGD.Presentacion.Utilities
{
    public static class PaginationUtilities
    {
        ///<summary>
        ///	Shows only the previous and next images with the page count .
        ///</summary>
        public static PagedListRenderOptions PageImageCount
        {
            get
            {
                return new PagedListRenderOptions
                {
                    DisplayLinkToFirstPage = PagedListDisplayMode.Never,
                    DisplayLinkToLastPage = PagedListDisplayMode.Never,
                    DisplayLinkToPreviousPage = PagedListDisplayMode.Always,
                    DisplayLinkToNextPage = PagedListDisplayMode.Always,
                    DisplayLinkToIndividualPages = false,
                    PageCountAndCurrentLocationFormat = "{0} de {1}",
                    DisplayPageCountAndCurrentLocation = true,
                    LinkToPreviousPageFormat = string.Empty,
                    LinkToNextPageFormat = string.Empty,
                };
            }
        }
    }
}