﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Utilities
{
    public static class ExtensionMethods
    {
        public static int ToInt(this object obj)
        {
            return Convert.ToInt32(obj);
        }
        public static int ToIntDefault(this int? obj)
        {
            return obj.HasValue ? obj.Value : 0;
        }
        public static int ToIntDefault(this int? obj, int defaultValue)
        {
            return obj.HasValue ? obj.Value : defaultValue;
        }
        public static bool TryParseToInt(this string obj)
        {
            int temp;
            return int.TryParse(obj, out temp);
        }
        public static bool ToBool(this object obj)
        {
            return Convert.ToBoolean(obj);
        }
        public static char ToChar(this object obj)
        {
            return Convert.ToChar(obj);
        }
        public static decimal ToDecimal(this object obj)
        {
            var ui = new CultureInfo("es-PE");
            ui.NumberFormat = new NumberFormatInfo() { NumberDecimalSeparator = "." };
            return Convert.ToDecimal(obj, ui);
        }
        public static decimal ToDecimal(this decimal? obj)
        {
            return obj.HasValue ? obj.Value : 0;
        }
        public static decimal? ToNullableDecimal(this decimal obj)
        {
            return obj != 0 ? (decimal?)obj : default(decimal?);
        }
        public static decimal ToDecimalZeros(this decimal obj, int numberOfDecimals)
        {
            return Math.Round(obj, numberOfDecimals);
        }
        public static double ToDouble(this object obj)
        {
            return Convert.ToDouble(obj);
        }
        public static string ToDecimalPointFormat(this decimal obj)
        {
            return obj.ToString("0.00", new NumberFormatInfo() { NumberDecimalSeparator = "." });
        }
        public static string ToDecimalPointFormat(this decimal obj, string format)
        {
            return obj.ToString(format, new NumberFormatInfo() { NumberDecimalSeparator = "." });
        }
        public static decimal? ToNullableDecimal(this object obj)
        {
            return (decimal?)obj;
        }
        public static DateTime ToDatetime(this string obj)
        {
            return DateTime.Parse(obj);
        }
        public static DateTime ToDatetime(this string obj, string format)
        {
            return DateTime.ParseExact(obj, format, null);
        }
        public static DateTime? ToNullableDatetime(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj) ? default(DateTime?) : obj.ToDatetime();
        }
        public static DateTime? ToNullableDatetime(this string obj, double addedDays)
        {
            return string.IsNullOrWhiteSpace(obj) ? default(DateTime?) : obj.ToDatetime().AddDays(addedDays);
        }
        public static Nullable<int> ToNullableInt(this object obj)
        {
            return (int?)obj;
        }
        public static Nullable<DateTime> ToNullableDatetime(this DateTime date)
        {
            return (DateTime?)date;
        }
        public static Nullable<TimeSpan> ToNullableTimespan(this TimeSpan obj)
        {
            return (Nullable<TimeSpan>)obj;
        }
        public static Nullable<TimeSpan> ToNullableTimespan(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj) ? default(Nullable<TimeSpan>) : new TimeSpan(obj.Split(':')[0].ToInt(), obj.Split(':')[1].ToInt(), 0).ToNullableTimespan();
        }
        public static string ToFullFormattedStringDate(this DateTime obj)
        {
            return obj.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
        }
        public static string ToFormattedDate(this Nullable<DateTime> obj)
        {
            return obj.HasValue ? obj.Value.ToFormattedDate() : string.Empty;
        }
        public static string ToFormattedDate(this DateTime obj)
        {
            return obj.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        public static string ToFormattedDate(this DateTime obj, string format)
        {
            return obj.ToString(format, CultureInfo.InvariantCulture);
        }
        public static string ToFormattedTime(this DateTime obj)
        {
            return obj.ToString("hh:mm tt", CultureInfo.InvariantCulture);
        }
        public static string ToFormattedTime12Hours(this DateTime obj)
        {
            return obj.ToString("hh:mm tt", CultureInfo.InvariantCulture);
        }
        public static string ToFormattedTime24Hours(this DateTime obj)
        {
            return obj.ToString("HH:mm", CultureInfo.InvariantCulture);
        }
        public static string ToFormattedTime(this DateTime obj, string format)
        {
            return obj.ToString(format, CultureInfo.InvariantCulture);
        }
        public static string ToTimeHourMinuteFormat(this TimeSpan obj)
        {
            return obj.ToString(@"hh\:mm");
        }
        public static string ToTimeHourMinuteFormat(this DateTime obj)
        {
            return obj.ToString(@"hh\:mm");
        }
        public static string ToTimeHourMinuteFormat(this Nullable<TimeSpan> obj)
        {
            return obj.HasValue ? obj.Value.ToString(@"hh\:mm") : "00:00";
        }
        public static byte[] ToBytesFromBase64String(this string obj)
        {
            return Convert.FromBase64String(obj);
        }
        public static string ToFileFormattedStringDate(this DateTime obj)
        {
            return obj.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
        }
        public static string ToFormattedMoneySolesString(this object obj)
        {
            return string.Format(CultureInfo.GetCultureInfo("es-PE"), "{0:C}", obj);
        }
        public static string ToFormattedMoneyDolarString(this object obj)
        {
            return string.Format(CultureInfo.GetCultureInfo("en-US"), "{0:C}", obj);
        }
        public static string ToFormattedPercentString(this object obj)
        {
            return string.Format(CultureInfo.GetCultureInfo("es-PE"), "{0:P}", obj.ToDecimal() / 100);
        }
        public static string ToStringWithSlash(this object obj)
        {
            return obj.ToString() + "/";
        }
        public static string ToMoneyFormat(this decimal obj)
        {
            return string.Format("S/. {0:0.00}", obj);
        }
        public static string TrimNullable(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj) ? string.Empty : obj.Trim();
        }
        public static byte[] ToBytes(this string obj)
        {
            return Encoding.UTF8.GetBytes(obj);
        }
        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static string ToStringDefault(this string obj, string defaultValue)
        {
            return string.IsNullOrWhiteSpace(obj) ? defaultValue : obj;
        }
        public static string ToStringEmpty(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj) ? string.Empty : obj;
        }
        public static string ToCapitallize(this string obj)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(obj.ToStringEmpty().ToLower());
        }
        public static string ToCapitallizeFirstLetter(this string obj)
        {
            obj = obj.ToStringEmpty();

            switch (obj.Length)
            {
                case 0:
                    return string.Empty;
                case 1:
                    return obj.ToUpper();
                default:
                    return obj.First().ToString().ToUpper() + obj.Substring(1);
            }
        }
        public static decimal GetIGV(this decimal obj)
        {
            return obj * 18 / 100;
        }
        public static decimal GetWithoutIGV(this decimal obj)
        {
            return obj * 82 / 100;
        }
        public static void AddDuplicate<TKey, TValue>(this Dictionary<TKey, List<TValue>> obj, TKey key, TValue value)
            where TKey : new()
            where TValue : new()
        {
            if (obj.ContainsKey(key))
            {
                obj[key].Add(value);
            }
            else
            {
                obj.Add(key, new List<TValue> { value });
            }
        }
        public static void SetInTempData(this ControllerBase obj, object variable)
        {
            obj.TempData[variable.GetType().GetProperties()[0].Name] = variable;
        }
        public static void SetInTempData(this ControllerBase obj, string key, object value)
        {
            obj.TempData[key] = value;
        }




        public static string ToOriginalFileName(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj) ? string.Empty : obj.Trim().Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
        }
        public static string ToDomainUrl(this string obj)
        {
            return string.IsNullOrWhiteSpace(obj) ? string.Empty : obj.Trim().Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
        }
    }
}