﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Proxy
{
    public class ProxyApi
    {
        public string token { get; set; }
        public ProxyApi(string token)
        {
            this.token = token;
        }
        public ResultadoBusquedaModel<DocumentoSeguimientoDTO> ProxyGetSeguimientoDocumento(int id)
        {
            //ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>("https://localhost:44307/api/documento/GetSeguimientoDocumento?id=" + id, token);
            ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>(UrlApi.ObtenerUrl("/documento/GetSeguimientoDocumento?id=") + id, token);
            return lstDoc;
        }

        public ResultadoBusquedaModel<DocumentoSeguimientoDTO> ProxyGetCabeceraSeguimiento(int id)
        {
            //ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>("https://localhost:44307/api/documento/GetCabeceraSeguimiento?id=" + id, token);
            ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>(UrlApi.ObtenerUrl("/documento/GetCabeceraSeguimiento?id=") + id, token);
            return lstDoc;
        }


        public ResultadoBusquedaModel<DocumentoSeguimientoDTO> ProxyGetCabeceraSeguimientoInterno(int id)
        {
            //ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>("https://localhost:44307/api/documento/GetCabeceraSeguimientoInterno?id=" + id, token);
            ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>(UrlApi.ObtenerUrl("/documento/GetCabeceraSeguimientoInterno?id=") + id, token);
            return lstDoc;
        }


        public ResultadoBusquedaModel<DocumentoSeguimientoDTO> ProxyGetDetalleSeguimiento(int id)
        {
            //ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>("https://localhost:44307/api/documento/GetDetalleSeguimiento?id=" + id, token);
            ResultadoBusquedaModel<DocumentoSeguimientoDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoSeguimientoDTO>>(UrlApi.ObtenerUrl("/documento/GetDetalleSeguimiento?id=") + id, token);
            return lstDoc;
        }


        public ResultadoBusquedaModel<Entidades.Entidades.DocDerivacion> ProxyGetDocumentosDerivados(int id)
        {
            //ResultadoBusquedaModel<Entidades.Entidades.DocDerivacion> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<Entidades.Entidades.DocDerivacion>>("https://localhost:44307/api/documento/GetDocumentoDerivado?id=" + id, token);
            ResultadoBusquedaModel<Entidades.Entidades.DocDerivacion> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<Entidades.Entidades.DocDerivacion>>(UrlApi.ObtenerUrl("/documento/GetDocumentoDerivado?id=") + id, token);
            return lstDoc;
        }

        public ResultadoBusquedaModel<ComentarioDocumentoModel> ProxyGetComentarios(int id)
        {
            //ResultadoBusquedaModel<ComentarioDocumentoModel> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<ComentarioDocumentoModel>>("https://localhost:44307/api/documento/GetComentariosDocumento?id=" + id, token);
            ResultadoBusquedaModel<ComentarioDocumentoModel> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<ComentarioDocumentoModel>>(UrlApi.ObtenerUrl("/documento/GetComentariosDocumento?id=") + id, token);
            return lstDoc;
        }

        public ResultadoBusquedaModel<DocAnexo> ProxyGetDocumentoAnexos(int id)
        {
            //ResultadoBusquedaModel<DocAnexo> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocAnexo>>("https://localhost:44307/api/documento/GetDocumentoAnexos?id=" + id, token);
            ResultadoBusquedaModel<DocAnexo> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocAnexo>>(UrlApi.ObtenerUrl("/documento/GetDocumentoAnexos?id=") + id, token);

            return lstDoc;
        }

        public ResultadoBusquedaModel<DocAnexo> ProxyGetDocumentoAnexosNoLF(int id)
        {
            //ResultadoBusquedaModel<DocAnexo> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocAnexo>>("https://localhost:44307/api/documento/GetDocumentoAnexos?id=" + id, token);
            ResultadoBusquedaModel<DocAnexo> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocAnexo>>(UrlApi.ObtenerUrl("/documento/GetDocumentoAnexosNoLF?id=") + id, token);

            return lstDoc;
        }

        public async Task<ResultadoBusquedaModel<DocumentoBandejaDTO>> ProxyListarBandejaDocumentos(int tipobandeja, int? page, string searchString, string FechaDesde, string FechaHasta, int pageSize, int iCodTrabajador, string vCodPerfil)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>("https://localhost:44307/api/documento/GetDocumentoBandejaEntrada/" + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandejaEntrada/") + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);

            return lstDoc;
        }

        public async Task<ResultadoBusquedaModel<DocumentoBandejaDTO>> ProxyListarBandejaDocumentosRecepcionSIED(int tipobandeja, int? page, string searchString, string FechaDesde, string FechaHasta, int pageSize, int iCodTrabajador, string vCodPerfil)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>("https://localhost:44307/api/documento/GetDocumentoBandejaEntrada/" + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandejaRecepcionSIED/") + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);

            return lstDoc;
        }

        public  ResultadoBusquedaModel<DocumentoBandejaDTO> ProxyListarBandejaDocumentosRecepcionSIED_Sync(int tipobandeja, int? page, string searchString, string FechaDesde, string FechaHasta, int pageSize, int iCodTrabajador, string vCodPerfil)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>("https://localhost:44307/api/documento/GetDocumentoBandejaEntrada/" + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc =  new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandejaRecepcionSIED/") + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);

            return lstDoc;
        }

        public async Task<ResultadoBusquedaModel<DocumentoBandejaDTO>> ProxyListarBandejaDocumentoseEnvioSIED( int? page, string searchString, string FechaDesde, string FechaHasta, int pageSize, int iCodTrabajador)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>("https://localhost:44307/api/documento/GetDocumentoBandejaEntrada/" + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = await Service.GetAsync<ResultadoBusquedaModel<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandejaEnvioSIED/") +  searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador, token);

            return lstDoc;
        }

        public ResultadoBusquedaModel<DocumentoBandejaDTO> ProxyListarBandejaDocumentoseEnvioSIED_Sync(int? page, string searchString, string FechaDesde, string FechaHasta, int pageSize, int iCodTrabajador)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoBandejaDTO>>("https://localhost:44307/api/documento/GetDocumentoBandejaEntrada/" + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandejaEnvioSIED/") + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador, token);
            return lstDoc;
        }

        public ResultadoBusquedaModel<DocumentoBandejaDTO> ProxyListarBandejaDocumentos_Sync(int tipobandeja, int? page, string searchString, string FechaDesde, string FechaHasta, int pageSize, int iCodTrabajador, string vCodPerfil)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoBandejaDTO>>("https://localhost:44307/api/documento/GetDocumentoBandejaEntrada/" + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            ResultadoBusquedaModel<DocumentoBandejaDTO> lstDoc = new Service().HttpGetJson<ResultadoBusquedaModel<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandejaEntrada/") + tipobandeja + "/" + searchString + "/" + FechaDesde + "/" + FechaHasta + "/" + page + "/" + pageSize + "/" + iCodTrabajador + "/" + vCodPerfil, token);
            return lstDoc;
        }
        ///
        public async Task<ResultadoBusquedaModel<Asunto>> ProxyListarAsuntos(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Asunto> lst = await Service.GetAsync<ResultadoBusquedaModel<Asunto>>(ConfigurationUtilities.WebApiUrl + "/asunto/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Asunto> lst = await Service.GetAsync<ResultadoBusquedaModel<Asunto>>(UrlApi.ObtenerUrl("/asunto/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }
        public ResultadoBusquedaModel<Asunto> ProxyListarAsuntos_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<Asunto> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Asunto>>(ConfigurationUtilities.WebApiUrl + "/asunto/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Asunto> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Asunto>>(UrlApi.ObtenerUrl("/asunto/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<Feriado>> ProxyListarFeriados(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Feriado> lst = await Service.GetAsync<ResultadoBusquedaModel<Feriado>>(ConfigurationUtilities.WebApiUrl + "/feriado/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Feriado> lst = await Service.GetAsync<ResultadoBusquedaModel<Feriado>>(UrlApi.ObtenerUrl("/feriado/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public ResultadoBusquedaModel<Feriado> ProxyListarFeriados_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Feriado> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Feriado>>(ConfigurationUtilities.WebApiUrl + "/feriado/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Feriado> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Feriado>>(UrlApi.ObtenerUrl("/feriado/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }
        public async Task<ResultadoBusquedaModel<Empresa>> ProxyListarEmpresa(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Empresa> lst = await Service.GetAsync<ResultadoBusquedaModel<Empresa>>(ConfigurationUtilities.WebApiUrl + "/empresa/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Empresa> lst = await Service.GetAsync<ResultadoBusquedaModel<Empresa>>(UrlApi.ObtenerUrl("/empresa/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }
        public ResultadoBusquedaModel<Empresa> ProxyListarEmpresa_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Empresa> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Empresa>>(ConfigurationUtilities.WebApiUrl + "/empresa/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Empresa> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Empresa>>(UrlApi.ObtenerUrl("/empresa/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<Representante>> ProxyListarRepresentante(int? page, string searchString, int codEmpresa, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Representante> lst = await Service.GetAsync<ResultadoBusquedaModel<Representante>>(ConfigurationUtilities.WebApiUrl + "/representante/Find/" + codEmpresa + "/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Representante> lst = await Service.GetAsync<ResultadoBusquedaModel<Representante>>(UrlApi.ObtenerUrl("/representante/Find/") + codEmpresa + "/" + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }
        public ResultadoBusquedaModel<Representante> ProxyListarRepresentante_Sync(int? page, string searchString, int codEmpresa, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString.Trim();
            //ResultadoBusquedaModel<Representante> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Representante>>(ConfigurationUtilities.WebApiUrl + "/representante/Find/" + codEmpresa + "/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Representante> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Representante>>(UrlApi.ObtenerUrl("/representante/Find/") + codEmpresa + "/" + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<TipoDocumento>> ProxyListarTipoDocumento(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<TipoDocumento> lst = await Service.GetAsync<ResultadoBusquedaModel<TipoDocumento>>(ConfigurationUtilities.WebApiUrl + "/documento/GetTiposDocumentos/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<TipoDocumento> lst = await Service.GetAsync<ResultadoBusquedaModel<TipoDocumento>>(UrlApi.ObtenerUrl("/documento/GetTiposDocumentos/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }
        public ResultadoBusquedaModel<TipoDocumento> ProxyListarTipoDocumento_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<TipoDocumento> lst = new Service().HttpGetJson<ResultadoBusquedaModel<TipoDocumento>>(ConfigurationUtilities.WebApiUrl + "/documento/GetTiposDocumentos/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<TipoDocumento> lst = new Service().HttpGetJson<ResultadoBusquedaModel<TipoDocumento>>(UrlApi.ObtenerUrl("/documento/GetTiposDocumentos/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<Secuencia>> ProxyListarSecuencia(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<Secuencia> lst = await Service.GetAsync<ResultadoBusquedaModel<Secuencia>>(ConfigurationUtilities.WebApiUrl + "/Secuencia/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Secuencia> lst = await Service.GetAsync<ResultadoBusquedaModel<Secuencia>>(UrlApi.ObtenerUrl("/Secuencia/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }
        public ResultadoBusquedaModel<Secuencia> ProxyListarSecuencia_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<Secuencia> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Secuencia>>(ConfigurationUtilities.WebApiUrl + "/Secuencia/Find/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Secuencia> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Secuencia>>(UrlApi.ObtenerUrl("/Secuencia/Find/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<Area>> ProxyListarArea(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<Area> lst = await Service.GetAsync<ResultadoBusquedaModel<Area>>(ConfigurationUtilities.WebApiUrl + "/area/GetListaAreas/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Area> lst = await Service.GetAsync<ResultadoBusquedaModel<Area>>(UrlApi.ObtenerUrl("/area/GetListaAreas/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }


        public ResultadoBusquedaModel<Area> ProxyListarArea_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;
            //ResultadoBusquedaModel<Area> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Area>>(ConfigurationUtilities.WebApiUrl + "/area/GetListaAreas/" + searchString + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<Area> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Area>>(UrlApi.ObtenerUrl("/area/GetListaAreas/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<ReportesMesaPartes>> ProxyReporteMesaPartes(int? page, int pageSize, string codAreas, string codTipoDoc, string fecInicio, string fecFinal, string codEstadoDocumento)
        {
            codAreas = String.IsNullOrEmpty(codAreas) ? "0" : codAreas;
            codTipoDoc = String.IsNullOrEmpty(codTipoDoc) ? "0" : codTipoDoc;
            
            //ResultadoBusquedaModel<ReportesMesaPartes> lst = await Service.GetAsync<ResultadoBusquedaModel<ReportesMesaPartes>>(ConfigurationUtilities.WebApiUrl + "/reportes/GetReporteMesaPartes/" + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<ReportesMesaPartes> lst = await Service.GetAsync<ResultadoBusquedaModel<ReportesMesaPartes>>(UrlApi.ObtenerUrl("/reportes/GetReporteMesaPartes/") + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public async Task<ResultadoBusquedaModel<ReportesAtencionDocumentos>> ProxyReporteAtencionDocumentos(int? page, int pageSize, string nrotramite, string fecInicio, string fecFinal, string codAreaResponsable,
                                                                                                    string codAreaDerivada, string codTipDoc, string EstadoDoc, string cboFirmado)
        {
            nrotramite = String.IsNullOrEmpty(nrotramite) ? "VACIO" : nrotramite;
            ResultadoBusquedaModel<ReportesAtencionDocumentos> lst = await Service.GetAsync<ResultadoBusquedaModel<ReportesAtencionDocumentos>>(UrlApi.ObtenerUrl("/reportes/GetReporteAtencionDocumentos/") + nrotramite + "/" + fecInicio + "/" + fecFinal + "/" + codAreaResponsable + "/" + codAreaDerivada + "/" + codTipDoc + "/"  + EstadoDoc + "/" + cboFirmado + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public ResultadoBusquedaModel<ReportesAtencionDocumentos> ProxyReporteAtencionDocumentos_Sync(int? page, int pageSize, string nrotramite, string fecInicio, string fecFinal, string codAreaResponsable,
                                                                                            string codAreaDerivada, string codTipDoc, string EstadoDoc, string cboFirmado)
        {
            nrotramite = String.IsNullOrEmpty(nrotramite) ? "VACIO" : nrotramite;
            ResultadoBusquedaModel<ReportesAtencionDocumentos> lst = new Service().HttpGetJson<ResultadoBusquedaModel<ReportesAtencionDocumentos>>(UrlApi.ObtenerUrl("/reportes/GetReporteAtencionDocumentos/") + nrotramite + "/" + fecInicio + "/" + fecFinal + "/" + codAreaResponsable + "/" + codAreaDerivada + "/" + codTipDoc + "/" + EstadoDoc + "/" + cboFirmado + "/" + page + "/" + pageSize, token);
            return lst;
        }

        public ResultadoBusquedaModel<ReportesMesaPartes> ProxyReporteMesaPartes_Sync(int? page, int pageSize, string codAreas, string codTipoDoc, string fecInicio, string fecFinal, string codEstadoDocumento)
        {

            codAreas = String.IsNullOrEmpty(codAreas) ? "0" : codAreas;
            codTipoDoc = String.IsNullOrEmpty(codTipoDoc) ? "0" : codTipoDoc;

            //ResultadoBusquedaModel<ReportesMesaPartes> lst = new Service().HttpGetJson<ResultadoBusquedaModel<ReportesMesaPartes>>(ConfigurationUtilities.WebApiUrl + "/reportes/GetReporteMesaPartes/" + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<ReportesMesaPartes> lst = new Service().HttpGetJson<ResultadoBusquedaModel<ReportesMesaPartes>>(UrlApi.ObtenerUrl("/reportes/GetReporteMesaPartes/") + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            return lst;
        }


        public async Task<ResultadoBusquedaModel<Trabajador>> ProxyListaTrabajador(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;            
            ResultadoBusquedaModel<Trabajador> lst = await Service.GetAsync<ResultadoBusquedaModel<Trabajador>>(UrlApi.ObtenerUrl("/trabajador/GetListaTrabajador/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }



        public ResultadoBusquedaModel<Trabajador> ProxyListaTrabajador_Sync(int? page, string searchString, int pageSize)
        {
            searchString = String.IsNullOrEmpty(searchString) ? "VACIO" : searchString;            
            ResultadoBusquedaModel<Trabajador> lst = new Service().HttpGetJson<ResultadoBusquedaModel<Trabajador>>(UrlApi.ObtenerUrl("/trabajador/GetListaTrabajador/") + searchString + "/" + page + "/" + pageSize, token);
            return lst;
        }

        /// <summary>
        /// Devuelte lista de documentos Internos
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="codAreas"></param>
        /// <param name="codTipoDoc"></param>
        /// <param name="fecInicio"></param>
        /// <param name="fecFinal"></param>
        /// <param name="codEstadoDocumento"></param>
        /// <returns></returns>
        public async Task<ResultadoBusquedaModel<ReportesMesaPartes>> ProxyReporteAreafechaEstado(int? page, int pageSize, string codAreas, string codTipoDoc, string fecInicio, string fecFinal, string codEstadoDocumento)
        {
            codAreas = String.IsNullOrEmpty(codAreas) ? "0" : codAreas;
            codTipoDoc = String.IsNullOrEmpty(codTipoDoc) ? "0" : codTipoDoc;

            //ResultadoBusquedaModel<ReportesMesaPartes> lst = await Service.GetAsync<ResultadoBusquedaModel<ReportesMesaPartes>>(ConfigurationUtilities.WebApiUrl + "/reportes/GetReporteMesaPartes/" + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<ReportesMesaPartes> lst = await Service.GetAsync<ResultadoBusquedaModel<ReportesMesaPartes>>(UrlApi.ObtenerUrl("/reportes/GetReporteAreaFechaEstado/") + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            return lst;
        }

        /// <summary>
        /// Devuelve Lista de documentos internos
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="codAreas"></param>
        /// <param name="codTipoDoc"></param>
        /// <param name="fecInicio"></param>
        /// <param name="fecFinal"></param>
        /// <param name="codEstadoDocumento"></param>
        /// <returns></returns>
        public ResultadoBusquedaModel<ReportesMesaPartes> ProxyReporteAreafechaEstado_Sync(int? page, int pageSize, string codAreas, string codTipoDoc, string fecInicio, string fecFinal, string codEstadoDocumento)
        {

            codAreas = String.IsNullOrEmpty(codAreas) ? "0" : codAreas;
            codTipoDoc = String.IsNullOrEmpty(codTipoDoc) ? "0" : codTipoDoc;

            //ResultadoBusquedaModel<ReportesMesaPartes> lst = new Service().HttpGetJson<ResultadoBusquedaModel<ReportesMesaPartes>>(ConfigurationUtilities.WebApiUrl + "/reportes/GetReporteMesaPartes/" + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            ResultadoBusquedaModel<ReportesMesaPartes> lst = new Service().HttpGetJson<ResultadoBusquedaModel<ReportesMesaPartes>>(UrlApi.ObtenerUrl("/reportes/GetReporteAreaFechaEstado/") + codAreas + "/" + codTipoDoc + "/" + fecInicio + "/" + fecFinal + "/" + codEstadoDocumento + "/" + page + "/" + pageSize, token);
            return lst;
        }
        
      
        /// <summary>
        /// Devuelte lista de documentos de Entrada
        /// </summary>
        /// <param name="nrotramite"></param>
        /// <param name="anio"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="codAreas"></param>
        /// <param name="codTipoDoc"></param>   
        /// <returns></returns>
        public async Task<ResultadoBusquedaModel<ReportesMesaPartes>> ProxyReporteSeguimientoDocumentos(int? page, int pageSize, string codAreas, string codTipoDoc, string anio, string nrotramite)
        {
            codAreas = String.IsNullOrEmpty(codAreas) ? "0" : codAreas;
            codTipoDoc = String.IsNullOrEmpty(codTipoDoc) ? "0" : codTipoDoc;
            anio = String.IsNullOrEmpty(anio) ? "0" : anio;
            nrotramite = String.IsNullOrEmpty(nrotramite) ? "VACIO" : nrotramite;
            ResultadoBusquedaModel<ReportesMesaPartes> lst = await Service.GetAsync<ResultadoBusquedaModel<ReportesMesaPartes>>(UrlApi.ObtenerUrl("/reportes/GetReporteSeguimientoDocumentos/") +  nrotramite  + "/" + anio + "/" + codAreas + "/" + codTipoDoc   + "/" + page + "/" + pageSize, token);
            return lst;
        }

        /// <summary>
        /// Devuelte lista de documentos de Entrada
        /// </summary>
        /// <param name="nrotramite"></param>
        /// <param name="anio"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="codAreas"></param>
        /// <param name="codTipoDoc"></param>   
        /// <returns></returns>
        public ResultadoBusquedaModel<ReportesMesaPartes> ProxyReporteSeguimientoDocumentos_Sync(int? page, int pageSize, string codAreas, string codTipoDoc, string anio, string nrotramite)
        {

            codAreas = String.IsNullOrEmpty(codAreas) ? "0" : codAreas;
            codTipoDoc = String.IsNullOrEmpty(codTipoDoc) ? "0" : codTipoDoc;
            anio = String.IsNullOrEmpty(anio) ? "0" : anio;
            nrotramite = String.IsNullOrEmpty(nrotramite) ? "VACIO" : nrotramite;
            ResultadoBusquedaModel<ReportesMesaPartes> lst = new Service().HttpGetJson<ResultadoBusquedaModel<ReportesMesaPartes>>(UrlApi.ObtenerUrl("/reportes/GetReporteSeguimientoDocumentos/") + nrotramite + "/" + anio + "/" + codAreas + "/" + codTipoDoc + "/" + page + "/" + pageSize, token);
            return lst;
        }


    }
}
