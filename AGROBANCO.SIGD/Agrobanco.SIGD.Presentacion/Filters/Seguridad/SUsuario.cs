﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Filters.Seguridad
{

    public class SUsuario
    {
        public string Token { get; set; }
        public string UsuarioID { get; set; }
        public string NombreCompleto { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Cargo { get; set; }
        public string TipoUsuario { get; set; }
        public string Ticket { get; set; }
        public string CorreoElectronico { get; set; }
        public string UsuarioWeb { get; set; }
        public string UsuarioAD { get; set; }
        public string EstadoAD { get; set; }
        public string AgenciaPrincipal { get; set; }
        public string AgenciaPrincipalDescripcion { get; set; }
        public string OtrasAgencias { get; set; }
        public string CodigoFuncionario { get; set; }
        public string Vigencia { get; set; }
        public DateTime? VigenciaDesde { get; set; }
        public DateTime? VigenciaHasta { get; set; }
        public string EstadoBloqueo { get; set; }
        public string EstadoLogico { get; set; }
        public DateTime? FechaBloqueo { get; set; }
        public DateTime? FechaUltAcceso { get; set; }
        public int IntentosFallidos { get; set; }
        public DateTime? FechaUltimoCambioClave { get; set; }

        public List<Modulo> ListaOpcionesMenu { get; set; }
        public List<Agencia> ListaAgencias { get; set; }

        public string UsuarioIdRegistro { get; set; }
        public string tipoOperacion { get; set; }
        public string tipoRegistro { get; set; }
        public string cambioEstadoAD { get; set; }
    }

    public class Modulo
    {
        public string IdPerfil { get; set; }
        public string NombrePerfil { get; set; }
        public string IdAplicacion { get; set; }
        public string NombreAplicacion { get; set; }
        public string UrlAplicacion { get; set; }
        public string IdOpcion { get; set; }
        public string NombreOpcion { get; set; }
        public string CorrelativoOpcion { get; set; }
        public string DescripcionAplicacion { get; set; }

        public string EstadoOpcion { get; set; }

        public string RutaFisicaIcono { get; set; }
        public string UrlOpcion { get; set; }
        public string TipoOpcion { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string TipoIconoCodigo { get; set; }
        public string TipoIconoDescripcion { get; set; }
        public string EstadoLogicoOpcion { get; set; }
        public string IdRelacion { get; set; }
    }
    public class Agencia
    {
        public string vCodigo { get; set; }

        public string Valor { get; set; }
        public string Descripcion { get; set; }
        public string NombreRegional { get; set; }
    }

}