﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Filters.Seguridad
{
    public class ResultadoObtenerModel<T>
    {
        public ResultadoObtenerModel()
        {
            eServicioResponse = new ServicioResponse();
        }

        public T Model { get; set; }
        public ServicioResponse eServicioResponse { get; set; }
    }
}