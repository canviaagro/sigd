﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Filters
{
    public static class HelperSeguridad
    {
        public static EUsuario ObtenerSessionUsuario()
        {
            return (EUsuario)HttpContext.Current.Session["oUsuario"];
        }
        public static void GrabarSessionUsuario(Object obj)
        {
            HttpContext.Current.Session["oUsuario"] = obj;
        }        

        public static bool AutenticacionSIGD()
        {
            var sesionSGS = (ResultadoObtenerModel<SUsuario>)HttpContext.Current.Session["UsuarioSIGD"];
            bool resultadoAutenticacion = true;
            ELoginRequest oBELoginReq = new ELoginRequest();
            oBELoginReq.Username = sesionSGS.Model.UsuarioWeb;
            //oBELoginReq.Password = sesionSGS.Model.
            string urlAutenticate = Configuracion.SeguridadUrlAutenticate;

            Service _service = new Service();

            string strBodyLogin = JsonComponent.Serialize<ELoginRequest>(oBELoginReq);
            string strRptToken = string.Empty;
            try
            {
                strRptToken = _service.HttpPostJson(urlAutenticate, strBodyLogin);
            }
            catch (Exception ex)
            {
                resultadoAutenticacion = false;

                return resultadoAutenticacion;
            }            

            if (!string.IsNullOrWhiteSpace(strRptToken))
            {
                strRptToken = strRptToken.Remove(0, 1).ToString();
                strRptToken = strRptToken.Remove(strRptToken.Length - 1, 1).ToString();
                strRptToken = "Bearer " + strRptToken;
                HttpContext.Current.Session["tokenUser"] = strRptToken;
                
                EUsuario UsuEObj = _service.HttpGetJson<EUsuario>(Configuracion.SeguridadUrlGetUsuario + oBELoginReq.Username, strRptToken);
                if (UsuEObj.MaeTrabajador == null)
                {
                    resultadoAutenticacion = false;
                }
                else
                {
                    Filters.HelperSeguridad.GrabarSessionUsuario(UsuEObj);                   
                }
            }
            else
            {
                resultadoAutenticacion = false;
            }

            return resultadoAutenticacion;
        }

        public static bool? VerificarPrivilegio(string controllerLoad, string actionLoad, string actionReferencial, bool buscarEnListaPermitidos)
        {
            bool? valido = null;
            EUsuario usuario = ObtenerSessionUsuario();
            //return usuario.Permisos.Contains(codigo);

            HelperAgroSeguridad helperAgro = new HelperAgroSeguridad();
            var userSesion = helperAgro.ObtenerSesionAgroSeguridad();

            var modulosOpciones = userSesion.Model.ListaOpcionesMenu;
            //var modulos = modulosOpciones.Where(m => m.TipoOpcion == "1").ToList();
            //var opcionesySubModulos = modulosOpciones.Where(m => m.TipoOpcion != "1").ToList();
            bool accesoModulo = modulosOpciones.Where(m => m.UrlOpcion == controllerLoad || m.Controller == controllerLoad || (!string.IsNullOrEmpty(m.Controller) && m.Controller.Split('/').Contains(controllerLoad))).Any();
            bool accesoAction = false;

            if (!accesoModulo)
            {
                valido = false;
                return valido;
            }
            /*Si tiene acceso al módulo, solo quedaría validar las acciones de menú, pues si fuera una action de ajax, 
             * al tener acceso al módulo por ende ya tiene acceso a las acciones internas ajax */
            if (controllerLoad == actionReferencial)
            {
                valido = true;
                return valido;
            }

            if (actionLoad == "Index")
            {
                accesoAction = true;
            }
            else
            {
                accesoAction = modulosOpciones.Where(m => (m.Controller == controllerLoad || (!string.IsNullOrEmpty(m.Controller) && m.Controller.Split('/').Contains(controllerLoad))) && (m.Action == actionLoad || m.Action == actionReferencial)).Any();
            }
            
            valido = accesoAction;

            //var listaPermisos = usuario.Permisos.FindAll(x => x.Permitido == buscarEnListaPermitidos).ToList();
            //var listModulos = listaPermisos.GroupBy(p => new { p.Modulo.vCodModulo, p.Modulo.vNombre, p.Modulo.vEstiloIcono, p.Modulo.vURL });

            //foreach (var item in listModulos)
            //{
            //    var modulo = item.Key;
            //    var listOpciones = listaPermisos.ToList().Where(x => x.vCodModulo == modulo.vCodModulo).ToList();
            //    var existsOpcion = listOpciones.Exists(x => !string.IsNullOrEmpty(x.Opcion.vURL));

            //    string controler = "";
            //    string accion = "";
            //    if (modulo.vURL != null)
            //    {
            //        if (modulo.vURL.Split('/').Length <= 1)
            //        {
            //            controler = modulo.vURL.Split('/')[0];
            //            accion = "Index";
            //        }
            //        else
            //        {
            //            controler = modulo.vURL.Split('/')[0];
            //            accion = modulo.vURL.Split('/')[1];
            //        }
            //        //
            //        if (controllerLoad.ToUpper() == controler.ToUpper() && actionLoad.ToUpper() == accion.ToUpper())
            //        {
            //            valido = true;
            //        }
            //    }
            //    foreach (var item2 in listOpciones)
            //    {
            //        if (!string.IsNullOrEmpty(item2.Opcion.vURL))
            //        {
            //            ESegOpcion OpcEObj = item2.Opcion;
            //            string controlerMod = "";
            //            string accionMod = "";
            //            if (OpcEObj.vURL != null)
            //            {
            //                if (OpcEObj.vURL.Split('/').Length <= 1)
            //                {
            //                    controlerMod = OpcEObj.vURL.Split('/')[0];
            //                    accionMod = "Index";
            //                }
            //                else
            //                {
            //                    controlerMod = OpcEObj.vURL.Split('/')[0];
            //                    accionMod = OpcEObj.vURL.Split('/')[1];
            //                }
            //                //
            //                if (controllerLoad.ToUpper() == controlerMod.ToUpper() && actionLoad.ToUpper() == accionMod.ToUpper())
            //                {
            //                    valido = true;
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //    if (valido == true)
            //        break;
            //}

            return valido;
        }
    }
}