﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    public class RepresentanteLegalController : Controller
    {
        // GET: Mantenimiento/RepresentanteLegal
        public async Task<ActionResult> Index()
        {
            return View(await ObtenerRepresentateLegal());
        }



        private async Task<RepresentanteLegal> ObtenerRepresentateLegal()
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/RepresentanteLegal/ObtenerRepresentanteLegal", GetToken());

            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosComun();
            ViewBag.Estados = parametros.ListaEstados;

            var item = Helpers.ConvertToObject<RepresentanteLegal>(respuesta.data);
            if (item == null)
            {
                item = new RepresentanteLegal();
            }
            return item;
        }

        [HttpPost]
        public async Task<ActionResult> Grabar(RepresentanteLegal obj)
        {

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            if (obj.Accion == "editar")
            {
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "RepresentanteLegal/Editar", GetToken());
            }
            else if (obj.Accion == "grabar")
            {
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "RepresentanteLegal/Grabar", GetToken());
            }

            return Json(respuesta);
        }

        public string GetToken()
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            return strRptToken;
        }

    }
}