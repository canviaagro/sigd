﻿using Agrobanco.SIGD.Presentacion.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Utilities;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using ClosedXML.Excel;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class TrabajadoresController : BaseController
    {

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            int nroPagina = 1;

            TrabajadorViewModel trabajadorViewModel = new TrabajadorViewModel();
            trabajadorViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<Trabajador>();

            //Obtener resultado busqueda         
            var onePageListaTrabajador = await new ProxyApi(base.GetToken()).ProxyListaTrabajador(nroPagina, "", ConfigurationUtilities.PageSize);
            trabajadorViewModel.ResultadoBusqueda = onePageListaTrabajador;
            trabajadorViewModel.ResultadoBusqueda.ListaPaginado = onePageListaTrabajador.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            return View(trabajadorViewModel);
        }


        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString)
        {
            int nroPagina = page == null ? 1 : page.Value;

            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);

            //Obtener resultado busqueda            
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyListaTrabajador(nroPagina, searchString, ConfigurationUtilities.PageSize);
            onePageListaAreas.ListaPaginado = onePageListaAreas.ToPageList();

            return PartialView("_Lista", onePageListaAreas);
        }

        public async Task<FileResult> ExportarPDF(string searchString)
        {
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDFTrabajador_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            List<Trabajador> lista = (await new ProxyApi(base.GetToken()).ProxyListaTrabajador(1, searchString, int.MaxValue)).Lista;
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }



        public void ExportarExcel(string searchString)
        {
            List<Trabajador> lista = (new ProxyApi(base.GetToken()).ProxyListaTrabajador_Sync(1, searchString, int.MaxValue)).Lista;
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de Trabajadores";
            string fileNameReport = "ReporteExcelTrabajador_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            GenerarExcelResponse<List<Trabajador>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 12;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "NOMBRES";
                ws.Cell(rowIndex, 3).Value = "TIPO DOCUMENTO";
                ws.Cell(rowIndex, 4).Value = "N° DOCUMENTO";
                ws.Cell(rowIndex, 5).Value = "AREA";
                ws.Cell(rowIndex, 6).Value = "JEFE";
                ws.Cell(rowIndex, 7).Value = "FIRMANTE";
                ws.Cell(rowIndex, 8).Value = "ESTADO";
                ws.Cell(rowIndex, 9).Value = "REGISTRADO POR";
                ws.Cell(rowIndex, 10).Value = "FECHA CREACION";
                ws.Cell(rowIndex, 11).Value = "ACTUALIZADO POR";
                ws.Cell(rowIndex, 12).Value = "FECHA ACTUALIZACION";


                ws.Column(1).Width = 20;
                ws.Column(2).Width = 35;
                ws.Column(3).Width = 15;
                ws.Column(4).Width = 20;
                ws.Column(5).Width = 20;
                ws.Column(6).Width = 15;
                ws.Column(7).Width = 15;
                ws.Column(7).Width = 20;
                ws.Column(8).Width = 25;
                ws.Column(9).Width = 25;
                ws.Column(10).Width = 25;
                ws.Column(11).Width = 25;



                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;

                    ws.Cell(rowIndex, 1).Value = item.iCodTrabajador;
                    ws.Cell(rowIndex, 1).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 1).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 2).Value = item.vNombreCompleto;
                    ws.Cell(rowIndex, 2).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 2).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 3).Value = item.vTipoDocumento;
                    ws.Cell(rowIndex, 3).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 3).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 4).Value = item.vNumDocumento;
                    ws.Cell(rowIndex, 4).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 4).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 5).Value = item.vArea;
                    ws.Cell(rowIndex, 5).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 5).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 6).Value = (item.siEsJefe == 1 ? "SI" : "NO");
                    ws.Cell(rowIndex, 6).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 6).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 7).Value = (item.siFirma == 1 ? "SI" : "NO");
                    ws.Cell(rowIndex, 7).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 7).Style.Alignment.SetWrapText(true);

                    ws.Cell(rowIndex, 8).Value = (item.siEstado == 1 ? "Activo" : "Inactivo");
                    ws.Cell(rowIndex, 8).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 8).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 9).Value = item.vUsuCreacion;
                    ws.Cell(rowIndex, 9).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 9).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 10).Value = item.dFecCreacion;
                    //ws.Cell(rowIndex, 9).Style.NumberFormat.SetFormat("@");
                    //ws.Cell(rowIndex, 9).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 11).Value = item.vUsuActualizacion;
                    ws.Cell(rowIndex, 11).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 11).Style.Alignment.SetWrapText(true);
                    ws.Cell(rowIndex, 12).Value = item.dFecActualizacion;
                    //ws.Cell(rowIndex, 11).Style.NumberFormat.SetFormat("@");
                    //ws.Cell(rowIndex, 11).Style.Alignment.SetWrapText(true);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                                     

                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                //ws.Columns("C").Width = 50;
            });
        }


        [HttpPost]
        public async Task<ActionResult> Eliminar(Trabajador obj)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Trabajador/EliminarTrabajador", GetToken());
            return Json(respuesta);
        }


        [HttpGet]
        public async Task<ActionResult> ObtenerTrabajadorID(int id)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            DocumentoController trabajadorController = new DocumentoController();
            Trabajador objTraba = new Trabajador();
            trabajadorController.Token = base.GetToken();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("Trabajador/ObtenerTrabajadorID?id=" + id, trabajadorController.Token);
            if (respuesta.Resultado == 1)
            {
                objTraba = ConvertToObject<Trabajador>(respuesta.data);
            }
            return Json(objTraba, JsonRequestBehavior.AllowGet);
        }


        private T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
            return result;
        }


        [HttpPost]
        public async Task<ActionResult> Registrar(Trabajador obj)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Trabajador/RegistrarTrabajador", GetToken());
            return Json(respuesta);
        }


        [HttpPost]
        public async Task<ActionResult> Actualizar(Trabajador obj)
        {

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Trabajador/ActualizarTrabajador", GetToken());

            return Json(respuesta);
        }
                          
    }
}
