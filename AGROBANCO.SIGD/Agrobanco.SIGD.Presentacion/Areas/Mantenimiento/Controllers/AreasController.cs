﻿using Agrobanco.SIGD.Presentacion.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Utilities;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using ClosedXML.Excel;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class AreasController : BaseController
    {        
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            int nroPagina = 1;

            AreasViewModel areaViewModel = new AreasViewModel();
            areaViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<Area>();

            //Obtener resultado busqueda         
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyListarArea(nroPagina, "", ConfigurationUtilities.PageSize);
            areaViewModel.ResultadoBusqueda = onePageListaAreas;
            areaViewModel.ResultadoBusqueda.ListaPaginado = onePageListaAreas.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            return View(areaViewModel);
        }


        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString)
        {
            int nroPagina = page == null ? 1 : page.Value;

            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);

            //Obtener resultado busqueda            
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyListarArea(nroPagina, searchString, ConfigurationUtilities.PageSize);
            onePageListaAreas.ListaPaginado = onePageListaAreas.ToPageList();

            return PartialView("_Lista", onePageListaAreas);
        }



        [HttpGet]
        public async Task<ActionResult> ObtenerAreaID(int id)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            DocumentoController areaController = new DocumentoController();
            Area objArea = new Area();
            areaController.Token = base.GetToken();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("area/ObtenerAreaID?id=" + id, areaController.Token);
            if (respuesta.Resultado == 1)
            {
                objArea = ConvertToObject<Area>(respuesta.data);
            }
            return Json(objArea, JsonRequestBehavior.AllowGet);
        }


        private T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
            return result;
        }


        public async Task<FileResult> ExportarPDF(string searchString)
        {
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDFArea_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            List<Area> lista = (await new ProxyApi(base.GetToken()).ProxyListarArea(1, searchString, int.MaxValue)).Lista;
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }


        public void ExportarExcel(string searchString)
        {
            List<Area> lista = (new ProxyApi(base.GetToken()).ProxyListarArea_Sync(1, searchString, int.MaxValue)).Lista;
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de Areas";
            string fileNameReport = "ReporteExcelArea_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            GenerarExcelResponse<List<Area>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 8;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "DESCRIPCIÓN";
                ws.Cell(rowIndex, 3).Value = "ABREVIATURA";
                ws.Cell(rowIndex, 4).Value = "ESTADO";
                ws.Cell(rowIndex, 5).Value = "REGISTRADO POR";
                ws.Cell(rowIndex, 6).Value = "FECHA CREACION";
                ws.Cell(rowIndex, 7).Value = "ACTUALIZADO POR";
                ws.Cell(rowIndex, 8).Value = "FECHA ACTUALIZACION";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.CodArea;
                    ws.Cell(rowIndex, 2).Value = item.Descripcion;
                    ws.Cell(rowIndex, 3).Value = item.Abreviatura;
                    ws.Cell(rowIndex, 4).Value = (item.Estado == 1 ? "Activo" : "Inactivo");
                    ws.Cell(rowIndex, 5).Value = item.UsuarioCreacion;
                    ws.Cell(rowIndex, 6).Value = item.FechaCreacion;
                    ws.Cell(rowIndex, 7).Value = item.UsuarioActualizacion;
                    ws.Cell(rowIndex, 8).Value = item.FechaActualizacion;
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 50;
            });
        }


        [HttpPost]
        public async Task<ActionResult> Registrar(Area obj)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Area/RegistrarArea", GetToken());

            return Json(respuesta);
        }


        [HttpPost]
        public async Task<ActionResult> Actualizar(Area obj)
        {

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Area/ActualizarArea", GetToken());

            return Json(respuesta);
        }


        [HttpPost]
        public async Task<ActionResult> Eliminar(Area obj)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Area/EliminarArea", GetToken());

            return Json(respuesta);
        }



    }
}
