﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class FeriadosController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> Ver(int id)
        {
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();
            var parametros = await documentoController.CargarParametrosFeriado();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipoFeriado = parametros.ListaTipoFeriado;
            return View("_Ver", await ObtenerFeriado(id));
        }
        [HttpGet]
        public async Task<ActionResult> Editar(int id)
        {
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();
            var parametros = await documentoController.CargarParametrosFeriado();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipoFeriado = parametros.ListaTipoFeriado;
            return View("_Editar", await ObtenerFeriado(id));
        }

        [HttpGet]
        public async Task<ActionResult> Nuevo()
        {
            Feriado feriado = new Feriado();

            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosFeriado();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipoFeriado = parametros.ListaTipoFeriado;

            feriado.Fecha = DateTime.Now;
            feriado.FecCreacion = DateTime.Now;
            feriado.Estado = 1;
            feriado.CodFeriado = 0;
            return View("_Nuevo", feriado);
        }
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            int nroPagina = 1;
            MantenimientoFeriadoListadoViewModel viewmodel = new MantenimientoFeriadoListadoViewModel();
            viewmodel.ResultadoBusqueda = new ResultadoBusquedaModel<Feriado>();

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarFeriados(nroPagina, "", ConfigurationUtilities.PageSize);
            viewmodel.ResultadoBusqueda = onePageListaDocumentos;
            viewmodel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametros = await documentoController.CargarParametrosFeriado();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipoFeriado = parametros.ListaTipoFeriado;

            return View(viewmodel);
        }
        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString)
        {
            int nroPagina = page == null ? 1 : page.Value;
            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);

            //Obtener resultado busqueda            
            var onePageLista = await new ProxyApi(base.GetToken()).ProxyListarFeriados(nroPagina, searchString, ConfigurationUtilities.PageSize);
            onePageLista.ListaPaginado = onePageLista.ToPageList();

            return PartialView("_Lista", onePageLista);
        }


        private async Task<Feriado> ObtenerFeriado(int id)
        {
            RespuestaOperacionServicio itemOp = new Service().HttpGetJson<RespuestaOperacionServicio>(
                ConfigurationUtilities.WebApiUrl + "/feriado/ObtenerFeriado?id=" + id, GetToken());

            var itemFeriado = Helpers.ConvertToObject<Feriado>(itemOp.data);
            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosFeriado();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipoFeriado = parametros.ListaTipoFeriado;

            var itemEstado = parametros.ListaEstados.Find(x => x.Valor == itemFeriado.Estado.ToString());
            if (itemEstado != null)
                itemFeriado.EstadoDescripcion = itemEstado.Campo;


            return itemFeriado;
        }

        [HttpPost]
        public async Task<ActionResult> Grabar(FormCollection form)
        {
            Feriado request = new Feriado();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModelV3(form, request);

            request.Fecha = DateTime.ParseExact(request.FechaStr, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (request.CodFeriado == 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Feriado/Registrar", GetToken());
            }
            else//actualizar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Feriado/Actualizar", GetToken());
            }
            return Json(respuesta);
        }
        [HttpPost]
        public async Task<ActionResult> Eliminar(int id)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Feriado request = new Feriado();
            request.CodFeriado = id;

            if (request.CodFeriado > 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Feriado/Eliminar", GetToken());
            }
            return Json(respuesta);
        }

        public async Task<FileResult> ExportarPDF(string searchString)
        {
            List<Feriado> lista = (await new ProxyApi(base.GetToken()).ProxyListarFeriados(1, searchString, int.MaxValue)).Lista;



            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosFeriado();

            lista.ForEach(x =>
            {
                var tipofer = parametros.ListaTipoFeriado.Find(y => y.Valor == x.TipoFeriado.ToString());
                if (tipofer != null)
                    x.TipoFeriadoDescripcion = tipofer.Campo;
            });


            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDF";
            
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }
        public void ExportarExcel(string searchString)
        {

            List<Feriado> lista = (new ProxyApi(base.GetToken()).ProxyListarFeriados_Sync(1, searchString, int.MaxValue)).Lista;



            DocumentoController documentoController = new DocumentoController();
            var parametros = documentoController.CargarParametrosFeriado_Sync();

            lista.ForEach(x =>
            {
                var tipofer = parametros.ListaTipoFeriado.Find(y => y.Valor == x.TipoFeriado.ToString());
                if (tipofer != null)
                    x.TipoFeriadoDescripcion = tipofer.Campo;
            });



            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de Feriados";
            string fileNameReport = "ReporteExcel";

            GenerarExcelResponse<List<Feriado>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 9;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "FERIADO";
                ws.Cell(rowIndex, 3).Value = "DESCRIPCIÓN";
                ws.Cell(rowIndex, 4).Value = "TIPO";
                ws.Cell(rowIndex, 5).Value = "ESTADO";
                ws.Cell(rowIndex, 6).Value = "REGISTRADO POR";
                ws.Cell(rowIndex, 7).Value = "FECHA CREACION";
                ws.Cell(rowIndex, 8).Value = "ACTUALIZADO POR";
                ws.Cell(rowIndex, 9).Value = "FECHA ACTUALIZACION";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.CodFeriado;
                    ws.Cell(rowIndex, 2).Value = item.Fecha.Value.ToString("dd/MM/yyyy hh:mm:ss");
                    ws.Cell(rowIndex, 3).Value = item.Descripcion;
                    ws.Cell(rowIndex, 4).Value = item.TipoFeriadoDescripcion;
                    ws.Cell(rowIndex, 5).Value = (item.Estado == 1 ? "Activo" : "Inactivo");
                    ws.Cell(rowIndex, 6).Value = item.UsuCreacion;
                    ws.Cell(rowIndex, 7).Value = item.FecCreacion;
                    ws.Cell(rowIndex, 8).Value = item.UsuActualizacion;
                    ws.Cell(rowIndex, 9).Value = item.FecActualizacion;
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 50;
            });
        }


    }
}
