﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static Agrobanco.SIGD.Comun.Enumerados;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class SecuencialesController : BaseController
    {
        // GET: Mantenimiento/Secuencial
        public async Task<ActionResult> Index()
        {
            int nroPagina = 1;


            SecuenciaViewModel secuenciaViewModel = new SecuenciaViewModel();
            secuenciaViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<Secuencia>();

            //Obtener resultado busqueda         
            var onePageListaSecuencia = await new ProxyApi(base.GetToken()).ProxyListarSecuencia(nroPagina, "", ConfigurationUtilities.PageSize);
            secuenciaViewModel.ResultadoBusqueda = onePageListaSecuencia;
            secuenciaViewModel.ResultadoBusqueda.ListaPaginado = onePageListaSecuencia.ToPageList();


            return View(secuenciaViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString)
        {
            int nroPagina = page == null ? 1 : page.Value;

            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);

            //Obtener resultado busqueda            
            var onePageListaTipoDocumentos = await new ProxyApi(base.GetToken()).ProxyListarSecuencia(nroPagina, searchString, ConfigurationUtilities.PageSize);
            onePageListaTipoDocumentos.ListaPaginado = onePageListaTipoDocumentos.ToPageList();

            return PartialView("_Lista", onePageListaTipoDocumentos);
        }

        [HttpPost]
        public async Task<ActionResult> RegistrarSecuencial(Secuencia Secuencia)
        {
            var ListaRestriccion = new Secuencia();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();

            List<Secuencia> lista = (new ProxyApi(base.GetToken()).ProxyListarSecuencia_Sync(1, "", int.MaxValue)).Lista;
            //validacion correlativo  unico
            var correlativoExist   = lista.Find(x => (x.Origen == Convert.ToInt16(TipoOrigen.EXTERNO)
                                                       && x.anio  == Secuencia.anio && x.ValorCorrelativo == Secuencia.ValorCorrelativo) ||
                                                       (x.Origen == Convert.ToInt16(TipoOrigen.INTERNO)
                                                       && x.ValorCorrelativo == Secuencia.ValorCorrelativo
                                                       && x.anio == Secuencia.anio
                                                       && x.CodArea == Secuencia.CodArea 
                                                       && x.CodTipoDocumento == Secuencia.CodTipoDocumento) && x.Estado == Convert.ToInt16(Estado.Activo));

            if (correlativoExist != null)
            {
                respuesta.Error = "EL CORRELATIVO " + Secuencia.ValorCorrelativo + " YA EXISTE";
            }
            else {
                if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
                {
                    ListaRestriccion = lista.Find(x => x.anio == Secuencia.anio && x.Origen == Convert.ToInt16(TipoOrigen.EXTERNO) && x.flagUsado == Convert.ToInt16(FlagUsadoSecuencial.SINUSO) &&
                                                   x.Estado == Convert.ToInt16(Estado.Activo));
                } else if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.INTERNO))
                {
                    ListaRestriccion = lista.Find(x => x.anio == Secuencia.anio && x.Origen == Convert.ToInt16(TipoOrigen.INTERNO) && x.flagUsado == Convert.ToInt16(FlagUsadoSecuencial.SINUSO) &&
                                                   x.Estado == Convert.ToInt16(Estado.Activo) && x.CodArea == Secuencia.CodArea && x.CodTipoDocumento == Secuencia.CodTipoDocumento);
                }

                if (ListaRestriccion == null)
                {
                    respuesta = await Service.PostAsync<RespuestaOperacionServicio>(Secuencia, "Secuencia/RegistrarSecuencia", GetToken());
                }
                else
                {
                    if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
                    {
                        respuesta.Error = "YA EXISTE UN CORRELATIVO SIN USAR PARA EL AÑO " + Secuencia.anio + ", ORIGEN EXTERNO ";
                    } else if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.INTERNO))
                    {
                        respuesta.Error = "YA EXISTE UN CORRELATIVO SIN USAR PARA EL AÑO " + Secuencia.anio + ", ORIGEN INTERNO , AREA :" + Secuencia.DescripcionArea + " , TIPO DOCUMENTO :" + Secuencia.DescripcionTipoDocumento;
                    }
                }
            }
            return Json(respuesta);
        }

        [HttpPost]
        public async Task<ActionResult> EditarSecuencial(Secuencia Secuencia)
        {
            var ListaRestriccion = new Secuencia();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();

            List<Secuencia> lista = (new ProxyApi(base.GetToken()).ProxyListarSecuencia_Sync(1, "", int.MaxValue)).Lista;
            //validacion correlativo  unico
            var correlativoExist = lista.Find(x => (x.Origen == Convert.ToInt16(TipoOrigen.EXTERNO)
                                                     && x.anio == Secuencia.anio && x.ValorCorrelativo == Secuencia.ValorCorrelativo
                                                     && x.Estado == Convert.ToInt16(Estado.Activo) && x.CodSecuencial != Secuencia.CodSecuencial ) || 
                                                     (x.Origen == Convert.ToInt16(TipoOrigen.INTERNO)
                                                     && x.ValorCorrelativo == Secuencia.ValorCorrelativo
                                                     && x.anio == Secuencia.anio
                                                     && x.CodArea == Secuencia.CodArea
                                                     && x.CodTipoDocumento == Secuencia.CodTipoDocumento
                                                     && x.Estado == Convert.ToInt16(Estado.Activo) && x.CodSecuencial != Secuencia.CodSecuencial) );

            if (correlativoExist != null)
            {
                respuesta.Error = "EL CORRELATIVO " + Secuencia.ValorCorrelativo + " YA EXISTE";
            }
            else
            {

                if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
                {
                    ListaRestriccion = lista.Find(x => x.anio == Secuencia.anio && x.Origen == Convert.ToInt16(TipoOrigen.EXTERNO) && x.flagUsado == Convert.ToInt16(FlagUsadoSecuencial.SINUSO) &&
                                                   x.Estado == Convert.ToInt16(Estado.Activo) && x.CodSecuencial != Secuencia.CodSecuencial);
                }
                else if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.INTERNO))
                {
                    ListaRestriccion = lista.Find(x => x.anio == Secuencia.anio && x.Origen == Convert.ToInt16(TipoOrigen.INTERNO) && x.flagUsado == Convert.ToInt16(FlagUsadoSecuencial.SINUSO) &&
                                                   x.Estado == Convert.ToInt16(Estado.Activo) && x.CodArea == Secuencia.CodArea && x.CodTipoDocumento == Secuencia.CodTipoDocumento && x.CodSecuencial != Secuencia.CodSecuencial);
                }

                if (ListaRestriccion== null)
                {
                    respuesta = await Service.PostAsync<RespuestaOperacionServicio>(Secuencia, "Secuencia/EditarSecuencial", GetToken());
                }
                else
                {
                    if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.EXTERNO))
                    {
                        respuesta.Error = "YA EXISTE UN CORRELATIVO SIN USAR PARA EL AÑO " + Secuencia.anio + ", ORIGEN INTERNO ";
                    }
                    else if (Secuencia.Origen == Convert.ToInt16(TipoOrigen.INTERNO))
                    {
                        respuesta.Error = "YA EXISTE UN CORRELATIVO SIN USAR PARA EL AÑO " + Secuencia.anio + ", ORIGEN INTERNO , AREA :" + Secuencia.DescripcionArea + " , TIPO DOCUMENTO :" + Secuencia.DescripcionTipoDocumento;
                    }
                }
            }
            return Json(respuesta);
        }

        [HttpGet]
        public async Task<ActionResult> Ver(int id)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            DocumentoController documentoController = new DocumentoController();
            Secuencia obj = new Secuencia();
            documentoController.Token = base.GetToken();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("Secuencia/ObtenerSecuencia?id=" + id, documentoController.Token);
            if (respuesta.Resultado == 1)
            {
                obj = ConvertToObject<Secuencia>(respuesta.data);
            }
            return View("_Ver", obj);
        }

        [HttpGet]
        public async Task<ActionResult> ObtenerSecuencia(int id)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            DocumentoController documentoController = new DocumentoController();
            Secuencia obj= new Secuencia();
            documentoController.Token = base.GetToken();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("Secuencia/ObtenerSecuencia?id=" + id, documentoController.Token);
            if (respuesta.Resultado == 1)
            {
                obj = ConvertToObject<Secuencia>(respuesta.data);
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ObtenerCorrelativo(Secuencia obj)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Secuencia/ObtenerCorrelativo", GetToken());

            return Json(respuesta);
        }

        [HttpPost]
        public async Task<ActionResult> Eliminar(Secuencia obj)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(obj, "Secuencia/EliminarSecuencia", GetToken());

            return Json(respuesta);
        }

        public void ExportarExcel(string searchString)
        {

            List<Secuencia> lista = (new ProxyApi(base.GetToken()).ProxyListarSecuencia_Sync(1, searchString, int.MaxValue)).Lista;
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de Secuencias";
            string fileNameReport = "ReporteExcel";

            GenerarExcelResponse<List<Secuencia>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 9;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "ORIGEN";
                ws.Cell(rowIndex, 3).Value = "AÑO";
                ws.Cell(rowIndex, 4).Value = "CORRELATIVO";
                ws.Cell(rowIndex, 5).Value = "AREA";
                ws.Cell(rowIndex, 6).Value = "TIPO DE DOCUMENTO";
                ws.Cell(rowIndex, 7).Value = "VALOR CORRELATIVO";
                ws.Cell(rowIndex, 8).Value = "USO";
                ws.Cell(rowIndex, 9).Value = "ESTADO";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.CodSecuencial;
                    ws.Cell(rowIndex, 2).Value = ((item.Origen == 1) ? "Interno" : "Externo");
                    ws.Cell(rowIndex, 3).Value = item.anio;
                    ws.Cell(rowIndex, 4).Value = item.correlativo;
                    ws.Cell(rowIndex, 5).Value = item.DescripcionArea;
                    ws.Cell(rowIndex, 6).Value = item.DescripcionTipoDocumento;
                    ws.Cell(rowIndex, 7).Value = item.ValorCorrelativo;
                    ws.Cell(rowIndex, 8).Value = ((item.flagUsado == 1) ? "En Uso" : "Sin Uso");
                    ws.Cell(rowIndex, 9).Value = (item.Estado == 1 ? "Activo" : "Inactivo");
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 50;
            });
        }

        public async Task<FileResult> ExportarPDF(string searchString)
        {
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDF";
            List<Secuencia> lista = (await new ProxyApi(base.GetToken()).ProxyListarSecuencia(1, searchString, int.MaxValue)).Lista;
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }

        private T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);

            return result;
        }
    }
}
