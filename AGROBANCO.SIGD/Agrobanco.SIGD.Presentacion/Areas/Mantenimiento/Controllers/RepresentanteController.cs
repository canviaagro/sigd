﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class RepresentanteController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> Ver(int id)
        {
            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosRepresentante();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipDocIdentidad = parametros.ListaTipDocIdentidad;
            ViewBag.TipoRepresentante = parametros.ListaTipoRepresentante;
            return View("_Ver", await ObtenerRepresentante(id));
        }
        [HttpGet]
        public async Task<ActionResult> Editar(int id)
        {
            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosRepresentante();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipDocIdentidad = parametros.ListaTipDocIdentidad;
            ViewBag.TipoRepresentante = parametros.ListaTipoRepresentante;

            return View("_Editar", await ObtenerRepresentante(id));
        }

        [HttpGet]
        public async Task<ActionResult> Nuevo(int codEmpresa)
        {
            Representante representante = new Representante();

            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosRepresentante();
            ViewBag.Estados = parametros.ListaEstados;
            ViewBag.TipDocIdentidad = parametros.ListaTipDocIdentidad;
            ViewBag.TipoRepresentante = parametros.ListaTipoRepresentante;
            representante.Estado = 1;
            representante.CodEmpresa = codEmpresa;
            return View("_Nuevo", representante);
        }
        [HttpGet]
        public async Task<ActionResult> Index(int? codEmpresa)
        {
            int nroPagina = 1;
            ViewBag.CodEmpresa = codEmpresa;
            MantenimientoRepresentanteListadoViewModel viewmodel = new MantenimientoRepresentanteListadoViewModel();
            viewmodel.ResultadoBusqueda = new ResultadoBusquedaModel<Representante>();
            
             //Obtener resultado busqueda            
             var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarRepresentante(nroPagina, "", codEmpresa.Value, ConfigurationUtilities.PageSize);
            viewmodel.ResultadoBusqueda = onePageListaDocumentos;
            viewmodel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametros = await documentoController.CargarParametrosRepresentante();
            ViewBag.Estados = parametros.ListaEstados;
            viewmodel.CodEmpresa = codEmpresa.Value;
            return View(viewmodel);
        }
        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString, int? codEmpresa)
        {
            int nroPagina = page == null ? 1 : page.Value;
            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);
            ViewBag.CodEmpresa = codEmpresa;
            //Obtener resultado busqueda            
            var onePageLista = await new ProxyApi(base.GetToken()).ProxyListarRepresentante(nroPagina, searchString, codEmpresa.Value, ConfigurationUtilities.PageSize);
            onePageLista.ListaPaginado = onePageLista.ToPageList();

            return PartialView("_Lista", onePageLista);
        }


        private async Task<Representante> ObtenerRepresentante(int id)
        {
            RespuestaOperacionServicio itemOp = new Service().HttpGetJson<RespuestaOperacionServicio>(
                ConfigurationUtilities.WebApiUrl + "/representante/ObtenerRepresentante?id=" + id, GetToken());

            var itemRepresentante = Helpers.ConvertToObject<Representante>(itemOp.data);
            DocumentoController documentoController = new DocumentoController();
            ParametrosComunDTO parametros = await documentoController.CargarParametrosComun();
            var itemEstado = parametros.ListaEstados.Find(x => x.Valor == itemRepresentante.Estado.ToString());
            if (itemEstado != null)
                itemRepresentante.EstadoDescripcion = itemEstado.Campo;

            ViewBag.Estados = parametros.ListaEstados;

            return itemRepresentante;
        }

        [HttpPost]
        public async Task<ActionResult> Grabar(FormCollection form)
        {
            Representante request = new Representante();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModelV3(form, request);

            if (request.CodRepresentante == 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Representante/Registrar", GetToken());
            }
            else//actualizar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Representante/Actualizar", GetToken());
            }
            return Json(respuesta);
        }
        [HttpPost]
        public async Task<ActionResult> Eliminar(int id)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Representante request = new Representante();
            request.CodRepresentante = id;

            if (request.CodRepresentante > 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Representante/Eliminar", GetToken());
            }
            return Json(respuesta);
        }

        public async Task<FileResult> ExportarPDF(string searchString, int codEmpresa)
        {
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDF";
            List<Representante> lista = (await new ProxyApi(base.GetToken()).ProxyListarRepresentante(1, searchString, codEmpresa, int.MaxValue)).Lista;
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }
        public void ExportarExcel(string searchString, int codEmpresa)
        {

            List<Representante> lista = (new ProxyApi(base.GetToken()).ProxyListarRepresentante_Sync(1, searchString, codEmpresa, int.MaxValue)).Lista;
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de Representantes";
            string fileNameReport = "ReporteExcel";

            GenerarExcelResponse<List<Representante>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 9;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "NOMBRE";
                ws.Cell(rowIndex, 3).Value = "CORREO";
                ws.Cell(rowIndex, 4).Value = "TELÉFONO";
                ws.Cell(rowIndex, 5).Value = "ESTADO";
                ws.Cell(rowIndex, 6).Value = "REGISTRADO POR";
                ws.Cell(rowIndex, 7).Value = "FECHA CREACION";
                ws.Cell(rowIndex, 8).Value = "ACTUALIZADO POR";
                ws.Cell(rowIndex, 9).Value = "FECHA ACTUALIZACION";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.CodRepresentante;
                    ws.Cell(rowIndex, 2).Value = item.Nombres;
                    ws.Cell(rowIndex, 3).Value = item.Email;
                    ws.Cell(rowIndex, 4).Value = item.Telefono;
                    ws.Cell(rowIndex, 5).Value = (item.Estado == 1 ? "Activo" : "Inactivo");
                    ws.Cell(rowIndex, 6).Value = item.UsuCreacion;
                    ws.Cell(rowIndex, 7).Value = item.FecCreacion;
                    ws.Cell(rowIndex, 8).Value = item.UsuActualizacion;
                    ws.Cell(rowIndex, 9).Value = item.FecActualizacion;
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 50;
            });
        }
    }
}
