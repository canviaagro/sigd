﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Controllers;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Areas.Mantenimiento.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class AsuntosEstandarController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> Ver(int id)
        {

            return View("_Ver", await ObtenerAsunto(id));
        }
        [HttpGet]
        public async Task<ActionResult> Editar(int id)
        {

            return View("_Editar", await ObtenerAsunto(id));
        }

        [HttpGet]
        public async Task<ActionResult> Nuevo()
        {
            Asunto asunto = new Asunto();

            DocumentoController documentoController = new DocumentoController();
            var parametros = await documentoController.CargarParametrosComun();
            ViewBag.Estados = parametros.ListaEstados;
            asunto.FechaCreacion = DateTime.Now;
            asunto.Estado = 1;
            return View("_Nuevo", asunto);
        }
        [HttpGet]
        // GET: Mantenimiento/AsuntosEstandar
        public async Task<ActionResult> Index()
        {
            int nroPagina = 1;
            MantenimientoAsuntoListadoViewModel viewmodel = new MantenimientoAsuntoListadoViewModel();
            viewmodel.ResultadoBusqueda = new ResultadoBusquedaModel<Asunto>();

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarAsuntos(nroPagina, "", ConfigurationUtilities.PageSize);
            viewmodel.ResultadoBusqueda = onePageListaDocumentos;
            viewmodel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametros = await documentoController.CargarParametrosComun();
            ViewBag.Estados = parametros.ListaEstados;

            return View(viewmodel);
        }
        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString)
        {
            int nroPagina = page == null ? 1 : page.Value;
            ViewBag.searchString = searchString;
            ViewBag.FiltrosActuales = Helpers.ClearString(searchString);

            //Obtener resultado busqueda            
            var onePageLista = await new ProxyApi(base.GetToken()).ProxyListarAsuntos(nroPagina, searchString, ConfigurationUtilities.PageSize);
            onePageLista.ListaPaginado = onePageLista.ToPageList();

            return PartialView("_Lista", onePageLista);
        }


        private async Task<Asunto> ObtenerAsunto(int id)
        {
            RespuestaOperacionServicio itemOp = new Service().HttpGetJson<RespuestaOperacionServicio>(
                ConfigurationUtilities.WebApiUrl + "/asunto/ObtenerAsunto?id=" + id, GetToken());

            var itemAsunto = Helpers.ConvertToObject<Asunto>(itemOp.data);
            DocumentoController documentoController = new DocumentoController();
            ParametrosComunDTO parametros = await documentoController.CargarParametrosComun();
            var itemEstado = parametros.ListaEstados.Find(x => x.Valor == itemAsunto.Estado.ToString());
            if (itemEstado != null)
                itemAsunto.EstadoDescripcion = itemEstado.Campo;

            ViewBag.Estados = parametros.ListaEstados;

            return itemAsunto;
        }

        [HttpPost]
        public async Task<ActionResult> Grabar(FormCollection form)
        {
            Asunto request = new Asunto();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModelV3(form, request);

            if (request.CodAsunto == 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Asunto/Registrar", GetToken());
            }
            else//actualizar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Asunto/Actualizar", GetToken());
            }
            return Json(respuesta);
        }
        [HttpPost]
        public async Task<ActionResult> Eliminar(int id)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Asunto request = new Asunto();
            request.CodAsunto = id;

            if (request.CodAsunto > 0)//registrar
            {
                Service service = new Service();
                respuesta = await Service.PostAsync<RespuestaOperacionServicio>(request, "Asunto/Eliminar", GetToken());
            }
            return Json(respuesta);
        }

        public async Task<FileResult> ExportarPDF(string searchString, string FechaDesde, string FechaHasta)
        {
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDF";
            string fileNameReport = "ReportePDF";
            List<Asunto> lista = (await new ProxyApi(base.GetToken()).ProxyListarAsuntos(1, searchString, int.MaxValue)).Lista;
            return PDFFileResult(viewReport, lista, fileNameReport, true);
        }
        public void ExportarExcel(string searchString)
        {

            List<Asunto> lista = (new ProxyApi(base.GetToken()).ProxyListarAsuntos_Sync(1, searchString, int.MaxValue)).Lista;
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Lista de asuntos";
            string fileNameReport = "ReporteExcel";

            GenerarExcelResponse<List<Asunto>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 7;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "CÓDIGO";
                ws.Cell(rowIndex, 2).Value = "DESCRIPCIÓN";
                ws.Cell(rowIndex, 3).Value = "ESTADO";
                ws.Cell(rowIndex, 4).Value = "REGISTRADO POR";
                ws.Cell(rowIndex, 5).Value = "FECHA CREACION";
                ws.Cell(rowIndex, 6).Value = "ACTUALIZADO POR";
                ws.Cell(rowIndex, 7).Value = "FECHA ACTUALIZACION";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.CodAsunto;
                    ws.Cell(rowIndex, 2).Value = item.Descripcion;
                    ws.Cell(rowIndex, 3).Value = (item.Estado == 1 ? "Activo" : "Inactivo");
                    ws.Cell(rowIndex, 4).Value = item.UsuarioCreacion;
                    ws.Cell(rowIndex, 5).Value = item.FechaCreacion;
                    ws.Cell(rowIndex, 6).Value = item.UsuarioActualizacion;
                    ws.Cell(rowIndex, 7).Value = item.FechaActualizacion;
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 50;
            });
        }


    }
}
