﻿var unavezMenu = true;
var feature;
$(document).ready(function () {
    feature = (function () {
        var sideNav = $('.sidenav');
        var collapsible = $('.collapsible');
        var menuOpen = $('#menu-open');
        var body = $('body');
        var sidenavOverlay = $('.sidenav-overlay');
        var dropdownTrigger = $('.dropdown-trigger');
        var modal = $('.modal');
        var tabs = $('.tabs');
        var closeSidebar = $('.btn-close-sidebar');

        //var autoComplete = $('.autocomplete');

        var refresh = function () {

            var datepicker = $('.datepicker');
            var select = $('select');

            datepicker.datepicker({
                autoClose: true,
                format: 'dd/mm/yyyy',
                i18n: {
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                    weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                    weekdaysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    weekdaysAbbrev: ["D", "L", "M", "M", "J", "V", "S"]
                }
            });
            $(".clearicon").on("click", function (x) {
                $(this).parent().find("input").val("");
            });

            select.formSelect();
        };
        var init = function () {
            //sideNav.sidenav();
            collapsible.collapsible();
            dropdownTrigger.dropdown();
            modal.modal({
                dismissible: false
            });
            tabs.tabs();

            //autoComplete.autocomplete({
            //    onAutocomplete: onAutocomplete,
            //     data: {
            //         "Apple 123": 123,
            //         "Microsoft 123": 123,
            //        "Google 123": 123
            //     }
            // });



            refresh();
            cargarMesaPartes();
            habilitarMenuPagina();
            mostrarBreadCrumb();
        };

        //var onAutocomplete = function (e) {
        //    var str = e.split("|");
        //    var codigo = str[0];
        //    console.log(codigo);
        //};
        var mostrarBreadCrumb = function () {
            var valorbc1 = $("#hdnbreadCrumbNivel1").val();
            var valorbc2 = $("#hdnbreadCrumbNivel2").val();
            var valorbc3 = $("#hdnbreadCrumbNivel3").val();
            if (!(valorbc1 === null || valorbc1 === "")) {
                $("#breadCrumbNivel1").html(valorbc1);
            }
            else {
                $("#breadCrumbNivel1").parent().remove();
            }
            if (!(valorbc2 === null || valorbc2 === "")) {
                $("#breadCrumbNivel2").html(valorbc2);
            }
            else {
                $("#breadCrumbNivel2").parent().remove();
            }
            if (!(valorbc3 === null || valorbc3 === "")) {
                $("#breadCrumbNivel3").html(valorbc3);
            }
            else {
                $("#breadCrumbNivel3").parent().remove();
            }
        };
        var cargarMesaPartes = function () {

            var txtPlazoChange = function (plazo) {
                alert(plazo * 2);
            }
        };
        var habilitarMenuPagina = function () {
            $(".collapsible-body.active").each(function () { $(this).addClass("active").parent().addClass("active"); });
            //$(".collapsible-body.active").each(function () { $(this).addClass("active").parent().addClass("active").find(".collapsible-body"); });
        };
        var showSidebar = function () {
            body.addClass('menu-open');
            //sideNav.sidenav('open');
            sidenavOverlay.attr('style', 'display: block; opacity: 1;');
            if (unavezMenu) {
                $(".collapsible-body.active").parent().addClass("active").find(".collapsible-body").css("display", "block");
                unavezMenu = false;
            }
        };

        var hideSidebar = function () {
            body.removeClass('menu-open');
            sidenavOverlay.removeAttr('style');


            $(".collapsible-header").map(function (e) {
                console.log(this)
                var $this = $(this);
                var $li = $($this.parent('li'))
                if ($li.hasClass('active')) {
                    $li.removeClass('active');
                    $li.children('.collapsible-body').removeAttr("style");
                }
            }).get().join();
        };

        sideNav.click(showSidebar);
        menuOpen.click(showSidebar);
        closeSidebar.click(hideSidebar);
        sidenavOverlay.click(hideSidebar);

        return {
            init: init,
            refresh: refresh
        };
    })();

    feature.init();
});
