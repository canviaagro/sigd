﻿using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    public class RedirectController : Controller
    {
        

        // GET: Redirect
        public ActionResult Index(string Token, string IdPerfil)
        {
            if (!string.IsNullOrEmpty(Token))
            {
                Session["TokenSIGD"] = Token;
                Session["IdPerfilSIGD"] = IdPerfil;

                try
                {

                    //HelperAgroSeguridad helper = new HelperAgroSeguridad();

                    ////ResultadoObtenerModel<EUsuario> usuarioResponse = helper.ObtenerSesionUsuario(Token, IdPerfil);
                    //ResultadoObtenerModel<SUsuario> usuarioResponse = helper.ObtenerSesionUsuario(Token, IdPerfil);

                    string strAction = "Index";
                    string strController = "Home";                  

                    return RedirectToAction(strAction, strController);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("UnauthorizedOperation", "Error", new { msjErrorExcepcion = ex.Message });
                }
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult RedirectExternalUrl(string optionRedirect)
        {
            if (optionRedirect == "1")
            {
                Session.Clear();
                Session.Abandon();
                Uri Url = Request.Url;
                string Link = $"{SeguridadKeys.UrlWebSSA}/Acceso/ValidateSesionExternal?urlApp={Url.Authority}";
                return Redirect(Link);
            }
            else
            {
                Session.Clear();
                Session.Abandon();
                Uri Url = Request.Url;
                string Link = $"{SeguridadKeys.UrlWebSSA}/Acceso/ValidateSesionExternal?urlApp={Url.Authority}";
                return JavaScript($"window.location = '{Link}'");
            }
        }

        [HttpGet]
        public ActionResult TrabajadorSIGDInexistente(string usuarioWeb)
        {
            ViewBag.usuarioWeb = usuarioWeb;

            return View();
        }

        [HttpGet]
        public ActionResult UsuarioSIEDSinPermiso()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Inactividad()
        {
            return View();
        }
    }
}