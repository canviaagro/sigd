﻿using Agrobanco.SIGD.Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Agrobanco.SIGD.Comun;
using System.Threading.Tasks;
using EN = Agrobanco.SIGD.Entidades.Entidades;
using PagedList;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Configuration;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using static Agrobanco.SIGD.Comun.Enumerados;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;

namespace Agrobanco.SIGD.Presentacion.Controllers
{

    [AutenticacionUsuario]
    [ErrorFilter]
    public class MesaPartesController : BaseController
    {
        public MesaPartesController()
        {
        }

        //[HttpGet]
        //public ActionResult ObtenerDocumentoMesaPartes(string codDocumento)
        //{
        //    var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
        //    Service _service = new Service();
        //    RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
        //    var jsR = Json(rptServ, JsonRequestBehavior.AllowGet);
        //    jsR.MaxJsonLength = int.MaxValue;

        //return jsR;
        //}
        [HttpGet]
        public ActionResult VerDocumento(string codDocumento)
        {
            return View("_VerDocumento");
        }
        public FileResult ExportarDocumentoLaserFicher(int CodFiche)
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=") + CodFiche, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationManager.AppSettings["MesaPartesExportarDocumentoLaserFicher"].ToString() + CodFiche, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=" + CodFiche, strRptToken);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

            if (rptServ.Resultado == 1)
            {
                rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptServ.data);
            }

            rptServ.data = rptArchivos;

            //MemoryStream ms = HelperIT.Base64ToMemoryStream(rptArchivos.ListaDocumentos[0].ArchivoBase64);

            byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);


            return File(ms2.GetBuffer(), "application/pdf", string.Format(rptArchivos.ListaDocumentos[0].NombreArchivo, DateTime.UtcNow.AddHours(TimeZoneOffset).ToFormattedDate(ConfigurationUtilities.FormatoFechaNombreReporte)));
        }

        public FileResult ExportarPDF_CargoRecepcion(int codDocumento)
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationManager.AppSettings["MesaPartesObtenerDocumento"].ToString() + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            return PDFFileResult("PDF_CargoRecepcion", _documento, ConfigurationUtilities.NombreReporteBandejaMesaPartes, true);
        }

        public FileResult ExportarPDF_HojaResumen(int codDocumento)
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationManager.AppSettings["MesaPartesObtenerDocumento"].ToString() + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            return PDFFileResult("PDF_HojaResumen", _documento, ConfigurationUtilities.NombreReporteBandejaMesaPartes, true);
        }


        public FileResult ExportarArchivosZIP(int codDocumento)
        {

            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationManager.AppSettings["MesaPartesObtenerDocumento"].ToString() + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream s = new ZipOutputStream(outputMemStream);

            s.SetLevel(9); // 0-9, 9 being the highest compression  

            foreach (var item in _documento.ArchivosLaserFiche.ListaDocumentos)
            {
                ZipEntry entry = new ZipEntry(item.NombreArchivo);
                entry.DateTime = DateTime.Now;
                entry.IsUnicodeText = true;
                s.PutNextEntry(entry);

                byte[] imageBytes = Convert.FromBase64String(item.ArchivoBase64);
                MemoryStream ms2 = new MemoryStream(imageBytes, 0,
                  imageBytes.Length, true, true);

                StreamUtils.Copy(ms2, s, new byte[4096]);
                ms2.Close();
                s.CloseEntry();
            }

            s.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            s.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;

            return File(outputMemStream.ToArray(), "application/octet-stream", "reports.zip");
        }



        public FileResult ExportarPDF(string searchString, string FechaDesde, string FechaHasta)
        {
            //  string strBusquedaDoc = TempData[TempDataKeys.BusquedaDocumento] as string;

            List<DocumentoBandejaDTO> lstDocumentos = ListarDocumentos(searchString, FechaDesde, FechaHasta);
            return PDFFileResult("PDF_ListaDocumentos", lstDocumentos, ConfigurationUtilities.NombreReporteBandejaMesaPartes, true);
        }

        public ActionResult PreviewPDF(string searchString, string FechaDesde, string FechaHasta)
        {
            List<DocumentoBandejaDTO> lstDocumentos = ListarDocumentos(searchString, FechaDesde, FechaHasta);
            return View("PDF_ListaDocumentos", lstDocumentos);
        }

        public void ExportarExcel(string searchString, string FechaDesde, string FechaHasta)
        {
            // string strBusquedaDoc = TempData[TempDataKeys.BusquedaDocumento] as string;

            List<DocumentoBandejaDTO> lstDocumentos = ListarDocumentos(searchString, FechaDesde, FechaHasta);

            GenerarExcelResponse<List<DocumentoBandejaDTO>>(ConfigurationUtilities.NombreReporteBandejaMesaPartes, "BandejaMesaPartes", lstDocumentos, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 8;
                ws.Cell(rowIndex, 1).Value = "Lista de Documentos Bandeja de Partes";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "PRIORIDAD";
                ws.Cell(rowIndex, 2).Value = "ESTADO";
                ws.Cell(rowIndex, 3).Value = "RECIBIDO";
                ws.Cell(rowIndex, 4).Value = "NRO. TRÁMITE";
                ws.Cell(rowIndex, 5).Value = "DOCUMENTO";
                ws.Cell(rowIndex, 6).Value = "REMITENTE";
                ws.Cell(rowIndex, 7).Value = "DERIVADO";
                ws.Cell(rowIndex, 8).Value = "PLAZO";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.EstadoPrioridad;
                    ws.Cell(rowIndex, 2).Value = item.EstadoDescripcion;
                    ws.Cell(rowIndex, 3).Value = item.Recibido;
                    ws.Cell(rowIndex, 4).Value = item.Correlativo;
                    ws.Cell(rowIndex, 5).Value = item.Documento + "\r" + item.DocumentoAsunto;
                    ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 6).Value = item.NombreEmpresa + "\r" + item.Remitente;
                    ws.Cell(rowIndex, 6).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.NombreTrabajador;
                    ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 8).Value = item.Plazo;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }

                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 10;
            });
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var sesion = Filters.HelperSeguridad.ObtenerSessionUsuario();
            var page = 1;
            string FechaDesde = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaHasta = base.MapFecha("", TipoFecha.Api);
            FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Web, -30);
            FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Web);

            if (sesion == null)
                return RedirectToAction("Index", "Seguridad");

            await CargarParametros();

            var onePageListaDocumentos = ListarBandejaDocumentos(page,"", ConfigurationUtilities.PageSize, FechaDesde, FechaHasta);

            if (Request.IsAjaxRequest())
                return PartialView("_ListaDocumentos", onePageListaDocumentos);
           
            return View(onePageListaDocumentos);
        }


        [HttpPost]
        public async Task<ActionResult> Index(int? page, string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {
            var sesion = Filters.HelperSeguridad.ObtenerSessionUsuario();
            page = page == null ? 1 : page.Value;
            FechaRegistroDesde = base.MapFecha(FechaRegistroDesde, TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            FechaRegistroHasta = base.MapFecha(FechaRegistroHasta, TipoFecha.Api);
            ViewBag.FechaRegistroDesde = FechaRegistroDesde;
            ViewBag.FechaRegistroHasta = FechaRegistroHasta;
            ViewBag.FiltrosActuales = searchString;

            if (sesion == null)
                return RedirectToAction("Index", "Seguridad");

            await CargarParametros();

            var onePageListaDocumentos = ListarBandejaDocumentos(page, searchString, ConfigurationUtilities.PageSize, FechaRegistroDesde, FechaRegistroHasta);

            if (Request.IsAjaxRequest())
                return PartialView("_ListaDocumentos", onePageListaDocumentos);

            return View(onePageListaDocumentos);
        }

        public IPagedList<DocumentoBandejaDTO> ListarBandejaDocumentos(int? page,string searchString,  int pageSize,string strFecharegistraDesdePar, string strFecharegistraHastaPar)
        {

            int pageNumber = page ?? 1;
            List<DocumentoBandejaDTO> lstDocumentos = ListarDocumentos(searchString, strFecharegistraDesdePar, strFecharegistraHastaPar);
            var onePageListaDocumentos = lstDocumentos.ToPagedList(pageNumber, pageSize);
            return onePageListaDocumentos;
        }

        public PartialViewResult BuscarDocumentos(string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {

            // TempData[TempDataKeys.BusquedaDocumento] = searchString;

            FechaRegistroDesde = Convert.ToDateTime(FechaRegistroDesde).ToString("yyyy-MM-dd");
            FechaRegistroHasta = Convert.ToDateTime(FechaRegistroHasta).ToString("yyyy-MM-dd");

            ViewBag.FechaRegistroDesde = FechaRegistroDesde;
            ViewBag.FechaRegistroHasta = FechaRegistroHasta;
            ViewBag.FiltrosActuales = searchString;

            List<DocumentoBandejaDTO> lstDocumentos = ListarDocumentos(searchString, FechaRegistroDesde, FechaRegistroHasta);
            var onePageListaDocumentos = lstDocumentos.ToPagedList(1, ConfigurationUtilities.PageSize);

            return PartialView("_ListaDocumentos", onePageListaDocumentos);
        }

        private List<DocumentoBandejaDTO> ListarDocumentos(string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            searchString = (string.IsNullOrWhiteSpace(searchString)) ? "LISTARTODOS" : searchString;
       
            if (string.IsNullOrWhiteSpace(FechaRegistroDesde) && string.IsNullOrWhiteSpace(FechaRegistroHasta))
            {
                FechaRegistroHasta = DateTime.Today.ToString("yyyy-MM-dd");
                FechaRegistroDesde = Convert.ToDateTime(FechaRegistroHasta).AddDays(-30).ToString("yyyy-MM-dd");
            }
            else
            {
                FechaRegistroDesde = Convert.ToDateTime(FechaRegistroDesde).ToString("yyyy-MM-dd");
                FechaRegistroHasta = Convert.ToDateTime(FechaRegistroHasta).ToString("yyyy-MM-dd");
            }

            List<DocumentoBandejaDTO> lstDoc = _service.HttpGetJson<List<DocumentoBandejaDTO>>(UrlApi.ObtenerUrl("/documento/GetDocumentoBandeja/") + searchString + "/" + FechaRegistroDesde + "/" + FechaRegistroHasta, strRptToken);
            return lstDoc;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegistrarDocumento(FormCollection form)
        {
            EUsuario userSession = Filters.HelperSeguridad.ObtenerSessionUsuario();
            if (userSession == null)
                return RedirectToAction("Index", "Seguridad");


            Documento documento = new Documento();

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModel(form, documento);

            documento.EstadoDocumento = 1;
            bool contenido = validarContenido(Request.Files);

            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                documento.LstDocAdjunto = new List<DocAdjunto>();
                documento.EstadoDocumento = 2;
                documento.FechaDerivacion = DateTime.Now;

                var memorystream = new MemoryStream();
                //var fileExtension = file?.FileName.Split('.');
                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;

                documento.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String });
            }

            //anexos
            if (Request.Files.Count > 0 && contenido)
            {
                documento.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    var memorystream = new MemoryStream();
                    Request.Files[i]?.InputStream.CopyTo(memorystream);
                    byte[] imageBytes = memorystream.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    documento.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });

                }
            }


            documento.CodTrabajador = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodTrabajador : 1;
            documento.CodRepresentante = documento.RemitidoPor;
            //documento.Asunto = viene desde el form
            //documento.FechaPlazo = //viene desde el form;
            //documento.Origen = //viene desde el form;
            documento.FechaRegistro = DateTime.Now;
            documento.DescripcionArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.DescripcionArea : "";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            Service service = new Service();

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(documento, "Documento/registrar", token);

            return Json(respuesta);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditarDocumento(FormCollection form)
        {
            EUsuario userSession = Filters.HelperSeguridad.ObtenerSessionUsuario();
            if (userSession == null)
                return RedirectToAction("Index", "Seguridad");

            Documento documento = new Documento();

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();

            Helpers.ConvertToModelV2(form, documento);
            if (documento.EstadoDocumento == Convert.ToInt16(EstadoDocumento.DEVUELTO))
            {
                documento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.PENDIENTE);
            }
            else
            {
                documento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.INGRESADO);
            }

            bool contenido = validarContenido(Request.Files);
            var memorystream = new MemoryStream();
            //adjunto
            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                documento.LstDocAdjunto = new List<DocAdjunto>();
                documento.EstadoDocumento = Convert.ToInt16(EstadoDocumento.PENDIENTE);
                documento.FechaDerivacion = DateTime.Now;

                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;
                documento.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String });

            }
            //anexos
            if (Request.Files.Count > 0 && contenido)
            {
                documento.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    Request.Files[i]?.InputStream.CopyTo(memorystream);
                    byte[] imageBytes = memorystream.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    documento.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });

                }
            }

            documento.CodTrabajador = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodTrabajador : 1;
            documento.CodRepresentante = documento.RemitidoPor;
            documento.FechaRegistro = DateTime.Now;

            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            Service service = new Service();

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(documento, "Documento/Actualizar", token);

            return Json(respuesta);
        }

        private bool validarContenido(HttpFileCollectionBase request)
        {
            bool validar = false;
            // 1 paraa no validar el adjunto, solo anexos
            for (int i = 1; i < request.Count; i++)
            {
                if (request[i].ContentLength > 0)
                {
                    validar = true;
                }
            }
            return validar;
        }

        private async Task CargarParametros()
        {
            ParametrosMesaPartesDTO parametrosMesaPartes = await CargarParametrosMesaPartes();
            //ParametrosMesaPartesDTO parametrosMesaPartes = new ParametrosMesaPartesDTO();
            //var task = CargarParametrosMesaPartes();
            //Task.WaitAll(task);
            //ParametrosMesaPartesDTO parametrosMesaPartes = task.Result;

            ViewBag.ListaArea = parametrosMesaPartes.LstArea;
            ViewBag.ListaOrigen = parametrosMesaPartes.LstOrigen;
            ViewBag.ListaTipoDocumento = parametrosMesaPartes.LstTipoDocumento;
            ViewBag.ListaPrioridad = parametrosMesaPartes.LstPrioridad;
            ViewBag.DiasPlazoNuevoDoc = Convert.ToInt32(ConfigurationManager.AppSettings["PlazoDiasDocumento"]);

        }

        private async Task<ParametrosMesaPartesDTO> CargarParametrosMesaPartes()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosMesaPartesDTO parametros = new ParametrosMesaPartesDTO();

            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            respuesta.data = new ParametrosMesaPartesDTO();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosMP", token);

            if (respuesta.Resultado == 1)
            {
                parametros = Helpers.ConvertToObject<ParametrosMesaPartesDTO>(respuesta.data);
            }

            return parametros;
        }
        private List<Parametro> CargarListaOrigen()
        {
            List<Parametro> listaOrigen = new List<Parametro>();

            //listaOrigen.Add(new Parametro() { IdParametro = 1, IdGrupo = 1, Estado = 1, Valor = "1", Campo = "INTERNO" });
            //listaOrigen.Add(new Parametro() { IdParametro = 2, IdGrupo = 1, Estado = 1, Valor = "2", Campo = "EXTERNO" });

            return listaOrigen;
        }

        private List<TipoDocumento> CargarTipoDocumento()
        {
            List<TipoDocumento> listaTipoDocumento = new List<TipoDocumento>();

            //listaTipoDocumento.Add(new TipoDocumento { IdTipoDocumento = 1, Abreviatura="CAR", Descripcion= "CARTA", Estado = 1 });
            //listaTipoDocumento.Add(new TipoDocumento { IdTipoDocumento = 2, Abreviatura = "INF", Descripcion = "INFORME", Estado = 1 });
            //listaTipoDocumento.Add(new TipoDocumento { IdTipoDocumento = 3, Abreviatura = "MEM", Descripcion = "MEMORANDUM", Estado = 1 });
            //listaTipoDocumento.Add(new TipoDocumento { IdTipoDocumento = 4, Abreviatura = "OFI", Descripcion = "OFICIO", Estado = 1 });
            //listaTipoDocumento.Add(new TipoDocumento { IdTipoDocumento = 5, Abreviatura = "RES", Descripcion = "RESOLUCIÓN", Estado = 1 });

            return listaTipoDocumento;
        }


    }
}