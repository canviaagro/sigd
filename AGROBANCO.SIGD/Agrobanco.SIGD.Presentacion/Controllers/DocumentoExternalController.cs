﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    public class DocumentoExternalController : Controller
    {
        private static int _CodigoArea = 0;
        private static int _UsuarioFirmante = 0;
        private static int? _codDocDerivacion = null;
        private static string _reason = "";
        private static int _posx = 0;
        private static int _posy = 0;

        // GET: DocumentoExternal
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public FileResult GetDocumentoLaserFiche(int codLaserfiche)
        {
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/ObtenerDocumentoLaserfiche/") + codLaserfiche);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

            if (rptServ.Resultado == 1)
            {
                rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptServ.data);
            }

            rptServ.data = rptArchivos;

            byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);


            return File(ms2.GetBuffer(), "application/pdf", string.Format(rptArchivos.ListaDocumentos[0].NombreArchivo));
        }


        [AllowAnonymous]
        [HttpGet]
        public FileResult GetDocumentoLaserFicheIMG(int codLaserfiche)
        {
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/ObtenerDocumentoLaserficheIMG/") + codLaserfiche);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

            if (rptServ.Resultado == 1)
            {
                rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptServ.data);
            }

            rptServ.data = rptArchivos;

            byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);


            return File(ms2.GetBuffer(), "image/png", string.Format(rptArchivos.ListaDocumentos[0].NombreArchivo));
        }

        [AllowAnonymous]
        [HttpGet]
        public string GetArgumentosFirma(string NombreArchivo,int CodArea, int CodFirmante,int CodDocDerivacion,int CodDocumento,string reason)
        {
            string response = "";
            var idCodLaserfiche = this.GetCodLaserfichePorCodDocumento(CodDocumento).data; 
            var EstampadoFirmaDigital = ConfigurationManager.AppSettings["EstampadoFirmaDigital"];
            var clientId = ConfigurationManager.AppSettings["clientIdFirmaDigital"].ToString();
            var clientSecret = ConfigurationManager.AppSettings["clientSecretFirmaDigital"].ToString();
            var posx = "400";
            var posy = "0";
            var sellospermitidosxpag = 8;
            var objFirmas = this.GetCantidadFirmas(CodDocumento);
            var cantFirmas = Convert.ToInt32(objFirmas.data);

            if (cantFirmas >= sellospermitidosxpag)
            {
                posx = "0";
                posy = (cantFirmas==sellospermitidosxpag)?"0":(objFirmas.posy + 100).ToString();
            }
            else if(cantFirmas > 0 && cantFirmas <= sellospermitidosxpag)
            {
                posy = (objFirmas.posy+100).ToString();
            }

            _posy = Convert.ToInt32(posy);
            _CodigoArea = CodArea;
            _UsuarioFirmante = CodFirmante;
            _codDocDerivacion = CodDocDerivacion;
            _posx = Convert.ToInt32(posx);

            ParameterRequest parameter = new ParameterRequest();
            parameter.app = "pdf"; 
            parameter.clientId = clientId;
            parameter.clientSecret = clientSecret;
            parameter.idFile = idCodLaserfiche.ToString();
            parameter.type = "W";
            parameter.protocol = "T";
            parameter.fileDownloadUrl = UrlApi.ObtenerUrlBase("/DocumentoExternal/GetDocumentoLaserFiche?codLaserfiche="+idCodLaserfiche.ToString());
            parameter.fileDownloadLogoUrl = "";
            parameter.fileDownloadStampUrl = EstampadoFirmaDigital;
            parameter.fileUploadUrl = UrlApi.ObtenerUrlBase("/DocumentoExternal/UploadDocumentoLaserFiche");
            parameter.contentFile = NombreArchivo;
            parameter.reason = reason;
            parameter.isSignatureVisible = "true";
            parameter.stampAppearanceId = "0";
            parameter.pageNumber = "0";
            parameter.posx = posx;
            parameter.posy = posy;
            parameter.fontSize = 7.ToString();
            parameter.dcfilter = "";
            parameter.timestamp = "false";
            parameter.signatureLevel = "";
            parameter.outputFile = RenameOutputFile(NombreArchivo);
            parameter.maxFileSize = 5242880.ToString();
            _reason = parameter.reason;
            response = Convert.ToBase64String(parameter.ToJson().ToBytes());

            return response;
        }


        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PostDocumentoLaserFiche(FormCollection form)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();

            bool contenido = validarContenido(Request.Files);

            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {

                var memorystream = new MemoryStream();
                //var fileExtension = file?.FileName.Split('.');
                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;
            }

            return Json(respuesta);
        }

        private bool validarContenido(HttpFileCollectionBase request)
        {
            bool validar = false;
            // 1 paraa no validar el adjunto, solo anexos
            for (int i = 1; i < request.Count; i++)
            {
                if (request[i].ContentLength > 0)
                {
                    validar = true;
                }
            }
            return validar;
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> UploadDocumentoLaserFicheV2(HttpPostedFileBase file)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            var nombreArchivo = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
            Documento objDoc = new Documento();
            objDoc.LstDocAdjunto = new List<DocAdjunto>();
            MemoryStream target = new MemoryStream();
            file.InputStream.CopyTo(target);
            byte[] data = target.ToArray();
            string base64String = Convert.ToBase64String(data);

            objDoc.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String, NombreArchivo = nombreArchivo });

            respuesta = await Service.PostAsyncExternal<RespuestaOperacionServicio>(objDoc, UrlApi.ObtenerUrl("documento/SubirDocumentoLaserFicher"));

            return Json(respuesta);
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> UploadDocumentoLaserFiche(FormCollection form)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            var nombreArchivo = System.IO.Path.GetFileNameWithoutExtension(Request.Files[0].FileName);
            Documento objDoc = new Documento();
            objDoc.CodTrabajador = _UsuarioFirmante;
            objDoc.CodArea = _CodigoArea;
            objDoc.CodDocDerivacion = ((_codDocDerivacion == 0 )? null : _codDocDerivacion);
            objDoc.reason = "MOTIVO FIRMA : " + _reason;
            objDoc.posx = _posx;
            objDoc.posy = _posy;

            var memorystream = new MemoryStream();
            Request.Files[0]?.InputStream.CopyTo(memorystream);
            var codLaserFiche = Convert.ToInt32(Request.Files.AllKeys[0]);
            byte[] imageBytes = memorystream.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);
            imageBytes = null;
            objDoc.LstDocAdjunto = new List<DocAdjunto>();
            objDoc.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String, NombreArchivo = nombreArchivo, CodLaserfiche = codLaserFiche });

            respuesta = await Service.PostAsyncExternal<RespuestaOperacionServicio>(objDoc, UrlApi.ObtenerUrl("documento/SubirDocumentoLaserFicher"));

            return Json(respuesta);
        }


        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> DocumentoEnAtencionFirma(int CodDocumento)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            var EnAtencion = 1;
            respuesta = await Service.GetAsyncExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/DocumentoAtencionFirma?CodDocumento=" + CodDocumento+"&Atencion="+EnAtencion));
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> DocumentoAtendidoFirma(int CodDocumento)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            var EnAtencion = 0;
            respuesta = await Service.GetAsyncExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/DocumentoAtencionFirma?CodDocumento=" + CodDocumento + "&Atencion=" + EnAtencion));
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> ObtenerEstadoFirmaDocumento(int CodDocumento)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            respuesta = await Service.GetAsyncExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/ObtenerEstadoFirmaDocumento?CodDocumento=" + CodDocumento));
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }


        [AllowAnonymous]
        [HttpGet]
        public RespuestaOperacionServicio GetCantidadFirmas(int CodDocumento)
        {
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/GetCantidadFirmas?CodDocumento=") + CodDocumento);
            return rptServ;
        }


        [AllowAnonymous]
        [HttpGet]
        public RespuestaOperacionServicio ObtenerOrigen(int CodDocumento)
        {
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/ObtenerOrigen?CodDocumento=") + CodDocumento);
            return rptServ;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> GetCantidadFirmasAsync(int CodDocumento)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            respuesta = await Service.GetAsyncExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/GetCantidadFirmas?CodDocumento=") + CodDocumento);
            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public RespuestaOperacionServicio GetCodLaserfichePorCodDocumento(int CodDocumento)
        {
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/GetCodLaserFIchePorCodDocumento?CodDocumento=") + CodDocumento);
            return rptServ;
        }

        private string RenameOutputFile(string cadena)
        {
            var extension = Path.GetExtension(cadena);
            var cadenaind = cadena.LastIndexOf(".");
            var cadenaMod = cadena.Substring(0, cadenaind)+"_R";
            return cadenaMod + extension;
        }
    }
}