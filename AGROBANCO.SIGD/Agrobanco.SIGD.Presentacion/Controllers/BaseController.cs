﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    public enum TipoFecha
    {
        Api,
        Web
    }
    public class BaseController : Controller
    {
        public EUsuario GetUsuario()
        {
            var sesion = Agrobanco.SIGD.Presentacion.Filters.HelperSeguridad.ObtenerSessionUsuario();
            return sesion;
        }
        public int? GetCodigoTrabajadorUsuario()
        {
            int? codtrabajador = null;
            var otrabajadorLogin = GetUsuario().MaeTrabajador;
            if (otrabajadorLogin != null)
            {
                codtrabajador = otrabajadorLogin.iCodTrabajador;
            }
            return codtrabajador;
        }

        public string GetCodigoPerfilUsuario()
        {
            string codperfil = "-";/*AGRO-REQ-020 - Integración SGS*/
            //var oPerfil = GetUsuario().Perfil;
            //if (oPerfil != null)
            //{
            //    codperfil = oPerfil.vCodPerfil;
            //}
            return codperfil;
        }
        

        public string MapFecha(string Fecha, TipoFecha tipofecha, int addNumeroDias = 0)
        {
            string _fecha = "";
            if (String.IsNullOrEmpty(Fecha))
            {
                if (tipofecha == TipoFecha.Api)
                {
                    _fecha = DateTime.Now.AddDays(addNumeroDias).ToString("yyyy-MM-dd");
                }
                else if (tipofecha == TipoFecha.Web)
                {
                    _fecha = DateTime.Now.AddDays(addNumeroDias).ToString("dd/MM/yyyy");
                }
            }
            else
            {
                if (tipofecha == TipoFecha.Api)
                {
                    _fecha = Convert.ToDateTime(Fecha).ToString("yyyy-MM-dd");
                }
                else if (tipofecha == TipoFecha.Web)
                {
                    _fecha = Convert.ToDateTime(Fecha).ToString("dd/MM/yyyy");
                }
            }
            return _fecha;
        }
        public int TimeZoneOffset
        {
            get
            {
                return Request.Cookies["TimeZone"] != null &&
                    Request.Cookies["TimeZone"].Value != null &&
                    Request.Cookies["TimeZone"].Value.TryParseToInt()
                    ? Request.Cookies["TimeZone"].Value.ToInt() : 0;
            }
        }
        public string GetToken()
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            return strRptToken;
        }
        public string RenderViewAsString(string viewName, object model)
        {
            // Create a string writer to receive the HTML code
            StringWriter stringWriter = new StringWriter();

            // Get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);

            // Create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                        ControllerContext,
                        viewResult.View,
                        new ViewDataDictionary(model),
                        new TempDataDictionary(),
                        stringWriter
                    );

            // Render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            // return the HTML code
            return stringWriter.ToString();
        }

        public FileResult PDFFileResult(string viewName, object viewModel, string fileName, bool landscape = false)
        {
            var ms = new MemoryStream();

            var rectangle = landscape ? new RectangleReadOnly(842, 595) : PageSize.A4;

            using (var document = new Document(rectangle))
            {
                var writer = PdfWriter.GetInstance(document, ms);

                document.Open();

                // CSS
                var cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                // HTML
                var cssAppliers = new CssAppliersImpl();
                var htmlContext = new HtmlPipelineContext(cssAppliers);

                htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                // PIPELINES
                var pdfPipeline = new PdfWriterPipeline(document, writer);
                var htmlPipeline = new HtmlPipeline(htmlContext, pdfPipeline);
                var cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);

                var worker = new XMLWorker(cssPipeline, true);
                var parser = new XMLParser(worker);

                var html = RenderViewAsString(viewName, viewModel);

                html = html.Replace("{logo}", Server.MapPath("~/Content/img/logo-principal.png"));

                using (var s = new MemoryStream(html.ToBytes()))
                {
                    parser.Parse(s);
                }

                worker.Close();
                document.Close();
            }

            return File(ms.GetBuffer(), "application/pdf", string.Format(fileName + ".pdf", DateTime.UtcNow.AddHours(TimeZoneOffset).ToFormattedDate(ConfigurationUtilities.FormatoFechaNombreReporte)));
        }

        //public FileResult PDFCargoFileResult()
        //{
        //    return File
        //}

        public string GenerarDocumentoPDF(string viewName, object viewModel, string fileName)
        {
            var ms = new MemoryStream();
            string resultado = string.Empty;
            var rectangle = PageSize.A4;

            using (var document = new Document(rectangle))
            {
                var writer = PdfWriter.GetInstance(document, ms);

                document.Open();
                
                // CSS
                var cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                // HTML
                var cssAppliers = new CssAppliersImpl();
                var htmlContext = new HtmlPipelineContext(cssAppliers);
                
                htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                // PIPELINES
                var pdfPipeline = new PdfWriterPipeline(document, writer);
                var htmlPipeline = new HtmlPipeline(htmlContext, pdfPipeline);
                var cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);

                var worker = new XMLWorker(cssPipeline, true);
                var parser = new XMLParser(worker);

                var html = RenderViewAsString(viewName, viewModel);

                html = html.Replace("{logo}", Server.MapPath("~/Content/img/logo-principal.png"));

                using (var s = new MemoryStream(html.ToBytes()))
                {
                    parser.Parse(s);
                }

                worker.Close();
                document.Close();
            }

            byte[] imageBytes = ms.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);
            imageBytes = null;

            resultado = base64String;

            return resultado;
        }

        public void GenerarExcelResponse<T>(string fileName, string worksheetName, T data, Action<IXLWorksheet, T> writeData) where T : class
        {
            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add(worksheetName);

            //var rango = worksheet.Range("A5:F5"); //Seleccionamos un rango
            ////rango.Style.Border.SetOutsideBorder(XLBorderStyleValues.DashDot); //Generamos las lineas exteriores
            ////rango.Style.Border.SetInsideBorder(XLBorderStyleValues.Medium); //Generamos las lineas interiores
            //rango.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
            //rango.Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);

            var imagePath = Server.MapPath("~/Content/img/logo-principal.png");

            worksheet.AddPicture(imagePath)
                .MoveTo(worksheet.Cell("A1"))
                .Scale(0.5); // optional: resize picture

            writeData(worksheet, data);

            var _fileName = string.Format(fileName, DateTime.UtcNow.AddHours(TimeZoneOffset).ToFormattedDate(ConfigurationUtilities.FormatoFechaNombreReporte));

            //Add excel to response
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", _fileName));

            using (var memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            //Clear content
            workbook.Dispose();

            Response.End();
        }


        public FileResult PDFreporteMesaPartesResult(string viewName, object viewModel, string fileName, bool landscape = false)
        {
            var ms = new MemoryStream();

            var rectangle = landscape ? new RectangleReadOnly(842, 595) : PageSize.A4;

            using (var document = new Document(rectangle))
            {
                var writer = PdfWriter.GetInstance(document, ms);

                document.Open();

                // CSS
                var cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                // HTML
                var cssAppliers = new CssAppliersImpl();
                var htmlContext = new HtmlPipelineContext(cssAppliers);

                htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                // PIPELINES
                var pdfPipeline = new PdfWriterPipeline(document, writer);
                var htmlPipeline = new HtmlPipeline(htmlContext, pdfPipeline);
                var cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);

                var worker = new XMLWorker(cssPipeline, true);
                var parser = new XMLParser(worker);


                var html = RenderViewAsString(viewName, viewModel);

                html = html.Replace("{logo}", Server.MapPath("~/Content/img/logo-principal.png"));

                using (var s = new MemoryStream(html.ToBytes()))
                {
                    parser.Parse(s);
                }

                worker.Close();
                document.Close();
            }

            return File(ms.GetBuffer(), "application/pdf", string.Format(fileName + ".pdf", DateTime.UtcNow.AddHours(TimeZoneOffset).ToFormattedDate(ConfigurationUtilities.FormatoFechaNombreReporte)));
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}