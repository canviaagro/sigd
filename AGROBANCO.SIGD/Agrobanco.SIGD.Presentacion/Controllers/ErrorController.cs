﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        public ActionResult AccesoRestringido()
        {
            return View();
        }

        public ActionResult NoEncontrado()
        {
            return View();
        }

        public ActionResult ErrorInterno()
        {
            return View();
        }

        public ActionResult AccesoRestringidoAjax()
        {
            return PartialView("_AccesoRestringidoAjax");

        }
        public ActionResult NoEncontradoAjax()
        {
            return PartialView("_NoEncontradoAjax");
        }


        public ActionResult ErrorAjax()
        {
            return PartialView("_ErrorAjax");
        }

        [HttpGet]
        public ActionResult UnauthorizedOperation(string operacion, string modulo, string msjErrorExcepcion)
        {

            ViewBag.operacion = operacion;
            ViewBag.modulo = modulo;
            ViewBag.msjErrorExcepcion = msjErrorExcepcion;
            return View();
        }
    }
}