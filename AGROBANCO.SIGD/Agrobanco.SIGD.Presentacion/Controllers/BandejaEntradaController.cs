﻿using Agrobanco.SIGD.Presentacion.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Utilities;
using PagedList;
using ClosedXML.Excel;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Proxy;
using System.IO;
using static Agrobanco.SIGD.Comun.Enumerados;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class BandejaEntradaController : BaseController
    {
        public ActionResult Index(int? page)
        {
            return RedirectToAction("Recibidos", "BandejaEntrada");
        }
        
        [HttpGet]
        public ActionResult VerDocumento(string codDocumento)
        {
            return View("_VerDocumento");
        }
        [HttpGet]
        public ActionResult VerDocumentoSIED(string codDocumento)
        {
            return View("_VerDocumentoSIED");
        }
        [HttpGet]
        // GET: BandejaEntrada
        public async Task<ActionResult> Recibidos()
        {
            int iCodTrabajador = 0;
            int nroPagina = 1;
            int tipobandeja = 1;
            string vCodPerfil = "";
            string FechaDesde = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaHasta = base.MapFecha("", TipoFecha.Api);
            BandejaEntradaViewModel bandejaEntradaViewModel = new BandejaEntradaViewModel();
            bandejaEntradaViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<DocumentoBandejaDTO>();
            bandejaEntradaViewModel.NuevoDocumentoDTO = new DocumentoBandejaDTO();
            bandejaEntradaViewModel.FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Web, -30);
            bandejaEntradaViewModel.FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Web);

            iCodTrabajador = base.GetCodigoTrabajadorUsuario().Value;
            vCodPerfil = base.GetCodigoPerfilUsuario();

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos(tipobandeja, nroPagina, "", FechaDesde, FechaHasta, ConfigurationUtilities.PageSize, iCodTrabajador, vCodPerfil);
            bandejaEntradaViewModel.ResultadoBusqueda = onePageListaDocumentos;
            bandejaEntradaViewModel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametrosNuevoDocumento = await documentoController.CargarParametrosBandeja();
            ViewBag.ListaArea = parametrosNuevoDocumento.LstArea;
            ViewBag.ListaOrigen = parametrosNuevoDocumento.LstOrigen;
            ViewBag.ListaTipoDocumento = parametrosNuevoDocumento.LstTipoDocumento;
            ViewBag.ListaPrioridad = parametrosNuevoDocumento.LstPrioridad;
            ViewBag.DiasPlazoNuevoDoc = Convert.ToInt32(ConfigurationManager.AppSettings["PlazoDiasDocumento"]);

            var objTraba = new Trabajador();
            var esJefe = await Service.GetAsync<RespuestaOperacionServicio>("Trabajador/ObtenerTrabajadorID?id=" + iCodTrabajador, GetToken());
            if (esJefe.Resultado == 1)
            {
                objTraba = ConvertToObject<Trabajador>(esJefe.data);
            }
            ViewBag.EsJefe = objTraba.siEsJefe == 0? false:true;


            return View(bandejaEntradaViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Recibidos(int? page, string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {
            int iCodTrabajador = 0;
            int nroPagina = page == null ? 1 : page.Value;
            int tipobandeja = 1;
            string vCodPerfil = "";

            FechaRegistroDesde = base.MapFecha(FechaRegistroDesde, TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            FechaRegistroHasta = base.MapFecha(FechaRegistroHasta, TipoFecha.Api);
            ViewBag.FechaRegistroDesde = FechaRegistroDesde; 
            ViewBag.FechaRegistroHasta = FechaRegistroHasta; 
            
            iCodTrabajador = base.GetCodigoTrabajadorUsuario().Value;
            vCodPerfil = base.GetCodigoPerfilUsuario();

            ViewBag.FiltrosActuales = searchString;

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos(tipobandeja, nroPagina, searchString, FechaRegistroDesde, FechaRegistroHasta, ConfigurationUtilities.PageSize, iCodTrabajador, vCodPerfil);
            onePageListaDocumentos.ListaPaginado = onePageListaDocumentos.ToPageList();

            var objTraba = new Trabajador();
            var esJefe = await Service.GetAsync<RespuestaOperacionServicio>("Trabajador/ObtenerTrabajadorID?id=" + iCodTrabajador, GetToken());
            if (esJefe.Resultado == 1)
            {
                objTraba = ConvertToObject<Trabajador>(esJefe.data);
            }
            ViewBag.EsJefe = objTraba.siEsJefe == 0 ? false : true;

            return PartialView("_ListaDocumentos", onePageListaDocumentos);
        }

        
        [HttpGet]
        // GET: BandejaEntrada
        public async Task<ActionResult> ConPlazo()
        {
            int iCodTrabajador = 0;
            int nroPagina = 1;
            int tipobandeja = 2;
            string vCodPerfil = "";
            string FechaDesde = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaHasta = base.MapFecha("", TipoFecha.Api);
            BandejaEntradaViewModel bandejaEntradaViewModel = new BandejaEntradaViewModel();
            bandejaEntradaViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<DocumentoBandejaDTO>();
            bandejaEntradaViewModel.NuevoDocumentoDTO = new DocumentoBandejaDTO();
            bandejaEntradaViewModel.FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Web, -30);
            bandejaEntradaViewModel.FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Web);

            iCodTrabajador = base.GetCodigoTrabajadorUsuario().Value;
            vCodPerfil = base.GetCodigoPerfilUsuario();


            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos(tipobandeja, nroPagina, "", FechaDesde, FechaHasta, ConfigurationUtilities.PageSize, iCodTrabajador, vCodPerfil);
            bandejaEntradaViewModel.ResultadoBusqueda = onePageListaDocumentos;
            bandejaEntradaViewModel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametrosNuevoDocumento = await documentoController.CargarParametrosBandeja();
            ViewBag.ListaArea = parametrosNuevoDocumento.LstArea;
            ViewBag.ListaOrigen = parametrosNuevoDocumento.LstOrigen;
            ViewBag.ListaTipoDocumento = parametrosNuevoDocumento.LstTipoDocumento;
            ViewBag.ListaPrioridad = parametrosNuevoDocumento.LstPrioridad;
            ViewBag.DiasPlazoNuevoDoc = Convert.ToInt32(ConfigurationManager.AppSettings["PlazoDiasDocumento"]);

            var objTraba = new Trabajador();
            var esJefe = await Service.GetAsync<RespuestaOperacionServicio>("Trabajador/ObtenerTrabajadorID?id=" + iCodTrabajador, GetToken());
            if (esJefe.Resultado == 1)
            {
                objTraba = ConvertToObject<Trabajador>(esJefe.data);
            }
            ViewBag.EsJefe = objTraba.siEsJefe == 0 ? false : true;

            return View(bandejaEntradaViewModel);
        }

        
        [HttpPost]
        public async Task<ActionResult> ConPlazo(int? page, string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {
            int iCodTrabajador = 0;
            int nroPagina = page == null ? 1 : page.Value;
            int tipobandeja = 2;
            string vCodPerfil = "";
            FechaRegistroDesde = base.MapFecha(FechaRegistroDesde, TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            FechaRegistroHasta = base.MapFecha(FechaRegistroHasta, TipoFecha.Api);
            ViewBag.FechaRegistroDesde = FechaRegistroDesde;
            ViewBag.FechaRegistroHasta = FechaRegistroHasta;
            iCodTrabajador = base.GetCodigoTrabajadorUsuario().Value;
            vCodPerfil = base.GetCodigoPerfilUsuario();

            ViewBag.FiltrosActuales = (searchString);

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos(tipobandeja, nroPagina, searchString, FechaRegistroDesde, FechaRegistroHasta, ConfigurationUtilities.PageSize, iCodTrabajador, vCodPerfil);
            onePageListaDocumentos.ListaPaginado = onePageListaDocumentos.ToPageList();

            var objTraba = new Trabajador();
            var esJefe = await Service.GetAsync<RespuestaOperacionServicio>("Trabajador/ObtenerTrabajadorID?id=" + iCodTrabajador, GetToken());
            if (esJefe.Resultado == 1)
            {
                objTraba = ConvertToObject<Trabajador>(esJefe.data);
            }
            ViewBag.EsJefe = objTraba.siEsJefe == 0 ? false : true;


            return PartialView("_ListaDocumentos", onePageListaDocumentos);
        }

        
        public async Task<FileResult> ExportarPDF(string searchString, string FechaDesde, string FechaHasta)
        {
            if (String.IsNullOrEmpty(FechaDesde))
            {
                FechaDesde = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                FechaHasta = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
             FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Api);
             FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Api);
            }

            var ousuarioLogin = Filters.HelperSeguridad.ObtenerSessionUsuario();
            var otrabajadorLogin = ousuarioLogin.MaeTrabajador;
            int iCodTrabajador = 0;
            string vCodPerfil = "";
            if (otrabajadorLogin != null)
            {
                iCodTrabajador = otrabajadorLogin.iCodTrabajador;
                //vCodPerfil = ousuarioLogin.Perfil.vCodPerfil;
                vCodPerfil = "-";/*REQ020 - Integración SGS*/
            }
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "";
            string fileNameReport = "";
            var tipobandeja = 0;
            switch (ultruta)
            {
                case "conplazo":
                    viewReport = "PDF_ListaDocumentosConPlazo";
                    fileNameReport = ConfigurationUtilities.NombreReporteBandejaEntradaConPlazo;
                    tipobandeja = 2;
                    break;
                case "recibidos":
                    viewReport = "PDF_ListaDocumentosRecibidos";
                    fileNameReport = ConfigurationUtilities.NombreReporteBandejaEntradaRecibidos;
                    tipobandeja = 1;
                    break;
            }

            List<DocumentoBandejaDTO> lstDocumentos = (await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos(tipobandeja, 1, searchString, FechaDesde, FechaHasta, int.MaxValue, iCodTrabajador, vCodPerfil)).Lista;//ListarDocumentos(strSearch, strFecharegistraDesde, strFecharegistraHasta);
            return PDFFileResult(viewReport, lstDocumentos, fileNameReport, true);
        }

        
        public void ExportarExcel(string searchString, string FechaDesde, string FechaHasta)
        {
            if (String.IsNullOrEmpty(FechaDesde))
            {
                FechaDesde = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                FechaHasta = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Api);
                FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Api);
            }
            var ousuarioLogin = Filters.HelperSeguridad.ObtenerSessionUsuario();
            var otrabajadorLogin = ousuarioLogin.MaeTrabajador;
            int iCodTrabajador = 0;
            string vCodPerfil = "";
            var tipobandeja = 0;
            if (otrabajadorLogin != null)
            {
                iCodTrabajador = otrabajadorLogin.iCodTrabajador;
                //vCodPerfil = ousuarioLogin.Perfil.vCodPerfil;
                vCodPerfil = "-";/*REQ020 - Integración SGS*/
            }
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "";
            string fileNameReport = "";
            switch (ultruta)
            {
                case "conplazo":
                    titulo = "Lista de Documentos Bandeja de Entrada - Con Plazo";
                    fileNameReport = ConfigurationUtilities.NombreReporteBandejaEntradaConPlazo;
                    tipobandeja = 2;
                    break;
                case "recibidos":
                    titulo = "Lista de Documentos Bandeja de Entrada -Recibidos";
                    fileNameReport = ConfigurationUtilities.NombreReporteBandejaEntradaRecibidos;
                    tipobandeja = 1;
                    break;
            }
            List<DocumentoBandejaDTO> lstDocumentos = (new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos_Sync(tipobandeja, 1, searchString, FechaDesde, FechaHasta, int.MaxValue, iCodTrabajador, vCodPerfil)).Lista;
           
            GenerarExcelResponse<List<DocumentoBandejaDTO>>(fileNameReport, "BandejaEntrada", lstDocumentos, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 7;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "PRIORIDAD";
                ws.Cell(rowIndex, 2).Value = "ESTADO";
                ws.Cell(rowIndex, 3).Value = "RECIBIDO";
                ws.Cell(rowIndex, 4).Value = "NRO. TRÁMITE";
                ws.Cell(rowIndex, 5).Value = "DOCUMENTO";
                ws.Cell(rowIndex, 6).Value = "REMITENTE";
                ///ws.Cell(rowIndex, 7).Value = "DERIVADO";
                ws.Cell(rowIndex, 7).Value = "PLAZO";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.EstadoPrioridad;
                    ws.Cell(rowIndex, 2).Value = item.EstadoDescripcion;
                    ws.Cell(rowIndex, 3).Value = item.Recibido;
                    ws.Cell(rowIndex, 4).Value = item.Correlativo;
                    ws.Cell(rowIndex, 5).Value = item.Documento + "\r" + item.DocumentoAsunto;
                    ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 6).Value = item.NombreEmpresa + "\r" + item.Remitente;
                    ws.Cell(rowIndex, 6).Style.Alignment.WrapText = true;
                    //ws.Cell(rowIndex, 7).Value = item.Derivado + "\r" + item.Jefe;
                    //ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 7).Value = item.Plazo;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 10;
            });
        }


        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegistrarDocumento(FormCollection form)
        {
            EUsuario userSession = Filters.HelperSeguridad.ObtenerSessionUsuario();

            if (userSession == null)
                return RedirectToAction("Index", "Seguridad");
            Documento documento = new Documento();

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModel(form, documento);

            documento.EstadoDocumento = 1;
            bool contenido = validarContenido(Request.Files);

            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                documento.LstDocAdjunto = new List<DocAdjunto>();
                documento.EstadoDocumento = 2;
                documento.FechaDerivacion = DateTime.Now;

                var memorystream = new MemoryStream();
                //var fileExtension = file?.FileName.Split('.');
                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;

                documento.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String });
            }

            //anexos
            if (Request.Files.Count > 0 && contenido)
            {
                documento.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    var memorystream = new MemoryStream();
                    Request.Files[i]?.InputStream.CopyTo(memorystream);
                    byte[] imageBytes = memorystream.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    documento.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });

                }
            }

            documento.CodArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodArea : 1;
            documento.CodTrabajador = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodTrabajador : 1;
            documento.CodRepresentante = documento.RemitidoPor;
            documento.DescripcionArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.DescripcionArea : "";
            //documento.FechaPlazo = //viene desde el form;
            //documento.Origen = //viene desde el form;
            documento.FechaRegistro = DateTime.Now;

            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            Service service = new Service();

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(documento, "Documento/registrarBandejaEntrada", token);

            return Json(respuesta);
        }

        
        [HttpPost]
        public async Task<ActionResult> FinalizarDocumentoBandejaEntrada(FormCollection form)
        {
            ComentarioDocumentoModel finalizar = new ComentarioDocumentoModel();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModelV2(form, finalizar);


            //anexos
            if (Request.Files.Count > 0)
            {
                finalizar.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files[i].ContentLength > 0)
                    {
                        var memorystream = new MemoryStream();
                        Request.Files[i]?.InputStream.CopyTo(memorystream);
                        byte[] imageBytes = memorystream.ToArray();
                        string base64String = Convert.ToBase64String(imageBytes);
                        imageBytes = null;
                        finalizar.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });
                    }
                }
            }
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(finalizar, "Documento/FinalizarDocumentoBandejaEntrada", GetToken());
            return Json(respuesta);
        }

        
        [HttpPost]
        public async Task<ActionResult> ValidarFinalizarDocumento(FormCollection form)
        {
            ComentarioDocumentoModel finalizar = new ComentarioDocumentoModel();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModelV2(form, finalizar);
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(finalizar, "Documento/validarfinalizar", GetToken());
            return Json(respuesta);
        }

        
        [HttpPost]
        public async Task<ActionResult> RegistrarAvance(FormCollection form)
        {
            RegistrarAvance registrarAvance = new RegistrarAvance();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Dictionary<string, string> reemplazar = new Dictionary<string, string>();
            reemplazar.Add("_RA", String.Empty);
            Helpers.ConvertToModelV3(form, registrarAvance, reemplazar);


            //anexos
            if (Request.Files.Count > 0)
            {
                registrarAvance.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files[i].ContentLength > 0)
                    {
                        var memorystream = new MemoryStream();
                        Request.Files[i]?.InputStream.CopyTo(memorystream);
                        byte[] imageBytes = memorystream.ToArray();
                        string base64String = Convert.ToBase64String(imageBytes);
                        imageBytes = null;
                        registrarAvance.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });
                    }
                }
            }

            registrarAvance.Origen = Convert.ToInt16(TipoOrigen.INTERNO);
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(registrarAvance, "Documento/registraravance", GetToken());
            return Json(respuesta);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditarDocumento(FormCollection form)
        {
            EUsuario userSession = Filters.HelperSeguridad.ObtenerSessionUsuario();

            if (userSession == null)
                return RedirectToAction("Index", "Seguridad");

            Documento documento = new Documento();

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();

            Helpers.ConvertToModelV2(form, documento);

            documento.EstadoDocumento = 1;

            bool contenido = validarContenido(Request.Files);
            var memorystream = new MemoryStream();
            //adjunto
            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                documento.LstDocAdjunto = new List<DocAdjunto>();
                documento.EstadoDocumento = 2;
                documento.FechaDerivacion = DateTime.Now;

                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;
                documento.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String });

            }
            //anexos
            if (Request.Files.Count > 0 && contenido)
            {
                documento.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    Request.Files[i]?.InputStream.CopyTo(memorystream);
                    byte[] imageBytes = memorystream.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    documento.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });

                }
            }

            documento.CodArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodArea : 1;
            documento.CodTrabajador = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodTrabajador : 1;
            documento.CodRepresentante = documento.RemitidoPor;
            documento.DescripcionArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.DescripcionArea : "";

            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            Service service = new Service();

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(documento, "Documento/ActualizarDocumentoInterno", token);

            return Json(respuesta);
        }

        private bool validarContenido(HttpFileCollectionBase request)
        {
            bool validar = false;
            // 1 paraa no validar el adjunto, solo anexos
            for (int i = 1; i < request.Count; i++)
            {
                if (request[i].ContentLength > 0)
                {
                    validar = true;
                }
            }
            return validar;
        }

        
        [HttpGet]
        public ActionResult DescargarArchivoLaserfiche(string id)
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/DescargarArchivoLaserfiche/" + id, strRptToken);
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento2/DescargarArchivoLaserfiche/") + id, strRptToken);

            if (rptServ.Resultado == 1)
            {
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + Guid.NewGuid() + "\"");
                //Response.AddHeader("Content-Length", response..ContentLength.ToString());
                Response.BinaryWrite((byte[])rptServ.data);
                Response.Flush();
                Response.Close();
                //Response.End();
                return null;
            }
            else
            {
                return Content(null);
            }


        }

        private T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
            return result;
        }

    }
}