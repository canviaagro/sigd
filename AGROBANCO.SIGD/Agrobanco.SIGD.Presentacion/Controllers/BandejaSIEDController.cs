﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Comun.SFTP;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static Agrobanco.SIGD.Comun.Enumerados;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class BandejaSIEDController : BaseController
    {
        List<documentoSeg> listaDocumentosAnexos = new List<documentoSeg>();
        string rutaEnvioSiedLogica = "";
        string userSIED = "";
        string claveSIED = "";
        // GET: BandejaSIED
        public ActionResult Index(int? page)
        {            
            return RedirectToAction("EnvioSIEDFONAFE", "BandejaSIED");
        }


        [HttpGet]
        // GET: BandejaEntrada
        public async Task<ActionResult> EnvioSIEDFONAFE()
        {
            var usuarioSIED = ObtenerUsuarioSIED();
            var usuarioActual = GetUsuario();

            if (usuarioSIED.WebUsr != usuarioActual.MaeTrabajador.WEBUSR)
            {
                return RedirectToAction("UsuarioSIEDSinPermiso", "Redirect");
            }

            int iCodTrabajador = 0;
            int nroPagina = 1;
            int tipobandeja = 1;
            string vCodPerfil = "";
            string FechaDesde = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaHasta = base.MapFecha("", TipoFecha.Api);
            BandejaEntradaViewModel bandejaEntradaViewModel = new BandejaEntradaViewModel();
            bandejaEntradaViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<DocumentoBandejaDTO>();
            bandejaEntradaViewModel.NuevoDocumentoDTO = new DocumentoBandejaDTO();
            bandejaEntradaViewModel.FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Web, -30);
            bandejaEntradaViewModel.FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Web);

            iCodTrabajador = base.GetCodigoTrabajadorUsuario().Value;
            vCodPerfil = base.GetCodigoPerfilUsuario();


            //Obtener resultado busqueda            
            //var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentos(tipobandeja, nroPagina, "", FechaDesde, FechaHasta, ConfigurationUtilities.PageSize, iCodTrabajador, vCodPerfil);
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentoseEnvioSIED( nroPagina, "", FechaDesde, FechaHasta, ConfigurationUtilities.PageSize, iCodTrabajador);
            bandejaEntradaViewModel.ResultadoBusqueda = onePageListaDocumentos;
            bandejaEntradaViewModel.ResultadoBusqueda.ListaPaginado = onePageListaDocumentos.ToPageList();

            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();

            var parametrosNuevoDocumento = await documentoController.CargarParametrosBandejaSIED();
            ViewBag.ListaArea = parametrosNuevoDocumento.LstArea;
            ViewBag.ListaOrigen = parametrosNuevoDocumento.LstOrigen;

            ViewBag.ListaTipoDocumentoExterno = parametrosNuevoDocumento.LstTipoDocumentoExterno;
            ViewBag.ListaTipoDocumentoInterno = parametrosNuevoDocumento.LstTipoDocumentoInterno;
            ViewBag.ListaTipoDocumento = "";
            ViewBag.ListaPrioridad = parametrosNuevoDocumento.LstPrioridad;
            ViewBag.DiasPlazoNuevoDoc = Convert.ToInt32(ConfigurationManager.AppSettings["PlazoDiasDocumento"]);

            return View(bandejaEntradaViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> EnvioSIEDFONAFE(int? page, string searchString, string FechaRegistroDesde, string FechaRegistroHasta)
        {
            int iCodTrabajador = 0;
            int nroPagina = page == null ? 1 : page.Value;
            string vCodPerfil = "";

            FechaRegistroDesde = base.MapFecha(FechaRegistroDesde, TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            FechaRegistroHasta = base.MapFecha(FechaRegistroHasta, TipoFecha.Api);
            ViewBag.FechaRegistroDesde = FechaRegistroDesde;
            ViewBag.FechaRegistroHasta = FechaRegistroHasta;

            iCodTrabajador = base.GetCodigoTrabajadorUsuario().Value;
            vCodPerfil = base.GetCodigoPerfilUsuario();

            ViewBag.FiltrosActuales = searchString;

            //Obtener resultado busqueda            
            var onePageListaDocumentos = await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentoseEnvioSIED( nroPagina, searchString, FechaRegistroDesde, FechaRegistroHasta, ConfigurationUtilities.PageSize, iCodTrabajador);
            onePageListaDocumentos.ListaPaginado = onePageListaDocumentos.ToPageList();

            return PartialView("_ListaDocumentos", onePageListaDocumentos);
        }


        [HttpGet]
        public JsonResult ObteneEmpresasFONAFE()
        {
            Service _service = new Service();

            AccesoSIED(out userSIED,out  claveSIED);
            GenerarLog g = new GenerarLog();
            g.GenerarArchivoLog("ACCESO SIED "+ userSIED + "/"+claveSIED);
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternalCredentials<RespuestaOperacionServicio>(ApplicationKeys.serviceSIEDListarEmpresas, userSIED, claveSIED);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EnviarSIED(int codDocumento)
        {
            AccesoSIED(out userSIED, out claveSIED);
            RespuestaOperacionServicio rptServ;
            Service _service = new Service();
            var doc = ObtenerDocumentoSIED(codDocumento);
            SubirArchivosLaserFicheSFTP(codDocumento,doc.numeroDocumento);

            var repLegal = ObtenerRepresentanteLegal();
            
            if(repLegal.Estado == Convert.ToInt16(Enumerados.Estado.Inactivo).ToString())
            {
                rptServ = new RespuestaOperacionServicio();
                rptServ.codigo = "-1";
                rptServ.mensaje = "No puede enviar este documento porque no tiene habilitado un representante legal, por favor regularizar en el maestro de Representate Legal";
                return Json(rptServ, JsonRequestBehavior.AllowGet); ;
            }

            var dataListaFirmantes = ListarFirmantes();
            var Remitente = dataListaFirmantes.listaFirmante.Find(x => x.dniPersona == repLegal.NroDoc);
            remitente rem = new remitente();

            if (Remitente == null)
            {
                rptServ = new RespuestaOperacionServicio();
                rptServ.codigo = "-1";
                rptServ.mensaje = "El DNI configurado en el maestro de Representante Legal, no esta autorizado en el servicio ListarFirmando de FONAFE.";
                return Json(rptServ, JsonRequestBehavior.AllowGet); ;
            }
            else
            {
                rem.codCargo = Remitente.codCargoPersona;
                rem.dni = Remitente.dniPersona;
            }

            var EmpresasSIED = GetDocumentosDerivadosPorCodDocumento(codDocumento, Convert.ToInt16(Enumerados.EnumTipoAcceso.EmpresaSIED));

            List<EmpresaDestino> listaEmpresaDestino = new List<EmpresaDestino>();
            EmpresaDestino empDestino;
            foreach (var emp in EmpresasSIED)
            {
                empDestino = new EmpresaDestino();
                empDestino.nombreDestinatario = Remitente.nomPersona;
                empDestino.nombreCargo = Remitente.cargoPersona;
                empDestino.unidadOrganica = doc.origen == Convert.ToInt16(Enumerados.TipoOrigen.EXTERNOSIED) ? Remitente.unidadOrganica : "";
                empDestino.nombreEntidad = emp.NombreEntidadSIED;
                empDestino.ruc = emp.rucSied;
                empDestino.original = Convert.ToInt32(emp.FormatoDocSied);
                listaEmpresaDestino.Add(empDestino);
            }
           
            EnvioSIED objEnvioSIED = new EnvioSIED();
            objEnvioSIED.dniFirmante = repLegal.NroDoc;
            objEnvioSIED.numeroDocumento = doc.numeroDocumento;
            objEnvioSIED.numSecuencial = codDocumento;
            objEnvioSIED.asunto = doc.asunto;
            objEnvioSIED.categoriaAsunto = doc.categoriaAsunto;
            objEnvioSIED.codSTD = codDocumento;
            objEnvioSIED.tipoDocumento = doc.tipoDocumento;
            objEnvioSIED.esExterno = doc.origen == Convert.ToInt16(Enumerados.TipoOrigen.EXTERNOSIED) ? 1 : 0;
            objEnvioSIED.nroFolio = doc.origen == Convert.ToInt16(Enumerados.TipoOrigen.EXTERNOSIED) ? codDocumento : 0; ;
            objEnvioSIED.codigoDocumentoSIEDRef = codDocumento;

            objEnvioSIED.remitente = rem;
            objEnvioSIED.listaEmpresaDestino = listaEmpresaDestino;
            objEnvioSIED.urlDocumento = rutaEnvioSiedLogica;
            objEnvioSIED.nomDocPrincipal = doc.numeroDocumento+".pdf";
            objEnvioSIED.listaDocumentosAnexos = listaDocumentosAnexos;


            rptServ = _service.HttpPostJsonExternalCredentials<RespuestaOperacionServicio>(ApplicationKeys.serviceSIEDRegistrarDocumento, objEnvioSIED, userSIED, claveSIED);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }

        public RespuestaOperacionServicio ListarFirmantes()
        {
            AccesoSIED(out userSIED, out claveSIED);
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternalCredentials<RespuestaOperacionServicio>(ApplicationKeys.serviceSIEDListarFirmante+""+ ApplicationKeys.RUCAGROBANCO, userSIED, claveSIED);
            return rptServ;
        }


        public List<Entidades.Entidades.DocDerivacion> GetDocumentosDerivadosPorCodDocumento(int CodDocumento, int TipoAcceso)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/GetDocumentosDerivadosPorCodDocumento?CodDocumento=") + CodDocumento + ("&TipoAcceso=" + TipoAcceso), strRptToken);

            return rptServ.lstDocDerivacion;
        }

        public RespuestaOperacionServicio ObteneEmpresasFONAFERO()
        {
            AccesoSIED(out userSIED, out claveSIED);
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternalCredentials<RespuestaOperacionServicio>(ApplicationKeys.serviceSIEDListarEmpresas, userSIED, claveSIED);
            return rptServ;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegistrarDocumentoSIED(FormCollection form)
        {
            EUsuario userSession = Filters.HelperSeguridad.ObtenerSessionUsuario();

            if (userSession == null)
                return RedirectToAction("Index", "Seguridad");
            Documento documento = new Documento();
            documento.lstEmpresaSied = new List<EmpresasSIED>();
            EmpresasSIED empSied = new EmpresasSIED();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Helpers.ConvertToModel(form, documento);


            documento.EstadoDocumento = 1;

            var dataEmpresas = ObteneEmpresasFONAFERO();
            var empresas = documento.CodAreaBE.Split('|');
            var formatoDocEmpresas = documento.CodFormatoDocumentoEmpresas.Split('|');
            var contador = 1;
            if (dataEmpresas.codigo == "0")
            foreach (string valor in empresas) {
            for(int x = 0; x < dataEmpresas.listaEmpresa.Count; x++)
            {
             if(valor == dataEmpresas.listaEmpresa[x].codEmpresa.ToString())
                {
                            empSied = new EmpresasSIED();
                            empSied.codEmpresa = Convert.ToInt32(valor);
                            empSied.nomEmpresaCorto = dataEmpresas.listaEmpresa[x].nomEmpresaCorto;
                            empSied.rucEmpresa = dataEmpresas.listaEmpresa[x].rucEmpresa;
                            empSied.nombreEmpresa = dataEmpresas.listaEmpresa[x].nombreEmpresa;
                            empSied.CodFormatoDocumentoEmpresas = formatoDocEmpresas[contador];

                            documento.lstEmpresaSied.Add(empSied);
                            contador++;
                            break;
                  }
             }
             }


            bool contenido = validarContenido(Request.Files);

            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                documento.LstDocAdjunto = new List<DocAdjunto>();
                documento.EstadoDocumento = 2;
                documento.FechaDerivacion = DateTime.Now;

                var memorystream = new MemoryStream();
                //var fileExtension = file?.FileName.Split('.');
                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;

                documento.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String });
            }

            //anexos
            if (Request.Files.Count > 0 && contenido)
            {
                documento.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    var memorystream = new MemoryStream();
                    Request.Files[i]?.InputStream.CopyTo(memorystream);
                    byte[] imageBytes = memorystream.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    documento.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });

                }
            }

            documento.CodArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodArea : 1;
            documento.CodTrabajador = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodTrabajador : 1;
            documento.CodRepresentante = documento.RemitidoPor;
            documento.DescripcionArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.DescripcionArea : "";
            //documento.FechaPlazo = //viene desde el form;
            //documento.Origen = //viene desde el form;
            documento.FechaRegistro = DateTime.Now;

            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            Service service = new Service();

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(documento, "Documento/registrarBandejaEnvioSIED", token);

            return Json(respuesta);
        }

        [HttpGet]
        public ActionResult VerDocumento(string codDocumento)
        {
            return View("_VerDocumento");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditarDocumentoSIED(FormCollection form)
        {
            EUsuario userSession = Filters.HelperSeguridad.ObtenerSessionUsuario();

            if (userSession == null)
                return RedirectToAction("Index", "Seguridad");
            Documento documento = new Documento();
            documento.lstEmpresaSied = new List<EmpresasSIED>();
            EmpresasSIED empSied = new EmpresasSIED();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();

            Helpers.ConvertToModelV2(form, documento);

            documento.EstadoDocumento = 1;


            var dataEmpresas = ObteneEmpresasFONAFERO();
            var empresas = documento.CodAreaBE.Split('|');
            var formatoDocEmpresas = documento.CodFormatoDocumentoEmpresas.Split('|');
            var contador = (formatoDocEmpresas.Length == 1? 0:1);
            if (dataEmpresas.codigo == "0")
                foreach (string valor in empresas)
                {
                    for (int x = 0; x < dataEmpresas.listaEmpresa.Count; x++)
                    {
                        if (valor == dataEmpresas.listaEmpresa[x].codEmpresa.ToString())
                        {
                            empSied = new EmpresasSIED();
                            empSied.codEmpresa = Convert.ToInt32(valor);
                            empSied.nomEmpresaCorto = dataEmpresas.listaEmpresa[x].nomEmpresaCorto;
                            empSied.rucEmpresa = dataEmpresas.listaEmpresa[x].rucEmpresa;
                            empSied.nombreEmpresa = dataEmpresas.listaEmpresa[x].nombreEmpresa;
                            empSied.CodFormatoDocumentoEmpresas = formatoDocEmpresas[contador];

                            documento.lstEmpresaSied.Add(empSied);
                            contador++;
                            break;
                        }
                    }
                }



            bool contenido = validarContenido(Request.Files);
            var memorystream = new MemoryStream();
            //adjunto
            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                documento.LstDocAdjunto = new List<DocAdjunto>();
                documento.EstadoDocumento = 2;
                documento.FechaDerivacion = DateTime.Now;

                Request.Files[0]?.InputStream.CopyTo(memorystream);
                byte[] imageBytes = memorystream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                imageBytes = null;
                documento.LstDocAdjunto.Add(new DocAdjunto { FileArray = base64String });

            }
            //anexos
            if (Request.Files.Count > 0 && contenido)
            {
                documento.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    Request.Files[i]?.InputStream.CopyTo(memorystream);
                    byte[] imageBytes = memorystream.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    imageBytes = null;
                    documento.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = Request.Files[i].FileName });

                }
            }

            documento.CodArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodArea : 1;
            documento.CodTrabajador = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.iCodTrabajador : 1;
            documento.CodRepresentante = documento.RemitidoPor;
            documento.DescripcionArea = userSession.MaeTrabajador != null ? userSession.MaeTrabajador.DescripcionArea : "";

            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);

            Service service = new Service();

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(documento, "Documento/ActualizarDocumentoEnvioSIED", token);

            return Json(respuesta);
        }

        [HttpGet]
        public ActionResult ObtenerDocumentoSIED(string codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
             RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoSIED?CodDocumento=") + codDocumento, strRptToken);
            ViewBag.OrigenEdit = rptServ.data;
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ObtenerTotalDerivaciones(string codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();

            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerTotalDerivaciones/") + codDocumento, strRptToken);

            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }


        public async Task<FileResult> ExportarPDF(string searchString, string FechaDesde, string FechaHasta)
        {
            if (String.IsNullOrEmpty(FechaDesde))
            {
                FechaDesde = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                FechaHasta = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Api);
                FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Api);
            }

            var ousuarioLogin = Filters.HelperSeguridad.ObtenerSessionUsuario();
            var otrabajadorLogin = ousuarioLogin.MaeTrabajador;
            int iCodTrabajador = 0;
            string vCodPerfil = "";
            if (otrabajadorLogin != null)
            {
                iCodTrabajador = otrabajadorLogin.iCodTrabajador;
                //vCodPerfil = ousuarioLogin.Perfil.vCodPerfil;
                vCodPerfil = "-";/*REQ020 - Integración SGS*/
            }
           
            string viewReport = "";
            string fileNameReport = "";
            viewReport = "PDF_ListaDocumentosEnvioSIED";
            fileNameReport = "Documentos SIED FONAFE";

            List<DocumentoBandejaDTO> lstDocumentos = (await new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentoseEnvioSIED(1, searchString, FechaDesde, FechaHasta, int.MaxValue, iCodTrabajador)).Lista;
        
            return PDFFileResult(viewReport, lstDocumentos, fileNameReport, true);
        }


        public void ExportarExcel(string searchString, string FechaDesde, string FechaHasta)
        {
            if (String.IsNullOrEmpty(FechaDesde))
            {
                FechaDesde = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                FechaHasta = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                FechaDesde = base.MapFecha(FechaDesde, TipoFecha.Api);
                FechaHasta = base.MapFecha(FechaHasta, TipoFecha.Api);
            }
            var ousuarioLogin = Filters.HelperSeguridad.ObtenerSessionUsuario();
            var otrabajadorLogin = ousuarioLogin.MaeTrabajador;
            int iCodTrabajador = 0;
            string vCodPerfil = "";
            var tipobandeja = 0;
            if (otrabajadorLogin != null)
            {
                iCodTrabajador = otrabajadorLogin.iCodTrabajador;
                //vCodPerfil = ousuarioLogin.Perfil.vCodPerfil;
                vCodPerfil = "-";/*REQ020 - Integración SGS*/
            }
            string viewReport = "";
            string fileNameReport = "";
            viewReport = "PDF_ListaDocumentosEnvioSIED";
            fileNameReport = "Documentos Envío SIED FONAFE";

            List<DocumentoBandejaDTO> lstDocumentos = new ProxyApi(base.GetToken()).ProxyListarBandejaDocumentoseEnvioSIED_Sync(1, searchString, FechaDesde, FechaHasta, int.MaxValue, iCodTrabajador).Lista;

            GenerarExcelResponse<List<DocumentoBandejaDTO>>(fileNameReport, "BandejaEntrada", lstDocumentos, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 8;
                ws.Cell(rowIndex, 1).Value = fileNameReport;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                rowIndex++;
                rowIndex++;
                ws.Cell(rowIndex, 1).Value = "PRIORIDAD";
                ws.Cell(rowIndex, 2).Value = "ESTADO";
                ws.Cell(rowIndex, 3).Value = "RESPUESTA SIED";
                ws.Cell(rowIndex, 4).Value = "NRO. TRÁMITE";
                ws.Cell(rowIndex, 5).Value = "DOCUMENTO";
                ws.Cell(rowIndex, 6).Value = "REMITENTE";
                ws.Cell(rowIndex, 7).Value = "DIRIGIDO A";
                ws.Cell(rowIndex, 8).Value = "PLAZO";
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                var comisiones = new Dictionary<int, List<decimal>>();

                foreach (var item in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = item.EstadoPrioridad;
                    ws.Cell(rowIndex, 2).Value = item.EstadoDescripcion;
                    ws.Cell(rowIndex, 3).Value = (item.CodDocumentoRespuestaSIED > 0) ? (item.CodRespuestaSIED + "\r" + item.CodDocumentoRespuestaSIED) : "";
                    ws.Cell(rowIndex, 3).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 4).Value = item.Correlativo;
                    ws.Cell(rowIndex, 5).Value = item.Documento + "\r" + item.DocumentoAsunto;
                    ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 6).Value = item.NombreEmpresa + "\r" + item.Remitente;
                    ws.Cell(rowIndex, 6).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 7).Value = item.EmpresasSied;
                    ws.Cell(rowIndex, 8).Value = item.Plazo;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);
                }
                //ws.Columns().AdjustToContents();
                ws.Columns().AdjustToContents();
                ws.Columns("C").Width = 10;
            });
        }



        private bool validarContenido(HttpFileCollectionBase request)
        {
            bool validar = false;
            // 1 paraa no validar el adjunto, solo anexos
            for (int i = 1; i < request.Count; i++)
            {
                if (request[i].ContentLength > 0)
                {
                    validar = true;
                }
            }
            return validar;
        }

        private UsuarioSIED ObtenerUsuarioSIED()
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/UsuarioSIED/ObtenerUsuarioSIED", GetToken());            

            var item = Helpers.ConvertToObject<UsuarioSIED>(respuesta.data);
            if (item == null)
            {
                item = new UsuarioSIED();
            }
            return item;
        }


        private EnvioSIED ObtenerDocumentoSIED(int codDocumento)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/documento/ObtenerDocumentoSIED_SeviceEnvio?CodDocumento="+codDocumento, GetToken());

            var item = Helpers.ConvertToObject<EnvioSIED>(respuesta.data);
            if (item == null)
            {
                item = new EnvioSIED();
            }
            return item;
        }


        private RepresentanteLegal ObtenerRepresentanteLegal()
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/RepresentanteLegal/ObtenerRepresentanteLegal", GetToken());

            DocumentoController documentoController = new DocumentoController();
         
            var item = Helpers.ConvertToObject<RepresentanteLegal>(respuesta.data);
            if (item == null)
            {
                item = new RepresentanteLegal();
            }
            return item;
        }

        [HttpPost]
        public async Task<ActionResult> AnularDerivacion(DerivacionModel derivacion)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            //DerivacionModel derivacion = new DerivacionModel();

            //derivacion.CodDocumento = Convert.ToInt32(codDocumento);
            //derivacion.CodDocumentoDerivacion = Convert.ToInt32(codDerivacion);
            //derivacion.Comentarios = comentario;

            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(derivacion, "Documento/AnularDerivacion", GetToken());

            return Json(respuesta);
        }

        [HttpPost]
        public async Task<ActionResult> FinalizarDocSIED(Documento objDocumento)
        {
            Service service = new Service();
            RespuestaOperacionServicio rptServ = await Service.PostAsync<RespuestaOperacionServicio>(objDocumento, "Documento/FinalizarDocSIED", GetToken());
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }


        public Stream GetDocumentoLaserFiche(int codLaserfiche)
        {
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJsonExternal<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("documento/ObtenerDocumentoLaserfiche/") + codLaserfiche);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

            if (rptServ.Resultado == 1)
            {
                rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptServ.data);
            }

            rptServ.data = rptArchivos;

            byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);


            return ms2;
        }

        public void SubirArchivosLaserFicheSFTP(int codDocumento, string correlativo)
        {
            listaDocumentosAnexos = new List<documentoSeg>();
            documentoSeg objDocSeg;

            var codDocPrincipal = GetCodLaserficheDocumento(codDocumento);
            var ListCodLF = GetCodLaserficheAnexosDocumento(codDocumento);
            
            var stream = GetDocumentoLaserFiche(codDocPrincipal);
            rutaEnvioSiedLogica = ApplicationKeys.rutaSFTPSubidaHome + ApplicationKeys.rutaSFTPSubidaEmpresas + "/" + ApplicationKeys.RUCAGROBANCO + "/" + DateTime.Now.Year.ToString() + ApplicationKeys.rutaSFTPCodSTD;
            var rutaEnvioSiedFisica = ApplicationKeys.rutaSFTPSubidaHome+ "/" + DateTime.Now.Year.ToString() + ApplicationKeys.rutaSFTPCodSTD;

            SFTP.UploadSFTPFileFromStream(correlativo+".pdf", rutaEnvioSiedFisica, stream);

            var cont = 0;
            foreach(var anx in ListCodLF)
            {
                cont++;
                var streamAnx = GetDocumentoLaserFiche(anx);
                var nombreAnx = correlativo + "_ANX" + cont + ".pdf";
                SFTP.UploadSFTPFileFromStream(nombreAnx, rutaEnvioSiedFisica, streamAnx);
                objDocSeg = new documentoSeg();

                objDocSeg.nomDocumento = nombreAnx;
                objDocSeg.desDocumento = "";
                listaDocumentosAnexos.Add(objDocSeg);

            }



        }

        public int GetCodLaserficheDocumento(int codDocumento)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/documento/GetCodLaserFIchePorCodDocumento?CodDocumento=" + codDocumento, GetToken());

            var item = Convert.ToInt16(respuesta.data);
            
            return item;
        }

        public List<int> GetCodLaserficheAnexosDocumento(int codDocumento)
        {
            Service _service = new Service();
            RespuestaOperacionServicio respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(
            ConfigurationUtilities.WebApiUrl + "/documento/GetCodLaserFIcheAnexosPorCodDocumento?CodDocumento=" + codDocumento, GetToken());

            return respuesta.listEnteros;
        }


        [HttpPost]
        public async Task<ActionResult> AdjuntarHojaDeCargo(FormCollection form)
        {
            RegistroCargoDTO registroCargo = new RegistroCargoDTO();
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();

            Helpers.ConvertToModelV2(form, registroCargo);


            //anexos
            if (Request.Files.Count > 0)
            {
                registroCargo.LstDocAnexo = new List<DocAnexo>();
                // 1 , solo tomamos anexos no adjunto
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    if (Request.Files[i].ContentLength > 0)
                    {
                        var memorystream = new MemoryStream();
                        Request.Files[i]?.InputStream.CopyTo(memorystream);
                        byte[] imageBytes = memorystream.ToArray();
                        string base64String = Convert.ToBase64String(imageBytes);
                        imageBytes = null;
                        registroCargo.LstDocAnexo.Add(new DocAnexo { FileArray = base64String, NombreDocumento = registroCargo.DocumentoCorrelativo+"_CARGO1.pdf" });
                    }
                }
            }

            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(registroCargo, "Documento/registrarcargoSIED", GetToken());
            return Json(respuesta);
        }

        [HttpGet]
        public ActionResult GetDocumentosAnexosNoLF(int codDocumento)
        {
            var lstAnexos = new ProxyApi(base.GetToken()).ProxyGetDocumentoAnexos(codDocumento);
            var listFiltro = lstAnexos.Lista.FindAll(x => x.TipoArchivo == Convert.ToInt16(Enumerados.TipoArchivos.Cargo));
            return Json(listFiltro, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportarPDF_CargoRecepcionSIED(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = new RespuestaOperacionServicio();

            var lstAnexos = new ProxyApi(base.GetToken()).ProxyGetDocumentoAnexos(codDocumento);
            var anexoCargo = lstAnexos.Lista.FirstOrDefault(a => a.TipoArchivo == TipoArchivos.Cargo.ToInt());


            RespuestaOperacionServicio rptCargo = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=") + anexoCargo.CodLaserfiche.ToString(), strRptToken);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

             if (rptCargo.Resultado == 1)
             {
                 rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptCargo.data);
             }

             rptServ.data = rptArchivos;

             byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
             MemoryStream ms2 = new MemoryStream(imageBytes, 0,
               imageBytes.Length, true, true);
            
            byte[] docExportacion = ms2.GetBuffer();

            return File(docExportacion, "application/pdf", anexoCargo.NombreDocumento);
        }


        public void AccesoSIED(out string user, out string clave)
        {
            SFTPAccess con = new SFTPAccess();
            try
            {
                con.ServicesSiedAccessConnection(out  user, out clave);
            }catch(Exception e)
            {
                user = "";
                clave = "";
            }
        }


    }
}