﻿
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Models;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Utilities;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static Agrobanco.SIGD.Comun.Enumerados;
using EN = Agrobanco.SIGD.Entidades.Entidades;

namespace Agrobanco.SIGD.Presentacion.Controllers
{    
    [ErrorFilter]
    public class DocumentoController : BaseController
    {
        public string Token { get; set; }
        // GET: Documento
       
        public async Task<ActionResult> CrearDocumento()
        {
            this.Token = base.GetToken();
            await CargarParametros();
            return View();
        }
       
        [HttpPost]
        public async Task<ActionResult> AnularDocumento(ComentarioDocumentoModel anular)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(anular, "Documento/anular", GetToken());

            return Json(respuesta);
        }

        [HttpPost]
        public async Task<ActionResult> AnularDocumentoYDerivados(ComentarioDocumentoModel anular)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(anular, "Documento/AnularDocumentoYDerivados", GetToken());

            return Json(respuesta);
        }
        
        [HttpPost]
        public async Task<ActionResult> AnularDocumentoEnvioSIED(ComentarioDocumentoModel anular)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(anular, "Documento/AnularDocumentoEnvioSIED", GetToken());

            return Json(respuesta);
        }
        
        [HttpPost]
        public async Task<ActionResult> AnularDocumentoRecepcionSIED(ComentarioDocumentoModel anular)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(anular, "Documento/AnularDocumentoRecepcionSIED", GetToken());

            return Json(respuesta);
        }

        [HttpPost]
        public async Task<ActionResult> RegistrarTipoDocumento(TipoDocumento tipoDocumento)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(tipoDocumento, "Documento/RegistrarTipoDocumento", GetToken());

            return Json(respuesta);
        }
       
        [HttpPost]
        public async Task<ActionResult> ActualizarTipoDocumento(TipoDocumento tipoDocumento)
        {

            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(tipoDocumento, "Documento/ActualizarTipoDocumento", GetToken());

            return Json(respuesta);
        }
       
        [HttpPost]
        public async Task<ActionResult> DerivarDocumento(DerivarDocumentoModel derivar)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(derivar, "Documento/derivar", GetToken());

            return Json(respuesta);
        }
       
        [HttpPost]
        public async Task<ActionResult> DevolverDocumento(ComentarioDocumentoModel comentario)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(comentario, "Documento/devolver", GetToken());

            return Json(respuesta);
        }
       
        [HttpPost]
        public async Task<ActionResult> DevolverDocumentoInterno(ComentarioDocumentoModel comentario)
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            Service service = new Service();
            respuesta = await Service.PostAsync<RespuestaOperacionServicio>(comentario, "Documento/devolverInterno", GetToken());

            return Json(respuesta);
        }
       
        public async Task CargarParametros()
        {
            ParametrosMesaPartesDTO parametrosMesaPartes = await CargarParametrosBandeja();
            //ParametrosMesaPartesDTO parametrosMesaPartes = new ParametrosMesaPartesDTO();
            //var task = CargarParametrosMesaPartes();
            //Task.WaitAll(task);
            //ParametrosMesaPartesDTO parametrosMesaPartes = task.Result;

            ViewBag.ListaArea = parametrosMesaPartes.LstArea;
            ViewBag.ListaOrigen = parametrosMesaPartes.LstOrigen;
            ViewBag.ListaTipoDocumento = parametrosMesaPartes.LstTipoDocumento;
            ViewBag.ListaPrioridad = parametrosMesaPartes.LstPrioridad;
            ViewBag.DiasPlazoNuevoDoc = Convert.ToInt32(ConfigurationManager.AppSettings["PlazoDiasDocumento"]);

        }
       
        public async Task<ParametrosMesaPartesDTO> CargarParametrosBandeja()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosMesaPartesDTO parametros = new ParametrosMesaPartesDTO();

            respuesta.data = new ParametrosMesaPartesDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosMP", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosMesaPartesDTO>(respuesta.data);
            }

            return parametros;
        }

        public async Task<ParametrosMesaPartesDTO> CargarParametrosBandejaSIED()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosMesaPartesDTO parametros = new ParametrosMesaPartesDTO();

            respuesta.data = new ParametrosMesaPartesDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosSIED", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosMesaPartesDTO>(respuesta.data);
            }

            return parametros;
        }

        [HttpGet]
        public async Task<ActionResult> CargarParametrosComunAjax()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosComun", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet);
        }
       
        [HttpGet]
        public async Task<ActionResult> CargarParametrosListaTipoDocumentoAjax()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosMesaPartesDTO parametros = new ParametrosMesaPartesDTO();

            respuesta.data = new ParametrosMesaPartesDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosListaTipoDocumento", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosMesaPartesDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }
       
        [HttpGet]
        public async Task<ActionResult> CargarParametrosComunEstadoDocumentoAjax()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosComuEstadoDocumento", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }
        

       [HttpGet]
        public async Task<ActionResult> CargarParametrosComunOrigenNoSIEDAjax()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosComunOrigenNoSiIED", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public async Task<ActionResult> CargarParametrosComunOrigenAjax()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosComunOrigen", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }


        [HttpGet]
        public async Task<ActionResult> CargarParametrosAsuntosSIED()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/CargarParametrosAsuntosSIED", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public async Task<ActionResult> CargarParametrosMotivoFirma()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosMotivoFirma", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public async Task<ActionResult> CargarParametrosFormatoDocumento()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosFormatoDocumento", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public async Task<ActionResult> CargarParametrosComunAccionAjax()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosComunAccion", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet); ;
        }

       
        public async Task<ParametrosComunDTO> CargarParametrosComun()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosComun", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return parametros;
        }

       
        public async Task<ParametrosRepresentanteDTO> CargarParametrosRepresentante()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosRepresentanteDTO parametros = new ParametrosRepresentanteDTO();

            respuesta.data = new ParametrosMesaPartesDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosRepresentante", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosRepresentanteDTO>(respuesta.data);
            }

            return parametros;
        }

       
        public async Task<ParametrosFeriadoDTO> CargarParametrosFeriado()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosFeriadoDTO parametros = new ParametrosFeriadoDTO();

            respuesta.data = new ParametrosMesaPartesDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosFeriado", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosFeriadoDTO>(respuesta.data);
            }

            return parametros;
        }

       
        public ParametrosFeriadoDTO CargarParametrosFeriado_Sync()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosFeriadoDTO parametros = new ParametrosFeriadoDTO();

            respuesta.data = new ParametrosMesaPartesDTO();
            respuesta = new Service().HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/parametro/GetParametrosFeriado"), this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosFeriadoDTO>(respuesta.data);
            }

            return parametros;
        }

        private T ConvertToObject<T>(object jsonObject)
        {
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject);
            T result = default(T);

            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);

            return result;
        }

        
        public async Task<FileResult> ExportarPDF_CargoRecepcion(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptaRegistroCargo = new RespuestaOperacionServicio();            

            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            string correlativo = _documento.Correlativo;
            string nombreFile = string.Concat(correlativo, "_", "CARGO1", ".pdf");

            var lstAnexos = new ProxyApi(base.GetToken()).ProxyGetDocumentoAnexos(codDocumento);
            var anexoCargo = lstAnexos.Lista.FirstOrDefault(a => a.TipoArchivo == TipoArchivos.Cargo.ToInt());

            //Generar el archivo
            string strDocumento = GenerarDocumentoPDF("PDF_CargoRecepcion", _documento, "string");

            byte[] docExportacion = strDocumento.ToBytesFromBase64String();
            
            /*No hay registrado un cargo*/
            if (anexoCargo == null)
            {                
                Service service = new Service();

                RegistroCargoDTO cargo = new RegistroCargoDTO();
                DocAnexo docAnexo = new DocAnexo();

                cargo.CodDocumento = codDocumento;                
                docAnexo.CodDocumento = codDocumento;
                docAnexo.FileArray = strDocumento;
                docAnexo.NombreDocumento = nombreFile;
                cargo.LstDocAnexo = new List<DocAnexo>();
                cargo.LstDocAnexo.Add(docAnexo);

                rptaRegistroCargo = await Service.PostAsync<RespuestaOperacionServicio>(cargo, "Documento/registrarcargo", strRptToken);
            }

            if (anexoCargo != null)
            {
                RespuestaOperacionServicio rptCargo = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=") + anexoCargo.CodLaserfiche.ToString(), strRptToken);
                RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

                if (rptCargo.Resultado == 1)
                {
                    rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptCargo.data);
                }

                rptServ.data = rptArchivos;

                byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
                MemoryStream ms2 = new MemoryStream(imageBytes, 0,
                  imageBytes.Length, true, true);

                docExportacion = ms2.GetBuffer();                
            }

            return File(docExportacion, "application/pdf", nombreFile);
        }


        public async Task<FileResult> ExportarPDF_CargoRecepcionSIED(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptaRegistroCargo = new RespuestaOperacionServicio();

            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            string correlativo = _documento.Correlativo;
            string nombreFile = string.Concat(correlativo, "_", "CARGO1", ".pdf");

            var lstAnexos = new ProxyApi(base.GetToken()).ProxyGetDocumentoAnexos(codDocumento);
            var anexoCargo = lstAnexos.Lista.FirstOrDefault(a => a.TipoArchivo == TipoArchivos.Cargo.ToInt());

            //Generar el archivo
            string strDocumento = GenerarDocumentoPDF("PDF_CargoRecepcionSIED", _documento, "string");

            byte[] docExportacion = strDocumento.ToBytesFromBase64String();

            /*No hay registrado un cargo*/
            if (anexoCargo == null)
            {
                Service service = new Service();

                RegistroCargoDTO cargo = new RegistroCargoDTO();
                DocAnexo docAnexo = new DocAnexo();

                cargo.CodDocumento = codDocumento;
                docAnexo.CodDocumento = codDocumento;
                docAnexo.FileArray = strDocumento;
                docAnexo.NombreDocumento = nombreFile;
                cargo.LstDocAnexo = new List<DocAnexo>();
                cargo.LstDocAnexo.Add(docAnexo);

                rptaRegistroCargo = await Service.PostAsync<RespuestaOperacionServicio>(cargo, "Documento/registrarcargo", strRptToken);
            }

            if (anexoCargo != null)
            {
                RespuestaOperacionServicio rptCargo = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=") + anexoCargo.CodLaserfiche.ToString(), strRptToken);
                RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

                if (rptCargo.Resultado == 1)
                {
                    rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptCargo.data);
                }

                rptServ.data = rptArchivos;

                byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
                MemoryStream ms2 = new MemoryStream(imageBytes, 0,
                  imageBytes.Length, true, true);

                docExportacion = ms2.GetBuffer();
            }

            return File(docExportacion, "application/pdf", nombreFile);
        }

        public FileResult ExportarPDF_HojaResumen(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            return PDFFileResult("PDF_HojaResumen", _documento, ConfigurationUtilities.NombreReporteBandejaMesaPartes, true);
        }

        public FileResult ExportarPDF_HojaResumenSIED(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }
            _documento.Observaciones = _documento.Observaciones.Replace("<br/>", "");
            _documento.Referencia = _documento.Referencia.Replace("<br/>", "");
            return PDFFileResult("PDF_HojaResumenSIED", _documento, ConfigurationUtilities.NombreReporteBandejaMesaPartes, true);
        }


        public FileResult ExportarDocumentoLaserFicher(int CodFiche)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=") + CodFiche, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=" + CodFiche, strRptToken);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

            if (rptServ.Resultado == 1)
            {
                rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptServ.data);
            }

            rptServ.data = rptArchivos;

            //MemoryStream ms = HelperIT.Base64ToMemoryStream(rptArchivos.ListaDocumentos[0].ArchivoBase64);

            byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);


            return File(ms2.GetBuffer(), "application/pdf", string.Format(rptArchivos.ListaDocumentos[0].NombreArchivo, DateTime.UtcNow.AddHours(TimeZoneOffset).ToFormattedDate(ConfigurationUtilities.FormatoFechaNombreReporte)));
        }

        public FileResult ExportarDocumentoLaserFicherDoc(int CodDocument)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoLaserFicherDoc?CodDocument=") + CodDocument, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumentoLaserFicher?CodLaserFicher=" + CodFiche, strRptToken);
            RespuestaConsultaArchivosDTO rptArchivos = new RespuestaConsultaArchivosDTO();

            if (rptServ.Resultado == 1)
            {
                rptArchivos = Helpers.ConvertToObject<RespuestaConsultaArchivosDTO>(rptServ.data);
            }

            rptServ.data = rptArchivos;

            //MemoryStream ms = HelperIT.Base64ToMemoryStream(rptArchivos.ListaDocumentos[0].ArchivoBase64);

            byte[] imageBytes = Convert.FromBase64String(rptArchivos.ListaDocumentos[0].ArchivoBase64);
            MemoryStream ms2 = new MemoryStream(imageBytes, 0,
              imageBytes.Length, true, true);


            return File(ms2.GetBuffer(), "application/pdf", string.Format(rptArchivos.ListaDocumentos[0].NombreArchivo, DateTime.UtcNow.AddHours(TimeZoneOffset).ToFormattedDate(ConfigurationUtilities.FormatoFechaNombreReporte)));
        }



        public FileResult ExportarArchivosZIP(int codDocumento)
        {

            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=") + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>("https://localhost:44307/api/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            EN.Documento _documento = new EN.Documento();

            if (rptServ.Resultado == 1)
            {
                _documento = Helpers.ConvertToObject<EN.Documento>(rptServ.data);
            }

            MemoryStream outputMemStream = new MemoryStream();
            ZipOutputStream s = new ZipOutputStream(outputMemStream);

            s.SetLevel(9); // 0-9, 9 being the highest compression  

            foreach (var item in _documento.ArchivosLaserFiche.ListaDocumentos)
            {
                ZipEntry entry = new ZipEntry(item.NombreArchivo);
                entry.DateTime = DateTime.Now;
                entry.IsUnicodeText = true;
                s.PutNextEntry(entry);

                byte[] imageBytes = Convert.FromBase64String(item.ArchivoBase64);
                MemoryStream ms2 = new MemoryStream(imageBytes, 0,
                  imageBytes.Length, true, true);

                StreamUtils.Copy(ms2, s, new byte[4096]);
                ms2.Close();
                s.CloseEntry();
            }

            s.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
            s.Close();          // Must finish the ZipOutputStream before using outputMemStream.

            outputMemStream.Position = 0;

            return File(outputMemStream.ToArray(), "application/octet-stream", "reports.zip");
        }
        [HttpGet]
        public ActionResult ObtenerDocumentoInterno(string codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoInterno?CodDocumento=") + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/ObtenerDocumentoInterno?CodDocumento=" + codDocumento, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ObtenerDocumento(string codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumento?CodDocumento=")+ codDocumento, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ObtenerDocumentoRecepcionSIED(string codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoRecepcionSIED?CodDocumento=") + codDocumento, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public ActionResult ObtenerDocumentoDetalleDerivacion(string CodDocumentoDer)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/ObtenerDocumento?CodDocumento=" + codDocumento, strRptToken);
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoDetalleDerivacion?CodDocumentoDer=") + CodDocumentoDer, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult ObtenerDocumentoDerivadoxTrabajador(int CodDocumento, int CodTrabajador)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerDocumentoDerivadoxTrabajador?CodDocumento=") + CodDocumento + "&CodTrabajador=" + CodTrabajador, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/ObtenerDocumentoDerivadoxTrabajador?CodDocumento=" + CodDocumento + "&CodTrabajador="+ CodTrabajador, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public ActionResult ValidarDevolverDocumento(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ValidarDevolverDocumento?codDocumento=") + codDocumento, strRptToken);
            //RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(ConfigurationUtilities.WebApiUrl + "/documento/ValidarDevolverDocumento?codDocumento=" + codDocumento, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetSeguimientoDocumento(int codDocumento)
        {
            //Obtener resultado busqueda            
            var rspt = new ProxyApi(base.GetToken()).ProxyGetSeguimientoDocumento(codDocumento);
            return View("_SeguimientoDocumento", rspt);
        }

        [HttpGet]
        public ActionResult GetSeguimiento(int codDocumento, int origen)
        {
            var comentarios = new ProxyApi(base.GetToken()).ProxyGetComentarios(codDocumento);
            var lstAnexos = new ProxyApi(base.GetToken()).ProxyGetDocumentoAnexos(codDocumento);
            var lstDerivados = new ProxyApi(base.GetToken()).ProxyGetDetalleSeguimiento(codDocumento);
            ViewBag.Comentarios = comentarios;
            ViewBag.ListaAnexos = lstAnexos;
            ViewBag.ListaDetalle = lstDerivados;
            var rspt = new ProxyApi(base.GetToken()).ProxyGetCabeceraSeguimiento(codDocumento);

            return View("_SeguimientoCabecera", rspt);
        }

        [HttpGet]
        public ActionResult GetSeguimientoInterno(int codDocumento)
        {

            var comentarios = new ProxyApi(base.GetToken()).ProxyGetComentarios(codDocumento);
            var lstAnexos = new ProxyApi(base.GetToken()).ProxyGetDocumentoAnexos(codDocumento);
            var lstDerivados = new ProxyApi(base.GetToken()).ProxyGetDetalleSeguimiento(codDocumento);
            ViewBag.Comentarios = comentarios;
            ViewBag.ListaAnexos = lstAnexos;
            ViewBag.ListaDetalle = lstDerivados;
            var rspt = new ProxyApi(base.GetToken()).ProxyGetCabeceraSeguimientoInterno(codDocumento);

            return View("_SeguimientoCabeceraInterno", rspt);
        }


        [HttpGet]
        public async Task<ActionResult> CargarParametroEstadoJefe()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosEstadoJefe", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> CargarParametroAfirmacion()
        {
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            ParametrosComunDTO parametros = new ParametrosComunDTO();

            respuesta.data = new ParametrosComunDTO();
            respuesta = await Service.GetAsync<RespuestaOperacionServicio>("parametro/GetParametrosAfirmacion", this.Token);

            if (respuesta.Resultado == 1)
            {
                parametros = ConvertToObject<ParametrosComunDTO>(respuesta.data);
            }

            return Json(parametros, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ValidarFirmante(int codDocumento,int codTrabajador)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ValidarFirmante?codDocumento=") + codDocumento+("&codTrabajador="+codTrabajador), strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDocumentosDerivadosPorCodDocumento(int CodDocumento, int TipoAcceso)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/GetDocumentosDerivadosPorCodDocumento?CodDocumento=") + CodDocumento + ("&TipoAcceso=" + TipoAcceso), strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetEstadoDocumento(int codDocumento)
        {
            var strRptToken = base.GetToken();
            Service _service = new Service();
            RespuestaOperacionServicio rptServ = _service.HttpGetJson<RespuestaOperacionServicio>(UrlApi.ObtenerUrl("/documento/ObtenerEstado/") + codDocumento, strRptToken);
            return Json(rptServ, JsonRequestBehavior.AllowGet);
        }
    }
}