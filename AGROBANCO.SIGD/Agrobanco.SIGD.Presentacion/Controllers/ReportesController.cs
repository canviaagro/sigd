﻿using Agrobanco.SIGD.Presentacion.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Presentacion.Utilities;
using Agrobanco.SIGD.Presentacion.Proxy;
using Agrobanco.SIGD.Presentacion.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using ClosedXML.Excel;
using static Agrobanco.SIGD.Comun.Enumerados;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;

namespace Agrobanco.SIGD.Presentacion.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class ReportesController : BaseController
    {
        // GET: Reportes
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> RegMesaPartes()
        {
            int nroPagina = 1;


            string FechaInicio = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaFinal = base.MapFecha("", TipoFecha.Api);

            ReportesMesaPartesViewModel reporteViewModel = new ReportesMesaPartesViewModel();
            reporteViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<ReportesMesaPartes>();
            reporteViewModel.FechaDesde = base.MapFecha(FechaInicio, TipoFecha.Web);
            reporteViewModel.FechaHasta = base.MapFecha(FechaFinal, TipoFecha.Web);

            string HD_codArea = "0";
            string HD_codTipDoc = "0";
            int HD_codEstadoDoc = Convert.ToInt16(EstadoDocumento.PENDIENTE);

            ViewBag.HD_codArea = HD_codArea;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_codEstadoDoc = HD_codEstadoDoc;
            ViewBag.HD_FecInicio = FechaInicio;
            ViewBag.HD_FecFin = FechaFinal;
              
            //Obtener resultado busqueda         
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyReporteMesaPartes(nroPagina, 10 , "0" ,"0", FechaInicio, FechaFinal, HD_codEstadoDoc.ToString());
            reporteViewModel.ResultadoBusqueda = onePageListaAreas;
            reporteViewModel.ResultadoBusqueda.ListaPaginado = onePageListaAreas.ToPageReporteList();
            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();
            return View(reporteViewModel);
        }
        

        [HttpPost]
        public async Task<ActionResult> RegMesaPartes(int? page, string HD_codArea, string fecInicio,string fecFinal,string HD_codTipDoc,string HD_codEstadoDoc)
        {
            int nroPagina = page == null ? 1 : page.Value;
            //string scboArea = cboArea == "" ? "0" : cboArea.Value;
            if (HD_codArea == null)
            {  HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ",");         }
                       
            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            {  HD_codTipDoc = HD_codTipDoc.Replace("|", ",");      }

            if (HD_codEstadoDoc == null)
            { HD_codEstadoDoc = Convert.ToInt16(EstadoDocumento.PENDIENTE).ToString(); ; }
            else
            { HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            ViewBag.HD_codArea = HD_codArea;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_codEstadoDoc = HD_codEstadoDoc;
            ViewBag.HD_FecInicio = fecInicio;
            ViewBag.HD_FecFin = fecFinal;

            fecInicio = base.MapFecha(fecInicio, TipoFecha.Api);
            fecFinal = base.MapFecha(fecFinal, TipoFecha.Api);  
            
            var onePageReporteMesaPartes = await new ProxyApi(base.GetToken()).ProxyReporteMesaPartes(nroPagina, 10, HD_codArea, HD_codTipDoc, fecInicio, fecFinal, HD_codEstadoDoc);            
            onePageReporteMesaPartes.ListaPaginado = onePageReporteMesaPartes.ToPageReporteList();               

            return PartialView("_SearchRegMesaPartes", onePageReporteMesaPartes);
        }

                
        public async Task<FileResult> ExportarPDF(string HD_codArea, string HD_codTipDoc, string HD_FecInicio, string HD_FecFin,string HD_codEstadoDoc)
        {
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }

            if (HD_codEstadoDoc == null)
            { HD_codEstadoDoc = "0"; }
            else
            { HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            HD_FecInicio = base.MapFecha(HD_FecInicio, TipoFecha.Api);
            HD_FecFin = base.MapFecha(HD_FecFin, TipoFecha.Api);
            
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "PDFMesaPartes";
            string fileNameReport = "ReporteMesaParte_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            List<ReportesMesaPartes> lista = (await new ProxyApi(base.GetToken()).ProxyReporteMesaPartes(1, int.MaxValue, HD_codArea, HD_codTipDoc, HD_FecInicio, HD_FecFin, HD_codEstadoDoc)).Lista;
            return PDFreporteMesaPartesResult(viewReport, lista, fileNameReport, true);          

        }

        public void ExportarExcel(string HD_codArea, string HD_codTipDoc, string HD_FecInicio, string HD_FecFin, string HD_codEstadoDoc)
        {
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }
            if (HD_codEstadoDoc == null)
            { HD_codEstadoDoc = "0"; }
            else
            { HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            HD_FecInicio = base.MapFecha(HD_FecInicio, TipoFecha.Api);
            HD_FecFin = base.MapFecha(HD_FecFin, TipoFecha.Api);

            List<ReportesMesaPartes> lista = (new ProxyApi(base.GetToken()).ProxyReporteMesaPartes_Sync(1, int.MaxValue, HD_codArea, HD_codTipDoc, HD_FecInicio, HD_FecFin, HD_codEstadoDoc)).Lista;
                                  
            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Reporte de Mesa de Partes";
            string fileNameReport = "ReporteExcelMesaPartes_" + DateTime.Now.ToString("yyyyMMddHHmmss");           

            GenerarExcelResponse<List<ReportesMesaPartes>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 8;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.FontSize = 15;

                rowIndex++;
                rowIndex++;
             
          
                foreach (var item in data.Select(c => c.DesArea).Distinct())
                {
                    ws.Cell(rowIndex, 1).Value = "Area : " + item.ToString();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                    rowIndex++;
                    rowIndex++;

                    ws.Cell(rowIndex, 1).Value = "Nro. Documento";
                    ws.Cell(rowIndex, 2).Value = "Nro. Trámite";
                    ws.Cell(rowIndex, 3).Value = "Institución";
                    ws.Cell(rowIndex, 4).Value = "Fecha Derivación";
                    ws.Cell(rowIndex, 5).Value = "Sumilla / Asunto";
                    ws.Cell(rowIndex, 6).Value = "Responsable";
                    ws.Cell(rowIndex, 7).Value = "Estado";
                    ws.Cell(rowIndex, 8).Value = "Firma / Sello";

                    
                    ws.Column(1).Width = 35;
                    ws.Column(2).Width = 18;
                    ws.Column(3).Width = 35;
                    ws.Column(4).Width = 18;
                    ws.Column(5).Width = 50;
                    ws.Column(6).Width = 35;
                    ws.Column(7).Width = 18;
                    ws.Column(8).Width = 30;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    
                    int cant = 0;
                
                    foreach (var ite in data.Where(l => l.DesArea == item).ToList())
                    {
                        cant = cant + 1;

                        rowIndex++;
                        ws.Cell(rowIndex, 1).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 1).Value = ite.vNumDocumento.ToString();
                        ws.Cell(rowIndex, 1).Style.Alignment.SetWrapText(true);
                        ws.Cell(rowIndex, 2).Value = ite.vCorrelativo;
                        ws.Cell(rowIndex, 2).Style.Alignment.SetWrapText(true);
                        ws.Cell(rowIndex, 3).Value = ite.Institucion;
                        ws.Cell(rowIndex, 3).Style.Alignment.WrapText = true;
                        //ws.Cell(rowIndex, 4).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 4).Value = ite.FechaDerivacion.ToString();
                        //ws.Cell(rowIndex, 4).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 5).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 5).Value = ite.vAsunto.ToString();
                        ws.Cell(rowIndex, 6).Value = ite.Responsable.ToString();
                        ws.Cell(rowIndex, 6).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 7).Value = ite.EstadoDoc.ToString();
                        ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 8).Value = ite.FirmaSello.ToString();
                        ws.Cell(rowIndex, 8).Style.Alignment.WrapText = true;
                        ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                        ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);

                    }
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = "Cantidad de Registros Encontrados : " + cant.ToString();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;


                    rowIndex++;
                    rowIndex++;
                }              
                ws.Range(6, 1, rowIndex, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                //ws.Range(6, 1, rowIndex, 7).Style.NumberFormat.SetFormat("@");
                //ws.Columns().AdjustToContents();
                //ws.Rows().AdjustToContents();

            });
                                             
        }

        // Reporte de Area Fecha y Estado
        #region  Reporte de Area Fecha y Estado
                       
        [HttpGet]
        public async Task<ActionResult> AreaFechaEstado()
        {

            //return View();
            int nroPagina = 1;


            string FechaInicio = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaFinal = base.MapFecha("", TipoFecha.Api);

            ReportesMesaPartesViewModel reporteViewModel = new ReportesMesaPartesViewModel();
            reporteViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<ReportesMesaPartes>();
            reporteViewModel.FechaDesde = base.MapFecha(FechaInicio, TipoFecha.Web);
            reporteViewModel.FechaHasta = base.MapFecha(FechaFinal, TipoFecha.Web);

            string HD_codArea = "0";
            string HD_codTipDoc = "0";
            int HD_codEstadoDoc = Convert.ToInt16(EstadoDocumento.PENDIENTE);

            ViewBag.HD_codArea = HD_codArea;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_codEstadoDoc = HD_codEstadoDoc;
            ViewBag.HD_FecInicio = FechaInicio;
            ViewBag.HD_FecFin = FechaFinal;

            //Obtener resultado busqueda         
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyReporteAreafechaEstado(nroPagina, 10, "0", "0", FechaInicio, FechaFinal, HD_codEstadoDoc.ToString());
            reporteViewModel.ResultadoBusqueda = onePageListaAreas;
            reporteViewModel.ResultadoBusqueda.ListaPaginado = onePageListaAreas.ToPageReporteList();
            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();
            return View(reporteViewModel);
        }


        /// <summary>
        ///Se obtiene una lista de reporte de documentos internos
        /// </summary>
        /// <param name="page"></param>
        /// <param name="HD_codArea"></param>
        /// <param name="fecInicio"></param>
        /// <param name="fecFinal"></param>
        /// <param name="HD_codTipDoc"></param>
        /// <param name="HD_codEstadoDoc"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AreaFechaEstado(int? page, string HD_codArea, string fecInicio, string fecFinal, string HD_codTipDoc, string HD_codEstadoDoc)
        {
            int nroPagina = page == null ? 1 : page.Value;
            //string scboArea = cboArea == "" ? "0" : cboArea.Value;
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }

            if (HD_codEstadoDoc == null)
            { HD_codEstadoDoc = Convert.ToInt16(EstadoDocumento.PENDIENTE).ToString(); ; }
            else
            { HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            ViewBag.HD_codArea = HD_codArea;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_codEstadoDoc = HD_codEstadoDoc;
            ViewBag.HD_FecInicio = fecInicio;
            ViewBag.HD_FecFin = fecFinal;

            fecInicio = base.MapFecha(fecInicio, TipoFecha.Api);
            fecFinal = base.MapFecha(fecFinal, TipoFecha.Api);

            var onePageReporteMesaPartes = await new ProxyApi(base.GetToken()).ProxyReporteAreafechaEstado(nroPagina, 10, HD_codArea, HD_codTipDoc, fecInicio, fecFinal, HD_codEstadoDoc);
            onePageReporteMesaPartes.ListaPaginado = onePageReporteMesaPartes.ToPageReporteList();

            return PartialView("AreaFechaEstado/_SearchAreaFechaEstado", onePageReporteMesaPartes);
        }


        public async Task<FileResult> ExportarPDFAreaFechaEstado(string HD_codArea, string HD_codTipDoc, string HD_FecInicio, string HD_FecFin, string HD_codEstadoDoc)
        {
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }

            if (HD_codEstadoDoc == null)
            { HD_codEstadoDoc = "0"; }
            else
            { HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            HD_FecInicio = base.MapFecha(HD_FecInicio, TipoFecha.Api);
            HD_FecFin = base.MapFecha(HD_FecFin, TipoFecha.Api);

            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "AreaFechaEstado/PDFAreaFechaEstado";
            string fileNameReport = "ReporteAreaFechaEstado_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            List<ReportesMesaPartes> lista = (await new ProxyApi(base.GetToken()).ProxyReporteAreafechaEstado(1, int.MaxValue, HD_codArea, HD_codTipDoc, HD_FecInicio, HD_FecFin, HD_codEstadoDoc)).Lista;
            return PDFreporteMesaPartesResult(viewReport, lista, fileNameReport, true);

        }

        public void ExportarExcelAreaFechaEstado(string HD_codArea, string HD_codTipDoc, string HD_FecInicio, string HD_FecFin, string HD_codEstadoDoc)
        {
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }
            if (HD_codEstadoDoc == null)
            { HD_codEstadoDoc = "0"; }
            else
            { HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            HD_FecInicio = base.MapFecha(HD_FecInicio, TipoFecha.Api);
            HD_FecFin = base.MapFecha(HD_FecFin, TipoFecha.Api);

            List<ReportesMesaPartes> lista = (new ProxyApi(base.GetToken()).ProxyReporteAreafechaEstado_Sync(1, int.MaxValue, HD_codArea, HD_codTipDoc, HD_FecInicio, HD_FecFin, HD_codEstadoDoc)).Lista;

            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Reporte Area, Fecha y Estado";
            string fileNameReport = "ReporteExcelAreaFechaEstado_" + DateTime.Now.ToString("yyyyMMddHHmmss");


            GenerarExcelResponse<List<ReportesMesaPartes>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 5;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.FontSize = 15;

                rowIndex++;
                rowIndex++;


                foreach (var item in data.Select(c => c.DesArea).Distinct())
                {
                    ws.Cell(rowIndex, 1).Value = "Área Derivado : " + item.ToString();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;

                    rowIndex++;
                    rowIndex++;

                    ws.Cell(rowIndex, 1).Value = "Nro. Trámite";
                    ws.Cell(rowIndex, 2).Value = "Fecha Documento";
                    ws.Cell(rowIndex, 3).Value = "Asunto";
                    ws.Cell(rowIndex, 4).Value = "Estado";
                    ws.Cell(rowIndex, 5).Value = "Responsable Cierre Tramite";

                    ws.Column(1).Width = 35;
                    ws.Column(2).Width = 18;
                    ws.Column(3).Width = 35;
                    ws.Column(4).Width = 18;
                    ws.Column(5).Width = 50;

                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                    int cant = 0;

                    foreach (var ite in data.Where(l => l.DesArea == item).ToList())
                    {
                        cant = cant + 1;

                        rowIndex++;
                        ws.Cell(rowIndex, 1).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 1).Value = ite.vCorrelativo.ToString();
                        ws.Cell(rowIndex, 1).Style.Alignment.SetWrapText(true);
                        ws.Cell(rowIndex, 2).Value = ite.FechaDocumento.ToString();
                        //ws.Cell(rowIndex, 2).Style.Alignment.SetWrapText(true);
                        ws.Cell(rowIndex, 3).Value = ite.vAsunto;
                        ws.Cell(rowIndex, 3).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 4).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 4).Value = ite.EstadoDoc.ToString();
                        ws.Cell(rowIndex, 4).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 5).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 5).Value = ite.Responsable.ToString();

                        ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                        ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);

                    }
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Value = "Cantidad de Registros Encontrados : " + cant.ToString();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;


                    rowIndex++;
                    rowIndex++;
                }
                ws.Range(6, 1, rowIndex, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                //ws.Range(6, 1, rowIndex, 7).Style.NumberFormat.SetFormat("@");
                //ws.Columns().AdjustToContents();
                //ws.Rows().AdjustToContents();

            });

        }


        #endregion



        // Reporte Seguimiento De Documentos
        #region  Seguimiento De Documentos

        [HttpGet]
        public async Task<ActionResult> SeguimientoDeDocumentos()
        {

            //return View();
            int nroPagina = 1;


            //string FechaInicio = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            //string FechaFinal = base.MapFecha("", TipoFecha.Api);

            ReportesMesaPartesViewModel reporteViewModel = new ReportesMesaPartesViewModel();
            reporteViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<ReportesMesaPartes>();
            //reporteViewModel.FechaDesde = base.MapFecha(FechaInicio, TipoFecha.Web);
            //reporteViewModel.FechaHasta = base.MapFecha(FechaFinal, TipoFecha.Web);

            string HD_codArea = "0";
            string HD_codTipDoc = "0";
            string HD_anio = "0";
            string HD_nrotramite = "";
            //int HD_codEstadoDoc = Convert.ToInt16(EstadoDocumento.PENDIENTE);

            ViewBag.HD_codArea = HD_codArea;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_anio = HD_anio;
            ViewBag.HD_nrotramite = HD_nrotramite;
            
            //ViewBag.HD_codEstadoDoc = HD_codEstadoDoc;
            //ViewBag.HD_FecInicio = FechaInicio;
            //ViewBag.HD_FecFin = FechaFinal;

            //Obtener resultado busqueda         
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyReporteSeguimientoDocumentos(nroPagina, 10, "0", "0","0", "");
            reporteViewModel.ResultadoBusqueda = onePageListaAreas;
            reporteViewModel.ResultadoBusqueda.ListaPaginado = onePageListaAreas.ToPageReporteList();
            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();
            return View(reporteViewModel);
        }

        
        /// <summary>
        ///Se obtiene una lista de reporte de documentos internos
        /// </summary>
        /// <param name="page"></param>
        /// <param name="HD_codArea"></param>
        /// <param name="fecInicio"></param>
        /// <param name="fecFinal"></param>
        /// <param name="HD_codTipDoc"></param>
        /// <param name="HD_codEstadoDoc"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> SeguimientoDeDocumentos(int? page, string HD_codArea, string HD_nrotramite, string HD_anio, string HD_codTipDoc)
        {            
            int nroPagina = page == null ? 1 : page.Value;
            //string scboArea = cboArea == "" ? "0" : cboArea.Value;
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }

            //if (HD_codEstadoDoc == null)
            //{ HD_codEstadoDoc = Convert.ToInt16(EstadoDocumento.PENDIENTE).ToString(); ; }
            //else
            //{ HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            if (HD_nrotramite == null)
            { HD_nrotramite = ""; }

            if (HD_anio == null || HD_anio == "")
            { HD_anio = "0"; }
            
            ViewBag.HD_codArea = HD_codArea;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_anio = HD_anio;
            ViewBag.HD_nrotramite = HD_nrotramite;

            //ViewBag.HD_codEstadoDoc = HD_codEstadoDoc;
            //ViewBag.HD_FecInicio = fecInicio;
            //ViewBag.HD_FecFin = fecFinal;

            //fecInicio = base.MapFecha(fecInicio, TipoFecha.Api);
            //fecFinal = base.MapFecha(fecFinal, TipoFecha.Api);

            var onePageReporteMesaPartes = await new ProxyApi(base.GetToken()).ProxyReporteSeguimientoDocumentos(nroPagina, 10, HD_codArea, HD_codTipDoc, HD_anio, HD_nrotramite);
            onePageReporteMesaPartes.ListaPaginado = onePageReporteMesaPartes.ToPageReporteList();

            return PartialView("SeguimientoDeDocumentos/_SearchSeguimientoDeDocumentos", onePageReporteMesaPartes);
        }

        

        public async Task<FileResult> ExportarPDFSeguimientoDeDocumentos(string HD_codArea, string HD_codTipDoc, string HD_nrotramite , string HD_anio)
        {
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }

            if (HD_nrotramite == null)
            { HD_nrotramite = ""; }

            if (HD_anio == null)
            { HD_anio = "0"; }

            //if (HD_codEstadoDoc == null)
            //{ HD_codEstadoDoc = "0"; }
            //else
            //{ HD_codEstadoDoc = HD_codEstadoDoc.Replace("|", ","); }

            //HD_FecInicio = base.MapFecha(HD_FecInicio, TipoFecha.Api);
            //HD_FecFin = base.MapFecha(HD_FecFin, TipoFecha.Api);

            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "SeguimientoDeDocumentos/PDFSeguimientoDeDocumentos";
            string fileNameReport = "ReporteSeguimientoDeDocumentos" + DateTime.Now.ToString("yyyyMMddHHmmss");
            List<ReportesMesaPartes> lista = (await new ProxyApi(base.GetToken()).ProxyReporteSeguimientoDocumentos(1, int.MaxValue, HD_codArea, HD_codTipDoc, HD_anio , HD_nrotramite)).Lista;
            return PDFreporteMesaPartesResult(viewReport, lista, fileNameReport, true);

        }

        
        public void ExportarExcelSeguimientoDeDocumentos(string HD_codArea, string HD_codTipDoc, string HD_nrotramite , string HD_anio)
        {
            if (HD_codArea == null)
            { HD_codArea = "0"; }
            else
            { HD_codArea = HD_codArea.Replace("|", ","); }

            if (HD_codTipDoc == null)
            { HD_codTipDoc = "0"; }
            else
            { HD_codTipDoc = HD_codTipDoc.Replace("|", ","); }


            if (HD_nrotramite == null)
            { HD_nrotramite = ""; }
            
            if (HD_anio == null)
            { HD_anio = "0"; }
                                  

            List<ReportesMesaPartes> lista = (new ProxyApi(base.GetToken()).ProxyReporteSeguimientoDocumentos_Sync(1, int.MaxValue, HD_codArea, HD_codTipDoc, HD_anio, HD_nrotramite)).Lista;

            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Reporte Seguimiento de Documentos";
            string fileNameReport = "ReporteExcelSeguimientoDeDocumentos_" + DateTime.Now.ToString("yyyyMMddHHmmss");


            GenerarExcelResponse<List<ReportesMesaPartes>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 7;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.FontSize = 15;

                rowIndex++;
                rowIndex++;
                          

                   ws.Cell(rowIndex, 1).Value = "Nro.Trámite";
                    ws.Cell(rowIndex, 2).Value = "Fecha Documento";
                    ws.Cell(rowIndex, 3).Value = "Remitente";
                    ws.Cell(rowIndex, 4).Value = "Destinatario";
                    ws.Cell(rowIndex, 5).Value = "Asunto del Documento";
                    ws.Cell(rowIndex, 6).Value = "Observación";
                    ws.Cell(rowIndex, 7).Value = "Estado";

                    ws.Column(1).Width = 25;
                    ws.Column(2).Width = 18;
                    ws.Column(3).Width = 35;
                    ws.Column(4).Width = 35;
                    ws.Column(5).Width = 35;
                    ws.Column(6).Width = 35;
                    ws.Column(7).Width = 18;
                
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                    
                    foreach (var ite in data)
                    {
                       

                        rowIndex++;
                        ws.Cell(rowIndex, 1).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 1).Value = ite.vCorrelativo.ToString();
                        ws.Cell(rowIndex, 1).Style.Alignment.SetWrapText(true);
                        ws.Cell(rowIndex, 2).Value = ite.FechaDocumento.ToString();
                        
                        ws.Cell(rowIndex, 3).Value = ite.vRemitente;
                        ws.Cell(rowIndex, 3).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 4).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 4).Value = ite.vDestinatario.ToString();
                        ws.Cell(rowIndex, 4).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 5).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 5).Value = ite.vAsunto.ToString();

                        ws.Cell(rowIndex, 6).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 6).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 6).Value = ite.vObservacion;


                        ws.Cell(rowIndex, 7).Style.NumberFormat.SetFormat("@");
                        ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;
                        ws.Cell(rowIndex, 7).Value = ite.EstadoDoc;


                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetInsideBorder(XLBorderStyleValues.Thin);

                    }
                    rowIndex++;
                    
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                    ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    ws.Range(6, 1, rowIndex, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
              

            });

        }

        [HttpGet]
        public async Task<ActionResult> ObtenerAnios()
        {
            string url = "Reportes/ListarAnios/";
            var token = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            RespuestaOperacionServicio respuesta = new RespuestaOperacionServicio();
            List<ReportesMesaPartes> anios = new List<ReportesMesaPartes>();

            respuesta = await Service.GetAsync<RespuestaOperacionServicio>(url, token);

            if (respuesta.Resultado == 1)
            {
                anios = Helpers.ConvertToObject<List<ReportesMesaPartes>>(respuesta.data);
            }

            respuesta.data = anios;

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        #endregion


        //Reporte Atencion de Documentos
        #region  Atencion de Documentos
        [HttpGet]

        public async Task<ActionResult> AtencionDocumentos() {

            int nroPagina = 1;

            string FechaInicio = base.MapFecha("", TipoFecha.Api, ConfigurationUtilities.DiasDiferenciaBusqueda);
            string FechaFinal = base.MapFecha("", TipoFecha.Api);

            ReportesAtencionDocumentosViewModel reporteViewModel = new ReportesAtencionDocumentosViewModel();
            reporteViewModel.ResultadoBusqueda = new ResultadoBusquedaModel<ReportesAtencionDocumentos>();
            reporteViewModel.FechaDesde = base.MapFecha(FechaInicio, TipoFecha.Web);
            reporteViewModel.FechaHasta = base.MapFecha(FechaFinal, TipoFecha.Web);


            ViewBag.HD_NroTramite = "VACIO";
            ViewBag.HD_FecInicio = FechaInicio;
            ViewBag.HD_FecFin = FechaFinal;
            ViewBag.HD_codAreaResponsable = "TODOS";
            ViewBag.HD_codAreaDerivada = "TODOS";
            ViewBag.HD_codTipDoc = "TODOS";
            ViewBag.HD_codEstadoDoc = "TODOS";
            ViewBag.HD_Firmado = "TODOS";

            //Obtener resultado busqueda         
            var onePageListaAreas = await new ProxyApi(base.GetToken()).ProxyReporteAtencionDocumentos(nroPagina, 10, "", FechaInicio, FechaFinal, "TODOS", "TODOS", "TODOS", "TODOS","TODOS");
            reporteViewModel.ResultadoBusqueda = onePageListaAreas;
            reporteViewModel.ResultadoBusqueda.ListaPaginado = onePageListaAreas.ToPageReporteList();
            //Obtener datos del nuevo registro
            DocumentoController documentoController = new DocumentoController();
            documentoController.Token = base.GetToken();
            return View("~/Views/Reportes/AtencionDocumentos/AtencionDocumentos.cshtml", reporteViewModel);
        }


        [HttpPost]
        public async Task<ActionResult> AtencionDocumentos(int? page, string nrotramite,string fecInicio,string fecFinal,string HD_codAreaResponsable,
                                               string HD_codAreaDerivada,string HD_codTipDoc, string HD_EstadoDoc,string cboFirmado)
        {
            int nroPagina = page == null ? 1 : page.Value;
            fecInicio = base.MapFecha(fecInicio, TipoFecha.Api);
            fecFinal = base.MapFecha(fecFinal, TipoFecha.Api);
            cboFirmado = String.IsNullOrEmpty(cboFirmado) == true ? "TODOS": cboFirmado;
            HD_codAreaResponsable = String.IsNullOrEmpty(HD_codAreaResponsable) ==true ? "TODOS" : HD_codAreaResponsable;
            HD_codAreaDerivada = String.IsNullOrEmpty(HD_codAreaDerivada) == true ? "TODOS" : HD_codAreaDerivada;
            HD_codTipDoc = String.IsNullOrEmpty(HD_codTipDoc) == true ? "TODOS" : HD_codTipDoc;
            HD_EstadoDoc = String.IsNullOrEmpty(HD_EstadoDoc) == true ? "TODOS" : HD_EstadoDoc;

            ViewBag.HD_NroTramite = nrotramite;
            ViewBag.HD_FecInicio = fecInicio;
            ViewBag.HD_FecFin = fecFinal;
            ViewBag.HD_codAreaResponsable = HD_codAreaResponsable;
            ViewBag.HD_codAreaDerivada = HD_codAreaDerivada;
            ViewBag.HD_codTipDoc = HD_codTipDoc;
            ViewBag.HD_codEstadoDoc = HD_EstadoDoc;
            ViewBag.HD_Firmado = cboFirmado;

            var onePageReporteAtencionDocumentos = await new ProxyApi(base.GetToken()).ProxyReporteAtencionDocumentos(nroPagina, 10, nrotramite, fecInicio, fecFinal, HD_codAreaResponsable,
                                                                    HD_codAreaDerivada, HD_codTipDoc, HD_EstadoDoc, cboFirmado);
            onePageReporteAtencionDocumentos.ListaPaginado = onePageReporteAtencionDocumentos.ToPageReporteList();

            return PartialView("AtencionDocumentos/_SearchAtencionDocumentos", onePageReporteAtencionDocumentos);
        }

        public async Task<FileResult> ExportarPDFAtencionDocumentos(string nrotramite, string fecInicio, string fecFinal, string HD_codAreaResponsable,
                                                                 string HD_codAreaDerivada, string HD_codTipDoc, string HD_EstadoDoc, string cboFirmado)
        {

            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string viewReport = "AtencionDocumentos/PDFAtencionDocumentos";
            string fileNameReport = "ReporteAtencionDeDocumentos" + DateTime.Now.ToString("yyyyMMddHHmmss");
            List<ReportesAtencionDocumentos> lista = (await new ProxyApi(base.GetToken()).ProxyReporteAtencionDocumentos(1, int.MaxValue, nrotramite, fecInicio, fecFinal, HD_codAreaResponsable,
                                                                    HD_codAreaDerivada, HD_codTipDoc, HD_EstadoDoc, cboFirmado)).Lista;

            return PDFreporteMesaPartesResult(viewReport, lista, fileNameReport, true);
        }


        public void ExportarExcelAtencionDocumentos(string nrotramite, string fecInicio, string fecFinal, string HD_codAreaResponsable,
                                                      string HD_codAreaDerivada, string HD_codTipDoc, string HD_EstadoDoc, string cboFirmado)
        {

            List<ReportesAtencionDocumentos> lista = (new ProxyApi(base.GetToken()).ProxyReporteAtencionDocumentos_Sync(1, int.MaxValue, nrotramite, fecInicio, fecFinal, HD_codAreaResponsable,
                                                                    HD_codAreaDerivada, HD_codTipDoc, HD_EstadoDoc, cboFirmado)).Lista;

            string ultruta = Convert.ToString(Request.UrlReferrer.Segments.Last()).ToLower();
            string titulo = "Reporte Atención de Documentos";
            string fileNameReport = "ReporteExcelAtencionDocumentos_" + DateTime.Now.ToString("yyyyMMddHHmmss");


            GenerarExcelResponse<List<ReportesAtencionDocumentos>>(fileNameReport, "Hoja1", lista, (ws, data) =>
            {
                var rowIndex = 3;
                var lastColum = 8;
                ws.Cell(rowIndex, 1).Value = titulo;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Merge();
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.FontSize = 15;

                rowIndex++;
                rowIndex++;


                ws.Cell(rowIndex, 1).Value = "Nro.Trámite";
                ws.Cell(rowIndex, 2).Value = "Remitente";
                ws.Cell(rowIndex, 3).Value = "Destinatario";
                ws.Cell(rowIndex, 4).Value = "Asunto";
                ws.Cell(rowIndex, 5).Value = "Comentarios";
                ws.Cell(rowIndex, 6).Value = "Firmado";
                ws.Cell(rowIndex, 7).Value = "Fecha Firma";
                ws.Cell(rowIndex, 8).Value = "Estado";

                ws.Column(1).Width = 25;
                ws.Column(2).Width = 18;
                ws.Column(3).Width = 20;
                ws.Column(4).Width = 25;
                ws.Column(5).Width = 35;
                ws.Column(6).Width = 15;
                ws.Column(7).Width = 18;
                ws.Column(8).Width = 18;

                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Font.Bold = true;
                ws.Range(rowIndex, 1, rowIndex, lastColum).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);


                foreach (var ite in data)
                {
                    rowIndex++;
                    ws.Cell(rowIndex, 1).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 1).Value = ite.vCorrelativo.ToString();
                    ws.Cell(rowIndex, 1).Style.Alignment.SetWrapText(true);
                    
                    ws.Cell(rowIndex, 2).Value = ite.Remitente.ToString();
                    ws.Cell(rowIndex, 2).Style.Alignment.SetWrapText(true);

                    ws.Cell(rowIndex, 3).Value = ite.Destinatario;
                    ws.Cell(rowIndex, 3).Style.Alignment.WrapText = true;

                    ws.Cell(rowIndex, 4).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 4).Value = ite.Asunto.ToString();
                    ws.Cell(rowIndex, 4).Style.Alignment.WrapText = true;

                    ws.Cell(rowIndex, 5).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 5).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 5).Value = ite.Comentarios;

                    ws.Cell(rowIndex, 6).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 6).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 6).Value = String.IsNullOrWhiteSpace(ite.siFirma)? "No" : "Sí";


                    ws.Cell(rowIndex, 7).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 7).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 7).Value = ite.fechaFirma;

                    ws.Cell(rowIndex, 8).Style.NumberFormat.SetFormat("@");
                    ws.Cell(rowIndex, 8).Style.Alignment.WrapText = true;
                    ws.Cell(rowIndex, 8).Value = ite.DescripcionEstado;

                }
            });

        }

        #endregion
    }
}