﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.EntidadesDb2;
using Agrobanco.SIGD.Presentacion.Filters;
using Agrobanco.SIGD.Presentacion.Filters.Seguridad;
using Agrobanco.SIGD.Presentacion.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Agrobanco.SIGD.Presentacion.Controllers
{
    [AutenticacionUsuario]
    [ErrorFilter]
    public class TrabajadorController : Controller
    {
        [HttpGet]
        public JsonResult ListaTrabajadorPorArea(int codarea, int siEsJefe)
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            List<TrabajadorDTO> lstDoc = _service.HttpGetJson<List<TrabajadorDTO>>(UrlApi.ObtenerUrl("/trabajador/GetTrabajadorArea/") + codarea + "/" + siEsJefe , strRptToken);

            return Json(lstDoc,JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListaTrabajadorPorAreaSIED()
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            List<TrabajadorDTO> lstDoc = _service.HttpGetJson<List<TrabajadorDTO>>(UrlApi.ObtenerUrl("/trabajador/GetTrabajadorAreaSIED") , strRptToken);

            return Json(lstDoc, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListaTrabajadores()
        {
            var strRptToken = Session["tokenUser"] == null ? string.Empty : Convert.ToString(Session["tokenUser"]);
            Service _service = new Service();
            List<TrabajadorDTO> lstDoc = _service.HttpGetJson<List<TrabajadorDTO>>(UrlApi.ObtenerUrl("/trabajador/GetTrabajadores"), strRptToken);

            return Json(lstDoc, JsonRequestBehavior.AllowGet);
        }

    }
}