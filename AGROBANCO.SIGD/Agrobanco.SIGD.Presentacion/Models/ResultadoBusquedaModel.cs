﻿using Agrobanco.SIGD.Presentacion.Utilities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ResultadoBusquedaModel<T>
    {
        public IPagedList<T> ListaPagina { get; set; }
        public IPagedList<int> ListaPaginado { get; set; }
        public List<T> Lista { get; set; }
        public int TotalRegistros { get; set; }
        public int PaginaActual { get; set; }

        public IPagedList<int> ToPageList()
        {
            int nroPaginas = (int)Math.Ceiling(Convert.ToDecimal((double)this.TotalRegistros / ConfigurationUtilities.PageSize));
            int[] dataPaginas = new int[this.TotalRegistros];
            return dataPaginas.ToList().ToPagedList(this.PaginaActual, ConfigurationUtilities.PageSize);
        }

        public IPagedList<int> ToPageReporteList()
        {
            int nroPaginas = (int)Math.Ceiling(Convert.ToDecimal((double)this.TotalRegistros / 10));
            int[] dataPaginas = new int[this.TotalRegistros];
            return dataPaginas.ToList().ToPagedList(this.PaginaActual, 10);
        }

    }
}