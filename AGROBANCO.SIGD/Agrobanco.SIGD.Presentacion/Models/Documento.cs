﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Documento
    {
        public int CodDocumento { get; set; }
        public string Correlativo { get; set; }
        public int Anio { get; set; }
        public short Estado { get; set; }
        public short Origen { get; set; }
        public string NumDocumento { get; set; }
        public short EstadoDocumento { get; set; }
        public DateTime? FechaDerivacion { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Asunto { get; set; }
        public short Prioridad { get; set; }
        public string Referencia { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public DateTime FechaRecepcionSIED { get; set; }
        public short Plazo { get; set; }
        public DateTime FechaPlazo { get; set; }
        public string Observaciones { get; set; }
        public int CodTipoDocumento { get; set; }
        public int CodRepresentante { get; set; }
        public int CodArea { get; set; }
        public int CodTrabajador { get; set; }
        public string DescripcionArea { get; set; }
        //Propiedades de Auditoría

        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public  string RolActualizacion { get; set; }

        public List<DocAdjunto> LstDocAdjunto { get; set; }
        public List<DocAnexo> LstDocAnexo { get; set; }

        public string FileArray { get; set; }
        public int RemitidoPor { get; set; }
        public int? CodDocDerivacion { get; set; }
        public int posx { get; set; }
        public int posy { get; set; }
        public string reason { get; set; }
        public string CodAreaBE { get; set; }
        public string CodAreaCC { get; set; }
        public string CodAnexos { get; set; }
        public string CodDerivadosInactivar { get; set; }
        public string Comentario { get; set; }
        public string RemitidoDescripcionPor { get; set; }
        public string DescripcionRemitente { get; set; }
        //SIED
        public string CodAsuntoSIED { get; set; }
        public string CodTipoDocumentoSIED { get; set; }
        public string CodFormatoDocumentoEmpresas { get; set; }
        public List<EmpresasSIED> lstEmpresaSied { get; set; }
        public string CodRespuestaSIED { get; set; }
        public int CodDocumentoRespuestaSIED { get; set; }
        public string CodCUORespuestaSIED { get; set; }
        public string MensajeSIED { get; set; }
    }
}