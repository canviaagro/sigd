﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class DocumentoBandejaDTO
    {
        public int CodDocumento { get; set; }
        public Int16 Estado { get; set; }
        public Int16 EstadoDocumento { get; set; }
        public Int16 Prioridad { get; set; }

        public string EstadoDescripcion { get; set; }
        public string EstadoPrioridad { get; set; }

        public string Recibido { get; set; }
        public int Reg { get; set; }
        public string Documento { get; set; }
        public string DocumentoAsunto { get; set; }
        public string Remitente { get; set; }
        public string Derivado { get; set; }
        public string Plazo { get; set; }
        public string ExisteAdjunto { get; set; }
        public string Recepcion { get; set; }
        public string Correlativo { get; set; }
        public int CodEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public int CodRepresentante { get; set; }
        public string NombreTrabajador { get; set; }
        public  int Origen { get; set; }
        public string TipoDocIng { get; set; }
        public bool HabilitarSeguimiento { get; set; }
        public bool HabilitarDerivar { get; set; }
        public bool HabilitarDevolver { get; set; }
        public bool HabilitarFinalizar { get; set; }
        public bool HabilitarRegistrarAvance { get; set; }
        public int CodDocDerivacion { get; set; }
        public int? CodDocFirma { get; set; }
        public string FILTRO { get; set; }
        public string EmpresasSied { get; set; }
        public string CodRespuestaSIED { get; set; }
        public int CodDocumentoRespuestaSIED { get; set; }
    }
}