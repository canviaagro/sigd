﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ParametrosRepresentanteDTO
    {
        public List<ParametroDTO> ListaTipoRepresentante { get; set; }
        public List<TipDocIdentidad> ListaTipDocIdentidad { get; set; }
        public List<ParametroDTO> ListaEstados { get; set; }
    }
}
