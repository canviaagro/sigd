﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ReportesMesaPartes
    {


        #region Reporte de Mesa de Partes
        public string vNumDocumento { get; set; }
        public string vCorrelativo { get; set; }
        public string Institucion { get; set; }
        //public string FechaDerivacion { get; set; }
        public string FechaDerivacion { get; set; }
        public string vAsunto { get; set; }
        public string Responsable { get; set; }
        public string FirmaSello { get; set; }
        public string CodArea { get; set; }
        public string DesArea { get; set; }
        public string EstadoDoc { get; set; }
        public string FechaDocumento { get; set; }
        public string vAño { get; set; }
        public string vRemitente { get; set; }
        public string vDestinatario { get; set; }
        public string vObservacion { get; set; }
        public int idestado { get; set; }


        #endregion



    }
}