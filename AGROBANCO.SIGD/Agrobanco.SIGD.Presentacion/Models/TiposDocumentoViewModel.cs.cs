﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class TiposDocumentoViewModel
    {
        public ResultadoBusquedaModel<TipoDocumento> ResultadoBusqueda { get; set; }
        public TipoDocumento NuevoTIpodocumento { get; set; }
    }
    public class TiposDocumentoFiltrosViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}