﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class RegistrarAvance
    {
        public int CodDocumento { get; set; }
        public int? CodDocDerivacion { get; set; }
        public string Comentario { get; set; }
        public List<DocAnexo> LstDocAnexo { get; set; }
        public int Origen { get; set; }
        #region"Seguridad"
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }
        #endregion
    }
}