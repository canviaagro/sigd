﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ParameterRequest
    {
        public string app { get; set; }
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public string idFile { get; set; }
        public string type { get; set; }
        public string protocol { get; set; }
        public string fileDownloadUrl { get; set; }
        public string fileDownloadLogoUrl { get; set; }
        public string fileDownloadStampUrl { get; set; }
        public string fileUploadUrl { get; set; }
        public string contentFile { get; set; }
        public string reason { get; set; }
        public string isSignatureVisible { get; set; }
        public string stampAppearanceId { get; set; }
        public string pageNumber { get; set; }

        public string posx { get; set; }
        public string posy { get; set; }        
               
        public string fontSize { get; set; }
        public string dcfilter { get; set; }
        public string timestamp { get; set; }
        public string signatureLevel { get; set; }
        public string outputFile { get; set; }
        public string maxFileSize { get; set; }
    }
}