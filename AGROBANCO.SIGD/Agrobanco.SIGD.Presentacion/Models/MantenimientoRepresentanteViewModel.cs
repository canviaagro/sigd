﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class MantenimientoRepresentanteListadoViewModel
    {
        public ResultadoBusquedaModel<Representante> ResultadoBusqueda { get; set; }
        public int CodEmpresa { get; set; }
    }
    public class MantenimientoRepresentanteViewModel : Representante
    {
    }
    public class MantenimientoRepresentanteFiltrosViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}