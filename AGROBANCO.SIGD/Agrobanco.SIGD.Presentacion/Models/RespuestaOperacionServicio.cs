﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class RespuestaOperacionServicio
    {
        public int Resultado { get; set; }

        public string correlativo { get; set; }

        public string Error { get; set; }
        public string Informacion { get; set; }
        public Object data { set; get; }
        public int ResultadoCorreo { get; set; }
        public int posx { get; set; }
        public int posy { get; set; }

        public string codigo { get; set; }
        public string mensaje { get; set; }
        public int codigoDocumentoSIED { get; set; }
        public string codigoCUO { get; set; }
        public List<int> listEnteros { get; set; }
        public List<EmpresasSIED> listaEmpresa { get; set; }
        public List<FirmantesSIED> listaFirmante { get; set; }
        public List<Entidades.Entidades.DocDerivacion> lstDocDerivacion { get; set; }
    }

}
