﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Secuencia
    {
        public Secuencia()
        {
            auditoria = new AuxiliarModel();
        }
        public AuxiliarModel auditoria { get; set; }
        public int anio { get; set; }
        public int Estado { get; set; }
        public int correlativo { get; set; }
        public int CodSecuencial { get; set; }
        public int? CodArea { get; set; }
        public int? CodTipoDocumento { get; set; }
        public string ValorCorrelativo { get; set; }
        public int Origen { get; set; }
        public int flagUsado { get; set; }

        public string DescripcionArea { get; set; }
        public string DescripcionTipoDocumento { get; set; }
    }
}