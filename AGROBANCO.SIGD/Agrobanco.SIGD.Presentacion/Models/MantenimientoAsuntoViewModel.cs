﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class MantenimientoAsuntoListadoViewModel
    {
        public ResultadoBusquedaModel<Asunto> ResultadoBusqueda { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
    }

    public class MantenimientoAsuntoFiltrosViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}