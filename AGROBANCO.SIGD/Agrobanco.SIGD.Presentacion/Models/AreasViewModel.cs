﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class AreasViewModel
    {
        public ResultadoBusquedaModel<Area> ResultadoBusqueda { get; set; }
        public Area NuevoTIpodocumento { get; set; }

    }
}