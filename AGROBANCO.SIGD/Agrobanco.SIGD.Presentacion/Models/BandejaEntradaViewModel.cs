﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class BandejaEntradaViewModel
    {
        public ResultadoBusquedaModel<DocumentoBandejaDTO> ResultadoBusqueda { get; set; }
        public DocumentoBandejaDTO NuevoDocumentoDTO { get; set; }

        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
    }
    public class BandejaEntradaFiltrosViewModel
    {
        public string TextoBusqueda { get; set; }
    }
}