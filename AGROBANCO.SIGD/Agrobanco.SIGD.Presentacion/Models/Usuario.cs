﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Usuario
    {        
        [Required(ErrorMessage = "Ingresar Usuario")]        
        public string UserName { get; set; }
        [Required(ErrorMessage = "Ingresar Contraseña")]
        public string Password { get; set; }
        public string Mensaje { get; set; }
    }
}