﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Empresa
    {
        public int CodEmpresa { get; set; }
        public int IdEmpresa { get; set; }

        public string Abreviatura { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public short Estado { get; set; }
        public string UsuarioCreacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string EstadoDescripcion { get; set; }
    }
}