﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Trabajador
    {
        public int iCodTrabajador { get; set; }
        public string vNumDocumento { get; set; }
        public string vNombres { get; set; }
        public string vCargo { get; set; }
        public string vUbicacion { get; set; }
        public int iAnexo { get; set; }
        public string vEmail { get; set; }
        public string vApePaterno { get; set; }
        public string vApeMaterno { get; set; }
        public Int16 siEsJefe { get; set; }
        public Int16 siFirma { get; set; }
        public int iCodArea { get; set; }
        public int iCodTipDocIdentidad { get; set; }
        public string vCelular { get; set; }
        public string WEBUSR { get; set; }
        public string vNombreCompleto { get; set; }
        
        public string vArea { get; set; }
        public string vTipoDocumento { get; set; }
        public DateTime dFecCreacion { get; set; }
        public string vUsuCreacion { get; set; }
        public DateTime? dFecActualizacion { get; set; }
        public string vUsuActualizacion { get; set; }
        public int siEstado { get; set; }

        public string sFecActualizacion { get; set; }
        public string sFecCreacion { get; set; }
        public string vDescripcion { get; set; }
        public string apeCompletos { get; set; }

        public int iCodPerfil { get; set; }
        public string vCodPerfil { get; set; }
        public string vUsrweb { get; set; }
        public int iOperador { get; set; }
    }
}