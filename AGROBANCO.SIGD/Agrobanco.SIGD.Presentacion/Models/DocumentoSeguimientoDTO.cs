﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class DocumentoSeguimientoDTO
    {
        public string Colaborador { get; set; }
        public string Area { get; set; }
        public string FechaRecepcion { get; set; }
        public string TipoReg { get; set; }
        public int? CodDocMovimiento { get; set; }
        public int? CodMovAnterior { get; set; }
        public int? CodDocDerivacion { get; set; }
        public int? CodDocAuxiliar { get; set; }
        public int? TipoMovimiento { get; set; }
        public string Comentario { get; set; }
        public string EstadoDescripcion { get; set; }
        public int EstadoDocumento { get; set; }
        public int EstadoDocumentoInterno { get; set; }
        public string ColaboradorCreacion { get; set; }
        public int siEstado { get; set; }
        public string NombreDesde { get; set; }
        public string AreaDesde { get; set; }
        public string NombrePara { get; set; }
        public string AreaPara { get; set; }
        public string CodDerivacionesInternas { get; set; }
    }
}
