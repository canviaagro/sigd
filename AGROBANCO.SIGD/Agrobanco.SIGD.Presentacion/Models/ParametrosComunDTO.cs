﻿using Agrobanco.SIGD.Entidades.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ParametrosComunDTO
    {
        public List<ParametroDTO> ListaEstados { get; set; }
    }
}
