﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class FirmantesSIED
    {

        public int codEmpresa { get; set; }
        public string nomEmpresa { get; set; }
        public string cargoPersona { get; set; }
        public int codCargoPersona { get; set; }
        public string dniPersona { get; set; }
        public string nomPersona { get; set; }
        public string tipoFirma { get; set; }
        public string unidadOrganica { get; set; }
    }
}