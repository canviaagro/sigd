﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ReportesMesaPartesViewModel
    {

        public ResultadoBusquedaModel<ReportesMesaPartes> ResultadoBusqueda { get; set; }
        public ReportesMesaPartes NuevoReportes { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
    }
}