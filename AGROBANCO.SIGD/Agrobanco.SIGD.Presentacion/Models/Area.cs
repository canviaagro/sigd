﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class Area
    {
        //public Area()
        //{
        //    auditoria = new AuxiliarModel();
        //}
        public int CodArea { get; set; }
        public string Abreviatura { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public string TextoBusqueda { get; set; }
        public int Pagina { get; set; }
        public int TamanioPagina { get; set; }
        public DateTime Fecha { get; set; }
        public string sFechaCreacion { get; set; }
        public string sFechaActualizacion { get; set; }


        #region Propiedades Auditoria
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string HostCreacion { get; set; }
        public string InstanciaCreacion { get; set; }
        public string LoginCreacion { get; set; }
        public string RolCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string HostActualizacion { get; set; }
        public string InstanciaActualizacion { get; set; }
        public string LoginActualizacion { get; set; }
        public string RolActualizacion { get; set; }

        #endregion

        //public AuxiliarModel auditoria { get; set; }

    }
}