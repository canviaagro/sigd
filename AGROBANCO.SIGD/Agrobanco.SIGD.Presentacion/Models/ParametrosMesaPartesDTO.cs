﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agrobanco.SIGD.Presentacion.Models
{
    public class ParametrosMesaPartesDTO
    {
        public ParametrosMesaPartesDTO()
        {
            LstArea = new List<Area>();
            LstOrigen = new List<Parametro>();
            LstPrioridad = new List<Parametro>();
            LstTipoDocumento = new List<TipoDocumento>();
        }
        public List<Area> LstArea { get; set; }
        public List<Parametro> LstOrigen { get; set; }
        public List<Parametro> LstPrioridad { get; set; }
        public List<TipoDocumento> LstTipoDocumento { get; set; }

        public List<Parametro> LstTipoDocumentoExterno { get; set; }
        public List<Parametro> LstTipoDocumentoInterno { get; set; }
    }
}