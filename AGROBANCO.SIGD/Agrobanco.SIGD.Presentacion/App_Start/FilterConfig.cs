﻿using Agrobanco.SIGD.Presentacion.Filters;
using System.Web;
using System.Web.Mvc;

namespace Agrobanco.SIGD.Presentacion
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            //filters.Add(new ErrorFilter());
            filters.Add(new AgroSeguridadAttribute());
            //filters.Add(new AutenticacionUsuarioAttribute());            
        }
    }
}
