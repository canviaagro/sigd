﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
   public class TipDocIdentidadBL : ITipDocIdentidadBL
    {
        #region Constructor

        private ITipDocIdentidadDAO TipDocIdentidadBD;
        private string strCadenaConexion;

        public TipDocIdentidadBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.TipDocIdentidadBD = new TipDocIdentidadDAO(strCadenaConexion);
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para el Listado de Tipo de Documentos Activos
        /// </summary>
        /// <returns>Retorna Lista genérica de Tipos de Documentos Activos</returns>   
        public List<TipDocIdentidad> ListarTipDocIdentidad()
        {
            return this.TipDocIdentidadBD.ListarTipDocIdentidad();
        }
        #endregion
    }
}
