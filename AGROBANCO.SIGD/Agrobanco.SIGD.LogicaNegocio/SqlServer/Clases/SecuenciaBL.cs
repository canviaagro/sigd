﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class SecuenciaBL : ISecuenciaBL
    {
        #region Constructor

        private ISecuenciaDAO SecuenciaBD;
        private string strCadenaConexion;

        public SecuenciaBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.SecuenciaBD = new SecuenciaDAO(strCadenaConexion);
        }

        public List<Secuencia> ListarSecuencia(FiltroMantenimientoBaseDTO texto, out int totalRegistros)
        {
            return this.SecuenciaBD.ListarSecuencia(texto, out totalRegistros);
        }

        public int RegistrarSecuencia(SecuenciaDTO obj)
        {
            return this.SecuenciaBD.RegistrarSecuencia(obj);
        }
        
        public int ActualizarSecuencia(SecuenciaDTO obj)
        {
            return this.SecuenciaBD.ActualizarSecuencia(obj);
        }

        public int EliminarSecuencia(SecuenciaDTO obj)
        {
            return this.SecuenciaBD.EliminarSecuencia(obj);
        }

        public SecuenciaDTO ObtenerSecuencia(SecuenciaDTO objParams)
        {
            return this.SecuenciaBD.ObtenerSecuencia(objParams);
        }

        public String ObtenerCorrelativo(SecuenciaDTO objParams)
        {
            return this.SecuenciaBD.ObtenerCorrelativo(objParams);
        }
        #endregion



    }
}
