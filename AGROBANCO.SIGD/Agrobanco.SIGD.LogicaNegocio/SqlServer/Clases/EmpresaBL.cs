﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class EmpresaBL : IEmpresaBL
    {
        #region Constructor

        private IEmpresaDAO EmpresaBD;        
        private string strCadenaConexion;

        public EmpresaBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.EmpresaBD = new EmpresaDAO(strCadenaConexion);
        }
 
        #endregion


        #region Métodos Públicos

        /// <summary>
        /// Método público para el Registro de una Nueva Empresa
        /// </summary>
        /// <returns>Retorna el Identificador de la Nueva Empresa</returns>  
        public int RegistrarEmpresa(Empresa objEmpresa)
        {
            return this.EmpresaBD.RegistrarEmpresa(objEmpresa);
        }

        /// <summary>
        /// Interface para el Listado de Empresas
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresas</returns> 
        public List<EmpresaDTO> ListarEmpresa()
        {
            return this.EmpresaBD.ListarEmpresa();
        }

        /// <summary>
        /// Método Público para el Listado de Representantes por Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Representantes por Empresa</returns>  
        public List<RepresentanteDTO> ListarRepresentantePorEmpresa(int codEmpresa)
        {
            return this.EmpresaBD.ListarRepresentantePorEmpresa(codEmpresa);
        }


        /// <summary>
        /// Método Público para el buscar Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        public List<Empresa> BuscarEmpresa(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            return this.EmpresaBD.BuscarEmpresa(filtro, out totalRegistros);
        }

        /// <summary>
        /// Método Público para el obtener un(a) Empresa
        /// </summary>
        /// <returns>Retorna Lista genérica de Empresa</returns>  
        public Empresa ObtenerEmpresa(Empresa objEmpresa)
        {
            return this.EmpresaBD.ObtenerEmpresa(objEmpresa);
        }

        /// <summary>
        /// Método Público para Registrar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Empresa objEmpresa)
        {
            return this.EmpresaBD.Registrar(objEmpresa);
        }
        /// <summary>
        /// Método Público para Actualizar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Empresa objEmpresa)
        {
            return this.EmpresaBD.Actualizar(objEmpresa);
        }


        /// <summary>
        /// Método Público para Eliminar Empresa
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Empresa objEmpresa)
        {
            return this.EmpresaBD.Eliminar(objEmpresa);
        }



        #endregion

    }
}
