﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class RepresentanteLegalBL : IRepresentanteLegal
    {
        private IRepresentanteLegalDAO obj;
        private string strCadenaConexion;

        public RepresentanteLegalBL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.obj = new RepresentanteLegalDAO(strCadenaConexion);
        }


        public RepresentanteLegalDTO ObtenerRepresentanteLegal()
        {
            return this.obj.ObtenerRepresentanteLegal();
        }

        public int GrabarRepresentanteLegal(RepresentanteLegalDTO obj)
        {
            return this.obj.GrabarRepresentanteLegal(obj);
        }

        public int EditarRepresentanteLegal(RepresentanteLegalDTO obj)
        {
            return this.obj.EditarRepresentanteLegal(obj);
        }
        
    } 
}