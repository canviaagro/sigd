﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
   public class TipoDocumentoBL : ITipoDocumentoBL
    {
        #region Constructor

        private ITipoDocumentoDAO TipoDocumentoBD;
        private string strCadenaConexion;

        public TipoDocumentoBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.TipoDocumentoBD = new TipoDocumentoDAO(strCadenaConexion);
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para el Listado de Tipo de Documentos Activos
        /// </summary>
        /// <returns>Retorna Lista genérica de Tipos de Documentos Activos</returns>   
        public List<TipoDocumentoDTO> ListarTipoDocumento()
        {
            return this.TipoDocumentoBD.ListarTipoDocumento();
        }
        #endregion
    }
}
