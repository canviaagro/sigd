﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class ReportesBL : IReportesBL
    {

        #region Constructor

        private IReportesDAO ReporteBD;
        private string strCadenaConexion;

        public ReportesBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.ReporteBD = new ReportesDAO(strCadenaConexion);
        }
        #endregion

        
        public List<ReportesDTO.ReporteMesaParteDTO> ReporteMesaPartes(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros)
        {
            return this.ReporteBD.ReporteMesaPartesDAO(objParams, out totalRegistros);
        }

        public List<ReportesDTO.ReporteMesaParteDTO> ReporteAreaFechaEstado(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros)
        {
            return this.ReporteBD.ReporteAreaFechaEstadoDAO(objParams, out totalRegistros);
        }

        public List<ReportesDTO.ReporteMesaParteDTO> ReporteSeguimientoDocumentos(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros)
        {
            return this.ReporteBD.ReporteSeguimientoDocumentosDAO(objParams, out totalRegistros);
        }

        public List<ReportesDTO.ReporteMesaParteDTO> ReporteListarAño()
        {
            return this.ReporteBD.ReporteListarAñoDAO();
        }

        public List<ReportesDTO.ReporteAtencionDocumentoDTO> ReporteAtencionDocumento(ReportesDTO.ReporteAtencionDocumentoDTO objParams, out int totalRegistros)
        {
            return this.ReporteBD.ReporteAtencionDocumento(objParams, out totalRegistros);
        }
    }
}
