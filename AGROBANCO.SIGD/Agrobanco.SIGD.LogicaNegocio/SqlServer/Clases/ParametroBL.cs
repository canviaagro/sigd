﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
   public class ParametroBL : IParametroBL
    {
        #region Constructor

        private IParametroDAO ParametroBD;
        private string strCadenaConexion;

        public ParametroBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.ParametroBD = new ParametroDAO(strCadenaConexion);
        }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para obtener Parametros por Grupo
        /// </summary>
        /// <returns>Retorna el Listado de Parámetros según el Grupo enviado como parámetro</returns> 
        public List<ParametroDTO> ListarParametrosPorGrupo(int codGrupo)
        {
            return this.ParametroBD.ListarParametrosPorGrupo(codGrupo);
        }
        #endregion
    }
}
