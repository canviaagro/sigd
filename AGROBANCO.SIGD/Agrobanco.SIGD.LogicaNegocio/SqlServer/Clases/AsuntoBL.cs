﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class AsuntoBL : IAsuntoBL
    {
        #region Constructor

        private IAsuntoDAO AsuntoBD;
        private string strCadenaConexion;

        public AsuntoBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.AsuntoBD = new AsuntoDAO(strCadenaConexion);
        }



        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Interface para el Listado de Asuntos
        /// </summary>
        /// <returns>Retorna Lista genérica de Asuntos</returns>  
        public List<AsuntoDTO> ListarAsunto()
        {
            return this.AsuntoBD.ListarAsunto();
        }
        
        /// <summary>
        /// Método Público para el buscar Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        public List<Asunto> BuscarAsunto(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            return this.AsuntoBD.BuscarAsunto(filtro, out totalRegistros);
        }

        /// <summary>
        /// Método Público para el obtener un(a) Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        public Asunto ObtenerAsunto(Asunto objAsunto)
        {
            return this.AsuntoBD.ObtenerAsunto(objAsunto);
        }

        /// <summary>
        /// Método Público para Registrar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Asunto objAsunto)
        {
            return this.AsuntoBD.Registrar(objAsunto);
        }
        /// <summary>
        /// Método Público para Actualizar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Asunto objAsunto)
        {
            return this.AsuntoBD.Actualizar(objAsunto);
        }


        /// <summary>
        /// Método Público para Eliminar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Asunto objAsunto)
        {
            return this.AsuntoBD.Eliminar(objAsunto);
        }

        #endregion
        
    }
}
