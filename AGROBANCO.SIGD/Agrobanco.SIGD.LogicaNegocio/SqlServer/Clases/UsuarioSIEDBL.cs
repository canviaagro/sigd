﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class UsuarioSIEDBL : IUsuarioSIED
    {
        private IUsuarioSIEDDAO obj;
        private string strCadenaConexion;

        public UsuarioSIEDBL()
        {
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.obj = new UsuariosSIEDDAO(strCadenaConexion);
        }


        public UsuarioSIEDDTO ObtenerUsuarioSIED()
        {
            return this.obj.ObtenerUsuarioSIED();
        }

        public int GrabarUsuarioSIED(UsuarioSIEDDTO obj)
        {
            return this.obj.GrabarUsuarioSIED(obj);
        }

        public int EditarUsuarioSIED(UsuarioSIEDDTO obj)
        {
            return this.obj.EditarUsuarioSIED(obj);
        }
        
    } 
}