﻿using Agrobanco.SIGD.AccesoDatos.SqlServer.Acceso;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Clases;
using Agrobanco.SIGD.AccesoDatos.SqlServer.Interfaces;
using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces;
using System;
using System.Collections.Generic;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Clases
{
    public class FeriadoBL : IFeriadoBL
    {
        #region Constructor

        private IFeriadosDAO FeriadoBD;
        private string strCadenaConexion;

        public FeriadoBL()
        {
            // obtener cadena de conexión desde el registro para EmpresaBD
            SqlServerAccess sqlAccess = new SqlServerAccess();

            strCadenaConexion = sqlAccess.ClaveConnectionStringSQL();

            if (strCadenaConexion.Equals(string.Empty))
            {
                throw new Exception("Error en la configuración de acceso a base de datos");
            }
            this.FeriadoBD = new FeriadoDAO(strCadenaConexion);
        }


        #endregion


        #region Métodos Públicos
        /// <summary>
        /// Método Público para el buscar Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        public List<Feriado> BuscarFeriado(FiltroMantenimientoBaseDTO filtro, out int totalRegistros)
        {
            return this.FeriadoBD.BuscarFeriado(filtro, out totalRegistros);
        }

        /// <summary>
        /// Método Público para el obtener un(a) Feriado
        /// </summary>
        /// <returns>Retorna Lista genérica de Feriado</returns>  
        public Feriado ObtenerFeriado(Feriado objFeriado)
        {
            return this.FeriadoBD.ObtenerFeriado(objFeriado);
        }

        /// <summary>
        /// Método Público para Registrar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Registrar(Feriado objFeriado)
        {
            return this.FeriadoBD.Registrar(objFeriado);
        }
        /// <summary>
        /// Método Público para Actualizar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Actualizar(Feriado objFeriado)
        {
            return this.FeriadoBD.Actualizar(objFeriado);
        }


        /// <summary>
        /// Método Público para Eliminar Feriado
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        public int Eliminar(Feriado objFeriado)
        {
            return this.FeriadoBD.Eliminar(objFeriado);
        }

        #endregion
    }
}
