﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
   public interface IAsuntoBL
    {
        /// <summary>
        /// Interface para el Listado de Asuntos
        /// </summary>
        /// <returns>Retorna Lista genérica de Asuntos</returns>     
        List<AsuntoDTO> ListarAsunto();


        /// <summary>
        /// Método Público para el buscar Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        List<Asunto> BuscarAsunto(FiltroMantenimientoBaseDTO texto, out int totalRegistros);
        /// <summary>
        /// Método Público para el obtener un(a) Asunto
        /// </summary>
        /// <returns>Retorna Lista genérica de Asunto</returns>  
        Asunto ObtenerAsunto(Asunto objAsunto);
        /// <summary>
        /// Método Público para Registrar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Registrar(Asunto objAsunto);
        /// <summary>
        /// Método Público para Actualizar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Actualizar(Asunto objAsunto);
        /// <summary>
        /// Método Público para Eliminar Asunto
        /// </summary>
        /// <returns>Retorna entero si es mayor que cero fue satisfactorio</returns>  
        int Eliminar(Asunto objAsunto);

    }
}
