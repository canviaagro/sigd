﻿using Agrobanco.SIGD.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface IAreaBL
    {
        /// <summary>
        /// Interface para el Listado de Áreas
        /// </summary>
        /// <returns>Retorna Lista genérica de Áreas</returns>     
        List<AreaDTO> ListarArea();
        List<AreaDTO> ListarAreaSIED();
        /// <summary>
        ///  Interface para obtener Datos De area Por ID
        /// </summary>
        /// <returns>Retorna Entidad Area</returns>  
        AreaDTO ObtenerAreasPorId(AreaDTO objParams);

        int RegistrarArea(AreaDTO objArea);

        int ActualizarArea(AreaDTO objArea);

        int EliminarArea(AreaDTO objArea);
    }
}
