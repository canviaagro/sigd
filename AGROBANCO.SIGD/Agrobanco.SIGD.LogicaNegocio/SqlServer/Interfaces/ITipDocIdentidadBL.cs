﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface ITipDocIdentidadBL
    {
        /// <summary>
        /// Interface para el Listado de Tipo de Doc.Identidad
        /// </summary>
        /// <returns>Retorna Lista genérica de Tipos de Documentos Identidad</returns>     
        List<TipDocIdentidad> ListarTipDocIdentidad();
    }
}
