﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface ITrabajador
    {
        /// <summary>
        /// Interface para el Listado de Trabajadores por Area
        /// </summary>
        /// <returns>Retorna Lista genérica de Trabajadores por Area</returns>     
        List<TrabajadorDTO> ListarTrabajadorPorArea(EMaeTrabajador objTrabajador);
        

        /// <summary>
        ///  Interface para obtener Datos De area Por ID
        /// </summary>
        /// <returns>Retorna Entidad Trabajador</returns>  
        TrabajadorDTO ObtenerTabajadorPorId(TrabajadorDTO objParams);

        int RegistrarTrabajador(TrabajadorDTO objTrabajador);

        int ActualizarTrabajador(TrabajadorDTO objTrabajador);

        int EliminarTrabajador(TrabajadorDTO objTrabajador);
        List<TrabajadorDTO> ListarTipDocIdentidad();
        
    }
}
