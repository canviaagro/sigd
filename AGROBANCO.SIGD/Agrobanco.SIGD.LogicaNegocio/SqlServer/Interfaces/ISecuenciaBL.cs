﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
   public interface ISecuenciaBL
    {
        /// <summary>
        /// Método Público para Listar Secuencia
        /// </summary>
        /// <returns>Retorna Lista genérica de Secuencia</returns>  
        List<Secuencia> ListarSecuencia(FiltroMantenimientoBaseDTO texto, out int totalRegistros);

        /// <summary>
        /// Interface para la anulacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anulacion del Documento</returns> 
        int RegistrarSecuencia(SecuenciaDTO objDocumento);

        /// <summary>
        /// Interface para la anulacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anulacion del Documento</returns> 
        int ActualizarSecuencia(SecuenciaDTO objDocumento);
        /// <summary>
        /// Interface para eliminar una secuencia
        /// </summary>
        /// <returns>Retorna el Indicador de eliminacion de una secuencia</returns> 
        int EliminarSecuencia(SecuenciaDTO objDocumento);
        /// <summary>
        /// Interface para obtener objeto secuencia
        /// </summary>
        /// <returns>Retorna el objeto secuencia</returns> 
        SecuenciaDTO ObtenerSecuencia(SecuenciaDTO objParams);
        /// <summary>
        /// Interface para obtener un correlativo
        /// </summary>
        /// <returns>Retorna el correlativo</returns> 
        String ObtenerCorrelativo(SecuenciaDTO objParams);
        
    }
}
