﻿using Agrobanco.SIGD.Comun;
using Agrobanco.SIGD.Entidades;
using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface IDocumentoBL
    {
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        Documento ObtenerDocumento(Documento objParams, out string informacion);
        /// <summary>
        ///  Interface para obtener Datos Del Documento de Recepcion
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        Documento ObtenerDocumentoRecepcionSIED(Documento objParams, out string informacion);
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        Documento ObtenerDocumentoInterno(Documento objParams, out string informacion);
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        Documento ObtenerDocumentoSIED(Documento objParams, out string informacion); 
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        EnvioSIED ObtenerDocumentoSIED_SeviceEnvio(Documento objParams);

        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        TipoDocumentoDTO ObtenerTipoDocumento(TipoDocumentoDTO objParams);
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        int EliminarTipoDocumento(TipoDocumentoDTO objParams);
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        DocDerivacion GetDocumentoDerivadoPorId(int codigo);
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        DocDerivacion ObtenerDocumentoDerivadoxTrabajador(DocDerivacion objParams);
        /// <summary>
        ///  Interface para obtener derivaciones del documento
        /// </summary>
        /// <returns>Retorna Entidad DocDerivacion</returns>  
        List<DocDerivacion> GetDocumentosDerivadosPorCodDocumento(int CodDocumento, int TipoAcceso);
        /// <summary>
        ///  Interface para finalizar documento SIED
        /// </summary>
        /// <returns>Retorna un entero</returns>  
        int FinalizarDocumentoEnvioSIED(Documento objDocumento);
        /// <summary>
        ///  Interface para obtener Datos Del Documento
        /// </summary>
        /// <returns>Retorna Entidad Documento</returns>  
        List<DocDerivacion> ObtenerDocumentoDerivado(DocDerivacion objParams);

        /// <summary>
        /// Interface para el listado de Bandeja Mesa Partes
        /// </summary>
        /// <returns>Retorna lista generica Bandeja Mesa de Partes</returns>     
        List<DocumentoBandejaDTO> ListarBandejaMesaPartes(DocumentoBandejaParamsDTO objParams);

        /// <summary>
        /// Interface para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>     
 		int RegistrarDocumento(Documento objMovDocumento, out string outCorrelativoDoc, out int RespuestaCorreo);
        /// <summary>
        /// Interface para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>     
        int RegistrarDocumentoBandejaEntrada(Documento objMovDocumento, out string outCorrelativoDoc);
        /// <summary>
        /// Interface para el Registro de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Registro del Documento</returns>     
        int RegistrarDocumentoBandejaEnvioSIED(Documento objMovDocumento, out string outCorrelativoDoc);
        /// <summary>
        /// Interface para la anulacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anulacion del Documento</returns> 
        int AnularDocumento(ComentarioDocumentoDTO objDocumento);
        int AnularDocumentoYDerivados(ComentarioDocumentoDTO objDocumento);
        int AnularDocumentoEnvioSIED(ComentarioDocumentoDTO objDocumento);
        int AnularDocumentoRecepcionSIED(ComentarioDocumentoDTO objDocumento);
        /// <summary>
        /// Interface para registrar avance del Documento
        /// </summary>
        /// <returns>Retorna el Indicador de registro de avance del Documento</returns> 
        int RegistrarAvance(RegistrarAvanceDTO objDocumento, out int resultadoCorreo);

        /// <summary>
        /// Interface para la anulacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anulacion del Documento</returns> 
        int DerivarDocumento(DerivarDocumentoDTO objDocumento, out int resultadoCorreo);

        /// <summary>
        /// Interface para la anulacion de un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Anulacion del Documento</returns> 
        int RegistrarTipoDocumento(TipoDocumentoDTO objDocumento);

        /// <summary>
        /// Interface para actualizar el tipo de documento
        /// </summary>
        /// <returns>Retorna el Indicador de actualizacion del Documento</returns> 
        int ActualizarTipoDocumento(TipoDocumentoDTO objDocumento);

        /// <summary>
        /// Interface para devolver un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Devolucion del Documento</returns> 
        int DevolverDocumento(ComentarioDocumentoDTO objDocumento);
        
        /// <summary>
        /// Interface para devolver un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Devolucion del Documento</returns> 
        int DevolverDocumentoInterno(ComentarioDocumentoDTO objDocumento);
        /// <summary>
        /// Interface para finalizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Devolucion del Documento</returns> 
        int FinalizarDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje);
        int FinalizarDocumentoBandejaEntrada(ComentarioDocumentoDTO objDocumento, out string mensaje, out int RespuestaCorreo);
        
        /// <summary>
        /// Interface para Actualizar un Documento
        /// </summary>
        /// <returns>Retorna el Indicador de Actualizacion del Documento</returns> 
        int ActualizarDocumento(Documento objMovDocumento);
        int ActualizarDocumentoInterno(Documento objMovDocumento);
        int ActualizarDocumentoInternoEnvioSIED(Documento objMovDocumento);
        /// <summary>
        /// Interface para Calcular la fecha de plazo del documento
        /// </summary>
        /// <returns>Retorna el Indicador de Fecha de Plazo del Documento</returns>     
        DateTime CalcularFechaPlazoDocumento(int cantidadPlazo, DateTime fechaInicio);
        RespuestaConsultaArchivosDTO DescargarArchivoLaserfiche(int codLaserFiche);

        List<DocumentoSeguimientoDTO> GetSeguimientoDocumento(int iCodDocumento);
        
        List<DocumentoSeguimientoDTO> GetCabeceraSeguimiento(int iCodDocumento);

        List<DocumentoSeguimientoDTO> GetCabeceraSeguimientoInterno(int iCodDocumento);

        List<DocumentoSeguimientoDTO> GetDetalleSeguimiento(int iCodDocumento);

        List<ComentarioDocumentoDTO> GetComentariosDocumento(int iCodDocumento);

        List<DocAnexo> GetDocumentoAnexos(DocAnexo EObj); 

        List<DocAnexo> GetDocumentoAnexosNoLF(DocAnexo EObj);

        int ValidarFinalizarDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje,out string codDerivados);

        int ValidarDevolverDocumento(ComentarioDocumentoDTO objDocumento, out string mensaje, out string codDerivados);

        int SubirDocumentoLaserFicher(Documento obj);

        int ValidarFirmante(int codDocumento, int codTrabajador);

        int DocumentoAtencionFirma(int CodDocumento, int Atencion);

        int ObtenerEstadoFirmaDocumento(int CodDocumento);

        int ObtenerCantidadFirmas(int CodDocumento, out int posx, out int posy);

        int ObtenerCodLaserFIchePorCodDocumento(int id);

        List<int> ObtenerCodLaserFIcheAnexosPorCodDocumento(int id);

        int RegistrarCargo(RegistroCargoDTO registroCargo);

        int RegistrarCargoSIED(RegistroCargoDTO registroCargo);

        int ObtenerTotalDerivaciones(int codDocumento, int tipoAcceso);

        int AnularDerivacion(DocDerivacion docDerivacion, string comentario);

        int ObtenerEstado(int codDocumento);

    }
}
