﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public interface IRepresentanteLegal
    {
        /// <summary>
        /// Interface para el Listado de RepresentanteLegal
        /// </summary>
        /// <returns>Retorna Lista genérica de UsuarioSIED</returns>     
        RepresentanteLegalDTO ObtenerRepresentanteLegal();
        /// <summary>
        /// Interface para grabar/actualizar el UsuarioSIED
        /// </summary>
        /// <returns>Retorna un obejeto RepresentanteLegal</returns>     
        int GrabarRepresentanteLegal(RepresentanteLegalDTO obj);
        /// <summary>
        /// Interface para grabar/actualizar el RepresentanteLegal
        /// </summary>
        /// <returns>Retorna un obejeto UsuarioSIED</returns>     
        int EditarRepresentanteLegal(RepresentanteLegalDTO obj);

    }
}
