﻿using Agrobanco.SIGD.Entidades.DTO;
using Agrobanco.SIGD.Entidades.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agrobanco.SIGD.LogicaNegocio.SqlServer.Interfaces
{
    public  interface IReportesBL
    {
        /// <summary>
        ///  Interface para Obtener Reporte Mesa de Pates
        /// </summary>
        /// <returns>Retorna Entidad ReporteDocumentos Externos</returns>  
        List<ReportesDTO.ReporteMesaParteDTO> ReporteMesaPartes(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros);

        /// <summary>
        ///  Interface para Obtener Reporte Ara fecha y estado
        /// </summary>
        /// <returns>Retorna Entidad Reportede documentos internos</returns>  
        List<ReportesDTO.ReporteMesaParteDTO> ReporteAreaFechaEstado(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros);

        /// <summary>
        ///  Interface para Obtener Reporte De Seguimiento de Documentos
        /// </summary>
        /// <returns>Retorna Entidad Reportede documentos internos</returns> 
        List<ReportesDTO.ReporteMesaParteDTO> ReporteSeguimientoDocumentos(ReportesDTO.ReporteMesaParteDTO objParams, out int totalRegistros);
        List<ReportesDTO.ReporteMesaParteDTO> ReporteListarAño();
               
    }
}
